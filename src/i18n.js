import i18n from "i18next";
import XHR from "i18next-xhr-backend";
import { reactI18nextModule } from "react-i18next";
import localStorage from "local-storage";

let userDetails = localStorage.get("userDetails");
let languages = ["en", "fr", "ar", "es", "pt"];
let supportedBanks = ["00000", "00034", "60010", "00225"];
let langBankFolder = "00000";

if (userDetails !== "" && userDetails !== undefined && userDetails !== null) {
  lang = userDetails.codeLangue;
  bankCode = userDetails.codeBanque;
  if (supportedBanks.includes(bankCode)) {
    langBankFolder = bankCode;
  }
} else {
  let url = new URL(window.location.href);
  let params = new URL(document.location).searchParams;

  let cl = "en";
  let cb = "00000";

  if (
    url.pathname.indexOf("login") >= 0 ||
    params.get("cb") ||
    params.get("cl")
  ) {
    if (params.get("cb") && params.get("cl")) {
      cb = params.get("cb");
      cl = params.get("cl");
    }

    if (url.pathname.indexOf("login") >= 0) {
      cl = url.pathname.split("/")[3];
      cb = url.pathname.split("/")[2];
    }

    if (
      cl !== undefined &&
      cl !== null &&
      cl !== "" &&
      languages.includes(cl)
    ) {
      lang = cl;
    }
    if (supportedBanks.includes(cb)) {
      langBankFolder = cb;
    }
  }
}

i18n
  .use(XHR)
  .use(reactI18nextModule) // If not using I18nextProvider
  .init({
    lng: lang,
    whitelist: languages,
    fallbackLng: "fr",
    ns: ["common"],
    defaultNS: ["common"],
    debug: false,
    interpolation: {
      escapeValue: false, // Not needed for react!!
    },
    backend: {
      loadPath: `./locales/${langBankFolder}/${lang}/{{ns}}.json`,
      allowMultiLoading: false,
      crossDomain: false,
    },
    fallbackNS: ["common"],
    // React i18next special options (optional)
    react: {
      wait: true,
    },
  });

export default i18n;
