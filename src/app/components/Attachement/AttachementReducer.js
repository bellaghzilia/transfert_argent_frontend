const UPLOAD_DOCUMENTS = 'Document/UPLOAD_DOCUMENTS';
const UPLOAD_DOCUMENTS_SUCCESS = 'Document/UPLOAD_DOCUMENTS_SUCCESS';
const UPLOAD_DOCUMENTS_FAIL = 'Document/UPLOAD_DOCUMENTS_FAIL';

const UPLOAD_DOCUMENTSU = 'Document/UPLOAD_DOCUMENTSU';
const UPLOAD_DOCUMENTSU_SUCCESS = 'Document/UPLOAD_DOCUMENTSU_SUCCESS';
const UPLOAD_DOCUMENTSU_FAIL = 'Document/UPLOAD_DOCUMENTSU_FAIL';

const LOAD_ATTACHEMENTS = 'Document/LOAD_ATTACHEMENTS';
const LOAD_ATTACHEMENTS_SUCCESS = 'Document/LOAD_ATTACHEMENTS_SUCCESS';
const LOAD_ATTACHEMENTS_FAIL = 'Document/LOAD_ATTACHEMENTS_FAIL';

const LOAD_ATTACHEMENTSU = 'Document/LOAD_ATTACHEMENTSU';
const LOAD_ATTACHEMENTSU_SUCCESS = 'Document/LOAD_ATTACHEMENTSU_SUCCESS';
const LOAD_ATTACHEMENTSU_FAIL = 'Document/LOAD_ATTACHEMENTSU_FAIL';

const GET_PARAM = 'Document/GET_PARAM';
const GET_PARAM_SUCCESS = 'Document/GET_PARAM_SUCCESS';
const GET_PARAM_FAIL = 'Document/GET_PARAM_FAIL';

const RESET_DOCUMTENTS = 'Document/RESET_DOCUMTENTS';

const RELOAD = 'Document/RELOAD';

const RELOAD_DOC = 'Document/RELOAD_DOC';

const initialState = {
    cashDocumentsLoadSuccess: null,
    cashUploadedDocuments: null,
    showDocumentLoadModal: null,
    reloadFiles : false
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case UPLOAD_DOCUMENTSU:
            return {
                ...state,
                loading: true,
                iSuccessContrat:false,
            };
        case UPLOAD_DOCUMENTSU_SUCCESS:
            let allLoadedDocsU = [];
            allLoadedDocsU = state.loadedDocuments;
            allLoadedDocsU !== undefined ? allLoadedDocsU.concat(action.result) : allLoadedDocsU;
            return {
                ...state,
                loadedDocuments: action.result.documents,
                newDoc:action.result.documentUpgrade,
                UploadedDocuments: allLoadedDocsU,
                cashDocumentsLoadSuccess: true,
                showDocumentLoadModal: true,
                iSuccessContrat:true,
                error: null,
                loading: false
            };
        case UPLOAD_DOCUMENTSU_FAIL:
            return {
                ...state,
                UploadedDocuments: action.error,
                cashDocumentsLoadSuccess: false,
                showDocumentLoadModal: true,
                loading: false,
                iSuccessContrat:false,
            };
        case UPLOAD_DOCUMENTS:
            return {
                ...state,
                loading: true,
                iSuccessContrat:false,
                reloadFiles: false
            };
        case UPLOAD_DOCUMENTS_SUCCESS:
            let allLoadedDocs = [];
            allLoadedDocs = state.loadedDocuments;
            allLoadedDocs !== undefined ? allLoadedDocs.concat(action.result) : allLoadedDocs;
            if(action.result.success) {
                return {
                    ...state,
                    loadedDocuments: action.result.documents,
                    UploadedDocuments: allLoadedDocs,
                    cashDocumentsLoadSuccess: true,
                    showDocumentLoadModal: true,
                    isUploadSuccessContrat: true,
                    error: null,
                    loading: false,
                    contratErrorMsg : null,
                    reloadFiles: false,
                    messageRetour: action.result.messageRetour
                };
            }else{
                return {
                    ...state,
                    isUploadSuccessContrat: false,
                    error: null,
                    loading: false,
                    contratErrorMsg: action.result.messageRetour,
                    reloadFiles: false,
                    messageRetour: action.result.messageRetour,
                };
            }
        case UPLOAD_DOCUMENTS_FAIL:
            return {
                ...state,
                UploadedDocuments: action.error,
                cashDocumentsLoadSuccess: false,
                showDocumentLoadModal: true,
                loading: false,
                iSuccessContrat:false,
            };

        case LOAD_ATTACHEMENTS:
            return {
                ...state,
                loading: true,
                loadedDocuments: null,
                reloadFiles: false
            };
        case LOAD_ATTACHEMENTS_SUCCESS:
            return {
                ...state,
                loadedDocuments: action.result.documents,
                cashDocumentsLoadSuccess: true,
                showDocumentLoadModal: true,
                error: null,
                loading: false
            };

        case LOAD_ATTACHEMENTS_FAIL:
            return {
                ...state,
                loadedDocuments: action.error,
                cashDocumentsLoadSuccess: false,
                showDocumentLoadModal: true,
                loading: false
            };
        case LOAD_ATTACHEMENTSU:
                return {
                    ...state,
                    loading: true,
                    reloadFiles: false
                };
        case LOAD_ATTACHEMENTSU_SUCCESS:
                return {
                    ...state,
                    loadedDocumentsU: action.result.documents,
                    cashDocumentsLoadSuccess: true,
                    showDocumentLoadModal: true,
                    error: null,
                    loading: false
                };
        case LOAD_ATTACHEMENTSU_FAIL:
                return {
                    ...state,
                    loadedDocumentsU: action.error,
                    cashDocumentsLoadSuccess: false,
                    showDocumentLoadModal: true,
                    loading: false
                };

        case GET_PARAM:
            return {
                ...state,

            };
        case GET_PARAM_SUCCESS:
            return {
                ...state,
                param:action.result.param,
            };
        case GET_PARAM_FAIL:
            return {
                ...state,
                param:false,

            };


        case RESET_DOCUMTENTS :
            return {
                ...state,
                loadedDocuments: [],
                reloadFiles : true,
                newDoc:[],
                contratErrorMsg : null,
                cashDocumentsLoadSuccess : false
            };
        case RELOAD :
            return{
                ...state,
                contratErrorMsg : null,
                iSuccessContrat:false,
                reloadFiles: false,
                cashDocumentsLoadSuccess : false
            };
        case RELOAD_DOC:
            return{
                ...state,
                reloadFiles: false,
                cashDocumentsLoadSuccess : false
            };
        default:
            return state;
    }
}

export function uploadDocuments(id, documents, isContrat, isValidate, isUpdate=false) {
    const params = {type: 'multipart/form-data'};
    var formData = new FormData();
    for (var i = 0; i < documents.length; i++) {
        let document = documents[i];
        console.log(document);
        for (var key in document) {
            formData.append(key, document[key]);
        }
    }
    let url = {};
    if(isValidate===undefined) 
        isValidate=false;
    url = 'demandeSouscription/saveDocuments/' + id + '?isContrat=' + isContrat + '&isValidate=' + isValidate + '&isUpdate=' + isUpdate;
    return {
        types: [UPLOAD_DOCUMENTS, UPLOAD_DOCUMENTS_SUCCESS, UPLOAD_DOCUMENTS_FAIL],
        promise: (client) => client.post(url, {params, data: formData})
    };
}

export function uploadDocumentUpgrade(id, documents, isContrat) {
    console.log("upload documents ", id, documents);
    const params = {type: 'multipart/form-data'};
    var formData = new FormData();
    for (var i = 0; i < documents.length; i++) {
        let document = documents[i];
        console.log(document);
        for (var key in document) {
            formData.append(key, document[key]);
        }
        console.log(formData);
    }
    let url = {};
    url = 'demandeSouscription/saveDocumentsUpgrade/' + id + '?isContrat=' + isContrat;
    console.log("url : " + url);
    console.log("params : " + params);
    console.log("formData : " + formData);
    return {
        types: [UPLOAD_DOCUMENTSU, UPLOAD_DOCUMENTSU_SUCCESS, UPLOAD_DOCUMENTSU_FAIL],
        promise: (client) => client.post(url, {params, data: formData})
    };
}

export function resetDocuments() {
    return {
        type: RESET_DOCUMTENTS,
    }
}

export function getDocuments(id) {
    return {
        types: [LOAD_ATTACHEMENTS, LOAD_ATTACHEMENTS_SUCCESS, LOAD_ATTACHEMENTS_FAIL],
        promise: (client) => client.get('demandeSouscription/getDocuments/'+ id)
    };
}

export function getDocumentsUpgrade(id, isUpgrade) {
    return {
        types: [LOAD_ATTACHEMENTSU, LOAD_ATTACHEMENTSU_SUCCESS, LOAD_ATTACHEMENTSU_FAIL],
        promise: (client) => client.get('demandeSouscription/getDocumentsUpgrade/'+ id + '?isUpgrade=' + isUpgrade)
    };
}


function objectToParams(object) {
    let str = '';
    for (const key in object) {
        if (str !== '') {
            str += '&';
        }
        str += key + '=' + encodeURIComponent(object[key]);
    }
    return str;
}

export function reload() {
    return {
        type: RELOAD,
    };
}

export function reloadDocuments() {
    return {
        type: RELOAD_DOC,
    };
}

export function uploadDocumentsAgent(id, documents) {
    const params = {type: 'multipart/form-data'};
    var formData = new FormData();
    for (var i = 0; i < documents.length; i++) {
        let document = documents[i];
        for (var key in document) {
            formData.append(key, document[key]);
        }
    }
    return {
        types: [UPLOAD_DOCUMENTS, UPLOAD_DOCUMENTS_SUCCESS, UPLOAD_DOCUMENTS_FAIL],
        promise: (client) => client.post('agent/saveDocuments/' + id, {params, data: formData})
    };
}

export function getDocumentsAgent(id) {
    return {
        types: [LOAD_ATTACHEMENTS, LOAD_ATTACHEMENTS_SUCCESS, LOAD_ATTACHEMENTS_FAIL],
        promise: (client) => client.get('agent/getDocuments/'+ id)
    };
}
export function getuploadParam() {
    return {
        types: [GET_PARAM, GET_PARAM_SUCCESS, GET_PARAM_FAIL],
        promise: (client) => client.get('agent/getUploadParam')
    };
}