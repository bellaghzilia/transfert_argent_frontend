import React, {Component} from 'react';
import {Button, Col, Alert, Modal, FormControl, ButtonGroup, Row,Panel,ControlLabel} from 'react-bootstrap';
import * as AttachementActions from "./AttachementReducer";
import {reduxForm, initializeWithKey} from 'redux-form';
import {connect} from 'react-redux';
import imageCompression from 'browser-image-compression';
import {translate} from "react-i18next";

@connect(
    state => ({
        reloadFiles: state.AttachementReducer.reloadFiles
    }), {...AttachementActions, initializeWithKey})
export default class Attachments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // downloaded files => from the server
            downloadedFiles: this.props.files,//null,
            // files details => list of handles files
            filesDetails: [],
            // temporary file details
            tempDetails: {id: 0, description: null, file: null},
            show: false
        };
        this.handleDeleteFile = this.handleDeleteFile.bind(this);
        this.handleChanges = this.handleChanges.bind(this); 
        this.addFileDetails = this.addFileDetails.bind(this); 
        this.resetForm = this.resetForm.bind(this); 
        this.setResetFormCallback = this.setResetFormCallback.bind(this);
        this.handleDeleteDownloadedFile = this.handleDeleteDownloadedFile.bind(this);
        this.handleDownloadAction = this.handleDownloadAction.bind(this);
        this.handlePreviewFile = this.handlePreviewFile.bind(this);
    }
    // callback handler => handle target changes 
    handleChanges(e){
        let _description = this.state.tempDetails?this.state.tempDetails.description:null;
        let _file = this.state.tempDetails?this.state.tempDetails.file:null;
        switch (e.target.name) {
            case "description":
                _description = e.target.value;
            break;
            case "file":
                _file = e.target.files[0];
            break;
        }
        this.setState({
            tempDetails : {id:0, description: _description, file: _file}
        })
    }
    // add file details 
    async addFileDetails() {
        let extension = this.state.tempDetails.file.name.substr(this.state.tempDetails.file.name.length - 4).toLowerCase();
        let size = this.state.tempDetails.file.size / 1024;
        let upload = false ;

        if (size < 1024) {
            upload = true;
        } else {

            const options = {
                maxSizeMB: 1,
                maxWidthOrHeight: 1920,
                useWebWorker: true
            };

            let fileName = this.state.tempDetails.file.name;
            this.setState({loadingFile: true});
            const blobFile = await imageCompression(this.state.tempDetails.file, options);
            this.state.tempDetails.file = new File([blobFile], fileName);
            upload = true;
            this.setState({loadingFile: false});
        }

        if (upload && upload === true && extension && extension !== null && extension !== "" && (extension === ".jpg" || extension === ".png" || extension === "jpeg" || extension === "pdf" || extension === ".pdf" || extension === "docx" || extension === ".docx")) {
            let _filesDetails = this.state.filesDetails ? this.state.filesDetails : [];
            // check if a file is attached
            if (this.state.tempDetails.file === null || this.state.tempDetails.file === undefined)
                return;
            // add attached file
            _filesDetails.push({
                id: null /*_filesDetails.length*/,
                description: this.state.tempDetails.description,
                file: this.state.tempDetails.file,
                isDeleted: false
            });
            // apply changes
            this.setState({
                filesDetails: _filesDetails,
                tempDetails: {id: 0, description: null, file: null}
            });
            //reset form
            this.resetForm();

            // call parent callback handler
            this.props.handleFiles(this.state.filesDetails);
        } else {
            this.setState({showModal: true});
        }
    }
    // handle delete file action
    handleDeleteFile(id) {
        let _filesDetails = this.state.filesDetails;
        if(_filesDetails.length>0)
            _filesDetails.splice(id, 1);
        else
            _filesDetails = {};

        // apply changes
        this.setState({
            filesDetails: _filesDetails
        });
        //reset form
        //this.resetForm();

        // call parent callback handler  
        this.props.handleFiles(this.state.filesDetails);
    }
    // handle delete file action  ==> files were downloaded from server
    handleDeleteDownloadedFile(instance,index) {
        let _downloadedFiles = this.state.downloadedFiles;//this.props.files.slice();
        let _downloadedfile = _downloadedFiles.find((e,i)=>{
            return e.id === instance.id;
        });
        if(!this.isNullOrUndefined(_downloadedfile)){
            _downloadedFiles.splice(index,1);
            // notify the server that the file of id it was deleted
            let _filesDetails = this.state.filesDetails;
            _filesDetails.push({id:_downloadedfile.id,isDeleted:true});
            // apply changes
            this.setState({
                filesDetails: _filesDetails,
                downloadedFiles : _downloadedFiles
            });
            // call parent callback handler  
            this.props.handleFiles(this.state.filesDetails);
        }

    }

    componentWillUnmount(){
        console.log("test unmount");
    }

    handlePreviewFile(instance) {
        let extension = instance.nameDocument.substring(instance.nameDocument.lastIndexOf('.')).toLowerCase();
        let typeFile = '';
        if(extension === '.pdf')
            typeFile = 'application/pdf';
        let bytes = this.base64ToArrayBuffer(instance.contenu);
        let blob = new Blob([bytes], {type: typeFile});
        let url =  window.URL.createObjectURL(blob);
        //url = "data:" +  "application/octet-stream"   + ";base64,"  +  window.btoa(blob);
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, instance.nameDocument);
        } else {
            this.setState({
                previewFile: url,
                show: !this.state.show,
                typeFile: extension
            });
        }
    }

    handleShow = () => {
        this.setState({show: !this.state.show})
    };

    // handle download file action
    handleDownloadAction(instance,index){
        let bytes = this.base64ToArrayBuffer(instance.contenu);
        let blob = new Blob([bytes], {type: instance.typeDocument});
        let url =  window.URL.createObjectURL(blob);
        //url = "data:" +  "application/octet-stream"   + ";base64,"  +  window.btoa(blob);
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, instance.nameDocument);
        }else
            this.saveAs(url, instance.nameDocument);
    }
    // create link target ==> temporary creation
    saveAs(uri, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
          document.body.appendChild(link); //Firefox requires the link to be in the body
          link.download = filename;
          link.href = uri;
          link.click();
          document.body.removeChild(link); //remove the link when done
        } else {
          location.replace(uri);
        }
    }
    // convert base64 to binary then to array buffer 
    base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
           var ascii = binaryString.charCodeAt(i);
           bytes[i] = ascii;
        }
        return bytes;
     }
    // reset form 
    resetForm(){
        this.state.resetFormCallback.reset();
    }
    // call back  ==> to Reset Form
    setResetFormCallback(ref){
        this.setState({resetFormCallback: ref.form});
    }
    isNullOrUndefined(_object){
        if(_object == null || _object==undefined)
            return true;
        else 
            return false;
    }
    componentWillReceiveProps(nextProps){
        // refresh downloaded files
         if(this.props.files !== nextProps.files) {
             this.setState({downloadedFiles: nextProps.files});
         }

         if(nextProps.reloadFiles === true){
             this.setState({filesDetails : []});
         }
    }
    render() {
        const close = () => {
            this.setState({showModal: false});
        };
        const styles = require('./Attachments.scss');
        return (
            <div>
                <Modal show={this.state.loadingFile} className="loadingModal" backdrop="static" keyboard={false}>
                    <Modal.Body>
                        <Row>
                            <Col xs={12} md={12}>
                                <div className="spinner">
                                    <span style={{fontSize: '11px'}}>Chargement en cours ...</span>
                                </div>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal
                    show={this.state.showModal}
                    onHide={close}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                            <div>Erreur de téléchargement fichier</div>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>Le format téléchargé est invalide ! Merci d'utiliser un format image</div>
                    </Modal.Body>
                    <Modal.Footer>
                        <ButtonGroup
                            className="pull-right" bsSize="small"
                        >
                            <Button className={styles.ButtonPasswordStyle}
                                    onClick={() => close()}>Fermer</Button>
                        </ButtonGroup>
                    </Modal.Footer>
                </Modal>
                <Modal show={this.state.show} onHide={this.handleShow} container={this}>
                    <Modal.Body>
                        {this.state.typeFile === '.pdf' || this.state.typeFile === '.docx'?
                            <iframe src={this.state.previewFile} frameBorder="0" scrolling="auto" width="100%" height="500" />
                            :
                            <img src={this.state.previewFile} width="100%" height="100%" />
                        }
                    </Modal.Body>
                </Modal>
                {/* -- print downloaded files & and setup the click events like callbacks -- */}
                {this.state.downloadedFiles!=undefined &&
                    <DownLoadedFiles downloadedFiles={this.state.downloadedFiles} clickEvents = {this.props.filesClickEvents} handleDeleteDownloadedFile={this.handleDeleteDownloadedFile}
                                     handleDownloadAction={this.handleDownloadAction} handlePreviewFile={this.handlePreviewFile}/>
                }
                {/* --  -- */}
                {this.props.handleFiles!=undefined&&
                <div>
                    <Row style={{padding:'8px 0'}}>
                        <FormComponent   states = {this.state.tempDetails} handleChanges={this.handleChanges} addFileDetails={this.addFileDetails} setResetFormCallback={this.setResetFormCallback}/>
                    </Row>

                    {/* --  -- */}
                    {   this.state.filesDetails &&
                        this.state.filesDetails.map((x, index) =>{
                            return (x.isDeleted == false || this.isNullOrUndefined(x.isDeleted))?
                                <Row key = {index} style={{padding:'5px 0'}}> 
                                    <Col xs={10} sm={10} md={11}>   
                                        <Alert bsStyle="success" >
                                            <i className="fa fa-file-pdf-o fa-lg" style={{marginRight:'15px'}}/>{x.file?x.file.name:''} | {x.description}
                                        </Alert>
                                    </Col>
                                    <Col xs={2} sm={2} md={1}>
                                        <Button
                                            className="btncancel"
                                            style={{marginLeft: '10px',marginTop:'14px',padding:'15px 20px'}}
                                            onClick={() => {
                                                this.handleDeleteFile(index);
                                            }}
                                        ><i className="fa fa-times"  style={{marginRight: 0}}/>
                                        </Button>
                                    </Col>
                                </Row>:undefined
                            }
                    )}
                </div>
                }
            </div>
        );
    }
}
@translate(['attachments'])
class FormComponent extends Component{
    constructor(props){
        super(props);
        
    }
    componentDidMount() {
        this.props.setResetFormCallback(this.refs);
    }
    render(){
        const styles = require('./Attachments.scss');
        const {t} = this.props;

        return(
            <form ref="form">
            <Row style={{padding:'5px 0'}}>
                <Col xs={12} sm={12} md={5}>
                    <ControlLabel> {t('form.label.description')}</ControlLabel>
                </Col>
                <Col xs={10} sm={10} md={5}>
                    <ControlLabel> {t('form.label.fichier')}</ControlLabel>
                </Col>
                <Col xs={2} sm={2} md={2}>
                </Col>
            </Row>     
            <Row style={{padding:'5px 0'}} className={styles.fieldRow}>
                <Col xs={12} sm={12} md={5}>
                    <FormControl
                        {...this.props.states.description}
                        name = "description"
                        className={styles.datePickerFormControl}
                        bsClass="formControls" type="text"
                        placeholder={t('form.label.description')}
                        ref="descriptionRef"
                        //value={x.description}
                        onBlur={(e) => {
                            this.props.handleChanges(e);
                            }
                        }
                    />
                </Col>
                <Col xs={10} sm={10} md={5}>

                    <div>

                       { <input type="file" {...this.props.states.file} name = "file"
                                ref="fileRef"
                                accept="image/*"
                                style={{height: 'auto'}}
                               onChange={(e) => {this.props.handleChanges(e)}} id="PJFiles"
                               className='form-control' placeholder={t('form.label.fichier')}
                        />}
                    </div>

                </Col>
                <Col xs={2} sm={2} md={2}>
                    <Button
                        className="form-control"
                        onClick={(e) => {this.props.addFileDetails();}}
                        disabled = {(this.props.states.file == null || this.props.states.file == undefined)?true:false}
                    >
                        <i className="fa fa-plus" style={{marginRight: 0}}/>
                    </Button>
                </Col>
            </Row> 
            </form>
        );
    }
}

class DownLoadedFiles extends Component{
    constructor(props){
        super(props);
    }
    render(){
        const {downloadedFiles,handleDeleteDownloadedFile,handleDownloadAction, handlePreviewFile} = this.props;
        return(
            <div>
                <Row>
            {downloadedFiles && Array.isArray(downloadedFiles) && downloadedFiles.map((x, index) =>
                <Col key = {index} xs={12} sm={12} md={12}>
                    <Alert bsStyle="info" >
                        <i className="fa fa-file-pdf-o fa-lg" style={{marginRight:'15px'}}/> {x.nameDocument} | {x.description}
                        <Button
                            style={{float:'right',marginTop:'-7px'}}
                            //onClick={(e) => {this.props.clickEvents!=undefined&&this.props.clickEvents[1]!=undefined&&this.props.clickEvents[1](index);}}
                            disabled = {(this.props.clickEvents==false)?false:true}
                            onClick={(e) => {handleDeleteDownloadedFile(x,index);}}
                        >
                            <i className="fa fa-trash" />
                        </Button>
                        <Button
                            style={{float:'right',marginTop:'-7px',marginRight:'5px'}}
                            onClick={(e) => {handleDownloadAction(x,index);}}
                        >
                            <i className="fa fa-download" />
                        </Button>
                        <Button
                            style={{float:'right',marginTop:'-7px',marginRight:'5px'}}
                            onClick={(e) => {handlePreviewFile(x);}}
                        >
                            <i className="fa fa-eye" />
                        </Button>
                    </Alert>
                </Col>
                    /*<Col xs={12} sm={12} md={4}>
                        <Panel  bsStyle="" className={styles.Panel}>
                            <p style={{marginTop:"25px"}}><i className="fa fa-file-pdf-o fa-4x" style={{fontSize:'4em'}} /><br/><br/>{x.nameDocument}<br/><br/>
                            {x.description}
                            </p>
                            <Col xs={6} md={6}>
                            <Button
                                className={'btnvalide ' + styles.btnDownload+styles.btnLeft}
                                //href={url}
                                block
                            >
                                <i className="fa fa-download" />

                            </Button>
                            </Col>
                            <Col xs={6} md={6}>
                            <Button
                                className={'btnvalide ' + styles.btnDownload+styles.btnRight}
                                onClick={(e) => {this.props.clickEvents!=undefined&&this.props.clickEvents[1]!=undefined&&this.props.clickEvents[1](index);}}
                                block
                                disabled = {(this.props.clickEvents!=undefined&&this.props.clickEvents[1]!=undefined)?false:true}
                            >
                                <i className="fa fa-times" />
                            </Button>

                            </Col>
                        </Panel>
                    </Col>   */
            )}
            </Row>
            </div>
        );
    }
}

class DownloadedFiles extends Component {
    render() {
        const {
            t, handleSelect, open, files, filesClickEvents, filesClickEvent, handleFiles, styles,formState, disableALL,
            fichiersjoints, loadedDocuments
        } = this.props;
        return (
            <Panel
                collapsible
                expanded={open["8"]}
                onSelect={handleSelect}
                header={
                    <div>
                        {t('form.label.downloadFiles.title')}
                        <i className="pull-right  fa fa-chevron-down"/>
                    </div>
                }


                eventKey="8"
            >
                <fieldset>
                    <Row className={styles.fieldRow}>
                        <Col xs={12} sm={12} md={6}>


                            <fieldset>
                                <Row >
                                    <Col xs={12} sm={12} md={6}>

                                        <div>
                                            <Attachments title={"Télécharger les fichiers"} files={file}/>
                                        </div>
                                    </Col>
                                </Row>
                            </fieldset>
                            <Attachments title={"ajouter fichiers"} handleFiles={handleFiles}
                                         files={files}  filesClickEvents={filesClickEvents} styles={styles}/>

                        </Col>
                    </Row>
                </fieldset>
            </Panel>
        );
    }
}