import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router";
// import { logout } from "redux/modules/auth";

import { withNamespaces } from "react-i18next";

import {
  Button,
  Col,
  MenuItem,
  Nav,
  Navbar,
  NavDropdown,
  NavItem,
} from "react-bootstrap/lib";

class MenuDC extends Component {
  constructor() {
    super();
    this.state = {
      width: "0",
      showcomptes: "none",
      showpaiements: "none",
      showservices: "none",
      showadministration: "none",
      showmessagerie: "none",
    };
  }
  static propTypes = {
    // user: PropTypes.object,
  };
  render() {
    const { t, user, userFrontDetails, isMenuHidden } = this.props;
    const styles = require("./VerticalMenu.scss");
    const showMenuCashInEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASH_IN") !== -1;
    const showMenuCashOutEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASH_OUT") !== -1;
    const showMenuSouscriptionEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_SOUSCRIPTION") !== -1;
    const showMenuHistoriqueEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_HISTORIQUE") !== -1;
    const showMenuValidationSouscription =
      userFrontDetails.roles.indexOf("ROLE_EDP_SOUSCRIPTION_TO_VALIDATE") !==
      -1;
    const showMenuRejectedSouscription =
      userFrontDetails.roles.indexOf("ROLE_EDP_SOUSCRIPTION_REJETE") !== -1;
    const showMenuUpgrade =
      userFrontDetails.roles.indexOf("ROLE_EDP_UPGRADE") !== -1;
    const showMenuUpdateClient =
      userFrontDetails.roles.indexOf("ROLE_EDP_UPDATE_CLIENT") !== -1;
    const showMenuPaiementFactureEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_PAIEMENT_FACTURE") !== -1;
    const showMenuCreateDetaillantEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_DIGIFI_CREATE_DETAILLANT") !==
      -1;
    const showMenuEnrolementDistance =
      userFrontDetails.roles.indexOf("ROLE_EDP_ENROLEMENT_DISTANCE_SHOW") !==
      -1;

    const showMenuCreatePrincipalEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CREATE_AGENT_PRINCIPAL") !== -1;
    const showMenuCashInAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASH_IN_AGENT") !== -1;
    const showMenuCashOutAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASH_OUT_AGENT") !== -1;
    const showVirementEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_VIREMENT_SIMT") !== -1;

    const appMenu = (
      <Navbar className={styles.sideMenu + " vMobileMenu "}>
        <Navbar.Header style={{ padding: "10px", background: "#e6e6e6" }}>
          <div
            className="pull-left"
            style={{ textTransform: "uppercase", padding: "10px" }}
          >
            Bienvenue, {userFrontDetails.fullName}
          </div>
          <Button
            className="menuBtn"
            onClick={() => this.setState({ width: "100%" })}
          >
            <i className="fa fa-bars fa-2x" />
          </Button>
        </Navbar.Header>

        <div style={{ width: this.state.width }} className={styles.sidenav}>
          <a
            className={styles.closebtn}
            onClick={() => this.setState({ width: "0" })}
          >
            &times;
          </a>

          {true && (
            <Link
              to={{ pathname: baseUrl + "app/UpdateClient" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-user" /> {"update client"}
            </Link>
          )}

          {showMenuRejectedSouscription && (
            <Link
              to={{ pathname: baseUrl + "app/SouscriptionRejete" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-user-unfollow" /> {t("menu.rejete.title")}
            </Link>
          )}

          {showMenuSouscriptionEDPAgent && (
            <Link
              to={{ pathname: baseUrl + "app/SouscriptionType" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-note" /> Ouvrir un compte
            </Link>
          )}

          {showMenuUpgrade && (
            <Link
              to={{ pathname: baseUrl + "app/UpgradeRejete" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-refresh" /> Changements rejetés
            </Link>
          )}

          {showMenuValidationSouscription && (
            <Link
              to={{ pathname: baseUrl + "app/SouscriptionValidation" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-note" /> {t("menu.validation.title")}
            </Link>
          )}

          {showMenuValidationSouscription && (
            <Link
              to={{ pathname: baseUrl + "app/UpgradeValidation" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-list" /> Gestion Upgrade
            </Link>
          )}

          {showVirementEDPAgent && (
            <Link
              to={{ pathname: baseUrl + "app/Virement" }}
              onClick={() => {
                this.closeMenu();
              }}
            >
              <i className="icon-arrow-right-circle" />{" "}
              <span>{t("menu.virement.title")}</span>
            </Link>
          )}

          {showMenuCashInEDPAgent && (
            <Link
              to={{ pathname: baseUrl + "app/CashIn" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-arrow-right-circle" /> {t("menu.cash.in")}
            </Link>
          )}

          {showMenuCashOutEDPAgent && (
            <Link
              to={{ pathname: baseUrl + "app/CashOut" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-arrow-left-circle" /> {t("menu.cash.out")}
            </Link>
          )}

          {showMenuCashInAgent && (
            <Link
              to={{ pathname: baseUrl + "app/CashInAgent" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-arrow-right-circle" /> {t("menu.cashAgent.in")}
            </Link>
          )}

          {showMenuCashOutAgent && (
            <Link
              to={{ pathname: baseUrl + "app/CashOutAgent" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-arrow-left-circle" /> {t("menu.cashAgent.out")}
            </Link>
          )}

          {showMenuUpgrade && (
            <Link
              to={{ pathname: baseUrl + "app/UpgradePack" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-equalizer" /> {t("menu.upgrade.title")}
            </Link>
          )}

          {showMenuPaiementFactureEDPAgent && (
            <Link
              to={{ pathname: baseUrl + "app/PaiementFacture/creanciersList" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-handbag" /> {t("menu.paiement.facture")}
            </Link>
          )}

          {showMenuHistoriqueEDPAgent && (
            <Link
              to={{ pathname: baseUrl + "app/Historique" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-home" /> {t("menu.historique.title")}
            </Link>
          )}

          {showMenuCreateDetaillantEDPAgent && (
            <Link
              to={{ pathname: baseUrl + "app/CreateAgent" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-home" /> {t("menu.create.agentDetaillant")}
            </Link>
          )}

          {showMenuCreatePrincipalEDPAgent && (
            <Link
              to={{ pathname: baseUrl + "app/CreateAgentPrincipal" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="icon-user" /> {t("menu.create.agentPrincipal")}
            </Link>
          )}

          {showMenuEnrolementDistance && (
            <Link
              to={{ pathname: baseUrl + "app/EnrolementDistance" }}
              onClick={() => this.setState({ width: "0" })}
            >
              <i className="fa fa-arrows-h" />
              {t("menu.enrolementDistance.title")}
            </Link>
          )}
        </div>
      </Navbar>
    );

    return appMenu;
  }
}
MenuDC = connect((state) => ({
  user: state.user,
  userFrontDetails: state.user.userFrontDetails,
}))(MenuDC);

export default withNamespaces(["menu"])(MenuDC);
