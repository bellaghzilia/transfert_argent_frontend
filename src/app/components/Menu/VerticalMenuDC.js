import React, { Component } from "react";
import { connect } from "react-redux";
import { translate } from "react-i18next";
import { Link } from "react-router";
import { MenuItem } from "react-bootstrap";
import { withNamespaces } from "react-i18next";

import ReactDOM from "react-dom";

// @translate(["menu"])
class VerticalMenuDC extends Component {
  constructor(props) {
    super(props);
    this.state = {
      widthcomptes: "0",
      widthpaiement: "0",
      widthservices: "0",
      widthadmin: "0",
      widthparam: "0",
      widthtrade: "0",
      collapsed: false,
      showEcTypes: false,
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleEcTypes = this.handleEcTypes.bind(this);
  }

  closeMenu() {
    this.setState({ widthcomptes: "0" });
    this.setState({ widthpaiement: "0" });
    this.setState({ widthservices: "0" });
    this.setState({ widthadmin: "0" });
    this.setState({ widthparam: "0" });
    this.setState({ widthtrade: "0" });
    this.setState({ showEcTypes: false });
  }

  handleEcTypes() {
    this.setState({ showEcTypes: !this.state.showEcTypes });
  }

  switchMenu() {
    this.setState({ collapsed: !this.state.collapsed });
  }

  componentDidMount() {
    document.addEventListener("click", this.handleClick);
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleClick);
  }

  handleClick = (e) => {
    const area = ReactDOM.findDOMNode(this.refs.area);
    if (!area.contains(e.target)) {
      this.closeMenu();
    }
  };

  render() {
    const { t, user, userFrontDetails, globalParam } = this.props;
    const styles = require("./VerticalMenu.scss");

    const showMenuCashInEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASH_IN") !== -1;
    const showMenuCashOutEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASH_OUT") !== -1;
    const showMenuSouscriptionEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_SOUSCRIPTION") !== -1;
    const showMenuSouscriptionMerchant =
      userFrontDetails.roles.indexOf("ROLE_EDP_UPDATE_MERCHANT") !== -1;
    const showMenuHistorique =
      userFrontDetails.roles.indexOf("ROLE_EDP_HISTORIQUE") !== -1;
    const showMenuHistoriqueAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_HISTORIQUE_AGENT") !== -1;
    const showMenuValidationSouscription =
      userFrontDetails.roles.indexOf("ROLE_EDP_SOUSCRIPTION_TO_VALIDATE") !==
      -1;
    const showMenuRejectedSouscription =
      userFrontDetails.roles.indexOf("ROLE_EDP_SOUSCRIPTION_REJETE") !== -1;
    const showMenuUpgrade =
      userFrontDetails.roles.indexOf("ROLE_EDP_UPGRADE") !== -1;
    const showMenuPaiementFactureEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_PAIEMENT_FACTURE") !== -1;
    const showMenuUpdateClient =
      userFrontDetails.roles.indexOf("ROLE_EDP_UPDATE_CLIENT") !== -1 ||
      userFrontDetails.roles.indexOf("ROLE_EDP_PRINCIPALE_UPDATE_CLIENT") !==
        -1 ||
      userFrontDetails.roles.indexOf("ROLE_EDP_DETAILLANT_UPDATE_CLIENT") !==
        -1;
    const showMenuUpdateMerchant =
      userFrontDetails.roles.indexOf("ROLE_EDP_UPDATE_MERCHANT") !== -1;
    const showMenuCreateDetaillantEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_DIGIFI_CREATE_DETAILLANT") !==
      -1;
    const showMenuCreatePrincipalEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CREATE_AGENT_PRINCIPAL") !== -1;
    const showMenuCashInAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASHIN_AGENT") !== -1;
    const showMenuCashOutAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASHOUT_AGENT") !== -1;
    const showMenuEnrolementDistance =
      userFrontDetails.roles.indexOf("ROLE_EDP_ENROLEMENT_DISTANCE_SHOW") !==
      -1;
    const showMenuTraitementMASSE =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASH/ENROLLEMENT_MASSE") !== -1;
    const showMenuFiltrageCreanciers =
      userFrontDetails.roles.indexOf("ROLE_EDP_FILTRAGE_CREANCIERS") !== -1;
    const showVirementEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_VIREMENT_SIMT") !== -1;
    const showGestionCarte =
      userFrontDetails.roles.indexOf("ROLE_EDP_MONETIQUE_GESTION_CARTE") !== -1;
    const showVenteCarte =
      userFrontDetails.roles.indexOf("ROLE_EDP_MONETIQUE_VENTE_CARTE") !== -1;
    const showCommandCarte =
      userFrontDetails.roles.indexOf("ROLE_EDP_MONETIQUE_COMMAND_CARTE") !== -1;
    const showEtatCarte =
      userFrontDetails.roles.indexOf("ROLE_EDP_MONETIQUE_ETAT_CARTE") !== -1;
    const showFraisCarte =
      userFrontDetails.roles.indexOf("ROLE_EDP_MONETIQUE_FRAIS_CARTE") !== -1;
    const showMenuUpdateAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_UPDATE_AGENT") !== -1;
    const showNotificationPushMasse =
      userFrontDetails.roles.indexOf("ROLE_EDP_NOTIFICATION_PUSH_MASSE") !== -1;
    const showViderCompte =
      userFrontDetails.roles.indexOf("ROLE_EDP_VIDER_COMPTE") !== -1;

    const menu = (
      <nav className={true ? "vMenu collapsed cp" : "vMenu"} ref="area">
        <div className="appLogo" />

        <div
          className="vMenuIcon"
          style={{
            height: "80px",
            paddingTop: "30px",
            background: "rgba(255, 255, 255, 0.11)",
          }}
        >
          <a
            onClick={() => {
              this.switchMenu();
            }}
            className="switchBtn"
          >
            <i className="icon-menu" />
          </a>
        </div>

        <MenuItem divider />

        <div className="vMenuIcon">
          <Link
            onClick={() => {
              this.closeMenu();
            }}
            to={{ pathname: baseUrl + "app/Historique" }}
          >
            <i className="icon-home" />
            <div>{t("menu.acceuil.title")}</div>
          </Link>
        </div>

        <MenuItem divider />

        <div>
          <div
            className={
              this.state.widthservices === "265px"
                ? "vMenuIcon active"
                : "vMenuIcon"
            }
          >
            <a
              onClick={() => {
                this.closeMenu();
                this.setState({ widthservices: "265px" });
              }}
            >
              <i className="icon-social-dropbox" />
              <div>{t("menu.services.title")}</div>
            </a>
          </div>

          <div
            className="sidenav2"
            id="sidenav2"
            style={{ width: this.state.widthservices }}
          >
            <a className="closebtn" onClick={() => this.closeMenu()}>
              &times;
            </a>
            <div>
              <div className="subHeader" id="subHeader">
                {t("menu.services.title")}
              </div>
              {showMenuSouscriptionEDPAgent && (
                <Link
                  to={{ pathname: baseUrl + "app/SouscriptionType" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-note" />{" "}
                  <span>{t("menu.compte.title")}</span>
                </Link>
              )}
              {showMenuSouscriptionMerchant && (
                <Link
                  to={{ pathname: baseUrl + "app/SouscriptionCommercant" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-note" />{" "}
                  <span>{t("menu.compteCommercant.title")}</span>
                </Link>
              )}
              {showMenuCreateDetaillantEDPAgent && (
                <Link
                  to={{ pathname: baseUrl + "app/CreateAgent" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-user-follow" />{" "}
                  <span>{t("menu.agent.agentDetaillant")}</span>
                </Link>
              )}
              {showMenuCreatePrincipalEDPAgent && (
                <Link
                  to={{ pathname: baseUrl + "app/CreateAgentPrincipal" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-user-follow" />{" "}
                  <span>{t("menu.agent.AgentPrincipal")}</span>
                </Link>
              )}
              {showMenuUpgrade && (
                <Link
                  to={{ pathname: baseUrl + "app/UpgradePack" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-equalizer" />{" "}
                  <span>{t("menu.upgrade.title")}</span>
                </Link>
              )}
              {showMenuTraitementMASSE && (
                <Link
                  to={{ pathname: baseUrl + "app/TraitementMasse" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-hourglass" />{" "}
                  <span>{t("menu.traitementMasse.title")}</span>
                </Link>
              )}
              {showMenuEnrolementDistance && (
                <Link
                  to={{ pathname: baseUrl + "app/EnrolementDistance" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="fa fa-arrows-h" />{" "}
                  <span>{t("menu.enrolementDistance.title")}</span>
                </Link>
              )}
              {showMenuUpdateClient && (
                <Link
                  to={{ pathname: baseUrl + "app/UpdateClient" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-people" />{" "}
                  <span>{t("menu.client.title")}</span>
                </Link>
              )}
              {showMenuUpdateMerchant && (
                <Link
                  to={{ pathname: baseUrl + "app/UpdateCommercant" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-people" />{" "}
                  <span>{t("menu.updateCommercant.title")}</span>
                </Link>
              )}
              {showMenuUpdateAgent && (
                <Link
                  to={{ pathname: baseUrl + "app/UpdateAgent" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-people" />{" "}
                  <span>{t("menu.agent.title")}</span>
                </Link>
              )}

              {showGestionCarte && (
                <Link
                  to={{ pathname: baseUrl + "app/GestionCartes" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-credit-card" />{" "}
                  <span>{t("menu.gestionCartes.title")}</span>
                </Link>
              )}
              {showCommandCarte && (
                <Link
                  to={{ pathname: baseUrl + "app/CommandCard" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-credit-card" />{" "}
                  <span>{t("menu.CommandCard.title")}</span>
                </Link>
              )}
              {showVenteCarte && (
                <Link
                  to={{ pathname: baseUrl + "app/VenteCarte" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-credit-card" />{" "}
                  <span>{t("menu.VenteCarte.title")}</span>
                </Link>
              )}
              {showEtatCarte && (
                <Link
                  to={{ pathname: baseUrl + "app/EtatCards" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-notebook" />{" "}
                  <span>{t("menu.card.etat")}</span>
                </Link>
              )}
              {showFraisCarte && (
                <Link
                  to={{ pathname: baseUrl + "app/Frait&Commission" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-arrow-left-circle" />{" "}
                  <span>{t("menu.frais.title")}</span>
                </Link>
              )}

              {showNotificationPushMasse && (
                <Link
                  to={{ pathname: baseUrl + "app/NotificationPushMasse" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-hourglass" />{" "}
                  <span>{t("menu.notificationPushMasse.title")}</span>
                </Link>
              )}
            </div>
          </div>
        </div>

        <MenuItem divider />

        <div>
          {(showMenuCashInEDPAgent ||
            showMenuCashOutAgent ||
            showMenuCashInAgent) && (
            <div
              className={
                this.state.widthpaiement === "265px"
                  ? "vMenuIcon active"
                  : "vMenuIcon"
              }
            >
              <a
                onClick={() => {
                  this.closeMenu();
                  this.setState({ widthpaiement: "265px" });
                }}
              >
                <i className="icon-wallet" />
                <div> {t("menu.operation.title")}</div>
              </a>
            </div>
          )}
          <div
            className="sidenav2"
            id="sidenav2"
            style={{ width: this.state.widthpaiement }}
          >
            <a className="closebtn" onClick={() => this.closeMenu()}>
              &times;
            </a>
            <div>
              <div className="subHeader" id="subHeader">
                {" "}
                {t("menu.operation.title")}
              </div>
              {showVirementEDPAgent && (
                <Link
                  to={{ pathname: baseUrl + "app/Virement" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="fa fa-money" />{" "}
                  <span>{t("menu.virement.title")}</span>
                </Link>
              )}
              {showMenuCashInEDPAgent && (
                <Link
                  to={{ pathname: baseUrl + "app/CashIn" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-arrow-right-circle" />{" "}
                  <span>{t("menu.cash.in")}</span>
                </Link>
              )}
              {showMenuCashOutEDPAgent && (
                <Link
                  to={{ pathname: baseUrl + "app/CashOut" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-arrow-left-circle" />{" "}
                  <span>{t("menu.cash.out")}</span>
                </Link>
              )}
              {showMenuCashInAgent && (
                <Link
                  to={{ pathname: baseUrl + "app/CashInAgent" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-arrow-right-circle" />{" "}
                  <span>{t("menu.cashAgent.in")}</span>
                </Link>
              )}
              {showMenuCashOutAgent && (
                <Link
                  to={{ pathname: baseUrl + "app/CashOutAgent" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-arrow-left-circle" />{" "}
                  <span>{t("menu.cashAgent.out")}</span>
                </Link>
              )}
              {showMenuPaiementFactureEDPAgent && (
                <Link
                  to={{
                    pathname: baseUrl + "app/PaiementFacture/creanciersList",
                  }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-handbag" />{" "}
                  <span>{t("menu.paiement.facture")}</span>
                </Link>
              )}
              {showViderCompte && (
                <Link
                  to={{ pathname: baseUrl + "app/ViderCompte" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-arrow-left-circle" />{" "}
                  <span>Vider un compte</span>
                </Link>
              )}
            </div>
          </div>
        </div>

        <MenuItem divider />

        <div>
          {showMenuFiltrageCreanciers && (
            <div
              className={
                this.state.widthparam === "265px"
                  ? "vMenuIcon active"
                  : "vMenuIcon"
              }
            >
              <a
                onClick={() => {
                  this.closeMenu();
                  this.setState({ widthparam: "265px" });
                }}
              >
                <i className="icon-wrench" />
                <div> {t("menu.parametrages.title")}</div>
              </a>
            </div>
          )}

          <div
            className="sidenav2"
            id="sidenav2"
            style={{ width: this.state.widthparam }}
          >
            <a className="closebtn" onClick={() => this.closeMenu()}>
              &times;
            </a>
            <div>
              <div className="subHeader" id="subHeader">
                {" "}
                {t("menu.parametrage.title")}
              </div>

              {showMenuFiltrageCreanciers && (
                <Link
                  to={{ pathname: baseUrl + "app/FiltrageCreanciers" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="fa fa-shopping-cart" />{" "}
                  <span>{t("menu.filtragecranciers.title")}</span>
                </Link>
              )}
            </div>
          </div>
        </div>

        <MenuItem divider />

        <div>
          <div
            className={
              this.state.widthadmin === "265px"
                ? "vMenuIcon active"
                : "vMenuIcon"
            }
          >
            <a
              onClick={() => {
                this.closeMenu();
                this.setState({ widthadmin: "265px" });
              }}
            >
              <i className="icon-calendar" />
              <div>{t("menu.historique1.title")} </div>
            </a>
          </div>

          <div
            className="sidenav2"
            id="sidenav2"
            style={{ width: this.state.widthadmin }}
          >
            <a className="closebtn" onClick={() => this.closeMenu()}>
              &times;
            </a>
            <div>
              <div className="subHeader" id="subHeader">
                {" "}
                {t("menu.historique1.title")}
              </div>
              {showMenuHistorique && (
                <Link
                  to={{ pathname: baseUrl + "app/Historique" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-notebook" />{" "}
                  <span>{t("menu.historique.title")}</span>
                </Link>
              )}
              {showMenuHistoriqueAgent && (
                <Link
                  to={{ pathname: baseUrl + "app/HistoriqueAgent" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-notebook" />{" "}
                  <span>{t("menu.historiqueOperationsAgent.title")}</span>
                </Link>
              )}
              {showMenuUpgrade && (
                <Link
                  to={{ pathname: baseUrl + "app/UpgradeRejete" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-refresh" />{" "}
                  <span>{t("menu.changementsrejetes.title")} </span>
                </Link>
              )}
              {showMenuValidationSouscription && (
                <Link
                  to={{ pathname: baseUrl + "app/SouscriptionValidation" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-list" />{" "}
                  <span>{t("menu.validation.title")}</span>
                </Link>
              )}
              {showMenuValidationSouscription && (
                <Link
                  to={{ pathname: baseUrl + "app/UpgradeValidation" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-reload" />{" "}
                  <span>{t("menu.changementEnInstance.title")}</span>
                </Link>
              )}
              {showMenuRejectedSouscription && (
                <Link
                  to={{ pathname: baseUrl + "app/SouscriptionRejete" }}
                  onClick={() => {
                    this.closeMenu();
                  }}
                >
                  <i className="icon-user-unfollow" />{" "}
                  <span>{t("menu.rejete.title")}</span>
                </Link>
              )}
            </div>
          </div>
        </div>
      </nav>
    );
    return menu;
  }
}
VerticalMenuDC = connect((state) => ({
  user: state.user,
  userFrontDetails: state.user.userFrontDetails,
}))(VerticalMenuDC);

export default withNamespaces(["menu"])(VerticalMenuDC);
