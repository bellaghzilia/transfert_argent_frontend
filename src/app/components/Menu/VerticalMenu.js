import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { translate } from "react-i18next";
import { Link } from "react-router";
// import { logout } from "redux/modules/auth";
import { MenuItem, Navbar, NavDropdown, NavItem } from "react-bootstrap/lib";

import { withNamespaces } from "react-i18next";
import ReactDOM from "react-dom";

class VerticalMenu extends Component {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.state = {
      widthcomptes: "0",
      widthpaiement: "0",
      widthservices: "0",
      widthadmin: "0",
      widthtrade: "0",
      collapsed: false,
      showEcTypes: false,
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleEcTypes = this.handleEcTypes.bind(this);
  }

  closeMenu() {
    this.setState({ widthcomptes: "0" });
    this.setState({ widthpaiement: "0" });
    this.setState({ widthservices: "0" });
    this.setState({ widthadmin: "0" });
    this.setState({ widthtrade: "0" });
    this.setState({ showEcTypes: false });
  }

  handleEcTypes() {
    this.setState({ showEcTypes: !this.state.showEcTypes });
  }

  switchMenu() {
    this.setState({ collapsed: !this.state.collapsed });
  }

  componentDidMount() {
    document.addEventListener("click", this.handleClick);
  }
  componentWillUnmount() {
    document.removeEventListener("click", this.handleClick);
  }

  handleClick = (e) => {
    const area = ReactDOM.findDOMNode(this.refs.area);
    if (!area.contains(e.target)) {
      this.closeMenu();
    }
  };

  render() {
    const { t, user, userFrontDetails, globalParam } = this.props;
    const styles = require("./VerticalMenu.scss");

    const showMenuCashOutEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASH_OUT") !== -1;
    const showMenuPaiementFactureEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_PAIEMENT_FACTURE") !== -1;
    const showMenuSouscriptionEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_SOUSCRIPTION") !== -1;
    const showMenuHistoriqueEDPAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_HISTORIQUE") !== -1;
    const showMenuValidationSouscription =
      userFrontDetails.roles.indexOf("ROLE_EDP_SOUSCRIPTION_TO_VALIDATE") !==
      -1;
    const showMenuRejectedSouscription =
      userFrontDetails.roles.indexOf("ROLE_EDP_SOUSCRIPTION_REJETE") !== -1;
    const showMenuUpgrade =
      userFrontDetails.roles.indexOf("ROLE_EDP_UPGRADE") !== -1;
    const showMenuUpdateClient =
      userFrontDetails.roles.indexOf("ROLE_EDP_UPDATE_CLIENT") !== -1 ||
      userFrontDetails.roles.indexOf("ROLE_EDP_PRINCIPALE_UPDATE_CLIENT") !==
        -1 ||
      userFrontDetails.roles.indexOf("ROLE_EDP_DETAILLANT_UPDATE_CLIENT") !==
        -1;

    const menu = (
      <nav
        className={
          this.state.collapsed ? "vMenu collapsed " + styles.cp : "vMenu"
        }
        ref="area"
      >
        <div className="appLogo" />

        <div
          className={styles.vMenuIcon}
          style={{
            height: "80px",
            paddingTop: "30px",
            background: "rgba(255, 255, 255, 0.11)",
          }}
        >
          <a
            onClick={() => {
              this.switchMenu();
            }}
            className={styles.switchBtn}
          >
            <i className="icon-menu" />
          </a>
        </div>

        <MenuItem divider />

        {showMenuHistoriqueEDPAgent && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{ pathname: baseUrl + "app/Historique" }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="icon-home" />{" "}
                <div>{t("menu.historique.title")}</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )}

        {showMenuSouscriptionEDPAgent && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{ pathname: baseUrl + "app/SouscriptionType" }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="icon-note" />
                <div>Ouvrir un compte</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )}

        {showMenuRejectedSouscription && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{ pathname: baseUrl + "app/SouscriptionRejete" }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="icon-user-unfollow" />
                <div>{t("menu.rejete.title")}</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )}

        {showMenuUpgrade && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{ pathname: baseUrl + "app/UpgradeRejete" }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="glyphicon glyphicon-align-justify" />
                <div>{t("menu.upgrade.rejete")}</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )}

        {showMenuValidationSouscription && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{ pathname: baseUrl + "app/SouscriptionValidation" }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="icon-home" />
                <div>{t("menu.validation.title")}</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )}

        {showMenuValidationSouscription && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{ pathname: baseUrl + "app/UpgradeValidation" }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="glyphicon glyphicon-dashboard" />
                <div>Changements en instance</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )}

        {/* {showMenuCashInEDPAgent && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{ pathname: baseUrl + "app/CashIn" }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="icon-arrow-right-circle" />
                <div>{"menu.cash.in"}</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )} */}

        {showMenuCashOutEDPAgent && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{ pathname: baseUrl + "app/CashOut" }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="icon-arrow-left-circle" />
                <div>{t("menu.cash.out")}</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )}

        {showMenuPaiementFactureEDPAgent && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{
                  pathname: baseUrl + "app/PaiementFacture/creanciersList",
                }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="icon-arrow-left-circle" />
                <div>{t("menu.paiement.facture")}</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )}

        {showMenuUpgrade && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{ pathname: baseUrl + "app/UpgradePack" }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="icon-equalizer" />
                <div>{t("menu.upgrade.title")}</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )}

        {showMenuUpdateClient && (
          <div>
            <div className={styles.vMenuIcon}>
              <Link
                to={{ pathname: baseUrl + "app/UpdateClient" }}
                onClick={() => {
                  this.closeMenu();
                }}
              >
                <i className="icon-user" />
                <div>{t("menu.client.title")}</div>
              </Link>
            </div>
            <MenuItem divider />
          </div>
        )}
      </nav>
    );
    return menu;
  }
}

VerticalMenu = connect((state) => ({
  user: state.user,
  userFrontDetails: state.user.userFrontDetails,
}))(VerticalMenu);

export default withNamespaces(["menu"])(VerticalMenu);
