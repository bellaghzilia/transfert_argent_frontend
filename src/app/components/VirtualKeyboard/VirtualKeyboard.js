import React, { Component } from 'react';
import { Button, FormControl, Panel, OverlayTrigger, Popover, Form } from 'react-bootstrap';

export default class VirtualKeyboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyPadValues: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
      password: '',
    };
    this.generatekeyPadValues = this.generatekeyPadValues.bind(this);
    this.handelDelete = this.handelDelete.bind(this);
    this.handelClick = this.handelClick.bind(this);
    this.initialize = this.initialize.bind(this);
    // this.handel = this.handel.bind(this);
  }
  generatekeyPadValues() {
    const keyPadValues = this.state.keyPadValues;
    let j;
    let x;
    for (let i = keyPadValues.length; i; i--) {
      j = Math.floor(Math.random() * i);
      x = keyPadValues[i - 1];
      keyPadValues[i - 1] = keyPadValues[j];
      keyPadValues[j] = x;
    }
    this.setState({ keyPadValues });
  }
  handelClick(key) {
    this.setState({ password: this.state.password + '' + key });
    console.log('handelclick: this.state.password' + this.state.password);
  }
  handelDelete() {
    if (this.state.password.length) {
      this.setState({ password: this.state.password.slice(0, this.state.password.length - 1) });
      console.log(this.state.password);
    }
  }
  initialize() {
    this.setState({ password: '' });
    console.log(this.state.password);
  }
  // handel() {
  //   // const lang = this.refs.dropdown.value;
  //   this.props.mappingPass(this.state.password);
  // }

  render() {
    const style = require('./styleKeyPad.scss');
    const styles = require('./Keyboard.scss');
    const placement = this.props.placement;
    const mappingPass = this.props.mappingPass;
    console.log('this.state.password');
    console.log(this.state.password);
    const handel = () => {
      mappingPass(this.state.password);
    };
    // const { ...rest} = this.props;
    // console.log(rest);
    const Keyboard = (
            <Popover >

                <Panel className={styles.PanelKey} collapsible expanded="true" >
                    <div className={style.keypad} onClick={handel}>
                    {
                      this.state.keyPadValues.map((key) =>
                        <span key={key} onClick={() => { this.handelClick(key); }} className={key}>{key}</span>
                      )}
                    <span onClick={() => { this.handelDelete(); }} className="glyphicon glyphicon-arrow-left" />
                    <span onClick={() => { this.initialize(); }} className="glyphicon glyphicon-refresh" />

                    <br />
                    </div>
                 </Panel>
                 </Popover>);

    return (

      <div>

            <FormControl value={this.state.password} type="password" className={styles.formControle} />

            <OverlayTrigger trigger="click" placement={placement} overlay={Keyboard} >
            <Button
              onClick={() => { this.generatekeyPadValues(); }}
              className={styles.ButtonPasswordStyle}
            >
            <i className="fa fa-keyboard-o" aria-hidden="true" />
            </Button>
            </OverlayTrigger>
          </div>
          );
  }
}
