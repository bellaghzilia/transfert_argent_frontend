import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
export default class CustomDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: '',
      tab: this.props.weekend,
      tabp: this.props.jourfiries
    };
    this.onFocusChange = this.onFocusChange.bind(this);
    this.isWeekday = this.isWeekday.bind(this);
  }

  onFocusChange(date) {
    this.setState({
      startDate: date
    });
  }
  isWeekday(date) {
    const day = date.day();
    // console.log('day');
    // console.log((new Date(date).toLocaleString().split(' ')[0]).split('/').join('-'));
    if (this.state.tab && this.state.tab.length) {
      for (let i = 0; i < this.state.tab.length; i++) {
        if (day === this.state.tab[i]) {
          return false;
        }
      }
    }
    if (this.state.tabp && this.state.tabp.length) {
      for (let i = 0; i < this.state.tabp.length; i++) {
        const d = (new Date(date).toLocaleString().split(' ')[0]).split('/').join('-');
        if (d === this.state.tabp[i]) {
          return false;
        }
      }
    }
    return true;
  }

  render() {
    const { value, onChange, maxDate, minDate, exludeDates, filterDate, Text, ...props } = this.props;
    return (
       <DatePicker
         minDate={minDate}
         maxDate={moment(maxDate)}
         filterDate={this.isWeekday}
         placeholderText={Text}
         dateFormat="DD/MM/YYYY"
         selected={this.state.startDate}
         onChange={this.onFocusChange} {...props}
         className="form-control"
       />
   );
  }

}

