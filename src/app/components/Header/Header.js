import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logout } from "../../../redux/modules/auth";
import { push } from "react-router-redux";
import { withNamespaces } from "react-i18next";

import {
  Navbar,
  Nav,
  NavItem,
  NavDropdown,
  MenuItem,
  Row,
  Col,
  Modal,
  Button,
} from "react-bootstrap/lib";
import { translate } from "react-i18next";
import * as UserActions from "../../containers/User/UserReducer";
// import i18n from "../../i18n";
import IdleTimer from "react-idle-timer";

class Header extends Component {
  static propTypes = {
    user: PropTypes.object,
    logout: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired,
  };
  static contextTypes = {
    store: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      showMenu: false,
    };
  }

  onIdleAction() {
    console.log("Timeout");
    window.location.assign(baseUrl + "login/logout");
  }

  handleLogout = (event) => {
    event.preventDefault();
    this.props.logout();
    sessionStorage.removeItem("myData");
    sessionStorage.removeItem("myData1");
    sessionStorage.removeItem("myData2");
    sessionStorage.clear();
  };

  viderCache = (event) => {
    event.preventDefault();
    this.props.logout();
    if (
      window.location.pathname.indexOf(baseUrl + "app/PaiementFacture") !== -1
    ) {
      window.location.replace(baseUrl + "app/PaiementFacture/creanciersList");
    } else {
      sessionStorage.removeItem("myData");
      sessionStorage.removeItem("myData1");
      sessionStorage.removeItem("myData2");
      window.location.reload();
      sessionStorage.clear();
    }
  };

  showMenu = () => {
    this.setState({
      showMenu: !this.state.showMenu,
      checkedlang: null,
    });
  };

  handleUpdateLangue = async () => {
    if (this.state.checkedlang) {
      this.setState({ showMenu: false });
      await this.props.updateLangue(this.state.checkedlang);
      window.location.reload();
      i18n.changeLanguage(this.state.checkedlang);
    }
  };

  handleLangue = (e) => {
    this.setState({ checkedlang: e.target.value });
  };

  render() {
    const { t, user, hideMenu } = this.props;
    console.log("state status  :" + { ...this.props });
    const styles = require("./Header.scss");
    console.log({ ...this.props });

    const handleClickMenu = () => {
      hideMenu();
    };
    const closeMenu = () => {
      this.setState({ showMenu: false });
    };
    const logoutUrl = baseUrl + "login/logout";
    const PasswordChange = baseUrl + "app/PasswordChange";
    // const showSoldeAgent = user.roles.indexOf("ROLE_EDP_CASH_IN") !== -1;
    const showSoldeAgent = true;
    // const showMonCompte = user.roles.indexOf("ROLE_EDP_MON_COMPTE") !== -1;
    const showMonCompte = true;

    // const dir = user.defaultLocale === "ar" ? "rtl" : "ltr";
    const dir = true ? "rtl" : "ltr";

    const MonCompte = baseUrl + "app/MonCompte";
    return (
      <div className={styles.appHeader}>
        <Navbar fixedTop fluid>
          <Col md={3} sm={3} xs={2}>
            <Navbar.Header>
              <Navbar.Brand />
            </Navbar.Header>
          </Col>

          <Col md={9} sm={9} xs={10}>
            <Nav pullRight dir={dir}>
              <NavItem
                onClick={this.viderCache}
                eventKey={6}
                href={logoutUrl}
                className={styles.IcoAff + " " + styles.logOut}
              >
                <span className={"icon-login"} />
              </NavItem>

              <NavDropdown
                id="user-dropdown"
                className={styles.userNav + " user-nav"}
                title={
                  <span>
                    <i className={styles.icoUser + " icon-user"} />
                    <span className="hidden-xs">{user.fullName}</span>{" "}
                    {/* <span className="hidden-xs">fullName</span>{" "} */}
                    <i className={styles.icoArrow + " icon-arrow-down"} />
                  </span>
                }
              >
                <MenuItem divider />
                {showMonCompte && (
                  <MenuItem
                    onClick={this.viderCache}
                    href={MonCompte}
                    className={styles.userNavItem}
                  >
                    <p style={{ lineHeight: "43px" }}>
                      {t("header.monCompte")}
                      {/* {"header.monCompte"} */}
                    </p>
                  </MenuItem>
                )}
                <MenuItem divider />

                <MenuItem
                  to="compte"
                  eventKey={5.1}
                  className={styles.userNavItem}
                >
                  <sup>
                    <small> {t("header.derniereConnexion")}</small>
                    {/* <small> {"header.monCompte"} </small> */}
                  </sup>
                  <p>{user.dataDerniereconnexion}</p>
                  {/* <p>{"user.dataDerniereconnexion"}</p> */}
                </MenuItem>

                <MenuItem divider />

                <MenuItem
                  onClick={this.viderCache}
                  href={PasswordChange}
                  className={styles.userNavItem}
                >
                  <sup>
                    <small>{t("header.gestionCompte")}</small>
                    {/* <small>{"header.gestionCompte"}</small> */}
                  </sup>
                  <p> {t("header.changerMotdepasse")}</p>
                  {/* <p> {"header.changerMotdepasse"}</p> */}
                </MenuItem>

                <MenuItem divider />

                <MenuItem
                  onClick={this.showMenu}
                  className={styles.userNavItem}
                >
                  <p style={{ lineHeight: "43px" }}>{t("header.langues")}</p>
                  {/* <p style={{ lineHeight: "43px" }}>{"header.langues"}</p> */}
                </MenuItem>

                <MenuItem divider />

                <MenuItem
                  onClick={this.viderCache}
                  href={logoutUrl}
                  className={styles.userNavItem}
                >
                  <p style={{ lineHeight: "43px" }}>
                    {" "}
                    {t("header.deconnexion")}
                    {/* {"header.deconnexion"} */}
                  </p>
                </MenuItem>
              </NavDropdown>
              {showSoldeAgent && (
                <NavItem refresh="true" onClick={this.viderCache} eventKey={6}>
                  <span style={{ marginRight: "7px" }}>
                    <i className="glyphicon glyphicon-refresh" />
                  </span>{" "}
                  {t("header.solde")} : {user.currency}{" "}
                  {/* {"header.solde"} :{"CARRUNCY"}{" "} */}
                </NavItem>
              )}
              {showSoldeAgent && (
                <NavItem refresh="true" onClick={this.viderCache} eventKey={6}>
                  {t("header.commition")} : {user.commission}{" "}
                  {/* {"header.commition"} : {"COMMISSION"}{" "} */}
                </NavItem>
              )}
            </Nav>
          </Col>
        </Navbar>

        <Modal
          show={this.state.showMenu}
          onHide={closeMenu}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              <div>{t("header.modal.title")}</div>
              {/* <div>{"header.modal.title"}</div> */}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <Row>
                <Col xs={12} md={12}>
                  <input
                    type="radio"
                    name="langues"
                    value="ar"
                    onClick={this.handleLangue}
                  />{" "}
                  {t("header.modal.arabe")}
                  {/* {"header.modal.arabe"} */}
                </Col>
                <Col xs={12} md={12}>
                  <input
                    type="radio"
                    name="langues"
                    value="fr"
                    onClick={this.handleLangue}
                  />{" "}
                  {t("header.modal.frensh")}
                  {/* {"header.modal.frensh"} */}
                </Col>
              </Row>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={() => this.handleUpdateLangue()}>
              {t("header.modal.btnOk")}
              {/* {"header.modal.btnOk"} */}
            </Button>
          </Modal.Footer>
        </Modal>

        <IdleTimer
          ref="idleTimer"
          element={document}
          // timeout={user.timeOut}
          timeout={1000000}
          idleAction={this.onIdleAction}
          format="MM-DD-YYYY HH:MM:ss.SSS"
        ></IdleTimer>
      </div>
    );
  }
}
Header = connect(
  (state) => ({
    user: state.user.userFrontDetails,
  }),
  { logout, pushState: push, ...UserActions }
)(Header);
// Export the UI
export default withNamespaces(["header"])(Header);
