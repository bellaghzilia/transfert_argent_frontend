import React, { Component } from "react";
import PropTypes from "prop-types";

import { browserHistory, IndexLink } from "react-router";
import { LinkContainer } from "react-router-bootstrap";
// import { logout } from "redux/modules/auth";
import { push } from "react-router-redux";
import {
  Navbar,
  Nav,
  NavItem,
  NavDropdown,
  MenuItem,
  Grid,
  Row,
  Col,
} from "react-bootstrap/lib";
import FontAwesome from "react-fontawesome";

export default class MenuOffline extends Component {
  render() {
    const styles = require("./Header.scss");
    return (
      <Nav className="pull-right">
        <LinkContainer to={"#"}>
          <NavItem eventKey={1}>Acceuil</NavItem>
        </LinkContainer>
        <LinkContainer to={"#"}>
          <NavItem eventKey={2}>Services</NavItem>
        </LinkContainer>
        <LinkContainer to={"#"}>
          <NavItem eventKey={3}>FAQ</NavItem>
        </LinkContainer>
      </Nav>
    );
  }
}
