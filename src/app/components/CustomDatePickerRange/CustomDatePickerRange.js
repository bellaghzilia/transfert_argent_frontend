import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
export default class CustomDatePickerRange extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: '',
      endDate: ''
    };
    this.handleChangeStart = this.handleChangeStart.bind(this);
    this.handleChangeEnd = this.handleChangeEnd.bind(this);
  }

  handleChangeStart(date) {
    this.setState({ startDate: date });
  }
  handleChangeEnd(date) {
    this.setState({ endDate: date });
  }
  render() {
    const { value, onChange, dateDebutText, dateFinText } = this.props;
    console.log('this.props');
    console.log(this.props);
    return (
    <div>
       <DatePicker
      // minDate={moment(minDateProfondeur)}
     //  maxDate={moment(maxDateProfondeur)}
         selected={this.state.startDate}
         selectsStart startDate={this.state.startDate}
         endDate={this.state.endDate}
         onChange={this.handleChangeStart}
         placeholderText={dateDebutText}
         className="form-control"
       />
        <DatePicker
                    // minDate={moment(minDateProfondeur)}
                    // maxDate={moment(maxDateProfondeur)}
          selected={this.state.endDate}
          selectsEnd startDate={this.state.startDate}
          endDate={this.state.endDate}
          onChange={this.handleChangeEnd}
          placeholderText={dateFinText}
          className="form-control"
        />
    </div>
    );
  }
}

