// // Types
// import * as TYPE from "../types/loginTypes";

// function objectToParams(object) {
//   let str = "";
//   for (const key in object) {
//     if (str !== "") {
//       str += "&";
//     }
//     str += key + "=" + encodeURIComponent(object[key]);
//   }
//   return str;
// }

// // Actions
// export const init = () => {
//   return {
//     type: TYPE.INIT,
//   };
// };

// export const initCodeBanque = (cb) => {
//   return {
//     type: TYPE.INIT_CODE_BANQUE,
//     cb,
//   };
// };

// export const initCodeLangue = (cl) => {
//   return {
//     type: TYPE.INIT_CODE_LANGUE,
//     cl,
//   };
// };

// export const stop = () => {
//   return {
//     type: TYPE.STOP,
//   };
// };
// export const signOut = () => {
//   return {
//     types: [TYPE.SIGNOUT],
//     promise: (client) => client.post("auth/logout"),
//   };
// };
// export const resetProps = () => {
//   return {
//     type: TYPE.RESET_PROPS,
//   };
// };

// export const getCsrfToken = () => {
//   const privateHash =
//     "ac64a95e29010f2bb94230af205e4a09845836c86fb8fe4ce7d1bb094b6a7eb25cd941cd233213dab6a8219a402cf52bbe4542807823947d4ecd4b689644b86e";
//   return {
//     types: [
//       TYPE.LOAD_CSRF_TOKEN,
//       TYPE.LOAD_CSRF_TOKEN_SUCCESS,
//       TYPE.LOAD_CSRF_TOKEN_FAIL,
//     ],
//     promise: (client) =>
//       client.get("client/entreEnRelationOffline?hash=" + privateHash),
//   };
// };

// export const login = /* istanbul ignore next */ () => {
//   console.log("Login call begin ");

//   const userAuth = {
//     j_username: "Agent",
//     j_password: "123456",
//     codeBanque: "00000",
//     lang: "fr",
//     codePays: "MA",
//   };

//   console.log("login reducer call : ");
//   return {
//     types: [TYPE.LOAD_LOGIN, TYPE.LOAD_LOGIN_SUCCESS, TYPE.LOAD_LOGIN_FAIL],
//     promise: (client) =>
//       client.post("login/mobile", { data: objectToParams(userAuth) }),
//   };
//   // }
// };

// // export const loginAuth = /* istanbul ignore next */ (values) => {
// //   console.log("LoginAuth call begin ");

// //   const userAuth = {
// //     j_username: values.username,
// //     j_username: values.password,
// //     codeBanque: values.codeBanque,
// //     lang: values.langue,
// //     codePays: "MA",
// //   };

// //   console.log("LoginAuth .... ");

// //   console.log(
// //     "form values: " +
// //       values.username +
// //       " " +
// //       values.password +
// //       " " +
// //       values.codeBanque +
// //       " " +
// //       values.langue +
// //       "" +
// //       userAuth.codePays +
// //       " end."
// //   );

// //   console.log("login reducer call : ");
// //   // let user = localStorage.get("userDetails");
// //   // if (user) {
// //   //   return { type: TYPE.ALREADY_CONNECTED };
// //   // } else {
// //   return {
// //     types: [TYPE.LOAD_LOGIN, TYPE.LOAD_LOGIN_SUCCESS, TYPE.LOAD_LOGIN_FAIL],
// //     promise: (client) => client.post("login/mobile/auth", { data: {} }),
// //   };
// //   // }
// // };

// export const getStatusLogin = /* istanbul ignore next */ (values) => {
//   const userAuth = {
//     username: values.username,
//     password: values.password,
//     codeBanque: values.codeBanque,
//   };
//   return {
//     types: [
//       TYPE.GET_STATUS_LOGIN,
//       TYPE.GET_STATUS_LOGIN_SUCCESS,
//       TYPE.GET_STATUS_LOGIN_FAIL,
//     ],
//     promise: (client) =>
//       client.post("contrats/auth/getStatusLogin", { data: userAuth }),
//   };
// };

// export const loginHistory = () => {
//   return {
//     types: [
//       TYPE.LOAD_LOGIN_HISTORY,
//       TYPE.LOAD_LOGIN_HISTORY_SUCCESS,
//       TYPE.LOAD_LOGIN_HISTORY_FAIL,
//     ],
//     promise: (client) =>
//       client.post(
//         "administration/auditLogEvent/listObjectAfter?format=json&_dc=1558959032304&page=1&offset=0&max=10"
//       ),
//   };
// };

// export const LastLogin = () => {
//   return {
//     types: [
//       TYPE.LAST_LOGIN_HISTORY,
//       TYPE.LAST_LOGIN_HISTORY_SUCCESS,
//       TYPE.LAST_LOGIN_HISTORY_FAIL,
//     ],
//     promise: (client) =>
//       client.post(
//         "administration/auditLogEvent/listObjectDate?format=json&_dc=1558959032243"
//       ),
//   };
// };

// export const keymap = () => {
//   return {
//     types: [TYPE.LOAD_KEYMAP, TYPE.LOAD_KEYMAP_SUCCESS, TYPE.LOAD_KEYMAP_FAIL],
//     promise: (client) => client.post("administration/parametrage/keymap"),
//   };
// };
