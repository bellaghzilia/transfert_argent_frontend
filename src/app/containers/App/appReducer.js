const SHOW_MENU = "header/SHOW_MENU";

const InitialState = {
  loaded: false,
  isMenuHidden: false,
};
export default function reducer(state = InitialState, action = {}) {
  switch (action.type) {
    // SHOW_MENU
    case SHOW_MENU:
      console.log("isMenuHidden");
      console.log(state.isMenuHidden);
      return {
        ...state,
        isMenuHidden: !state.isMenuHidden,
      };
    default:
      return state;
  }
}

export function hideMenu(id) {
  return { type: SHOW_MENU };
}
