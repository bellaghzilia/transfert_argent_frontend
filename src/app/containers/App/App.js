import React, { Component } from "react";
import PropTypes from "prop-types";

import { Helmet } from "react-helmet";
import { asyncConnect } from "redux-connect";

import config from "../../config";
import { LoadingBar } from "react-redux-loading-bar";
import Header from "../../components/Header/Header";

//ui
import HeaderOffline from "../../components/Header/HeaderOffline";
import MenuDC from "../../components/Menu/MenuDC";
import VerticalMenu from "../../components/Menu/VerticalMenu";
import VerticalMenuDC from "../../components/Menu/VerticalMenuDC";
import Menu from "../../components/Menu/Menu";

// import axios from "axios";

import { connect } from "react-redux";
import { push } from "react-router-redux";
import {
  logout,
  isLoaded as isAuthLoaded,
  load as loadAuth,
} from "../../../redux/modules/auth";

import * as UserActions from "../User/UserReducer";
import * as loginActions from "../../actions/loginActions";

import * as AppAction from "./appReducer";

@asyncConnect([
  {
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];
      if (!UserActions.isUserFrontDetailsLoaded(getState())) {
        promises.push(dispatch(UserActions.loadUserFrontDetails()));
      }
      return Promise.all(promises);
    },
  },
])
@connect(
  (state) => ({
    //	  user: state.auth.user,
    userLoadSuccess: state.user.userLoadSuccess,
    userFrontDetails: state.user.userFrontDetails,
    isMenuHidden: state.app.isMenuHidden,
  }),
  { logout, pushState: push, ...AppAction, ...UserActions }
)
export default class App extends Component {
  constructor(props) {
    super(props);
    this.callWS = this.callWS.bind(this);
  }

  async callWS() {
    await this.props.loadUserFrontDetails();
  }

  static propTypes = {
    children: PropTypes.object.isRequired,
    //user: PropTypes.object,
    userFrontDetails: PropTypes.object,
    logout: PropTypes.func.isRequired,
    hideMenu: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired,
  };

  static contextTypes = {
    store: PropTypes.object.isRequired,
  };

  componentWillReceiveProps(nextProps) {
    if (!this.props.userFrontDetails && nextProps.userFrontDetails) {
      // login
      this.props.pushState("/loginSuccess");
    } else if (this.props.userFrontDetails && !nextProps.userFrontDetails) {
      // logout
      this.props.pushState("/compte");
    }
  }

  render() {
    const styles = require("./App.scss");

    const { userLoadSuccess, isMenuHidden, hideMenu } = this.props;

    return (
      <div className={styles.app}>
        <Helmet {...config.app.head} />
        <LoadingBar
          className="loading-bar"
          updateTime={100}
          maxProgress={80}
          progressIncrease={10}
        />
        {userLoadSuccess ? <Header hideMenu={hideMenu} /> : <HeaderOffline />}
        {userLoadSuccess && userLoadSuccess !== null && bankCode === "00000" ? (
          <MenuDC className="mobile" />
        ) : (
          <Menu className="mobile" />
        )}
        {userLoadSuccess && userLoadSuccess !== null && bankCode === "00013" ? (
          <VerticalMenu />
        ) : (
          <VerticalMenuDC />
        )}
        <div className="appContent"> {this.props.children} </div>
      </div>
    );
  }
}
