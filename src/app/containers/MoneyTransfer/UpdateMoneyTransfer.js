import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  InputGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import moment from "moment";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./moneytransferreducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import Button from "react-bootstrap-button-loader";
import * as ParametrageAction from "./ParametrageReducer";
import * as UserActions from "../User/UserReducer";

var TBD = sessionStorage.getItem("myData4");

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.getInstance(params.id, location)),
        dispatch(ParametrageAction.loadListVilles()),
      ]);
    },
  },
])
@connect(
  (state) => ({
    dataForDetail: state.moneyTransfer.dataForDetail,
    step: state.moneyTransfer.step,
    fraienvois: state.moneyTransfer.fraienvois,
    successfraisenvois: state.moneyTransfer.successfraisenvois,
    updateSuccess: state.moneyTransfer.updateSuccess,
    id: state.moneyTransfer.saveSuccessObject.id,
    loadingupdate: state.moneyTransfer.loadingupdate,
    saveSuccess: state.moneyTransfer.saveSuccessObject.saveSuccess,
    loadingSigneTransfert: state.moneyTransfer.loadingSigneTransfert,
    successSigneTransfert: state.moneyTransfer.successSigneTransfert,
    lisVilles: state.ParametrageReducer.lisVilles,
    userFrontDetails: state.user.userFrontDetails,
    message: state.moneyTransfer.message,
    TYPETB: state.moneyTransfer.TYPETB,
    successVirifierSolde: state.moneyTransfer.successVirifierSolde,
    soldegarantie: state.moneyTransfer.soldegarantie,
  }),
  {
    ...MoneyTransferAction,
    ...UserActions,
    ...ParametrageAction,
    initializeWithKey,
  }
)
@reduxForm(
  {
    form: "NewMoneyTransfer",
    fields: [
      "numtelemetteur",
      "numeroPieceIdentiteemeteur",
      "nomemetteur",
      "prenomemeteur",
      "emailemetteur",
      "villeemetteur",
      "numerotelebeneficiare",
      "numeroPieceIdentitebeneficiare",
      "nombeneficiare",
      "prenombeneficiare",
      "emailbeneficiare",
      "regionbeneficiare",
      "paysindicatifbeneficiare",
      "paysbeneficiare",
      ,
      "montanttransfere",
      "fraiinclus",
      "montantremise",
      "motiftransfert",
      "fraisenvois",
      "motif",
    ],

    destroyOnUnmount: true,
  },
  (state) =>
    state.moneyTransfer.dataForDetail &&
    state.moneyTransfer.dataForDetail !== null &&
    state.moneyTransfer.dataForDetail !== undefined && {
      initialValues: state.moneyTransfer.dataForDetail,
    }
)
@translate(["MoneyTransfer"], { wait: true })
export default class UpdateMoneyTransfer extends Component {
  static propTypes = {
    getInstance: PropTypes.func,
    createAvisTransfertPDF: PropTypes.func,
    signeTransfertCltCltByID: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      selectville: false,
      successmontantremise: true,
      pays: "",
      indicatifpays: "",
      montantTransfertError: false,
      montantRemiseError: false,
      numtelemetteurError: false,
      numtelbeneficiareError: false,
      statutModifier: false,
      regionNom: "",
      checked: props.dataForDetail.fraiinclus,
      villeBen: props.dataForDetail.idvillebeneficiare,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.signeTransfert = this.signeTransfert.bind(this);
    this.redirectToHomePage = this.redirectToHomePage.bind(this);
  }

  async onSubmit(id, values, idVille) {
    // Here, we use an async/await approach that allows our code
    // to look synchronous which is always a plus. The code looks clean!
    try {
      await this.props.UpdateTransfert(id, values, idVille);
      //  if(this.props.updateSuccess == true) {
      //  this.props.switchf();
      //  }
    } catch (error) {
      console.log(error.message);
    }
  }

  async signeTransfert(id) {
    this.setState({ showModal: false });
    await this.props.signeTransfertCltCltByID(id);
    this.props.getInstanceTransfert(id);
    setTimeout(() => {
      this.props.resetAlertSoldeInsufisante();
    }, 6000);
  }

  redirectToHomePage(TBD) {
    if (TBD == "TB_AGENT") {
      browserHistory.push(baseUrl + "app/TDBAgentCaisse");
    } else if (TBD == "TB_PARTENAIRE") {
      browserHistory.push(baseUrl + "app/TabeBordPartenaire");
    }
  }

  render() {
    const {
      fields: {
        numtelemetteur,
        numeroPieceIdentiteemeteur,
        nomemetteur,
        prenomemeteur,
        emailemetteur,
        villeemetteur,
        numerotelebeneficiare,
        regionbeneficiare,
        paysindicatifbeneficiare,
        paysbeneficiare,
        numeroPieceIdentitebeneficiare,
        nombeneficiare,
        prenombeneficiare,
        emailbeneficiare,
        montanttransfere,
        fraisenvois,
        montantremise,
        motif,
      },
      TYPETB,
      soldegarantie,
      fraienvoi,
      fraiinclus,
      message,
      validatepwdError,
      handleSubmit,
      fraienvois,
      telDetails,
      telbenDetails,
      clientDetails,
      clientbenDetails,
      successtel,
      successtelben,
      successclient,
      successclientbene,
      successfraisenvois,
      successmontantremise,
      selectville,
      values,
      submitting,
      listBeneficiary,
      listDebitAccount,
      step,
      setState,
      setStateGAB,
      validerSTEP,
      UpdateTransfert,
      saveexisteclient,
      getInstance,
      getInstanceTransfert,
      createAvisTransfertPDF,
      loadfraisenvois,
      loadmontantremise,
      loadListTelClient,
      loadListTelClientben,
      loadlClient,
      loadlClientbeneficiare,
      saveSuccessObject,
      saveSuccess,
      updateSuccess,
      moneyTransferObject,
      statut,
      signerDemande,
      codeErreur,
      maxdate,
      setStatut,
      jourfiries,
      weekend,
      toStepX,
      successSigneTransfert,
      dataForDetail,
      loadingupdate,
      abandonnerDemande,
      instance,
      otpSMS,
      taskid,
      user,
      listBeneficiaryGAB,
      Request,
      initpwd,
      saveUrl,
      updateUrl,
      setTransfertType,
      isInstance,
      loadingSigneTransfert,
      switchf,
      typeOfTransfer,
      listeServiceTransfert,
      resetForm,
      params,
      t,
      signRedirect,
      signUrlred,
      taskId,
      lisVilles,
      userFrontDetails,
      loadClient,
      loadClients,
      successVirifierSolde,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const close = () => {
      this.setState({ showModal: false });
    };

    return (
      <div>
        <br />
        <Row className={styles.compteCard}>
          <Wizard>{step}</Wizard>

          {userFrontDetails.caisseId === "" && (
            <Alert bsStyle="danger">{t("msg.aucunecaisseouverte")}</Alert>
          )}
          {updateSuccess === true && (
            <Alert bsStyle="info">{t("msg.sucessmodification")}</Alert>
          )}

          {updateSuccess === "false" && (
            <Alert bsStyle="danger">{t("msg.unsucessmodification")}</Alert>
          )}

          {successSigneTransfert === true && (
            <Alert bsStyle="info">{t("msg.sucesssignature")}</Alert>
          )}

          {successVirifierSolde === false && (
            <Alert bsStyle="danger">Solde insuffisant dans cette caisse</Alert>
          )}

          {successmontantremise && montantremise.max !== null && (
            <Alert bsStyle="danger">{t("msg.saisirmontantinf")}</Alert>
          )}
          {successSigneTransfert === false && message !== "msg" && (
            <Alert bsStyle="danger">
              <p>{message}</p>
            </Alert>
          )}
          {soldegarantie === false && (
            <Alert bsStyle="danger">{t("msg.soldegarantieinsuffisant")}</Alert>
          )}

          <form onSubmit={handleSubmit} className="formContainer">
            <fieldset>
              <legend>{t("form.legend.infosemeteur")}</legend>
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>
                    {t("form.label.numidentiemeteur")}:
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...numeroPieceIdentiteemeteur}
                  />
                </Col>

                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.numtelemeteur")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...numtelemetteur}
                    onBlur={() => {
                      if (
                        values.numtelemetteur == undefined ||
                        values.numtelemetteur == ""
                      ) {
                        this.setState({ numtelemetteurError: true });
                      } else {
                        loadClient(values.numtelemetteur);
                      }
                    }}
                  />
                  {this.state.numtelemetteurError && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {numtelemetteur.error}
                      </i>
                    </div>
                  )}
                </Col>
              </Row>
              <Row className={styles.fieldRow}>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.nom")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...nomemetteur}
                  />
                  {nomemetteur.error && nomemetteur.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {nomemetteur.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.prenom")} :</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...prenomemeteur}
                  />
                  {prenomemeteur.error && prenomemeteur.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {prenomemeteur.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                  <InputGroup>
                    <InputGroup.Addon>
                      <i className="fa fa-at" />
                    </InputGroup.Addon>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...emailemetteur}
                    />
                  </InputGroup>
                </Col>
              </Row>

              <Row className={styles.fieldRow}>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.ville")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...villeemetteur}
                    value={userFrontDetails.villeNom}
                  />
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.region")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    value={userFrontDetails.regionNom}
                  />
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.ville")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    value={userFrontDetails.paysNom}
                  />
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.indicatif")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    value={userFrontDetails.paysIndicatif}
                  />
                </Col>
              </Row>
            </fieldset>

            <fieldset style={{ marginTop: "25px" }}>
              <legend> {t("form.legend.infosbeneficiare")}</legend>
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>
                    {t("form.label.numidentibeneficiare")}:
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...numeroPieceIdentitebeneficiare}
                  />
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>
                    {t("form.label.numidtelbeneficiare")}:
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...numerotelebeneficiare}
                    onBlur={() => {
                      if (
                        (values.numerotelebeneficiare == undefined ||
                          values.numerotelebeneficiare == "") &&
                        (values.numtelemetteur == undefined ||
                          values.numtelemetteur == "")
                      ) {
                        this.setState({ numtelemetteurError: true });
                        this.setState({ numtelbeneficiareError: true });
                      } else {
                        loadClients(
                          values.numtelemetteur,
                          values.numerotelebeneficiare
                        );
                      }
                    }}
                  />
                  {this.state.numtelbeneficiareError && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {numerotelebeneficiare.error}
                      </i>
                    </div>
                  )}
                </Col>
              </Row>

              <Row className={styles.fieldRow}>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.nom")} :</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...nombeneficiare}
                  />
                  {nombeneficiare.error && nombeneficiare.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {nombeneficiare.error}
                      </i>
                    </div>
                  )}
                </Col>

                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.prenom")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...prenombeneficiare}
                  />
                  {prenombeneficiare.error && prenombeneficiare.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {prenombeneficiare.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...emailbeneficiare}
                  />
                </Col>
              </Row>

              {this.state.selectville === true ? (
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.ville")}</ControlLabel>
                    <FormControl
                      componentClass="select"
                      className={styles.datePickerFormControl}
                      placeholder="select"
                      onChange={(e) => {
                        for (var i = 0; i < lisVilles.length; i++) {
                          if (e.target.value == lisVilles[i].id) {
                            this.setState({ pays: lisVilles[i].libellepays });
                            this.setState({ selectville: true });
                            this.setState({
                              indicatifpays: lisVilles[i].indicatifpays,
                            });
                            this.setState({ villeBen: lisVilles[i].id });
                            this.setState({
                              regionNom: lisVilles[i].nomRegion,
                            });
                            break;
                          }
                        }
                      }}
                    >
                      <option value="" hidden>
                        {t("form.hidden.selectionnerville")}
                      </option>
                      {lisVilles &&
                        lisVilles.length !== 0 &&
                        lisVilles.map((ville) => (
                          <option
                            value={ville.id}
                            selected={
                              dataForDetail.idvillebeneficiare ===
                              ville.id.toString()
                            }
                          >
                            {" "}
                            {ville.libelle}{" "}
                          </option>
                        ))}
                    </FormControl>
                  </Col>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.region")}:</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      value={this.state.regionNom}
                    />
                  </Col>

                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      value={this.state.pays}
                    />
                  </Col>

                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.indicatif")}:</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      value={this.state.indicatifpays}
                    />
                  </Col>
                </Row>
              ) : (
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.ville")}</ControlLabel>
                    <FormControl
                      componentClass="select"
                      className={styles.datePickerFormControl}
                      placeholder="select"
                      onChange={(e) => {
                        for (var i = 0; i < lisVilles.length; i++) {
                          if (e.target.value == lisVilles[i].id) {
                            this.setState({ pays: lisVilles[i].libellepays });
                            this.setState({ selectville: true });
                            this.setState({
                              indicatifpays: lisVilles[i].indicatifpays,
                            });
                            this.setState({ villeBen: lisVilles[i].id });
                            this.setState({
                              regionNom: lisVilles[i].nomRegion,
                            });
                            break;
                          }
                        }
                      }}
                    >
                      <option value="" hidden>
                        {t("form.hidden.selectionnerville")}
                      </option>
                      {lisVilles &&
                        lisVilles.length !== 0 &&
                        lisVilles.map((ville) => (
                          <option
                            value={ville.id}
                            selected={
                              dataForDetail.idvillebeneficiare ===
                              ville.id.toString()
                            }
                          >
                            {" "}
                            {ville.libelle}{" "}
                          </option>
                        ))}
                    </FormControl>
                  </Col>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.region")}:</ControlLabel>
                    <FormControl
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...regionbeneficiare}
                    />
                  </Col>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                    <FormControl
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...paysbeneficiare}
                    />
                  </Col>

                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.indicatif")}:</ControlLabel>
                    <FormControl
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...paysindicatifbeneficiare}
                    />
                  </Col>
                </Row>
              )}
            </fieldset>

            <fieldset style={{ marginTop: "25px" }}>
              <legend>{t("form.legend.infostransfert")}</legend>
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>
                    {t("form.label.montanttransfert")}:
                  </ControlLabel>
                  <FormControl
                    {...montanttransfere}
                    type="number"
                    disabled={true}
                    className={styles.datePickerFormControl}
                    onBlur={(e) => {
                      e.preventDefault();
                      loadfraisenvois(values.montanttransfere);
                    }}
                  />
                </Col>

                {successfraisenvois ? (
                  <Col xs="12" md="4">
                    <ControlLabel>{t("form.label.fraienvoi")}:</ControlLabel>
                    {(values.fraisenvois = fraienvois.fraienvoi)}
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...fraisenvois}
                      value={fraienvois.fraienvoi}
                    />
                  </Col>
                ) : (
                  <Col xs="12" md="4">
                    <ControlLabel>{t("form.label.fraienvoi")}:</ControlLabel>
                    <FormControl
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...fraisenvois}
                    />
                  </Col>
                )}
                <Col xs="12" md="2">
                  <FormControl
                    type="checkbox"
                    checked={this.state.checked}
                    style={{ width: "35px", float: "left", marginTop: "24px" }}
                  />
                  <span style={{ padding: "30px 0 0 15px", float: "left" }}>
                    Frais inclus
                  </span>
                </Col>
              </Row>

              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.monatntremise")}:</ControlLabel>
                  <FormControl
                    type="number"
                    disabled={true}
                    className={styles.datePickerFormControl}
                    {...montantremise}
                  />
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.motiftransfert")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...motif}
                  />
                </Col>
              </Row>

              <div className="pull-right" style={{ marginTop: "15px" }}>
                {this.state.statutModifier === true ? (
                  <Button
                    loading={loadingSigneTransfert}
                    disabled={
                      dataForDetail.statutOperation === "signer" ||
                      userFrontDetails.caisseId === ""
                    }
                    onClick={() =>
                      this.setState({ showModal: true, statutModifier: false })
                    }
                    bsStyle="primary"
                  >
                    <i
                      className={
                        "fa " + (submitting ? "fa-cog fa-spin" : "fa fa-check")
                      }
                    />{" "}
                    {t("form.buttons.confirmersigner")}
                  </Button>
                ) : (
                  <Button
                    loading={loadingupdate}
                    disabled={
                      dataForDetail.statutOperation === "signer" ||
                      userFrontDetails.caisseId === ""
                    }
                    onClick={() => {
                      this.onSubmit(
                        dataForDetail.id,
                        values,
                        this.state.villeBen
                      );
                      this.setState({ statutModifier: true });
                    }}
                    bsStyle="primary"
                  >
                    <i
                      className={
                        "fa " + (submitting ? "fa-cog fa-spin" : "fa fa-check")
                      }
                    />{" "}
                    {t("form.buttons.confirmer")}
                  </Button>
                )}

                <Button
                  bsStyle="primary"
                  onClick={() =>
                    window.open(
                      baseUrl +
                        "transfertargentBNIF/createAvisTransfertPDF/" +
                        dataForDetail.id
                    )
                  }
                >
                  <i className="fa fa-file-pdf-o fa-4" />{" "}
                  {t("form.buttons.uploadpdf")}
                </Button>
                <Button
                  onClick={(e) => {
                    this.redirectToHomePage(this.props.TYPETB);
                  }}
                  bsStyle="primary"
                >
                  <i className="fa fa-reply" /> {t("form.buttons.retour")}
                </Button>
              </div>

              <Modal
                show={this.state.showModal}
                onHide={close}
                container={this}
                aria-labelledby="contained-modal-title"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="contained-modal-title">
                    <div>{t("popup.signature.title")}</div>
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div>{t("popup.signature.msg")}</div>
                </Modal.Body>
                <Modal.Footer>
                  <ButtonGroup className="pull-right" bsSize="small">
                    <Button
                      className={styles.ButtonPasswordStyle}
                      onClick={() => close()}
                    >
                      Non
                    </Button>
                    <Button
                      className={styles.ButtonPasswordStyle}
                      onClick={() => {
                        this.signeTransfert(dataForDetail.id);
                      }}
                    >
                      Oui
                    </Button>
                  </ButtonGroup>
                </Modal.Footer>
              </Modal>
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
