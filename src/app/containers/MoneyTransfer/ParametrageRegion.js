import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Button,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  PanelGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./ParametrageReducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import ParametrageRegionValidator from "./Validateur/ParametrageRegionValidator";
import { AuthorizedComponent } from "react-router-role-authorization";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

@connect(
  (state) => ({
    dataForDetailregion: state.ParametrageReducer.dataForDetailregion,
    successinstanceregion: state.ParametrageReducer.successinstanceregion,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getInstanregion: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
    showBouttonSupprimer: PropTypes.func,
    deleteTransfertCltCltByID: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    annulerTransfertCltCltByID: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      showModal: false,
      updating: false,
      deleting: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick(event) {
    try {
      if (this.state.deleting) {
        await this.props.deleteRegion(this.props.rowData.id);
        this.props.loadListRegion();
      } else if (this.state.updating) {
        await this.props.getInstanregion(this.props.rowData.id);
        //this.props.loadListRegion();
      }
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      saveRegion,
      lisCollaborateurs,
      loadListRegion,
      updateRegion,
      lisRegions,
      getInstanregion,
      deleteRegion,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
    };

    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: true,
                showModal: true,
                deleting: false,
              })
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>
          <Button
            bsSize="small"
            bsStyle="danger"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                deleting: true,
                showModal: true,
                updating: false,
              })
            }
          >
            <i className="fa fa-trash-o fa-fw" />
          </Button>
        </ButtonGroup>

        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {this.state.deleting && <div>{t("popup.supression.title")}</div>}
              {this.state.updating && (
                <div>{t("popup.modification.title")}</div>
              )}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.deleting && <div>{t("popup.supression.msgr")}</div>}
            {this.state.updating && <div>{t("popup.modification.msgr")}</div>}
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.supression.noBtn")}
              </Button>
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={(e) => {
                  e.preventDefault();
                  this.handleClick(e);
                }}
              >
                {t("popup.supression.yesBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.loadListchefRegionale()),
        dispatch(MoneyTransferAction.loadListRegion()),
        dispatch(MoneyTransferAction.loadListPays()),
      ]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",
    fields: [
      "nomcollaborateur",
      "libelle",
      "codeRegion",
      "pays",
      "idPays",
      "idcollaborateur",
      "action",
    ],
    validate: ParametrageRegionValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    state.ParametrageReducer.dataForDetailregion &&
    state.ParametrageReducer.dataForDetailregion !== null &&
    state.ParametrageReducer.dataForDetailregion !== undefined && {
      initialValues: state.ParametrageReducer.dataForDetailregion,
    }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    saveRegion: state.ParametrageReducer.saveRegion,
    listAdresse: state.ParametrageReducer.listAdresse,
    deleteRegion: state.ParametrageReducer.deleteRegion,
    updateRegion: state.ParametrageReducer.updateRegion,
    transfetList: state.moneyTransfer.transfetList,
    etatCaisse: state.moneyTransfer.etatCaisse,
    dataForDetailregion: state.ParametrageReducer.dataForDetailregion,
    successinstanceregion: state.ParametrageReducer.successinstanceregion,
    listCaisse: state.moneyTransfer.listCaisse,
    caisseDetails: state.moneyTransfer.caisseDetails,
    listChefRegionale: state.ParametrageReducer.listChefRegionale,
    lisRegions: state.ParametrageReducer.lisRegions,
    loadListRegion: state.ParametrageReducer.loadListRegion,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
    libelle: "",
    libellepays: "",
    codeRegion: " ",
    nomcollaborateur: "",
    pays: "",
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class ParametrageRegion extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.handlAnnuler = this.handlAnnuler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
  }

  handlAnnuler(event) {
    this.setState({ libelle: "" });
  }

  async onSubmit(values) {
    try {
      await this.props.saveRegion(values);
      this.props.loadListRegion();
    } catch (error) {
      console.log(error.message);
    }
  }

  async onSubmitTWO(values, id) {
    try {
      await this.props.updateRegion(values, id);
      this.props.loadListRegion();
      this.props.switchf();
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    const {
      fields: {
        libelle,
        nomcollaborateur,
        idcollaborateur,
        idPays,
        codeRegion,
        pays,
        action,
      },
      handleSubmit,
      dataForDetailregion,
      resetForm,
      switchf,
      userFrontDetails,
      successinstanceregion,
      updating,
      deleting,
      saveRegion,
      loadListRegion,
      deleteRegion,
      listChefRegionale,
      updateRegion,
      caisseDetails,
      openCaisse,
      successOpenCaisse,
      lisRegions,
      etatCaisse,
      getListCaisseByAgence,
      values,
      listCaisse,
      listAdresse,
      t,
    } = this.props;

    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const gridMetaData = [
      {
        columnName: "libelle",
        displayName: t("list.cols.libelle"),

        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "codeRegion",
        displayName: t("list.cols.codeRegion"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "nomcollaborateur",
        displayName: t("list.cols.nomcollaborateur"),

        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "nomPays",
        displayName: t("list.cols.pays"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.legend.parametrageregion")}</h2>
          </Col>
        </Row>
        <Row className={styles.compteCard}>
          <form className="formContainer">
            <fieldset>
              {successinstanceregion ? (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.libelle")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libelle}
                      />
                      {libelle.error && libelle.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {libelle.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.codeRegion")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...codeRegion}
                      />
                      {codeRegion.error && codeRegion.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {codeRegion.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.collaborateur")}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...idcollaborateur}
                      >
                        {/*<option value={dataForDetailregion.idcollaborateur}
                                                    hidden>{dataForDetailregion.nomcollaborateur} </option>*/}
                        {listChefRegionale &&
                          listChefRegionale.length !== 0 &&
                          listChefRegionale.map((Adresse) => (
                            <option value={Adresse.id}> {Adresse.nom}</option>
                          ))}
                      </FormControl>
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.pays")}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...idPays}
                      >
                        {/*<option value={dataForDetailregion.idPays}
                                                    hidden>{dataForDetailregion.nomPays} </option>*/}
                        {listAdresse &&
                          listAdresse.length !== 0 &&
                          listAdresse.map((payss) => (
                            <option value={payss.id}> {payss.libelle}</option>
                          ))}
                      </FormControl>
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right">
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmitTWO(values, dataForDetailregion.id);
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.modifier")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          switchf();
                        }}
                      >
                        <i className="fa fa-times" />
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              ) : (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.libelle")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libelle}
                      />
                      {libelle.error && libelle.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {libelle.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.codeRegion")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...codeRegion}
                      />
                      {codeRegion.error && codeRegion.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {codeRegion.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.collaborateur")}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...nomcollaborateur}
                      >
                        <option value="" hidden>
                          {t("form.hidden.selectionnercollaborateur")}{" "}
                        </option>
                        {listChefRegionale &&
                          listChefRegionale.length !== 0 &&
                          listChefRegionale.map((Adresse) => (
                            <option value={Adresse.id}> {Adresse.nom}</option>
                          ))}
                      </FormControl>
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.pays")}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...pays}
                      >
                        <option value="" hidden>
                          {t("form.hidden.selectionnerpays")}{" "}
                        </option>
                        {listAdresse &&
                          listAdresse.length !== 0 &&
                          listAdresse.map((payss) => (
                            <option value={payss.id}> {payss.libelle}</option>
                          ))}
                      </FormControl>
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right">
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmit(values);
                          resetForm();
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.ajouter")}
                      </Button>

                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          values.libelle = "";
                          resetForm();
                        }}
                      >
                        <i className="fa fa-times" />
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              )}
            </fieldset>

            <fieldset>
              <legend>{t("form.legend.listeregion")}</legend>
              <Row className="table-responsive">
                <Griddle
                  results={lisRegions}
                  columnMetadata={gridMetaData}
                  useGriddleStyles={false}
                  noDataMessage={t("list.search.msg.noResult")}
                  resultsPerPage={10}
                  nextText={<i className="fa fa-chevron-right" />}
                  previousText={<i className="fa fa-chevron-left" />}
                  tableClassName="table"
                  columns={[
                    "libelle",
                    "codeRegion",
                    "nomcollaborateur",
                    "nomPays",
                    "action",
                  ]}
                />
              </Row>
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
