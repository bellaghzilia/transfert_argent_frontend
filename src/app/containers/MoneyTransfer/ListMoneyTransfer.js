import React, {Component} from 'react';
import PropTypes from "prop-types";

import {
    Label,
    Col,
    Panel,
    Row,
    ButtonGroup,
    Alert,
    Modal,
    ControlLabel,
    Form,
    FormControl,
    FormGroup,
    PanelGroup
} from 'react-bootstrap';
import {connect} from 'react-redux';
import {initializeWithKey, reduxForm} from 'redux-form';
import Griddle from 'griddle-react';
import {browserHistory} from 'react-router';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import gridPagination from './MoneyTransferPagination';
import SearchFormValidation from './SearchFormValidation';
import * as MoneyTransferAction from './moneytransferreducer';
import Button from 'react-bootstrap-button-loader';
import * as UserActions from '../User/UserReducer';


@connect(
  state => ({
    userFrontDetails : state.user.userFrontDetails,
    dataForDetail: state.moneyTransfer.dataForDetail,
    id: state.moneyTransfer.saveSuccessObject.id,
    successSigneTransfert: state.moneyTransfer.successSigneTransfert,        
}),
{...MoneyTransferAction,...UserActions,initializeWithKey})

class ActionComponent extends Component {
    static propTypes = {
        getInstance: PropTypes.func,
        getInstanceCollaborateur: PropTypes.func,
        showBouttonAnnuler: PropTypes.func,
        showBouttonSupprimer: PropTypes.func,
        deleteTransfertCltCltByID: PropTypes.func,
        loadListTransfertAgence: PropTypes.func,
        annulerTransfertCltCltByID: PropTypes.func,
    };

    constructor() {
        super();
        this.state = {
            showModal: false,
            canceling : false,
            deleting : false,
        }
        this.handleClick = this.handleClick.bind(this);
    }

    async handleClick() {

        try {
            if(this.state.deleting) {
                await this.props.deleteTransfertCltCltByID(this.props.rowData.id);
                this.props.loadListTransfertAgence(this.props.userFrontDetails, '', '', '', '', '', '', '', '', '', '');
            }else
            if(this.state.canceling){
                await this.props.annulerTransfertCltCltByID(this.props.userFrontDetails.caisseNum,this.props.rowData.id);
                this.props.loadListTransfertAgence(this.props.userFrontDetails, '', '', '', '', '', '', '', '', '', '');
            }
        } catch (error) {
            console.log(error.message);
        }
        this.setState({showModal: false});
    }

    render() {
        const {getInstance,userFrontDetails,rowData, getInstanceCollaborateur, showBouttonAnnuler, loadListTransfertAgence, deleteTransfertCltCltByID,annulerTransfertCltCltByID} = this.props;
        const styles = require('./moneytransfer.scss');
        const idtransfert =this.props.rowData.id;
        const close = () => {
            this.setState({showModal: false});
        };


        return (

            <div>
                <ButtonGroup >
                    <Button
                        bsSize="small" bsStyle="warning" className={styles.actionButtonStyle}
                        onClick={(e) => {e.preventDefault();
                            getInstance(rowData.id);
                            showBouttonAnnuler('hide');
                        }}
                    >
                        <i
                            className="fa fa-eye"
                        />

                    </Button>
                    <Button
                        bsSize="small" bsStyle="warning" className={styles.actionButtonStyle}
                        onClick={() =>browserHistory.push(baseUrl+'app/moneyTransfer/update/'+idtransfert)}

                    >
                        <i className="fa fa-pencil fa-fw"/>
                    </Button>

                    <Button
                        bsSize="small"
                        bsStyle="warning" className={styles.actionButtonStyle}
                        onClick={() => this.setState({showModal: true,
                            canceling : true,
                            deleting : false })}>
                        <i className="fa fa-times"/>
                    </Button>


                    <Button
                        bsSize="small"
                        bsStyle="danger" className={styles.actionButtonStyle}
                        onClick={() => this.setState({showModal: true,
                            canceling : false,
                            deleting : true})}
                    >

                        <i className="fa fa-trash-o fa-fw"/>
                    </Button>

                    <Modal
                        show={this.state.showModal}
                        onHide={close}
                        container={this}
                        aria-labelledby="contained-modal-title"
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="contained-modal-title">
                                {this.state.deleting &&
                                <div>Confirmation du suppression</div>
                                }
                                {this.state.canceling &&
                                <div>Confirmation du annulation</div>
                                }
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            {this.state.canceling &&
                            <div>Êtes-vous sûr de vouloir Annuler cette transaction ?</div>
                            }
                            {this.state.deleting &&
                            <div>Êtes-vous sûr de vouloir Supprimer cette transaction ?</div>
                            }
                        </Modal.Body>
                        <Modal.Footer>
                            <ButtonGroup
                                className="pull-right" bsSize="small"
                            >
                                <Button className={styles.ButtonPasswordStyle}
                                        onClick={() => close()}>Non</Button>
                                <Button className={styles.ButtonPasswordStyle} onClick={this.handleClick}>Oui</Button>
                            </ButtonGroup>
                        </Modal.Footer>
                    </Modal>

                </ButtonGroup>
            </div>

        );
    }
}

class StatusComponent extends Component {
    render() {
        let libelleStatut = '';
        if (this.props.rowData.statutCode === 'annuler_client') {
            libelleStatut = <div style={{color: '#999'}}>{this.props.data}</div>;
        } else if (this.props.rowData.statutCode === 'Enregistre') {
            libelleStatut = <div style={{color: '#FFC125'}}>{this.props.data}</div>;
        } else if (this.props.rowData.statutCode === 'rejete') {
            libelleStatut = <div style={{color: 'red'}}>{this.props.data}</div>;
        } else {
            libelleStatut = <div style={{color: '#859'}}>{this.props.data}</div>;
        }
        return (
            <div>
                {libelleStatut}
            </div>
        );
    }
}

class HeaderComponent extends Component {
    render() {
        return (<div
            className="text-center"
        >{this.props.displayName}</div>);
    }
}

@connect(
    state => ({
        caisseObj : state.moneyTransfer.caisseObj,
        userFrontDetails : state.user.userFrontDetails,
        transfetList: state.moneyTransfer.transfetList,
        successSigneTransfert: state.moneyTransfer.successSigneTransfert,
        loadingListTransfertAgence: state.moneyTransfer.loadingListTransfertAgence,
        loadingDeleteTransfert: state.moneyTransfer.loadingDeleteTransfert,
        successDeleteTransfert: state.moneyTransfer.successDeleteTransfert,
        loadingCancelTransfert: state.moneyTransfer.loadingCancelTransfert,
        successCancelTransfert: state.moneyTransfer.successCancelTransfert,

    }),

    {...MoneyTransferAction,...UserActions, initializeWithKey})


@reduxForm({
        form: 'searchMoneyTransferAgence',
        fields: ['codeagence','typeTransfert', 'dateDebut','dateFin','nomEmetteur', 'nomBenif', 'montantMax','montantMin',  'statut'],
        validate: SearchFormValidation,
        initialValues: {
            typeTransfert : '',
            dateDebut : '',
            dateFin : '',
            nomEmetteur :'',
            nomBenif:'',
            montantMax : '',
            montantMin : '',
            statut : '',
        },
        destroyOnUnmount: false
    },
)
export default class ListMoneyTransfer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            startDate: moment(),
            endDate: moment()
        };
        this.handleChangeStart = this.handleChangeStart.bind(this);
        this.handleChangeEnd = this.handleChangeEnd.bind(this);
        this.onStart = this.onStart.bind(this);

    }

    componentWillMount() {
       // this.onStart();
    }

    async onStart() {
        try {
            await this.props.loadUserFrontDetails();
            this.props.loadCaisse(this.props.userFrontDetails.caisseId);
            this.props.loadListTransfertAgence(this.props.userFrontDetails.agenceId, this.props.userFrontDetails.caisseNum, '', moment(), moment(), '', '', '', '', '', '');
        } catch (error) {
            // logging the error
            console.log(error.message);
        }
    }


    handleChangeStart(date) {
        this.setState({startDate: date});
    }

    handleChangeEnd(date) {
        this.setState({endDate: date});
    }


    render() {
        const {
            userFrontDetails, loadUserFrontDetails, loadCaisse, loadListTransfertAgence,
            fields: {codeagence, typeTransfert, dateDebut, dateFin, nomEmetteur, nomBenif, montantMax, montantMin, statut}, listMoneyTransfer, getInstance,
            dataForDetail,successSigneTransfert, view, setView, showAnnuler, showSupprimer, annulerDemande, supprimerDemande, isAnnule, isSupprimer, comptes, showPanelSearch,
            handleSubmit, values, resetForm, caisseObj, transfetList,loadingListTransfertAgence,
            loadingDeleteTransfert, successDeleteTransfert, deleteTransfertCltCltByID, successCancelTransfert,loadingCancelTransfert
        } = this.props;
             
         const styles = require('./moneytransfer.scss');
        const panelStyles = {margin: '5px 10px 5px 10px'};
        const close = () => {
            this.setState({showModal: false});
        };
        values.dateDebut = this.state.startDate;
        values.dateFin = this.state.endDate;
        const gridMetaData = [

            {
                columnName: 'dateTransfert',
                displayName: 'Date Operation',
                customHeaderComponent: HeaderComponent,
            },
            {
                columnName: 'typeTransfert',
                displayName: 'Type Operation',
                customHeaderComponent: HeaderComponent,

            },

            {
                columnName: 'nomEmetteur',
                displayName: 'Emetteur',
                customHeaderComponent: HeaderComponent,

            },
            {
                columnName: 'nomBenif',
                displayName: 'Bénéficiare',
                customHeaderComponent: HeaderComponent,

            },
            {
                columnName: 'montant',
                displayName: 'Montant (XOF)',
                customHeaderComponent: HeaderComponent,

            },
            {
                columnName: 'frais',
                displayName: 'Frais    (XOF)',
                customHeaderComponent: HeaderComponent,

            },
            {
                columnName: 'statut',
                displayName: 'statut Opération',
                customComponent: StatusComponent,
                customHeaderComponent: HeaderComponent,

            },
            {
                columnName: 'action',
                displayName: 'Action',
                customHeaderComponent: HeaderComponent,
                customComponent: ActionComponent,
            }
        ];

        return (


                    <div>
                        <br /> <br /> <br />
                        { view === 'grid' ?
                            <Row>
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="12">
                                        <Col xs="12" md="3">
                                            <ControlLabel></ControlLabel>
                                        </Col>
                                        <Col xs="12" md="6">

                                            <ControlLabel><h2>{t('form.titleForm.tbagentcaisse')}</h2></ControlLabel>

                                        </Col>
                                        <Col xs="12" md="3">
                                            <ControlLabel></ControlLabel>
                                        </Col>
                                    </Col></Row>
                                <br />
                                <br />

                                {loadingDeleteTransfert === false && successDeleteTransfert === true &&
                                <Alert bsStyle="info">
                                    <strong>{t('form.msg.sucesssuppraission')} </strong>
                                </Alert>
                                }
                                {userFrontDetails.caisseId === "" &&
                                    <Alert bsStyle="danger">
                                        <strong>{t('form.msg.aucunecaisseouverte')} </strong>
                                    </Alert>
                                }
                                {loadingDeleteTransfert === false && successDeleteTransfert === false &&
                                <Alert bsStyle="danger">
                                    <strong>{t('form.msg.sucesssuppraission')} </strong>
                                </Alert>
                                }

                                {loadingCancelTransfert === false && successCancelTransfert === true &&
                                <Alert bsStyle="info">
                                    <strong>{t('form.msg.sucessannulation')} </strong>
                                </Alert>
                                }
                                {loadingCancelTransfert === false && successCancelTransfert === "null" &&
                                <Alert bsStyle="danger">
                                    <strong>{t('form.msg.erreurannulation')} </strong>
                                </Alert>
                                }
                                {loadingCancelTransfert === false && successCancelTransfert === false &&
                                <Alert bsStyle="warning">
                                    <strong>{t('form.msg.montantannulationdepasse')} </strong>
                                </Alert>
                                }
                                <br />

                                <Row className={styles.paddingColumn}>
                                    <Col xs={12} md={3}>
                                        <Col xs={12} md={6}>
                                            <ControlLabel>{t('form.label.dateaujourdui')}:</ControlLabel>
                                        </Col>
                                        <Col xs={12} md={6}>
                                            {moment().format('DD/MM/YYYY')}
                                        </Col>
                                    </Col>
                                    <Col xs={12} md={3}>
                                        <Col xs={12} md={4}>
                                            <ControlLabel>{t('form.label.agence')}: </ControlLabel>
                                        </Col>
                                        <Col xs={12} md={8}>
                                            {userFrontDetails.caisseId !== "" ?
                                                caisseObj.nomAgence : userFrontDetails.agenceNom}
                                        </Col>

                                    </Col>
                                    <Col xs={12} md={3}>
                                        <Col xs={12} md={4}>
                                            <ControlLabel>{t('form.label.agent')} : </ControlLabel>
                                        </Col>
                                        <Col xs={12} md={8}>
                                            {userFrontDetails.caisseId !== "" && caisseObj != null && caisseObj != "undefined" ? caisseObj.nomAgent :  userFrontDetails.collaborateurNom}
                                        </Col>
                                    </Col>
                                    <Col xs={12} md={3}>
                                        <Col xs={12} md={6}>
                                            <ControlLabel>Numéro de caisse :</ControlLabel>
                                        </Col>
                                        <Col xs={12} md={6}>
                                            {userFrontDetails.caisseId !== "" && caisseObj.numCaisse}
                                        </Col>
                                    </Col>
                                </Row>
                                <PanelGroup defaultActiveKey="1" accordion>

                                    <Panel header="Situation Caisse " eventKey="2" className={styles.accordionPanel}
                                           style={panelStyles}>

                                        <Row className={styles.paddingColumn}>

                                            <Col xs={12} md={4}>
                                                <Col xs={12} md={6}>
                                                    <ControlLabel> Solde initial :</ControlLabel>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    {userFrontDetails.caisseId !== "" && caisseObj.soldeInitial} <b>XOF</b>
                                                </Col>
                                            </Col>
                                            <Col xs={12} md={4}>
                                                <Col xs={12} md={6}>
                                                    <ControlLabel> Solde actuel :</ControlLabel>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    {userFrontDetails.caisseId !== "" && caisseObj.soldeActuel} <b>XOF</b>
                                                </Col>
                                            </Col>

                                            <Col xs={12} md={4}>
                                                <Col xs={12} md={6}>
                                                    <ControlLabel>Date de mise à jour:</ControlLabel>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    {userFrontDetails.caisseId !== "" && caisseObj.dateModif}
                                                </Col>
                                            </Col>
                                        </Row>
                                    </Panel>
                                    <Panel header="Mouvements de caisse " eventKey="3" className={styles.accordionPanel}
                                           style={panelStyles}>

                                        <Row className={styles.paddingColumn}>
                                            <Col xs={12} md={3}>
                                                <ControlLabel> Total des envois : </ControlLabel>
                                                {userFrontDetails.caisseId !== "" && caisseObj.totalEnvois} <b>XOF</b>
                                            </Col>
                                            <Col xs={12} md={3}>
                                                <ControlLabel> Total des mise a dispositon : </ControlLabel>
                                                {userFrontDetails.caisseId !== "" && caisseObj.totalMiseDispositon} <b>XOF</b>
                                            </Col>
                                            <Col xs={12} md={3}>
                                                <ControlLabel> Total des retraits : </ControlLabel>
                                                {userFrontDetails.caisseId !== "" && caisseObj.totalRetraits} <b>XOF</b>
                                            </Col>
                                            <Col xs={12} md={3}>
                                                <ControlLabel> Total des attributions : </ControlLabel>
                                                {userFrontDetails.caisseId !== "" && caisseObj.totalEnvois} <b>XOF</b>
                                            </Col>

                                        </Row>
                                    </Panel>
                                    <Panel header="Détails mouvements de ciasse" eventKey="4"
                                           className={styles.accordionPanel} style={panelStyles}>
                                        <form >

                                            <Row className={styles.paddingColumn}>

                                                <Col xs={12} md={3}>
                                                    <ControlLabel>Type d'opération : </ControlLabel>
                                                    <FormControl {...typeTransfert} componentClass="select"
                                                                 bsClass={styles.datePickerFormControl}
                                                                 placeholder="select">
                                                        <option value="" hidden></option>
                                                        <option key="1" value="CLTOCA">Transfert</option>
                                                        <option key="2" value="CATOCL">Paiement</option>
                                                        <option key="3" value="CATOAG">Mise à disposition</option>
                                                        <option key="4" value="AGTOCA">Attribution</option>
                                                    </FormControl>
                                                </Col>
                                                <Col xs={12} md={3}>
                                                    <ControlLabel>Status opération : </ControlLabel>
                                                    <FormControl {...statut} type="text"
                                                                 bsClass={styles.datePickerFormControl}/>
                                                </Col>
                                                <Col xs={12} md={3}>
                                                    <ControlLabel>Montant opération min : </ControlLabel>
                                                    <FormControl type="number" {...montantMin}
                                                                 bsClass={styles.datePickerFormControl} placeholder=""
                                                                 min="1"/>
                                                </Col>
                                                <Col xs={12} md={3}>
                                                    <ControlLabel>Date opération min : </ControlLabel>
                                                    <DatePicker
                                                        selectsStart
                                                        selected={this.state.startDate}
                                                        startDate={this.state.startDate}
                                                        maxDate={this.state.endDate}
                                                        onChange={this.handleChangeStart}
                                                        className={styles.datePickerFormControl}
                                                        isClearable="true"
                                                        locale="fr-FR"
                                                        dateFormat="DD/MM/YYYY"
                                                    />
                                                </Col>


                                            </Row>

                                            <Row className={styles.paddingColumn}>

                                                <Col xs={12} md={3}>
                                                </Col>
                                                <Col xs={12} md={3}>
                                                </Col>


                                                <Col xs={12} md={3}>
                                                    <ControlLabel>Montant opération max : </ControlLabel>
                                                    <FormControl {...montantMax}
                                                                 type="number"
                                                                 bsClass={styles.datePickerFormControl}
                                                                 min="1"/>
                                                </Col>


                                                <Col xs={12} md={3}>
                                                    <ControlLabel>Date opération max : </ControlLabel>
                                                    <DatePicker
                                                        selectsEnd
                                                        selected={this.state.endDate}
                                                        endDate={this.state.endDate}
                                                        minDate={this.state.startDate}
                                                        maxDate={moment()}
                                                        onChange={this.handleChangeEnd}
                                                        className={styles.datePickerFormControl}
                                                        isClearable="true"
                                                        locale="fr-FR"
                                                        dateFormat="DD/MM/YYYY"
                                                    />
                                                </Col>
                                            </Row>
                                            <Row className={styles.paddingColumn}>
                                                <Col xs={6} xsOffset={9}>
                                                    <Button
                                                        loading={loadingListTransfertAgence}
                                                        bsStyle="primary"
                                                        onClick={() => {
                                                            loadListTransfertAgence(2, '222', values.typeTransfert, values.dateDebut, values.dateFin, values.nomEmetteur, values.nomBenif, values.montantMin, values.montantMax, '', values.statut)
                                                        }}
                                                    >
                                                        <i className="fa fa-search"/>Rechercher
                                                    </Button>
                                                    <Button
                                                        bsStyle="primary"
                                                        onClick={() => {
                                                            this.setState({startDate: moment(), endDate: moment()});
                                                            values.typeTransfert = '';
                                                            values.dateDebut = this.state.startDate;
                                                            values.dateFin = this.state.endDate;
                                                            values.montantMax = '';
                                                            values.montantMin = '';
                                                            values.statut = '';
                                                            resetForm();
                                                            loadListTransfertAgence(2, '222', '', moment(), moment(), '', '', '', '', '', '');
                                                        }}
                                                    >
                                                        <i className="fa fa-refresh"/> Réinitialiser
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </form>
                                        <Griddle
                                            results={userFrontDetails.caisseId !== "" && transfetList}
                                            columnMetadata={gridMetaData}
                                            useGriddleStyles={false}
                                            noDataMessage={"Aucun résultat trouvé"}
                                            resultsPerPage={10}
                                            nextText={<i className="fa fa-chevron-right"/>}
                                            previousText={<i className="fa fa-chevron-left"/>}
                                            tableClassName="table"
                                            columns={['dateTransfert', 'typeTransfert', 'nomEmetteur', 'nomBenif', 'montant', 'frais', 'statut', 'action']}

                                        />
                                    </Panel>
                                </PanelGroup>
                            </Row>
                            :
                            dataForDetail &&
                            <div>
                                <fieldset>
                                {successSigneTransfert === true &&
                                   <Alert bsStyle="info">
                                        <p>
                                            <strong>{t('msg.signedSuccess')}</strong>
                                        </p>
                                    </Alert>
                                    } 
                                     <legend>{t('form.legend.infosemeteur')}</legend>

                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.numidentiemeteur')}</ControlLabel>
                                    <p>{dataForDetail.numeroPieceIdentiteemeteur}</p>
                                    </Col>
                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.numtelemeteur')}:</ControlLabel>
                                        <p>{dataForDetail.numtelemetteur}</p>
                                    </Col>
                                    <Col xs={12} md={2}>
                                        <ControlLabel>{t('form.label.nom')}:</ControlLabel>
                                        <p>{dataForDetail.nomemetteur}</p>
                                    </Col>
                                    <Col xs={12} md={2}>
                                        <ControlLabel>{t('form.label.prenom')}:</ControlLabel>
                                        <p>{dataForDetail.prenomemeteur}</p>
                                    </Col>
                                    <Col xs={12} md={2}>
                                        <ControlLabel>{t('form.label.ville')}:</ControlLabel>
                                        <p> {dataForDetail.villeemetteur}</p>
                                    </Col>

                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.adresse')}:</ControlLabel>
                                        <p>{dataForDetail.adresseemeteur}</p>
                                    </Col>
                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.codepostal')}:</ControlLabel>
                                        <p>{dataForDetail.codepostalemetteur}</p>
                                    </Col>


                                    <Col xs={12} md={2}>
                                        <ControlLabel>{t('form.label.mail')}</ControlLabel>
                                        <p>{dataForDetail.emailemetteur}</p>
                                    </Col>
                                    <Col xs={12} md={2}>
                                        <ControlLabel>{t('form.label.pays')}</ControlLabel>
                                        <p>{dataForDetail.paysemetteur}</p>
                                    </Col>
                                </fieldset>
                                <fieldset>
                                    <legend>{t('form.legend.infosbeneficiare')}</legend>

                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.numidentibeneficiare')}</ControlLabel>
                                        <p>{dataForDetail.numeroPieceIdentitebeneficiare}</p>
                                    </Col>
                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.numidtelbeneficiare')}</ControlLabel>
                                        <p>{dataForDetail.numerotelebeneficiare}</p>
                                    </Col>
                                    <Col xs={12} md={2}>
                                        <ControlLabel>{t('form.label.nom')} </ControlLabel>
                                        <p>{dataForDetail.nombeneficiare}</p>
                                    </Col>
                                    <Col xs={12} md={2}>
                                        <ControlLabel>{t('form.label.prenom')} </ControlLabel>
                                        <p>{dataForDetail.prenombeneficiare}</p>
                                    </Col>
                                    <Col xs={12} md={2}>
                                        <ControlLabel>{t('form.label.ville')} </ControlLabel>
                                        <p> {dataForDetail.villebeneficiare}</p>
                                    </Col>

                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.adresse')}</ControlLabel>
                                        <p>{dataForDetail.adressebeneficiare}</p>
                                    </Col>
                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.codepostal')}</ControlLabel>
                                        <p>{dataForDetail.codepostalbeneficiare}</p>
                                    </Col>


                                    <Col xs={12} md={2}>
                                        <ControlLabel>{t('form.label.mail')}</ControlLabel>
                                        <p>{dataForDetail.emailbeneficiare}</p>
                                    </Col>
                                    <Col xs={12} md={2}>
                                        <ControlLabel>{t('form.label.pays')}</ControlLabel>
                                        <p>{dataForDetail.paysbeneficiare}</p>
                                    </Col>
                                </fieldset>
                                <fieldset>
                                   <legend>{t('form.legend.infostransfert')}</legend>
                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.montanttransfert')}</ControlLabel>
                                        <p>{dataForDetail.montantOperation}</p>
                                    </Col>
                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.fraienvoi')}</ControlLabel>
                                        <p>{dataForDetail.motif}</p>
                                    </Col>

                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.monatntremise')}</ControlLabel>
                                        <p>{dataForDetail.montantremise}</p>
                                    </Col>
                                    <Col xs={12} md={3}>
                                        <ControlLabel>{t('form.label.motifRemise')}</ControlLabel>
                                        <p>{dataForDetail.fraisenvois}</p>
                                    </Col>


                                </fieldset>
                                { showAnnuler &&
                                <Alert bsStyle="info">
                                    <p>
                                        <i className="fa fa-exclamation-circle"/> &nbsp;&nbsp; {t('form.msg.annulertransaction')}
                                    </p>
                                </Alert>
                                }
                                <Row className="pull-right">

                                    <Button bsStyle="primary" className="glyphicon glyphicon-upload"> <i> Upload
                                        PDF </i></Button>
                                    <Button
                                        onClick={() => {
                                            setView('grid');
                                            browserHistory.push(baseUrl + 'app/request/MoneyTransfer');
                                        }}
                                        bsStyle="primary"
                                    >
                                        <i className="fa fa-reply"/>
                                        &nbsp;&nbsp; {t('form.buttons.retour')}
                                    </Button>

                                    {showAnnuler &&
                                    <Button
                                        onClick={() => this.setState({showModal: true})}
                                        className={styles.ButtonStyle}
                                    >
                                        <i className="fa fa-times"/>
                                        &nbsp;&nbsp; {t('form.buttons.annuler')}
                                    </Button>
                                    }

                                    { showSupprimer &&
                                    <Alert bsStyle="info">
                                        <p>
                                            <i className="fa fa-exclamation-circle"/> &nbsp;&nbsp; {t('form.msg.supprimertransaction')}
                                        </p>
                                    </Alert>
                                    }
                                    {showSupprimer &&
                                    <Button
                                        onClick={() => this.setState({showModal: true})}
                                        className={styles.ButtonStyle}
                                    >
                                        <i className="fa fa-times"/>
                                        &nbsp;&nbsp; Supprimer
                                    </Button>
                                    }
                                    <Modal
                                        show={this.state.showModal}
                                        onHide={close}
                                        container={this}
                                        aria-labelledby="contained-modal-title"
                                    >
                                        <Modal.Header closeButton>
                                            <Modal.Title id="contained-modal-title">{t('form.popup.confirmation.title')}</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                          {t('form.popup.supression.msg')}
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <ButtonGroup
                                                className="pull-right" bsSize="small"
                                            >
                                                <Button className={styles.ButtonPasswordStyle}
                                                        onClick={() => close()}> {t('form.popup.supression.noBtn')}</Button>
                                                <Button className={styles.ButtonPasswordStyle} onClick={() => {
                                                    close();
                                                    supprimerDemande(dataForDetail.id);
                                                }}>{t('form.popup.supression.yesBtn')}</Button>
                                            </ButtonGroup>
                                        </Modal.Footer>
                                    </Modal>

                                    <Modal
                                        show={this.state.showModal}
                                        onHide={close}
                                        container={this}
                                        aria-labelledby="contained-modal-title"
                                    >
                                        <Modal.Header closeButton>
                                            <Modal.Title id="contained-modal-title">{t('form.popup.confirmation.title')}</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            {t('form.popup.confirmation.msg')} ?
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <ButtonGroup
                                                className="pull-right" bsSize="small"
                                            >
                                                <Button className={styles.ButtonPasswordStyle}
                                                        onClick={() => close()}>{t('form.popup.confirmation.noBtn')}</Button>
                                                <Button className={styles.ButtonPasswordStyle} onClick={() => {
                                                    close();
                                                    annulerDemande(dataForDetail.id);
                                                }}>{t('form.popup.confirmation.yesBtn')}</Button>
                                            </ButtonGroup>
                                        </Modal.Footer>
                                    </Modal>
                                </Row>
                            </div>
                }
            </div>
        );
    }
}
