import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Button,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  PanelGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./ParametrageReducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import ParametrageSchemasComptablesValidator from "./Validateur/ParametrageSchemasComptablesValidator";
import { AuthorizedComponent } from "react-router-role-authorization";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

@connect(
  (state) => ({
    listShemas: state.ParametrageReducer.listShemas,
    successinstanceshema: state.ParametrageReducer.successinstanceshema,
    dataForDetailshema: state.ParametrageReducer.dataForDetailshema,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getInstanceShema: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
    showBouttonSupprimer: PropTypes.func,
    deleteTransfertCltCltByID: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    annulerTransfertCltCltByID: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      showModal: false,
      updating: false,
      deleting: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick() {
    try {
      if (this.state.deleting) {
        await this.props.deleteshema(this.props.rowData.id);
        this.props.loadListShemaComptable();
      } else if (this.state.updating) {
        await this.props.getInstanceShema(this.props.rowData.id);
        this.props.loadListShemaComptable();
      }
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      saveShema,
      listType,
      loadListRegion,
      loadListShemaComptable,
      updateShema,
      lisRegions,
      listShemas,
      getInstanceShema,
      deleteshema,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
    };

    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: true,
                showModal: true,
                deleting: false,
              })
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>
          <Button
            bsSize="small"
            bsStyle="danger"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                deleting: true,
                showModal: true,
                updating: false,
              })
            }
          >
            <i className="fa fa-trash-o fa-fw" />
          </Button>
        </ButtonGroup>

        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {this.state.deleting && <div>{t("popup.supression.title")}</div>}
              {this.state.updating && (
                <div>{t("popup.modification.title")}</div>
              )}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.deleting && <div>{t("popup.supression.msg")}</div>}
            {this.state.updating && <div>{t("popup.modification.msg")}</div>}
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.supression.noBtn")}
              </Button>
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={this.handleClick}
              >
                {t("popup.supression.yesBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.loadListtypeoperation()),
        dispatch(MoneyTransferAction.loadListShemaComptable()),
        dispatch(MoneyTransferAction.loadListRegion()),
      ]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",
    fields: [
      "libelle",
      "empMontant",
      "empDate",
      "empCompte",
      "sens",
      "typeOperation",
      "action",
    ],
    validate: ParametrageSchemasComptablesValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    state.ParametrageReducer.dataForDetailshema &&
    state.ParametrageReducer.dataForDetailshema !== null &&
    state.ParametrageReducer.dataForDetailshema !== undefined && {
      initialValues: state.ParametrageReducer.dataForDetailshema,
    }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    listShemas: state.ParametrageReducer.listShemas,
    saveShema: state.ParametrageReducer.saveShema,
    deleteshema: state.ParametrageReducer.deleteshema,
    updateShema: state.ParametrageReducer.updateShema,
    transfetList: state.moneyTransfer.transfetList,
    etatCaisse: state.moneyTransfer.etatCaisse,
    successinstanceshema: state.ParametrageReducer.successinstanceshema,
    successinstanceshema: state.ParametrageReducer.successinstanceshema,
    listCaisse: state.moneyTransfer.listCaisse,
    caisseDetails: state.moneyTransfer.caisseDetails,
    listType: state.ParametrageReducer.listType,
    lisRegions: state.ParametrageReducer.lisRegions,
    loadListShemaComptable: state.ParametrageReducer.loadListShemaComptable,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
    dataForDetailshema: state.ParametrageReducer.dataForDetailshema,
    getInstanceShema: state.moneyTransfer.getInstanceShema,
    libelle: "",
    libellepays: "",
    nomcollaborateur: "",
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class ParametrageSchemasComptables extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.handlAnnuler = this.handlAnnuler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
  }

  handlAnnuler(event) {
    this.setState({ libelle: "" });
  }

  async onSubmit(values) {
    try {
      await this.props.saveShema(values);
      this.props.loadListShemaComptable();
    } catch (error) {
      console.log(error.message);
    }
  }

  async onSubmitTWO(values, id, idtypeOperation) {
    try {
      await this.props.updateShema(values, id, idtypeOperation);
      this.props.loadListShemaComptable();
      this.props.switchf();
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    const {
      fields: {
        libelle,
        empMontant,
        empDate,
        empCompte,
        sens,
        typeOperation,
        action,
      },
      handleSubmit,
      switchf,
      successinstanceshema,
      dataForDetailshema,
      resetForm,
      updating,
      deleting,
      saveShema,
      loadListShemaComptable,
      deleteshema,
      listType,
      updateShema,
      caisseDetails,
      openCaisse,
      successOpenCaisse,
      lisRegions,
      listShemas,
      etatCaisse,
      getListCaisseByAgence,
      getInstanceShema,
      values,
      userFrontDetails,
      listCaisse,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const gridMetaData = [
      {
        columnName: "libelle",
        displayName: t("list.cols.libelle"),

        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "empMontant",
        displayName: t("list.cols.empMontant"),
        customHeaderComponent: HeaderComponent,
      },

      {
        columnName: "empDate",
        displayName: t("list.cols.empDate"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "empCompte",
        displayName: t("list.cols.empCompte"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "sens",
        displayName: t("list.cols.sens"),
        customHeaderComponent: HeaderComponent,
      },

      {
        columnName: "typeOperation",
        displayName: t("list.cols.typeOperation"),
        customHeaderComponent: HeaderComponent,
      },

      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.legend.parametrageshema")}</h2>
          </Col>
        </Row>
        <Row>
          <form className="formContainer">
            <fieldset>
              {successinstanceshema ? (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.libelle")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libelle}
                        placeholder={dataForDetailshema.libelle}
                      />
                      {libelle.error && libelle.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {libelle.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.empmontant")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...empMontant}
                        placeholder={dataForDetailshema.empMontant}
                      />
                      {empMontant.error && empMontant.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {empMontant.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.empdate")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...empDate}
                        placeholder={dataForDetailshema.empDate}
                      />
                      {empDate.error && empDate.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {empDate.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.empcompte")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...empCompte}
                        placeholder={dataForDetailshema.empCompte}
                      />
                      {empCompte.error && empCompte.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {empCompte.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>

                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.sens")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...sens}
                        placeholder={dataForDetailshema.sens}
                      />
                      {sens.error && sens.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {sens.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.typeoperation")}{" "}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...typeOperation}
                      >
                        <option
                          value={dataForDetailshema.idtypeOperation}
                          hidden
                        >
                          {dataForDetailshema.typeOperation}
                        </option>
                        {listType &&
                          listType.length !== 0 &&
                          listType.map((Type) => (
                            <option value={Type.id}>
                              {" "}
                              {Type.typeOperation}
                            </option>
                          ))}
                      </FormControl>
                    </Col>
                  </Row>

                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmitTWO(
                            values,
                            dataForDetailshema.id,
                            dataForDetailshema.idtypeOperation
                          );
                          resetForm();
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.modifier")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          switchf();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              ) : (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.libelle")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libelle}
                      />
                      {libelle.error && libelle.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {libelle.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.empmontant")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...empMontant}
                      />
                      {empMontant.error && empMontant.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {empMontant.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.empdate")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...empDate}
                      />
                      {empDate.error && empDate.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {empDate.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.empcompte")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...empCompte}
                      />
                      {empCompte.error && empCompte.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {empCompte.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>

                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.sens")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...sens}
                      />
                      {sens.error && sens.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {sens.error}
                          </i>
                        </div>
                      )}
                    </Col>

                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.typeoperation")}{" "}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...typeOperation}
                      >
                        <option value="" hidden>
                          {t("form.hidden.selectionnertype")}
                        </option>
                        {listType &&
                          listType.length !== 0 &&
                          listType.map((Type) => (
                            <option value={Type.id}>
                              {" "}
                              {Type.typeOperation}
                            </option>
                          ))}
                      </FormControl>
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmit(values);
                          resetForm();
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.ajouter")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          values.libelle = "";
                          resetForm();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              )}
            </fieldset>

            <fieldset style={{ marginTop: "15px" }}>
              <legend>{t("form.legend.listeshema")}</legend>
              <Row className="table-responsive">
                <Griddle
                  results={listShemas}
                  columnMetadata={gridMetaData}
                  useGriddleStyles={false}
                  noDataMessage={t("list.search.msg.noResult")}
                  resultsPerPage={10}
                  nextText={<i className="fa fa-chevron-right" />}
                  previousText={<i className="fa fa-chevron-left" />}
                  tableClassName="table"
                  columns={[
                    "libelle",
                    "empMontant",
                    "empDate",
                    "empCompte",
                    "sens",
                    "typeOperation",
                    "action",
                  ]}
                />
              </Row>
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
