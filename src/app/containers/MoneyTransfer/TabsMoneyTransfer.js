import React, { Component } from "react";
import PropTypes from "prop-types";

import { Tabs, Tab, Button } from "react-bootstrap";
import { reduxForm, initializeWithKey } from "redux-form";
import { connect } from "react-redux";
import NewMoneyTransfer from "./NewMoneyTransfer";
import ListMoneyTransfer from "./ListMoneyTransfer";
import TooltipComposant from "../Commons/TooltipComposant";
import * as MoneyTransferAction from "./moneytransferreducer";

@connect(
  (state) => ({
    step: state.moneyTransfer.step,
    statut: state.moneyTransfer.statut,
    activeAllTabs: state.moneyTransfer.activeAllTabs,
    user: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class TabsMoneyTransfer extends Component {
  static propTypes = {
    toStepX: PropTypes.func,
    setStatut: PropTypes.func,
    step: PropTypes.array,
  };
  constructor() {
    super();
    this.state = {
      listMoneyTransfer: true,
      activeKey: "1",
    };
  }
  render() {
    const {
      listMoneyTransfer,
      activeAllTabs,
      listBeneficiary,
      listDebitAccount,
      setStatut,
      toStepX,
      step,
      user,
    } = this.props;
    const styles = require("./moneytransfer.scss");

    const handleSelect = (selectedKey) => {
      this.setState({ activeKey: selectedKey });
    };
    let moneyTransfer;
    return (
      <div>
        <div className="pull-right">
          <Button
            className={styles.ButtonStyle}
            onClick={() => {
              this.setState({
                listMoneyTransfer: !this.state.listMoneyTransfer,
              });
              setStatut("");
              toStepX("stepOne");
            }}
          >
            {this.state.listMoneyTransfer ? (
              <i className="fa fa-plus"> &nbsp;Nouveau Transfert</i>
            ) : (
              <i className="fa fa-list-alt"> &nbsp;Liste Transfert</i>
            )}
          </Button>
        </div>

        {activeAllTabs ? (
          <Tabs defaultActiveKey={1} onSelect={handleSelect}>
            <Tab eventKey={1} title="liste">
              <br />
              {this.state.listMoneyTransfer ? (
                <ListMoneyTransfer>{listMoneyTransfer}</ListMoneyTransfer>
              ) : (
                <NewMoneyTransfer
                  user={user}
                  listBeneficiary={listBeneficiary}
                  listDebitAccount={listDebitAccount}
                />
              )}
            </Tab>
          </Tabs>
        ) : (
          <NewMoneyTransfer
            listBeneficiary={listBeneficiary}
            listDebitAccount={listDebitAccount}
          />
        )}
      </div>
    );
  }
}
