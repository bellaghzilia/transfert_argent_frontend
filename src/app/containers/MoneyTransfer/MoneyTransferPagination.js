import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, Pagination } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { initializeWithKey } from "redux-form";
import { asyncConnect } from "redux-connect";
import * as MoneyTransferAction from "./moneytransferreducer";

@connect(
  (state) => ({
    maxPages: state.moneyTransfer.maxPages,
    currentPage: state.moneyTransfer.currentPage,
    dateDebutRecherche: state.moneyTransfer.dateDebutRecherche,
    dateFinRecherche: state.moneyTransfer.dateFinRecherche,
    statutRecherche: state.moneyTransfer.statutRecherche,
    identifiantDemandeRecheche: state.moneyTransfer.identifiantDemandeRecheche,
    montantMin: state.moneyTransfer.montantMin,
    montantMax: state.moneyTransfer.montantMax,
    ncompteRecherche: state.moneyTransfer.ncompteRecherche,
    typeTransfert: state.moneyTransfer.typeTransfert,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class MoneyTransferPagination extends Component {
  static propTypes = {
    changeIndexPage: PropTypes.func,
    maxPages: PropTypes.number,
  };
  constructor() {
    super();
    this.state = {
      activePage: 1,
    };
  }

  render() {
    const {
      changeIndexPage,
      maxPages,
      currentPage,
      dateDebutRecherche,
      dateFinRecherche,
      identifiantDemandeRecheche,
      montantMax,
      montantMin,
      ncompteRecherche,
      statutRecherche,
      typeTransfert,
    } = this.props;
    let maxPage = 0;
    if (maxPages % 10 === 0) {
      maxPage = maxPages / 10;
    } else {
      maxPage = Math.ceil(maxPages / 10);
    }
    const changePage = (event) => {
      this.setState({
        activePage: event,
      });
      changeIndexPage(
        event,
        ncompteRecherche,
        dateDebutRecherche,
        dateFinRecherche,
        montantMin,
        montantMax,
        statutRecherche,
        identifiantDemandeRecheche,
        typeTransfert
      );
    };
    return (
      <div>
        {maxPages > 10 ? (
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={maxPage}
            maxButtons={5}
            bsSize="medium"
            activePage={this.state.activePage}
            onSelect={changePage}
          />
        ) : (
          <div />
        )}
      </div>
    );
  }
}
