// import React, {Component, PropTypes}    from "react";
// import {Alert, ButtonGroup, Col, ControlLabel, FormControl, Panel, Modal, PanelGroup, Row} from "react-bootstrap";
// import {connect}                        from "react-redux";
// import {initializeWithKey, reduxForm}   from "redux-form";
// import {browserHistory}                 from "react-router";
// import {asyncConnect}                   from "redux-connect";
// import moment                           from "moment";
// import DatePicker                       from "react-datepicker";
// import {translate}                      from 'react-i18next';
// import Griddle                          from "griddle-react";
// import * as MoneyTransferAction         from "./moneytransferreducer";
// import * as UserActions                 from '../User/UserReducer';
// import Button                           from 'react-bootstrap-button-loader';
// import {AuthorizedComponent}            from 'react-router-role-authorization';
//
// var result;
// var myData = sessionStorage.getItem('myData');
// if (myData != null && myData != "null") {
//     result = myData;
// } else {
//     var url1 = window.location.href;
//     var captured2;
//     captured2 = /idAgence=([^&]+)/.exec(url1)[1];
// // setter
//     window.sessionStorage.setItem("myData", captured2 ? captured2 : 'null');
// // getter
//     result = sessionStorage.getItem('myData');
// }
//
//
// @connect(
//     state => ({
//         dataForDetail: state.moneyTransfer.dataForDetail,
//         successSignechefTransfert:state.moneyTransfer.successSignechefTransfert,
//         successSignechefPaiement:state.moneyTransfer.successSignechefPaiement,
//         id: state.moneyTransfer.saveSuccessObject.id, step: state.moneyTransfer.step,
//         idAgence:result
//     }),
//     {...MoneyTransferAction, ...UserActions, initializeWithKey})
//
// class MontantComponent extends Component {
//     render() {
//         const url = this.props.data;
//         return (
//             <div> <b> {this.props.data} XOF </b> </div>
//         );
//     }
// }
//
// @translate(['MoneyTransfer'], {wait: true})
// class CenterComponent extends Component {
//     render() {
//         const url = this.props.data;
//         return <div>{this.props.data}</div>;
//     }
// }
//
// @connect(
//     state => ({
//         userFrontDetails: state.user.userFrontDetails,
//     }),
//     {...MoneyTransferAction, ...UserActions, initializeWithKey})
// class ActionComponent extends Component {
//     static propTypes = {
//         getInstance: PropTypes.func,
//         validationMiseadispositionAgence:PropTypes.func,
//         getInstanceCollaborateur: PropTypes.func,
//         showBouttonAnnuler: PropTypes.func,
//         showBouttonSupprimer: PropTypes.func,
//         deleteTransfertCltCltByID: PropTypes.func,
//         loadListTransfertAgence: PropTypes.func,
//         annulerTransfertCltCltByID: PropTypes.func,
//     };
//
//     constructor(props) {
//         super(props);
//         this.state = {
//             showModal: false,
//             canceling: false,
//             deleting: false,
//         }
//         this.handleClick = this.handleClick.bind(this);
//     }
//
//     async handleClick() {
//
//         try {
//             if (this.state.deleting) {
//                 await this.props.deleteTransfertCltCltByID(this.props.rowData.id);
//                 this.props.loadListTransfertAgence(this.props.userFrontDetails.agenceId, '', '', '', '', '', '', '', '', '', '');
//             }
//         } catch (error) {
//             console.log(error.message);
//         }
//         this.setState({showModal: false});}
//     async ValideMiseadisposition(agenceId,caisseNum,montant,id) {
//         await  this.props.validationMiseadispositionAgence(agenceId,caisseNum,montant,id);
//         this.props.loadListTransfertAgence(agenceId,caisseNum, '', moment(), moment(), '', '', '', '', '', '');
//         this.props.loadAgence(agenceId);
//     }
//     async RefussionMiseadisposition(agenceId,caisseNum,montant,id) {
//         await  this.props.refussionMiseadispositionAgence(agenceId,caisseNum,montant,id);
//         this.props.loadListTransfertAgence(agenceId,caisseNum, '', moment(), moment(), '', '', '', '', '', '');
//         this.props.loadAgence(agenceId);
//     }
//
//
//     render() {
//
//         const {getInstance,resetForm,validationMiseadispositionAgence,getInstanceCollaborateur, userFrontDetails, showBouttonAnnuler, loadListTransfertAgence, t, deleteTransfertCltCltByID, annulerTransfertCltCltByID} = this.props;
//         const styles = require('./moneytransfer.scss');
//         const handleSelect = (list) => {
//             if (!list) {
//                 browserHistory.push(baseUrl + 'app/request/MoneyTransfer');
//             }
//         };
//         const close = () => {
//             this.setState({showModal: false});
//         };
//         const idtransfert = this.props.rowData.id;
//
//         const aa = this.props.rowData.statutOperation;
//
//         return (
//             <div>
//
//                 {(this.props.rowData.typeTransfert === "Transfert" || this.props.rowData.typeTransfert === "Paiement") ?
//                     <ButtonGroup>
//                         <Button
//                             bsSize="small" bsStyle="warning" className={styles.actionButtonStyle}
//                             onClick={() => {getInstance(this.props.rowData.id);  showBouttonAnnuler('hide');
//                             }} >  <i className="fa fa-eye"/>
//                         </Button>
//                         {this.props.rowData.statut !== "signer" && this.props.rowData.typeTransfert !== "Paiement" &&
//                         <Button
//                             bsSize="small" bsStyle="warning" className={styles.actionButtonStyle}
//                             onClick={() => browserHistory.push(baseUrl + 'app/moneyTransfer/update/' + idtransfert)} >
//                             <i className="fa fa-pencil fa-fw"/>
//                         </Button>
//                         }
//                         {(this.props.rowData.statut === "En cours d'enregistrement" || this.props.rowData.statut === "signer" ||this.props.rowData.statut === "En cours validation") &&
//
//                         <Button
//                             bsSize="small"
//                             bsStyle="danger" className={styles.actionButtonStyle}
//                             onClick={() => this.setState({  showModal: true,  canceling: false,  deleting: true})}>
//                             <i className="fa fa-trash-o fa-fw"/>
//                         </Button>
//                         }
//                         <Modal
//                             show={this.state.showModal}
//                             onHide={close}
//                             container={this}
//                             aria-labelledby="contained-modal-title"
//                         >
//                             <Modal.Header closeButton>
//                                 <Modal.Title id="contained-modal-title">
//                                     {this.state.deleting &&
//                                     <div>{t('popup.supression.title')}</div>
//                                     }
//                                     {this.state.canceling &&
//                                     <div>{t('popup.confirmation.title')}</div>
//                                     }
//                                 </Modal.Title>
//                             </Modal.Header>
//                             <Modal.Body>
//                                 {this.state.canceling &&
//                                 <div>{t('popup.confirmation.msg')}</div>
//                                 }
//                                 {this.state.deleting &&
//                                 <div>{t('popup.supression.msg')}</div>
//                                 }
//                             </Modal.Body>
//                             <Modal.Footer>
//                                 <ButtonGroup
//                                     className="pull-right" bsSize="small"
//                                 >
//                                     <Button className={styles.ButtonPasswordStyle}
//                                             onClick={() => close()}>Non</Button>
//                                     <Button className={styles.ButtonPasswordStyle}
//                                             onClick={this.handleClick}>Oui</Button>
//                                 </ButtonGroup>
//                             </Modal.Footer>
//                         </Modal>
//
//
//                     </ButtonGroup>
//                     :
//                     <div>
//                         {(this.props.rowData.statut !== "Validé" &&  this.props.rowData.statut !=="Refusé" && this.props.rowData.typeTransfert ==="Mise à disposition") &&
//                         <ButtonGroup >
//                             <Button bsSize="small" bsStyle="warning" className={styles.actionButtonStyle}
//                                     onClick={(e) => {this.ValideMiseadisposition(this.props.userFrontDetails.agenceId,this.props.userFrontDetails.caisseNum,this.props.rowData.montant,this.props.rowData.id);
//                                     }}  ><i className="fa fa-check"/></Button>
//                             <Button bsSize="small" bsStyle="danger" className={styles.actionButtonStyle}
//                                     onClick={(e) => {this.RefussionMiseadisposition(this.props.userFrontDetails.agenceId,this.props.userFrontDetails.caisseNum,this.props.rowData.montant,this.props.rowData.id);
//                                     }} ><i className="fa fa-times"/></Button>
//                         </ButtonGroup>
//                         }
//                     </div>}
//             </div>
//
//         );
//     }
// }
//
//
// @connect(
//     state => ({
//         userFrontDetails: state.user.userFrontDetails,
//         agenceObj: state.moneyTransfer.agenceObj,
//     }),
//     {...MoneyTransferAction, ...UserActions, initializeWithKey})
// class ActionAgenceAgenceComponent extends Component {
//     static propTypes = {
//         validationMiseadispositionAgence: PropTypes.func,
//         refussionMiseadispositionAgence: PropTypes.func,
//     };
//     constructor(props) {
//         super(props);
//         this.state = {
//             showModal: false,
//             canceling: false,
//             deleting: false,
//         }
//     }
//     render() {
//         const styles = require('./moneytransfer.scss');
//         const {validationMiseadispositionAgence,resetForm,agenceObj,userFrontDetails,refussionMiseadispositionAgence}= this.props;
//         return (
//             <div>
//                 <a className="glyphicon glyphicon-upload btn btn-primary"
//                    href={baseUrl + 'agencetransfertargent/createAvisTransfertPDF/' + this.props.rowData.reference}>
//                 </a>
//
//             </div>
//         );
//     }
// }
//
// @translate(['MoneyTransfer'], {wait: true})
// class HeaderComponent extends Component {
//     render() {
//         return (<div>{this.props.displayName}</div>);
//     }
// }
//
//
// @connect(
//     state => ({
//         agenceObj: state.moneyTransfer.agenceObj,
//         actionList: state.moneyTransfer.actionList,
//         userFrontDetails: state.user.userFrontDetails,
//         loadingAgence: state.user.loadingAgence,
//         transfetList: state.moneyTransfer.transfetList,
//         statusChangingEtat: state.moneyTransfer.statusChangingEtat,
//         successOpenAgnc: state.moneyTransfer.successOpenAgnc,
//         successChangingEtat: state.moneyTransfer.successChangingEtat,
//         transfetAgncAgncList: state.moneyTransfer.transfetAgncAgncList,
//         totalMontant: state.moneyTransfer.totalMontant,
//         totalMontantTransfertAgence: state.moneyTransfer.totalMontantTransfertAgence,
//         unsuccessChangingEtat: state.moneyTransfer.unsuccessChangingEtat,
//         miseadispositionAgenceNv: state.moneyTransfer.miseadispositionAgenceNv,
//         isSafeToClose: state.moneyTransfer.isSafeToClose,
//         loadingDeleteTransfert: state.moneyTransfer.loadingDeleteTransfert,
//         successDeleteTransfert: state.moneyTransfer.successDeleteTransfert,
//         loadingCancelTransfert: state.moneyTransfer.loadingCancelTransfert,
//         successCancelTransfert: state.moneyTransfer.successCancelTransfert,
//         loadingChangeEtatAgnece: state.moneyTransfer.loadingChangeEtatAgnece,
//         dataForDetail: state.moneyTransfer.dataForDetail,
//         successSignechefTransfert:state.moneyTransfer.successSignechefTransfert,
//         successSignechefPaiement:state.moneyTransfer.successSignechefPaiement,
//         successtransfert: state.moneyTransfer.successtransfert,
//         loadingListTransfertAgence: state.moneyTransfer.loadingListTransfertAgence,
//         loadingListTransfertAgenceAgence: state.moneyTransfer.loadingListTransfertAgenceAgence,
//         loadingActionList: state.moneyTransfer.loadingActionList,
//         successseuilagence: state.moneyTransfer.successseuilagence,
//         messageSeuilAgence: state.moneyTransfer.messageSeuilAgence,
//
//
//     }),
//     {...MoneyTransferAction, ...UserActions, initializeWithKey})
//
// @asyncConnect([{
//     promise: ({store: {dispatch, getState}}) => {
//         const promises = [];
//         promises.push(dispatch(MoneyTransferAction.RestForm()));
//         if (!UserActions.isUserFrontDetailsLoaded(getState())) {
//             promises.push(dispatch(UserActions.loadUserFrontDetails()));
//         }
//         if(result==null){
//             promises.push(dispatch(MoneyTransferAction.loadSeuilSoldeAgence(getState().user.userFrontDetails.agenceId)));
//             promises.push(dispatch(MoneyTransferAction.loadAgence(getState().user.userFrontDetails.agenceId)));
//             promises.push(dispatch(MoneyTransferAction.loadListTransfertAgence(getState().user.userFrontDetails.agenceId, '', '', moment(), moment(), '', '', '', '', '', '')));
//             promises.push(dispatch(MoneyTransferAction.loadListAgncAgncTransfertAgence(getState().user.userFrontDetails.agenceId, '', '', '', '', moment(), moment(), '', '')));
//             promises.push(dispatch(MoneyTransferAction.loadListOuvertureClotureCaisses(getState().user.userFrontDetails.agenceId, '', '', '', '', moment(), moment(), '', '')));
//
//         }else{
//             promises.push(dispatch(MoneyTransferAction.loadSeuilSoldeAgence(result)));
//             promises.push(dispatch(MoneyTransferAction.loadAgence(result)));
//             promises.push(dispatch(MoneyTransferAction.loadListTransfertAgence(result, '', '', moment(), moment(), '', '', '', '', '', '')));
//             promises.push(dispatch(MoneyTransferAction.loadListAgncAgncTransfertAgence(result, '', '', '', '', moment(), moment(), '', '')));
//             promises.push(dispatch(MoneyTransferAction.loadListOuvertureClotureCaisses(result, '', '', '', '', moment(), moment(), '', '')));
//         }
//
//         return Promise.all(promises);
//     }
// }])
//
// @reduxForm({
//         form: 'searchMoneyTransferAgence',
//         fields: ['codeagence', 'typeTransfert', 'dateDebut', 'dateFin', 'nomEmetteur', 'nomBenif', 'montantMax', 'montantMin', 'agent', 'statut',
//             'agenceEchange', 'dateEmissionDebut', 'dateEmissionFin', 'dateReceptionDebut', 'dateReceptionFin', 'montantOperationDebut', 'montantOperationFin', 'statutOperation',
//             'numcaisse','agentaction','montantinitial','montantfinal','dateActionDebut','dateActionFin','heuremin','heuremax'],
//         initialValues: {
//             id: '',
//             dataForDetail: {},
//             codeagence: '',
//             typeTransfert: '',
//             dateDebut: '',
//             dateFin: '',
//             nomEmetteur: '',
//             nomBenif: '',
//             montantMax: '',
//             montantMin: '',
//             frais: '',
//             statut: '',
//             agenceEchange: '',
//             dateEmissionDebut: '',
//             dateEmissionFin: '',
//             dateReceptionDebut: '',
//             dateReceptionFin: '',
//             montantOperationDebut: '',
//             montantOperationFin: '',
//             statutOperation: '',
//             numcaisse : '',
//             agentaction : '',
//             montantinitial : '',
//             montantfinal : '',
//             dateActionDebut : '',
//             dateActionFin : '',
//             heuremin : '',
//             heuremax : ''
//         },
//         destroyOnUnmount: true
//     },
// )
// @translate(['MoneyTransfer'], {wait: true})
// export default class TabeBordChefAgence extends AuthorizedComponent {
//     static propTypes = {
//         validationMiseadispositionAgence: PropTypes.func,
//         refussionMiseadispositionAgence: PropTypes.func,
//     };
//     constructor(props) {
//         super(props);
//         this.userRoles = this.props.userFrontDetails.roles;
//         this.notAuthorizedPath = baseUrl + 'app/AccessDenied';
//         this.state = {
//             /////
//             openClavier: false,
//             keyPadValues: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
//             ShowPanelValidationStepOne: false,
//             ////
//             showPanelPassword: false,
//             showModalAbandonne: false,
//             pass: '',
//             devise: '',
//             GAB: false,
//             buttonchoice: true,
//             date: false,
//             color1: 'info',
//             color2: 'danger',
//             checked: true,
//             startDate: moment(),
//             endDate: moment(),
//             startReceptionDate: '',
//             endReceptionDate: '',
//             startEmissionDate: moment(),
//             endEmissionDate: moment(),
//             startActionDate: moment(),
//             endActionDate: moment(),
//             showModal: false,
//             maxmin1:false,
//             maxmin2:false,
//             maxmin3:false,
//         };
//         this.handleChangeStart = this.handleChangeStart.bind(this);
//         this.handleChangeEnd = this.handleChangeEnd.bind(this);
//         this.handleChangeEmissionStart = this.handleChangeEmissionStart.bind(this);
//         this.handleChangeEmissionEnd = this.handleChangeEmissionEnd.bind(this);
//         this.handleChangeReceptionStart = this.handleChangeReceptionStart.bind(this);
//         this.handleChangeReceptionEnd = this.handleChangeReceptionEnd.bind(this);
//         this.handleChangeActionStart = this.handleChangeActionStart.bind(this);
//         this.handleChangeActionEnd = this.handleChangeActionEnd.bind(this);
//         this.handleChangeEtat = this.handleChangeEtat.bind(this);
//     }
//
//     async handleChangeEtat(e) {
//
//         e.preventDefault();
//         await this.props.changeEtatAgence(this.props.agenceObj);
//         this.props.loadUserFrontDetails();
//         this.props.loadAgence(this.props.userFrontDetails.agenceId);
//
//     };
//
//     handleChangeStart(date) {
//         this.setState({startDate: date});
//     };
//
//     handleChangeEnd(date) {
//         this.setState({endDate: date});
//     };
//
//     handleChangeReceptionStart(date) {
//         this.setState({startReceptionDate: date});
//     };
//
//     handleChangeReceptionEnd(date) {
//         this.setState({endReceptionDate: date});
//     };
//
//     handleChangeEmissionStart(date) {
//         this.setState({startEmissionDate: date});
//     };
//
//     handleChangeEmissionEnd(date) {
//         this.setState({endEmissionDate: date});
//     };
//
//     handleChangeActionStart(date) {
//         this.setState({startActionDate: date});
//     };
//
//     handleChangeActionEnd(date) {
//         this.setState({endActionDate: date});
//     };
// componentDidMount(){
//         console.log("componentDidMount")
//         this.props.resetForm();
//     }
//
//     render() {
//
//         const {
//             fields: {
//                 codeagence, typeTransfert, dateDebut, dateFin, nomEmetteur, nomBenif, montantMax, montantMin, agent, statut,
//                 agenceEchange, dateEmissionDebut, dateEmissionFin, dateReceptionDebut, dateReceptionFin, montantOperationDebut, montantOperationFin, statutOperation,
//                 numcaisse, agentaction, montantinitial, montantfinal, dateActionDebut,dateActionFin, heuremin,heuremax
//             },successseuilagence,messageSeuilAgence, loadUserFrontDetails,validationMiseadispositionAgence,signeTransfertChefAgenceByID,signePaiementChefAgenceByID,submitting,successSignechefTransfert,  successSignechefPaiement,
//             values, listMoneyTransfer, getInstance, dataForDetail, view, setView, successtransfert, showAnnuler, showSupprimer, annulerDemande, supprimerDemande,
//             isAnnule, isSupprimer, comptes, showPanelSearch, params, resetForm, signRedirect, signUrlred, taskId, loadAgence, agenceObj,
//             changeEtatAgence, success, loadListTransfertAgence, transfetList, loadListAgncAgncTransfertAgence,totalMontantTransfertAgence, totalMontant,transfetAgncAgncList,
//             loadingChangeEtatAgnece, loadingAgence, listeStatuts, loadListMoneyTransferAgence, successChangingEtat, successOpenAgnc,isSafeToClose,miseadispositionAgenceNv, unsuccessChangingEtat, loadingListTransfertAgence, loadingListTransfertAgenceAgence,
//             loadingDeleteTransfert, successDeleteTransfert, deleteTransfertCltCltByID, switchf, successCancelTransfert, loadingCancelTransfert, userFrontDetails, statusChangingEtat, actionList, loadingActionList,loadListOuvertureClotureCaisses,
//             t
//         } = this.props;
//
//         const gridMetaData = [
//             {
//                 columnName: 'agent',
//                 displayName: 'Agent',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'typeTransfert',
//                 displayName: t('list.cols.typeOperation'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'dateTransfert',
//                 displayName: t('list.cols.dateOperation'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'nomEmetteur',
//                 displayName: t('list.cols.nomEmetteur'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'nomBenif',
//                 displayName: t('list.cols.nomBenif'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'montant',
//                 displayName: t('list.cols.Montant'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: MontantComponent,
//             },
//             {
//                 columnName: 'frais',
//                 displayName: t('list.cols.frais'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'statut',
//                 displayName: t('list.cols.statusoperation'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'action',
//                 displayName: t('list.cols.actions'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: ActionComponent,
//             }
//         ];
//
//         const actionGridMetaData = [
//             {
//                 columnName: 'numcaisse',
//                 displayName: 'Numero de Caisse',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'agentaction',
//                 displayName: 'Agent',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'montantinitial',
//                 displayName: 'Total attribution',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'montantfinal',
//                  displayName: 'Total mise à disposition',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'dateaction',
//                 displayName: 'Date d\'action',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'heuredebut',
//                 displayName: 'Heure debut',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'heurefin',
//                 displayName: 'Heure fin',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             }
//         ];
//
//         const gridMetaDataAgncAgnc = [
//
//
//             {
//                 columnName: 'agenceEmitteur',
//                 displayName: 'Agence émission',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'agenceRecepteur',
//                 displayName: 'Agence réception',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'dateEmission',
//                 displayName: 'Date émission',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'dateReception',
//                 displayName: 'Date réception',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'montantOperation',
//                 displayName: 'Montant opération',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: MontantComponent,
//             },
//             {
//                 columnName: 'statutOperation',
//                 displayName: 'Statut opération',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'action',
//                 displayName: t('list.cols.actions'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: ActionAgenceAgenceComponent,
//             }
//         ];
//         const styles = require('./moneytransfer.scss');
//         const panelStyles = {margin: '5px 10px 5px 10px'};
//
//
//         values.dateDebut = this.state.startDate;
//         values.dateFin = this.state.endDate;
//         values.dateEmissionDebut = this.state.startEmissionDate;
//         values.dateEmissionFin = this.state.endEmissionDate;
//         values.dateReceptionDebut = this.state.startReceptionDate;
//         values.dateReceptionFin = this.state.endReceptionDate;
//         values.dateActionDebut = this.state.startActionDate;
//         values.dateActionFin = this.state.endActionDate;
//
//         return (
//             <div>
//
//                 {dataForDetail && dataForDetail.id !== "" ?
//                     <div>
//                         {successSignechefTransfert === true &&
//                         <Alert bsStyle="success">
//                                 {t('msg.signedSuccess')}
//                         </Alert>
//                         }
//                         { successSignechefPaiement === true &&
//                         <Alert bsStyle="success">
//                                 {t('msg.signedpaiementSuccess')}
//                         </Alert>
//                         }
//                         <Row className={styles.compteCard}>
//                         <form className="formContainer">
//                             <fieldset>
//                                 <legend>{t('form.legend.infosemeteur')}</legend>
//                                 <Row>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.numidentiemeteur')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.numeroPieceIdentiteemeteur}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.numtelemeteur')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.numtelemetteur}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.nom')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.nomemetteur}</p>
//                                     </Col>
//                                 </Row>
//
//                                 <Row>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.prenom')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.prenomemeteur}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.mail')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.emailemetteur}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.ville')}</ControlLabel>
//                                         <p className="detail-value"> {dataForDetail.villeemetteur}</p>
//                                     </Col>
//                                 </Row>
//
//                                 <Row>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.region')}</ControlLabel>
//                                         <p className="detail-value"> {dataForDetail.regionemeteur}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.pays')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.paysemetteur}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.indicatif')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.paysindicatifemetteur}</p>
//                                     </Col>
//                                 </Row>
//                             </fieldset>
//
//                             <fieldset style={{marginTop:'25px'}}>
//                                 <legend>{t('form.legend.infosbeneficiare')}</legend>
//                                 <Row>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.numidentibeneficiare')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.numeroPieceIdentitebeneficiare}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.numidtelbeneficiare')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.numerotelebeneficiare}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.nom')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.nombeneficiare}</p>
//                                     </Col>
//                                 </Row>
//
//                                 <Row>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.prenom')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.prenombeneficiare}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.mail')}:</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.emailbeneficiare}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.ville')}</ControlLabel>
//                                         <p className="detail-value"> {dataForDetail.villebeneficiare}</p>
//                                     </Col>
//                                 </Row>
//
//                                 <Row>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.region')}</ControlLabel>
//                                         <p className="detail-value"> {dataForDetail.regionbeneficiare}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.pays')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.paysbeneficiare}</p>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.indicatif')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.paysindicatifbeneficiare}</p>
//                                     </Col>
//                                 </Row>
//                             </fieldset>
//
//                             <fieldset style={{marginTop:'25px'}}>
//                                 <legend>{t('form.legend.infostransfert')}</legend>
//                                 <Row>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.montanttransfert')}:</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.montantOperation}</p>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.fraienvoi')}:</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.fraisenvois}</p>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.monatntremise')}:</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.montantremise}</p>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.motifRemise')}</ControlLabel>
//                                         <p className="detail-value">{dataForDetail.motif}</p>
//                                     </Col>
//                                 </Row>
//                             </fieldset>
//
//                             <Row className="pull-right">
//                                 <Button bsStyle="primary"
//                                         onClick={() => window.open(baseUrl + 'transfertargentBNIF/createAvisTransfertPDF/' + dataForDetail.id)}>
//                                     <i className="fa fa-file-excel-o" />{t('form.buttons.uploadpdf')}
//                                 </Button>
//
//                                 <Button
//                                     onClick={() => {setView('grid');switchf(); browserHistory.push(baseUrl + 'app/TabeBordChefAgence');}}  bsStyle="primary">
//                                     <i className="fa fa-reply"/> {t('form.buttons.retour')}
//                                 </Button>
//                                 {dataForDetail.statutOperation === "En cours validation" &&
//                                 <Button
//                                     disabled={successSignechefTransfert}
//                                     onClick={() =>{signeTransfertChefAgenceByID(dataForDetail.id)}  } bsStyle="primary"  >
//                                     <i className={'fa ' + (submitting ? 'fa-cog fa-spin' : 'fa fa-check')}/> {t('form.buttons.confirmersigner')}
//                                 </Button>}
//
//                                 {dataForDetail.statutOperation === "En cours paiement" &&
//                                 <Button
//                                     disabled={successSignechefPaiement}
//                                     onClick={() =>{signePaiementChefAgenceByID(dataForDetail.id)}  } bsStyle="primary"  >
//                                     <i className={'fa ' + (submitting ? 'fa-cog fa-spin' : 'fa fa-check')}/> {t('form.buttons.confirmersignerpaiement')}
//                                 </Button>}
//                                 {showAnnuler &&
//                                 <Button  onClick={() => this.setState({showModal: true})} className={styles.ButtonStyle}>
//                                     <i className="fa fa-times"/> {t('form.buttons.annuler')}
//                                 </Button>
//                                 }
//                             </Row>
//                         </form>
//                         </Row>
//                     </div>
//                     :
//
//                     <div>
//                         <Row>
//                             <Col xs="12" md="12">
//                                <h2>{t('form.titleForm.tbchefagence')}</h2>
//                             </Col>
//                         </Row>
//                         <Row>
//                         {successseuilagence === true &&
//                         <Col xs="12" md="12">
//                             <Alert bsStyle="danger">
//                                  {messageSeuilAgence}
//                             </Alert>
//                         </Col>
//                         }
//                          {unsuccessChangingEtat === true && successChangingEtat === false &&
//                          <Col xs="12" md="12">
//                              <Alert bsStyle="danger">
//                                 {t('msg.colotureagencemessage')}
//                              </Alert>
//                          </Col>
//                         }
//                         {miseadispositionAgenceNv === true &&
//                          <Col xs="12" md="12">
//                              <Alert bsStyle="danger">
//                                vous avez des mise a disposition agence non valide
//                              </Alert>
//                          </Col>
//                         }
//
//
//
//                         {isSafeToClose === false &&
//                         <Col xs="12" md="12">
//                         <Alert bsStyle="warning">
//                             {t('msg.colotureagencemessage')}
//                         </Alert>
//                         </Col>
//                         }
//
//                         {unsuccessChangingEtat === true && successChangingEtat === false &&
//                         <Col xs="12" md="12">
//                         <Alert bsStyle="danger">
//                            {t('msg.colotureagencemessageerreur')}
//                         </Alert>
//                         </Col>
//                         }
//
//
//                         {loadingDeleteTransfert === false && successDeleteTransfert === true &&
//                         <Col xs="12" md="12">
//                         <Alert bsStyle="info">
//                             {t('msg.sucesssuppraission')}
//                         </Alert>
//                         </Col>
//                         }
//                         {loadingDeleteTransfert === false && successDeleteTransfert === false &&
//                         <Alert bsStyle="danger">
//                             {t('msg.sucesssuppraission')}
//                         </Alert>
//                         }
//
//                         {loadingCancelTransfert === false && successCancelTransfert === true &&
//                         <Col xs="12" md="12">
//                         <Alert bsStyle="info">
//                             {t('msg.sucessannulation')}
//                         </Alert>
//                         </Col>
//                         }
//                         {loadingCancelTransfert === false && successCancelTransfert === "null" &&
//                         <Col xs="12" md="12">
//                         <Alert bsStyle="danger">
//                             {t('msg.erreurannulation')}
//                         </Alert>
//                         </Col>
//                         }
//                         {loadingCancelTransfert === false && successCancelTransfert === false &&
//                         <Col xs="12" md="12">
//                         <Alert bsStyle="warning">
//                             {t('msg.montantannulationdepasse')}
//                         </Alert>
//                         </Col>
//                         }
//                         </Row>
//
//                         <Row className="detailsBloc">
//                             <Col xs={12} md={3}>
//                                 <ControlLabel>{t('form.label.dateaujourdui')}: </ControlLabel>
//                                 <p className="detail-value">{moment().format('DD/MM/YYYY')}</p>
//                             </Col>
//
//                             <Col xs={12} md={2}>
//                                 <ControlLabel>{t('form.label.agent')} : </ControlLabel>
//                                 <p className="detail-value">{agenceObj.nomAgent}</p>
//                             </Col>
//                             <Col xs={12} md={3}>
//                                 <ControlLabel>{t('form.label.nombrecaisseouvert')} :</ControlLabel>
//                                 <p className="detail-value">{agenceObj.nbrCaisse}</p>
//                             </Col>
//
//                             <Col xs={12} md={2}>
//                                 {statusChangingEtat === "CLOSE" ?
//                                     <Button onClick={this.handleChangeEtat}
//                                             loading={loadingChangeEtatAgnece || loadingAgence}
//                                             bsStyle="primary">
//                                         <span><i
//                                             className="fa fa-folder-open-o"/> {t('form.buttons.ouvertureagence')}</span>
//                                     </Button>
//                                     :
//                                     <Button onClick={this.handleChangeEtat}
//                                             loading={loadingChangeEtatAgnece || loadingAgence}
//                                             bsStyle="danger">
//                                         <span><i
//                                             className="fa fa-lock"/> {t('form.buttons.colotureagence')}</span>
//                                     </Button>
//                                 }
//                             </Col>
//                         </Row>
//
//                         <PanelGroup defaultActiveKey="4" accordion>
//
//                             <Panel header="Situation Agence" eventKey="1" className={styles.accordionPanel} style={panelStyles}>
//                                 <fieldset>
//                                     <Row className={styles.paddingColumn}>
//
//                                         <Col xs={12} md={4}>
//                                                 <ControlLabel> {t('form.label.soldeInitial')} :</ControlLabel>
//                                                 <p className="detail-value">{agenceObj.soldeInitial} XOF</p>
//                                         </Col>
//                                         <Col xs={12} md={4}>
//                                                 <ControlLabel> {t('form.label.soldeactuelprincipal')} :</ControlLabel>
//                                                 <p className="detail-value"> {agenceObj.soldePrincipal} XOF</p>
//                                         </Col>
//                                         <Col xs={12} md={4}>
//                                                 <ControlLabel> {t('form.label.soldeagents')} :</ControlLabel>
//                                                 <p className="detail-value">{agenceObj.soldeAgent} XOF</p>
//                                         </Col>
//                                     </Row>
//                                     <Row className={styles.paddingColumn}>
//
//                                         <Col xs={12} md={4}>
//                                                 <ControlLabel>{t('form.label.soldeactuelglobal')} :</ControlLabel>
//                                                 <p className="detail-value">{agenceObj.soldeGlobal} XOF</p>
//                                         </Col>
//                                         <Col xs={12} md={4}>
//                                                 <ControlLabel>{t('form.label.datemiseajour')}:</ControlLabel>
//                                                 <p className="detail-value"> {agenceObj.dateModif} </p>
//                                         </Col>
//                                     </Row>
//                                 </fieldset>
//                             </Panel>
//
//                             <Panel header="Mouvements des caisses" eventKey="2" className={styles.accordionPanel}
//                                    style={panelStyles}>
//                                 <Row className={styles.paddingColumn}>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> {t('form.label.totalenvoi')} : </ControlLabel>
//                                         <p className="detail-value">{agenceObj.totalEnvois} XOF</p>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>  {t('form.label.totalmiseadisposotion')} : </ControlLabel>
//                                         <p className="detail-value">{agenceObj.totalMiseDispositon} XOF</p>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>  {t('form.label.totalretrait')}: </ControlLabel>
//                                         <p className="detail-value">{agenceObj.totalRetraits} XOF</p>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> {t('form.label.totalattribution')}: </ControlLabel>
//                                         <p className="detail-value">{agenceObj.totalAttributions} XOF</p>
//                                     </Col>
//                                 </Row>
//                             </Panel>
//
//                             {this.state.maxmin1 === true &&
//                             <div className="alert alert-danger">
//                               {t('msg.msgMinMax')}
//                             </div>
//                             }
//                             <Panel header="Détails mouvements des caisses" eventKey="3"
//                                    className={styles.accordionPanel}
//                                    style={panelStyles}>
//
//                                 <form>
//
//                                     <Row className={styles.paddingColumn}>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.agent')} : </ControlLabel>
//                                             <FormControl {...agent} type="text" bsClass={styles.datePickerFormControl}/>
//                                         </Col>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.nomemeteur')} : </ControlLabel>
//                                             <FormControl type="text" {...nomEmetteur}
//                                                          bsClass={styles.datePickerFormControl} placeholder="" min="1"/>
//                                         </Col>
//                                         <Col xs={12} md={2}>
//                                             <ControlLabel>{t('form.label.typeoperation')} : </ControlLabel>
//                                             <FormControl {...typeTransfert} componentClass="select"
//                                                          bsClass={styles.datePickerFormControl} placeholder="select">
//                                                 <option value="" hidden></option>
//                                                 <option key="1" value="CLTOCA">Transfert</option>
//                                                 <option key="2" value="CATOCL">Paiement</option>
//                                                 <option key="3" value="CATOAG">Mise à disposition</option>
//                                                 <option key="4" value="AGTOCA">Attribution</option>
//                                             </FormControl>
//                                         </Col>
//                                         <Col xs={12} md={2}>
//                                             <ControlLabel> {t('form.label.montantoperationmin')}: </ControlLabel>
//                                             <FormControl type="number" {...montantMin}
//                                                          bsClass={styles.datePickerFormControl} placeholder="" min="1"/>
//                                         </Col>
//                                         <Col xs={12} md={2}>
//                                             <ControlLabel>{t('form.label.dateoperationmin')}: </ControlLabel>
//                                             <DatePicker
//                                                 selectsStart
//                                                 selected={this.state.startDate}
//                                                 startDate={this.state.startDate}
//                                                 maxDate={this.state.endDate}
//                                                 onChange={this.handleChangeStart}
//                                                 className={styles.datePickerFormControl}
//                                                 isClearable="true"
//                                                 locale="fr-FR"
//                                                 dateFormat="DD/MM/YYYY"
//                                             />
//                                         </Col>
//                                     </Row>
//
//                                     <Row className={styles.paddingColumn}>
//
//                                         <Col xs={12} md={6}>
//                                             <ControlLabel>{t('form.label.nombeneficiare')}: </ControlLabel>
//                                             <FormControl {...nomBenif} type="text"
//                                                          bsClass={styles.datePickerFormControl}/>
//                                         </Col>
//                                         <Col xs={12} md={2}>
//                                             <ControlLabel>{t('form.label.statusoperation')}: </ControlLabel>
//                                             <FormControl {...statut} type="text"
//                                                          bsClass={styles.datePickerFormControl}/>
//                                         </Col>
//
//                                         <Col xs={12} md={2}>
//                                             <ControlLabel>{t('form.label.montantoperationmax')}: </ControlLabel>
//                                             <FormControl {...montantMax} type="number"
//                                                          bsClass={styles.datePickerFormControl} min="1"/>
//                                         </Col>
//
//                                         <Col xs={12} md={2}>
//                                             <ControlLabel>{t('form.label.dateoperationmax')} :</ControlLabel>
//                                             <DatePicker
//                                                 selectsEnd
//                                                 selected={this.state.endDate}
//                                                 endDate={this.state.endDate}
//                                                 minDate={this.state.startDate}
//                                                 maxDate={moment()}
//                                                 onChange={this.handleChangeEnd}
//                                                 className={styles.datePickerFormControl}
//                                                 isClearable="true"
//                                                 locale="fr-FR"
//                                                 dateFormat="DD/MM/YYYY"
//                                             />
//                                         </Col>
//                                     </Row>
//
//                                     <Row className={styles.paddingColumn}>
//                                         <div className="pull-right">
//                                             <Button
//                                                 loading={loadingListTransfertAgence}
//                                                 bsStyle="primary"
//                                                 onClick={() => {
//                                                     if(montantMin.value>montantMax.value){
//
//                                                         this.setState({
//                                                             maxmin1:true,
//                                                         });
//                                                         window.setTimeout(() => {
//                                                             this.setState({
//                                                                 maxmin1: false
//                                                             });
//                                                         }, 6000);
//                                                     }
//                                                     else{
//                                                     loadListTransfertAgence(userFrontDetails.agenceId, '', values.typeTransfert, values.dateDebut, values.dateFin, values.nomEmetteur, values.nomBenif, values.montantMin, values.montantMax, values.agent, values.statut)
//                                                 }}}
//                                             >
//                                                 <i className="fa fa-search"/>{t('list.search.buttons.search')}
//                                             </Button>
//
//
//                                             <Button
//                                                 bsStyle="primary"
//                                                 onClick={() => {
//                                                     this.setState({startDate: moment(), endDate: moment()});
//                                                     values.codeagence = '';
//                                                     values.typeTransfert = '';
//                                                     values.dateDebut = this.state.startDate;
//                                                     values.dateFin = this.state.endDate;
//                                                     values.nomEmetteur = '';
//                                                     values.nomBenif = '';
//                                                     values.montantMax = '';
//                                                     values.montantMin = '';
//                                                     values.agent = '';
//                                                     values.statut = '';
//                                                     resetForm();
//                                                     loadListTransfertAgence(userFrontDetails.agenceId, '', '', moment(), moment(), '', '', '', '', '', '')
//                                                 }}
//                                             >
//                                                 <i className="fa fa-refresh"/> {t('form.buttons.reinitialiser')}
//                                             </Button>
//                                         </div>
//                                     </Row>
//                                 </form>
//
//                                 <Row className="table-responsive">
//                                         <Griddle
//                                             results={transfetList}
//                                             columnMetadata={gridMetaData}
//                                             useGriddleStyles={false}
//                                             noDataMessage={t('list.search.msg.noResult')}
//                                             resultsPerPage={10}
//                                             nextText={<i className="fa fa-chevron-right"/>}
//                                             previousText={<i className="fa fa-chevron-left"/>}
//                                             tableClassName="table"
//                                             columns={['agent', 'typeTransfert', 'dateTransfert', 'nomEmetteur', 'nomBenif', 'montant', 'frais', 'statut', 'action']}
//                                         />
//                                 </Row>
//                                 <Row className="table-responsive">
//                                 <Button bsStyle="primary"
//                                         onClick={() => window.open(baseUrl + 'agencetransfertargent/generateXLS/' + userFrontDetails.agenceId + '/listtransfert?codeCaisse=' + values.numCaisse + '&typeTransfert=' + values.typeTransfert + '&dateTransfertDebut=' + values.dateDebut + '&dateTransfertFin=' + values.dateFin + '&nomEmetteur=' + values.nomEmetteur + '&numerotelephone='+values.numEmetteur+'&nomBenif=' + values.nomBenif + '&montantMin=' + values.montantMin + '&montantMax=' + values.montantMax + '&agent=' + '' + '&statut=' + values.statut)}>
//                                     <i className="fa fa-file-excel-o" /> liste Transferts
//                                 </Button>
//                                   <Col xs={12} md={4}>
//                                 <div style={{border:'1px solid #e4e4e4', background:'#fff', padding:'10px'}}>
//                                 <span className="icon-layers"/> Total Montant mouveent caisse : {this.props.totalMontant} <b>CFA</b>
//                                 </div>
//                                 </Col>
//
//
//                                 </Row>
//                             </Panel>
//                             {this.state.maxmin2 === true &&
//                                 <div className="alert alert-danger">
//                                     {t('msg.msgMinMax')}
//                                 </div>
//                             }
//                             <Panel header="Details opérations agence" eventKey="4" className={styles.accordionPanel}
//                                    style={panelStyles}>
//                                 <form>
//                                     <Row className={styles.paddingColumn}>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel> {t('form.label.agenceechange')}: </ControlLabel>
//                                             <FormControl {...agenceEchange} type="text"
//                                                          bsClass={styles.datePickerFormControl}/>
//                                         </Col>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.dateemissionmin')} : </ControlLabel>
//                                             <DatePicker
//                                                 selected={this.state.startEmissionDate}
//                                                 selectsStart
//                                                 startDate={this.state.startEmissionDate}
//                                                 maxDate={this.state.endEmissionDate}
//                                                 onChange={this.handleChangeEmissionStart}
//                                                 className={styles.datePickerFormControl}
//                                                 dateFormat='DD/MM/YYYY'
//                                                 isClearable="true"
//                                             />
//                                         </Col>
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.datereceptionmin')} : </ControlLabel>
//                                             <DatePicker
//                                                 selected={this.state.startReceptionDate}
//                                                 selectsStart
//                                                 startDate={this.state.startReceptionDate}
//                                                 maxDate={this.state.endReceptionDate}
//                                                 onChange={this.handleChangeReceptionStart}
//                                                 className={styles.datePickerFormControl}
//                                                 dateFormat='DD/MM/YYYY'
//                                                 isClearable="true"
//                                             />
//                                         </Col>
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.montantoperationmin')} : </ControlLabel>
//                                             <FormControl {...montantOperationDebut} type="number"
//                                                          bsClass={styles.datePickerFormControl} min="1"/>
//                                         </Col>
//
//
//                                     </Row>
//
//                                     <Row className={styles.paddingColumn}>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.typeoperation')} : </ControlLabel>
//                                             <FormControl {...typeTransfert} componentClass="select"
//                                                          bsClass={styles.datePickerFormControl} placeholder="select">
//                                                 <option value="" hidden></option>
//                                                 <option key="1" value="AGTOCAG">Mise à disposition Agence</option>
//                                                 <option key="2" value="CAGTOAG">Encaissment</option>
//                                             </FormControl>
//                                         </Col>
//
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.dateemissionmax')} : </ControlLabel>
//                                             <DatePicker
//                                                 selectsEnd
//                                                 selected={this.state.endEmissionDate}
//                                                 endDate={this.state.endEmissionDate}
//                                                 minDate={this.state.startEmissionDate}
//                                                 maxDate={moment()}
//                                                 onChange={this.handleChangeEmissionEnd}
//                                                 className={styles.datePickerFormControl}
//                                                 isClearable="true"
//                                                 locale="fr-FR"
//                                                 dateFormat="DD/MM/YYYY"
//                                             />
//
//                                         </Col>
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel> {t('form.label.datereceptionmax')} : </ControlLabel>
//                                             <DatePicker
//                                                 selectsEnd
//                                                 selected={this.state.endReceptionDate}
//                                                 endDate={this.state.endReceptionDate}
//                                                 minDate={this.state.startReceptionDate}
//                                                 maxDate={moment()}
//                                                 onChange={this.handleChangeReceptionEnd}
//                                                 className={styles.datePickerFormControl}
//                                                 isClearable="true"
//                                                 locale="fr-FR"
//                                                 dateFormat="DD/MM/YYYY"
//                                             />
//                                         </Col>
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel> {t('form.label.montantoperationmax')}: </ControlLabel>
//                                             <FormControl {...montantOperationFin} type="number"
//                                                          bsClass={styles.datePickerFormControl} min="1"/>
//
//                                         </Col>
//
//
//                                     </Row>
//                                     <Row className={styles.paddingColumn}>
//                                         <div className="pull-right">
//                                             <Button
//                                                 loading={loadingListTransfertAgenceAgence}
//                                                 bsStyle="primary"
//                                                 onClick={() => {
//
//                                                     if(montantOperationDebut.value>montantOperationFin.value){
//
//                                                         this.setState({
//                                                             maxmin2:true,
//                                                         });
//                                                         window.setTimeout(() => {
//                                                             this.setState({
//                                                                 maxmin2: false
//                                                             });
//                                                         }, 6000);
//                                                     }
//                                                     else{
//                                                         loadListAgncAgncTransfertAgence(userFrontDetails.agenceId, values.typeTransfert, values.agenceEchange, values.dateReceptionDebut, values.dateReceptionFin, values.dateEmissionDebut, values.dateEmissionFin, values.montantOperationDebut, values.montantOperationFin)
//                                                     }
//
//                                                 }}
//                                             >
//                                                 <i className="fa fa-search"/>Rechercher
//                                             </Button>
//                                             <Button
//                                                 bsStyle="primary"
//                                                 onClick={() => {
//                                                     this.setState({
//                                                         startEmissionDate: moment(),
//                                                         endEmissionDate: moment(),
//                                                         startReceptionDate: '',
//                                                         endReceptionDate: ''
//                                                     });
//                                                     values.agenceEchange = '';
//                                                     values.montantOperationDebut = '';
//                                                     values.montantOperationFin = '';
//                                                     values.dateReceptionDebut = this.state.startReceptionDate;
//                                                     values.dateReceptionFin = this.state.endReceptionDate;
//                                                     values.dateEmissionDebut = this.state.startEmissionDate;
//                                                     values.dateEmissionFin = this.state.endEmissionDate;
//                                                     values.typeTransfert = '';
//                                                     resetForm();
//                                                     loadListAgncAgncTransfertAgence(userFrontDetails.agenceId, '', '', '', '', moment(), moment(), '', '')
//                                                 }}
//
//                                             >
//                                                 <i className="fa fa-refresh"/>{t('form.buttons.reinitialiser')}
//                                             </Button>
//                                         </div>
//                                     </Row>
//                                 </form>
//                                 <Row className="table-responsive">
//                                         <Griddle
//                                             results={transfetAgncAgncList}
//                                             useGriddleStyles={false}
//                                             noDataMessage={t('list.search.msg.noResult')}
//                                             resultsPerPage={10}
//                                             nextText={<i className="fa fa-chevron-right"/>}
//                                             previousText={<i className="fa fa-chevron-left"/>}
//                                             tableClassName="table"
//                                             columns={['agenceEmitteur', 'agenceRecepteur', 'dateEmission', 'dateReception', 'montantOperation', 'statutOperation','action']}
//
//                                             columnMetadata={gridMetaDataAgncAgnc}
//                                         />
//                                 </Row>
//                                 <Row className="table-responsive">
//                                 <Button bsStyle="primary"
//                                         onClick={() => window.open(baseUrl + 'agencetransfertargent/generateXLS/' + userFrontDetails.agenceId + '/listagenceagencetransfert?typeTransfert=' + values.typeTransfert + '&agenceEchange=' + values.agenceEchange + '&dateReceptionDebut=' + values.dateReceptionDebut + '&dateReceptionFin=' +  values.dateReceptionFin+'&dateEmissionDebut='+values.dateEmissionDebut+'&dateEmissionFin='+values.dateEmissionFin+'&montantOperationDebut='+values.montantOperationDebut+'&montantOperationFin='+values.montantOperationFin)}>
//                                     <i className="fa fa-file-excel-o" /> liste Transferts
//                                 </Button>
//                                 <Col xs={12} md={2}>
//                                 <div style={{border:'1px solid #e4e4e4', background:'#fff', padding:'10px'}}>
//                                 <span className="icon-layers"/> Total Montant : {this.props.totalMontantTransfertAgence} <b>CFA</b>
//                                 </div>
//                                 </Col>
//                                 </Row>
//
//                             </Panel>
//
//                             {this.state.maxmin3 === true &&
//                             <div className="alert alert-danger">
//                                 {t('msg.msgMinMax')}
//                             </div>
//                             }
//                             <Panel header="Les mouvements d'ouverture cloture de caisses " eventKey="7"
//                                    className={styles.accordionPanel}
//                                    style={panelStyles}>
//                                 <form>
//                                     <Row className={styles.paddingColumn}>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel> Numero de Caisse : </ControlLabel>
//                                             <FormControl {...numcaisse} type="text"
//                                                          bsClass={styles.datePickerFormControl}/>
//                                         </Col>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>Date Min : </ControlLabel>
//                                             <DatePicker
//                                                 selected={this.state.startActionDate}
//                                                 selectsStart
//                                                 startDate={this.state.startActionDate}
//                                                 maxDate={this.state.endActionDate}
//                                                 onChange={this.handleChangeActionStart}
//                                                 className={styles.datePickerFormControl}
//                                                 dateFormat='DD/MM/YYYY'
//                                                 isClearable="true"
//                                             />
//                                         </Col>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel> Montant Initial : </ControlLabel>
//                                             <FormControl {...montantinitial} type="number"
//                                                          bsClass={styles.datePickerFormControl} min="1"/>
//                                         </Col>
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>Heure Debut : </ControlLabel>
//                                             <FormControl {...heuremin} type="number"
//                                                          bsClass={styles.datePickerFormControl} min="1"/>
//                                         </Col>
//
//
//                                     </Row>
//
//                                     <Row className={styles.paddingColumn}>
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel> Agent : </ControlLabel>
//                                             <FormControl {...agentaction} type="text"
//                                                          bsClass={styles.datePickerFormControl}/>
//                                         </Col>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>Date Max : </ControlLabel>
//                                             <DatePicker
//                                                 selectsEnd
//                                                 selected={this.state.endActionDate}
//                                                 endDate={this.state.endActionDate}
//                                                 minDate={this.state.startActionDate}
//                                                 maxDate={moment()}
//                                                 onChange={this.handleChangeReceptionEnd}
//                                                 className={styles.datePickerFormControl}
//                                                 isClearable="true"
//                                                 locale="fr-FR"
//                                                 dateFormat="DD/MM/YYYY"
//                                             />
//                                         </Col>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel> Montant Final : </ControlLabel>
//                                             <FormControl {...montantfinal} type="number"
//                                                          bsClass={styles.datePickerFormControl} min="1"/>
//                                         </Col>
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel> Heure Clôture : </ControlLabel>
//                                             <FormControl {...heuremax} type="number"
//                                                          bsClass={styles.datePickerFormControl} min="1"/>
//
//                                         </Col>
//
//
//                                     </Row>
//                                     <Row className={styles.paddingColumn}>
//                                         <div className="pull-right">
//                                             <Button
//                                                 loading={loadingActionList}
//                                                 bsStyle="primary"
//                                                 onClick={() => {
//                                                     if(montantinitial.value>montantfinal.value){
//
//                                                         this.setState({
//                                                             maxmin3:true,
//                                                         });
//                                                         window.setTimeout(() => {
//                                                             this.setState({
//                                                                 maxmin3: false
//                                                             });
//                                                         }, 6000);
//                                                     }
//                                                     else{
//                                                     loadListOuvertureClotureCaisses(userFrontDetails.agenceId, values.numcaisse, values.agentaction, values.montantinitial, values.montantfinal, values.dateActionDebut, values.dateActionFin, values.heuremin, values.heuremax);
//                                                 }}}
//                                             >
//                                                 <i className="fa fa-search"/>Rechercher
//                                             </Button>
//                                             <Button
//                                                 bsStyle="primary"
//                                                 onClick={() => {
//                                                     this.setState({
//                                                         startActionDate: moment(),
//                                                         endActionDate: moment(),
//                                                     });
//
//                                                     resetForm();
//                                                     loadListOuvertureClotureCaisses(userFrontDetails.agenceId, '', '', '', '', moment(), moment(), '', '');
//                                                 }}
//
//                                             >
//                                                 <i className="fa fa-refresh"/>{t('form.buttons.reinitialiser')}
//                                             </Button>
//                                         </div>
//                                     </Row>
//                                 </form>
//                                 <Row className="table-responsive">
//                                         <Griddle
//                                             results={actionList}
//                                             useGriddleStyles={false}
//                                             noDataMessage={t('list.search.msg.noResult')}
//                                             resultsPerPage={10}
//                                             nextText={<i className="fa fa-chevron-right"/>}
//                                             previousText={<i className="fa fa-chevron-left"/>}
//                                             tableClassName="table"
//                                             columns={['numcaisse','agentaction','montantinitial','montantfinal','dateaction','heuredebut','heurefin']}
//
//                                             columnMetadata={actionGridMetaData}
//                                         />
//                                 </Row>
//                                 <Row className="table-responsive">
//                                 <Button bsStyle="primary"
//
//                                   onClick={() => window.open(baseUrl + 'agencetransfertargent/generateCSV/' + userFrontDetails.agenceId + '/listouvetturecloture?numcaisse=' + values.numcaisse + '&agentaction=' + values.agentaction + '&montantinitial=' + values.montantinitial + '&montantfinal=' + values.montantfinal + '&dateActionDebut=' + values.dateActionDebut + '&dateActionFin=' + values.dateActionFin + '&heuredebut=' + values.heuremin + '&heurefin=' + values.heuremax)}>
//                                     <i className="fa fa-file-excel-o" /> liste ouvertures clotures
//                                 </Button>
//
//
//                                 </Row>
//
//                             </Panel>
//                         </PanelGroup>
//
//
//                     </div>
//                 }
//                 <br></br>
//                 <br></br>
//
//
//             </div>
//         );
//     }
//
//
// }
