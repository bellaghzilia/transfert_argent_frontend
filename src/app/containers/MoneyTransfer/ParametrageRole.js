import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Button,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  PanelGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./ParametrageReducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import ParametrageRoleValidator from "./Validateur/ParametrageRoleValidator";
import { AuthorizedComponent } from "react-router-role-authorization";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

@connect(
  (state) => ({
    listRole: state.ParametrageReducer.listRole,
    successinstancerole: state.ParametrageReducer.successinstancerole,
    dataForDetailshema: state.ParametrageReducer.dataForDetailshema,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getInstanceRole: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
    showBouttonSupprimer: PropTypes.func,
    deleteTransfertCltCltByID: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    annulerTransfertCltCltByID: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      showModal: false,
      updating: false,
      deleting: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick() {
    try {
      if (this.state.deleting) {
        await this.props.deleteuser(this.props.rowData.id);
        this.props.loadListRoleUser();
      } else if (this.state.updating) {
        await this.props.getInstanceRole(this.props.rowData.id);
      }
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      saveRoleUser,
      listRole,
      loadListRegion,
      loadListAgence,
      loadListRoleUser,
      updateRoleUser,
      lisRegions,
      getInstanceRole,
      deleteuser,
      t,
      successsupdate,
      resetAlert,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
    };

    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: true,
                showModal: true,
                deleting: false,
              })
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>
          <Button
            bsSize="small"
            bsStyle="danger"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                deleting: true,
                showModal: true,
                updating: false,
              })
            }
          >
            <i className="fa fa-trash-o fa-fw" />
          </Button>
        </ButtonGroup>

        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {this.state.deleting && <div>{t("popup.supression.title")}</div>}
              {this.state.updating && (
                <div>{t("popup.modification.title")}</div>
              )}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.deleting && <div>{t("popup.supression.msguser")}</div>}
            {this.state.updating && (
              <div>{t("popup.modification.msguser")}</div>
            )}
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.supression.noBtn")}
              </Button>
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={this.handleClick}
              >
                {t("popup.supression.yesBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.loadListRoleUser()),
        dispatch(MoneyTransferAction.loadListAgence()),
      ]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",

    fields: [
      "firstName",
      "lastName",
      "userName",
      "password",
      "montantremisemax",
      "tauxremise",
      "collaborateur",
      "superAdmin",
      "admin",
      "chefAgence",
      "chefRegion",
      "idAgence",
    ],
    validate: ParametrageRoleValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    state.ParametrageReducer.dataForDetailrole &&
    state.ParametrageReducer.dataForDetailrole !== null &&
    state.ParametrageReducer.dataForDetailrole !== undefined
      ? {
          initialValues: state.ParametrageReducer.dataForDetailrole,
        }
      : {
          initialValues: {
            firstName: "",
            lastName: "",
            userName: "",
            password: "",
            idAgence: "",
            nomAgence: "",
            admin: false,
            collaborateur: false,
            superAdmin: false,
            chefRegion: false,
            chefAgence: false,
            montantremisemax: "",
            tauxremise: "",
          },
        }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    saveRoleUser: state.ParametrageReducer.saveRoleUser,
    deleteuser: state.ParametrageReducer.deleteuser,
    updateRoleUser: state.ParametrageReducer.updateRoleUser,
    transfetList: state.moneyTransfer.transfetList,
    etatCaisse: state.moneyTransfer.etatCaisse,
    successinstanceshema: state.ParametrageReducer.successinstanceshema,
    successinstancerole: state.ParametrageReducer.successinstancerole,
    listCaisse: state.moneyTransfer.listCaisse,
    caisseDetails: state.moneyTransfer.caisseDetails,
    listRole: state.ParametrageReducer.listRole,
    lisRegions: state.ParametrageReducer.lisRegions,
    loadListRoleUser: state.ParametrageReducer.loadListRoleUser,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
    dataForDetailrole: state.ParametrageReducer.dataForDetailrole,
    roles: state.ParametrageReducer.roles,
    lisAgence: state.ParametrageReducer.lisAgence,
    getInstanceRole: state.moneyTransfer.getInstanceRole,
    libelle: "",
    libellepays: "",
    nomcollaborateur: "",
    successsupdate: state.ParametrageReducer.successsupdate,
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class ParametrageRole extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      checked: false,
      admin: false,
      collaborateur: false,
      superAdmin: false,
      chefRegion: false,
      chefAgence: false,
      enabled: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
  }

  async onSubmit(values) {
    try {
      await this.props.saveRoleUser(values);
      this.props.loadListRoleUser();
      this.props.switchf();
    } catch (error) {
      console.log(error.message);
    }
  }

  async onSubmitTWO(values, id) {
    try {
      await this.props.updateRoleUser(values, id);
      this.props.loadListRoleUser();
      this.props.switchf();
      setTimeout(() => {
        this.props.resetAlert();
      }, 2000);
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    const {
      fields: {
        firstName,
        lastName,
        userName,
        password,
        montantremisemax,
        tauxremise,
        collaborateur,
        superAdmin,
        admin,
        chefAgence,
        chefRegion,
        idAgence,
      },
      handleSubmit,
      switchf,
      successinstancerole,
      dataForDetailrole,
      roles,
      resetForm,
      updating,
      deleting,
      saveRoleUser,
      userFrontDetails,
      loadListRoleUser,
      deleteuser,
      lisAgence,
      listType,
      updateRoleUser,
      caisseDetails,
      openCaisse,
      successOpenCaisse,
      lisRegions,
      listRole,
      etatCaisse,
      getListCaisseByAgence,
      getInstanceRole,
      values,
      listCaisse,
      t,
      successsupdate,
      resetAlert,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };

    const gridMetaData = [
      {
        columnName: "firstName",
        displayName: t("list.cols.nom"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "lastName",
        displayName: t("list.cols.prenom"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "userName",
        displayName: t("list.cols.userName"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "nomAgence",
        displayName: t("list.cols.agence"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "role",
        displayName: t("list.cols.role"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.legend.parametragerole")}</h2>
          </Col>
        </Row>
        <Row>
          {successsupdate === true && (
            <Col xs="12" md="12">
              <Alert bsStyle="success">
                {t("form.legend.modificationSucces")}
              </Alert>
            </Col>
          )}
        </Row>
        <Row>
          <form className="formContainer">
            <fieldset style={{ marginBottom: "20px" }}>
              <Row className={styles.paddingColumn}>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.nom")}
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...firstName}
                  />
                  {firstName.error && firstName.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {firstName.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.prenom")}
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...lastName}
                  />
                  {lastName.error && lastName.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {lastName.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.username")}
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...userName}
                  />
                  {userName.error && userName.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {userName.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.password")}
                  </ControlLabel>
                  <FormControl
                    type="password"
                    className={styles.datePickerFormControl}
                    {...password}
                  />
                  {password.error && password.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {password.error}
                      </i>
                    </div>
                  )}
                </Col>
              </Row>

              <Row className={styles.paddingColumn}>
                <Col xs="12" md="4">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.montantremisemax")}
                  </ControlLabel>
                  <FormControl
                    type="number"
                    className={styles.datePickerFormControl}
                    {...montantremisemax}
                  />
                  {montantremisemax.error && montantremisemax.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {montantremisemax.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="4">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.tauxmax")}
                  </ControlLabel>
                  <FormControl
                    type="number"
                    className={styles.datePickerFormControl}
                    {...tauxremise}
                  />
                  {tauxremise.error && tauxremise.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {tauxremise.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="4">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.agence")}{" "}
                  </ControlLabel>
                  <FormControl
                    componentClass="select"
                    className={styles.datePickerFormControl}
                    placeholder="select"
                    {...idAgence}
                  >
                    <option value="" hidden>
                      Selectionné une agence
                    </option>
                    {lisAgence &&
                      lisAgence.length !== 0 &&
                      lisAgence.map((agence) => (
                        <option value={agence.id}> {agence.nomAgence}</option>
                      ))}
                  </FormControl>
                </Col>
              </Row>
              <Row
                className={styles.paddingColumn}
                style={{ paddingTop: "10px" }}
              >
                <Col xs="12" md="2">
                  <input type="checkbox" {...collaborateur} /> Collaborateur
                </Col>
                <Col xs="12" md="2">
                  <input type="checkbox" {...admin} /> Admin
                </Col>
                <Col xs="12" md="2">
                  <input type="checkbox" {...superAdmin} /> Super Admin
                </Col>
                <Col xs="12" md="2">
                  <input type="checkbox" {...chefAgence} /> Chef Agence
                </Col>
                <Col xs="12" md="2">
                  <input type="checkbox" {...chefRegion} /> Chef Region
                </Col>

                <Col xs="12" md="1">
                  {successinstancerole ? (
                    <Button
                      style={{ marginLeft: "-20px" }}
                      bsStyle="primary"
                      onClick={handleSubmit((e) => {
                        this.onSubmitTWO(values, dataForDetailrole.id);
                        resetForm();
                      })}
                    >
                      <i className="fa fa-check " />
                      {t("form.buttons.modifier")}
                    </Button>
                  ) : (
                    <Button
                      style={{ marginLeft: "-20px" }}
                      bsStyle="primary"
                      onClick={handleSubmit((e) => {
                        this.onSubmit(values);
                        resetForm();
                      })}
                    >
                      <i className="fa fa-check " />
                      {t("form.buttons.ajouter")}
                    </Button>
                  )}
                </Col>

                <Col xs="12" md="1">
                  <Button
                    style={{ marginLeft: "-20px" }}
                    bsStyle="primary"
                    onClick={() => {
                      resetForm();
                      switchf();
                    }}
                  >
                    <i className="fa fa-times" /> {t("form.buttons.annuler")}
                  </Button>
                </Col>
              </Row>
            </fieldset>

            <fieldset>
              <legend>{t("form.legend.listerole")}</legend>
              <Row className="table-responsive">
                <Griddle
                  results={listRole}
                  columnMetadata={gridMetaData}
                  useGriddleStyles={false}
                  noDataMessage={"Aucun résultat trouvé"}
                  resultsPerPage={10}
                  nextText={<i className="fa fa-chevron-right" />}
                  previousText={<i className="fa fa-chevron-left" />}
                  tableClassName="table"
                  columns={[
                    "firstName",
                    "lastName",
                    "userName",
                    "nomAgence",
                    "roles",
                    "action",
                  ]}
                />
              </Row>
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
