import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Button,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  PanelGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./ParametrageReducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import ParametrageFraisValidator from "./Validateur/ParametrageFraisValidator";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

@connect(
  (state) => ({
    listCommission: state.ParametrageReducer.listCommission,
    successinstancecommission:
      state.ParametrageReducer.successinstancecommission,
    dataForDetailshema: state.ParametrageReducer.dataForDetailshema,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getInstanceCommission: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
    showBouttonSupprimer: PropTypes.func,
    deleteTransfertCltCltByID: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    annulerTransfertCltCltByID: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      showModal: false,
      updating: false,
      deleting: false,
      startDate: "",
      endDate: "",
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick() {
    try {
      if (this.state.deleting) {
        await this.props.deletcommission(this.props.rowData.id);
        this.props.loadListCommission();
      } else if (this.state.updating) {
        await this.props.getInstanceCommission(this.props.rowData.id);

        this.props.loadListCommission();
      }
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      savefrais,
      chevauchementdate,
      intervaldateexceeded,
      chevauchementMontant,
      listCommission,
      loadListRegion,
      loadListCommission,
      updatefrais,
      lisRegions,
      getInstanceCommission,
      deletcommission,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
    };
    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: true,
                showModal: true,
                deleting: false,
              })
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>
          <Button
            bsSize="small"
            bsStyle="danger"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                deleting: true,
                showModal: true,
                updating: false,
              })
            }
          >
            <i className="fa fa-trash-o fa-fw" />
          </Button>
        </ButtonGroup>

        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {this.state.deleting && <div>{t("popup.supression.title")}</div>}
              {this.state.updating && (
                <div>{t("popup.modification.title")}</div>
              )}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.deleting && <div>{t("popup.supression.msg")}</div>}
            {this.state.updating && <div>{t("popup.modification.msg")}</div>}
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.supression.noBtn")}
              </Button>
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={this.handleClick}
              >
                {t("popup.supression.yesBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.loadListtypeoperation()),
        dispatch(MoneyTransferAction.loadListCommission()),
        dispatch(MoneyTransferAction.loadListRegion()),
      ]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",

    fields: [
      "montantMin",
      "montantMax",
      "dateMin",
      "dateMax",
      "typefrai",
      "valeurfrai",
      "action",
    ],
    validate: ParametrageFraisValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    (state.ParametrageReducer.dataForDetailcommission &&
      state.ParametrageReducer.dataForDetailcommission !== null &&
      state.ParametrageReducer.dataForDetailcommission !== undefined && {
        initialValues: {
          montantMin:
            state.ParametrageReducer.dataForDetailcommission.montantMin,
          montantMax:
            state.ParametrageReducer.dataForDetailcommission.montantMax,
          dateMin: state.ParametrageReducer.dataForDetailcommission.dateMinimin,
          dateMax: state.ParametrageReducer.dataForDetailcommission.dateMaximin,
          typefrai: state.ParametrageReducer.dataForDetailcommission.typefrai,
          valeurfrai:
            state.ParametrageReducer.dataForDetailcommission.valeurfrai,
        },
      }: {
      initialValues: {
        montantMin: "",
        montantMax: "",
        dateMin: "",
        dateMax: "",
        typefrai: "",
        valeurfrai: "",
      },
    })
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    savefrais: state.ParametrageReducer.savefrais,
    chevauchementdate: state.ParametrageReducer.chevauchementdate,
    intervaldateexceeded: state.ParametrageReducer.intervaldateexceeded,
    chevauchementdatesuces: state.ParametrageReducer.chevauchementdatesuces,
    chevauchementMontant: state.ParametrageReducer.chevauchementMontant,
    chevauchementmontantsucces:
      state.ParametrageReducer.chevauchementmontantsucces,
    successfrais: state.ParametrageReducer.successfrais,
    deletcommission: state.ParametrageReducer.deletcommission,
    updatefrais: state.ParametrageReducer.updatefrais,
    transfetList: state.moneyTransfer.transfetList,
    etatCaisse: state.moneyTransfer.etatCaisse,
    successinstanceshema: state.ParametrageReducer.successinstanceshema,
    successinstancecommission:
      state.ParametrageReducer.successinstancecommission,
    listCaisse: state.moneyTransfer.listCaisse,
    caisseDetails: state.moneyTransfer.caisseDetails,
    listCommission: state.ParametrageReducer.listCommission,
    lisRegions: state.ParametrageReducer.lisRegions,
    loadListCommission: state.ParametrageReducer.loadListCommission,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
    dataForDetailcommission: state.ParametrageReducer.dataForDetailcommission,
    getInstanceCommission: state.moneyTransfer.getInstanceCommission,
    libelle: "",
    libellepays: "",
    nomcollaborateur: "",
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class ParametrageFraisUpdate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dateMin: moment(props.values.dateMin, "DD/MM/YYYY"),
      dateMax: moment(props.values.dateMax, "DD/MM/YYYY"),
    };

    this.handleChangeStart = this.handleChangeStart.bind(this);
    this.handleChangeEnd = this.handleChangeEnd.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
    this.formatDate = this.formatDate.bind(this);
    this.showdateModifier = this.showdateModifier.bind(this);
  }
  formatDate(dateToFormat) {
    const res = dateToFormat.split("-");
    const formatedDate = res[2] + "-" + res[1] + "-" + res[0];
    return formatedDate;
  }
  showdateModifier(date) {
    const date1 =
      this.props.dataForDetailcommission != undefined &&
      moment(this.formatDate(date));
    return date1;
  }

  handleChangeStart(date) {
    this.setState({ dateMin: date });
    this.props.values.dateMin = date.format("DD/MM/YYYY");
  }

  handleChangeEnd(date) {
    this.setState({ dateMax: date });
    this.props.values.dateMin = date.format("DD/MM/YYYY");
  }

  async onSubmitTWO(values, id) {
    try {
      await this.props.updatefrais(values, id);
      this.props.loadListCommission();
      browserHistory.push(baseUrl + "app/ParametrageFrais");
      this.props.switchf();
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    const {
      fields: {
        montantMin,
        montantMax,
        dateMin,
        dateMax,
        typefrai,
        valeurfrai,
        action,
      },
      handleSubmit,
      chevauchementmontant,
      chevauchementdatesuces,
      chevauchementmontantsucces,
      switchf,
      successinstancecommission,
      dataForDetailcommission,
      resetForm,
      updating,
      deleting,
      savefrais,
      chevauchementMontant,
      intervaldateexceeded,
      chevauchementdate,
      loadListCommission,
      deletcommission,
      listType,
      updatefrais,
      caisseDetails,
      openCaisse,
      successOpenCaisse,
      lisRegions,
      listCommission,
      etatCaisse,
      getListCaisseByAgence,
      getInstanceCommission,
      values,
      listCaisse,
      successfrais,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const gridMetaData = [
      {
        columnName: "montantMin",
        displayName: t("list.cols.montantMin"),

        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "montantMax",
        displayName: t("list.cols.montantMax"),
        customHeaderComponent: HeaderComponent,
      },

      {
        columnName: "dateMinimin",
        displayName: t("list.cols.dateMinimin"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "dateMaximin",
        displayName: t("list.cols.dateMaximin"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "typefrai",
        displayName: t("list.cols.typefrai"),
        customHeaderComponent: HeaderComponent,
      },

      {
        columnName: "valeurfrai",
        displayName: t("list.cols.valeurfrai"),
        customHeaderComponent: HeaderComponent,
      },

      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];

    values.dateMin = this.state.dateMin;
    values.dateMax = this.state.dateMax;

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.legend.parametragefrai")}</h2>
          </Col>
        </Row>
        <Row className={styles.compteCard}>
          {chevauchementmontantsucces && chevauchementdatesuces && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{t("msg.chevechment")}</Alert>
            </Col>
          )}
          <form className="formContainer">
            <fieldset>
              {successinstancecommission && (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.montantMin")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...montantMin}
                      />
                      {montantMin.error && montantMin.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {montantMin.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.montantMax")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...montantMax}
                      />
                      {montantMax.error && montantMax.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {montantMax.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.dateMin")}
                      </ControlLabel>
                      <DatePicker
                        selectsStart
                        selected={this.state.dateMin}
                        dateMin={this.state.dateMin}
                        onChange={this.handleChangeStart}
                        className={styles.datePickerFormControl}
                        dateFormat="DD/MM/YYYY"
                        isClearable="true"
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.dateMax")}{" "}
                      </ControlLabel>
                      <DatePicker
                        selectsEnd
                        selected={this.state.dateMax}
                        dateMax={this.state.dateMax}
                        onChange={this.handleChangeEnd}
                        className={styles.datePickerFormControl}
                        dateFormat="DD/MM/YYYY"
                        isClearable="true"
                      />
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.typefrai")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...typefrai}
                        placeholder={dataForDetailcommission.typefrai}
                      />
                      {typefrai.error && typefrai.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {typefrai.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.valeurfrai")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...valeurfrai}
                        placeholder={dataForDetailcommission.valeurfrai}
                      />
                      {valeurfrai.error && valeurfrai.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {valeurfrai.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmitTWO(values, dataForDetailcommission.id);
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.modifier")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          values.montantMin = "";
                          values.montantMax = "";
                          values.typefrai = "";
                          values.valeurfrai = "";
                          this.setState({
                            dateMin: "",
                            dateMax: "",
                          });
                          this.props.switchf();
                          browserHistory.push(baseUrl + "app/ParametrageFrais");
                          switchf();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              )}
            </fieldset>
            <legend>{t("form.legend.listefrai")}</legend>
            <fieldset>
              <Griddle
                results={listCommission}
                columnMetadata={gridMetaData}
                useGriddleStyles={false}
                noDataMessage={t("list.search.msg.noResult")}
                resultsPerPage={10}
                nextText={<i className="fa fa-chevron-right" />}
                previousText={<i className="fa fa-chevron-left" />}
                tableClassName="table"
                columns={[
                  "montantMin",
                  "montantMax",
                  "dateMinimin",
                  "dateMaximin",
                  "typefrai",
                  "valeurfrai",
                  "action",
                ]}
              />
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
