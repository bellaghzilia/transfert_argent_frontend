import React, { Component } from "react";
import { Col, ControlLabel, FormControl, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { initializeWithKey, reduxForm } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import * as MoneyTransferAction from "./moneytransferreducer";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import Button from "react-bootstrap-button-loader";
import moment from "moment";
import DatePicker from "react-datepicker";
import { AuthorizedComponent } from "react-router-role-authorization";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

class CenterComponent extends Component {
  render() {
    const url = this.props.data;
    return <div>{this.props.data}</div>;
  }
}

@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.loadListCompteComptable()),
      ]);
    },
  },
])
@reduxForm({
  form: "compteComptable",
  fields: [
    "numCompte",
    "typeCompte",
    "dateDebut",
    "dateFin",
    "soldeMin",
    "soldeMax",
  ],
  destroyOnUnmount: true,
  initialValues: {
    numCompte: "",
    typeCompte: "",
    dateDebut: "",
    dateFin: "",
    soldeMin: "",
    soldeMax: "",
  },
})
@connect(
  (state) => ({
    listCompteCompta: state.moneyTransfer.listCompteCompta,
    loadingCompteComptaList: state.moneyTransfer.loadingCompteComptaList,
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["MoneyTransfer"], { wait: true })
export default class CompteComptable extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";

    this.state = {
      startDate: "",
      endDate: "",
    };
    this.handlAnnuler = this.handlAnnuler.bind(this);
    this.handleChangeEnd = this.handleChangeEnd.bind(this);
    this.handleChangeStart = this.handleChangeStart.bind(this);
    this.getTotal = this.getTotal.bind(this);
  }

  handleChangeStart(date) {
    this.setState({ startDate: date });
  }

  handleChangeEnd(date) {
    this.setState({ endDate: date });
  }

  handlAnnuler(event) {
    this.setState({ libelle: "" });
  }

  getTotal(list) {
    let tot = 0;
    list.forEach(function(element) {
      tot = Number(tot) + Number(element.solde);
    });
    return tot;
  }

  render() {
    const {
      fields: { numCompte, typeCompte, dateDebut, dateFin, soldeMin, soldeMax },
      values,
      userFrontDetails,
      listCompteCompta,
      loadListCompteComptable,
      loadingCompteComptaList,
      resetForm,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const gridMetaData = [
      {
        columnName: "numCompte",
        displayName: t("list.cols.numCompte"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "typeCompte",
        displayName: t("list.cols.typeCompte"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "solde",
        displayName: t("list.cols.solde"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "dateMiseajour",
        displayName: t("list.cols.dateMiseajour"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
    ];
    values.dateDebut = this.state.startDate;
    values.dateFin = this.state.endDate;

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.titleForm.comptecomptable")}</h2>
          </Col>
        </Row>
        <Row className="searchBloc">
          <form>
            <Row className={styles.paddingColumn}>
              <Col xs={12} md={4}>
                <ControlLabel>{t("form.label.numCompte")} : </ControlLabel>
                <FormControl
                  {...numCompte}
                  type="text"
                  bsClass={styles.datePickerFormControl}
                />
              </Col>
              <Col xs={12} md={4}>
                <ControlLabel>{t("form.label.soldemin")}: </ControlLabel>
                <FormControl
                  type="number"
                  {...soldeMin}
                  bsClass={styles.datePickerFormControl}
                  placeholder=""
                  min="1"
                />
              </Col>
              <Col xs={12} md={4}>
                <ControlLabel>
                  {t("form.label.datemiseajourmin")}:{" "}
                </ControlLabel>
                <DatePicker
                  selectsStart
                  selected={this.state.startDate}
                  startDate={this.state.startDate}
                  maxDate={this.state.endDate}
                  onChange={this.handleChangeStart}
                  className={styles.datePickerFormControl}
                  isClearable="true"
                  locale="fr-FR"
                  dateFormat="DD/MM/YYYY"
                />
              </Col>
            </Row>

            <Row className={styles.paddingColumn}>
              <Col xs={12} md={4}>
                <ControlLabel>{t("form.label.typeCompte")} : </ControlLabel>
                <FormControl
                  type="text"
                  {...typeCompte}
                  bsClass={styles.datePickerFormControl}
                  placeholder=""
                />
              </Col>
              <Col xs={12} md={4}>
                <ControlLabel>{t("form.label.soldemax")} : </ControlLabel>
                <FormControl
                  {...soldeMax}
                  type="number"
                  bsClass={styles.datePickerFormControl}
                  min="1"
                />
              </Col>
              <Col xs={12} md={4}>
                <ControlLabel>
                  {t("form.label.datemiseajourmax")} :{" "}
                </ControlLabel>
                <DatePicker
                  selectsEnd
                  selected={this.state.endDate}
                  endDate={this.state.endDate}
                  minDate={this.state.startDate}
                  maxDate={moment()}
                  onChange={this.handleChangeEnd}
                  className={styles.datePickerFormControl}
                  isClearable="true"
                  locale="fr-FR"
                  dateFormat="DD/MM/YYYY"
                />
              </Col>
            </Row>

            <Row className={styles.paddingColumn}>
              <div className="pull-right">
                <Button
                  loading={loadingCompteComptaList}
                  bsStyle="primary"
                  onClick={() => {
                    loadListCompteComptable(
                      values.numCompte,
                      values.typeCompte,
                      values.dateDebut,
                      values.dateFin,
                      values.soldeMin,
                      values.soldeMax
                    );
                  }}
                >
                  <i className="fa fa-search" />
                  Rechercher
                </Button>
                <Button
                  bsStyle="primary"
                  onClick={() => {
                    //this.setState({startDate: moment(), endDate: moment()});
                    values.numCompte = "";
                    values.typeCompte = "";
                    values.dateDebut = this.state.startDate;
                    values.dateFin = this.state.endDate;
                    values.soldeMin = "";
                    values.soldeMax = "";
                    resetForm();
                    loadListCompteComptable("", "", "", "", "", "");
                  }}
                >
                  <i className="fa fa-refresh" /> Réinitialiser
                </Button>
              </div>
            </Row>
          </form>
        </Row>

        <Row className="table-responsive tableContainer">
          <Griddle
            results={listCompteCompta}
            columnMetadata={gridMetaData}
            useGriddleStyles={false}
            noDataMessage={t("list.search.msg.noResult")}
            resultsPerPage={10}
            nextText={<i className="fa fa-chevron-right" />}
            previousText={<i className="fa fa-chevron-left" />}
            tableClassName="table"
            columns={["numCompte", "typeCompte", "solde", "dateMiseajour"]}
          />
        </Row>
        <Row>
          <Col xs={12} md={12}>
            <div
              style={{
                border: "1px solid #e4e4e4",
                background: "#fff",
                padding: "10px",
              }}
            >
              <span className="icon-layers" /> Total :{" "}
              {this.getTotal(listCompteCompta)} <b>CFA</b>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
