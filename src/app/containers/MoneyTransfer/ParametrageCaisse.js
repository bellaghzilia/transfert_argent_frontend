import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Button,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  PanelGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./ParametrageReducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import ParametrageCaisseValidator from "./Validateur/ParametrageCaisseValidator";
import { AuthorizedComponent } from "react-router-role-authorization";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

@connect(
  (state) => ({
    dataForDetailcaisse: state.ParametrageReducer.dataForDetailcaisse,
    successinstancecaisse: state.ParametrageReducer.successinstancecaisse,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getInstancecaisse: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
    showBouttonSupprimer: PropTypes.func,
    deleteTransfertCltCltByID: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    annulerTransfertCltCltByID: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      showModal: false,
      updating: false,
      deleting: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick() {
    try {
      if (this.state.deleting) {
        await this.props.deletecaisse(this.props.rowData.id);
        this.props.loadListCaisse();
      } else if (this.state.updating) {
        await this.props.getInstancecaisse(this.props.rowData.id);
        this.props.loadListCaisse();
      }
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      saveCaisse,
      loadListCaisse,
      loadListAgence,
      updateCaisse,
      listCaisse,
      getInstancecaisse,
      deletecaisse,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
    };

    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: true,
                showModal: true,
                deleting: false,
              })
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>
          <Button
            bsSize="small"
            bsStyle="danger"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                deleting: true,
                showModal: true,
                updating: false,
              })
            }
          >
            <i className="fa fa-trash-o fa-fw" />
          </Button>
        </ButtonGroup>

        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {this.state.deleting && <div>{t("popup.supression.title")}</div>}
              {this.state.updating && (
                <div>{t("popup.modification.title")}</div>
              )}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.deleting && <div>{t("popup.supression.msgc")}</div>}
            {this.state.updating && <div>{t("popup.modification.msgc")}</div>}
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.supression.noBtn")}
              </Button>
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={this.handleClick}
              >
                {t("popup.supression.yesBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.loadListCaisse()),
        dispatch(MoneyTransferAction.loadListAgence()),
      ]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",
    fields: ["numeroCaisse", "nomAgence", "action"],
    validate: ParametrageCaisseValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    state.ParametrageReducer.dataForDetailcaisse &&
    state.ParametrageReducer.dataForDetailcaisse !== null &&
    state.ParametrageReducer.dataForDetailcaisse !== undefined && {
      initialValues: state.ParametrageReducer.dataForDetailcaisse,
    }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    saveCaisse: state.ParametrageReducer.saveCaisse,
    deletecaisse: state.ParametrageReducer.deletecaisse,
    updateCaisse: state.ParametrageReducer.updateCaisse,
    transfetList: state.moneyTransfer.transfetList,
    etatCaisse: state.moneyTransfer.etatCaisse,
    dataForDetailcaisse: state.ParametrageReducer.dataForDetailcaisse,
    successinstancecaisse: state.ParametrageReducer.successinstancecaisse,
    caisseDetails: state.moneyTransfer.caisseDetails,
    listCaisse: state.ParametrageReducer.listCaisse,
    lisAgence: state.ParametrageReducer.lisAgence,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class ParametrageCaisse extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.handlAnnuler = this.handlAnnuler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
  }

  handlAnnuler(event) {
    this.setState({ numeroCaisse: "" });
  }

  async onSubmit(values) {
    try {
      await this.props.saveCaisse(values);
      this.props.loadListCaisse();
      this.props.switchf();
    } catch (error) {
      console.log(error.message);
    }
  }

  async onSubmitTWO(values, id, idagence) {
    try {
      await this.props.updateCaisse(values, id, idagence);
      this.props.loadListCaisse();
      this.props.switchf();
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    const {
      fields: { numeroCaisse, nomAgence, action },
      handleSubmit,
      dataForDetailcaisse,
      switchf,
      resetForm,
      userFrontDetails,
      successinstancecaisse,
      updating,
      deleting,
      saveCaisse,
      deletecaisse,
      loadListCaisse,
      updateCaisse,
      caisseDetails,
      openCaisse,
      successOpenCaisse,
      lisAgence,
      etatCaisse,
      getListCaisseByAgence,
      values,
      listCaisse,
      t,
    } = this.props;

    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const gridMetaData = [
      {
        columnName: "nomAgence",
        displayName: t("list.cols.nomAgence"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "numeroCaisse",
        displayName: t("list.cols.numeroCaisse"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.legend.parametragecaisse")}</h2>
          </Col>
        </Row>
        <Row className={styles.compteCard}>
          <form className="formContainer">
            <fieldset>
              {successinstancecaisse ? (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.agence")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...nomAgence}
                        placeholder={dataForDetailcaisse.nomAgence}
                      />
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.numeroCaisse")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...numeroCaisse}
                        placeholder={dataForDetailcaisse.numeroCaisse}
                      />
                      {numeroCaisse.error && numeroCaisse.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {numeroCaisse.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        bsStyle="primary"
                        onClick={(e) => {
                          this.onSubmitTWO(
                            values,
                            dataForDetailcaisse.id,
                            dataForDetailcaisse.idagence
                          );
                          resetForm();
                        }}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.modifier")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit(() => {
                          switchf();
                        })}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              ) : (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.agence")}{" "}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...nomAgence}
                      >
                        <option value="" hidden>
                          {t("form.hidden.selectionneragence")}
                        </option>
                        {lisAgence &&
                          lisAgence.length !== 0 &&
                          lisAgence.map((Adresse) => (
                            <option value={Adresse.id}>
                              {" "}
                              {Adresse.nomAgence}
                            </option>
                          ))}
                      </FormControl>
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.numeroCaisse")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...numeroCaisse}
                      />
                      {numeroCaisse.error && numeroCaisse.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {numeroCaisse.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmit(values);
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.ajouter")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          values.libelle = "";
                          resetForm();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              )}
            </fieldset>

            <fieldset style={{ marginTop: "15px" }}>
              <legend>{t("form.legend.listecaisse")}</legend>
              <Row className="table-responsive">
                <Griddle
                  results={listCaisse}
                  columnMetadata={gridMetaData}
                  useGriddleStyles={false}
                  noDataMessage={t("list.search.msg.noResult")}
                  resultsPerPage={10}
                  nextText={<i className="fa fa-chevron-right" />}
                  previousText={<i className="fa fa-chevron-left" />}
                  tableClassName="table"
                  columns={["nomAgence", "numeroCaisse", "action"]}
                />
              </Row>
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
