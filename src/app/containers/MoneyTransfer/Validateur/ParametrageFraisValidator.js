import { createValidator, required } from '../../Commons/validationRules';

const ParametrageFraisValidator = createValidator({
    montantMin :[required],
    montantMax :[required],
    typefrai :[required],
    valeurfrai :[required],
});
export default ParametrageFraisValidator;