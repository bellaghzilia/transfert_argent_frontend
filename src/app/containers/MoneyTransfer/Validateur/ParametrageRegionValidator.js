import { createValidator, required } from '../../Commons/validationRules';

const ParametrageRegionValidator = createValidator({
    libelle :[required],
    codeRegion :[required],
});
export default ParametrageRegionValidator;
