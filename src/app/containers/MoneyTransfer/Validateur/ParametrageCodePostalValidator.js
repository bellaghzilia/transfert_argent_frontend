import { createValidator, required } from '../../Commons/validationRules';

const ParametrageCodePostalValidator = createValidator({
    libelle :[required],
    abreviation :[required],

});
export default ParametrageCodePostalValidator;
