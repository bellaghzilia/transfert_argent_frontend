import { createValidator, required } from '../../Commons/validationRules';

const ParametrageRoleValidator = createValidator({
    firstName :[required],
    lastName :[required],
    userName :[required],
    password :[required],
    montantremisemax :[required],
    tauxremise :[required],
});
export default ParametrageRoleValidator;
