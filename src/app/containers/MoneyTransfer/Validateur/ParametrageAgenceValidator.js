import { createValidator, required } from '../../Commons/validationRules';

const ParametrageAgenceValidator = createValidator({
    nomAgence :[required],
    codeAgence :[required],
    telAgence :[required],
    adresse :[required],
});
export default ParametrageAgenceValidator;
