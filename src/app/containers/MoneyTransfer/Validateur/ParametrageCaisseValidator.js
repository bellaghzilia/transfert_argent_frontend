import { createValidator, required } from '../../Commons/validationRules';

const ParametrageFraisValidator = createValidator({
    numeroCaisse :[required],
});
export default ParametrageFraisValidator;
