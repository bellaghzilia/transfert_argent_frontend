import { createValidator, required,  email,phone, match } from '../../Commons/validationRules';

const NewTransfertValidator = createValidator({
    numeroPieceIdentiteemeteur:[required],
    nomemetteur :[required],
    prenomemeteur :[required],
    nombeneficiare :[required],
    prenombeneficiare :[required],
    mailPrincipal: [required, email],
    emailbeneficiare: [required,email],
    numeroPieceIdentitebeneficiare :[required],
    emailemetteur :[required,email],
    numtelemetteur :[required],
    numtelbeneficiare :[required],
    montanttransfere :[required],
    montantremise :[required],
    motiftransfert :[required],
});
export default NewTransfertValidator;
