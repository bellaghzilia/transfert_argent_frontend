import { createValidator, required } from '../../Commons/validationRules';

const ParametrageVilleValidator = createValidator({
    libelle :[required],
});
export default ParametrageVilleValidator;
