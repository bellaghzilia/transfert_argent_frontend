import { createValidator, required } from '../../Commons/validationRules';

const ParametrageSchemasComptablesValidator = createValidator({
    libelle :[required],
    empMontant :[required],
    empDate :[required],
    sens :[required],
    empCompte :[required],
});
export default ParametrageSchemasComptablesValidator;
