import { createValidator, required } from '../../Commons/validationRules';

const ParametragePaysValidator = createValidator({
    libelle :[required],
    abreviation :[required],
    indicatif :[required],
});
export default ParametragePaysValidator;
