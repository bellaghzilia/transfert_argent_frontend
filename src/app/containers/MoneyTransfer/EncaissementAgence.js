import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import { asyncConnect } from "redux-connect";
import moment from "moment";
import DatePicker from "react-datepicker";
import * as MoneyTransferAction from "./moneytransferreducer";
import { translate } from "react-i18next";
import Button from "react-bootstrap-button-loader";
import * as UserActions from "../User/UserReducer";
import { AuthorizedComponent } from "react-router-role-authorization";

var result = sessionStorage.getItem("myData");
@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];
      if (result == null) {
        promises.push(
          dispatch(
            MoneyTransferAction.loadAgence(
              getState().user.userFrontDetails.agenceId
            )
          )
        );
      } else {
        promises.push(dispatch(MoneyTransferAction.loadAgence(result)));
      }
      return Promise.all(promises).then(() => {
        if (result == null) {
          dispatch(
            MoneyTransferAction.loadListMiseADipositionAgence(
              getState().user.userFrontDetails.agenceId,
              "",
              ""
            )
          );
        } else {
          dispatch(
            MoneyTransferAction.loadListMiseADipositionAgence(result, "", "")
          );
        }

        dispatch(MoneyTransferAction.loadListAgence());
      });
    },
  },
])
@connect(
  (state) => ({
    agenceObj: state.moneyTransfer.agenceObj,
    userFrontDetails: state.user.userFrontDetails,
    listAgence: state.moneyTransfer.listAgence,
    loadingMiseadispoList: state.moneyTransfer.loadingMiseadispoList,
    loadingEncaissemntAgence: state.moneyTransfer.loadingEncaissemntAgence,
    successEncaissementAgence: state.moneyTransfer.successEncaissementAgence,
    listMiseadispositionAgence: state.moneyTransfer.listMiseadispositionAgence,
    refMADA: state.moneyTransfer.refMADA,
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@translate(["MoneyTransfer"], { wait: true })
export default class EncaissementAgence extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      motif: "",
      montant: "",
      reference: "",
      formValid: false,
      idAgenceEnvois: "",
      dateOperation: moment(),
      disableFields: false,
    };

    this.isFormValid = this.isFormValid.bind(this);
    this.encaissementAg = this.encaissementAg.bind(this);
    this.handleRefChange = this.handleRefChange.bind(this);
    this.handleChangeStart = this.handleChangeStart.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  async encaissementAg(event) {
    event.preventDefault();
    await this.props.encaissementAgenceAgence(
      this.state.idAgenceEnvois,
      this.props.agenceObj.id,
      this.state.reference,
      this.state.montant
    );
    this.props.loadListMiseADipositionAgence(this.props.agenceObj.id, "", "");
    this.setState({ formValid: false });
  }

  handleSelectChange(event) {
    event.preventDefault();
    this.setState({ idAgenceEnvois: event.target.value }, function() {
      this.isFormValid();
    });
  }

  handleChangeStart(date) {
    this.setState({ dateOperation: date });
  }

  handleRefChange(event) {
    var transfert = event.target.value.split(",");
    this.setState(
      {
        reference: transfert[0],
        montant: transfert[1],
        motif: transfert[2],
        idAgenceEnvois: transfert[3],
      },
      function() {
        this.isFormValid();
      }
    );
  }

  isFormValid() {
    if (
      this.state.reference.length > 0 &&
      this.state.montant.length > 0 &&
      this.state.motif.length > 0 &&
      this.state.idAgenceEnvois.length > 0
    )
      this.setState({ formValid: true });
    else this.setState({ formValid: false });
  }

  render() {
    const {
      t,
      agenceObj,
      listAgence,
      listMiseadispositionAgence,
      loadingMiseadispoList,
      successEncaissementAgence,
      loadListMiseADipositionAgence,
      encaissementAgenceAgence,
      loadingEncaissemntAgence,
      userFrontDetails,
      refMADA,
    } = this.props;
    const styles = require("./moneytransfer.scss");

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.titleForm.encaissementagence")}</h2>
          </Col>
        </Row>
        <Row>
          {successEncaissementAgence && successEncaissementAgence === true && (
            <Col xs="12" md="12">
              <Alert bsStyle="info">{t("msg.sucessencaissement")}</Alert>
            </Col>
          )}

          {successEncaissementAgence && successEncaissementAgence === "null" && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{t("msg.failedencaissement")}</Alert>
            </Col>
          )}
          {userFrontDetails.etatAgence === "CLOSE" && (
            <Col xs="12" md="12">
              <Alert bsStyle="warning">{t("msg.ouvertureagence")}</Alert>
            </Col>
          )}
        </Row>

        <Row className="detailsBloc">
          <Col xs={12} md={3}>
            <ControlLabel>{t("form.label.dateaujourdui")}: </ControlLabel>
            <p className="detail-value">{moment().format("DD/MM/YYYY")}</p>
          </Col>
          <Col xs={12} md={3}>
            <ControlLabel>{t("form.label.agence")} : </ControlLabel>
            <p className="detail-value">{agenceObj.nomAgence}</p>
          </Col>
          <Col xs={12} md={3}>
            <ControlLabel>{t("form.label.chefagence")}: </ControlLabel>
            <p className="detail-value">{agenceObj.nomAgent}</p>
          </Col>
        </Row>

        <Row style={{ padding: "0 10px" }}>
          <form className="formContainer" style={{ padding: "30px 20px 20px" }}>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="4">
                <ControlLabel>{t("form.label.reference")}:</ControlLabel>
                <FormControl
                  componentClass="select"
                  className={styles.datePickerFormControl}
                  placeholder="select"
                  onChange={this.handleRefChange}
                >
                  <option value="">
                    {t("form.hidden.selectionnerreference")}
                  </option>
                  {listMiseadispositionAgence &&
                    listMiseadispositionAgence.length !== 0 &&
                    listMiseadispositionAgence.map((transfert) => (
                      <option
                        value={
                          transfert.reference +
                          "," +
                          transfert.montant +
                          "," +
                          transfert.motif +
                          "," +
                          transfert.idAgenceSrc
                        }
                      >
                        {transfert.reference}
                      </option>
                    ))}
                </FormControl>
              </Col>
              <Col xs="12" md="4">
                <ControlLabel>{t("form.label.agenceenvoi")}: </ControlLabel>
                <FormControl
                  componentClass="select"
                  className={styles.datePickerFormControl}
                  placeholder="select"
                  onChange={this.handleSelectChange}
                  disabled={this.state.formValid}
                >
                  <option value="" hidden>
                    Selectionné une Agence
                  </option>
                  {listAgence &&
                    listAgence.length !== 0 &&
                    listAgence.map(
                      (agence) =>
                        agence.id !== agenceObj.id && (
                          <option
                            value={agence.id}
                            selected={
                              agence.id.toString() === this.state.idAgenceEnvois
                            }
                          >
                            {agence.nomAgence}
                          </option>
                        )
                    )}
                </FormControl>
              </Col>
              <Col xs="12" md="4">
                <div style={{ float: "left", width: "73%" }}>
                  <ControlLabel>{t("form.label.dateenvoi")}: </ControlLabel>
                  <DatePicker
                    selectsStart
                    selected={this.state.dateOperation}
                    maxDate={moment()}
                    onChange={this.handleChangeStart}
                    className={styles.datePickerFormControl}
                    isClearable="true"
                    locale="fr-FR"
                    disabled={this.state.formValid}
                  />
                </div>
                <Button
                  disabled={this.state.formValid}
                  loading={loadingMiseadispoList}
                  bsStyle="primary"
                  onClick={() => {
                    if (result == null) {
                      loadListMiseADipositionAgence(
                        userFrontDetails.agenceId,
                        this.state.idAgenceEnvois,
                        this.state.dateOperation
                      );
                    } else {
                      loadListMiseADipositionAgence(
                        result,
                        this.state.idAgenceEnvois,
                        this.state.dateOperation
                      );
                    }
                    this.isFormValid();
                  }}
                  style={{
                    float: "right",
                    width: "20%",
                    height: "35px",
                    marginTop: "23px",
                  }}
                >
                  Filter
                </Button>
              </Col>
            </Row>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.Montant")}: </ControlLabel>
                <FormControl
                  disabled="true"
                  value={this.state.montant + " XOF"}
                  type="text"
                  bsClass={styles.datePickerFormControl}
                />
              </Col>

              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.motif")}: </ControlLabel>
                <FormControl
                  disabled="true"
                  value={this.state.motif}
                  type="text"
                  bsClass={styles.datePickerFormControl}
                />
              </Col>
            </Row>

            <Row className={styles.fieldRow}>
              <div className="pull-right" style={{ paddingTop: "10px" }}>
                <Button
                  loading={loadingEncaissemntAgence}
                  disabled={
                    !this.state.formValid ||
                    userFrontDetails.etatAgence === "CLOSE"
                  }
                  onClick={this.encaissementAg}
                  bsStyle="primary"
                >
                  {t("form.buttons.confirmer")}
                </Button>
                <Button
                  onClick={() => {
                    browserHistory.push(baseUrl + "app/TabeBordChefAgence");
                  }}
                  bsStyle="primary"
                >
                  <i className="fa fa-times" />
                  {t("form.buttons.cancel")}
                </Button>

                {successEncaissementAgence &&
                  successEncaissementAgence === true && (
                    <a
                      className="glyphicon glyphicon-upload btn btn-primary"
                      href={
                        baseUrl +
                        "agencetransfertargent/createAvisTransfertPDF/" +
                        refMADA
                      }
                    >
                      {" "}
                      {t("form.buttons.uploadpdf")}
                    </a>
                  )}
              </div>
            </Row>
          </form>
        </Row>
      </div>
    );
  }
}
