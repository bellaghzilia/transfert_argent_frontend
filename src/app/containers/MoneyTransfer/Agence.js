import React, {Component, PropTypes} from 'react';
import {
    Button,
    Row,
    Col,
    FormControl,
    Form,
    FormGroup,
    ButtonGroup,
    Alert,
    Panel,
    Modal,
    ControlLabel,
    Popover,
    OverlayTrigger
} from 'react-bootstrap';
import {connect} from 'react-redux';
import {reduxForm, initializeWithKey} from 'redux-form';
import {browserHistory} from 'react-router';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import Wizard from '../Commons/Wizard';
import * as MoneyTransferAction from './moneytransferreducer';
import VirtualKeyboard from '../../components/VirtualKeyboard/VirtualKeyboard';
import CustomDatePicke from '../../components/CustomDatePicke/CustomDatePicke';
import {translate} from 'react-i18next';


export const validate = values => {
    const errors = {};
    if (!values.agence) {
        errors.agence = 'Ce champ est obligatoire';
    }
    if (!values.agent) {
        errors.agent = 'Ce champ est obligatoire';
    }
    if (!values.caisse) {
        errors.caisse = 'Ce champ est obligatoire';
    }
    if (!values.soldeciasse) {
        errors.soldeciasse = 'Ce champ est obligatoire';
    }
    if (!values.montantmiseadisposition) {
        errors.montantmiseadisposition = 'Ce champ est obligatoire';
    }
    return errors;
};
@reduxForm({
        form: 'NewMoneyTransfer',
        fields: ['agence', 'agent', 'caisse', 'soldeciasse', 'montantmiseadisposition'],
        validate,
        destroyOnUnmount: true
    },
    state => (
        state.moneyTransfer.instance && state.moneyTransfer.instance !== null && state.moneyTransfer.instance !== undefined ?
            {
                initialValues: {
                    debiter: state.moneyTransfer.instance.numeroCompte,
                    beneficiare: state.moneyTransfer.instance.intituleCompteCrediter + ", " + state.moneyTransfer.instance.numeroPieceIdentite,
                    type: state.moneyTransfer.instance.typeTransfert,
                    montant: state.moneyTransfer.instance.montant,
                    serviceTransfer: state.moneyTransfer.instance.typeService,
                    motif: state.moneyTransfer.instance.motif,
                    immediate: true
                }
            }
            :
            {
                initialValues: {
                    montant: '',
                    agence: '',
                    agent: '',
                    caisse: '',
                    soldeciasse: '',
                }
            }
    )
)

@translate(['MoneyTransfer'], {wait: true})
export default class Agence extends Component {
    constructor() {
        super();
        this.state = {
            /////
            openClavier: false,
            keyPadValues: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
            ShowPanelValidationStepOne: false,
            ////
            showPanelPassword: false,
            showModalAbandonne: false,
            pass: '',
            devise: '',
            GAB: false,
            buttonchoice: true,
            date: false,
            color1: 'info',
            color2: 'danger',
            checked: true,
        };
        this.mappingPass = this.mappingPass.bind(this);
        this.generatekeyPadValues = this.generatekeyPadValues.bind(this);
        this.handelDelete = this.handelDelete.bind(this);
        this.handlechecked = this.handlechecked.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handelChange = this.handelChange.bind(this);
        this.handelChangeB = this.handelChangeB.bind(this);
        this.handleChangeType = this.handleChangeType.bind(this);
        this.redirectToOuvertureCaissePage = this.redirectToOuvertureCaissePage.bind(this);
        this.redirectToClotureCaissePage = this.redirectToClotureCaissePage.bind(this);
        this.redirectToMiseaDispositionage = this.redirectToMiseaDispositionPage.bind(this);
    }

    redirectToOuvertureCaissePage(e) {
        const {user} = this.props;
        browserHistory.push(baseUrl + 'app/EncaissementAgence');
    };

    redirectToClotureCaissePage(e) {
        const {user} = this.props;
        browserHistory.push(baseUrl + 'app/ClotureCaisse');
    };

    redirectToMiseaDispositionPage(e) {
        const {user} = this.props;
        browserHistory.push(baseUrl + 'app/MiseadispositionAgence');
    };

    mappingPass(val) {
        this.setState({pass: val});
    }

    generatekeyPadValues() {
        const keyPadValues = this.state.keyPadValues;
        let j;
        let x;
        for (let i = keyPadValues.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = keyPadValues[i - 1];
            keyPadValues[i - 1] = keyPadValues[j];
            keyPadValues[j] = x;
        }
        this.setState({keyPadValues});
    }

    handelClick(index) {
        this.setState({pass: this.state.pass + '' + index});
        this.props.values.password = this.state.pass;
    }

    handelDelete() {
        if (this.state.pass.length) {
            this.setState({pass: this.state.pass.slice(0, this.state.pass.length - 1)});
        }
    }

    handlechecked() {
        this.setState({checked: !this.state.checked});
    };

    handleSelect() {
        browserHistory.push(baseUrl + 'app/BeneficiaryStatement');
    }

    handelChange(event) {
        const numberAccount = event.target.value;
        if (numberAccount !== '') {
            const compte = this.props.listDebitAccount.find((account) => account.identifiantInterne === numberAccount);
            this.setState({devise: compte.devise});
        }
    };

    handleChangeType(event) {
        const type = event.target.value;
        this.props.setTransfertType(type);
    };


    handelChangeB(event) {
        const num = event.target.value;
        if (num !== '') {
            const compte = this.props.listBeneficiaryGAB.find((account) => account.toString_ === num);
            if (compte) {
                this.setState({GAB: true});
            }
            else {
                this.setState({GAB: false});
            }
        }
    };

    render() {
        const {
            fields: {agence, agent, caisse, soldeciasse,},
            validatepwdError, handleSubmit, values, submitting, listBeneficiary, listDebitAccount, step, setState,
            setStateGAB, validerSTEP, saveSuccessObject, moneyTransferObject, statut, signerDemande, codeErreur, maxdate,
            setStatut, jourfiries, weekend, toStepX, abandonnerDemande, instance, otpSMS, taskid, user, listBeneficiaryGAB,
            Request, initpwd, saveUrl, updateUrl, setTransfertType, isInstance, typeOfTransfer, listeServiceTransfert, resetForm, params,
            t, signRedirect, signUrlred, taskId
        } = this.props;
        const styles = require('./moneytransfer.scss');
        const style = require('../Commons/styleKeyPad.scss');
        const popoverBottom = (
            <Popover id="popover-positioned-top" title={t('form.label.codeValidation')}>
                <div className="pageWrap">
                    <div className="keypad">
                        {
                            this.state.keyPadValues.map((key) =>
                                <span onClick={this.handelClick.bind(this, key)} className="key">{key}</span>
                            )}
                        <span onClick={this.handelDelete} className="glyphicon glyphicon-arrow-left"/>
                        <span onClick={() => this.setState({pass: ''})} className="glyphicon glyphicon-refresh"/>
                        <br />
                    </div>
                </div>
            </Popover>
        );
        const showSignBlocks = taskId != null && instance && !instance.statutCode.startsWith('Signe') && !instance.statutCode.startsWith('Rejet');

        return (
            <div>
                <Row className={styles.compteCard}>


                    <form onSubmit={handleSubmit}>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>

                        <Row className={styles.fieldRow}>
                            <Col xs="12" md="12">

                                <Col xs="12" md="4">
                                    <ControlLabel></ControlLabel>
                                </Col>
                                <Col xs="12" md="4">
                                    <ControlLabel><h2>Gestion Agence</h2></ControlLabel>
                                </Col>
                                <Col xs="12" md="4">
                                    <ControlLabel></ControlLabel>
                                </Col>
                            </Col></Row>
                        <br></br>
                        <br></br>
                        <br></br>
                        <fieldset >

                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="12">
                                    <Row className={styles.fieldRow}>
                                        <Col xs="12" md="2">
                                            <ControlLabel></ControlLabel>
                                        </Col>

                                        <Col xs="12" md="4">
                                            <div className="pull-right">
                                                <Button disabled={false} onClick={this.redirectToOuvertureCaissePage}
                                                        bsStyle="success"><i className="glyphicon glyphicon-ok-sign"/>
                                                    &nbsp; {t('form.buttons.encaissementagence')}
                                                </Button>
                                            </div>

                                        </Col>

                                        <Col xs="12" md="4">
                                            <div className="pull-right">
                                                <Button disabled={false} onClick={this.redirectToMiseaDispositionage}
                                                        bsStyle="info"><i className="glyphicon glyphicon-refresh"/>
                                                    &nbsp; {t('form.buttons.miseadispositoncagence')}
                                                </Button></div>
                                        </Col>

                                        <Col xs="12" md="2">
                                            <ControlLabel></ControlLabel>
                                        </Col>

                                    </Row>

                                </Col>
                            </Row>
                            <br/><br/><br/><br/> <br/>
                        </fieldset>


                    </form>


                </Row>

            </div>
        );
    }
}
