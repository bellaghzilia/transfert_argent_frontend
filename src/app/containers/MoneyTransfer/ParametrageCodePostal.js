import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Button,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  PanelGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./ParametrageReducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import ParametrageCodePostalValidator from "./Validateur/ParametrageCodePostalValidator";
import { AuthorizedComponent } from "react-router-role-authorization";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

@connect(
  (state) => ({
    dataForDetailcode: state.ParametrageReducer.dataForDetailcode,
    successinstancecode: state.ParametrageReducer.successinstancecode,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getInstanceodepostal: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
    showBouttonSupprimer: PropTypes.func,
    deleteTransfertCltCltByID: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    annulerTransfertCltCltByID: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      showModal: false,
      updating: false,
      deleting: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick() {
    try {
      if (this.state.deleting) {
        await this.props.deleteodepostal(this.props.rowData.id);
        this.props.loadListcodepostal();
      } else if (this.state.updating) {
        await this.props.getInstanceodepostal(this.props.rowData.id);
        this.props.loadListcodepostal();
      }
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      savecodepostal,
      loadListVilles,
      loadListcodepostal,
      updateodepostal,
      lisVilles,
      lisCodes,
      getInstanceodepostal,
      deleteodepostal,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
    };

    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: true,
                showModal: true,
                deleting: false,
              })
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>
          <Button
            bsSize="small"
            bsStyle="danger"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                deleting: true,
                showModal: true,
                updating: false,
              })
            }
          >
            <i className="fa fa-trash-o fa-fw" />
          </Button>
        </ButtonGroup>

        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {this.state.deleting && <div>{t("popup.supression.title")}</div>}
              {this.state.updating && (
                <div>{t("popup.modification.title")}</div>
              )}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.deleting && <div>{t("popup.supression.msg")}</div>}
            {this.state.updating && <div>{t("popup.modification.msg")}</div>}
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.supression.noBtn")}
              </Button>
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={this.handleClick}
              >
                {t("popup.supression.yesBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.loadListVilles()),
        dispatch(MoneyTransferAction.loadListcodepostal()),
      ]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",
    fields: ["libelle", "libellevilles", "action"],
    validate: ParametrageCodePostalValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    state.ParametrageReducer.dataForDetailcode &&
    state.ParametrageReducer.dataForDetailcode !== null &&
    state.ParametrageReducer.dataForDetailcode !== undefined && {
      initialValues: state.ParametrageReducer.dataForDetailcode,
    }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    savecodepostal: state.ParametrageReducer.savecodepostal,
    deleteodepostal: state.ParametrageReducer.deleteodepostal,
    updateodepostal: state.ParametrageReducer.updateodepostal,
    transfetList: state.moneyTransfer.transfetList,
    etatCaisse: state.moneyTransfer.etatCaisse,
    dataForDetailcode: state.ParametrageReducer.dataForDetailcode,
    successinstancecode: state.ParametrageReducer.successinstancecode,
    listCaisse: state.moneyTransfer.listCaisse,
    caisseDetails: state.moneyTransfer.caisseDetails,
    lisVilles: state.ParametrageReducer.lisVilles,
    lisCodes: state.ParametrageReducer.lisCodes,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
    libelle: "",
    libellepays: "",
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class ParametrageCodePostal extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.handlAnnuler = this.handlAnnuler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
  }

  handlAnnuler(event) {
    this.setState({ libelle: "" });
  }
  async onSubmit(values) {
    try {
      await this.props.savecodepostal(values);
      this.props.loadListcodepostal();
    } catch (error) {
      console.log(error.message);
    }
  }

  async onSubmitTWO(values, idville, id) {
    try {
      await this.props.updateodepostal(values, idville, id);
      this.props.loadListcodepostal();
      this.props.switchf();
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    const {
      fields: { libelle, libellevilles, action },
      handleSubmit,
      userFrontDetails,
      dataForDetailcode,
      switchf,
      resetForm,
      successinstancecode,
      updating,
      deleting,
      savecodepostal,
      deleteodepostal,
      loadListVilles,
      updateodepostal,
      caisseDetails,
      openCaisse,
      successOpenCaisse,
      lisVilles,
      lisCodes,
      etatCaisse,
      getListCaisseByAgence,
      values,
      listCaisse,
      t,
    } = this.props;

    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const gridMetaData = [
      {
        columnName: "libellevilles",
        displayName: t("list.cols.ville"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "libelle",
        displayName: t("list.cols.libelle"),
        customHeaderComponent: HeaderComponent,
      },

      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];

    return (
      <div>
        <br></br>
        <Row className={styles.compteCard}>
          <form>
            <br></br>

            <legend>{t("form.legend.parametragecodepostal")}</legend>
            <fieldset>
              {successinstancecode ? (
                <Row className={styles.paddingColumn}>
                  <Col xs="12" md="12">
                    <Col xs="12" md="5">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.ville")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libellevilles}
                        value={dataForDetailcode.ville.libelle}
                      />
                    </Col>
                    <Col xs="12" md="5">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.libelle")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libelle}
                        placeholder={dataForDetailcode.libelle}
                      />
                      {libelle.error && libelle.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {libelle.error}
                          </i>
                        </div>
                      )}
                    </Col>

                    <Col xs="12" md="1">
                      <ControlLabel></ControlLabel>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmitTWO(
                            values,
                            dataForDetailcode.ville.id,
                            dataForDetailcode.id
                          );
                          resetForm();
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.modifier")}
                      </Button>
                    </Col>
                    <Col xs="12" md="1">
                      <ControlLabel></ControlLabel>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          switchf();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </Col>
                  </Col>
                </Row>
              ) : (
                <Row className={styles.paddingColumn}>
                  <Col xs="12" md="12">
                    <Col xs="12" md="5">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.ville")}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...libellevilles}
                      >
                        <option value="" hidden>
                          {t("form.hidden.selectionnerville")}{" "}
                        </option>
                        {lisVilles &&
                          lisVilles.length !== 0 &&
                          lisVilles.map((Adresse) => (
                            <option value={Adresse.id}>
                              {" "}
                              {Adresse.libelle}
                            </option>
                          ))}
                      </FormControl>
                    </Col>
                    <Col xs="12" md="5">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.libelle")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libelle}
                      />
                      {libelle.error && libelle.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {libelle.error}
                          </i>
                        </div>
                      )}
                    </Col>

                    <Col xs="12" md="1">
                      <ControlLabel></ControlLabel>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmit(values);
                          resetForm();
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.ajouter")}
                      </Button>
                    </Col>
                    <Col xs="12" md="1">
                      <ControlLabel></ControlLabel>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          values.libelle = "";
                          resetForm();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </Col>
                  </Col>
                </Row>
              )}
            </fieldset>
            <legend>{t("form.legend.listecodepostal")}</legend>
            <fieldset>
              <Griddle
                results={lisCodes}
                columnMetadata={gridMetaData}
                useGriddleStyles={false}
                noDataMessage={t("list.search.msg.noResult")}
                resultsPerPage={10}
                nextText={<i className="fa fa-chevron-right" />}
                previousText={<i className="fa fa-chevron-left" />}
                tableClassName="table"
                columns={["libellevilles", "libelle", "action"]}
              />
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
