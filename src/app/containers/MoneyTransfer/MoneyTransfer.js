import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Tabs,
  Tab,
  Button,
  Row,
  Col,
  Alert,
  ControlLabel,
  Nav,
  MenuItem,
  NavDropdown,
  LinkContainer,
  Modal,
  ButtonGroup,
} from "react-bootstrap";
import { connect } from "react-redux";
import { initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import { asyncConnect } from "redux-connect";
import PageHeader from "react-bootstrap/lib/PageHeader";
import NewMoneyTransfer from "./NewMoneyTransfer";
import ListMoneyTransfer from "./ListMoneyTransfer";
import OuvertureCaisse from "./OuvertureCaisse";
import * as MoneyTransferAction from "./moneytransferreducer";
import moment from "moment";
import * as UserActions from "../User/UserReducer";
import * as ParametrageAction from "./ParametrageReducer";
import { reset } from "redux-form";

var TBD = sessionStorage.getItem("myData4");

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.initializeForm()),
        dispatch(MoneyTransferAction.RestFormN()),
        dispatch(reset("NewMoneyTransfer")),
        dispatch(MoneyTransferAction.switchf()),
        dispatch(MoneyTransferAction.typeTB(TBD)),
        dispatch(ParametrageAction.loadListVilles()),
      ]);
    },
  },
])
@connect(
  (state) => ({
    validatepwdError: state.moneyTransfer.validatepwdError,
    listBeneficiary: state.moneyTransfer.listBeneficiary,
    listDebitAccount: state.moneyTransfer.listDebitAccount,
    listDebitAccountGAB: state.moneyTransfer.listDebitAccountGAB,
    listBeneficiaryGAB: state.moneyTransfer.listBeneficiaryGAB,
    step: state.moneyTransfer.step,
    listMoneyTransfer: state.moneyTransfer.listMoneyTransfer,
    listMoneyTransferGAB: state.moneyTransfer.listMoneyTransferGAB,
    moneyTransferObject: state.moneyTransfer.moneyTransferObject,
    statut: state.moneyTransfer.statut,
    saveSuccessObject: state.moneyTransfer.saveSuccessObject,
    saveSuccessValide: state.moneyTransfer.saveSuccessValide,
    saveSuccess: state.moneyTransfer.saveSuccess,
    success: state.moneyTransfer.success,
    codeErreur: state.moneyTransfer.codeErreur,
    versionFromInstance: state.moneyTransfer.versionFromInstance,
    instance: state.moneyTransfer.instance,
    otpSMS: state.moneyTransfer.otpSMS,
    listLcnRequest: state.moneyTransfer.listLcnRequest,
    Request: state.moneyTransfer.Request,
    maxdate: state.moneyTransfer.maxdate,
    jourfiries: state.moneyTransfer.jourfiries,
    weekend: state.moneyTransfer.weekend,
    dataForDetail: state.moneyTransfer.dataForDetail,
    id: state.moneyTransfer.saveSuccessObject.id,
    caisseDetails: state.moneyTransfer.caisseDetails,
    fraienvois: state.moneyTransfer.fraienvois,
    telDetails: state.moneyTransfer.telDetails,
    telbenDetails: state.moneyTransfer.telbenDetails,
    clientDetails: state.moneyTransfer.clientDetails,
    clientbenDetails: state.moneyTransfer.clientbenDetails,
    successtel: state.moneyTransfer.successtel,
    successtelben: state.moneyTransfer.successtelben,
    successclient: state.moneyTransfer.successclient,
    successclientbene: state.moneyTransfer.successclientbene,
    successfraisenvois: state.moneyTransfer.successfraisenvois,
    successmontantremise: state.moneyTransfer.successmontantremise,
    view: state.moneyTransfer.view,
    showAnnuler: state.moneyTransfer.showAnnuler,
    showSupprimer: state.moneyTransfer.showSupprimer,
    isSupprimer: state.moneyTransfer.isSupprimer,
    isAnnule: state.moneyTransfer.isAnnule,
    taskid: state.moneyTransfer.taskid,
    user: state.user.userFrontDetails,
    listeStatuts: state.moneyTransfer.listeStatuts,
    saveUrl: state.moneyTransfer.saveUrl,
    updateUrl: state.moneyTransfer.updateUrl,
    cancelUrl: state.moneyTransfer.cancelUrl,
    signeUrl: state.moneyTransfer.signeUrl,
    isInstance: state.moneyTransfer.isInstance,
    typeOfTransfer: state.moneyTransfer.typeOfTransfer,
    listeServiceTransfert: state.moneyTransfer.listeServiceTransfert,
    isSummary: state.moneyTransfer.isSummary,
    signRedirect: state.moneyTransfer.signRedirect,
    signUrlred: state.moneyTransfer.signUrlred,
    caisseObj: state.moneyTransfer.caisseObj,
    transfetList: state.moneyTransfer.transfetList,
    loadingListTransfertAgence: state.moneyTransfer.loadingListTransfertAgence,
    fraisenvois: state.moneyTransfer.fraisenvois,
    lisVilles: state.ParametrageReducer.lisVilles,
    TYPETB: state.moneyTransfer.TYPETB,
  }),
  {
    ...MoneyTransferAction,
    ...UserActions,
    ...ParametrageAction,
    initializeWithKey,
  }
)
export default class MoneyTransfer extends Component {
  static propTypes = {
    loadCaisse: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    listBeneficiary: PropTypes.array,
    listDebitAccount: PropTypes.array,
    listDebitAccountGAB: PropTypes.array,
    listBeneficiaryGAB: PropTypes.array,
    listMoneyTransfer: PropTypes.array,
    listMoneyTransferGAB: PropTypes.array,
    validerSTEP: PropTypes.array,
    saveexisteclient: PropTypes.array,
    loadListTelClient: PropTypes.array,
    loadListTelClientben: PropTypes.array,
    loadlClient: PropTypes.array,
    loadlClientbeneficiare: PropTypes.array,
    setState: PropTypes.array,
    setStateGAB: PropTypes.func,
    saveSuccessObject: PropTypes.array,
    saveSuccessValide: PropTypes.array,
    moneyTransferObject: PropTypes.array,
    step: PropTypes.string,
    statut: PropTypes.string,
    signerDemande: PropTypes.func,
    success: PropTypes.boolean,
    codeErreur: PropTypes.string,
    setStatut: PropTypes.func,
    toStepX: PropTypes.func,
    setToInitialState: PropTypes.func,
    abandonnerDemande: PropTypes.func,
    listLcnRequest: PropTypes.boolean,
    Request: PropTypes.boolean,
    taskid: PropTypes.number,
    //
    maxdate: PropTypes.string,
    jourfiries: PropTypes.array,
    weekend: PropTypes.array,
    validatepwdError: PropTypes.boolean,
    initpwd: PropTypes.func,
    showAnnuler: PropTypes.boolean,
    showSupprimer: PropTypes.boolean,
    isAnnule: PropTypes.boolean,
    isSupprimer: PropTypes.boolean,
    view: PropTypes.string,
    setView: PropTypes.func,
    dataForDetail: PropTypes.array,
    id: PropTypes.array,
    caisseDetails: PropTypes.array,
    fraienvois: PropTypes.array,
    telDetails: PropTypes.array,
    telbenDetails: PropTypes.array,
    clientDetails: PropTypes.array,
    clientbenDetails: PropTypes.array,
    successtel: PropTypes.array,
    successtelben: PropTypes.array,
    successclient: PropTypes.array,
    successclientbene: PropTypes.array,
    successfraisenvois: PropTypes.array,
    successmontantremise: PropTypes.array,
    getInstance: PropTypes.func,
    createAvisTransfertPDF: PropTypes.func,
    loadfraisenvois: PropTypes.func,
    loadmontantremise: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    annulerDemande: PropTypes.func,
    supprimerDemande: PropTypes.func,
    loadListMoneyTransferAgence: PropTypes.func,
    setTransfertType: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      showPanelSearch: false,
    };
  }

  render() {
    const {
      TYPETB,
      validatepwdError,
      initpwd,
      getInstance,
      createAvisTransfertPDF,
      loadfraisenvois,
      loadmontantremise,
      getInstanceCollaborateur,
      showAnnuler,
      showSupprimer,
      setView,
      view,
      fraienvois,
      id,
      dataForDetail,
      caisseDetails,
      telDetails,
      telbenDetails,
      lientDetails,
      successtel,
      successtelben,
      successclient,
      successclientbene,
      successfraisenvois,
      successmontantremise,
      listBeneficiary,
      listDebitAccount,
      listMoneyTransfer,
      listMoneyTransferGAB,
      step,
      loadListTelClient,
      loadListTelClientben,
      loadlClient,
      loadlClientbeneficiare,
      validerSTEP,
      saveexisteclient,
      setState,
      setStateGAB,
      saveSuccessValide,
      saveSuccessObject,
      saveSuccess,
      moneyTransferObject,
      statut,
      signerDemande,
      success,
      codeErreur,
      clientDetails,
      clientbenDetails,
      listLcnRequest,
      maxdate,
      Request,
      jourfiries,
      weekend,
      setStatut,
      toStepX,
      setToInitialState,
      abandonnerDemande,
      instance,
      otpSMS,
      annulerDemande,
      supprimerDemande,
      isAnnule,
      isSupprimer,
      taskid,
      user,
      listBeneficiaryGAB,
      listDebitAccountGAB,
      loadListMoneyTransferAgence,
      setTransfertType,
      saveUrl,
      updateUrl,
      isInstance,
      typeOfTransfer,
      listeServiceTransfert,
      isSummary,
      initializeForm,
      params,
      signRedirect,
      signUrlred,
      loadCaisse,
      caisseObj,
      loadingListTransfertAgence,
      loadListTransfertAgence,
      transfetList,
      signeTransfertCltCltByID,
      userFrontDetails,
      loadUserFrontDetails,
      lisVilles,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    if (instance) {
    }
    const handleSelect = (list) => {
      if (!list) {
        browserHistory.push(baseUrl + "app/request/MoneyTransfer");
      }
    };

    return (
      <div>
        <br />

        <Row>
          <Col xs={12} md={12} className={styles.compteCard}>
            <NewMoneyTransfer
              listBeneficiary={listBeneficiary}
              listDebitAccount={listDebitAccount}
              telDetails={telDetails}
              telbenDetails={telbenDetails}
              clientDetails={clientDetails}
              clientbenDetails={clientbenDetails}
              successtel={successtel}
              successtelben={successtelben}
              successclient={successclient}
              successclientbene={successclientbene}
              successfraisenvois={successfraisenvois}
              successmontantremise={successmontantremise}
              fraienvois={fraienvois}
              step={step}
              validerSTEP={validerSTEP}
              saveexisteclient={saveexisteclient}
              loadListTelClient={loadListTelClient}
              loadListTelClientben={loadListTelClientben}
              loadlClient={loadlClient}
              loadlClientbeneficiare={loadlClientbeneficiare}
              setState={setState}
              setStateGAB={setStateGAB}
              saveSuccessObject={saveSuccessObject}
              saveSuccessValide={saveSuccessValide}
              saveSuccess={saveSuccess}
              moneyTransferObject={moneyTransferObject}
              statut={statut}
              TYPETB={TYPETB}
              signerDemande={signerDemande}
              codeErreur={codeErreur}
              setStatut={setStatut}
              toStepX={toStepX}
              abandonnerDemande={abandonnerDemande}
              instance={instance}
              otpSMS={otpSMS}
              taskid={taskid}
              user={user}
              dataForDetail={dataForDetail}
              id={id}
              listDebitAccountGAB={listDebitAccountGAB}
              listBeneficiaryGAB={listBeneficiaryGAB}
              Request={Request}
              validatepwdError={validatepwdError}
              initpwd={initpwd}
              jourfiries={jourfiries}
              weekend={weekend}
              maxdate={maxdate}
              setTransfertType={setTransfertType}
              saveUrl={saveUrl}
              updateUrl={updateUrl}
              isInstance={isInstance}
              loadfraisenvois={loadfraisenvois}
              loadmontantremise={loadmontantremise}
              typeOfTransfer={typeOfTransfer}
              listeServiceTransfert={listeServiceTransfert}
              params={params}
              signRedirect={signRedirect}
              signUrlred={signUrlred}
              taskId={this.props.location.query.taskId}
              signeTransfertCltCltByID={signeTransfertCltCltByID}
              lisVilles={lisVilles}
            />
          </Col>
        </Row>
      </div>
    );
  }
}
