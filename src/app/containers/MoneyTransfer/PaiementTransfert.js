import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Alert,
  Col,
  ControlLabel,
  FormControl,
  InputGroup,
  Popover,
  Row,
} from "react-bootstrap";
import { connect } from "react-redux";
import { initializeWithKey, reduxForm } from "redux-form";
import { browserHistory } from "react-router";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./moneytransferreducer";
import { translate } from "react-i18next";
import Button from "react-bootstrap-button-loader";
import * as UserActions from "../User/UserReducer";
import { asyncConnect } from "redux-connect";
import { AuthorizedComponent } from "react-router-role-authorization";

var TB = sessionStorage.getItem("myData4");

@connect(
  (state) => ({
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
    transfertDetails: state.moneyTransfer.transfertDetails,
    successtransfert: state.moneyTransfer.successtransfert,
    successreferencetransfert: state.moneyTransfer.successreferencetransfert,
    telbenDetails: state.moneyTransfer.telbenDetails,
    successtelben: state.moneyTransfer.successtelben,
    referenceList: state.moneyTransfer.referenceList,
    successreference: state.moneyTransfer.successreference,
    identDetails: state.moneyTransfer.identDetails,
    successidentite: state.moneyTransfer.successidentite,
    loadingpaiement: state.moneyTransfer.loadingpaiement,
    userFrontDetails: state.user.userFrontDetails,
    successpaiement: state.moneyTransfer.successpaiement,
    transfertsupprimer: state.moneyTransfer.transfertsupprimer,
    transfertannuler: state.moneyTransfer.transfertannuler,
    transfertenregistrer: state.moneyTransfer.transfertenregistrer,
    messagepaiement: state.moneyTransfer.messagepaiement,
    successVirifierSolde: state.moneyTransfer.successVirifierSolde,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([dispatch(MoneyTransferAction.switchf())]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",
    fields: [
      "numtelemetteur",
      "numeroPieceIdentiteemeteur",
      "numeroPieceIdentitepercepteur",
      "reference",
      "nomemetteur",
      "prenomemeteur",
      "emailemetteur",
      "villeemetteur",
      "adresseemeteur",
      "codepostalemetteur",
      "paysemetteur",
      "numerotelebeneficiare",
      "numeroPieceIdentitebeneficiare",
      "nombeneficiare",
      "nompercepteur",
      "prenombeneficiare",
      "prenompercepteur",
      "emailbeneficiare",
      "emailpercepteur",
      "villebeneficiare",
      "villepercepteur",
      "adressebeneficiare",
      "adressepercepteur",
      "codepostalbeneficiare",
      "codepostalpercepteur",
      "paysbeneficiare",
      "payspercepteur",
      "lientbeneficiaire",
      ,
      "numtelpercepteur",
      "montantOperation",
      "fraiinclus",
      "montantremise",
      "agence",
      "agent",
      "motiftransfert",
      "motif",
      "fraisenvois",
      "regionemeteur",
      "paysindicatifemetteur",
      "regionbeneficiare",
      "paysindicatifbeneficiare",
    ],

    destroyOnUnmount: true,
  },
  (state) =>
    state.moneyTransfer.transfertDetails &&
    state.moneyTransfer.transfertDetails !== null &&
    state.moneyTransfer.transfertDetails !== undefined && {
      initialValues: state.moneyTransfer.transfertDetails,
    }
)
@translate(["MoneyTransfer"], { wait: true })
export default class PaiementTransfert extends AuthorizedComponent {
  static propTypes = { transfertDetails: PropTypes.array };
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      openClavier: false,
      keyPadValues: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
      ShowPanelValidationStepOne: false,
      showPanelPassword: false,
      showModalAbandonne: false,
      pass: "",
      devise: "",
      step: "stepOne",
      GAB: false,
      buttonchoice: true,
      date: false,
      color1: "info",
      color2: "danger",
      identifiant: "",
      telphone: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleChangetel = this.handleChangetel.bind(this);
    this.mappingPass = this.mappingPass.bind(this);
    this.generatekeyPadValues = this.generatekeyPadValues.bind(this);
    this.handelDelete = this.handelDelete.bind(this);
    this.redirectToHomePage = this.redirectToHomePage.bind(this);
    this.changestep = this.changestep.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.searchReference = this.searchReference.bind(this);
  }

  async onSubmit(transfertDetails, values) {
    // Here, we use an async/await approach that allows our code
    // to look synchronous which is always a plus. The code looks clean!
    try {
      await this.props.payerTransfert(transfertDetails, values);
      if (this.props.successpaiement) {
        this.props.switchf();
      }
    } catch (error) {
      // logging the error
      console.log(error.message);
    }
  }

  async searchReference(reference) {
    // Here, we use an async/await approach that allows our code
    // to look synchronous which is always a plus. The code looks clean!
    try {
      await this.props.loadReference(reference);

      if (this.props.successreferencetransfert == false) {
        setTimeout(() => {
          this.props.resetAlertReference();
        }, 2000);
        this.props.switchf();
      }
    } catch (error) {
      // logging the error
      console.log(error.message);
    }
  }

  handleChange(event) {
    this.setState({ identifiant: event.target.value });
  }

  handleChangetel(event) {
    this.setState({ telphone: event.target.value });
  }

  redirectToHomePage(TB) {
    this.props.switchf();
    if (TB == "TB_AGENT") {
      browserHistory.push(baseUrl + "app/TDBAgentCaisse");
    } else if (TB == "TB_PARTENAIRE") {
      browserHistory.push(baseUrl + "app/TabeBordPartenaire");
    }
  }

  mappingPass(val) {
    this.setState({ pass: val });
  }

  changestep(step) {
    if (step == "stepOne") {
      step = "stepTwo";
    }
  }

  generatekeyPadValues() {
    const keyPadValues = this.state.keyPadValues;
    let j;
    let x;
    for (let i = keyPadValues.length; i; i--) {
      j = Math.floor(Math.random() * i);
      x = keyPadValues[i - 1];
      keyPadValues[i - 1] = keyPadValues[j];
      keyPadValues[j] = x;
    }
    this.setState({ keyPadValues });
  }

  handelClick(index) {
    this.setState({ pass: this.state.pass + "" + index });
    this.props.values.password = this.state.pass;
  }

  handelDelete() {
    if (this.state.pass.length) {
      this.setState({
        pass: this.state.pass.slice(0, this.state.pass.length - 1),
      });
    }
  }

  render() {
    const {
      fields: {
        numtelemetteur,
        numeroPieceIdentiteemeteur,
        numeroPieceIdentitepercepteur,
        reference,
        nomemetteur,
        prenomemeteur,
        emailemetteur,
        emailpercepteur,
        villeemetteur,
        adresseemeteur,
        codepostalemetteur,
        paysemetteur,
        codepostalpercepteur,
        numerotelebeneficiare,
        numtelpercepteur,
        numeroPieceIdentitebeneficiare,
        nombeneficiare,
        nompercepteur,
        prenombeneficiare,
        prenompercepteur,
        emailbeneficiare,
        villebeneficiare,
        villepercepteur,
        adressebeneficiare,
        adressepercepteur,
        codepostalbeneficiare,
        paysbeneficiare,
        payspercepteur,
        lientbeneficiaire,
        montantOperation,
        fraisenvois,
        montantremise,
        motiftransfert,
        beneficiare,
        agence,
        agent,
        motif,
        regionemeteur,
        paysindicatifemetteur,
        regionbeneficiare,
        paysindicatifbeneficiare,
      },
      userFrontDetails,
      fraienvoi,
      resetAlertReference,
      fraiinclus,
      validatepwdError,
      successtransfert,
      successreferencetransfert,
      handleSubmit,
      identDetails,
      telDetails,
      telbenDetails,
      clientDetails,
      transfertDetails,
      clientbenDetails,
      successtel,
      successidentite,
      successtelben,
      successclient,
      successclientbene,
      values,
      submitting,
      listBeneficiary,
      listDebitAccount,
      step = "stepTwo",
      setState,
      setStateGAB,
      validerSTEP,
      saveexisteclient,
      loadListTelClient,
      loadListTelClientben,
      loadListidentiteClient,
      loadListReferences,
      loadReference,
      loadlClient,
      loadlClientbeneficiare,
      switchf,
      payerTransfert,
      saveSuccessObject,
      moneyTransferObject,
      statut,
      signerDemande,
      codeErreur,
      maxdate,
      setStatut,
      jourfiries,
      weekend,
      toStepX,
      loadingpaiement,
      successreference,
      transfertenregistrer,
      transfertannuler,
      transfertsupprimer,
      successpaiement,
      messagepaiement,
      abandonnerDemande,
      instance,
      otpSMS,
      taskid,
      user,
      listBeneficiaryGAB,
      Request,
      initpwd,
      saveUrl,
      updateUrl,
      referenceList,
      setTransfertType,
      isInstance,
      typeOfTransfer,
      listeServiceTransfert,
      resetForm,
      params,
      t,
      signRedirect,
      signUrlred,
      taskId,
      successVirifierSolde,
    } = this.props;
    const styles = require("./moneytransfer.scss");

    return (
      <div>
        <Row className={styles.compteCard}>
          <Wizard>{step}</Wizard>

          {transfertannuler === true && (
            <Alert bsStyle="danger">{t("msg.transfertannuler")}</Alert>
          )}

          {transfertenregistrer === true && (
            <Alert bsStyle="danger">{t("msg.transfertnonsigne")}</Alert>
          )}
          {successreferencetransfert === false && (
            <Alert bsStyle="danger">{t("msg.transfertrefersnce")}</Alert>
          )}
          {transfertsupprimer === true && (
            <Alert bsStyle="danger">{t("msg.transfertsupprime")}</Alert>
          )}

          {successpaiement === false && successVirifierSolde === false && (
            <Alert bsStyle="danger">{messagepaiement}</Alert>
          )}
          {messagepaiement === "la solde dans la Caisse insuffisant!!" && (
            <Alert bsStyle="danger">{messagepaiement}</Alert>
          )}

          {successpaiement === true && (
            <Alert bsStyle="success">{t("msg.paiementsucces")}</Alert>
          )}

          {userFrontDetails.caisseId === "" && TB !== "TB_PARTENAIRE" && (
            <Alert bsStyle="danger">{t("msg.aucuncaisseauverte")}</Alert>
          )}

          {successreferencetransfert &&
            transfertDetails.statutPaiement === "payer" && (
              <Alert bsStyle="danger">{t("msg.transfertpaye")}</Alert>
            )}

          {successreferencetransfert &&
            transfertDetails.statutPaiement === "nonautorise" && (
              <Alert bsStyle="danger">{t("msg.transfertnonautorise")}</Alert>
            )}

          <form onSubmit={handleSubmit} className="formContainer">
            <fieldset>
              <Row className={styles.fieldRow}>
                {successreference ? (
                  <Col xs="6" md="4">
                    <ControlLabel>Reference :</ControlLabel>
                    <FormControl
                      {...reference}
                      componentClass="select"
                      className={styles.datePickerFormControl}
                      placeholder="select"
                    >
                      <option value="" hidden>
                        Reference
                      </option>
                      {referenceList &&
                        referenceList.length &&
                        referenceList.map((Ref) => (
                          <option value={Ref}>{Ref}</option>
                        ))}
                    </FormControl>
                  </Col>
                ) : (
                  <Col xs="6" md="4">
                    <ControlLabel>Reference :</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...reference}
                    />
                  </Col>
                )}

                <Col xs="6" md="2">
                  <Button
                    bsStyle="primary"
                    style={{ marginTop: "24px", width: "100%", height: "35px" }}
                    onClick={(e) => {
                      e.preventDefault();
                      this.searchReference(values.reference);
                    }}
                  >
                    <i className="fa fa-search" /> Rechercher
                  </Button>
                </Col>
              </Row>
            </fieldset>

            <fieldset style={{ marginTop: "25px" }}>
              <legend>{t("form.legend.infosemeteur")}</legend>
              {successtransfert ? (
                <div>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.numidentiemeteur")}:
                      </ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...numeroPieceIdentiteemeteur}
                      />
                    </Col>

                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.numtelemeteur")}:
                      </ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...numtelemetteur}
                      />
                    </Col>
                  </Row>

                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.nom")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...nomemetteur}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.prenom")} :</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...prenomemeteur}
                      />
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                      <InputGroup>
                        <InputGroup.Addon>
                          <i className="fa fa-at" />
                        </InputGroup.Addon>
                        <FormControl
                          type="text"
                          className={styles.datePickerFormControl}
                          {...emailemetteur}
                        />
                      </InputGroup>
                    </Col>
                  </Row>

                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.ville")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...villeemetteur}
                        value={userFrontDetails.villeNom}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.region")}:</ControlLabel>
                      <FormControl
                        type="text"
                        {...regionemeteur}
                        className={styles.datePickerFormControl}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...paysemetteur}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.indicatif")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...paysindicatifemetteur}
                      />
                    </Col>
                  </Row>
                </div>
              ) : (
                <div>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.numidentiemeteur")}:
                      </ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...numeroPieceIdentiteemeteur}
                      />
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.numtelemeteur")}:
                      </ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...numtelemetteur}
                      />
                    </Col>
                  </Row>

                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.nom")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...nomemetteur}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.prenom")} :</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...prenomemeteur}
                      />
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                      <InputGroup>
                        <InputGroup.Addon>
                          <i className="fa fa-at" />
                        </InputGroup.Addon>
                        <FormControl
                          type="text"
                          disabled={true}
                          className={styles.datePickerFormControl}
                          {...emailemetteur}
                        />
                      </InputGroup>
                    </Col>
                  </Row>

                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.ville")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...villeemetteur}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.region")}:</ControlLabel>
                      <FormControl
                        type="text"
                        {...regionemeteur}
                        disabled={true}
                        className={styles.datePickerFormControl}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...paysemetteur}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.indicatif")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...paysindicatifemetteur}
                      />
                    </Col>
                  </Row>
                </div>
              )}
            </fieldset>

            <fieldset style={{ marginTop: "25px" }}>
              <legend>{t("form.legend.infosbeneficiare")}</legend>
              {successtransfert ? (
                <div>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.numidentibeneficiare")} :{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...numeroPieceIdentitebeneficiare}
                      />
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.numidtelbeneficiare")}:
                      </ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...numerotelebeneficiare}
                      />
                    </Col>
                  </Row>

                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.nom")} :</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...nombeneficiare}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.prenom")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...prenombeneficiare}
                      />
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                      <InputGroup>
                        <InputGroup.Addon>
                          <i className="fa fa-at" />
                        </InputGroup.Addon>
                        <FormControl
                          type="text"
                          disabled={true}
                          className={styles.datePickerFormControl}
                          {...emailbeneficiare}
                        />
                      </InputGroup>
                    </Col>
                  </Row>

                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.ville")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...villebeneficiare}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.region")}:</ControlLabel>
                      <FormControl
                        type="text"
                        {...regionbeneficiare}
                        disabled={true}
                        className={styles.datePickerFormControl}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...paysbeneficiare}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.indicatif")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...paysindicatifbeneficiare}
                      />
                    </Col>
                  </Row>
                </div>
              ) : (
                <div>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.numidentibeneficiare")}:
                      </ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...numeroPieceIdentitebeneficiare}
                        onBlur={(e) => {
                          e.preventDefault();
                          loadListTelClientben(
                            values.numeroPieceIdentitebeneficiare
                          );
                        }}
                      />
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.numidtelbeneficiare")}:
                      </ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...numerotelebeneficiare}
                      />
                    </Col>
                  </Row>

                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.nom")} :</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...nombeneficiare}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.prenom")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...prenombeneficiare}
                      />
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                      <InputGroup>
                        <InputGroup.Addon>
                          <i className="fa fa-at" />
                        </InputGroup.Addon>
                        <FormControl
                          type="text"
                          disabled={true}
                          className={styles.datePickerFormControl}
                          {...emailbeneficiare}
                        />
                      </InputGroup>
                    </Col>
                  </Row>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.ville")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...villebeneficiare}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.region")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        {...regionbeneficiare}
                        className={styles.datePickerFormControl}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...paysbeneficiare}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel>{t("form.label.indicatif")}:</ControlLabel>
                      <FormControl
                        type="text"
                        disabled={true}
                        className={styles.datePickerFormControl}
                        {...paysindicatifbeneficiare}
                      />
                    </Col>
                  </Row>
                </div>
              )}
            </fieldset>

            <fieldset style={{ marginTop: "25px" }}>
              <legend>{t("form.legend.infospercepteur")}</legend>
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>Numéro Identité Percepteur:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...numeroPieceIdentitepercepteur}
                  />
                </Col>

                <Col xs="12" md="6">
                  <ControlLabel>Numero tél percepteur:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...numtelpercepteur}
                  />
                </Col>
              </Row>

              <Row className={styles.fieldRow}>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.nom")} :</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...nompercepteur}
                  />
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.prenom")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...prenompercepteur}
                  />
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                  <InputGroup>
                    <InputGroup.Addon>
                      <i className="fa fa-at" />
                    </InputGroup.Addon>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...emailpercepteur}
                    />
                  </InputGroup>
                </Col>
              </Row>

              <Row className={styles.fieldRow}>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.ville")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...villepercepteur}
                  />
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...payspercepteur}
                  />
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.lienbeneficiare")}</ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...lientbeneficiaire}
                  />
                </Col>
              </Row>
            </fieldset>

            <fieldset style={{ marginTop: "25px" }}>
              <legend>{t("form.legend.infostransfert")}</legend>

              <Row className={styles.fieldRow}>
                {successtransfert ? (
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.reference")}</ControlLabel>
                    <FormControl
                      {...reference}
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      value={transfertDetails.reference}
                    />
                  </Col>
                ) : (
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.reference")}</ControlLabel>
                    <FormControl
                      {...reference}
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                    />
                  </Col>
                )}

                {successtransfert ? (
                  <Col xs="12" md="3">
                    <ControlLabel>
                      {t("form.label.montanttransfert")}:
                    </ControlLabel>
                    <FormControl
                      {...montantOperation}
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                    />
                  </Col>
                ) : (
                  <Col xs="12" md="3">
                    <ControlLabel>
                      {t("form.label.montanttransfert")}:
                    </ControlLabel>
                    <FormControl
                      {...montantOperation}
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                    />
                  </Col>
                )}

                <Col xs="12" md="4">
                  <ControlLabel>{t("form.label.fraienvoi")}:</ControlLabel>
                  <FormControl
                    type="text"
                    disabled={true}
                    className={styles.datePickerFormControl}
                    {...fraisenvois}
                  />
                </Col>

                {successreferencetransfert === true ? (
                  <Col xs="12" md="2">
                    <FormControl
                      type="checkbox"
                      checked={transfertDetails.fraiinclus}
                      style={{
                        width: "35px",
                        float: "left",
                        marginTop: "24px",
                      }}
                    />
                    <span style={{ padding: "30px 0 0 15px", float: "left" }}>
                      Frais inclus
                    </span>
                  </Col>
                ) : (
                  <Col xs="12" md="2">
                    <FormControl
                      type="checkbox"
                      {...fraiinclus}
                      style={{
                        width: "35px",
                        float: "left",
                        marginTop: "24px",
                      }}
                    />
                    <span style={{ padding: "30px 0 0 15px", float: "left" }}>
                      Frais inclus
                    </span>
                  </Col>
                )}
              </Row>

              {successtransfert ? (
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("form.label.monatntremise")}:
                    </ControlLabel>
                    <FormControl
                      type="number"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...montantremise}
                    />
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("form.label.motiftransfert")}:
                    </ControlLabel>
                    <FormControl
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...motif}
                    />
                  </Col>
                </Row>
              ) : (
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("form.label.monatntremise")}:
                    </ControlLabel>
                    <FormControl
                      type="number"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...montantremise}
                    />
                  </Col>

                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("form.label.motiftransfert")}:
                    </ControlLabel>
                    <FormControl
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...motif}
                    />
                  </Col>
                </Row>
              )}

              {successtransfert ? (
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("form.label.agentinitial")}</ControlLabel>
                    <FormControl
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...agent}
                      value={transfertDetails.agent}
                    />
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("form.label.agencecreation")}
                    </ControlLabel>
                    <FormControl
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...agence}
                      value={transfertDetails.agence}
                    />
                  </Col>
                </Row>
              ) : (
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("form.label.agentinitial")}</ControlLabel>
                    <FormControl
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...agent}
                    />
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("form.label.agencecreation")}
                    </ControlLabel>
                    <FormControl
                      type="text"
                      disabled={true}
                      className={styles.datePickerFormControl}
                      {...agence}
                    />
                  </Col>
                </Row>
              )}

              <div className="pull-right" style={{ marginTop: "15px" }}>
                {successreferencetransfert === true &&
                transfertDetails.statutPaiement === "nonpayer" ? (
                  <Button
                    loading={loadingpaiement}
                    onClick={(e) => {
                      e.preventDefault();
                      this.onSubmit(transfertDetails, values);
                      resetForm();
                    }}
                    bsStyle="primary"
                  >
                    <i className="fa fa-check " />
                    {t("form.buttons.confirmer")}
                  </Button>
                ) : (
                  <Button
                    loading={loadingpaiement}
                    disabled={true}
                    bsStyle="primary"
                  >
                    <i className="fa fa-check " />
                    {t("form.buttons.confirmer")}
                  </Button>
                )}

                <Button
                  bsStyle="primary"
                  onClick={(e) => {
                    this.redirectToHomePage(TB);
                  }}
                >
                  <i className="fa fa-times" /> {t("form.buttons.abandonner")}
                </Button>
                {successtransfert && (
                  <Button
                    bsStyle="primary"
                    style={{ marginRight: "0" }}
                    onClick={() =>
                      window.open(
                        baseUrl +
                          "transfertargentBNIF/createAvisTransfertPDF/" +
                          transfertDetails.id
                      )
                    }
                  >
                    <i className="fa fa-file-pdf-o fa-4" /> Upload PDF
                  </Button>
                )}
              </div>
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
