import { createValidator, required, notNumber, email, match, isValidNumber, minLength, lessThanForTextInput, moreThanForTextInput ,isValidAmount } from '../Commons/validationRules';

const SearchFormValidation = createValidator({
 montantMin:[ minLength(1), lessThanForTextInput('montantMax'), isValidNumber],
 montantMax:[ minLength(1), moreThanForTextInput('montantMin'), isValidNumber],

});
export default SearchFormValidation;