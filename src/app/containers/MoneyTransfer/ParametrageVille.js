import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Button,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  PanelGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./ParametrageReducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import ParametrageVilleValidator from "./Validateur/ParametrageVilleValidator";
import { AuthorizedComponent } from "react-router-role-authorization";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

@connect(
  (state) => ({
    dataForDetailville: state.ParametrageReducer.dataForDetailville,
    successinstanceville: state.ParametrageReducer.successinstanceville,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getInstancevilles: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
    showBouttonSupprimer: PropTypes.func,
    deleteTransfertCltCltByID: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    annulerTransfertCltCltByID: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      showModal: false,
      updating: false,
      deleting: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick() {
    try {
      if (this.state.deleting) {
        await this.props.deletevilles(this.props.rowData.id);
        this.props.loadListVilles();
      } else if (this.state.updating) {
        await this.props.getInstancevilles(this.props.rowData.id);
        //this.props.loadListVilles();
      }
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      saveville,
      loadListVilles,
      loadListPays,
      updatevilles,
      lisVilles,
      getInstancevilles,
      deletevilles,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
    };

    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: true,
                showModal: true,
                deleting: false,
              })
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>
          <Button
            bsSize="small"
            bsStyle="danger"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                deleting: true,
                showModal: true,
                updating: false,
              })
            }
          >
            <i className="fa fa-trash-o fa-fw" />
          </Button>
        </ButtonGroup>

        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {this.state.deleting && <div>{t("popup.supression.title")}</div>}
              {this.state.updating && (
                <div>{t("popup.modification.title")}</div>
              )}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.deleting && <div>{t("popup.supression.msgv")}</div>}
            {this.state.updating && <div>{t("popup.modification.msgv")}</div>}
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.supression.noBtn")}
              </Button>
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={this.handleClick}
              >
                {t("popup.supression.yesBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.loadListVilles()),
        dispatch(MoneyTransferAction.loadListRegion()),
      ]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",
    fields: ["libelle", "idRegion", "libellepays", "action"],
    validate: ParametrageVilleValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    state.ParametrageReducer.dataForDetailville &&
    state.ParametrageReducer.dataForDetailville !== null &&
    state.ParametrageReducer.dataForDetailville !== undefined && {
      initialValues: state.ParametrageReducer.dataForDetailville,
    }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    lisRegions: state.ParametrageReducer.lisRegions,
    saveville: state.ParametrageReducer.saveville,
    deletevilles: state.ParametrageReducer.deletevilles,
    updatevilles: state.ParametrageReducer.updatevilles,
    transfetList: state.moneyTransfer.transfetList,
    etatCaisse: state.moneyTransfer.etatCaisse,
    dataForDetailville: state.ParametrageReducer.dataForDetailville,
    successinstanceville: state.ParametrageReducer.successinstanceville,
    listCaisse: state.moneyTransfer.listCaisse,
    caisseDetails: state.moneyTransfer.caisseDetails,
    lisVilles: state.ParametrageReducer.lisVilles,
    listAdresse: state.ParametrageReducer.listAdresse,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
    libelle: "",
    libellepays: "",
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class ParametrageVille extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      numCaisse: "",
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
  }

  async onSubmit(values) {
    try {
      await this.props.saveville(values);
      this.props.loadListVilles();
    } catch (error) {
      console.log(error.message);
    }
  }

  async onSubmitTWO(values, idville) {
    try {
      await this.props.updatevilles(values, idville);
      this.props.loadListVilles();
      this.props.switchf();
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    const {
      fields: { libelle, libellepays, idRegion, action },
      handleSubmit,
      dataForDetailville,
      resetForm,
      successinstanceville,
      updating,
      deleting,
      saveville,
      deletevilles,
      loadListVilles,
      updatevilles,
      caisseDetails,
      openCaisse,
      userFrontDetails,
      successOpenCaisse,
      lisVilles,
      listAdresse,
      etatCaisse,
      getListCaisseByAgence,
      values,
      listCaisse,
      switchf,
      lisRegions,
      t,
    } = this.props;

    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const gridMetaData = [
      {
        columnName: "libelle",
        displayName: t("list.cols.libelle"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "nomRegion",
        displayName: t("list.cols.region"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "libellepays",
        displayName: t("list.cols.libellepays"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.legend.parametrageville")}</h2>
          </Col>
        </Row>
        <Row className={styles.compteCard}>
          <form className="formContainer">
            <fieldset>
              {successinstanceville ? (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.libelle")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libelle}
                      />
                      {libelle.error && libelle.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {libelle.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.region")}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...idRegion}
                      >
                        {lisRegions &&
                          lisRegions.length !== 0 &&
                          lisRegions.map((region) => (
                            <option value={region.id}>
                              {" "}
                              {region.libelle} | {region.nomPays}
                            </option>
                          ))}
                      </FormControl>
                    </Col>
                  </Row>

                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmitTWO(values, dataForDetailville.id);
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.modifier")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          values.libelle = "";
                          switchf();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              ) : (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.libelle")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libelle}
                      />
                      {libelle.error && libelle.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {libelle.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.region")}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...idRegion}
                      >
                        <option value="" hidden>
                          {t("form.hidden.selectionnerregion")}{" "}
                        </option>
                        {lisRegions &&
                          lisRegions.length !== 0 &&
                          lisRegions.map((region) => (
                            <option value={region.id}>
                              {" "}
                              {region.libelle} | {region.nomPays}
                            </option>
                          ))}
                      </FormControl>
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmit(values);
                          resetForm();
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.ajouter")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          values.libelle = "";
                          resetForm();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              )}
            </fieldset>
            <fieldset>
              <legend>{t("form.legend.listeville")}</legend>
              <Row className="table-responsive">
                <Griddle
                  results={lisVilles}
                  columnMetadata={gridMetaData}
                  useGriddleStyles={false}
                  noDataMessage={t("list.search.msg.noResult")}
                  resultsPerPage={10}
                  nextText={<i className="fa fa-chevron-right" />}
                  previousText={<i className="fa fa-chevron-left" />}
                  tableClassName="table"
                  columns={["libelle", "nomRegion", "libellepays", "action"]}
                />
              </Row>
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
