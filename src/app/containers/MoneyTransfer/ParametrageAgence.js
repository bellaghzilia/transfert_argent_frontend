import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Button,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  PanelGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./ParametrageReducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import ParametrageAgenceValidator from "./Validateur/ParametrageAgenceValidator";
import { AuthorizedComponent } from "react-router-role-authorization";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

@connect(
  (state) => ({
    lisAgence: state.ParametrageReducer.lisAgence,
    dataForDetailagence: state.ParametrageReducer.dataForDetailagence,
    successinstanceagence: state.ParametrageReducer.successinstanceagence,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getInstanragence: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
    showBouttonSupprimer: PropTypes.func,
    deleteTransfertCltCltByID: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    annulerTransfertCltCltByID: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      showModal: false,
      updating: false,
      deleting: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick() {
    try {
      if (this.state.deleting) {
        await this.props.deleteagences(this.props.rowData.id);
        this.props.loadListAgence();
      } else if (this.state.updating) {
        await this.props.getInstanragence(this.props.rowData.id);
      }
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      saveAgence,
      lisChefs,
      loadListRegion,
      loadListAgence,
      updateAgence,
      lisRegions,
      lisAgence,
      getInstanragence,
      deleteagences,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
    };

    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: true,
                showModal: true,
                deleting: false,
              })
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>
          <Button
            bsSize="small"
            bsStyle="danger"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                deleting: true,
                showModal: true,
                updating: false,
              })
            }
          >
            <i className="fa fa-trash-o fa-fw" />
          </Button>
        </ButtonGroup>

        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {this.state.deleting && <div>{t("popup.supression.title")}</div>}
              {this.state.updating && (
                <div>{t("popup.modification.title")}</div>
              )}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.deleting && <div>{t("popup.supression.msga")}</div>}
            {this.state.updating && <div>{t("popup.modification.msga")}</div>}
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.supression.noBtn")}
              </Button>
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={this.handleClick}
              >
                {t("popup.supression.yesBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.loadListChefAgence()),
        dispatch(MoneyTransferAction.loadListAgence()),
        dispatch(MoneyTransferAction.loadListVilles()),
      ]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",
    fields: [
      "nomAgence",
      "telAgence",
      "codeAgence",
      "adresse",
      "idcollaborateur",
      "idVille",
      "action",
    ],
    validate: ParametrageAgenceValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    state.ParametrageReducer.dataForDetailagence &&
    state.ParametrageReducer.dataForDetailagence !== null &&
    state.ParametrageReducer.dataForDetailagence !== undefined && {
      initialValues: state.ParametrageReducer.dataForDetailagence,
    }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    lisAgence: state.ParametrageReducer.lisAgence,
    saveAgence: state.ParametrageReducer.saveAgence,
    deleteagences: state.ParametrageReducer.deleteagences,
    updateAgence: state.ParametrageReducer.updateAgence,
    transfetList: state.moneyTransfer.transfetList,
    etatCaisse: state.moneyTransfer.etatCaisse,
    dataForDetailagence: state.ParametrageReducer.dataForDetailagence,
    successinstanceagence: state.ParametrageReducer.successinstanceagence,
    lisVilles: state.ParametrageReducer.lisVilles,
    caisseDetails: state.moneyTransfer.caisseDetails,
    lisChefs: state.ParametrageReducer.lisChefs,
    loadListAgence: state.ParametrageReducer.loadListAgence,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
    getInstanragence: state.moneyTransfer.getInstanragence,
    libelle: "",
    libellepays: "",
    nomcollaborateur: "",
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
    successsupdate: state.ParametrageReducer.successsupdate,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class ParametrageAgence extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.handlAnnuler = this.handlAnnuler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
  }

  handlAnnuler(event) {
    this.setState({ libelle: "" });
  }

  async onSubmit(values) {
    try {
      await this.props.saveAgence(values);
      this.props.loadListAgence();
      this.props.switchf();
      values.nomAgence = "";
      values.telAgence = "";
      values.codeAgence = "";
      values.libelleRegion = "";
      values.nomcollaborateur = "";
      values.idcollaborateur = "";
    } catch (error) {
      console.log(error.message);
    }
  }

  async onSubmitTWO(values, id) {
    try {
      await this.props.updateAgence(values, id);
      this.props.loadListAgence();
      this.props.switchf();
      setTimeout(() => {
        this.props.resetAlert();
      }, 2000);
    } catch (error) {
      console.log(error.message);
    }
  }

  /*componentDidMount(){
        if(this.props.successsupdate){
            setTimeout(() => {
                this.props.resetAlert();
            }, 2000);
        }
}*/
  render() {
    const {
      fields: {
        nomAgence,
        telAgence,
        codeAgence,
        idcollaborateur,
        idVille,
        adresse,
        action,
      },
      handleSubmit,
      dataForDetailagence,
      userFrontDetails,
      switchf,
      resetForm,
      successsupdate,
      resetAlert,
      successinstanceagence,
      updating,
      deleting,
      saveAgence,
      loadListAgence,
      deleteagences,
      lisChefs,
      updateAgence,
      caisseDetails,
      openCaisse,
      successOpenCaisse,
      lisRegions,
      lisAgence,
      etatCaisse,
      getListCaisseByAgence,
      getInstanragence,
      values,
      listCaisse,
      lisVilles,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const gridMetaData = [
      {
        columnName: "codeAgence",
        displayName: t("list.cols.codeAgence"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "nomAgence",
        displayName: t("list.cols.nomAgence"),

        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "telAgence",
        displayName: t("list.cols.telAgence"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "nomcollaborateur",
        displayName: t("list.cols.nomcollaborateur"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "libelleVille",
        displayName: t("list.cols.ville"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "libelleRegion",
        displayName: t("list.cols.region"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.legend.parametrageagence")}</h2>
          </Col>
        </Row>
        <Row className={styles.compteCard}>
          <form className="formContainer">
            {successsupdate === true && (
              <Col xs="12" md="12">
                <Alert bsStyle="success">
                  <strong>les données ont été modifiées avec succés </strong>
                </Alert>
              </Col>
            )}
            <fieldset>
              {successinstanceagence ? (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.codeAgence")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...codeAgence}
                      />
                      {codeAgence.error && codeAgence.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {codeAgence.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.nomAgence")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...nomAgence}
                      />
                      {nomAgence.error && nomAgence.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {nomAgence.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.collaborateur")}{" "}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...idcollaborateur}
                      >
                        <option value="" hidden>
                          Selectionner un collaborateur
                        </option>
                        {lisChefs &&
                          lisChefs.length !== 0 &&
                          lisChefs.map((collab) => (
                            <option value={collab.id}>
                              {" "}
                              {collab.nom + " " + collab.prenom}
                            </option>
                          ))}
                      </FormControl>
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.ville")}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...idVille}
                      >
                        <option value="" hidden>
                          {t("form.hidden.selectionnerville")}
                        </option>
                        {lisVilles &&
                          lisVilles.length !== 0 &&
                          lisVilles.map((ville) => (
                            <option value={ville.id}>
                              {" "}
                              {ville.libelle} | {ville.nomRegion}
                            </option>
                          ))}
                      </FormControl>
                    </Col>
                  </Row>

                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.telAgence")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...telAgence}
                      />
                      {telAgence.error && telAgence.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {telAgence.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.adresse")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...adresse}
                      />
                      {adresse.error && adresse.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {adresse.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmitTWO(values, dataForDetailagence.id);
                          resetForm();
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.modifier")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          switchf();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              ) : (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.codeAgence")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...codeAgence}
                      />
                      {codeAgence.error && codeAgence.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {codeAgence.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.nomAgence")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...nomAgence}
                      />
                      {nomAgence.error && nomAgence.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {nomAgence.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.collaborateur")}{" "}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...idcollaborateur}
                      >
                        <option value="" hidden>
                          Selectionné un collaborateur
                        </option>
                        {lisChefs &&
                          lisChefs.length !== 0 &&
                          lisChefs.map((collab) => (
                            <option value={collab.id}>
                              {" "}
                              {collab.nom + " " + collab.prenom}
                            </option>
                          ))}
                      </FormControl>
                    </Col>
                    <Col xs="12" md="3">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.ville")}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        className={styles.datePickerFormControl}
                        placeholder="select"
                        {...idVille}
                      >
                        <option value="" hidden>
                          {t("form.hidden.selectionnerregion")}
                        </option>
                        {lisVilles &&
                          lisVilles.length !== 0 &&
                          lisVilles.map((ville) => (
                            <option value={ville.id}>
                              {" "}
                              {ville.libelle} | {ville.nomRegion}
                            </option>
                          ))}
                      </FormControl>
                    </Col>
                  </Row>

                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.telAgence")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...telAgence}
                      />
                      {telAgence.error && telAgence.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {telAgence.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.adresse")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...adresse}
                      />
                      {adresse.error && adresse.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {adresse.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmit(values);
                          resetForm();
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.ajouter")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          values.libelle = "";
                          resetForm();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              )}
            </fieldset>
            <fieldset>
              <legend>{t("form.legend.listeagence")}</legend>
              <Row className="table-responsive">
                <Griddle
                  results={lisAgence}
                  columnMetadata={gridMetaData}
                  useGriddleStyles={false}
                  noDataMessage={t("list.search.msg.noResult")}
                  resultsPerPage={10}
                  nextText={<i className="fa fa-chevron-right" />}
                  previousText={<i className="fa fa-chevron-left" />}
                  tableClassName="table"
                  columns={[
                    "codeAgence",
                    "nomAgence",
                    "telAgence",
                    "nomcollaborateur",
                    "libelleVille",
                    "libelleRegion",
                    "action",
                  ]}
                />
              </Row>
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
