import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  InputGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import moment from "moment";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./moneytransferreducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import Button from "react-bootstrap-button-loader";
import * as UserActions from "../User/UserReducer";
import { asyncConnect } from "redux-connect";
import NewTransfertValidator from "./Validateur/NewTransfertValidator";
import { reset } from "redux-form";

var TBD = sessionStorage.getItem("myData4");

@reduxForm(
  {
    form: "NewMoneyTransfer",
    fields: [
      "numtelemetteur",
      "numeroPieceIdentiteemeteur",
      "nomemetteur",
      "prenomemeteur",
      "emailemetteur",
      "villeemetteur",
      "numtelbeneficiare",
      "numeroPieceIdentitebeneficiare",
      "nombeneficiare",
      "prenombeneficiare",
      "emailbeneficiare",
      ,
      "montanttransfere",
      "fraiinclus",
      "montantremise",
      "motiftransfert",
      "fraienvoi",
    ],
    validate: NewTransfertValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    state.moneyTransfer.successclient
      ? {
          initialValues: {
            numeroPieceIdentiteemeteur:
              state.moneyTransfer.clientDetails.numeroPieceIdentiteemeteur,
            numtelemetteur: state.moneyTransfer.clientDetails.numtelemetteur,
            nomemetteur: state.moneyTransfer.clientDetails.nomemetteur,
            prenomemeteur: state.moneyTransfer.clientDetails.prenomemeteur,
            emailemetteur: state.moneyTransfer.clientDetails.emailemetteur,
            numeroPieceIdentitebeneficiare:
              state.moneyTransfer.clientDetails.numeroPieceIdentitebeneficiare,
            numtelbeneficiare:
              state.moneyTransfer.clientDetails.numtelbeneficiare,
            nombeneficiare: state.moneyTransfer.clientDetails.nombeneficiare,
            prenombeneficiare:
              state.moneyTransfer.clientDetails.prenombeneficiare,
            emailbeneficiare:
              state.moneyTransfer.clientDetails.emailbeneficiare,
          },
        }
      : {
          initialValues: {
            fraiinclus: false,
            idemeteur: false,
          },
        }
)
@translate(["MoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    dataForDetail: state.moneyTransfer.dataForDetail,
    id: state.moneyTransfer.saveSuccessObject.id,
    updateSuccessObject: state.moneyTransfer.updateSuccessObject,
    loadingsaveexiste: state.moneyTransfer.loadingsaveexiste,
    loadingvalidestep: state.moneyTransfer.loadingvalidestep,
    loadingSigneTransfert: state.moneyTransfer.loadingSigneTransfert,
    successSigneTransfert: state.moneyTransfer.successSigneTransfert,
    message: state.moneyTransfer.message,
    soldegarantie: state.moneyTransfer.soldegarantie,
    userFrontDetails: state.user.userFrontDetails,
    successVirifierSolde: state.moneyTransfer.successVirifierSolde,
    TYPETB: state.moneyTransfer.TYPETB,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
export default class NewMoneyTransfer extends Component {
  constructor() {
    super();
    this.state = {
      buttonchoice: true,
      date: false,
      telphoneeme: "",
      telphoneben: "",
      showModal: false,
      pays: "",
      indicatifpays: "",
      montantTransfertError: false,
      montantRemiseError: false,
      numtelemetteurError: false,
      numtelbeneficiareError: false,
      regionNom: "",
      villeBen: "",
      montanttransfere: 0,
      montantremise: 0,
      isreq: false,
    };

    this.redirectToHomePage = this.redirectToHomePage.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
    this.handelAmountChange = this.handelAmountChange.bind(this);
    this.handelAmountRemiseChange = this.handelAmountRemiseChange.bind(this);
  }

  async signeTransfert(id, values) {
    this.setState({ showModal: false });
    await this.props.signeTransfertCltCltByID(id);
    this.props.getInstanceTransfert(id);
    setTimeout(() => {
      this.props.resetAlertSoldeInsufisante();
    }, 6000);
    this.props.reset("NewMoneyTransfer");
    this.props.resetForm();
    this.setState({
      montantremise: 0,
      montanttransfere: 0,
      villeBen: "",
      pays: "",
      regionNom: "",
    });
    this.props.switchf();
    setTimeout(() => {
      this.props.resetAlertSignature();
    }, 6000);
  }

  async onSubmitTWO(values, fraienvois) {
    // Here, we use an async/await approach that allows our code
    // to look synchronous which is always a plus. The code looks clean!
    try {
      if (
        values.montanttransfere != undefined &&
        values.montanttransfere != "" &&
        values.nombeneficiare != undefined &&
        values.nombeneficiare != "" &&
        values.nomemetteur != undefined &&
        values.nomemetteur != "" &&
        values.numtelemetteur != undefined &&
        values.numtelemetteur != "" &&
        values.motiftransfert != undefined &&
        values.motiftransfert != "" &&
        values.numtelbeneficiare != undefined &&
        values.numtelbeneficiare != "" &&
        fraienvois != undefined &&
        this.state.villeBen != ""
      ) {
        if (values.fraiinclus == undefined) {
          values.fraiinclus = false;
        }
        this.setState({ isreq: false });

        await this.props.validerSTEP(
          values,
          fraienvois.fraienvoi,
          this.state.villeBen
        );
        this.props.getInstance(this.props.saveSuccessValide.id);
        this.setState({
          montantremise: "0",
          montanttransfere: "0",
          villeBen: "",
          pays: "",
          regionNom: "",
        });

        if (saveSuccessValide.saveSuccess == true) {
          this.props.switchf();
          this.props.resetForm();
          this.setState({
            montantremise: "0",
            montanttransfere: "0",
            villeBen: "",
            pays: "",
            regionNom: "",
          });
          this.props.switchf();
        }
      } else {
        this.setState({ isreq: true });
        setTimeout(() => {
          this.setState({ isreq: false });
        }, 6000);
      }
    } catch (error) {
      // logging the error
      console.log(error.message);
    }
  }

  redirectToHomePage(TBD) {
    console.log("redirectToHomePage" + TBD);
    if (TBD == "TB_AGENT") {
      browserHistory.push(baseUrl + "app/TDBAgentCaisse");
    } else if (TBD == "TB_PARTENAIRE") {
      browserHistory.push(baseUrl + "app/TabeBordPartenaire");
    }
  }
  handelAmountChange(e) {
    let montanttransfere = this.state.montanttransfere;
    if (
      e &&
      e.target &&
      !isNaN(e.target.value) &&
      e.target.value > 0 &&
      !e.target.value.endsWith(".")
    ) {
      montanttransfere = parseFloat(e.target.value);
      this.setState({
        montanttransfere: montanttransfere,
      });
    }
  }

  handelAmountRemiseChange(e) {
    let montantremise = this.state.montantremise;
    if (e && e.target && e.target.value > 0) {
      montantremise = parseFloat(e.target.value);
      this.setState({
        montantremise: montantremise,
      });
    }
  }
  componentWillMount() {
    this.handelAmountChange();
    this.setState({
      montantremise: "0",
      montanttransfere: "0",
      villeBen: "",
      pays: "",
      regionNom: "",
    });
  }
  componentDidMount() {
    this.setState({
      montantremise: "0",
      montanttransfere: "0",
      villeBen: "",
      pays: "",
      regionNom: "",
    });
  }

  /*componentWillReceiveProps(){
    if (this.state.montantremise!==0 ) {
          this.setState({montantremise:"0",});

   
    }
}*/
  render() {
    const {
      fields: {
        numtelemetteur,
        numeroPieceIdentiteemeteur,
        nomemetteur,
        prenomemeteur,
        emailemetteur,
        villeemetteur,
        numtelbeneficiare,
        numeroPieceIdentitebeneficiare,
        nombeneficiare,
        prenombeneficiare,
        emailbeneficiare,
        montanttransfere,
        fraisenvoi,
        montantremise,
        motiftransfert,
        fraiinclus,
      },
      idtransfert,
      reset,
      TYPETB,
      resetAlertSoldeInsufisante,
      fraienvoi,
      handleSubmit,
      fraienvois,
      telDetails,
      telbenDetails,
      clientDetails,
      clientbenDetails,
      successtel,
      successtelben,
      successclient,
      successclientbene,
      successfraisenvois,
      successmontantremise,
      values,
      submitting,
      listBeneficiary,
      listDebitAccount,
      step,
      setState,
      setStateGAB,
      validerSTEP,
      saveexisteclient,
      getInstance,
      sentMessage,
      createAvisTransfertPDF,
      loadfraisenvois,
      loadmontantremise,
      loadListTelClient,
      loadListTelClientben,
      loadlClient,
      loadlClientbeneficiare,
      saveSuccessObject,
      saveSuccessValide,
      saveSuccess,
      moneyTransferObject,
      statut,
      signerDemande,
      codeErreur,
      maxdate,
      toStepX,
      dataForDetail,
      abandonnerDemande,
      instance,
      otpSMS,
      taskid,
      user,
      listBeneficiaryGAB,
      Request,
      saveUrl,
      updateUrl,
      isInstance,
      listeServiceTransfert,
      resetForm,
      params,
      t,
      signRedirect,
      signUrlred,
      taskId,
      loadingSigneTransfert,
      getInstanceTransfert,
      loadingsaveexiste,
      message,
      soldegarantie,
      successSigneTransfert,
      signeTransfertCltCltByID,
      loadingvalidestep,
      userFrontDetails,
      lisVilles,
      loadClient,
      loadClients,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const style = require("../Commons/styleKeyPad.scss");
    const url = baseUrl + "transfertargentBNIF/createAvisTransfertPDF/12";
    const isFormValid =
      values.montanttransfere != undefined &&
      values.montanttransfere != "" &&
      values.nombeneficiare != undefined &&
      values.nombeneficiare != "" &&
      values.nomemetteur != undefined &&
      values.nomemetteur != "" &&
      values.numtelemetteur != undefined &&
      values.numtelemetteur != "" &&
      values.motiftransfert != undefined &&
      values.motiftransfert != "" &&
      values.numtelbeneficiare != undefined &&
      values.numtelbeneficiare != "" &&
      fraienvois != undefined &&
      this.state.villeBen != "";

    const close = () => {
      this.setState({ showModal: false });
    };
    values.montanttransfere = this.state.montanttransfere;
    values.montantremise = this.state.montantremise;
    console.log("qq" + this.props.TYPETB);
    return (
      <div>
        <Row className={styles.compteCard}>
          <Wizard>{step}</Wizard>
          {userFrontDetails.caisseId === "" && TBD !== "TB_PARTENAIRE" && (
            <Alert bsStyle="danger">{t("msg.aucuncaisseauverte")}</Alert>
          )}
          {this.state.isreq && (
            <Alert bsStyle="danger">
              Veuillez remplir les champs obligatoires !!
            </Alert>
          )}
          {successmontantremise === false && montantremise.max !== null && (
            <Alert bsStyle="danger">{t("msg.saisirmontantinf")}</Alert>
          )}

          {step === "stepOne" ? (
            <form onSubmit={handleSubmit} className="formContainer">
              <fieldset>
                <legend>{t("form.legend.infosemeteur")}</legend>
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.numidentiemeteur")}:
                    </ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...numeroPieceIdentiteemeteur}
                    />
                  </Col>

                  <Col xs="12" md="6">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.numtelemeteur")}:
                    </ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...numtelemetteur}
                      onBlur={() => {
                        if (
                          values.numtelemetteur == undefined ||
                          values.numtelemetteur == ""
                        ) {
                          this.setState({ numtelemetteurError: true });
                        } else {
                          loadClient(values.numtelemetteur);
                        }
                      }}
                    />
                    {numtelemetteur.error && numtelemetteur.touched && (
                      <div className={styles.error}>
                        <i
                          className="fa fa-exclamation-triangle"
                          aria-hidden="true"
                        >
                          {numtelemetteur.error}
                        </i>
                      </div>
                    )}
                  </Col>
                </Row>

                <Row className={styles.fieldRow}>
                  <Col xs="12" md="3">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.nom")}:
                    </ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...nomemetteur}
                    />
                    {nomemetteur.error && nomemetteur.touched && (
                      <div className={styles.error}>
                        <i
                          className="fa fa-exclamation-triangle"
                          aria-hidden="true"
                        >
                          {nomemetteur.error}
                        </i>
                      </div>
                    )}
                  </Col>

                  <Col xs="12" md="3">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.prenom")} :
                    </ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...prenomemeteur}
                    />
                    {prenomemeteur.error && prenomemeteur.touched && (
                      <div className={styles.error}>
                        <i
                          className="fa fa-exclamation-triangle"
                          aria-hidden="true"
                        >
                          {prenomemeteur.error}
                        </i>
                      </div>
                    )}
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                    <InputGroup>
                      <InputGroup.Addon>
                        <i className="fa fa-at" />
                      </InputGroup.Addon>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...emailemetteur}
                      />
                      {emailemetteur.error && emailemetteur.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {emailemetteur.error}
                          </i>
                        </div>
                      )}
                    </InputGroup>
                  </Col>
                </Row>

                <Row className={styles.fieldRow}>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.ville")}:</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...villeemetteur}
                      value={userFrontDetails.villeNom}
                    />
                  </Col>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.region")}:</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      value={userFrontDetails.regionNom}
                    />
                  </Col>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      value={userFrontDetails.paysNom}
                    />
                  </Col>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.indicatif")}:</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      value={userFrontDetails.paysIndicatif}
                    />
                  </Col>
                </Row>
              </fieldset>

              <fieldset style={{ marginTop: "25px" }}>
                <legend> {t("form.legend.infosbeneficiare")}</legend>
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.numidentibeneficiare")}:
                    </ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...numeroPieceIdentitebeneficiare}
                    />
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.numidtelbeneficiare")}:
                    </ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...numtelbeneficiare}
                      onBlur={() => {
                        if (
                          (values.numtelbeneficiare == undefined ||
                            values.numtelbeneficiare == "") &&
                          (values.numtelemetteur == undefined ||
                            values.numtelemetteur == "")
                        ) {
                          this.setState({ numtelemetteurError: true });
                          this.setState({ numtelbeneficiareError: true });
                        } else {
                          loadClients(
                            values.numtelemetteur,
                            values.numtelbeneficiare
                          );
                        }
                      }}
                    />
                    {numtelbeneficiare.error && numtelbeneficiare.touched && (
                      <div className={styles.error}>
                        <i
                          className="fa fa-exclamation-triangle"
                          aria-hidden="true"
                        >
                          {numtelbeneficiare.error}
                        </i>
                      </div>
                    )}
                  </Col>
                </Row>

                <Row className={styles.fieldRow}>
                  <Col xs="12" md="3">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.nom")} :
                    </ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...nombeneficiare}
                    />
                    {nombeneficiare.error && nombeneficiare.touched && (
                      <div className={styles.error}>
                        <i
                          className="fa fa-exclamation-triangle"
                          aria-hidden="true"
                        >
                          {nombeneficiare.error}
                        </i>
                      </div>
                    )}
                  </Col>

                  <Col xs="12" md="3">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.prenom")}:
                    </ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...prenombeneficiare}
                    />
                    {prenombeneficiare.error && prenombeneficiare.touched && (
                      <div className={styles.error}>
                        <i
                          className="fa fa-exclamation-triangle"
                          aria-hidden="true"
                        >
                          {prenombeneficiare.error}
                        </i>
                      </div>
                    )}
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                    <InputGroup>
                      <InputGroup.Addon>
                        <i className="fa fa-at" />
                      </InputGroup.Addon>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...emailbeneficiare}
                      />
                      {emailbeneficiare.error && emailbeneficiare.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {emailbeneficiare.error}
                          </i>
                        </div>
                      )}
                    </InputGroup>
                  </Col>
                </Row>

                <Row className={styles.fieldRow}>
                  <Col xs="12" md="3">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.ville")}
                    </ControlLabel>
                    <FormControl
                      componentClass="select"
                      className={styles.datePickerFormControl}
                      placeholder="select"
                      onChange={(e) => {
                        for (var i = 0; i < lisVilles.length; i++) {
                          if (e.target.value == lisVilles[i].id) {
                            this.setState({ pays: lisVilles[i].libellepays });
                            this.setState({
                              indicatifpays: lisVilles[i].indicatifpays,
                            });
                            this.setState({ villeBen: lisVilles[i].id });
                            this.setState({
                              regionNom: lisVilles[i].nomRegion,
                            });
                            break;
                          }
                        }
                      }}
                    >
                      <option value="" hidden>
                        {t("form.hidden.selectionnerville")}
                      </option>
                      {lisVilles &&
                        lisVilles.length !== 0 &&
                        lisVilles.map((ville) => (
                          <option value={ville.id}> {ville.libelle} </option>
                        ))}
                    </FormControl>
                  </Col>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.region")}:</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      value={this.state.regionNom}
                    />
                  </Col>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      value={this.state.pays}
                    />
                  </Col>

                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.indicatif")}:</ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      value={this.state.indicatifpays}
                    />
                  </Col>
                </Row>
              </fieldset>

              <fieldset style={{ marginTop: "25px" }}>
                <legend>{t("form.legend.infostransfert")}</legend>
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.montanttransfert")}:
                    </ControlLabel>
                    <FormControl
                      {...montanttransfere}
                      type="number"
                      onChange={this.handelAmountChange}
                      value={this.state.montanttransfere}
                      className={styles.datePickerFormControl}
                      onBlur={(e) => {
                        e.preventDefault();
                        loadfraisenvois(values.montanttransfere);
                        if (
                          values.montanttransfere == undefined ||
                          values.montanttransfere == null
                        ) {
                          this.setState({ montantTransfertError: true });
                        }
                        //loadmontantremise(values.montantremise,values.montanttransfere);
                      }}
                    />
                    {this.state.montantTransfertError === true && (
                      <div className={styles.error}>
                        <i
                          className="fa fa-exclamation-triangle"
                          aria-hidden="true"
                        >
                          Ce champs est Obligatoire
                        </i>
                      </div>
                    )}
                  </Col>

                  {successfraisenvois ? (
                    <Col xs="12" md="4">
                      <ControlLabel>{t("form.label.fraienvoi")}:</ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...fraienvoi}
                        value={fraienvois.fraienvoi}
                      />
                    </Col>
                  ) : (
                    <Col xs="12" md="4">
                      <ControlLabel>{t("form.label.fraienvoi")}:</ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...fraienvoi}
                      />
                    </Col>
                  )}
                  <Col xs="12" md="2">
                    <FormControl
                      type="checkbox"
                      {...fraiinclus}
                      style={{
                        width: "35px",
                        float: "left",
                        marginTop: "24px",
                      }}
                    />
                    <span style={{ padding: "30px 0 0 15px", float: "left" }}>
                      Frais inclus
                    </span>
                  </Col>
                </Row>

                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("form.label.monatntremise")}:
                    </ControlLabel>
                    <FormControl
                      {...montantremise}
                      type="number"
                      onChange={this.handelAmountRemiseChange}
                      disabled={values.fraiinclus}
                      className={styles.datePickerFormControl}
                      onBlur={(e) => {
                        e.preventDefault();
                        loadmontantremise(
                          values.montanttransfere,
                          values.montantremise
                        );
                        if (
                          values.montantremise == undefined ||
                          values.montantremise == null
                        ) {
                          this.setState({ montantRemiseError: true });
                        }
                      }}
                    />
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel className={"requiredInput"}>
                      {t("form.label.motiftransfert")}:
                    </ControlLabel>
                    <FormControl
                      type="text"
                      className={styles.datePickerFormControl}
                      {...motiftransfert}
                    />
                    {motiftransfert.error && motiftransfert.touched && (
                      <div className={styles.error}>
                        <i
                          className="fa fa-exclamation-triangle"
                          aria-hidden="true"
                        >
                          {motiftransfert.error}
                        </i>
                      </div>
                    )}
                  </Col>
                </Row>

                <div className="pull-right" style={{ marginTop: "15px" }}>
                  <Button
                    loading={loadingvalidestep}
                    disabled={successmontantremise === false}
                    onClick={(e) => {
                      this.onSubmitTWO(values, fraienvois);
                    }}
                    bsStyle="primary"
                  >
                    <i className="fa fa-check " />
                    {t("form.buttons.confirmer")}
                  </Button>

                  <Button
                    bsStyle="primary"
                    onClick={(e) => {
                      this.redirectToHomePage(this.props.TYPETB);
                    }}
                  >
                    <i className="fa fa-reply" /> {t("form.buttons.retour")}
                  </Button>
                </div>
              </fieldset>
            </form>
          ) : (
            dataForDetail && (
              <div>
                {successSigneTransfert === true && soldegarantie !== false && (
                  <Alert bsStyle="success">{t("msg.signedSuccess")}</Alert>
                )}
                <form className="formContainer">
                  <fieldset>
                    <legend>{t("form.legend.infosemeteur")}</legend>
                    <Row>
                      <Col xs={12} md={4}>
                        <ControlLabel>
                          {t("form.label.numidentiemeteur")}
                        </ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.numeroPieceIdentiteemeteur}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>
                          {t("form.label.numtelemeteur")}
                        </ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.numtelemetteur}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.nom")}</ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.nomemetteur}
                        </p>
                      </Col>
                    </Row>

                    <Row>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.prenom")}</ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.prenomemeteur}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.mail")}</ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.emailemetteur}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.ville")}</ControlLabel>
                        <p className="detail-value">
                          {" "}
                          {dataForDetail.villeemetteur}
                        </p>
                      </Col>
                    </Row>

                    <Row>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.region")}</ControlLabel>
                        <p className="detail-value">
                          {" "}
                          {dataForDetail.regionemeteur}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.pays")}</ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.paysemetteur}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.indicatif")}</ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.paysindicatifemetteur}
                        </p>
                      </Col>
                    </Row>
                  </fieldset>

                  <fieldset style={{ marginTop: "25px" }}>
                    <legend>{t("form.legend.infosbeneficiare")}</legend>
                    <Row>
                      <Col xs={12} md={4}>
                        <ControlLabel>
                          {t("form.label.numidentibeneficiare")}
                        </ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.numeroPieceIdentitebeneficiare}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>
                          {t("form.label.numidtelbeneficiare")}
                        </ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.numerotelebeneficiare}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.nom")}</ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.nombeneficiare}
                        </p>
                      </Col>
                    </Row>

                    <Row>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.prenom")}</ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.prenombeneficiare}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.emailbeneficiare}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.ville")}</ControlLabel>
                        <p className="detail-value">
                          {" "}
                          {dataForDetail.villebeneficiare}
                        </p>
                      </Col>
                    </Row>

                    <Row>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.region")}</ControlLabel>
                        <p className="detail-value">
                          {" "}
                          {dataForDetail.regionbeneficiare}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.pays")}</ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.paysbeneficiare}
                        </p>
                      </Col>
                      <Col xs={12} md={4}>
                        <ControlLabel>{t("form.label.indicatif")}</ControlLabel>
                        <p className="detail-value">
                          {dataForDetail.paysindicatifbeneficiare}
                        </p>
                      </Col>
                    </Row>
                  </fieldset>

                  <fieldset style={{ marginTop: "25px" }}>
                    <legend>{t("form.legend.infostransfert")}</legend>
                    <Col xs={12} md={3}>
                      <ControlLabel>
                        {t("form.label.montanttransfert")}
                      </ControlLabel>
                      <p className="detail-value">
                        {dataForDetail.montantOperation}
                      </p>
                    </Col>
                    <Col xs={12} md={3}>
                      <ControlLabel>{t("form.label.fraienvoi")}</ControlLabel>
                      <p className="detail-value">
                        {dataForDetail.fraisenvois}
                      </p>
                    </Col>
                    <Col xs={12} md={3}>
                      <ControlLabel>
                        {t("form.label.monatntremise")}
                      </ControlLabel>
                      <p className="detail-value">
                        {dataForDetail.montantremise === "undefined" ||
                        dataForDetail.montantremise === "null"
                          ? 0
                          : dataForDetail.montantremise}
                      </p>
                    </Col>
                    <Col xs={12} md={3}>
                      <ControlLabel>{t("form.label.motifRemise")}</ControlLabel>
                      <p className="detail-value">{dataForDetail.motif}</p>
                    </Col>
                  </fieldset>

                  {step !== "stepThree" && (
                    <fieldset>
                      <Row>
                        {successSigneTransfert === false &&
                          message !== "msg" && (
                            <Alert bsStyle="danger">{message}</Alert>
                          )}
                        {soldegarantie === false && (
                          <Alert bsStyle="danger">
                            {t("msg.soldegarantieinsuffisant")}
                          </Alert>
                        )}
                        <Alert>{t("msg.actionsHelp")}</Alert>
                      </Row>

                      <div className="pull-right" style={{ marginTop: "15px" }}>
                        <Button
                          loading={loadingSigneTransfert}
                          disabled={dataForDetail.statutOperation === "signer"}
                          onClick={() => this.setState({ showModal: true })}
                          bsStyle="primary"
                        >
                          <i
                            className={
                              "fa " +
                              (submitting ? "fa-cog fa-spin" : "fa fa-check")
                            }
                          />
                          {t("form.buttons.confirmersigner")}
                        </Button>
                        <Button
                          disabled={dataForDetail.statutOperation === "signer"}
                          onClick={() =>
                            browserHistory.push(
                              baseUrl +
                                "app/moneyTransfer/update/" +
                                dataForDetail.id
                            )
                          }
                          bsStyle="primary"
                        >
                          <i className="fa fa-pencil-square-o" />
                          {t("form.buttons.modifier")}
                        </Button>
                        <Button
                          bsStyle="primary"
                          disabled={dataForDetail.statutOperation === "signer"}
                          onClick={(e) => {
                            this.redirectToHomePage(this.props.TYPETB);
                          }}
                        >
                          <i className="fa fa-reply" />{" "}
                          {t("form.buttons.retour")}
                        </Button>
                        <Button
                          bsStyle="primary"
                          style={{ marginRight: "0" }}
                          onClick={() =>
                            window.open(
                              baseUrl +
                                "transfertargentBNIF/createAvisTransfertPDF/" +
                                dataForDetail.id
                            )
                          }
                        >
                          <i className="fa fa-file-pdf-o fa-4" />
                          {t("form.buttons.uploadpdf")}
                        </Button>
                      </div>
                    </fieldset>
                  )}
                  <Modal
                    show={this.state.showModal}
                    onHide={close}
                    container={this}
                    aria-labelledby="contained-modal-title"
                  >
                    <Modal.Header closeButton>
                      <Modal.Title id="contained-modal-title">
                        <div>{t("popup.signature.title")}</div>
                      </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div>{t("popup.signature.msg")}</div>
                    </Modal.Body>
                    <Modal.Footer>
                      <ButtonGroup className="pull-right" bsSize="small">
                        <Button
                          className={styles.ButtonPasswordStyle}
                          onClick={() => close()}
                        >
                          Non
                        </Button>
                        <Button
                          className={styles.ButtonPasswordStyle}
                          onClick={() => {
                            this.signeTransfert(dataForDetail.id, values);
                          }}
                        >
                          Oui
                        </Button>
                      </ButtonGroup>
                    </Modal.Footer>
                  </Modal>
                </form>
              </div>
            )
          )}
        </Row>
      </div>
    );
  }
}
