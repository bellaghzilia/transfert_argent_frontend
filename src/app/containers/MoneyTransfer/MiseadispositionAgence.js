import React, { Component } from "react";
import PropTypes from "prop-types";

import { Col, ControlLabel, Alert, FormControl, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { initializeWithKey } from "redux-form";
import { asyncConnect } from "redux-connect";
import { browserHistory } from "react-router";
import * as MoneyTransferAction from "./moneytransferreducer";
import { translate } from "react-i18next";
import Button from "react-bootstrap-button-loader";
import moment from "moment";
import * as UserActions from "../User/UserReducer";
import { AuthorizedComponent } from "react-router-role-authorization";

var result = sessionStorage.getItem("myData");
@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];
      if (result == null) {
        promises.push(
          dispatch(
            MoneyTransferAction.loadAgence(
              getState().user.userFrontDetails.agenceId
            )
          )
        );
      } else {
        promises.push(dispatch(MoneyTransferAction.loadAgence(result)));
      }
      promises.push(dispatch(MoneyTransferAction.loadListAgence()));
      return Promise.all(promises);
    },
  },
])
@connect(
  (state) => ({
    refMADA: state.moneyTransfer.refMADA,
    agenceObj: state.moneyTransfer.agenceObj,
    listAgence: state.moneyTransfer.listAgence,
    loadingMiseadispoAgence: state.moneyTransfer.loadingMiseadispoAgence,
    successMiseadispoAgence: state.moneyTransfer.successMiseadispoAgence,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@translate(["MoneyTransfer"], { wait: true })
export default class MiseadispositionAgence extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      idAgence: "",
      montant: "",
      motif: "",
      formValid: false,
    };

    this.isFormValid = this.isFormValid.bind(this);
    this.handleMotifChange = this.handleMotifChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleMontantChange = this.handleMontantChange.bind(this);
  }

  handleMontantChange(event) {
    event.preventDefault();
    this.setState({ montant: event.target.value }, function() {
      this.isFormValid();
    });
  }

  handleMotifChange(event) {
    event.preventDefault();
    this.setState({ motif: event.target.value }, function() {
      this.isFormValid();
    });
  }

  handleSelectChange(event) {
    event.preventDefault();
    this.setState({ idAgence: event.target.value }, function() {
      this.isFormValid();
    });
  }

  isFormValid() {
    if (
      this.state.montant.length > 0 &&
      this.state.motif.length > 0 &&
      this.state.idAgence.length > 0
    )
      this.setState({ formValid: true });
    else this.setState({ formValid: false });
  }

  render() {
    const {
      t,
      listAgence,
      userFrontDetails,
      agenceObj,
      refMADA,
      successMiseadispoAgence,
      loadingMiseadispoAgence,
      miseadispoAgenceAgence,
    } = this.props;
    const styles = require("./moneytransfer.scss");

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.titleForm.miseadispositonagence")} </h2>
          </Col>
        </Row>

        <Row>
          {successMiseadispoAgence === true && (
            <Alert bsStyle="info">
              {t("msg.sucessemiseadisposition")} <br />
              {t("form.label.referenceoperation")} : <b>{refMADA}</b>
            </Alert>
          )}
          {successMiseadispoAgence === false && (
            <Alert bsStyle="danger">{t("msg.ereursolde")}</Alert>
          )}
          {successMiseadispoAgence == "null" && (
            <Alert bsStyle="danger">{t("msg.failedmisedisposition")}</Alert>
          )}
          {userFrontDetails.etatAgence === "CLOSE" && (
            <Alert bsStyle="warning">{t("msg.ouvertureagence")}</Alert>
          )}
        </Row>

        <Row className="detailsBloc">
          <Col xs={12} md={3}>
            <ControlLabel>{t("form.label.dateaujourdui")}: </ControlLabel>
            <p className="detail-value">{moment().format("DD/MM/YYYY")}</p>
          </Col>
          <Col xs={12} md={3}>
            <ControlLabel>{t("form.label.agence")}: </ControlLabel>
            <p className="detail-value">{agenceObj.nomAgence}</p>
          </Col>
          <Col xs={12} md={3}>
            <ControlLabel>{t("form.label.chefagence")}: </ControlLabel>
            <p className="detail-value">{agenceObj.nomAgent}</p>
          </Col>
        </Row>

        <Row style={{ padding: "0 10px" }}>
          <form className="formContainer" style={{ padding: "30px 20px 20px" }}>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="4">
                <ControlLabel>
                  {t("form.label.agencedestination")}:
                </ControlLabel>
                <FormControl
                  componentClass="select"
                  className={styles.datePickerFormControl}
                  placeholder="select"
                  onChange={this.handleSelectChange}
                >
                  <option value="" hidden>
                    {t("form.hidden.selectionneragence")}
                  </option>
                  {listAgence &&
                    listAgence.length !== 0 &&
                    listAgence.map(
                      (agence) =>
                        agence.id !== agenceObj.id && (
                          <option value={agence.id}>{agence.nomAgence}</option>
                        )
                    )}
                </FormControl>
              </Col>
              <Col xs="12" md="4">
                <ControlLabel>{t("form.label.montant")} : </ControlLabel>
                <FormControl
                  type="text"
                  className={styles.datePickerFormControl}
                  onChange={this.handleMontantChange}
                />
              </Col>
              <Col xs="12" md="4">
                <ControlLabel>{t("form.label.motif")} : </ControlLabel>
                <FormControl
                  type="text"
                  className={styles.datePickerFormControl}
                  onChange={this.handleMotifChange}
                />
              </Col>
            </Row>

            <Row className={styles.fieldRow}>
              <div className="pull-right" style={{ paddingTop: "10px" }}>
                <Button
                  loading={loadingMiseadispoAgence}
                  disabled={
                    !this.state.formValid ||
                    userFrontDetails.etatAgence === "CLOSE"
                  }
                  onClick={(e) => {
                    e.preventDefault();
                    miseadispoAgenceAgence(
                      agenceObj.id,
                      this.state.idAgence,
                      this.state.montant,
                      this.state.motif
                    );
                  }}
                  bsStyle="primary"
                >
                  {t("form.buttons.confirmer")}
                </Button>
                <Button
                  onClick={() => {
                    browserHistory.push(baseUrl + "app/TabeBordChefAgence");
                  }}
                  bsStyle="primary"
                >
                  <i className="fa fa-times" />
                  {t("form.buttons.cancel")}
                </Button>
                {successMiseadispoAgence === true && (
                  <a
                    className="glyphicon glyphicon-upload btn btn-primary"
                    href={
                      baseUrl +
                      "agencetransfertargent/createAvisTransfertPDF/" +
                      refMADA
                    }
                  >
                    {" "}
                    {t("form.buttons.uploadpdf")}
                  </a>
                )}
              </div>
            </Row>
          </form>
        </Row>
      </div>
    );
  }
}
