import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Button,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  PanelGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./ParametrageReducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import ParametrageRoleValidator from "./Validateur/ParametrageRoleValidator";
import { AuthorizedComponent } from "react-router-role-authorization";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

@connect(
  (state) => ({
    listPartenaire: state.ParametrageReducer.listPartenaire,
    successinstancerole: state.ParametrageReducer.successinstancerole,
    dataForDetailshema: state.ParametrageReducer.dataForDetailshema,
    id: state.moneyTransfer.saveSuccessObject.id,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getInstancePartenaire: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
    showBouttonSupprimer: PropTypes.func,
    deleteTransfertCltCltByID: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    annulerTransfertCltCltByID: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      showModal: false,
      updating: false,
      deleting: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick() {
    try {
      if (this.state.deleting) {
        await this.props.deletePartenaire(this.props.rowData.id);
        this.props.loadListPartenaireUser();
      } else if (this.state.updating) {
        await this.props.getInstancePartenaire(this.props.rowData.id);
      }
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      savePartnaire,
      listPartenaire,
      loadListRegion,
      loadListAgence,
      userFrontDetails,
      loadListPartenaireUser,
      updatePartenaire,
      lisRegions,
      getInstancePartenaire,
      deletePartenaire,
      t,
      successsupdate,
      resetAlert,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
    };

    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: true,
                showModal: true,
                deleting: false,
              })
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>
          <Button
            bsSize="small"
            bsStyle="danger"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                deleting: true,
                showModal: true,
                updating: false,
              })
            }
          >
            <i className="fa fa-trash-o fa-fw" />
          </Button>
        </ButtonGroup>

        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {this.state.deleting && <div>{t("popup.supression.title")}</div>}
              {this.state.updating && (
                <div>{t("popup.modification.title")}</div>
              )}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.deleting && <div>{t("popup.supression.msgpa")}</div>}
            {this.state.updating && <div>{t("popup.modification.msgpa")}</div>}
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.supression.noBtn")}
              </Button>
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={this.handleClick}
              >
                {t("popup.supression.yesBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([
        dispatch(MoneyTransferAction.loadListPartenaireUser()),
        dispatch(MoneyTransferAction.loadListAgence()),
      ]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",

    fields: [
      "firstName",
      "lastName",
      "userName",
      "password",
      "montantremisemax",
      "tauxremise",
      "numeroidentite",
      "telephone",
      "email",
      "partenaire",
      "commissiontransfert",
      "commissionpaiement",
    ],
    validate: ParametrageRoleValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    state.ParametrageReducer.dateForDetailPartenaire &&
    state.ParametrageReducer.dateForDetailPartenaire !== null &&
    state.ParametrageReducer.dateForDetailPartenaire !== undefined
      ? {
          initialValues: state.ParametrageReducer.dateForDetailPartenaire,
        }
      : {
          initialValues: {
            firstName: "",
            lastName: "",
            userName: "",
            password: "",
            idAgence: "",
            nomAgence: "",
            partenaire: false,
            commissiontransfert: "",
            commissionpaiement: "",
            montantremisemax: "",
            tauxremise: "",
            numeroidentite: "",
          },
        }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    savePartnaire: state.ParametrageReducer.savePartnaire,
    userFrontDetails: state.user.userFrontDetails,
    deletePartenaire: state.ParametrageReducer.deletePartenaire,
    updatePartenaire: state.ParametrageReducer.updatePartenaire,
    transfetList: state.moneyTransfer.transfetList,
    etatCaisse: state.moneyTransfer.etatCaisse,
    successinstanceshema: state.ParametrageReducer.successinstanceshema,
    successinstancerole: state.ParametrageReducer.successinstancerole,
    listCaisse: state.moneyTransfer.listCaisse,
    caisseDetails: state.moneyTransfer.caisseDetails,
    listPartenaire: state.ParametrageReducer.listPartenaire,
    lisRegions: state.ParametrageReducer.lisRegions,
    loadListPartenaireUser: state.ParametrageReducer.loadListPartenaireUser,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
    dateForDetailPartenaire: state.ParametrageReducer.dateForDetailPartenaire,
    roless: state.ParametrageReducer.roless,
    lisAgence: state.ParametrageReducer.lisAgence,
    getInstancePartenaire: state.moneyTransfer.getInstancePartenaire,
    libelle: "",
    libellepays: "",
    nomcollaborateur: "",
    user: state.user,
    successsupdate: state.ParametrageReducer.successsupdate,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class ParametragePartenaires extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      checked: false,
      partenaire: false,
      enabled: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
  }

  async onSubmit(values, idagence) {
    try {
      await this.props.savePartnaire(values, idagence);
      this.props.loadListPartenaireUser();
      this.props.switchf();
    } catch (error) {
      console.log(error.message);
    }
  }

  async onSubmitTWO(values, id, idUser) {
    try {
      await this.props.updatePartenaire(values, id, idUser);
      this.props.loadListPartenaireUser();
      this.props.switchf();
      setTimeout(() => {
        this.props.resetAlert();
      }, 20000);
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    const {
      userFrontDetails,
      fields: {
        firstName,
        lastName,
        userName,
        password,
        montantremisemax,
        numeroidentite,
        telephone,
        email,
        tauxremise,
        partenaire,
        commissiontransfert,
        commissionpaiement,
        idAgence,
      },
      handleSubmit,
      switchf,
      successinstancerole,
      dateForDetailPartenaire,
      roless,
      resetForm,
      updating,
      deleting,
      savePartnaire,
      loadListPartenaireUser,
      deletePartenaire,
      lisAgence,
      listType,
      updatePartenaire,
      caisseDetails,
      openCaisse,
      successOpenCaisse,
      lisRegions,
      listPartenaire,
      etatCaisse,
      getListCaisseByAgence,
      getInstancePartenaire,
      values,
      listCaisse,
      t,
      successsupdate,
      resetAlert,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };

    const gridMetaData = [
      {
        columnName: "firstName",
        displayName: t("list.cols.nom"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "lastName",
        displayName: t("list.cols.prenom"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "userName",
        displayName: t("list.cols.userName"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "numeroidentite",
        displayName: t("list.cols.numeriidentite"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "telephone",
        displayName: t("list.cols.telephone"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "email",
        displayName: t("list.cols.mail"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "commissiontransfert",
        displayName: t("list.cols.commissiontransfert"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "commissionpaiement",
        displayName: t("list.cols.commissionpaiement"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.legend.parametragepartenaires")}</h2>
          </Col>
        </Row>

        {successsupdate === true && (
          <Col>
            <Alert bsStyle="success">
              Les données ont été modifiées avec succés
            </Alert>
          </Col>
        )}

        <Row>
          <form className="formContainer">
            <fieldset>
              <Row className={styles.paddingColumn}>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.nom")}
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...firstName}
                  />
                  {firstName.error && firstName.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {firstName.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.prenom")}
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...lastName}
                  />
                  {lastName.error && lastName.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {lastName.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.username")}
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...userName}
                  />
                  {userName.error && userName.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {userName.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.password")}
                  </ControlLabel>
                  <FormControl
                    type="password"
                    className={styles.datePickerFormControl}
                    {...password}
                  />
                  {password.error && password.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {password.error}
                      </i>
                    </div>
                  )}
                </Col>
              </Row>

              <Row className={styles.paddingColumn}>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.numeriidentite")}
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...numeroidentite}
                  />
                  {firstName.error && firstName.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {firstName.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.telephone")}
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...telephone}
                  />
                  {telephone.error && telephone.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {telephone.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.mail")}
                  </ControlLabel>
                  <FormControl
                    type="text"
                    className={styles.datePickerFormControl}
                    {...email}
                  />
                  {email.error && email.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {email.error}
                      </i>
                    </div>
                  )}
                </Col>
              </Row>

              <Row className={styles.paddingColumn}>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.montantremisemax")}
                  </ControlLabel>
                  <FormControl
                    type="number"
                    className={styles.datePickerFormControl}
                    {...montantremisemax}
                  />
                  {montantremisemax.error && montantremisemax.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {montantremisemax.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.tauxmax")}
                  </ControlLabel>
                  <FormControl
                    type="number"
                    className={styles.datePickerFormControl}
                    {...tauxremise}
                  />
                  {tauxremise.error && tauxremise.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {tauxremise.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.commissiontransfert")}{" "}
                  </ControlLabel>
                  <FormControl
                    type="number"
                    className={styles.datePickerFormControl}
                    {...commissiontransfert}
                  />
                  {tauxremise.error && tauxremise.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {tauxremise.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel className={"requiredInput"}>
                    {t("form.label.commissionpaiement")}{" "}
                  </ControlLabel>
                  <FormControl
                    type="number"
                    className={styles.datePickerFormControl}
                    {...commissionpaiement}
                  />
                  {tauxremise.error && tauxremise.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {tauxremise.error}
                      </i>
                    </div>
                  )}
                </Col>
              </Row>

              <Row className={styles.paddingColumn}>
                <div className="pull-right" style={{ paddingTop: "10px" }}>
                  {successinstancerole ? (
                    <Button
                      bsStyle="primary"
                      onClick={handleSubmit((e) => {
                        this.onSubmitTWO(
                          values,
                          dateForDetailPartenaire.id,
                          dateForDetailPartenaire.idUser
                        );
                        resetForm();
                      })}
                    >
                      <i className="fa fa-check " />
                      {t("form.buttons.modifier")}
                    </Button>
                  ) : (
                    <Button
                      bsStyle="primary"
                      onClick={handleSubmit((e) => {
                        this.onSubmit(values, userFrontDetails.agenceId);
                        resetForm();
                      })}
                    >
                      <i className="fa fa-check " />
                      {t("form.buttons.ajouter")}
                    </Button>
                  )}

                  <Button
                    bsStyle="primary"
                    onClick={() => {
                      resetForm();
                      switchf();
                    }}
                  >
                    <i className="fa fa-times" />
                    {t("form.buttons.annuler")}
                  </Button>
                </div>
              </Row>
            </fieldset>

            <fieldset style={{ marginTop: "15px" }}>
              <legend>{t("form.legend.listepartenaire")}</legend>
              <Row className="table-responsive">
                <Griddle
                  results={listPartenaire}
                  columnMetadata={gridMetaData}
                  useGriddleStyles={false}
                  noDataMessage={"Aucun résultat trouvé"}
                  resultsPerPage={10}
                  nextText={<i className="fa fa-chevron-right" />}
                  previousText={<i className="fa fa-chevron-left" />}
                  tableClassName="table"
                  columns={[
                    "firstName",
                    "lastName",
                    "userName",
                    "numeroidentite",
                    "telephone",
                    "email",
                    "commissiontransfert",
                    "commissionpaiement",
                    "action",
                  ]}
                />
              </Row>
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
