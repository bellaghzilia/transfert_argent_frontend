// import React, {Component, PropTypes}         from 'react';
// import {Label,Col, Panel,Row,  ButtonGroup,  Alert,  Modal,ControlLabel,Form,FormControl,FormGroup, PanelGroup} from 'react-bootstrap';
// import {translate}                            from "react-i18next";
// import {asyncConnect}                         from 'redux-connect';
// import {connect}                              from 'react-redux';
// import {initializeWithKey, reduxForm}         from 'redux-form';
// import Griddle                                from 'griddle-react';
// import {browserHistory}                       from 'react-router';
// import DatePicker                             from 'react-datepicker';
// import moment                                 from 'moment';
// import gridPagination                         from './MoneyTransferPagination';
// import SearchFormValidation                   from './SearchFormValidation';
// import * as MoneyTransferAction               from './moneytransferreducer';
// import Button                                 from 'react-bootstrap-button-loader';
// import * as UserActions                       from '../User/UserReducer';
// var myData1 = sessionStorage.getItem('myData1');
// var myData2 = sessionStorage.getItem('myData2');
// var myData3 = sessionStorage.getItem('myData3');
// var myData4 = sessionStorage.getItem('myData4');
// var myData  =  sessionStorage.getItem('myData');
// var idCaisse;
// var idColaborateur;var TB;
// var numCaisse
// var idAgence
// if (myData1 != null && myData2 != null && myData3 != null && myData != null) {
//     idCaisse = myData1;
//     numCaisse = myData2;
//     idColaborateur = myData3;
//     idAgence = myData;
// } else {
//     var url = window.location.href;
//     var captured;
//     var captured1;
//     var captured2;
//     var captured3;
//     var captured4;
//     captured = /idCaisse=([^&]+)/.exec(url)[1];
//     captured1 = /numCaisse=([^&]+)/.exec(url)[1];
//     captured2 = /idAgence=([^&]+)/.exec(url)[1];
//     captured3 = /idColaborateur=([^&]+)/.exec(url)[1];
//     captured4 = /TB=([^&]+)/.exec(url)[1];
//     window.sessionStorage.setItem("myData1", captured ? captured : 'null');
//     window.sessionStorage.setItem("myData2", captured1 ? captured1 : 'null');
//     window.sessionStorage.setItem("myData", captured2 ? captured2 : 'null');
//     window.sessionStorage.setItem("myData3", captured3 ? captured3 : 'null');
//     window.sessionStorage.setItem("myData4", captured4 ? captured4 : 'null');
// // getter
//     idCaisse = sessionStorage.getItem('myData1');
//     numCaisse = sessionStorage.getItem('myData2');
//     idAgence = sessionStorage.getItem('myData');
//     idColaborateur = sessionStorage.getItem('myData3');
//     TB = sessionStorage.getItem('myData4');
// }
//
//
// @asyncConnect([{
//     deferred: false,
//     promise: ({location,store: {dispatch}}) => {
//         console.log("captured4 ");
//         console.log(captured4);
//         return Promise.all([
//             dispatch(MoneyTransferAction.switchf()),
//             dispatch(MoneyTransferAction.typeTB(TB)),
//
//
//
//         ]);
//     }
// }])
//
// @connect(
//     state => ({
//         userFrontDetails: state.user.userFrontDetails,
//         dataForDetail: state.moneyTransfer.dataForDetail,
//         loadingSigneTransfert: state.moneyTransfer.loadingSigneTransfert,
//         compteGarantie: state.moneyTransfer.compteGarantie,
//         TYPETB: state.moneyTransfer.TYPETB,
//         successSigneTransfert: state.moneyTransfer.successSigneTransfert,
//         id: state.moneyTransfer.saveSuccessObject.id,
//     }),
//     {...MoneyTransferAction, ...UserActions, initializeWithKey})
//
// @translate(['MoneyTransfer'], {wait: true})
// class ActionComponent extends Component {
//     static propTypes = {
//         getInstance: PropTypes.func,
//         getInstanceCollaborateur: PropTypes.func,
//         valideattributionAgenceCaisse :PropTypes.func,
//         refusionattributionAgenceCaisse :PropTypes.func,
//         showBouttonAnnuler: PropTypes.func,
//         showBouttonSupprimer: PropTypes.func,
//         deleteTransfertCltCltByID: PropTypes.func,
//         loadListTransfertcomptegarantie: PropTypes.func,
//         annulerTransfertCltCltByID: PropTypes.func,
//     };
//
//     constructor(props) {
//         super(props);
//         this.state = {
//             showModalSigne: false,
//             showModal: false,
//             canceling: false,
//             deleting: false,
//             maxmin:false,
//         }
//         this.handleClick = this.handleClick.bind(this);
//         this.ValideAttribution = this.ValideAttribution.bind(this);
//         this.RefussionAttribution = this.RefussionAttribution.bind(this);
//
//     }
//
//     async handleClick() {
//
//         try {
//             if (this.state.deleting) {
//                 await this.props.deleteTransfertCltCltByID(this.props.rowData.id);
//                   this.props.loadCaissePartenairePartenaire(idCaisse);
//                 if (idAgence == null) {
//                     this.props.loadListTransfertcomptegarantie(this.props.userFrontDetails.agenceId, this.props.userFrontDetails.caisseNum, '',  moment(), moment(), '', '', '', '', '', '');
//                     this.props.loadCaissePartenairePartenaire(this.props.userFrontDetails.caisseId);
//                 } else {
//                     this.props.loadListTransfertcomptegarantie(idAgence, '', '',  moment(), moment(), '', '', '', '', '', '');
//                     this.props.loadCaissePartenairePartenaire(idCaisse);
//                 }
//
//             } else if (this.state.canceling) {
//                 if (numCaisse == "null" || numCaisse == null) {
//                     await this.props.annulerTransfertCltCltByID(this.props.userFrontDetails.caisseNum, this.props.rowData.id);
//                     this.props.loadCaissePartenaire(this.props.userFrontDetails.caisseId);
//                 } else {
//                     await this.props.annulerTransfertCltCltByID(numCaisse, this.props.rowData.id);
//                     this.props.loadCaissePartenaire(idCaisse);
//                 }
//
//                 if (idAgence == null) {
//                     this.props.loadListTransfertcomptegarantie(this.props.userFrontDetails.agenceId, '', '', '', '', '', '', '', '', '', '');
//                     this.props.loadCaissePartenaire(this.props.userFrontDetails.caisseId);
//                 } else {
//                     this.props.loadListTransfertcomptegarantie(idAgence, '', '',  moment(), moment(), '', '', '', '', '', '');
//                     this.props.loadCaissePartenaire(idCaisse);
//                 }
//
//             }
//         } catch (error) {
//             console.log(error.message);
//         }
//         this.setState({showModal: false});
//     }
//     async ValideAttribution(agenceId,caisseNum,montant,id) {
//          await this.props.valideattributionAgenceCaisse(agenceId,caisseNum,montant,id);
//           this.props.loadListTransfertcomptegarantie(agenceId,caisseNum, '',  moment(), moment(), '', '', '', '', '', '');
//           this.props.loadCaissePartenaire(this.props.userFrontDetails.caisseId);
//     }
//     async RefussionAttribution(agenceId,caisseNum,montant,id) {
//          await this.props.refusionattributionAgenceCaisse(agenceId,caisseNum,montant,id);
//           this.props.loadListTransfertcomptegarantie(agenceId,caisseNum, '',  moment(), moment(), '', '', '', '', '', '');
//           this.props.loadCaissePartenaire(this.props.userFrontDetails.caisseId);
//     }
//
//     render() {
//         const {getInstance,valideattributionAgenceCaisse,loadCompteGarantie,resetForm,compteGarantie,refusionattributionAgenceCaisse, userFrontDetails, rowData, getInstanceCollaborateur, showBouttonAnnuler, t, loadListTransfertcomptegarantie, deleteTransfertCltCltByID, annulerTransfertCltCltByID} = this.props;
//         const styles = require('./moneytransfer.scss');
//         const idtransfert = this.props.rowData.id;
//         const close = () => {
//             this.setState({showModal: false});
//             this.setState({showModalSigne: false});
//         };
//
//
//         return (
//
//             <div style={{minWidth:"57px"}}>
//                 {(this.props.rowData.typeTransfert === "Transfert" || this.props.rowData.typeTransfert === "Paiement") ?
//
//                     <ButtonGroup>
//                         <Button
//                             bsSize="small" bsStyle="warning" className={styles.actionButtonStyle}
//                             onClick={(e) => {
//                                 e.preventDefault();
//                                 getInstance(rowData.id);
//                                 showBouttonAnnuler('hide');
//                             }}
//                         >
//                             <i
//                                 className="fa fa-eye"
//                             />
//
//                         </Button>
//                         {this.props.rowData.statut !== "signer" &&  this.props.rowData.typeTransfert !== "Paiement" &&
//
//                         <Button
//                             bsSize="small" bsStyle="warning" className={'pull-right  ' + styles.actionButtonStyle}
//                             onClick={() => browserHistory.push(baseUrl + 'app/moneyTransfer/update/' + idtransfert)}
//
//                         >
//                             <i className="fa fa-pencil fa-fw"/>
//                         </Button>
//                         }
//                         {(this.props.rowData.statut === "En cours d'enregistrement" ||this.props.rowData.statut === "En cours validation") &&
//                          <Button
//                             bsSize="small"
//                             bsStyle="warning" className={styles.actionButtonStyle}
//                             onClick={() => this.setState({
//                                 showModal: true,
//                                 canceling: true,
//                                 deleting: false,
//                                 showModalSigne: false
//                             })}>  <i className="fa fa-times"/>
//                         </Button>
//                         }
//                       {(this.props.rowData.statut === "En cours d'enregistrement" ||this.props.rowData.statut === "En cours validation") &&
//                        <Button
//                             bsSize="small"
//                             bsStyle="danger" className={styles.actionButtonStyle}
//                             onClick={() => this.setState({
//                                 showModal: true,
//                                 canceling: false,
//                                 deleting: true,
//                                 showModalSigne: true
//                             })}
//                         ><i className="fa fa-trash-o fa-fw"/>
//                         </Button>
//                       }
//
//                         <Modal
//                             show={this.state.showModal}
//                             onHide={close}
//                             container={this}
//                             aria-labelledby="contained-modal-title"
//                         >
//                             <Modal.Header closeButton>
//                                 <Modal.Title id="contained-modal-title">
//                                     {this.state.deleting &&
//                                     <div>{t('popup.supression.title')}</div>
//                                     }
//                                     {this.state.canceling &&
//                                     <div>{t('popup.confirmation.title')}</div>
//                                     }
//                                 </Modal.Title>
//                             </Modal.Header>
//                             <Modal.Body>
//                                 {this.state.canceling &&
//                                 <div>{t('popup.confirmation.msg')}</div>
//                                 }
//                                 {this.state.deleting &&
//                                 <div>{t('popup.supression.msg')}</div>
//                                 }
//                             </Modal.Body>
//                             <Modal.Footer>
//                                 <ButtonGroup
//                                     className="pull-right" bsSize="small"
//                                 >
//                                     <Button className={styles.ButtonPasswordStyle}
//                                             onClick={() => close()}>{t('popup.supression.noBtn')}</Button>
//                                     <Button className={styles.ButtonPasswordStyle}
//                                             onClick={this.handleClick}>{t('popup.supression.yesBtn')}</Button>
//                                 </ButtonGroup>
//                             </Modal.Footer>
//                         </Modal>
//
//                     </ButtonGroup>
//                     :
//                     <div>
//                          {(this.props.rowData.statut !== "Validé" && this.props.rowData.statut !=="Refusé" && this.props.rowData.typeTransfert ==="Attribution") &&
//                             <ButtonGroup >
//                               <Button bsSize="small" bsStyle="warning" className={styles.actionButtonStyle}
//                                onClick={(e) => {this.ValideAttribution(userFrontDetails.agenceId,userFrontDetails.caisseNum,rowData.montant,rowData.id);
//                                 }} ><i className="fa fa-check"/></Button>
//                               <Button bsSize="small" bsStyle="danger" className={styles.actionButtonStyle}
//                               onClick={(e) => {this.RefussionAttribution(userFrontDetails.agenceId,userFrontDetails.caisseNum,rowData.montant,rowData.id);
//                                }}
//                                 ><i className="fa fa-times"/></Button>
//                             </ButtonGroup>
//                   }
//                   </div>
//                 }
//             </div>
//
//         );
//     }
// }
//
// class StatusComponent extends Component {
//     render() {
//         let libelleStatut = '';
//         if (this.props.rowData.statutCode === 'annuler_client') {
//             libelleStatut = <div style={{color: '#999'}}>{this.props.data}</div>;
//         } else if (this.props.rowData.statutCode === 'Enregistre') {
//             libelleStatut = <div style={{color: '#FFC125'}}>{this.props.data}</div>;
//         } else if (this.props.rowData.statutCode === 'rejete') {
//             libelleStatut = <div style={{color: 'red'}}>{this.props.data}</div>;
//         } else {
//             libelleStatut = <div style={{color: '#859'}}>{this.props.data}</div>;
//         }
//         return (
//             <div>
//                 {libelleStatut}
//             </div>
//         );
//     }
// }
//
// class CenterComponent extends Component {
//     render() {
//         const url = this.props.data;
//         return <div className="text-center">{this.props.data}</div>;
//     }
// }
//
// @translate(['MoneyTransfer'], {wait: true})
// class HeaderComponent extends Component {
//     render() {
//         return (<div
//             className="text-center"
//         >{this.props.displayName}</div>);
//     }
// }
//
//
// @connect(
//     state => ({
//         loadingListTransfertAgence: state.moneyTransfer.loadingListTransfertAgence,
//         loadingDeleteTransfert: state.moneyTransfer.loadingDeleteTransfert,
//         successDeleteTransfert: state.moneyTransfer.successDeleteTransfert,
//         loadingCancelTransfert: state.moneyTransfer.loadingCancelTransfert,
//         successCancelTransfert: state.moneyTransfer.successCancelTransfert,
//         dataForDetail: state.moneyTransfer.dataForDetail,
//          soldegarantie: state.moneyTransfer.soldegarantie,
//         caisseDetails: state.moneyTransfer.caisseDetails,
//         userFrontDetails: state.user.userFrontDetails,
//         compteGarantie: state.moneyTransfer.compteGarantie,
//         transfetList: state.moneyTransfer.transfetList,
//         totalMontant: state.moneyTransfer.totalMontant,
//         id: state.moneyTransfer.saveSuccessObject.id,
//         caisseObjP: state.moneyTransfer.caisseObjP,
//         successeuil:state.moneyTransfer.successeuil,
//         msgSeuilSoldecaisee:state.moneyTransfer.msgSeuilSoldecaisee,
//         view: state.moneyTransfer.view,
//         successSigneTransfert: state.moneyTransfer.successSigneTransfert,
//         loadingSigneTransfert: state.moneyTransfer.loadingSigneTransfert,
//
//     }),
//
//     {...MoneyTransferAction, ...UserActions, initializeWithKey})
//
//
// @asyncConnect([{
//     promise: ({store: {dispatch, getState}}) => {
//         const promises = [];
//         console.log("captured4 ");
//         console.log(TB);
//         promises.push(dispatch(MoneyTransferAction.typeTB(TB)));
//         promises.push(dispatch(MoneyTransferAction.RestForm()));
//         promises.push(dispatch(MoneyTransferAction.initializeForm()));
//           promises.push(dispatch(MoneyTransferAction.loadCompteGarantie(idColaborateur)));
//         if (idCaisse != "null") {
//
//             promises.push(dispatch(MoneyTransferAction.loadCaissePartenaire(idCaisse)));
//             promises.push(dispatch(MoneyTransferAction.loadCompteGarantie(idColaborateur)));
//             promises.push(dispatch(MoneyTransferAction.VerificationSeuilSoldeCaisse(idCaisse)));
//             promises.push(dispatch(MoneyTransferAction.loadListTransfertcomptegarantie(idAgence, numCaisse, '', moment(), moment(), '', '', '', '', '', '')));
//         } else{
//             if (idCaisse == null || idCaisse == "null" || idCaisse == "" || idCaisse == undefined ) {
//                 if (getState().user.userFrontDetails != undefined && getState().user.userFrontDetails.caisseId != "") {
//                     promises.push(dispatch(MoneyTransferAction.loadCompteGarantie(getState().user.userFrontDetails.collaborateurId)));
//                     promises.push(dispatch(MoneyTransferAction.loadCaissePartenaire(getState().user.userFrontDetails.caisseId)));
//                     promises.push(dispatch(MoneyTransferAction.VerificationSeuilSoldeCaisse(getState().user.userFrontDetails.caisseId)));
//                     promises.push(dispatch(MoneyTransferAction.loadListTransfertcomptegarantie(getState().user.userFrontDetails.agenceId, getState().user.userFrontDetails.caisseNum, '', moment(), moment(), '', '', '', '', '', '')));
//                 }
//             }
//
//         }
//
//         return Promise.all(promises);
//     }
// }])
//
// @reduxForm({
//         form: 'searchMoneyTransferAgence',
//         fields: ['codeagence', 'typeTransfert', 'dateDebut', 'dateFin', 'nomEmetteur', 'nomBenif', 'montantMax', 'montantMin', 'statut'],
//         validate: SearchFormValidation,
//         initialValues: {
//             typeTransfert: '',
//             dateDebut: '',
//             dateFin: '',
//             nomEmetteur: '',
//             nomBenif: '',
//             montantMax: '',
//             montantMin: '',
//             statut: '',
//         },
//         destroyOnUnmount: false
//     },
// )
// @translate(['MoneyTransfer'], {wait: true})
// export default class TabeBordPartenaire extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             showModal: false,
//             startDate: moment(),
//             endDate: moment()
//         };
//         this.handleChangeStart = this.handleChangeStart.bind(this);
//         this.handleChangeEnd = this.handleChangeEnd.bind(this);
//         this.signeTransfert = this.signeTransfert.bind(this);
//
//     }
// async signeTransfert(id) {
//         this.setState({showModal: false});
//         await this.props.signeTransfertCltCltByID(id);
//         this.props.getInstanceTransfert(id);
//         setTimeout(() => {this.props.resetAlertSoldeInsufisante()}, 6000);
//
//     }
//     componentWillMount() {
//     }
//
//     handleChangeStart(date) {
//         this.setState({startDate: date});
//     }
//
//     handleChangeEnd(date) {
//         this.setState({endDate: date});
//     }
//     componentDidMount(){
//         console.log("componentDidMount")
//         this.props.resetForm();
//     }
//
//     render() {
//         const {
//             getInstanceCollaborateur, t, showBouttonSupprimer, annulerTransfertCltCltByID,
//             userFrontDetails, loadUserFrontDetails, compteGarantie,loadCompteGarantie, loadCaissePartenaire, initializeForm, loadListTransfertcomptegarantie,
//             fields: {codeagence, typeTransfert, dateDebut, dateFin, nomEmetteur, nomBenif, montantMax, montantMin, statut}, listMoneyTransfer, getInstance,
//             dataForDetail,soldegarantie, view, setView, showAnnuler, showSupprimer, annulerDemande, supprimerDemande, isAnnule, isSupprimer, comptes, showPanelSearch,
//             handleSubmit, values, resetForm, caisseObjP, transfetList, totalMontant,loadingListTransfertAgence,signeTransfert,
//             loadingDeleteTransfert,successSigneTransfert,loadingSigneTransfert,submitting,msgSeuilSoldecaisee,successeuil,  successDeleteTransfert, deleteTransfertCltCltByID, successCancelTransfert, loadingCancelTransfert, showBouttonAnnuler
//         } = this.props;
//
//         const styles = require('./moneytransfer.scss');
//         const panelStyles = {margin: '5px 10px 5px 10px'};
//         const close = () => {
//             this.setState({showModal: false});
//             this.setState({showModalSigne: false});
//         };
//         values.dateDebut = this.state.startDate;
//         values.dateFin = this.state.endDate;
//         const gridMetaData = [
//
//             {
//                 columnName: 'dateTransfert',
//                 displayName: t('list.cols.dateOperation'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'typeTransfert',
//                 displayName: t('list.cols.typeOperation'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//
//             {
//                 columnName: 'nomEmetteur',
//                 displayName: t('list.cols.nomEmetteur'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'nomBenif',
//                 displayName: t('list.cols.nomBenif'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'montant',
//                 displayName: t('list.cols.Montant'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'commissionglobale',
//                 displayName: t('list.cols.commissionglobal'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'commissionpartenaire',
//                 displayName: t('list.cols.commissionpartenaire'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'statut',
//                 displayName: t('list.cols.statusoperation'),
//                 customComponent: StatusComponent,
//                 customHeaderComponent: HeaderComponent,
//
//             },
//             {
//                 columnName: 'action',
//                 displayName: t('list.cols.actions'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: ActionComponent,
//             }
//         ];
//
//         return (
//
//
//             <div>
//
//                 {view === 'grid' ?
//                     <Row>
//                         <Row >
//                             <Col xs="12" md="8">
//                             <ControlLabel><h2>{t('form.titleForm.tbpartenaire')}</h2></ControlLabel>
//                             </Col>
//                              <Col xs="12" md="4">
//                             <Button className="pull-right"
//                            bsStyle="primary"
//                          onClick={() => {
//                          window.location.reload(true)
//                         browserHistory.push(baseUrl + 'app/request/MoneyTransfer')
//                         }}>
//                     <i className="fa fa-plus"> &nbsp;{t('toolbar.newBtn')}</i>
//
//                     </Button></Col>
//                            </Row>
//                          <Row>
//
//                         {loadingDeleteTransfert === false && successDeleteTransfert === true &&
//                         <Alert bsStyle="info">
//                             <strong>{t('msg.sucesssuppraission')} </strong>
//                         </Alert>
//                         }
//
//
//
//                         {loadingCancelTransfert === false && successCancelTransfert === true &&
//                         <Alert bsStyle="info">
//                             <strong>{t('msg.sucessannulation')} </strong>
//                         </Alert>
//                         }
//                         {loadingCancelTransfert === false && successCancelTransfert === "null" &&
//                         <Alert bsStyle="danger">
//                             <strong>{t('msg.erreurannulation')} </strong>
//                         </Alert>
//                         }
//
//                          </Row>
//                         <Row  className="detailsBloc">
//                             <Col xs={12} md={3}>
//                                 <ControlLabel>{t('form.label.dateaujourdui')}: </ControlLabel>
//                                 <p className="detail-value">{moment().format('DD/MM/YYYY')}</p>
//                             </Col>
//                             <Col xs={12} md={3}>
//                                 <ControlLabel>{t('form.label.nompartenaire')} :</ControlLabel>
//                                <p className="detail-value">
//                                 {userFrontDetails.caisseId !== "" && caisseObjP != null && caisseObjP != "undefined" ? caisseObjP.nomAgent : userFrontDetails.collaborateurNom}</p>
//                             </Col>
//                             <Col xs={12} md={3}>
//                                 <ControlLabel>{t('form.label.soldedepotgarantie')} : </ControlLabel>
//                                 <p className="detail-value">{compteGarantie.solde}</p>
//                             </Col>
//                             <Col xs={12} md={3}>
//                                 <ControlLabel>{t('form.label.datedernierMiseajour')} :</ControlLabel>
//
//                                 <p className="detail-value">{userFrontDetails.caisseId !== "" && caisseObjP != null && caisseObjP != "undefined" && caisseObjP.dateModif}</p>
//
//
//                             </Col>
//
//                         </Row>
//
//                         <PanelGroup defaultActiveKey="1" accordion>
//
//
//                             <Panel header={t('form.panel.mouvementpartenaire')} eventKey="2" className={styles.accordionPanel}
//                                    style={panelStyles}>
//
//                                 <Row className={styles.paddingColumn}>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.totalenvoi')} : </ControlLabel>
//                                          <p className="detail-value">{userFrontDetails.caisseId !== "" && caisseObjP != null && caisseObjP != "undefined" && caisseObjP.totalEnvois}
//                                        XOF</p>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> {t('form.label.totalPaiements')} : </ControlLabel>
//                                          <p className="detail-value">{userFrontDetails.caisseId !== "" && caisseObjP != null && caisseObjP != "undefined" && caisseObjP.totalPaiements}
//                                       XOF</p>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.totalalimentation')}: </ControlLabel>
//                                            <p className="detail-value">{userFrontDetails.caisseId !== "" && caisseObjP != null && caisseObjP != "undefined" && caisseObjP.totalAlimentations}
//                                         XOF</p>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.totalretrait')}: </ControlLabel>
//                                           <p className="detail-value">{userFrontDetails.caisseId !== "" && caisseObjP != null && caisseObjP != "undefined" && caisseObjP.totalRetraits}
//                                        XOF</p>
//                                     </Col>
//
//                                 </Row>
//                             </Panel>
//                             {this.state.maxmin === true &&
//                             <div className="alert alert-danger">
//                                 {t('msg.msgMinMax')}
//                             </div>
//                             }
//                             <Panel header={t('form.panel.interactionpartenaire')} eventKey="4"
//                                    className={styles.accordionPanel}
//                                    style={panelStyles}>
//
//                                 <form>
//
//                                     <Row className={styles.paddingColumn}>
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.nomemeteur')} : </ControlLabel>
//                                             <FormControl type="text" {...nomEmetteur}
//                                                          bsClass={styles.datePickerFormControl} placeholder="" min="1"/>
//                                         </Col>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.typeoperation')} : </ControlLabel>
//                                             <FormControl {...typeTransfert} componentClass="select"
//                                                          bsClass={styles.datePickerFormControl} placeholder="select">
//                                                 <option value="" hidden></option>
//                                                 <option key="1" value="CLTOCA">Transfert</option>
//                                                 <option key="2" value="CATOCL">Paiement</option>
//                                                 <option key="3" value="CATOCOM">Alimentation</option>
//                                                 <option key="4" value="COMTOCA">Retrait</option>
//                                             </FormControl>
//                                         </Col>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel> {t('form.label.montantoperationmin')}: </ControlLabel>
//                                             <FormControl type="number" {...montantMin}
//                                                          bsClass={styles.datePickerFormControl} placeholder="" min="1"/>
//                                         </Col>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.dateoperationmin')}: </ControlLabel>
//                                             <DatePicker
//                                                 selectsStart
//                                                 selected={this.state.startDate}
//                                                 startDate={this.state.startDate}
//                                                 maxDate={this.state.endDate}
//                                                 onChange={this.handleChangeStart}
//                                                 className={styles.datePickerFormControl}
//                                                 isClearable="true"
//                                                 locale="fr-FR"
//                                                 dateFormat="DD/MM/YYYY"
//                                             />
//                                         </Col>
//
//
//                                     </Row>
//
//                                     <Row className={styles.paddingColumn}>
//
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.nombeneficiare')}: </ControlLabel>
//                                             <FormControl {...nomBenif} type="text"
//                                                          bsClass={styles.datePickerFormControl}/>
//                                         </Col>
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.statusoperation')}: </ControlLabel>
//                                             <FormControl {...statut} type="text"
//                                                          bsClass={styles.datePickerFormControl}/>
//                                         </Col>
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.montantoperationmax')}: </ControlLabel>
//                                             <FormControl {...montantMax} type="number"
//                                                          bsClass={styles.datePickerFormControl} min="1"/>
//                                         </Col>
//
//                                         <Col xs={12} md={3}>
//                                             <ControlLabel>{t('form.label.dateoperationmax')} :</ControlLabel>
//                                             <DatePicker
//                                                 selectsEnd
//                                                 selected={this.state.endDate}
//                                                 endDate={this.state.endDate}
//                                                 minDate={this.state.startDate}
//                                                 maxDate={moment()}
//                                                 onChange={this.handleChangeEnd}
//                                                 className={styles.datePickerFormControl}
//                                                 isClearable="true"
//                                                 locale="fr-FR"
//                                                 dateFormat="DD/MM/YYYY"
//                                             />
//                                         </Col>
//                                     </Row>
//
//                                     <Row className={styles.paddingColumn}>
//                                         <Col xs={6} xsOffset={9}>
//                                             <Button
//                                                 loading={loadingListTransfertAgence}
//                                                 bsStyle="primary"
//                                                 onClick={() => {
//                                                     if(montantMin.value>montantMax.value){
//
//                                                         this.setState({
//                                                             maxmin:true,
//                                                         });
//                                                         window.setTimeout(() => {
//                                                             this.setState({
//                                                                 maxmin: false
//                                                             });
//                                                         }, 6000);
//                                                     }
//                                                     else{
//                                                     loadListTransfertcomptegarantie(userFrontDetails.agenceId, userFrontDetails.caisseNum, values.typeTransfert, values.dateDebut, values.dateFin, values.nomEmetteur, values.nomBenif, values.montantMin, values.montantMax, values.agent, values.statut)
//                                                 }}}
//                                             >
//                                                 <i className="fa fa-search"/>{t('list.search.buttons.search')}
//                                             </Button>
//                                             <Button
//                                                 bsStyle="primary"
//                                                 onClick={() => {
//                                                     this.setState({startDate: moment(), endDate: moment()});
//                                                     values.codeagence = '';
//                                                     values.typeTransfert = '';
//                                                     values.dateDebut = this.state.startDate;
//                                                     values.dateFin = this.state.endDate;
//                                                     values.nomEmetteur = '';
//                                                     values.nomBenif = '';
//                                                     values.montantMax = '';
//                                                     values.montantMin = '';
//                                                     values.agent = '';
//                                                     values.statut = '';
//                                                     resetForm();
//                                                     loadListTransfertcomptegarantie(userFrontDetails.agenceId, '', '', moment(), moment(), '', '', '', '', '', '')
//                                                 }}
//                                             >
//                                                 <i className="fa fa-refresh"/> {t('form.buttons.reinitialiser')}
//                                             </Button>
//                                         </Col>
//                                     </Row>
//                                 </form>
//
//
//                                 <Row className="table-responsive">
//                                     <Col xs="12" md="12">
//                                         <Griddle
//                                             results={transfetList}
//                                             columnMetadata={gridMetaData}
//                                             useGriddleStyles={false}
//                                             noDataMessage={t('list.search.msg.noResult')}
//                                             resultsPerPage={10}
//                                             nextText={<i className="fa fa-chevron-right"/>}
//                                             previousText={<i className="fa fa-chevron-left"/>}
//                                             tableClassName="table"
//                                             columns={[ 'typeTransfert', 'dateTransfert', 'nomEmetteur', 'nomBenif', 'montant', 'commissionglobale', 'commissionpartenaire', 'statut', 'action']}
//                                         />
//                                     </Col>
//                                 </Row>
//                                 <Row className="table-responsive">
//                                 <Button bsStyle="primary"
//                                         onClick={() => window.open(baseUrl + 'agencetransfertargent/generateXLS/' + userFrontDetails.agenceId + '/listtransfert?codeCaisse=' + values.numCaisse + '&typeTransfert=' + values.typeTransfert + '&dateTransfertDebut=' + values.dateDebut + '&dateTransfertFin=' + values.dateFin + '&nomEmetteur=' + values.nomEmetteur + '&numerotelephone='+values.numEmetteur+'&nomBenif=' + values.nomBenif + '&montantMin=' + values.montantMin + '&montantMax=' + values.montantMax + '&agent=' + '' + '&statut=' + values.statut)}>
//                                     <i className="fa fa-file-pdf-o fa-4" /> liste Transferts
//                                 </Button>
//                                  <Col xs={12} md={2}>
//                                 <div style={{border:'1px solid #e4e4e4', background:'#fff', padding:'10px'}}>
//                                 <span className="icon-layers"/> Total : {this.props.totalMontant} <b>CFA</b>
//                                 </div>
//                                 </Col>
//
//                                 </Row>
//                             </Panel>
//
//                         </PanelGroup>
//                     </Row>
//                     :
//                     dataForDetail && dataForDetail !== "undefined" &&
//                    <Row className={styles.compteCard}>
//
//                     {soldegarantie === false &&
//                     <Alert bsStyle="danger">{t('msg.soldegarantieinsuffisant')}</Alert>
//                    }
//                     {successSigneTransfert === true && <Alert bsStyle="success">{t('msg.signedSuccess')}</Alert>}
//                         <form className="formContainer">
//
//                     <fieldset>
//                         <legend>{t('form.legend.infosemeteur')}</legend>
//                         <Row>
//                         <Col xs={12} md={4}>
//                             <ControlLabel>{t('form.label.numidentiemeteur')}</ControlLabel>
//                             <p className="detail-value"> {dataForDetail.numeroPieceIdentiteemeteur}</p>
//                         </Col>
//                         <Col xs={12} md={4}>
//                             <ControlLabel>{t('form.label.numtelemeteur')}</ControlLabel>
//                             <p className="detail-value"> {dataForDetail.numtelemetteur}</p>
//                         </Col>
//                         <Col xs={12} md={4}>
//                             <ControlLabel>{t('form.label.nom')}</ControlLabel>
//                             <p className="detail-value"> {dataForDetail.nomemetteur}</p>
//                         </Col>
//                         </Row>
//
//                             <Row>
//                         <Col xs={12} md={4}>
//                             <ControlLabel>{t('form.label.prenom')}</ControlLabel>
//                             <p className="detail-value"> {dataForDetail.prenomemeteur}</p>
//                         </Col>
//
//                         <Col xs={12} md={4}>
//                             <ControlLabel>{t('form.label.mail')}</ControlLabel>
//                             <p className="detail-value"> {dataForDetail.emailemetteur}</p>
//                         </Col>
//
//                         <Col xs={12} md={4}>
//                             <ControlLabel>{t('form.label.ville')}</ControlLabel>
//                             <p className="detail-value">  {dataForDetail.villeemetteur}</p>
//                         </Col>
//                         </Row>
//
//                         <Row>
//                         <Col xs={12} md={4}>
//                             <ControlLabel>{t('form.label.region')}</ControlLabel>
//                             <p className="detail-value"> {dataForDetail.regionemeteur}</p>
//                         </Col>
//                         <Col xs={12} md={4}>
//                             <ControlLabel>{t('form.label.pays')}</ControlLabel>
//                             <p className="detail-value"> {dataForDetail.paysemetteur}</p>
//                         </Col>
//                         <Col xs={12} md={4}>
//                             <ControlLabel>{t('form.label.indicatif')}</ControlLabel>
//                            <p className="detail-value"> {dataForDetail.paysindicatifemetteur}</p>
//                         </Col>
//                           </Row>
//                     </fieldset>
//                         <fieldset style={{marginTop:'25px'}}>
//                             <legend>{t('form.legend.infosbeneficiare')}</legend>
//                             <Row>
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.numidentibeneficiare')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.numeroPieceIdentitebeneficiare}</p>
//                             </Col>
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.numidtelbeneficiare')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.numerotelebeneficiare}</p>
//                             </Col>
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.nom')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.nombeneficiare}</p>
//                             </Col>
//                             </Row>
//                             <Row>
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.prenom')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.prenombeneficiare}</p>
//                             </Col>
//
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.mail')}:</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.emailbeneficiare}</p>
//                             </Col>
//
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.ville')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.villebeneficiare}</p>
//                             </Col>
//                             </Row>
//                             <Row>
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.region')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.regionbeneficiare}</p>
//                             </Col>
//
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.pays')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.paysbeneficiare}</p>
//                             </Col>
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.indicatif')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.paysindicatifbeneficiare}</p>
//                             </Col>
//                             </Row>
//
//                         </fieldset>
//                         <fieldset style={{marginTop:'25px'}}>
//                             <legend>{t('form.legend.infostransfert')}</legend>
//                             <Row>
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.montanttransfert')}:</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.montantOperation}</p>
//                             </Col>
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.fraienvoi')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.fraisenvois}</p>
//                             </Col>
//
//                             <Col xs={12} md={4}>
//                                 <ControlLabel>{t('form.label.monatntremise')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.montantremise}</p>
//                             </Col>
//
//                             <Col xs={12} md={3}>
//                                 <ControlLabel>{t('form.label.motifRemise')}</ControlLabel>
//                                 <p className="detail-value"> {dataForDetail.motif}</p>
//                             </Col>
//                             </Row>
//
//                         </fieldset>
//                         {showAnnuler &&
//                         <Alert bsStyle="info">
//                             <p>
//                                 <i className="fa fa-exclamation-circle"/> &nbsp;&nbsp; {t('msg.annulertransaction')}
//                             </p>
//                         </Alert>
//                         }
//                         <Row className="pull-right">
//
//                             <a className="glyphicon glyphicon-upload btn btn-primary"
//                                href={baseUrl + 'transfertargentBNIF/createAvisTransfertPDF/' + dataForDetail.id}>  {t('form.buttons.uploadpdf')}
//                             </a>
//                             {dataForDetail.statutOperation !== "signer" &&
//                                     <Button
//                                      loading={loadingSigneTransfert}
//                                      onClick={() => this.setState({showModalSigne: true})}
//                                      bsStyle="primary" >
//                                      <i className={'fa ' + (submitting ? 'fa-cog fa-spin' : 'fa fa-check')}/> {t('form.buttons.confirmersigner')}
//                                      </Button>
//                                 }
//
//                             <Button
//                                 onClick={() => {
//                                     setView('grid');
//                                     browserHistory.push(baseUrl + 'app/TabeBordPartenaire');
//                                 }}
//                                 bsStyle="primary"
//                             >
//                                 <i className="fa fa-reply"/> {t('form.buttons.retour')}
//                             </Button>
//
//                             {showAnnuler &&
//                             <Button
//                                 onClick={() => this.setState({showModal: true})}
//                                 className={styles.ButtonStyle}
//                             >
//                                 <i className="fa fa-times"/> {t('form.buttons.annuler')}
//                             </Button>
//                             }
//
//                             {showSupprimer &&
//                             <Alert bsStyle="info">
//                                 <p>
//                                     <i className="fa fa-exclamation-circle"/> &nbsp;&nbsp; {t('msg.supprimertransaction')}
//                                 </p>
//                             </Alert>
//                             }
//                             {showSupprimer &&
//                             <Button
//                                 onClick={() => this.setState({showModal: true})}
//                                 className={styles.ButtonStyle}
//                             >
//                                 <i className="fa fa-times"/>
//                                 &nbsp;&nbsp;{t('form.buttons.supprimer')}
//                             </Button>
//                             }
//                             <Modal
//                                 show={this.state.showModal}
//                                 onHide={close}
//                                 container={this}
//                                 aria-labelledby="contained-modal-title"
//                             >
//                                 <Modal.Header closeButton>
//                                     <Modal.Title
//                                         id="contained-modal-title">{t('popup.confirmation.title')}</Modal.Title>
//                                 </Modal.Header>
//                                 <Modal.Body>
//                                     {t('popup.supression.msg')}
//                                 </Modal.Body>
//                                 <Modal.Footer>
//                                     <ButtonGroup
//                                         className="pull-right" bsSize="small"
//                                     >
//                                         <Button className={styles.ButtonPasswordStyle}
//                                                 onClick={() => close()}>{t('popup.supression.noBtn')}</Button>
//                                         <Button className={styles.ButtonPasswordStyle} onClick={() => {
//                                             close();
//                                             supprimerDemande(dataForDetail.id);
//                                         }}>{t('popup.supression.yesBtn')}</Button>
//                                     </ButtonGroup>
//                                 </Modal.Footer>
//                             </Modal>
//
//                             <Modal
//                                 show={this.state.showModal}
//                                 onHide={close}
//                                 container={this}
//                                 aria-labelledby="contained-modal-title"
//                             >
//                                 <Modal.Header closeButton>
//                                     <Modal.Title
//                                         id="contained-modal-title">{t('popup.confirmation.title')}</Modal.Title>
//                                 </Modal.Header>
//                                 <Modal.Body>
//                                     {t('popup.confirmation.msg')}
//                                 </Modal.Body>
//                                 <Modal.Footer>
//                                     <ButtonGroup
//                                         className="pull-right" bsSize="small"
//                                     >
//                                         <Button className={styles.ButtonPasswordStyle}
//                                                 onClick={() => close()}>{t('popup.confirmation.noBtn')}</Button>
//                                         <Button className={styles.ButtonPasswordStyle} onClick={() => {
//                                             close();
//                                             annulerDemande(dataForDetail.id);
//                                         }}>{t('popup.confirmation.yesBtn')}</Button>
//                                     </ButtonGroup>
//                                 </Modal.Footer>
//                             </Modal>
//                              <Modal
//                                     show={this.state.showModalSigne}
//                                     onHide={close}
//                                     container={this}
//                                     aria-labelledby="contained-modal-title"
//                                 >
//                                     <Modal.Header closeButton>
//                                         <Modal.Title id="contained-modal-title">
//                                             <div>{t('popup.signature.title')}</div>
//                                         </Modal.Title>
//                                     </Modal.Header>
//                                     <Modal.Body>
//                                         <div>{t('popup.signature.msg')}</div>
//                                     </Modal.Body>
//                                     <Modal.Footer>
//                                         <ButtonGroup
//                                             className="pull-right" bsSize="small"
//                                         >
//                                             <Button className={styles.ButtonPasswordStyle}
//                                                     onClick={() => close()}>Non</Button>
//                                             <Button className={styles.ButtonPasswordStyle} onClick={() => {
//                                                   close();
//                                                 this.signeTransfert(dataForDetail.id)
//                                             }}>Oui</Button>
//                                         </ButtonGroup>
//                                     </Modal.Footer>
//                                 </Modal>
//
//                         </Row>
//                         </form>
//                     </Row>
//
//                 }
//             </div>
//         );
//     }
// }
