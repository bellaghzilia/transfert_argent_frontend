import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Label,
  Col,
  Panel,
  Row,
  ButtonGroup,
  Alert,
  Modal,
  ControlLabel,
  Form,
  FormControl,
  FormGroup,
  PanelGroup,
} from "react-bootstrap";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import { connect } from "react-redux";
import { initializeWithKey, reduxForm } from "redux-form";
import Griddle from "griddle-react";
import { browserHistory } from "react-router";
import DatePicker from "react-datepicker";
import moment from "moment";
import gridPagination from "./MoneyTransferPagination";
import SearchFormValidation from "./SearchFormValidation";
import * as MoneyTransferAction from "./moneytransferreducer";
import Button from "react-bootstrap-button-loader";
import * as UserActions from "../User/UserReducer";

@connect(
  (state) => ({
    userFrontDetails: state.user.userFrontDetails,
    dataForDetail: state.moneyTransfer.dataForDetail,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
class CenterComponent extends Component {
  render() {
    const url = this.props.data;
    return <div className="text-center">{this.props.data}</div>;
  }
}
@translate(["MoneyTransfer"], { wait: true })
class HeaderComponent extends Component {
  render() {
    return <div className="text-center">{this.props.displayName}</div>;
  }
}

@connect(
  (state) => ({
    userFrontDetails: state.user.userFrontDetails,
    transfetList: state.moneyTransfer.transfetList,
    caisseObj: state.moneyTransfer.caisseObj,
  }),

  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@asyncConnect([
  {
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];
      promises.push(dispatch(MoneyTransferAction.initializeForm()));
      if (getState().user.userFrontDetails.caisseId != "") {
        promises.push(
          dispatch(
            MoneyTransferAction.loadCaisse(
              getState().user.userFrontDetails.caisseId
            )
          )
        );
        promises.push(
          dispatch(
            MoneyTransferAction.loadListTransfertAgence(
              getState().user.userFrontDetails.agenceId,
              getState().user.userFrontDetails.caisseNum,
              "",
              moment(),
              moment(),
              "",
              "",
              "",
              "",
              "",
              ""
            )
          )
        );
      }
      return Promise.all(promises);
    },
  },
])
@reduxForm({
  form: "searchMoneyTransferAgence",
  fields: [
    "codeagence",
    "typeTransfert",
    "dateDebut",
    "dateFin",
    "nomEmetteur",
    "nomBenif",
    "montantMax",
    "montantMin",
    "statut",
  ],
  validate: SearchFormValidation,
  initialValues: {
    typeTransfert: "",
    dateDebut: "",
    dateFin: "",
    nomEmetteur: "",
    nomBenif: "",
    montantMax: "",
    montantMin: "",
    statut: "",
  },
  destroyOnUnmount: false,
})
@translate(["MoneyTransfer"], { wait: true })
export default class TDBAgentCaisse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      startDate: moment(),
      endDate: moment(),
    };
    this.handleChangeStart = this.handleChangeStart.bind(this);
    this.handleChangeEnd = this.handleChangeEnd.bind(this);
  }

  componentWillMount() {}

  handleChangeStart(date) {
    this.setState({ startDate: date });
  }

  handleChangeEnd(date) {
    this.setState({ endDate: date });
  }

  render() {
    const {
      t,
      userFrontDetails,
      fields: {
        codeagence,
        typeTransfert,
        dateDebut,
        dateFin,
        nomEmetteur,
        nomBenif,
        montantMax,
        montantMin,
        statut,
      },
      values,
      resetForm,
    } = this.props;

    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const close = () => {
      this.setState({ showModal: false });
    };
    values.dateDebut = this.state.startDate;
    values.dateFin = this.state.endDate;
    const gridMetaData = [
      {
        columnName: "dateTransfert",
        displayName: t("list.cols.dateOperation"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "typeTransfert",
        displayName: t("list.cols.typeOperation"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },

      {
        columnName: "nomEmetteur",
        displayName: t("list.cols.nomEmetteur"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "nomBenif",
        displayName: t("list.cols.nomBenif"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "montant",
        displayName: t("list.cols.Montant"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "frais",
        displayName: t("list.cols.frais"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "statut",
        displayName: t("list.cols.statusoperation"),
        customComponent: StatusComponent,
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
      },
    ];

    return (
      <div>
        <br /> <br /> <br />
        <Button
          className={styles.ButtonStyle + " pull-right"}
          disabled={userFrontDetails.caisseId === ""}
          onClick={() => {
            browserHistory.push(baseUrl + "app/request/MoneyTransfer");
          }}
        >
          <i className="fa fa-plus"> &nbsp;{t("toolbar.newBtn")}</i>
        </Button>
        {view === "grid" ? (
          <Row>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="12">
                <Col xs="12" md="3">
                  <ControlLabel></ControlLabel>
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>
                    <h2>{t("form.titleForm.tbagentcaisse")}</h2>
                  </ControlLabel>
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel></ControlLabel>
                </Col>
              </Col>
            </Row>
            <br />
            <br />

            {loadingDeleteTransfert === false &&
              successDeleteTransfert === true && (
                <Alert bsStyle="info">
                  <strong>{t("msg.sucesssuppraission")} </strong>
                </Alert>
              )}
            {userFrontDetails.caisseId === "" && (
              <Alert bsStyle="danger">
                <strong>{t("msg.aucunecaisseouverte")} </strong>
              </Alert>
            )}
            {loadingDeleteTransfert === false &&
              successDeleteTransfert === false && (
                <Alert bsStyle="danger">
                  <strong>{t("msg.sucesssuppraission")} </strong>
                </Alert>
              )}

            {loadingCancelTransfert === false &&
              successCancelTransfert === true && (
                <Alert bsStyle="info">
                  <strong>{t("msg.sucessannulation")} </strong>
                </Alert>
              )}
            {loadingCancelTransfert === false &&
              successCancelTransfert === "null" && (
                <Alert bsStyle="danger">
                  <strong>{t("msg.erreurannulation")} </strong>
                </Alert>
              )}
            {loadingCancelTransfert === false &&
              successCancelTransfert === false && (
                <Alert bsStyle="warning">
                  <strong>{t("msg.montantannulationdepasse")} </strong>
                </Alert>
              )}
            <br />

            <Row className={styles.paddingColumn}>
              <Col xs={12} md={3}>
                <Col xs={12} md={6}>
                  <ControlLabel>{t("form.label.dateaujourdui")}:</ControlLabel>
                </Col>
                <Col xs={12} md={6}>
                  {moment().format("DD/MM/YYYY")}
                </Col>
              </Col>
              <Col xs={12} md={3}>
                <Col xs={12} md={4}>
                  <ControlLabel>{t("form.label.agence")} : </ControlLabel>
                </Col>
                <Col xs={12} md={8}>
                  {userFrontDetails.caisseId !== "" &&
                  caisseObj != null &&
                  caisseObj != "undefined"
                    ? caisseObj.nomAgence
                    : userFrontDetails.agenceNom}
                </Col>
              </Col>
              <Col xs={12} md={3}>
                <Col xs={12} md={4}>
                  <ControlLabel>{t("form.label.agent")} :</ControlLabel>
                </Col>
                <Col xs={12} md={8}>
                  {userFrontDetails.caisseId !== "" &&
                  caisseObj != null &&
                  caisseObj != "undefined"
                    ? caisseObj.nomAgent
                    : userFrontDetails.collaborateurNom}
                </Col>
              </Col>
              <Col xs={12} md={3}>
                <Col xs={12} md={6}>
                  <ControlLabel>{t("form.label.numeroCaisse")}:</ControlLabel>
                </Col>
                <Col xs={12} md={6}>
                  {userFrontDetails.caisseId !== "" &&
                    caisseObj != null &&
                    caisseObj != "undefined" &&
                    caisseObj.numCaisse}
                </Col>
              </Col>
            </Row>
            <PanelGroup defaultActiveKey="1" accordion>
              <Panel
                header="Situation Caisse "
                eventKey="2"
                className={styles.accordionPanel}
                style={panelStyles}
              >
                <Row className={styles.paddingColumn}>
                  <Col xs={12} md={4}>
                    <Col xs={12} md={6}>
                      <ControlLabel>
                        {t("form.label.soldeInitial")}:
                      </ControlLabel>
                    </Col>
                    <Col xs={12} md={6}>
                      {userFrontDetails.caisseId !== "" &&
                        caisseObj != null &&
                        caisseObj != "undefined" &&
                        caisseObj.soldeInitial}{" "}
                      <b>XOF</b>
                    </Col>
                  </Col>
                  <Col xs={12} md={4}>
                    <Col xs={12} md={6}>
                      <ControlLabel>
                        {t("form.label.soldeactuel")}:
                      </ControlLabel>
                    </Col>
                    <Col xs={12} md={6}>
                      {userFrontDetails.caisseId !== "" &&
                        caisseObj != null &&
                        caisseObj != "undefined" &&
                        caisseObj.soldeActuel}{" "}
                      <b>XOF</b>
                    </Col>
                  </Col>

                  <Col xs={12} md={4}>
                    <Col xs={12} md={6}>
                      <ControlLabel>
                        {t("form.label.datemiseajour")}:
                      </ControlLabel>
                    </Col>
                    <Col xs={12} md={6}>
                      {userFrontDetails.caisseId !== "" &&
                        caisseObj != null &&
                        caisseObj != "undefined" &&
                        caisseObj.dateModif}
                    </Col>
                  </Col>
                </Row>
              </Panel>
              <Panel
                header="Mouvements de caisse "
                eventKey="3"
                className={styles.accordionPanel}
                style={panelStyles}
              >
                <Row className={styles.paddingColumn}>
                  <Col xs={12} md={3}>
                    <ControlLabel>{t("form.label.totalenvoi")} : </ControlLabel>
                    {userFrontDetails.caisseId !== "" &&
                      caisseObj != null &&
                      caisseObj != "undefined" &&
                      caisseObj.totalEnvois}{" "}
                    <b>XOF</b>
                  </Col>
                  <Col xs={12} md={3}>
                    <ControlLabel>
                      {" "}
                      {t("form.label.totalmiseadisposotion")} :{" "}
                    </ControlLabel>
                    {userFrontDetails.caisseId !== "" &&
                      caisseObj != null &&
                      caisseObj != "undefined" &&
                      caisseObj.totalMiseDispositon}{" "}
                    <b>CFA</b>
                  </Col>
                  <Col xs={12} md={3}>
                    <ControlLabel>
                      {t("form.label.totalretrait")}:{" "}
                    </ControlLabel>
                    {userFrontDetails.caisseId !== "" &&
                      caisseObj != null &&
                      caisseObj != "undefined" &&
                      caisseObj.totalRetraits}{" "}
                    <b>CFA</b>
                  </Col>
                  <Col xs={12} md={3}>
                    <ControlLabel>
                      {t("form.label.totalattribution")}:{" "}
                    </ControlLabel>
                    {userFrontDetails.caisseId !== "" &&
                      caisseObj != null &&
                      caisseObj != "undefined" &&
                      caisseObj.totalEnvois}{" "}
                    <b>CFA</b>
                  </Col>
                </Row>
              </Panel>
              <Panel
                header="Détails mouvements de ciasse"
                eventKey="4"
                className={styles.accordionPanel}
                style={panelStyles}
              >
                <form>
                  <Row className={styles.paddingColumn}>
                    <Col xs={12} md={3}>
                      <ControlLabel>
                        {t("form.label.typeoperation")}:{" "}
                      </ControlLabel>
                      <FormControl
                        {...typeTransfert}
                        componentClass="select"
                        bsClass={styles.datePickerFormControl}
                        placeholder="select"
                      >
                        <option value="" hidden></option>
                        <option key="1" value="CLTOCA">
                          Transfert
                        </option>
                        <option key="2" value="CATOCL">
                          Paiement
                        </option>
                        <option key="3" value="CATOAG">
                          Mise à disposition
                        </option>
                        <option key="4" value="AGTOCA">
                          Attribution
                        </option>
                      </FormControl>
                    </Col>
                    <Col xs={12} md={3}>
                      <ControlLabel>
                        {t("form.label.statusoperation")}:{" "}
                      </ControlLabel>
                      <FormControl
                        {...statut}
                        type="text"
                        bsClass={styles.datePickerFormControl}
                      />
                    </Col>
                    <Col xs={12} md={3}>
                      <ControlLabel>
                        {t("form.label.montantoperationmin")}:
                      </ControlLabel>
                      <FormControl
                        type="number"
                        {...montantMin}
                        bsClass={styles.datePickerFormControl}
                        placeholder=""
                        min="1"
                      />
                    </Col>
                    <Col xs={12} md={3}>
                      <ControlLabel>
                        {" "}
                        {t("form.label.dateoperationmin")} :{" "}
                      </ControlLabel>
                      <DatePicker
                        selectsStart
                        selected={this.state.startDate}
                        startDate={this.state.startDate}
                        maxDate={this.state.endDate}
                        onChange={this.handleChangeStart}
                        className={styles.datePickerFormControl}
                        isClearable="true"
                        locale="fr-FR"
                      />
                    </Col>
                  </Row>

                  <Row className={styles.paddingColumn}>
                    <Col xs={12} md={3}></Col>
                    <Col xs={12} md={3}></Col>

                    <Col xs={12} md={3}>
                      <ControlLabel>
                        {t("form.label.montantoperationmax")}:{" "}
                      </ControlLabel>
                      <FormControl
                        {...montantMax}
                        type="number"
                        bsClass={styles.datePickerFormControl}
                        min="1"
                      />
                    </Col>

                    <Col xs={12} md={3}>
                      <ControlLabel>
                        {t("form.label.dateoperationmax")} :{" "}
                      </ControlLabel>
                      <DatePicker
                        selectsEnd
                        selected={this.state.endDate}
                        endDate={this.state.endDate}
                        minDate={this.state.startDate}
                        maxDate={moment()}
                        onChange={this.handleChangeEnd}
                        className={styles.datePickerFormControl}
                        isClearable="true"
                        locale="fr-FR"
                      />
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <Col xs={6} xsOffset={9}>
                      <Button
                        loading={loadingListTransfertAgence}
                        bsStyle="primary"
                        disabled={userFrontDetails.caisseId === ""}
                        onClick={() => {
                          loadListTransfertAgence(
                            userFrontDetails.agenceId,
                            caisseObj.numCaisse,
                            values.typeTransfert,
                            values.dateDebut,
                            values.dateFin,
                            values.nomEmetteur,
                            values.nomBenif,
                            values.montantMin,
                            values.montantMax,
                            "",
                            values.statut
                          );
                        }}
                      >
                        <i className="fa fa-search" />
                        {t("list.search.buttons.search")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        disabled={userFrontDetails.caisseId === ""}
                        onClick={() => {
                          this.setState({
                            startDate: moment(),
                            endDate: moment(),
                          });
                          values.typeTransfert = "";
                          values.dateDebut = this.state.startDate;
                          values.dateFin = this.state.endDate;
                          values.montantMax = "";
                          values.montantMin = "";
                          values.statut = "";
                          resetForm();
                          loadListTransfertAgence(
                            userFrontDetails.agenceId,
                            caisseObj.numCaisse,
                            "",
                            moment(),
                            moment(),
                            "",
                            "",
                            "",
                            "",
                            "",
                            ""
                          );
                        }}
                      >
                        <i className="fa fa-refresh" />{" "}
                        {t("form.buttons.reinitialiser")}
                      </Button>
                    </Col>
                  </Row>
                </form>
                <Griddle
                  results={userFrontDetails.caisseId !== "" && transfetList}
                  columnMetadata={gridMetaData}
                  useGriddleStyles={false}
                  noDataMessage={t("list.search.msg.noResult")}
                  resultsPerPage={10}
                  nextText={<i className="fa fa-chevron-right" />}
                  previousText={<i className="fa fa-chevron-left" />}
                  tableClassName="table"
                  columns={[
                    "dateTransfert",
                    "typeTransfert",
                    "nomEmetteur",
                    "nomBenif",
                    "montant",
                    "frais",
                    "statut",
                    "action",
                  ]}
                />
              </Panel>
            </PanelGroup>
          </Row>
        ) : (
          dataForDetail &&
          dataForDetail !== "undefined" && (
            <div>
              <fieldset>
                <legend>{t("form.legend.infosemeteur")}</legend>

                <Col xs={12} md={3}>
                  <ControlLabel>
                    {t("form.label.numidentiemeteur")}:
                  </ControlLabel>
                  <p>{dataForDetail.numeroPieceIdentiteemeteur}</p>
                </Col>
                <Col xs={12} md={3}>
                  <ControlLabel>{t("form.label.numtelemeteur")}:</ControlLabel>
                  <p>{dataForDetail.numtelemetteur}</p>
                </Col>
                <Col xs={12} md={2}>
                  <ControlLabel>{t("form.label.nom")}:</ControlLabel>
                  <p>{dataForDetail.nomemetteur}</p>
                </Col>
                <Col xs={12} md={2}>
                  <ControlLabel>{t("form.label.prenom")}:</ControlLabel>
                  <p>{dataForDetail.prenomemeteur}</p>
                </Col>
                <Col xs={12} md={2}>
                  <ControlLabel>{t("form.label.ville")}:</ControlLabel>
                  <p> {dataForDetail.villeemetteur}</p>
                </Col>

                <Col xs={12} md={3}>
                  <ControlLabel>{t("form.label.adresse")}:</ControlLabel>
                  <p>{dataForDetail.adresseemeteur}</p>
                </Col>
                <Col xs={12} md={3}>
                  <ControlLabel>{t("form.label.codepostal")}:</ControlLabel>
                  <p>{dataForDetail.codepostalemetteur}</p>
                </Col>

                <Col xs={12} md={2}>
                  <ControlLabel>{t("form.label.mail")}:</ControlLabel>
                  <p>{dataForDetail.emailemetteur}</p>
                </Col>
                <Col xs={12} md={2}>
                  <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                  <p>{dataForDetail.paysemetteur}</p>
                </Col>
              </fieldset>
              <fieldset>
                <legend>{t("form.legend.infosbeneficiare")}</legend>

                <Col xs={12} md={3}>
                  <ControlLabel>
                    {t("form.label.numidentibeneficiare")}:
                  </ControlLabel>
                  <p>{dataForDetail.numeroPieceIdentitebeneficiare}</p>
                </Col>
                <Col xs={12} md={3}>
                  <ControlLabel>
                    {t("form.label.numidtelbeneficiare")}:
                  </ControlLabel>
                  <p>{dataForDetail.numerotelebeneficiare}</p>
                </Col>
                <Col xs={12} md={2}>
                  <ControlLabel>{t("form.label.nom")} :</ControlLabel>
                  <p>{dataForDetail.nombeneficiare}</p>
                </Col>
                <Col xs={12} md={2}>
                  <ControlLabel>{t("form.label.prenom")} :</ControlLabel>
                  <p>{dataForDetail.prenombeneficiare}</p>
                </Col>
                <Col xs={12} md={2}>
                  <ControlLabel>{t("form.label.ville")} </ControlLabel>
                  <p> {dataForDetail.villebeneficiare}</p>
                </Col>

                <Col xs={12} md={3}>
                  <ControlLabel>{t("form.label.adresse")} </ControlLabel>
                  <p>{dataForDetail.adressebeneficiare}</p>
                </Col>
                <Col xs={12} md={3}>
                  <ControlLabel>{t("form.label.codepostal")}</ControlLabel>
                  <p>{dataForDetail.codepostalbeneficiare}</p>
                </Col>

                <Col xs={12} md={2}>
                  <ControlLabel>{t("form.label.mail")}</ControlLabel>
                  <p>{dataForDetail.emailbeneficiare}</p>
                </Col>
                <Col xs={12} md={2}>
                  <ControlLabel>{t("form.label.pays")}:</ControlLabel>
                  <p>{dataForDetail.paysbeneficiare}</p>
                </Col>
              </fieldset>
              <fieldset>
                <legend>{t("form.legend.infostransfert")}</legend>
                <Col xs={12} md={3}>
                  <ControlLabel>
                    {t("form.label.montanttransfert")}:
                  </ControlLabel>
                  <p>{dataForDetail.montantOperation}</p>
                </Col>
                <Col xs={12} md={3}>
                  <ControlLabel>{t("form.label.fraienvoi")}</ControlLabel>
                  <p>{dataForDetail.fraisenvois}</p>
                </Col>

                <Col xs={12} md={3}>
                  <ControlLabel>{t("form.label.monatntremise")}</ControlLabel>
                  <p>{dataForDetail.montantremise}</p>
                </Col>
                <Col xs={12} md={3}>
                  <ControlLabel>{t("form.label.motifRemise")}</ControlLabel>
                  <p>{dataForDetail.motif}</p>
                </Col>
              </fieldset>
              {showAnnuler && (
                <Alert bsStyle="info">
                  <p>
                    <i className="fa fa-exclamation-circle" /> &nbsp;&nbsp;{" "}
                    {t("msg.annulertransaction")}
                  </p>
                </Alert>
              )}
              <Row className="pull-right">
                <Button bsStyle="primary">
                  {" "}
                  <a
                    className="glyphicon glyphicon-upload"
                    href={
                      baseUrl +
                      "transfertargentBNIF/createAvisTransfertPDF/" +
                      dataForDetail.id
                    }
                  >
                    {" "}
                  </a>
                  {t("form.buttons.uploadpdf")}
                </Button>
                <Button
                  onClick={() => {
                    setView("grid");
                    browserHistory.push(baseUrl + "app/TDBAgentCaisse");
                  }}
                  bsStyle="primary"
                >
                  <i className="fa fa-reply" />
                  &nbsp;&nbsp;{t("form.buttons.retour")}
                </Button>

                {showAnnuler && (
                  <Button
                    onClick={() => this.setState({ showModal: true })}
                    className={styles.ButtonStyle}
                  >
                    <i className="fa fa-times" />
                    &nbsp;&nbsp; {t("form.buttons.annuler")}
                  </Button>
                )}

                {showSupprimer && (
                  <Alert bsStyle="info">
                    <p>
                      <i className="fa fa-exclamation-circle" /> &nbsp;&nbsp;{" "}
                      {t("msg.supprimertransaction")}
                    </p>
                  </Alert>
                )}
                {showSupprimer && (
                  <Button
                    onClick={() => this.setState({ showModal: true })}
                    className={styles.ButtonStyle}
                  >
                    <i className="fa fa-times" />
                    &nbsp;&nbsp;{t("form.buttons.supprimer")}
                  </Button>
                )}
                <Modal
                  show={this.state.showModal}
                  onHide={close}
                  container={this}
                  aria-labelledby="contained-modal-title"
                >
                  <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title">
                      {t("popup.confirmation.title")}
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>{t("popup.supression.msg")}</Modal.Body>
                  <Modal.Footer>
                    <ButtonGroup className="pull-right" bsSize="small">
                      <Button
                        className={styles.ButtonPasswordStyle}
                        onClick={() => close()}
                      >
                        {t("popup.supression.noBtn")}
                      </Button>
                      <Button
                        className={styles.ButtonPasswordStyle}
                        onClick={() => {
                          close();
                          supprimerDemande(dataForDetail.id);
                        }}
                      >
                        {t("popup.supression.yesBtn")}
                      </Button>
                    </ButtonGroup>
                  </Modal.Footer>
                </Modal>

                <Modal
                  show={this.state.showModal}
                  onHide={close}
                  container={this}
                  aria-labelledby="contained-modal-title"
                >
                  <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title">
                      {t("popup.confirmation.title")}
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>{t("popup.confirmation.msg")}</Modal.Body>
                  <Modal.Footer>
                    <ButtonGroup className="pull-right" bsSize="small">
                      <Button
                        className={styles.ButtonPasswordStyle}
                        onClick={() => close()}
                      >
                        {t("popup.confirmation.noBtn")}
                      </Button>
                      <Button
                        className={styles.ButtonPasswordStyle}
                        onClick={() => {
                          close();
                          annulerDemande(dataForDetail.id);
                        }}
                      >
                        {t("popup.confirmation.yesBtn")}
                      </Button>
                    </ButtonGroup>
                  </Modal.Footer>
                </Modal>
              </Row>
            </div>
          )
        )}
      </div>
    );
  }
}
