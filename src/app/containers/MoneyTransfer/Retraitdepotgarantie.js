import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Alert,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  annulerTransfertCltCltByID,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { asyncConnect } from "redux-connect";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./moneytransferreducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import Button from "react-bootstrap-button-loader";
import * as UserActions from "../User/UserReducer";
import { AuthorizedComponent } from "react-router-role-authorization";

var idCaisseDT = sessionStorage.getItem("myData1");
var result = sessionStorage.getItem("myData");

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      const promises = [];
      if (result == null) {
        promises.push(
          dispatch(
            MoneyTransferAction.verificationvalidationattribution(
              getState().user.userFrontDetails.agenceId,
              "CATOAG",
              moment(),
              moment()
            )
          )
        );
        promises.push(dispatch(MoneyTransferAction.loadListPartenaireUser()));
        promises.push(
          dispatch(
            MoneyTransferAction.loadCaisse(
              getState().user.userFrontDetails.caisseId
            )
          )
        );
      } else {
        promises.push(
          dispatch(
            MoneyTransferAction.verificationvalidationattribution(
              result,
              "CATOAG",
              moment(),
              moment()
            )
          )
        );
        promises.push(dispatch(MoneyTransferAction.loadListPartenaireUser()));
        promises.push(dispatch(MoneyTransferAction.loadCaisse(idCaisseDT)));
      }
      if (idCaisseDT == null || idCaisseDT == "" || idCaisseDT == undefined) {
        if (getState().user.userFrontDetails.caisseId != "") {
          promises.push(
            dispatch(
              MoneyTransferAction.loadCaisse(
                getState().user.userFrontDetails.caisseId
              )
            )
          );
          promises.push(dispatch(MoneyTransferAction.loadListPartenaireUser()));
        }
      } else {
        promises.push(dispatch(MoneyTransferAction.loadCaisse(idCaisseDT)));
        promises.push(dispatch(MoneyTransferAction.loadListPartenaireUser()));
      }
      promises.push(dispatch(MoneyTransferAction.loadCompteGarantie(0)));
      return Promise.all(promises);
    },
  },
])
@connect(
  (state) => ({
    userFrontDetails: state.user.userFrontDetails,
    caisseObj: state.moneyTransfer.caisseObj,
    successverificationattribution:
      state.moneyTransfer.successverificationattribution,
    successRetrait: state.moneyTransfer.successRetrait,
    compteGarantie: state.moneyTransfer.compteGarantie,
    listPartenaire: state.moneyTransfer.listPartenaire,
    loadingRetrai: state.moneyTransfer.loadingRetrai,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@translate(["MoneyTransfer"], { wait: true })
export default class Retraitdepotgarantie extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      idPartenaire: "",
      montant: "",
      formValid: "",
    };
    this.setValidity = this.setValidity.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleTotalClick = this.handleTotalClick.bind(this);
    this.handleMontantChange = this.handleMontantChange.bind(this);
    this.RetraitDepotGarantieAsync = this.RetraitDepotGarantieAsync.bind(this);
  }

  handleChange(event) {
    this.setState({ idPartenaire: event.target.value });
  }
  handleMontantChange(event) {
    this.setState({ montant: event.target.value });
    this.setValidity(event.target.value);
  }
  async RetraitDepotGarantieAsync(
    agenceId,
    idCaisse,
    numerocaisse,
    idPartenaire,
    montant
  ) {
    await this.props.retraitDepotGarantie(
      agenceId,
      idCaisse,
      numerocaisse,
      idPartenaire,
      montant
    );
    this.setState({ montant: "0" });
  }
  setValidity(mnt) {
    if (mnt.length > 0) this.setState({ formValid: true });
    else this.setState({ formValid: false });
  }

  handleTotalClick(mnt) {
    this.setState({ montant: mnt });
    this.setValidity(mnt);
  }

  render() {
    const {
      t,
      loadCaisse,
      caisseObj,
      listPartenaire,
      compteGarantie,
      successRetrait,
      loadCompteGarantie,
      successverificationattribution,
      loadingRetrai,
      retraitDepotGarantie,
      userFrontDetails,
    } = this.props;
    const styles = require("./moneytransfer.scss");

    return (
      <div>
        <Row>
          <Col xs="12" md="6">
            <ControlLabel>
              <h2>{t("form.titleForm.Retraitdepotgarantie")} </h2>
            </ControlLabel>
          </Col>
        </Row>
        <Row>
          {successRetrait === true && (
            <Alert bsStyle="info">
              <strong>{t("msg.sucesseretraite")}</strong>
            </Alert>
          )}
          {userFrontDetails.caisseId === "" && (
            <Alert bsStyle="danger">
              <strong>{t("msg.aucunecaisseouverte")} </strong>
            </Alert>
          )}
        </Row>

        <Row className="detailsBloc">
          <Col xs="12" md="12">
            <Row className={styles.fieldRow}>
              <Col xs={12} md={4}>
                <ControlLabel>{t("form.label.dateaujourdui")}:</ControlLabel>
                <p className="detail-value">{moment().format("DD/MM/YYYY")}</p>
              </Col>
              <Col xs={12} md={4}>
                <ControlLabel>{t("form.label.agence")}: </ControlLabel>
                <p className="detail-value">
                  {userFrontDetails.caisseId !== "" &&
                  caisseObj != null &&
                  caisseObj != "undefined"
                    ? caisseObj.nomAgence
                    : userFrontDetails.agenceNom}
                </p>
              </Col>
              <Col xs={12} md={4}>
                <ControlLabel>{t("form.label.agent")}:</ControlLabel>
                <p className="detail-value">
                  {userFrontDetails.caisseId !== "" &&
                  caisseObj != null &&
                  caisseObj != "undefined"
                    ? caisseObj.nomAgent
                    : userFrontDetails.collaborateurNom}
                </p>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className={styles.fieldRow}>
          <form
            className="formContainer"
            style={{ padding: "30px 30px 100px" }}
          >
            <Col xs={12} md={3}>
              <ControlLabel>{t("form.label.partenaire")}: </ControlLabel>
              <FormControl
                componentClass="select"
                className={styles.datePickerFormControl}
                placeholder="select"
                onChange={(e) => {
                  loadCompteGarantie(e.target.value);
                  this.handleChange(e);
                }}
              >
                <option value="" hidden>
                  Selectionné un collaborateur
                </option>
                {listPartenaire &&
                  listPartenaire.length !== 0 &&
                  listPartenaire.map((collab) => (
                    <option value={collab.id}>
                      {" "}
                      {collab.lastName + " " + collab.userName}
                    </option>
                  ))}
              </FormControl>
            </Col>
            <Col xs="12" md="3">
              <ControlLabel>{t("form.label.montantaretrait")} :</ControlLabel>
              <FormControl
                type="text"
                className={styles.datePickerFormControl}
                onChange={this.handleMontantChange}
                value={this.state.montant}
              />
            </Col>
            <Col xs={12} md={3}>
              <ControlLabel> {t("form.label.garantieactuelle")}: </ControlLabel>
              <p className="detail-value">
                {" "}
                {compteGarantie !== "" &&
                compteGarantie != null &&
                compteGarantie != "undefined"
                  ? compteGarantie.solde
                  : "0"}{" "}
                <b>XOF</b>
              </p>
            </Col>
            <Col xs="12" md="2">
              <div className="pull-right">
                <Button
                  disabled={userFrontDetails.caisseId === ""}
                  loading={loadingRetrai}
                  onClick={(e) => {
                    this.RetraitDepotGarantieAsync(
                      caisseObj.idAgence,
                      caisseObj.id,
                      caisseObj.numCaisse,
                      this.state.idPartenaire,
                      this.state.montant
                    );
                  }}
                  bsStyle="primary"
                >
                  {t("form.buttons.confirmer")}
                </Button>
              </div>
            </Col>
            <Col xs="12" md="1">
              <div className="pull-right">
                <Button
                  onClick={() => {
                    browserHistory.push(baseUrl + "app/TDBAgentCaisse");
                  }}
                  bsStyle="primary"
                >
                  <i className="fa fa-times" />
                  {t("form.buttons.cancel")}
                </Button>
              </div>
            </Col>
          </form>
        </Row>
      </div>
    );
  }
}
