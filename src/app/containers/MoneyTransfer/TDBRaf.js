import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Label,
  Col,
  Panel,
  Row,
  ButtonGroup,
  Alert,
  Modal,
  ControlLabel,
  Form,
  FormControl,
  FormGroup,
  PanelGroup,
} from "react-bootstrap";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import { connect } from "react-redux";
import { initializeWithKey, reduxForm } from "redux-form";
import Griddle from "griddle-react";
import { browserHistory } from "react-router";
import DatePicker from "react-datepicker";
import moment from "moment";
import gridPagination from "./MoneyTransferPagination";
import SearchFormValidation from "./SearchFormValidation";
import * as MoneyTransferAction from "./moneytransferreducer";
import Button from "react-bootstrap-button-loader";
import * as UserActions from "../User/UserReducer";

class CenterComponent extends Component {
  render() {
    const url = this.props.data;
    return <div className="text-center">{this.props.data}</div>;
  }
}
@translate(["MoneyTransfer"], { wait: true })
class HeaderComponent extends Component {
  render() {
    return <div className="text-center">{this.props.displayName}</div>;
  }
}
@connect(
  (state) => ({
    loadingRegion: state.moneyTransfer.loadingRegion,
    userFrontDetails: state.user.userFrontDetails,
    regionObj: state.moneyTransfer.regionObj,
    transfetListRegion: state.moneyTransfer.transfetListRegion,
    listeAgenceByRegion: state.moneyTransfer.listeAgenceByRegion,
  }),

  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@asyncConnect([
  {
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];
      promises.push(dispatch(MoneyTransferAction.initializeForm()));
      promises.push(
        dispatch(
          MoneyTransferAction.loadRegionByIdRegion(
            getState().user.userFrontDetails.agenceId
          )
        )
      );
      promises.push(
        dispatch(
          MoneyTransferAction.loadListAgenceByRegion(
            getState().user.userFrontDetails.agenceId
          )
        )
      );
      promises.push(
        dispatch(
          MoneyTransferAction.loadListTransfertRegion(
            getState().user.userFrontDetails.agenceId,
            "",
            "",
            "",
            moment(),
            moment(),
            "",
            "",
            "",
            "",
            "",
            ""
          )
        )
      );
      return Promise.all(promises);
    },
  },
])
@reduxForm({
  form: "searchMoneyTransferAgence",
  fields: [
    "codeagence",
    "typeTransfert",
    "dateDebut",
    "dateFin",
    "nomEmetteur",
    "nomBenif",
    "montantMax",
    "montantMin",
    "statut",
    "idAgence",
    "codeCaisse",
  ],
  validate: SearchFormValidation,
  initialValues: {
    typeTransfert: "",
    dateDebut: "",
    dateFin: "",
    nomEmetteur: "",
    nomBenif: "",
    montantMax: "",
    montantMin: "",
    statut: "",
    idAgence: "",
    codeCaisse: "",
  },
  destroyOnUnmount: false,
})
@translate(["MoneyTransfer"], { wait: true })
export default class TDBRaf extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      startDate: moment(),
      endDate: moment(),
    };
    this.handleChangeStart = this.handleChangeStart.bind(this);
    this.handleChangeEnd = this.handleChangeEnd.bind(this);
  }

  componentWillMount() {}

  handleChangeStart(date) {
    this.setState({ startDate: date });
  }

  handleChangeEnd(date) {
    this.setState({ endDate: date });
  }
  componentDidMount() {
    console.log("componentDidMount");
    this.props.resetForm();
  }
  render() {
    const {
      t,
      showBouttonSupprimer,
      userFrontDetails,
      loadUserFrontDetails,
      loadRegionByIdRegion,
      initializeForm,
      loadListTransfertRegion,
      listeAgenceByRegion,
      fields: {
        codeagence,
        typeTransfert,
        dateDebut,
        dateFin,
        nomEmetteur,
        nomBenif,
        idAgence,
        codeCaisse,
        montantMax,
        montantMin,
        statut,
      },
      handleSubmit,
      values,
      resetForm,
      regionObj,
      transfetListRegion,
      loadingListTransfertAgence,
    } = this.props;

    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    values.dateDebut = this.state.startDate;
    values.dateFin = this.state.endDate;
    const gridMetaData = [
      {
        columnName: "dateTransfert",
        displayName: t("list.cols.dateOperation"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "typeTransfert",
        displayName: t("list.cols.typeOperation"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },

      {
        columnName: "nomEmetteur",
        displayName: t("list.cols.nomEmetteur"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },

      {
        columnName: "nomBenif",
        displayName: t("list.cols.nomBenif"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "codeCaisse",
        displayName: t("list.cols.codeCaisse"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "montant",
        displayName: t("list.cols.Montant"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "frais",
        displayName: t("list.cols.frais"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "statut",
        displayName: t("list.cols.statusoperation"),
        customHeaderComponent: HeaderComponent,
      },
    ];

    return (
      <div>
        <br /> <br /> <br />
        <Row>
          <Row className={styles.fieldRow}>
            <Col xs="12" md="12">
              <Col xs="12" md="3">
                <ControlLabel></ControlLabel>
              </Col>
              <Col xs="12" md="6">
                <ControlLabel>
                  <h2>{t("form.titleForm.tdbRaf")}</h2>
                </ControlLabel>
              </Col>
              <Col xs="12" md="3">
                <ControlLabel></ControlLabel>
              </Col>
            </Col>
          </Row>
          <br />
          <br />

          <br />

          <Row className={styles.paddingColumn}>
            <Col xs={12} md={3}>
              <Col xs={12} md={6}>
                <ControlLabel>{t("form.label.dateaujourdui")}:</ControlLabel>
              </Col>
              <Col xs={12} md={6}>
                {moment().format("DD/MM/YYYY")}
              </Col>
            </Col>
            <Col xs={12} md={3}>
              <Col xs={12} md={4}>
                <ControlLabel>{t("form.label.region")} : </ControlLabel>
              </Col>
              <Col xs={12} md={8}>
                {regionObj != null && regionObj != "undefined"
                  ? regionObj.libelle
                  : "-"}
              </Col>
            </Col>
            <Col xs={12} md={3}>
              <Col xs={12} md={4}>
                <ControlLabel>{t("form.label.agent")} :</ControlLabel>
              </Col>
              <Col xs={12} md={8}>
                {regionObj != null && regionObj != "undefined"
                  ? regionObj.nomcollaborateur
                  : "-"}
              </Col>
            </Col>
            <Col xs={12} md={3}>
              <Col xs={12} md={6}>
                <ControlLabel>{t("form.label.codeRegion")}:</ControlLabel>
              </Col>
              <Col xs={12} md={6}>
                {regionObj != null &&
                  regionObj != "undefined" &&
                  regionObj.codeRegion}
              </Col>
            </Col>
          </Row>
          <PanelGroup defaultActiveKey="1" accordion>
            <Panel
              header="Situation Régionale"
              eventKey="2"
              className={styles.accordionPanel}
              style={panelStyles}
            >
              <Row className={styles.paddingColumn}>
                <Col xs={12} md={4}>
                  <Col xs={12} md={6}>
                    <ControlLabel>{t("form.label.soldeactuel")}:</ControlLabel>
                  </Col>
                  <Col xs={12} md={6}>
                    {regionObj != null &&
                      regionObj != "undefined" &&
                      regionObj.soldeGlobal}{" "}
                    <b>CFA</b>
                  </Col>
                </Col>

                <Col xs={12} md={4}>
                  <Col xs={12} md={6}>
                    <ControlLabel>
                      {t("form.label.datemiseajour")}:
                    </ControlLabel>
                  </Col>
                  <Col xs={12} md={6}>
                    {regionObj != null &&
                      regionObj != "undefined" &&
                      regionObj.dateModif}
                  </Col>
                </Col>
              </Row>
            </Panel>
            <Panel
              header="Mouvements de la région "
              eventKey="3"
              className={styles.accordionPanel}
              style={panelStyles}
            >
              <Row className={styles.paddingColumn}>
                <Col xs={12} md={3}>
                  <ControlLabel>{t("form.label.totalenvoi")} : </ControlLabel>
                  {regionObj != null &&
                    regionObj != "undefined" &&
                    regionObj.totalEnvois}{" "}
                  <b>CFA</b>
                </Col>
                <Col xs={12} md={3}>
                  <ControlLabel>
                    {" "}
                    {t("form.label.totalmiseadisposotion")} :{" "}
                  </ControlLabel>
                  {regionObj != null &&
                    regionObj != "undefined" &&
                    regionObj.totalMiseDispositon}{" "}
                  <b>CFA</b>
                </Col>
                <Col xs={12} md={3}>
                  <ControlLabel>{t("form.label.totalretrait")}: </ControlLabel>
                  {regionObj != null &&
                    regionObj != "undefined" &&
                    regionObj.totalRetraits}{" "}
                  <b>CFA</b>
                </Col>
                <Col xs={12} md={3}>
                  <ControlLabel>
                    {t("form.label.totalattribution")}:{" "}
                  </ControlLabel>
                  {regionObj != null &&
                    regionObj != "undefined" &&
                    regionObj.totalAttributions}{" "}
                  <b>CFA</b>
                </Col>
              </Row>
            </Panel>
            <Panel
              header="Détails mouvements de la région"
              eventKey="4"
              className={styles.accordionPanel}
              style={panelStyles}
            >
              <form>
                <Row className={styles.paddingColumn}>
                  <Col xs="12" md="3">
                    <ControlLabel>{t("form.label.agence")} </ControlLabel>
                    <FormControl
                      componentClass="select"
                      className={styles.datePickerFormControl}
                      placeholder="select"
                      {...idAgence}
                    >
                      <option value="" hidden>
                        Selectionner une agence
                      </option>
                      {listeAgenceByRegion &&
                        listeAgenceByRegion.length !== 0 &&
                        listeAgenceByRegion.map((agence) => (
                          <option value={agence.id}> {agence.nomAgence}</option>
                        ))}
                    </FormControl>
                  </Col>
                  <Col xs={12} md={3}>
                    <ControlLabel>Numéro de Caisse: </ControlLabel>
                    <FormControl
                      {...codeCaisse}
                      type="text"
                      bsClass={styles.datePickerFormControl}
                    />
                  </Col>
                  <Col xs={12} md={3}>
                    <ControlLabel>
                      {t("form.label.typeoperation")}:{" "}
                    </ControlLabel>
                    <FormControl
                      {...typeTransfert}
                      componentClass="select"
                      bsClass={styles.datePickerFormControl}
                      placeholder="select"
                    >
                      <option value="" hidden></option>
                      <option key="1" value="CLTOCA">
                        Transfert
                      </option>
                      <option key="2" value="CATOCL">
                        Paiement
                      </option>
                      <option key="3" value="CATOAG">
                        Mise à disposition
                      </option>
                      <option key="4" value="AGTOCA">
                        Attribution
                      </option>
                    </FormControl>
                  </Col>
                  <Col xs={12} md={3}>
                    <ControlLabel>
                      {t("form.label.statusoperation")}:{" "}
                    </ControlLabel>
                    <FormControl
                      {...statut}
                      type="text"
                      bsClass={styles.datePickerFormControl}
                    />
                  </Col>
                </Row>

                <Row className={styles.paddingColumn}>
                  <Col xs={12} md={3}>
                    <ControlLabel>
                      {t("form.label.montantoperationmin")}:
                    </ControlLabel>
                    <FormControl
                      type="number"
                      {...montantMin}
                      bsClass={styles.datePickerFormControl}
                      placeholder=""
                      min="1"
                    />
                  </Col>
                  <Col xs={12} md={3}>
                    <ControlLabel>
                      {t("form.label.montantoperationmax")}:{" "}
                    </ControlLabel>
                    <FormControl
                      {...montantMax}
                      type="number"
                      bsClass={styles.datePickerFormControl}
                      min="1"
                    />
                  </Col>
                  <Col xs={12} md={3}>
                    <ControlLabel>
                      {" "}
                      {t("form.label.dateoperationmin")} :{" "}
                    </ControlLabel>
                    <DatePicker
                      selectsStart
                      selected={this.state.startDate}
                      startDate={this.state.startDate}
                      maxDate={this.state.endDate}
                      onChange={this.handleChangeStart}
                      className={styles.datePickerFormControl}
                      isClearable="true"
                      locale="fr-FR"
                    />
                  </Col>

                  <Col xs={12} md={3}>
                    <ControlLabel>
                      {t("form.label.dateoperationmax")} :{" "}
                    </ControlLabel>
                    <DatePicker
                      selectsEnd
                      selected={this.state.endDate}
                      endDate={this.state.endDate}
                      minDate={this.state.startDate}
                      maxDate={moment()}
                      onChange={this.handleChangeEnd}
                      className={styles.datePickerFormControl}
                      isClearable="true"
                      locale="fr-FR"
                    />
                  </Col>
                </Row>
                <Row className={styles.paddingColumn}>
                  <Col xs={12} md={3}>
                    <ControlLabel>{t("form.label.nomemeteur")} : </ControlLabel>
                    <FormControl
                      {...nomEmetteur}
                      type="text"
                      bsClass={styles.datePickerFormControl}
                    />
                  </Col>
                  <Col xs={12} md={3}>
                    <ControlLabel>
                      {t("form.label.nombeneficiare")} :{" "}
                    </ControlLabel>
                    <FormControl
                      {...nomBenif}
                      type="text"
                      bsClass={styles.datePickerFormControl}
                    />
                  </Col>
                </Row>
                <Row className={styles.paddingColumn}>
                  <Col xs={6} xsOffset={9}>
                    <Button
                      bsStyle="primary"
                      onClick={() => {
                        loadListTransfertRegion(
                          userFrontDetails.agenceId,
                          values.idAgence,
                          values.codeCaisse,
                          values.typeTransfert,
                          values.dateDebut,
                          values.dateFin,
                          values.nomEmetteur,
                          values.nomBenif,
                          values.montantMin,
                          values.montantMax,
                          values.statut
                        );
                      }}
                    >
                      <i className="fa fa-search" />
                      {t("list.search.buttons.search")}
                    </Button>
                    <Button
                      bsStyle="primary"
                      onClick={() => {
                        this.setState({
                          startDate: moment(),
                          endDate: moment(),
                        });
                        values.typeTransfert = "";
                        values.dateDebut = this.state.startDate;
                        values.dateFin = this.state.endDate;
                        values.montantMax = "";
                        values.montantMin = "";
                        values.statut = "";
                        resetForm();
                        loadListTransfertRegion(
                          userFrontDetails.agenceId,
                          "",
                          "",
                          "",
                          moment(),
                          moment(),
                          "",
                          "",
                          "",
                          "",
                          "",
                          ""
                        );
                      }}
                    >
                      <i className="fa fa-refresh" />{" "}
                      {t("form.buttons.reinitialiser")}
                    </Button>
                  </Col>
                </Row>
              </form>
              <Griddle
                results={transfetListRegion}
                columnMetadata={gridMetaData}
                useGriddleStyles={false}
                noDataMessage={t("list.search.msg.noResult")}
                resultsPerPage={10}
                nextText={<i className="fa fa-chevron-right" />}
                previousText={<i className="fa fa-chevron-left" />}
                tableClassName="table"
                columns={[
                  "dateTransfert",
                  "typeTransfert",
                  "nomEmetteur",
                  "nomBenif",
                  "codeCaisse",
                  "montant",
                  "frais",
                  "statut",
                ]}
              />
            </Panel>
          </PanelGroup>
        </Row>
      </div>
    );
  }
}
