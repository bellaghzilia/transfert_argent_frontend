import React, { Component } from "react";
import { Col, ControlLabel, Alert, FormControl, Row } from "react-bootstrap";
import * as MoneyTransferAction from "./moneytransferreducer";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import { browserHistory } from "react-router";
import { initializeWithKey } from "redux-form";
import { connect } from "react-redux";
import moment from "moment";
import * as UserActions from "../User/UserReducer";
import Button from "react-bootstrap-button-loader";
import { AuthorizedComponent } from "react-router-role-authorization";

var result = sessionStorage.getItem("myData");
@asyncConnect([
  {
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];
      if (result == "null" || result == null) {
        promises.push(
          dispatch(
            MoneyTransferAction.loadAgence(
              getState().user.userFrontDetails.agenceId
            )
          )
        );
        promises.push(
          dispatch(
            MoneyTransferAction.getListCaisseByAgence(
              true,
              getState().user.userFrontDetails.agenceId
            )
          )
        );
      } else {
        promises.push(dispatch(MoneyTransferAction.loadAgence(result)));
        promises.push(
          dispatch(MoneyTransferAction.getListCaisseByAgence(true, result))
        );
      }
      return Promise.all(promises);
    },
  },
])
@connect(
  (state) => ({
    loadingCloseCaisse: state.user.loadingCloseCaisse,
    caisseObj: state.moneyTransfer.caisseObj,
    etatCaisse: state.moneyTransfer.etatCaisse,
    successCloseCaisse: state.moneyTransfer.successCloseCaisse,
    messagevalidationattmise: state.moneyTransfer.messagevalidationattmise,
    listCaisse: state.moneyTransfer.listCaisse,
    agenceObj: state.moneyTransfer.agenceObj,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@translate(["MoneyTransfer"], { wait: true })
export default class ClotureForce extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      err: "",
      numCaisse: "",
      formValid: false,
    };
    this.changeEtatCaisses = this.changeEtatCaisses.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  changeEtatCaisses(caisse, etat) {
    if (caisse != null || caisse != undefined) caisse.etatCaisse = etat;
    return true;
  }

  async handleClick(e) {
    try {
      e.preventDefault();
      if (this.props.caisseObj != null && this.props.caisseObj != undefined) {
        let callWS = true;
        if (this.props.caisseObj.etatCaisse === "CLOSE") {
          callWS = false;
          this.setState({ err: "La caisse est deja fermé" });
          this.props.getListCaisseByAgence(
            true,
            this.props.userFrontDetails.agenceId
          );
        }
        if (callWS) {
          await this.props.closeForceCaisse(
            this.props.caisseObj,
            moment(),
            moment()
          );
          this.setState({ formValid: false });
          this.props.loadUserFrontDetails();
          this.props.getListCaisseByAgence(
            true,
            this.props.userFrontDetails.agenceId
          );
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  }

  componentWillMount() {
    this.setState({
      err: "",
    });
  }

  render() {
    const {
      t,
      caisseObj,
      listCaisse,
      agenceObj,
      closeCaisse,
      successCloseCaisse,
      messagevalidationattmise,
      etatCaisse,
      userFrontDetails,
      loadCaisseClotureForce,
      loadCaisse,
      loadingCloseCaisse,
      loadUserFrontDetails,
    } = this.props;
    const styles = require("./moneytransfer.scss");

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.titleForm.cloturecaisseforce")}</h2>
          </Col>
        </Row>

        <Row>
          {this.state.err !== "" && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{this.state.err}</Alert>
            </Col>
          )}
          {successCloseCaisse &&
            etatCaisse === "CLOSE" &&
            caisseObj != undefined &&
            this.changeEtatCaisses(caisseObj, etatCaisse) && (
              <Col xs="12" md="12">
                <Alert bsStyle="info">{t("msg.sucesscloturecaisse")}</Alert>
              </Col>
            )}
          {!successCloseCaisse && etatCaisse === "null" && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{t("msg.failedcloturecaisse")}</Alert>
            </Col>
          )}
          {successCloseCaisse === false && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{messagevalidationattmise}</Alert>
            </Col>
          )}
        </Row>

        <Row className="detailsBloc">
          <Col xs={12} md={4}>
            <ControlLabel>{t("form.label.dateaujourdui")}:</ControlLabel>
            <p className="detail-value">{moment().format("DD/MM/YYYY")}</p>
          </Col>
          <Col xs={12} md={4}>
            <ControlLabel>{t("form.label.agence")}: </ControlLabel>
            <p className="detail-value">
              {userFrontDetails.caisseId !== "" &&
              caisseObj != null &&
              caisseObj != "undefined"
                ? caisseObj.nomAgence
                : userFrontDetails.agenceNom}
            </p>
          </Col>
          <Col xs={12} md={4}>
            <ControlLabel>{t("form.label.agent")}:</ControlLabel>
            <p className="detail-value">
              {userFrontDetails.caisseId !== "" &&
              caisseObj != null &&
              caisseObj != "undefined"
                ? caisseObj.nomAgent
                : userFrontDetails.collaborateurNom}
            </p>
          </Col>
        </Row>

        <Row style={{ padding: "0 10px" }}>
          <form className="formContainer" style={{ padding: "30px 20px 20px" }}>
            <Row className={styles.fieldRow}>
              <Col xs={12} md={3}>
                <ControlLabel> {t("form.label.numeroCaisse")}: </ControlLabel>
                <FormControl
                  componentClass="select"
                  className={styles.datePickerFormControl}
                  placeholder="select"
                  onChange={(event) => {
                    this.setState({ numCaisse: event.target.value });
                    loadCaisseClotureForce(event.target.value, true);
                    if (event.target.value.length > 0)
                      this.setState({ formValid: true });
                    else this.setState({ formValid: false });
                  }}
                >
                  <option value="" hidden>
                    {t("form.hidden.selectionercaisse")}
                  </option>
                  {listCaisse &&
                    listCaisse.length !== 0 &&
                    listCaisse.map((caisseObjj) => (
                      <option value={caisseObjj}>{caisseObjj}</option>
                    ))}
                </FormControl>
              </Col>
              <Col xs={12} md={3}>
                <ControlLabel>{t("form.label.soldeCaisse")}:</ControlLabel>
                <FormControl
                  disabled="true"
                  value={
                    caisseObj.soldeActuel
                      ? caisseObj.soldeActuel + " XOF"
                      : 0 + " XOF"
                  }
                  type="text"
                  bsClass={styles.datePickerFormControl}
                />
              </Col>
              <Col xs={12} md={3}>
                <ControlLabel>{t("form.label.agent")}:</ControlLabel>
                <FormControl
                  disabled="true"
                  value={caisseObj.nomAgent}
                  type="text"
                  bsClass={styles.datePickerFormControl}
                />
              </Col>
              <Col xs={12} md={3}>
                <ControlLabel>{t("form.label.heure")}:</ControlLabel>
                <FormControl
                  disabled="true"
                  value={caisseObj.heureOuvreture}
                  type="text"
                  bsClass={styles.datePickerFormControl}
                />
              </Col>
            </Row>

            <Row className={styles.fieldRow} style={{ paddingTop: "15px" }}>
              <div className="pull-right">
                <Button
                  loading={loadingCloseCaisse}
                  disabled={!this.state.formValid}
                  onClick={this.handleClick}
                  bsStyle="primary"
                >
                  <i className="fa fa-expeditedssl" />{" "}
                  {t("form.buttons.coloturer")}
                </Button>
                <Button
                  onClick={() => {
                    browserHistory.push(baseUrl + "app/TDBAgentCaisse");
                  }}
                  bsStyle="primary"
                >
                  <i className="fa fa-times" />
                  {t("form.buttons.cancel")}
                </Button>
              </div>
            </Row>
          </form>
        </Row>
      </div>
    );
  }
}
