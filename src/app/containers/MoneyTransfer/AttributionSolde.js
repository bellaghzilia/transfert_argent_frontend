import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import { asyncConnect } from "redux-connect";
import moment from "moment";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./moneytransferreducer";
import { translate } from "react-i18next";
import Button from "react-bootstrap-button-loader";
import * as UserActions from "../User/UserReducer";
import { AuthorizedComponent } from "react-router-role-authorization";

var result = sessionStorage.getItem("myData");
@asyncConnect([
  {
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];
      if (result == null) {
        promises.push(
          dispatch(
            MoneyTransferAction.loadAgence(
              getState().user.userFrontDetails.agenceId
            )
          )
        );
        promises.push(
          dispatch(
            MoneyTransferAction.verificationvalidationattribution(
              getState().user.userFrontDetails.agenceId,
              "AGTOCA",
              moment(),
              moment()
            )
          )
        );
      } else {
        promises.push(dispatch(MoneyTransferAction.loadAgence(result)));

        promises.push(
          dispatch(
            MoneyTransferAction.verificationvalidationattribution(
              result,
              "AGTOCA",
              moment(),
              moment()
            )
          )
        );
      }
      return Promise.all(promises).then(() => {
        if (result == null) {
          dispatch(
            MoneyTransferAction.getListCaisseByAgence(
              true,
              getState().user.userFrontDetails.agenceId
            )
          ),
            dispatch(
              MoneyTransferAction.verificationvalidationattribution(
                getState().user.userFrontDetails.agenceId,
                "AGTOCA",
                moment(),
                moment()
              )
            );
        } else {
          dispatch(MoneyTransferAction.getListCaisseByAgence(true, result)),
            dispatch(
              MoneyTransferAction.verificationvalidationattribution(
                result,
                "AGTOCA",
                moment(),
                moment()
              )
            );
        }
      });
    },
  },
])
@connect(
  (state) => ({
    caisseObj: state.moneyTransfer.caisseObj,
    listCaisse: state.moneyTransfer.listCaisse,
    agenceObj: state.moneyTransfer.agenceObj,
    successverificationattribution:
      state.moneyTransfer.successverificationattribution,
    successAttribution: state.moneyTransfer.successAttribution,
    loadingAttribution: state.moneyTransfer.loadingAttribution,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@translate(["MoneyTransfer"], { wait: true })
export default class AttributionSolde extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      numCaisse: "",
      montant: "",
      formValid: false,
    };
    this.handleMontantChange = this.handleMontantChange.bind(this);
    this.AttributionAgenceCaisseAsync = this.AttributionAgenceCaisseAsync.bind(
      this
    );
  }

  handleMontantChange(event) {
    event.preventDefault();
    this.setState({ montant: event.target.value });
    if (event.target.value.length > 0 && this.state.numCaisse.length > 0)
      this.setState({ formValid: true });
    else this.setState({ formValid: false });
  }
  async AttributionAgenceCaisseAsync(agenceId, caisseNum, montant) {
    await this.props.attributionAgenceCaisse(
      agenceId,
      caisseNum,
      montant,
      moment(),
      moment()
    );
    this.setState({ montant: "0" });
  }

  render() {
    const {
      t,
      listCaisse,
      agenceObj,
      caisseObj,
      attributionAgenceCaisse,
      successverificationattribution,
      successAttribution,
      loadCaisse,
      loadingAttribution,
      userFrontDetails,
    } = this.props;
    const styles = require("./moneytransfer.scss");

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.titleForm.attributionsolde")}</h2>
          </Col>
        </Row>

        <Row>
          {successverificationattribution === true && (
            <Alert bsStyle="danger">{t("msg.attributionEnValidation")}</Alert>
          )}
          {successAttribution === true && (
            <Alert bsStyle="success">{t("msg.succesattribution")}</Alert>
          )}
          {successAttribution === false && (
            <Alert bsStyle="danger">Vous avez depassé le solde possible</Alert>
          )}
          {successAttribution == "null" && (
            <Alert bsStyle="danger">
              Une erreur est survenu lors de l'attribution
            </Alert>
          )}
          {userFrontDetails.etatAgence === "CLOSE" && (
            <Alert bsStyle="warning">Veuillez ouvrir l'agence !!!</Alert>
          )}
        </Row>

        <Row className="detailsBloc">
          <Col xs={12} md={3}>
            <ControlLabel>{t("form.label.dateaujourdui")}: </ControlLabel>
            <p className="detail-value">{moment().format("DD/MM/YYYY")}</p>
          </Col>
          <Col xs={12} md={3}>
            <ControlLabel>{t("form.label.agence")}: </ControlLabel>
            <p className="detail-value">{agenceObj.nomAgence}</p>
          </Col>
          <Col xs={12} md={3}>
            <ControlLabel>{t("form.label.chefagence")}: </ControlLabel>
            <p className="detail-value">{agenceObj.nomAgent}</p>
          </Col>
          <Col xs={12} md={3}>
            <ControlLabel>{t("form.label.nombrecaisseouvert")}:</ControlLabel>
            <p className="detail-value">{agenceObj.nbrCaisse}</p>
          </Col>
        </Row>

        <Row style={{ padding: "0 10px" }}>
          <form className="formContainer" style={{ padding: "30px 20px 20px" }}>
            <Row className={styles.fieldRow}>
              <Col xs={12} md={3}>
                <ControlLabel> {t("form.label.numeroCaisse")} : </ControlLabel>
                <FormControl
                  componentClass="select"
                  bsClass={styles.datePickerFormControl}
                  placeholder="select"
                  onChange={(event) => {
                    this.setState({ numCaisse: event.target.value });
                    loadCaisse(event.target.value, true);
                    if (
                      event.target.value.length > 0 &&
                      this.state.montant.length > 0
                    )
                      this.setState({ formValid: true });
                    else this.setState({ formValid: false });
                  }}
                >
                  <option value="" hidden>
                    {t("form.hidden.selectionercaisse")}
                  </option>
                  {listCaisse &&
                    listCaisse.length !== 0 &&
                    listCaisse.map((caisseObjj) => (
                      <option value={caisseObjj}>{caisseObjj}</option>
                    ))}
                </FormControl>
              </Col>
              <Col xs={12} md={3}>
                <ControlLabel>{t("form.label.soldeCaisse")}:</ControlLabel>
                <FormControl
                  disabled="true"
                  value={
                    caisseObj.soldeActuel
                      ? caisseObj.soldeActuel + " XOF"
                      : 0 + " XOF"
                  }
                  type="text"
                  bsClass={styles.datePickerFormControl}
                />
              </Col>
              <Col xs={12} md={3}>
                <ControlLabel>{t("form.label.agent")}:</ControlLabel>
                <FormControl
                  disabled="true"
                  value={caisseObj.nomAgent}
                  type="text"
                  bsClass={styles.datePickerFormControl}
                />
              </Col>
              <Col xs={12} md={3}>
                <ControlLabel>{t("form.label.heure")}:</ControlLabel>
                <FormControl
                  disabled="true"
                  value={caisseObj.heureOuvreture}
                  type="text"
                  bsClass={styles.datePickerFormControl}
                />
              </Col>
            </Row>
            <Row className={styles.fieldRow} style={{ paddingTop: "15px" }}>
              <Col xs="12" md="2" style={{ paddingTop: "6px" }}>
                <ControlLabel>
                  {t("form.label.montantaatribution")}:
                </ControlLabel>
              </Col>
              <Col xs="12" md="7">
                <FormControl
                  type="text"
                  bsClass={styles.datePickerFormControl}
                  onChange={this.handleMontantChange}
                  value={this.state.montant}
                />
              </Col>
              <div className="pull-right">
                <Button
                  loading={loadingAttribution}
                  disabled={
                    !this.state.formValid ||
                    userFrontDetails.etatAgence === "CLOSE"
                  }
                  onClick={(e) => {
                    this.AttributionAgenceCaisseAsync(
                      agenceObj.id,
                      this.state.numCaisse,
                      this.state.montant
                    );
                  }}
                  bsStyle="primary"
                >
                  {t("form.buttons.confirmer")}
                </Button>
                <Button
                  bsStyle="primary"
                  onClick={() => {
                    browserHistory.push(baseUrl + "app/TabeBordChefAgence");
                  }}
                >
                  <i className="fa fa-times" />
                  {t("form.buttons.cancel")}
                </Button>
              </div>
            </Row>
          </form>
        </Row>
      </div>
    );
  }
}
