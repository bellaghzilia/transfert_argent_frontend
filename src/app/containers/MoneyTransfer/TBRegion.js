// import React, {Component, PropTypes} from 'react';
// import {
//     Label,
//     Col,
//     Panel,
//     Row,
//     ButtonGroup,
//     Alert,
//     Modal,
//     ControlLabel,
//     Form,
//     FormControl,
//     FormGroup,
//     PanelGroup
// } from 'react-bootstrap';
// import {translate} from "react-i18next";
// import {asyncConnect} from 'redux-connect';
// import {connect} from 'react-redux';
// import {initializeWithKey, reduxForm} from 'redux-form';
// import Griddle from 'griddle-react';
// import {browserHistory} from 'react-router';
// import DatePicker from 'react-datepicker';
// import moment from 'moment';
// import gridPagination from './MoneyTransferPagination';
// import SearchFormValidation from './SearchFormValidation';
// import * as MoneyTransferAction from './moneytransferreducer';
// import Button from 'react-bootstrap-button-loader';
// import * as UserActions from '../User/UserReducer';
// import { AuthorizedComponent } from 'react-router-role-authorization';
//
// class CenterComponent extends Component {
//     render() {
//         const url = this.props.data;
//         return <div>{this.props.data}</div>;
//     }
// }
//
// class MontantComponent extends Component {
//     render() {
//         return <div> {this.props.data} <b>XOF</b></div>;
//     }
// }
//
// @translate(['MoneyTransfer'], {wait: true})
// class HeaderComponent extends Component {
//     render() {
//         return (<div>{this.props.displayName}</div>);
//     }
// }
// var result;
// var myData = sessionStorage.getItem('myData');
// if (myData != null) {
//     result = myData;
// } else {
//     var url1 = window.location.href;
//     var captured2;
//     captured2 = /idAgence=([^&]+)/.exec(url1)[1];
// // setter
//     window.sessionStorage.setItem("myData", captured2 ? captured2 : 'null');
// // getter
//     result = sessionStorage.getItem('myData');
// }
//
//
// @connect(
//     state => ({
//         loadingRegion: state.moneyTransfer.loadingRegion,
//         userFrontDetails: state.user.userFrontDetails,
//         regionObj: state.moneyTransfer.regionObj,
//         transfetListRegion: state.moneyTransfer.transfetListRegion,
//         listeAgenceByRegion: state.moneyTransfer.listeAgenceByRegion,
//         listPaimentNonPayer: state.moneyTransfer.listPaimentNonPayer,
//         listVille: state.moneyTransfer.listVille,
//         loadingPaimentNonPayer: state.moneyTransfer.loadingPaimentNonPayer,
//         transfetListAgncAgncRegion: state.moneyTransfer.transfetListAgncAgncRegion,
//         totalmontantagenceregion: state.moneyTransfer.totalmontantagenceregion,
//         loadingListTransfertAgncAgncRegion: state.moneyTransfer.loadingListTransfertAgncAgncRegion,
//         listOuvertureClotureRegion: state.moneyTransfer.listOuvertureClotureRegion,
//         idAgence:result,
//         user: state.user,
//         userFrontDetails: state.user.userFrontDetails,
//
//
//     }),
//
//     {...MoneyTransferAction, ...UserActions, initializeWithKey})
//
//
// @asyncConnect([{
//     promise: ({store: {dispatch, getState}}) => {
//         const promises = [];
//          promises.push(dispatch(MoneyTransferAction.RestForm()));
//         if(result==null){
//         promises.push(dispatch(MoneyTransferAction.initializeForm()));
//         promises.push(dispatch(MoneyTransferAction.loadRegionByIdRegion(getState().user.userFrontDetails.agenceId)));
//         promises.push(dispatch(MoneyTransferAction.loadVilleByIdRegion(getState().user.userFrontDetails.agenceId)));
//         promises.push(dispatch(MoneyTransferAction.loadPaiementNonPayer(getState().user.userFrontDetails.agenceId)));
//         promises.push(dispatch(MoneyTransferAction.loadListAgenceByRegion(getState().user.userFrontDetails.agenceId)));
//         promises.push(dispatch(MoneyTransferAction.loadListTransfertRegion(getState().user.userFrontDetails.agenceId, '', '', '', moment(), moment(), '', '', '', '', '', '')));
//         promises.push(dispatch(MoneyTransferAction.loadListOuvertureClotureByRegion(getState().user.userFrontDetails.agenceId,'', '', '', '', '', moment(), moment(), '', '')));
//         promises.push(dispatch(MoneyTransferAction.loadListTransfertAgenceAgenceRegion(getState().user.userFrontDetails.agenceId, '', '', '', '', moment(), moment(), '', '', '', '', '', '')));
//         }else {
//           promises.push(dispatch(MoneyTransferAction.initializeForm()));
//           promises.push(dispatch(MoneyTransferAction.loadRegionByIdRegion(result)));
//           promises.push(dispatch(MoneyTransferAction.loadVilleByIdRegion(result)));
//           promises.push(dispatch(MoneyTransferAction.loadPaiementNonPayer(result)));
//           promises.push(dispatch(MoneyTransferAction.loadListAgenceByRegion(result)));
//           promises.push(dispatch(MoneyTransferAction.loadListTransfertRegion(result, '', '', '', moment(), moment(), '', '', '', '', '', '')));
//           promises.push(dispatch(MoneyTransferAction.loadListOuvertureClotureByRegion(result,'', '', '', '', '', moment(), moment(), '', '')));
//           promises.push(dispatch(MoneyTransferAction.loadListTransfertAgenceAgenceRegion(result, '', '', '', '', moment(), moment(), '', '', '', '', '', '')));
//
//         }
//         return Promise.all(promises);
//     }
// }])
//
// @reduxForm({
//         form: 'searchMoneyTransferAgence',
//         fields: ['codeagence', 'typeTransfert', 'dateDebut', 'dateFin', 'nomEmetteur', 'nomBenif', 'montantMax', 'montantMin', 'statut', 'idAgence', 'codeCaisse', 'nomAgence', 'montantTotPNP', 'villePNP',
//             'agenceEchange', 'dateEmissionDebut', 'dateEmissionFin', 'dateReceptionDebut', 'dateReceptionFin', 'montantOperationDebut', 'montantOperationFin', 'statutOperation',
//             'agenceAction', 'numcaisse', 'agentaction', 'montantinitial', 'montantfinal', 'dateActionDebut', 'dateActionFin', 'heuremin', 'heuremax'],
//         validate: SearchFormValidation,
//         initialValues: {
//             typeTransfert: '',
//             dateDebut: '',
//             dateFin: '',
//             nomEmetteur: '',
//             nomBenif: '',
//             montantMax: '',
//             montantMin: '',
//             statut: '',
//             idAgence: '',
//             codeCaisse: '',
//             nomAgence: '',
//             montantTotPNP: '',
//             villePNP: '',
//             agenceEchange: '',
//             dateEmissionDebut: '',
//             dateEmissionFin: '',
//             dateReceptionDebut: '',
//             dateReceptionFin: '',
//             montantOperationDebut: '',
//             montantOperationFin: '',
//             statutOperation: '',
//             numcaisse: '',
//             agentaction: '',
//             montantinitial: '',
//             montantfinal: '',
//             dateActionDebut: '',
//             dateActionFin: '',
//             heuremin: '',
//             heuremax: '',
//             agenceAction: ''
//         },
//         destroyOnUnmount: false
//     },
// )
// @translate(['MoneyTransfer'], {wait: true})
// export default class TBRegion extends AuthorizedComponent {
//     constructor(props) {
//         super(props);
//         this.userRoles = this.props.userFrontDetails.roles;
//         this.notAuthorizedPath = baseUrl + 'app/AccessDenied';
//         this.state = {
//             showModal: false,
//             startDate: moment(),
//             endDate: moment(),
//             startReceptionDate: '',
//             endReceptionDate: '',
//             startEmissionDate: moment(),
//             endEmissionDate: moment(),
//             startActionDate: moment(),
//             endActionDate: moment(),
//             maxmin1:false,
//             maxmin2:false,
//             maxmin3:false,
//         };
//         this.handleChangeStart = this.handleChangeStart.bind(this);
//         this.handleChangeEnd = this.handleChangeEnd.bind(this);
//         this.handleChangeEmissionStart = this.handleChangeEmissionStart.bind(this);
//         this.handleChangeEmissionEnd = this.handleChangeEmissionEnd.bind(this);
//         this.handleChangeReceptionStart = this.handleChangeReceptionStart.bind(this);
//         this.handleChangeReceptionEnd = this.handleChangeReceptionEnd.bind(this);
//         this.handleChangeActionStart = this.handleChangeActionStart.bind(this);
//         this.handleChangeActionEnd = this.handleChangeActionEnd.bind(this);
//
//     }
//
//     componentWillMount() {
//     }
//
//     handleChangeStart(date) {
//         this.setState({startDate: date});
//     }
//
//     handleChangeEnd(date) {
//         this.setState({endDate: date});
//     }
//
//     handleChangeReceptionStart(date) {
//         this.setState({startReceptionDate: date});
//     };
//
//     handleChangeReceptionEnd(date) {
//         this.setState({endReceptionDate: date});
//     };
//
//     handleChangeEmissionStart(date) {
//         this.setState({startEmissionDate: date});
//     };
//
//     handleChangeEmissionEnd(date) {
//         this.setState({endEmissionDate: date});
//     };
//
//     handleChangeActionStart(date) {
//         this.setState({startActionDate: date});
//     };
//
//     handleChangeActionEnd(date) {
//         this.setState({endActionDate: date});
//     };
//      componentDidMount(){
//         console.log("componentDidMount")
//         this.props.resetForm();
//     }
//     render() {
//         const {
//             t, showBouttonSupprimer,
//             userFrontDetails, loadUserFrontDetails, loadRegionByIdRegion, initializeForm, loadListTransfertRegion, listeAgenceByRegion,
//             fields: {
//                 codeagence, typeTransfert, dateDebut, dateFin, nomEmetteur, nomBenif, idAgence, codeCaisse,
//                 montantMax, montantMin, statut, montantTotPNP, villePNP,
//                 agenceEchange, dateEmissionDebut, dateEmissionFin, dateReceptionDebut, dateReceptionFin, montantOperationDebut, montantOperationFin, statutOperation,
//                 agenceAction, numcaisse, agentaction, montantinitial, montantfinal, dateActionDebut, dateActionFin, heuremin, heuremax
//             }, handleSubmit, values, resetForm, regionObj, transfetListRegion,totalmontantagenceregion, transfetListAgncAgncRegion, listPaimentNonPayer, listVille, loadPaiementNonPayer, loadingPaimentNonPayer, loadingListTransfertAgncAgncRegion, loadListTransfertAgenceAgenceRegion, loadListOuvertureClotureByRegion, listOuvertureClotureRegion, loadingListOuvertureClotureRegion
//         } = this.props;
//
//         const styles = require('./moneytransfer.scss');
//         const panelStyles = {margin: '5px 10px 5px 10px'};
//         values.dateDebut = this.state.startDate;
//         values.dateFin = this.state.endDate;
//
//         const actionGridMetaData = [
//             {
//                 columnName: 'agenceAction',
//                 displayName: 'Nom Agence',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'numcaisse',
//                 displayName: 'Numero de Caisse',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'agentaction',
//                 displayName: 'Agent',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'montantinitial',
//                 displayName: 'Total attribution',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'montantfinal',
//                 displayName: 'Total mise à disposition',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'dateaction',
//                 displayName: 'Date d\'action',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'heuredebut',
//                 displayName: 'Heure debut',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'heurefin',
//                 displayName: 'Heure fin',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             }
//
//         ];
//
//
//         const gridMetaDataAgncAgnc = [
//             {
//                 columnName: 'agenceEmitteur',
//                 displayName: 'Agence émission',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'agenceRecepteur',
//                 displayName: 'Agence réception',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'dateEmission',
//                 displayName: 'Date émission',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//             },
//             {
//                 columnName: 'dateReception',
//                 displayName: 'Date réception',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'montantOperation',
//                 displayName: 'Montant opération',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: MontantComponent,
//
//             },
//             {
//                 columnName: 'statutOperation',
//                 displayName: 'Statut opération',
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             }
//         ];
//         const gridMetaData = [
//
//             {
//                 columnName: 'dateTransfert',
//                 displayName: t('list.cols.dateOperation'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'typeTransfert',
//                 displayName: t('list.cols.typeOperation'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//
//             {
//                 columnName: 'nomEmetteur',
//                 displayName: t('list.cols.nomEmetteur'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'nomBenif',
//                 displayName: t('list.cols.nomBenif'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'codeCaisse',
//                 displayName: t('list.cols.codeCaisse'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'montant',
//                 displayName: t('list.cols.Montant'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'frais',
//                 displayName: t('list.cols.frais'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'statut',
//                 displayName: t('list.cols.statusoperation'),
//                 customHeaderComponent: HeaderComponent,
//
//             }
//         ];
//         const gridMetaDataPNP = [
//
//             {
//                 columnName: 'nomRegion',
//                 displayName: t('list.cols.region'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//             {
//                 columnName: 'nomVille',
//                 displayName: t('list.cols.ville'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: CenterComponent,
//
//             },
//
//             {
//                 columnName: 'mnttotal',
//                 displayName: t('list.cols.montanttotal'),
//                 customHeaderComponent: HeaderComponent,
//                 customComponent: MontantComponent,
//
//             }
//         ];
//         values.dateEmissionDebut = this.state.startEmissionDate;
//         values.dateEmissionFin = this.state.endEmissionDate;
//         values.dateReceptionDebut = this.state.startReceptionDate;
//         values.dateReceptionFin = this.state.endReceptionDate;
//         values.dateActionDebut = this.state.startActionDate;
//         values.dateActionFin = this.state.endActionDate;
//         return (
//
//
//             <div>
//                 <Row className={styles.fieldRow}>
//                     <Col xs="12" md="12">
//                             <h2>{t('form.titleForm.tbchefregional')}</h2>
//                     </Col>
//                 </Row>
//                 <Row className="detailsBloc">
//                     <Col xs={12} md={3}>
//                             <ControlLabel>{t('form.label.dateaujourdui')}:</ControlLabel>
//                         <p className="detail-value">{moment().format('DD/MM/YYYY')}</p>
//                     </Col>
//                     <Col xs={12} md={3}>
//                             <ControlLabel>{t('form.label.region')} : </ControlLabel>
//                         <p className="detail-value">{regionObj != null && regionObj != "undefined" ? regionObj.libelle : "-"}</p>
//                     </Col>
//                     <Col xs={12} md={3}>
//                             <ControlLabel>{t('form.label.agent')} :</ControlLabel>
//                         <p className="detail-value">{regionObj != null && regionObj != "undefined" ? regionObj.nomcollaborateur : "-"}</p>
//                     </Col>
//                     <Col xs={12} md={3}>
//                             <ControlLabel>{t('form.label.codeRegion')}:</ControlLabel>
//                         <p className="detail-value"> {regionObj != null && regionObj != "undefined" && regionObj.codeRegion}</p>
//                     </Col>
//                 </Row>
//
//                     <PanelGroup defaultActiveKey="1" accordion>
//
//                         <Panel header="Situation Régionale" eventKey="2" className={styles.accordionPanel}
//                                style={panelStyles}>
//
//                             <Row className={styles.paddingColumn}>
//                                 <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.soldeactuel')}:</ControlLabel>
//                                     <p className="detail-value">{regionObj != null && regionObj != "undefined" && regionObj.soldeGlobal} XOF</p>
//                                 </Col>
//
//                                 <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.datemiseajour')}:</ControlLabel>
//                                     <p className="detail-value">{regionObj != null && regionObj != "undefined" && regionObj.dateModif}</p>
//                                 </Col>
//                             </Row>
//                         </Panel>
//                         <Panel header="Mouvements de la région " eventKey="3" className={styles.accordionPanel}
//                                style={panelStyles}>
//
//                             <Row className={styles.paddingColumn}>
//                                 <Col xs={12} md={3}>
//                                     <ControlLabel>{t('form.label.totalenvoi')} : </ControlLabel>
//                                     <p className="detail-value">{regionObj != null && regionObj != "undefined" && regionObj.totalEnvois} XOF</p>
//                                 </Col>
//                                 <Col xs={12} md={3}>
//                                     <ControlLabel> {t('form.label.totalmiseadisposotion')} : </ControlLabel>
//                                     <p className="detail-value">{regionObj != null && regionObj != "undefined" && regionObj.totalMiseDispositon} XOF</p>
//                                 </Col>
//                                 <Col xs={12} md={3}>
//                                     <ControlLabel>{t('form.label.totalretrait')}: </ControlLabel>
//                                     <p className="detail-value">{regionObj != null && regionObj != "undefined" && regionObj.totalRetraits} XOF</p>
//                                 </Col>
//                                 <Col xs={12} md={3}>
//                                     <ControlLabel>{t('form.label.totalattribution')}: </ControlLabel>
//                                     <p className="detail-value">{regionObj != null && regionObj != "undefined" && regionObj.totalAttributions} XOF</p>
//                                 </Col>
//
//                             </Row>
//                         </Panel>
//                         {this.state.maxmin1 === true &&
//                         <div className="alert alert-danger">
//                             {t('msg.msgMinMax')}
//                         </div>
//                         }
//                         <Panel header="Détails mouvements des caisses de la région" eventKey="4"
//                                className={styles.accordionPanel} style={panelStyles}>
//                             <form>
//
//                                 <Row className={styles.paddingColumn}>
//                                     <Col xs="12" md="3">
//                                         <ControlLabel>{t('form.label.agence')} </ControlLabel>
//                                         <FormControl componentClass="select"
//                                                      className={styles.datePickerFormControl}
//                                                      placeholder="select"  {...idAgence}
//                                         >
//                                             <option value="" hidden>Selectionner une agence</option>
//                                             {
//                                                 listeAgenceByRegion && listeAgenceByRegion.length !== 0 && listeAgenceByRegion.map((agence) =>
//                                                     <option value={agence.id}> {agence.nomAgence}</option>
//                                                 )
//                                             }
//                                         </FormControl>
//
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>Numéro de Caisse: </ControlLabel>
//                                         <FormControl {...codeCaisse} type="text" bsClass={styles.datePickerFormControl}/>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.typeoperation')}: </ControlLabel>
//                                         <FormControl {...typeTransfert} componentClass="select"
//                                                      bsClass={styles.datePickerFormControl}
//                                                      placeholder="select">
//                                             <option value="" hidden></option>
//                                             <option key="1" value="CLTOCA">Transfert</option>
//                                             <option key="2" value="CATOCL">Paiement</option>
//                                             <option key="3" value="CATOAG">Mise à disposition</option>
//                                             <option key="4" value="AGTOCA">Attribution</option>
//                                         </FormControl>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.statusoperation')}: </ControlLabel>
//                                         <FormControl {...statut} type="text" bsClass={styles.datePickerFormControl}/>
//                                     </Col>
//                                 </Row>
//
//                                 <Row className={styles.paddingColumn}>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.montantoperationmin')}:</ControlLabel>
//                                         <FormControl type="number" {...montantMin}
//                                                      bsClass={styles.datePickerFormControl} placeholder=""
//                                                      min="1"/>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.montantoperationmax')}: </ControlLabel>
//                                         <FormControl {...montantMax}
//                                                      type="number"
//                                                      bsClass={styles.datePickerFormControl}
//                                                      min="1"/>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> {t('form.label.dateoperationmin')} : </ControlLabel>
//                                         <DatePicker
//                                             selectsStart
//                                             selected={this.state.startDate}
//                                             startDate={this.state.startDate}
//                                             maxDate={this.state.endDate}
//                                             onChange={this.handleChangeStart}
//                                             className={styles.datePickerFormControl}
//                                             isClearable="true"
//                                             locale="fr-FR"
//                                         />
//                                     </Col>
//
//
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.dateoperationmax')} : </ControlLabel>
//                                         <DatePicker
//                                             selectsEnd
//                                             selected={this.state.endDate}
//                                             endDate={this.state.endDate}
//                                             minDate={this.state.startDate}
//                                             maxDate={moment()}
//                                             onChange={this.handleChangeEnd}
//                                             className={styles.datePickerFormControl}
//                                             isClearable="true"
//                                             locale="fr-FR"
//                                         />
//                                     </Col>
//                                 </Row>
//                                 <Row className={styles.paddingColumn}>
//                                     <Col xs={12} md={6}>
//                                         <ControlLabel>{t('form.label.nomemeteur')} : </ControlLabel>
//                                         <FormControl {...nomEmetteur} type="text"
//                                                      bsClass={styles.datePickerFormControl}/>
//                                     </Col>
//                                     <Col xs={12} md={6}>
//                                         <ControlLabel>{t('form.label.nombeneficiare')} : </ControlLabel>
//                                         <FormControl {...nomBenif} type="text"
//                                                      bsClass={styles.datePickerFormControl}/>
//                                     </Col>
//                                 </Row>
//                                 <Row className={styles.paddingColumn}>
//                                     <div className="pull-right">
//                                         <Button
//                                             bsStyle="primary"
//                                             onClick={() => {
//                                                 if(montantMin.value>montantMax.value){
//
//                                                     this.setState({
//                                                         maxmin1:true,
//                                                     });
//                                                     window.setTimeout(() => {
//                                                         this.setState({
//                                                             maxmin1: false
//                                                         });
//                                                     }, 6000);
//                                                 }
//                                                 else{
//                                                 loadListTransfertRegion(userFrontDetails.agenceId, values.idAgence, values.codeCaisse, values.typeTransfert, values.dateDebut, values.dateFin, values.nomEmetteur, values.nomBenif, values.montantMin, values.montantMax, values.statut)
//                                             }}}
//                                         >
//                                             <i className="fa fa-search"/>{t('list.search.buttons.search')}
//                                         </Button>
//                                         <Button
//                                             bsStyle="primary"
//
//                                             onClick={() => {
//                                                 this.setState({startDate: moment(), endDate: moment()});
//                                                 values.typeTransfert = '';
//                                                 values.dateDebut = this.state.startDate;
//                                                 values.dateFin = this.state.endDate;
//                                                 values.montantMax = '';
//                                                 values.montantMin = '';
//                                                 values.statut = '';
//                                                 resetForm();
//                                                 loadListTransfertRegion(userFrontDetails.agenceId, '', '', '', moment(), moment(), '', '', '', '', '', '');
//                                             }}
//                                         >
//                                             <i className="fa fa-refresh"/> {t('form.buttons.reinitialiser')}
//                                         </Button>
//                                     </div>
//                                 </Row>
//                             </form>
//                             <Row className="table-responsive">
//                                 <Griddle
//                                     results={transfetListRegion}
//                                     columnMetadata={gridMetaData}
//                                     useGriddleStyles={false}
//                                     noDataMessage={t('list.search.msg.noResult')}
//                                     resultsPerPage={10}
//                                     nextText={<i className="fa fa-chevron-right"/>}
//                                     previousText={<i className="fa fa-chevron-left"/>}
//                                     tableClassName="table"
//                                     columns={['dateTransfert', 'typeTransfert', 'nomEmetteur', 'nomBenif', 'codeCaisse', 'montant', 'frais', 'statut']}
//
//                                 />
//                             </Row>
//                               <Row className="table-responsive">
//                                 <Button bsStyle="primary"
//                                           onClick={() => window.open(baseUrl + 'regionTransfertArgent/region/' + userFrontDetails.agenceId + '/listtransfert?idAgence1=' + values.idAgence + '&codeCaisse=' + values.codeCaisse + '&typeTransfert=' + values.typeTransfert + '&dateTransfertDebut=' + values.dateTransfertDebut + '&dateTransfertFin=' + values.dateTransfertFin + '&nomEmetteur=' + values.nomEmetteur + '&nomBenif=' + values.nomBenif + '&montantMin=' + values.montantMin + '&montantMax=' + values.montantMax + '&agent=' + values.agent + '&statut=' + values.statut)}>
//                                     <i className="fa fa-file-excel-o" /> liste Transferts
//                                 </Button>
//                                  <Col xs={12} md={2}>
//                                 <div style={{border:'1px solid #e4e4e4', background:'#fff', padding:'10px'}}>
//                                 <span className="icon-layers"/> Total transferts caisses : {this.props.totalmontantagenceregion} <b>CFA</b>
//                                 </div>
//                                 </Col>
//
//                                 </Row>
//                         </Panel>
//                         {this.state.maxmin2 === true &&
//                         <div className="alert alert-danger">
//                             {t('msg.msgMinMax')}
//                         </div>
//                         }
//                         <Panel header="Details des mouvements des agences de la region" eventKey="8"
//                                className={styles.accordionPanel}
//                                style={panelStyles}>
//                             <form>
//                                 <Row className={styles.paddingColumn}>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> {t('form.label.agenceechange')}: </ControlLabel>
//                                         <FormControl componentClass="select"
//                                                      className={styles.datePickerFormControl}
//                                                      placeholder="select"  {...agenceEchange}
//
//                                         >
//                                             <option value="" hidden>Selectionner une agence</option>
//                                             {
//                                                 listeAgenceByRegion && listeAgenceByRegion.length !== 0 && listeAgenceByRegion.map((agence) =>
//                                                     <option value={agence.id}> {agence.nomAgence}</option>
//                                                 )
//                                             }
//                                         </FormControl>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.dateemissionmin')} : </ControlLabel>
//                                         <DatePicker
//                                             selected={this.state.startEmissionDate}
//                                             selectsStart
//                                             startDate={this.state.startEmissionDate}
//                                             maxDate={this.state.endEmissionDate}
//                                             onChange={this.handleChangeEmissionStart}
//                                             className={styles.datePickerFormControl}
//                                             dateFormat='DD/MM/YYYY'
//                                             isClearable="true"
//                                         />
//                                     </Col>
//
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.datereceptionmin')} : </ControlLabel>
//                                         <DatePicker
//                                             selected={this.state.startReceptionDate}
//                                             selectsStart
//                                             startDate={this.state.startReceptionDate}
//                                             maxDate={this.state.endReceptionDate}
//                                             onChange={this.handleChangeReceptionStart}
//                                             className={styles.datePickerFormControl}
//                                             dateFormat='DD/MM/YYYY'
//                                             isClearable="true"
//                                         />
//                                     </Col>
//
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.montantoperationmin')} : </ControlLabel>
//                                         <FormControl {...montantOperationDebut} type="number"
//                                                      bsClass={styles.datePickerFormControl} min="1"/>
//                                     </Col>
//
//
//                                 </Row>
//
//                                 <Row className={styles.paddingColumn}>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.typeoperation')} : </ControlLabel>
//                                         <FormControl {...typeTransfert} componentClass="select"
//                                                      bsClass={styles.datePickerFormControl} placeholder="select">
//                                             <option value="" hidden></option>
//                                             <option key="1" value="AGTOCAG">Mise à disposition Agence</option>
//                                             <option key="2" value="CAGTOAG">Encaissment</option>
//                                         </FormControl>
//                                     </Col>
//
//
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel>{t('form.label.dateemissionmax')} : </ControlLabel>
//                                         <DatePicker
//                                             selectsEnd
//                                             selected={this.state.endEmissionDate}
//                                             endDate={this.state.endEmissionDate}
//                                             minDate={this.state.startEmissionDate}
//                                             maxDate={moment()}
//                                             onChange={this.handleChangeEmissionEnd}
//                                             className={styles.datePickerFormControl}
//                                             isClearable="true"
//                                             locale="fr-FR"
//                                             dateFormat="DD/MM/YYYY"
//                                         />
//
//                                     </Col>
//
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> {t('form.label.datereceptionmax')} : </ControlLabel>
//                                         <DatePicker
//                                             selectsEnd
//                                             selected={this.state.endReceptionDate}
//                                             endDate={this.state.endReceptionDate}
//                                             minDate={this.state.startReceptionDate}
//                                             maxDate={moment()}
//                                             onChange={this.handleChangeReceptionEnd}
//                                             className={styles.datePickerFormControl}
//                                             isClearable="true"
//                                             locale="fr-FR"
//                                             dateFormat="DD/MM/YYYY"
//                                         />
//                                     </Col>
//
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> {t('form.label.montantoperationmax')}: </ControlLabel>
//                                         <FormControl {...montantOperationFin} type="number"
//                                                      bsClass={styles.datePickerFormControl} min="1"/>
//
//                                     </Col>
//
//
//                                 </Row>
//                                 <Row className={styles.paddingColumn}>
//                                     <div className="pull-right">
//                                         <Button
//                                             loading={loadingListTransfertAgncAgncRegion}
//                                             bsStyle="primary"
//                                             onClick={() => {
//                                                 if(montantOperationDebut.value>montantOperationFin.value){
//
//                                                     this.setState({
//                                                         maxmin2:true,
//                                                     });
//                                                     window.setTimeout(() => {
//                                                         this.setState({
//                                                             maxmin2: false
//                                                         });
//                                                     }, 6000);
//                                                 }
//                                                 else{
//                                                 loadListTransfertAgenceAgenceRegion(userFrontDetails.agenceId, values.typeTransfert, values.agenceEchange, values.dateReceptionDebut, values.dateReceptionFin, values.dateEmissionDebut, values.dateEmissionFin, values.montantOperationDebut, values.montantOperationFin);
//                                             }}}
//                                         >
//                                             <i className="fa fa-search"/>Rechercher
//                                         </Button>
//                                         <Button
//                                             bsStyle="primary"
//                                             onClick={() => {
//                                                 this.setState({
//                                                     startEmissionDate: moment(),
//                                                     endEmissionDate: moment(),
//                                                     startReceptionDate: '',
//                                                     endReceptionDate: ''
//                                                 });
//                                                 values.agenceEchange = '';
//                                                 values.montantOperationDebut = '';
//                                                 values.montantOperationFin = '';
//                                                 values.dateReceptionDebut = this.state.startReceptionDate;
//                                                 values.dateReceptionFin = this.state.endReceptionDate;
//                                                 values.dateEmissionDebut = this.state.startEmissionDate;
//                                                 values.dateEmissionFin = this.state.endEmissionDate;
//                                                 values.typeTransfert = '';
//                                                 resetForm();
//                                                 loadListTransfertAgenceAgenceRegion(userFrontDetails.agenceId, '', '', '', '', moment(), moment(), '', '');
//                                             }}
//
//                                         >
//                                             <i className="fa fa-refresh"/>{t('form.buttons.reinitialiser')}
//                                         </Button>
//                                     </div>
//                                 </Row>
//                             </form>
//                             <Row className="table-responsive">
//                                     <Griddle
//                                         results={transfetListAgncAgncRegion}
//                                         useGriddleStyles={false}
//                                         noDataMessage={t('list.search.msg.noResult')}
//                                         resultsPerPage={10}
//                                         nextText={<i className="fa fa-chevron-right"/>}
//                                         previousText={<i className="fa fa-chevron-left"/>}
//                                         tableClassName="table"
//                                         columns={['agenceEmitteur', 'agenceRecepteur', 'dateEmission', 'dateReception', 'montantOperation', 'statutOperation']}
//                                         columnMetadata={gridMetaDataAgncAgnc}
//                                     />
//                             </Row>
//                              <Row className="table-responsive">
//                                 <Button bsStyle="primary"
//
//                                         onClick={() => window.open(baseUrl + 'regionTransfertArgent/generateCSV/'+userFrontDetails.agenceId+'/listtransfertagncagnc?typeTransfert='+values.typeTransfert+'&agenceEchange='+values.agenceEchange+'&dateReceptionDebut='+values.dateReceptionDebut+'&dateReceptionFin='+values.dateReceptionFin+'&dateEmissionDebut='+values.dateEmissionDebut+'&dateEmissionFin='+values.dateEmissionFin+'&montantOperationDebut='+values.montantOperationDebut+'&montantOperationFin='+values.montantOperationFin)}>
//                                     <i className="fa fa-file-excel-o" /> liste Transferts
//                                 </Button>
//                                  <Col xs={12} md={2}>
//                                 <div style={{border:'1px solid #e4e4e4', background:'#fff', padding:'10px'}}>
//                                 <span className="icon-layers"/> Total : {this.props.totalmontantagenceregion} <b>CFA</b>
//                                 </div>
//                                 </Col>
//
//                                 </Row>
//
//                         </Panel>
//
//                         {this.state.maxmin3 === true &&
//                         <div className="alert alert-danger">
//                             {t('msg.msgMinMax')}
//                         </div>
//                         }
//                         <Panel header="Les mouvements d'ouverture clôture des caisses" eventKey="7"
//                                className={styles.accordionPanel}
//                                style={panelStyles}>
//                             <form>
//                                 <Row className={styles.paddingColumn}>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel> Numero de Caisse : </ControlLabel>
//                                         <FormControl {...numcaisse} type="text"
//                                                      bsClass={styles.datePickerFormControl}/>
//                                     </Col>
//                                     <Col xs={12} md={2}>
//                                         <ControlLabel>Date Min : </ControlLabel>
//                                         <DatePicker
//                                             selected={this.state.startActionDate}
//                                             selectsStart
//                                             startDate={this.state.startActionDate}
//                                             maxDate={this.state.endActionDate}
//                                             onChange={this.handleChangeActionStart}
//                                             className={styles.datePickerFormControl}
//                                             dateFormat='DD/MM/YYYY'
//                                             isClearable="true"
//                                         />
//                                     </Col>
//                                     <Col xs={12} md={2}>
//                                         <ControlLabel>Date Max : </ControlLabel>
//                                         <DatePicker
//                                             selectsEnd
//                                             selected={this.state.endActionDate}
//                                             endDate={this.state.endActionDate}
//                                             minDate={this.state.startActionDate}
//                                             maxDate={moment()}
//                                             onChange={this.handleChangeReceptionEnd}
//                                             className={styles.datePickerFormControl}
//                                             isClearable="true"
//                                             locale="fr-FR"
//                                             dateFormat="DD/MM/YYYY"
//                                         />
//                                     </Col>
//                                     <Col xs={6} md={2}>
//                                         <ControlLabel>Heure Debut : </ControlLabel>
//                                         <FormControl {...heuremin} type="number"
//                                                      bsClass={styles.datePickerFormControl} min="1"/>
//                                     </Col>
//                                     <Col xs={6} md={2}>
//                                         <ControlLabel> Heure Clôture : </ControlLabel>
//                                         <FormControl {...heuremax} type="number"
//                                                      bsClass={styles.datePickerFormControl} min="1"/>
//                                     </Col>
//                                 </Row>
//
//                                 <Row className={styles.paddingColumn}>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> {t('form.label.agenceechange')}: </ControlLabel>
//                                         <FormControl componentClass="select"
//                                                      className={styles.datePickerFormControl}
//                                                      placeholder="select"  {...agenceAction}
//
//                                         >
//                                             <option value="" hidden>Selectionner une agence</option>
//                                             {
//                                                 listeAgenceByRegion && listeAgenceByRegion.length !== 0 && listeAgenceByRegion.map((agence) =>
//                                                     <option value={agence.id}> {agence.nomAgence}</option>
//                                                 )
//                                             }
//                                         </FormControl>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> Agent : </ControlLabel>
//                                         <FormControl {...agentaction} type="text"
//                                                      bsClass={styles.datePickerFormControl}/>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> Montant Initial : </ControlLabel>
//                                         <FormControl {...montantinitial} type="number"
//                                                      bsClass={styles.datePickerFormControl} min="1"/>
//                                     </Col>
//                                     <Col xs={12} md={3}>
//                                         <ControlLabel> Montant Final : </ControlLabel>
//                                         <FormControl {...montantfinal} type="number"
//                                                      bsClass={styles.datePickerFormControl} min="1"/>
//                                     </Col>
//                                 </Row>
//                                 <Row className={styles.paddingColumn}>
//                                     <div className="pull-right">
//                                         <Button
//                                             loading={loadingListOuvertureClotureRegion}
//                                             bsStyle="primary"
//                                             onClick={() => {
//                                                 if(montantinitial.value>montantfinal.value){
//
//                                                     this.setState({
//                                                         maxmin3:true,
//                                                     });
//                                                     window.setTimeout(() => {
//                                                         this.setState({
//                                                             maxmin3: false
//                                                         });
//                                                     }, 6000);
//                                                 }
//                                                 else{
//                                                 loadListOuvertureClotureByRegion(userFrontDetails.agenceId, values.agenceAction, values.numcaisse, values.agentaction, values.montantinitial, values.montantfinal, values.dateActionDebut, values.dateActionFin, values.heuremin, values.heuremax);
//                                             }}}
//                                         >
//                                             <i className="fa fa-search"/>Rechercher
//                                         </Button>
//                                         <Button
//                                             bsStyle="primary"
//                                             onClick={() => {
//                                                 this.setState({
//                                                     startActionDate: moment(),
//                                                     endActionDate: moment(),
//                                                 });
//
//                                                 resetForm();
//                                                 loadListOuvertureClotureByRegion(userFrontDetails.agenceId, '', '', '', '', '', moment(), moment(), '', '');
//                                             }}
//                                         >
//                                             <i className="fa fa-refresh"/>{t('form.buttons.reinitialiser')}
//                                         </Button>
//                                     </div>
//                                 </Row>
//                             </form>
//                             <Row className="table-responsive">
//                                     <Griddle
//                                         results={listOuvertureClotureRegion}
//                                         useGriddleStyles={false}
//                                         noDataMessage={t('list.search.msg.noResult')}
//                                         resultsPerPage={10}
//                                         nextText={<i className="fa fa-chevron-right"/>}
//                                         previousText={<i className="fa fa-chevron-left"/>}
//                                         tableClassName="table"
//                                         columns={['agenceAction', 'numcaisse', 'agentaction', 'montantinitial', 'montantfinal', 'dateaction', 'heuredebut', 'heurefin']}
//
//                                         columnMetadata={actionGridMetaData}
//                                     />
//                             </Row>
//                             <Row className="table-responsive">
//                                 <Button bsStyle="primary"
//                                   onClick={() => window.open(baseUrl + 'regionTransfertArgent/generateCSV/' + userFrontDetails.agenceId + '/listouvetturecloture?agenceAction='+values.agenceAction+'&numcaisse=' + values.numcaisse + '&agentaction=' + values.agentaction + '&montantinitial=' + values.montantinitial + '&montantfinal=' + values.montantfinal + '&dateActionDebut=' + values.dateActionDebut + '&dateActionFin=' + values.dateActionFin + '&heuredebut=' + values.heuremin + '&heurefin=' + values.heuremax)}>
//                                     <i className="fa fa-file-excel-o" /> liste ouvertures clotures
//                                 </Button>
//
//
//                                 </Row>
//                         </Panel>
//
//
//                         <Panel header="Les estimation des paiements de la région" eventKey="6"
//                                className={styles.accordionPanel} style={panelStyles}>
//                             <form>
//                                 <Row className={styles.paddingColumn}>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>Region : </ControlLabel>
//                                         <FormControl type="text"
//                                                      value={regionObj != null && regionObj != "undefined" ? regionObj.libelle : "-"}
//                                                      bsClass={styles.datePickerFormControl}/>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.ville')} </ControlLabel>
//                                         <FormControl componentClass="select"
//                                                      className={styles.datePickerFormControl}
//                                                      placeholder="select"  {...villePNP}
//                                         >
//
//                                             <option value="" hidden>Selectionner une ville</option>
//                                             {
//                                                 listVille && listVille.length !== 0 && listVille.map((ville) =>
//                                                     <option value={ville.id}> {ville.libelle}</option>
//                                                 )
//                                             }
//                                         </FormControl>
//                                     </Col>
//                                     <Col xs={12} md={4}>
//                                         <ControlLabel>{t('form.label.montant')} : </ControlLabel>
//                                         <FormControl type="number"  {...montantTotPNP}
//                                                      bsClass={styles.datePickerFormControl}/>
//                                     </Col>
//                                 </Row>
//                                 <Row className={styles.paddingColumn}>
//                                     <div className="pull-right">
//                                         <Button bsStyle="primary" loading={loadingPaimentNonPayer}
//                                             onClick={() => {loadPaiementNonPayer(userFrontDetails.agenceId, values.villePNP, values.montantTotPNP); }}
//                                          ><i className="fa fa-search"/>{t('list.search.buttons.search')}
//                                         </Button>
//                                         <Button
//                                             bsStyle="primary"
//                                             loading={loadingPaimentNonPayer}
//                                             onClick={() => {
//                                                 values.villePNP = '';values.montantTotPNP = '';
//                                                 resetForm();loadPaiementNonPayer(userFrontDetails.agenceId, "", "");
//                                             }}
//                                         ><i className="fa fa-refresh"/> {t('form.buttons.reinitialiser')}
//                                         </Button>
//                                     </div>
//                                 </Row>
//                             </form>
//                             <Row className="table-responsive">
//                                 <Griddle
//                                     results={listPaimentNonPayer}
//                                     columnMetadata={gridMetaDataPNP}
//                                     useGriddleStyles={false}
//                                     noDataMessage={t('list.search.msg.noResult')}
//                                     resultsPerPage={10}
//                                     nextText={<i className="fa fa-chevron-right"/>}
//                                     previousText={<i className="fa fa-chevron-left"/>}
//                                     tableClassName="table"
//                                     columns={['nomRegion', 'nomVille', 'mnttotal']}
//                                 />
//                             </Row>
//                             <Row className="table-responsive">
//                                 <Button bsStyle="primary"
//                                 onClick={() => window.open(baseUrl + 'regionTransfertArgent/listEstimationCSV?idVille='+values.villePNP+'&idAgence=' + userFrontDetails.agenceId + '&montant=' + values.montantTotPNP )}>
//                                 <i className="fa fa-file-excel-o" /> liste estimations
//                                 </Button>
//                             </Row>
//                         </Panel>
//                     </PanelGroup>
//             </div>
//         );
//     }
// }
