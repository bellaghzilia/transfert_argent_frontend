const SAVE_PAYS = 'Parametrage/SAVE_PAYS';
const SAVE_PAYS_SUCCESS = 'Parametrage/SAVE_PAYSE_SUCCESS';
const SAVE_PAYS_FAIL = 'Parametrage/SAVE_PAYS_FAIL';

const UPDATE_PAYS = 'Parametrage/UPDATE_PAYS';
const UPDATE_PAYS_SUCCESS = 'Parametrage/UPDATE_PAYS_SUCCESS';
const UPDATE_PAYS_FAIL = 'Parametrage/UPDATE_PAYS_FAIL';

const LOAD_LIST_PAYS = 'Parametrage/LOAD_LIST_PAYS';
const LOAD_LIST_PAYS_SUCCESS = 'Parametrage/LOAD_LIST_PAYS_SUCCESS';
const LOAD_LIST_PAYS_FAIL = 'Parametrage/LOAD_LIST_PAYS_FAIL';

const DELETE_PAYS = 'Parametrage/DELETE_PAYS';
const DELETE_PAYS_SUCCESS = 'Parametrage/DELETE_PAYSE_SUCCESS';
const DELETE_PAYS_FAIL = 'Parametrage/DELETE_PAYS_FAIL';


const GET_INSTANCE = 'MoneyTransfer/GET_INSTANCE';
const GET_INSTANCE_SUCCESS = 'MoneyTransfer/GET_INSTANCE_SUCCESS';
const GET_INSTANCE_FAIL = 'MoneyTransfer/GET_INSTANCE_FAIL';


const CHE_MONTANT_FRAIS = 'MoneyTransfer/CHE_MONTANT_FRAIS';
const CHE_MONTANT_SUCCESS = 'MoneyTransfer/CHE_MONTANT_SUCCESS';
const CHE_MONTANT_FAIL = 'MoneyTransfer/CHE_MONTANT_FAIL';


const CHE_DATE = 'MoneyTransfer/CHE_DATE';
const CHE_DATE_SUCCESS = 'MoneyTransfer/CHE_DATE_SUCCESS';
const CHE_DATE_FAIL = 'MoneyTransfer/CHE_DATE_FAIL';


const CHE_DATE_EXPE = 'MoneyTransfer/CHE_DATE_EXPE';
const CHE_DATE_EXPE_SUCCESS = 'MoneyTransfer/CHE_DATE_EXPE_SUCCESS';
const CHE_DATE_EXPE_FAIL = 'MoneyTransfer/CHE_DATE_EXPE_FAIL';


const SAVE_VILLES = 'Parametrage/SAVE_VILLES';
const SAVE_VILLES_SUCCESS = 'Parametrage/SAVE_VILLES_SUCCESS';
const SAVE_VILLES_FAIL = 'Parametrage/SAVE_VILLES_FAIL';

const UPDATE_VILLES = 'Parametrage/UPDATE_VILLES';
const UPDATE_VILLES_SUCCESS = 'Parametrage/UPDATE_VILLES_SUCCESS';
const UPDATE_VILLES_FAIL = 'Parametrage/UPDATE_VILLES_FAIL';

const LOAD_LIST_VILLES = 'Parametrage/LOAD_LIST_VILLES';
const LOAD_LIST_VILLES_SUCCESS = 'Parametrage/LOAD_LIST_VILLES_SUCCESS';
const LOAD_LIST_VILLES_FAIL = 'Parametrage/LOAD_LIST_VILLES_FAIL';

const DELETE_VILLES = 'Parametrage/DELETE_VILLES';
const DELETE_VILLES_SUCCESS = 'Parametrage/DELETE_VILLES_SUCCESS';
const DELETE_VILLES_FAIL = 'Parametrage/DELETE_VILLES_FAIL';


const GET_INSTANCE_VILLES = 'MoneyTransfer/GET_INSTANCE_VILLES';
const GET_INSTANCE_VILLES_SUCCESS = 'MoneyTransfer/GET_INSTANCE_VILLES_SUCCESS';
const GET_INSTANCE_VILLES_FAIL = 'MoneyTransfer/GET_INSTANCE_VILLES_FAIL';


const SAVE_CODE = 'Parametrage/SAVE_CODE';
const SAVE_CODE_SUCCESS = 'Parametrage/SAVE_CODE_SUCCESS';
const SAVE_CODE_FAIL = 'Parametrage/SAVE_CODE_FAIL';

const SAVE_REGION = 'Parametrage/SAVE_REGION';
const SAVE_REGION_SUCCESS = 'Parametrage/SAVE_REGION_SUCCESS';
const SAVE_REGION_FAIL = 'Parametrage/SAVE_REGION_FAIL';


const SAVE_AGENCE = 'Parametrage/SAVE_AGENCE';
const SAVE_AGENCE_SUCCESS = 'Parametrage/SAVE_AGENCE_SUCCESS';
const SAVE_AGENCE_FAIL = 'Parametrage/SAVE_AGENCE_FAIL';


const SAVE_SHEMA = 'Parametrage/SAVE_SHEMA';
const SAVE_SHEMA_SUCCESS = 'Parametrage/SAVE_SHEMA_SUCCESS';
const SAVE_SHEMA_FAIL = 'Parametrage/SAVE_SHEMA_FAIL';


const SAVE_USER = 'Parametrage/SAVE_USER';
const SAVE_USER_SUCCESS = 'Parametrage/SAVE_USER_SUCCESS';
const SAVE_USER_FAIL = 'Parametrage/SAVE_USER_FAIL';


const SAVE_PARTENAIRE = 'Parametrage/SAVE_PARTENAIRE';
const SAVE_PARTENAIRE_SUCCESS = 'Parametrage/SAVE_PARTENAIRE_SUCCESS';
const SAVE_PARTENAIRE_FAIL = 'Parametrage/SAVE_PARTENAIRE_FAIL';

const UPDATE_CODE = 'Parametrage/UPDATE_CODE';
const UPDATE_CODE_SUCCESS = 'Parametrage/UPDATE_CODE_SUCCESS';
const UPDATE_CODE_FAIL = 'Parametrage/UPDATE_CODE_FAIL';


const UPDATE_USER = 'Parametrage/UPDATE_USER';
const UPDATE_USER_SUCCESS = 'Parametrage/UPDATE_USER_SUCCESS';
const UPDATE_USER_FAIL = 'Parametrage/UPDATE_USER_FAIL';


const UPDATE_PARTENAIRE = 'Parametrage/UPDATE_PARTENAIRE';
const UPDATE_PARTENAIRE_SUCCESS = 'Parametrage/UPDATE_PARTENAIRE_SUCCESS';
const UPDATE_PARTENAIRE_FAIL = 'Parametrage/UPDATE_PARTENAIRE_FAIL';


const UPDATE_REGION = 'Parametrage/UPDATE_REGION';
const UPDATE_REGION_SUCCESS = 'Parametrage/UPDATE_REGION_SUCCESS';
const UPDATE_REGION_FAIL = 'Parametrage/UPDATE_REGION_FAIL';


const UPDATE_AGENCE = 'Parametrage/UPDATE_AGENCE';
const UPDATE_AGENCE_SUCCESS = 'Parametrage/UPDATE_AGENCE_SUCCESS';
const UPDATE_AGENCE_FAIL = 'Parametrage/UPDATE_AGENCE_FAIL';


const UPDATE_CAISSE = 'Parametrage/UPDATE_CAISSE';
const UPDATE_CAISSE_SUCCESS = 'Parametrage/UPDATE_CAISSE_SUCCESS';
const UPDATE_CAISSE_FAIL = 'Parametrage/UPDATE_CAISSE_FAIL';


const UPDATE_SHEMA = 'Parametrage/UPDATE_SHEMA';
const UPDATE_SHEMA_SUCCESS = 'Parametrage/UPDATE_SHEMA_SUCCESS';
const UPDATE_SHEMA_FAIL = 'Parametrage/UPDATE_SHEMA_FAIL';


const UPDATE_FRAIS = 'Parametrage/UPDATE_FRAIS';
const UPDATE_FRAIS_SUCCESS = 'Parametrage/UPDATE_FRAIS_SUCCESS';
const UPDATE_FRAIS_FAIL = 'Parametrage/UPDATE_FRAIS_FAIL';


const SAVE_CAISSE = 'Parametrage/SAVE_CAISSE';
const SAVE_CAISSE_SUCCESS = 'Parametrage/SAVE_CAISSE_SUCCESS';
const SAVE_CAISSE_FAIL = 'Parametrage/SAVE_CAISSE_FAIL';

const SAVE_FRAIS = 'Parametrage/SAVE_FRAIS';
const SAVE_FRAIS_SUCCESS = 'Parametrage/SAVE_FRAIS_SUCCESS';
const SAVE_FRAIS_FAIL = 'Parametrage/SAVE_FRAIS_FAIL';

const LOAD_LIST_CODE = 'Parametrage/LOAD_LIST_CODE';
const LOAD_LIST_CODE_SUCCESS = 'Parametrage/LOAD_LIST_CODE_SUCCESS';
const LOAD_LIST_CODE_FAIL = 'Parametrage/LOAD_LIST_CODE_FAIL';

const DELETE_CODE = 'Parametrage/DELETE_CODE';
const DELETE_CODE_SUCCESS = 'Parametrage/DELETE_CODE_SUCCESS';
const DELETE_CODE_FAIL = 'Parametrage/DELETE_CODE_FAIL';

const DELETE_REGION = 'Parametrage/DELETE_REGION';
const DELETE_REGION_SUCCESS = 'Parametrage/DELETE_REGION_SUCCESS';
const DELETE_REGION_FAIL = 'Parametrage/DELETE_REGION_FAIL';


const DELETE_AGENCE = 'Parametrage/DELETE_AGENCE';
const DELETE_AGENCE_SUCCESS = 'Parametrage/DELETE_AGENCE_SUCCESS';
const DELETE_AGENCE_FAIL = 'Parametrage/DELETE_AGENCE_FAIL';


const DELETE_CAISSE = 'Parametrage/DELETE_CAISSE';
const DELETE_CAISSE_SUCCESS = 'Parametrage/DELETE_CAISSE_SUCCESS';
const DELETE_CAISSE_FAIL = 'Parametrage/DELETE_CAISSE_FAIL';


const DELETE_SHEMA = 'Parametrage/DELETE_SHEMA';
const DELETE_SHEMA_SUCCESS = 'Parametrage/DELETE_SHEMA_SUCCESS';
const DELETE_SHEMA_FAIL = 'Parametrage/DELETE_SHEMA_FAIL';


const DELETE_CO = 'Parametrage/DELETE_CO';
const DELETE_CO_SUCCESS = 'Parametrage/DELETE_CO_SUCCESS';
const DELETE_CO_FAIL = 'Parametrage/DELETE_CO_FAIL';

const DELETE_USER = 'Parametrage/DELETE_USER';
const DELETE_USER_SUCCESS = 'Parametrage/DELETE_USER_SUCCESS';
const DELETE_USER_FAIL = 'Parametrage/DELETE_USER_FAIL';

const DELETE_PARTENAIRE = 'Parametrage/DELETE_PARTENAIRE';
const DELETE_PARTENAIRE_SUCCESS = 'Parametrage/DELETE_PARTENAIRE_SUCCESS';
const DELETE_PARTENAIRE_FAIL = 'Parametrage/DELETE_PARTENAIRE_FAIL';

const GET_INSTANCE_CODE = 'MoneyTransfer/GET_INSTANCE_CODE';
const GET_INSTANCE_CODE_SUCCESS = 'MoneyTransfer/GET_INSTANCE_CODE_SUCCESS';
const GET_INSTANCE_CODE_FAIL = 'MoneyTransfer/GET_INSTANCE_CODE_FAIL';


const GET_INSTANCE_REGION = 'MoneyTransfer/GET_INSTANCE_REGION';
const GET_INSTANCE_REGION_SUCCESS = 'MoneyTransfer/GET_INSTANCE_REGION_SUCCESS';
const GET_INSTANCE_REGION_FAIL = 'MoneyTransfer/GET_INSTANCE_REGION_FAIL';


const GET_INSTANCE_AGENCE = 'MoneyTransfer/GET_INSTANCE_AGENCE';
const GET_INSTANCE_AGENCE_SUCCESS = 'MoneyTransfer/GET_INSTANCE_AGENCE_SUCCESS';
const GET_INSTANCE_AGENCE_FAIL = 'MoneyTransfer/GET_INSTANCE_AGENCE_FAIL';


const GET_INSTANCE_SHEMA = 'MoneyTransfer/GET_INSTANCE_SHEMA';
const GET_INSTANCE_SHEMA_SUCCESS = 'MoneyTransfer/GET_INSTANCE_SHEMA_SUCCESS';
const GET_INSTANCE_SHEMA_FAIL = 'MoneyTransfer/GET_INSTANCE_SHEMA_FAIL';


const GET_INSTANCE_COMMISSION = 'MoneyTransfer/GET_INSTANCE_COMMISSION';
const GET_INSTANCE_COMMISSION_SUCCESS = 'MoneyTransfer/GET_INSTANCE_COMMISSION_SUCCESS';
const GET_INSTANCE_COMMISSION_FAIL = 'MoneyTransfer/GET_INSTANCE_COMMISSION_FAIL';


const GET_INSTANCE_ROLE = 'MoneyTransfer/GET_INSTANCE_ROLE';
const GET_INSTANCE_ROLE_SUCCESS = 'MoneyTransfer/GET_INSTANCE_ROLE_SUCCESS';
const GET_INSTANCE_ROLE_FAIL = 'MoneyTransfer/GET_INSTANCE_ROLE_FAIL';


const GET_INSTANCE_PARTENAIRE = 'MoneyTransfer/GET_INSTANCE_PARTENAIRE';
const GET_INSTANCE_PARTENAIRE_SUCCESS = 'MoneyTransfer/GET_INSTANCE_PARTENAIRE_SUCCESS';
const GET_PARTENAIRE_FAIL = 'MoneyTransfer/GET_PARTENAIRE_FAIL';


const GET_INSTANCE_CAISSE = 'MoneyTransfer/GET_INSTANCE_CAISSE';
const GET_INSTANCE_CAISSE_SUCCESS = 'MoneyTransfer/GET_INSTANCE_CAISSE_SUCCESS';
const GET_INSTANCE_CAISSE_FAIL = 'MoneyTransfer/GET_INSTANCE_CAISSE_FAIL';

const LOAD_LIST_REGION = 'Parametrage/LOAD_LIST_REGION';
const LOAD_LIST_REGION_SUCCESS = 'Parametrage/LOAD_LIST_REGION_SUCCESS';
const LOAD_LIST_REGION_FAIL = 'Parametrage/LOAD_LIST_REGION_FAIL';

const LOAD_LIST_COLLABORATEUR = 'Parametrage/LOAD_LIST_COLLABORATEUR';
const LOAD_LIST_COLLABORATEUR_SUCCESS = 'Parametrage/LOAD_LIST_COLLABORATEUR_SUCCESS';
const LOAD_LIST_COLLABORATEUR_FAIL = 'Parametrage/LOAD_LIST_COLLABORATEUR_FAIL';

const LOAD_LIST_CHEF_REGIONALE = 'Parametrage/LOAD_LIST_CHEF_REGIONALE';
const LOAD_LIST_CHEF_REGIONALE_SUCCESS = 'Parametrage/LOAD_LIST_CHEF_REGIONALE_SUCCESS';
const LOAD_LIST_CHEF_REGIONALE_FAIL = 'Parametrage/LOAD_LIST_CHEF_REGIONALE_FAIL';


const LOAD_LIST_CHEF = 'Parametrage/LOAD_LIST_CHEF';
const LOAD_LIST_CHEF_SUCCESS = 'Parametrage/LOAD_LIST_CHEF_SUCCESS';
const LOAD_LIST_CHEF_FAIL = 'Parametrage/LOAD_LIST_CHEF_FAIL';


const LOAD_LIST_AGENCE = 'Parametrage/LOAD_LIST_AGENCE';
const LOAD_LIST_AGENCE_SUCCESS = 'Parametrage/LOAD_LIST_AGENCE_SUCCESS';
const LOAD_LIST_AGENCE_FAIL = 'Parametrage/LOAD_LIST_AGENCE_FAIL';

const LOAD_LIST_CAISSE = 'Parametrage/LOAD_LIST_CAISSE';
const LOAD_LIST_CAISSE_SUCCESS = 'Parametrage/LOAD_LIST_CAISSE_SUCCESS';
const LOAD_LIST_CAISSE_FAIL = 'Parametrage/LOAD_LIST_CAISSE_FAIL';

const LOAD_LIST_SHEMA = 'Parametrage/LOAD_LIST_SHEMA';
const LOAD_LIST_SHEMA_SUCCESS = 'Parametrage/LOAD_LIST_SHEMA_SUCCESS';
const LOAD_LIST_SHEMA_FAIL = 'Parametrage/LOAD_LIST_SHEMA_FAIL';


const LOAD_LIST_TYPE = 'Parametrage/LOAD_LIST_TYPE';
const LOAD_LIST_TYPE_SUCCESS = 'Parametrage/LOAD_LIST_TYPE_SUCCESS';
const LOAD_LIST_TYPE_FAIL = 'Parametrage/LOAD_LIST_TYPE_FAIL';


const LOAD_LIST_COMMISSION = 'Parametrage/LOAD_LIST_COMMISSION';
const LOAD_LIST_COMMISSION_SUCCESS = 'Parametrage/LOAD_LIST_COMMISSION_SUCCESS';
const LOAD_LIST_COMMISSION_FAIL = 'Parametrage/LOAD_LIST_COMMISSION_FAIL';

const LOAD_LIST_ROLES = 'Parametrage/LOAD_LIST_ROLES';
const LOAD_LIST_ROLES_SUCCESS = 'Parametrage/LOAD_LIST_ROLES_SUCCESS';
const LOAD_LIST_ROLES_FAIL = 'Parametrage/LOAD_LIST_ROLES_FAIL';

const LOAD_LIST_PARTENAIRES = 'Parametrage/LOAD_LIST_PARTENAIRES';
const LOAD_LIST_PARTENAIRES_SUCCESS = 'Parametrage/LOAD_LIST_PARTENAIRES_SUCCESS';
const LOAD_LIST_PARTENAIRES_FAIL = 'Parametrage/LOAD_LIST_PARTENAIRES_FAIL';


const SUCCESS = 'Parametrage/SUCCESS';
const SWITCHF = 'Parametrage/SWITCHF';
const DATEMIN ='Parametrage/DATEMIN';

const RESET = 'Parametrage/RESET';

const initialState = {
    loaded: false,
    step: 'stepOne',
    statut: '',
    dateMinfrai:'',
    activeAllTabs: true,
    codeErreur: '',
    success: true,
    saveSuccessObject: {
        saveSuccess: false,
        id: ''

    },
    instance: '',
    dataForDetailvillef: {
        "libelle": "",
        "pays": {
            "libelle": "",
        },
    },
    datapays: {
        "libelle": "",
        "abreviation": "",
        "indicatif": "",
    },
    dataRegion: {
        "libelle": "",
        "nomcollaborateur": "",
        "codeRegion": "",
        "nomPays": "",
        "idPays": ""
    },
    dataAgence: {
        "nomAgence": "",
        "codeAgence": "",
        "telAgence": "",
        "libelleRegion": "",
        "nomcollaborateur": "",
        "idcollaborateur": "",
        "idregion": ""
    },
    dataCaisse: {
        "numeroCaisse": "",
        "nomAgence": "",
    },
    dataCodepostal: {
        "libelle": "",
    },
    datashema: {
        "libelle": "",
        "empMontant": "",
        "empDate": "",
        "empCompte": "",
        "sens": "",
        "idtypeOperation": "",
        "typeOperation": "",
    },
    dataFrais: {
        "libelle": "",
    },

    dataUser: {
        "firstName": "",
        "lastName": "",
        "userName": "",
        "password": "",
        "idAgence": "",
        "nomAgence": "",
        "admin": false,
        "collaborateur": false,
        "superAdmin": false,
        "chefRegion": false,
        "chefAgence": false,
        "montantremisemax": "",
        "tauxremise": "",
        "id": ""
    }


}
export default function reducer(state = initialState, action = {}) {
    switch (action.type) {


        case SUCCESS:
            return {
                ...state,
                loading: false,
                successinstance: false,
                error: null
            };
        case DATEMIN :
        return{

              dateMinfrai:action.date,
        }

        case RESET:
            return {
                ...state,
                successsupdate:null,
            };


            case SWITCHF:
            return {
                ...state,
                successinstanceagence: false,
                successinstanceville: false,
                successinstanceregion: false,
                successinstance: false,
                successinstancecaisse: false,
                successinstancecode: false,
                successinstanceshema: false,
                successinstancecommission: false,
                successinstancerole: false,
                dataForDetailville: initialState.dataForDetailvillef,
                dataForDetail: initialState.datapays,
                dataForDetailregion: initialState.dataRegion,
                dataForDetailagence: initialState.dataAgence,
                dataForDetailcaisse: initialState.dataCaisse,
                dataForDetailcode: initialState.dataCodepostal,
                dataForDetailshema: initialState.datashema,
                dataForDetailcommission: initialState.dataFrais,
                dataForDetailrole: initialState.dataUser,
                dateForDetailPartenaire: initialState.dataUser,
                error: null
            };
//SAVE_CLIENTEXITE

        case SAVE_PAYS:
            return {
                ...state,
                loadingsave: true
            };
        case SAVE_PAYS_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_PAYS_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
//SAVE VILLES
        case SAVE_VILLES:
            return {
                ...state,
                loadingsave: true
            };
        case SAVE_VILLES_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_VILLES_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

        //SAVE FRAI

        case SAVE_FRAIS:
            return {
                ...state,
                loadingsave: true
            };
        case SAVE_FRAIS_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    //successfrais : null,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    successfrais: action.result.success,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    successfrais: action.result.success,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_FRAIS_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

// save code

        case SAVE_CODE:
            return {
                ...state,
                loadingsave: true
            };
        case SAVE_CODE_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_CODE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //SAVE REGION


        case SAVE_REGION:
            return {
                ...state,
                loadingsave: true
            };
        case SAVE_REGION_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_REGION_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        // SAVE AGENCE


        case SAVE_AGENCE:
            return {
                ...state,
                loadingsave: true
            };
        case SAVE_AGENCE_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_AGENCE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //SAVE SHEMA
        case SAVE_SHEMA:
            return {
                ...state,
                loadingsave: true
            };
        case SAVE_SHEMA_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_SHEMA_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //SAVE Partenaire  types: [SAVE_PARTENAIRE, SAVE_PARTENAIRE_SUCCESS, SAVE_PARTENAIRE_FAIL],

        case SAVE_PARTENAIRE:
            return {
                ...state,
                loadingsave: true
            };
        case SAVE_PARTENAIRE_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_PARTENAIRE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };


        case SAVE_USER:
            return {
                ...state,
                loadingsave: true
            };
        case SAVE_USER_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_USER_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };


        //SAVE CAISSE


        case SAVE_CAISSE:
            return {
                ...state,
                loadingsave: true
            };
        case SAVE_CAISSE_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_CAISSE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

//UPDTAE


        case UPDATE_PAYS:
            return {
                ...state,
                loadingsave: true
            };
        case UPDATE_PAYS_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,
                    updateSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case UPDATE_PAYS_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //UPDTAE  VILLE


        case UPDATE_VILLES:
            return {
                ...state,
                loadingsave: true
            };
        case UPDATE_VILLES_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    updateSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case UPDATE_VILLES_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };


// update code

        case UPDATE_CODE:
            return {
                ...state,
                loadingsave: true
            };
        case UPDATE_CODE_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    updateSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case UPDATE_CODE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

        //updtae user

        case UPDATE_USER:
            return {
                ...state,
                loadingsave: true
            };
        case UPDATE_USER_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    successsupdate: action.result.success,
                    moneyTransferObject: action.moneyTransferObject,
                    updateSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case UPDATE_USER_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //UPDATE Partenaire

          case UPDATE_PARTENAIRE:
              return {
                  ...state,
                  loadingsave: true
              };
          case UPDATE_PARTENAIRE_SUCCESS:

              if (action.result.success) {


                  return {
                      ...state,
                      validatepwdError: false,
                      loadingsave: false,
                      loaded: true,
                      step: 'stepTwo',
                      isInstance: false,
                      statut: 'Enregistre',
                      successsupdate: action.result.success,
                      moneyTransferObject: action.moneyTransferObject,
                      updateSuccessObject: {
                          id: action.result.id,
                          urlred: action.result.urlred,
                          referenceDemande: action.result.referenceDemande,
                          saveSuccess: false,
                          version: action.result.version,
                          CONTEXTPATH: action.result.CONTEXTPATH,
                      },
                      error: null
                  };
              } else {
                  return {
                      ...state,
                      validatepwdError: true,
                      loading: false,
                      loaded: true,
                      codeErreur: action.result.codeErreur,
                  };
              }
          case UPDATE_PARTENAIRE_FAIL:
              return {
                  ...state,
                  loadingsaveexiste: false,
                  loaded: false,
                  success: null,
                  step: 'stepTwo',
                  error: action.error
              };

        //Update region

        case UPDATE_REGION:
            return {
                ...state,
                loadingsave: true
            };
        case UPDATE_REGION_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    updateSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case UPDATE_REGION_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

        //UPDATE AGENCE
        case UPDATE_AGENCE:
            return {
                ...state,
                loadingsave: true
            };
        case UPDATE_AGENCE_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',

                    moneyTransferObject: action.moneyTransferObject,
                    updateSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };

            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case UPDATE_AGENCE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

        //UPDATE CAISSE

        case UPDATE_CAISSE:
            return {
                ...state,
                loadingsave: true
            };
        case UPDATE_CAISSE_SUCCESS:
            if (action.result.success) {
                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    updateSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case UPDATE_CAISSE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //UPDATE SHEMA
        case UPDATE_SHEMA:
            return {
                ...state,
                loadingsave: true
            };
        case UPDATE_SHEMA_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    updateSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case UPDATE_SHEMA_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

        //UPDATE FRAIS
        case UPDATE_FRAIS:
            return {
                ...state,
                loadingsave: true
            };
        case UPDATE_FRAIS_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    updateSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case UPDATE_FRAIS_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        // GET_INSTANCE
        case GET_INSTANCE:
            return {
                ...state,
                loading: true,
                successinstance: false
            };
        case GET_INSTANCE_SUCCESS:
            const dataForDetail = action.result.data;
            return {
                ...state,
                loading: false,
                loaded: true,
                successinstance: true,
                dataForDetail,
                view: 'details',
                error: null
            };
        case GET_INSTANCE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successinstance: false,
                dataForDetail: null,
                error: action.error
            };
        //montant chevechement
        case CHE_MONTANT_FRAIS:
            return {
                ...state,
                loading: true,

            };
        case CHE_MONTANT_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                chevauchementmontantsucces: action.result.chevauchement,
                intervalexceeded: action.result.intervalexceeded,
                view: 'details',
                error: null
            };
        case CHE_MONTANT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,


                error: action.error
            };
        // chevechement fate

        case CHE_DATE:
            return {
                ...state,
                loading: true,

            };
        case CHE_DATE_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                chevauchementdatesuces: action.result.chevauchement,

                view: 'details',
                error: null
            };
        case CHE_DATE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,

                error: action.error
            };
        //interval expexted

        case CHE_DATE_EXPE:
            return {
                ...state,
                loading: true,

            };
        case CHE_DATE_EXPE_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                intervalexceeded: action.result.intervalexceeded,
                view: 'details',
                error: null
            };
        case CHE_DATE_EXPE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,

                error: action.error
            };

        // GET_INSTANCE_VILLE

        case GET_INSTANCE_VILLES:
            return {
                ...state,
                loading: true,
                successinstanceville: false
            };
        case GET_INSTANCE_VILLES_SUCCESS:
            const dataForDetailville = action.result.data;
            return {
                ...state,
                loading: false,
                loaded: true,
                successinstanceville: true,
                dataForDetailville,
                view: 'details',
                error: null
            };
        case GET_INSTANCE_VILLES_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successinstanceville: false,
                dataForDetailville: null,
                error: action.error
            };

        // instance code postal

        case GET_INSTANCE_CODE:
            return {
                ...state,
                loading: true,
                successinstancecode: false
            };
        case GET_INSTANCE_CODE_SUCCESS:
            const dataForDetailcode = action.result.data;
            return {
                ...state,
                loading: false,
                loaded: true,
                successinstancecode: true,
                dataForDetailcode,
                view: 'details',
                error: null
            };
        case GET_INSTANCE_CODE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successinstancecode: false,
                dataForDetailcode: null,
                error: action.error
            };
        //iNSTANCE AGENCE
        case GET_INSTANCE_AGENCE:
            return {
                ...state,
                loading: true,
                successinstanceagence: false
            };
        case GET_INSTANCE_AGENCE_SUCCESS:
            const dataForDetailagence = action.result.data;
            return {
                ...state,
                loading: false,
                loaded: true,
                successinstanceagence: true,
                dataForDetailagence,
                view: 'details',
                error: null
            };
        case GET_INSTANCE_AGENCE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successinstanceagence: false,
                dataForDetailagence: null,
                error: action.error
            };
        //INSTANCE SHEMA
        case GET_INSTANCE_SHEMA:
            return {
                ...state,
                loading: true,
                successinstanceshema: false
            };
        case GET_INSTANCE_SHEMA_SUCCESS:
            const dataForDetailshema = action.result.data;
            return {
                ...state,
                loading: false,
                loaded: true,
                successinstanceshema: true,
                dataForDetailshema,
                view: 'details',
                error: null
            };
        case GET_INSTANCE_SHEMA_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successinstanceshema: false,
                dataForDetailshema: null,
                error: action.error
            };
        //INSTANCE COMMISSION
        case GET_INSTANCE_COMMISSION:
            return {
                ...state,
                loading: true,
                successinstancecommission: false
            };
        case GET_INSTANCE_COMMISSION_SUCCESS:
            const dataForDetailcommission = action.result.data;
            return {
                ...state,
                loading: false,
                loaded: true,
                successinstancecommission: true,
                dataForDetailcommission,
                view: 'details',
                error: null
            };
        case GET_INSTANCE_COMMISSION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successinstancecommission: false,
                dataForDetailcommission: null,
                error: action.error
            };
        //INSTANCE ROLE
        case GET_INSTANCE_ROLE:
            return {
                ...state,
                loading: true,
                successinstancerole: false
            };
        case GET_INSTANCE_ROLE_SUCCESS:
            const dataForDetailrole = action.result.data;
            const roles = action.result.data.roles;
            return {
                ...state,
                loading: false,
                loaded: true,
                successinstancerole: true,
                dataForDetailrole,
                roles,
                view: 'details',
                error: null
            };
        case GET_INSTANCE_ROLE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successinstancerole: false,
                dataForDetailrole: null,
                roles: null,
                error: action.error
            };
        //INSTNACE_PARTENAIRE

        case GET_INSTANCE_PARTENAIRE:
            return {
                ...state,
                loading: true,
                successinstancerole: false
            };
        case GET_INSTANCE_PARTENAIRE_SUCCESS:
            const dateForDetailPartenaire = action.result.data;
            const roless = action.result.data.roles;
            return {
                ...state,
                loading: false,
                loaded: true,
                successinstancerole: true,
                dateForDetailPartenaire,
                roless,
                view: 'details',
                error: null
            };
        case GET_PARTENAIRE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successinstancerole: false,
                dateForDetailPartenaire: null,
                roless: null,
                error: action.error
            };
        //INSTANCE CAISSE

        case GET_INSTANCE_CAISSE:
            return {
                ...state,
                loading: true,
                successinstancecaisse: false
            };
        case GET_INSTANCE_CAISSE_SUCCESS:
            const dataForDetailcaisse = action.result.data;
            return {
                ...state,
                loading: false,
                loaded: true,
                successinstancecaisse: true,
                dataForDetailcaisse,
                view: 'details',
                error: null
            };
        case GET_INSTANCE_CAISSE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successinstancecaisse: false,
                dataForDetailcaisse: null,
                error: action.error
            };
        //INSTANCE REGION

        case GET_INSTANCE_REGION:
            return {
                ...state,
                loading: true,
                successinstanceregion: false
            };
        case GET_INSTANCE_REGION_SUCCESS:
            const dataForDetailregion = action.result.data;
            return {
                ...state,
                loading: false,
                loaded: true,
                successinstanceregion: true,
                dataForDetailregion,
                view: 'details',
                error: null
            };
        case GET_INSTANCE_REGION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successinstanceregion: false,
                dataForDetailregion: null,
                error: action.error
            };

//delete pays


        case DELETE_PAYS:
            return {
                ...state,
                loadingsave: true
            };
        case DELETE_PAYS_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case DELETE_PAYS_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //delete VILLES


        case DELETE_VILLES:
            return {
                ...state,
                loadingsave: true
            };
        case DELETE_VILLES_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case DELETE_VILLES_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

        //delete code postal


        case DELETE_CODE:
            return {
                ...state,
                loadingsave: true
            };
        case DELETE_CODE_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case DELETE_CODE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //DELET REGION
        case DELETE_REGION:
            return {
                ...state,
                loadingsave: true
            };
        case DELETE_REGION_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case DELETE_REGION_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

        //DELETE AGENCE

        case DELETE_AGENCE:
            return {
                ...state,
                loadingsave: true
            };
        case DELETE_AGENCE_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case DELETE_AGENCE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //DELETE CAISSE

        case DELETE_CAISSE:
            return {
                ...state,
                loadingsave: true
            };
        case DELETE_CAISSE_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case DELETE_CAISSE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //DELETE SHEMA


        case DELETE_SHEMA:
            return {
                ...state,
                loadingsave: true
            };
        case DELETE_SHEMA_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case DELETE_SHEMA_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };
        //DELETE Partenaire
        case DELETE_PARTENAIRE:
            return {
                ...state,
                loadingsave: true
            };
        case DELETE_PARTENAIRE_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case DELETE_PARTENAIRE_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

        //DELETE USER

        case DELETE_USER:
            return {
                ...state,
                loadingsave: true
            };
        case DELETE_USER_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case DELETE_USER_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };

        //DELET COMMICION


        case DELETE_CO:
            return {
                ...state,
                loadingsave: true
            };
        case DELETE_CO_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsave: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case DELETE_CO_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };


        //LOAD List Agence
        case LOAD_LIST_PAYS:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_PAYS_SUCCESS:
            const listAdresse = action.result.list;
            listAdresse.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                listAdresse,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_PAYS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };
        //LOAD List villes

        case LOAD_LIST_VILLES:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null,
                successfraisenvois : null,

            };
        case LOAD_LIST_VILLES_SUCCESS:
            const lisVilles = action.result.list;
            lisVilles.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });
            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                lisVilles,
                error: null,
                successMiseadispoAgence: null,
            };
        case LOAD_LIST_VILLES_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null,

            };

        //load list code postal
        case LOAD_LIST_CODE:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_CODE_SUCCESS:
            const lisCodes = action.result.list;
            lisCodes.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                lisCodes,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_CODE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };
        // list region


        case LOAD_LIST_REGION:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_REGION_SUCCESS:
            const lisRegions = action.result.list;
            lisRegions.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                lisRegions,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_REGION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };

        //LIST AGENCE
        case LOAD_LIST_AGENCE:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_AGENCE_SUCCESS:
            const lisAgence = action.result.list;
            lisAgence.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                chevauchementdatesuces: false,
                chevauchementmontantsucces: false,
                success: action.result.success,
                lisAgence,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_AGENCE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };

        //LIST CAISSE


        case LOAD_LIST_CAISSE:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_CAISSE_SUCCESS:
            const listCaisse = action.result.list;
            listCaisse.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                chevauchementdatesuces: false,
                chevauchementmontantsucces: false,
                listCaisse,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_CAISSE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };
//LIST SHEMA

        case LOAD_LIST_SHEMA:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_SHEMA_SUCCESS:
            const listShemas = action.result.list;
            listShemas.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                listShemas,
                chevauchementdatesuces: false,
                chevauchementmontantsucces: false,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_SHEMA_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };

        //LIST type


        case LOAD_LIST_TYPE:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_TYPE_SUCCESS:
            const listType = action.result.list;
            listType.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                listType,
                chevauchementdatesuces: false,
                chevauchementmontantsucces: false,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_TYPE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };
        //LIST COMMISSION
        case LOAD_LIST_COMMISSION:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null,

            };
        case LOAD_LIST_COMMISSION_SUCCESS:
            const listCommission = action.result.list;
            listCommission.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                listCommission,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_COMMISSION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };
//LIST PARTENAIRES

case LOAD_LIST_PARTENAIRES:
    return {
        ...state,
        loading: true,
        successMiseadispoAgence: null
    };
case LOAD_LIST_PARTENAIRES_SUCCESS:
    const listPartenaire = action.result.list;
    listPartenaire.forEach(function (element) {
        const demande = element;
        demande.action = 'xxx';
    });

    return {
        ...state,
        loading: false,
        loaded: true,
        success: action.result.success,
        listPartenaire,
        chevauchementdatesuces: false,
        chevauchementmontantsucces: false,
        error: null,
        successMiseadispoAgence: null

    };
case LOAD_LIST_PARTENAIRES_FAIL:
    return {
        ...state,
        loading: false,
        loaded: false,
        success: false,
        msg: action.result.msg,
        error: action.error,
        successMiseadispoAgence: null
    };
//LIST ROLE

        case LOAD_LIST_ROLES:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_ROLES_SUCCESS:
            const listRole = action.result.list;
            listRole.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                listRole,
                chevauchementdatesuces: false,
                chevauchementmontantsucces: false,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_ROLES_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };
//LOADCHEF

        case LOAD_LIST_CHEF:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_CHEF_SUCCESS:
            const lisChefs = action.result.list;
            lisChefs.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                lisChefs,
                chevauchementdatesuces: false,
                chevauchementmontantsucces: false,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_CHEF_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };

//LOADCHEF

        case LOAD_LIST_CHEF_REGIONALE:
            return {
                ...state,
                loading: true,
            };
        case LOAD_LIST_CHEF_REGIONALE_SUCCESS:
            const listChefRegionale = action.result.list;
            listChefRegionale.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                listChefRegionale,
                error: null,
                chevauchementdatesuces: false,
                chevauchementmontantsucces: false,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_CHEF_REGIONALE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                error: action.error,
                successMiseadispoAgence: null
            };
// list collaboateur
        case LOAD_LIST_COLLABORATEUR:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_COLLABORATEUR_SUCCESS:
            const lisCollaborateurs = action.result.list;
            lisCollaborateurs.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                lisCollaborateurs,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_COLLABORATEUR_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                msg: action.result.msg,
                error: action.error,
                chevauchementdatesuces: false,
                chevauchementmontantsucces: false,
                successMiseadispoAgence: null
            };

        default:
            return state;
    }
}


function objectToParams(object) {
    let str = '';
    for (const key in object) {
        if (str !== '') {
            str += '&';
        }
        str += key + '=' + encodeURIComponent(object[key]);
    }
    return str;
}


export function changebutton(success) {
    if (success) {
        return {type: SUCCESS};
    }
    return {type: SUCCESS};
}

export function saveAdreese(values) {


    const paysobject = values;
    paysobject.libelle = values.libelle;
    paysobject.abreviation = values.abreviation;
    paysobject.indicatif = values.indicatif;

    return {
        types: [SAVE_PAYS, SAVE_PAYS_SUCCESS, SAVE_PAYS_FAIL],
        promise: (client) => client.post('ParametragePays/save', {data: objectToParams(paysobject)})


    };
}

export function updatepays(values, id) {


    const paysobject = values;
    paysobject.id = id;
    paysobject.libelle = values.libelle;
    paysobject.abreviation = values.abreviation;
    paysobject.indicatif = values.indicatif;
    return {
        types: [UPDATE_PAYS, UPDATE_PAYS_SUCCESS, UPDATE_PAYS_FAIL],
        promise: (client) => client.post('ParametragePays/updtae', {data: objectToParams(paysobject)})


    };
}

export function loadListPays() {
    return {
        types: [LOAD_LIST_PAYS, LOAD_LIST_PAYS_SUCCESS, LOAD_LIST_PAYS_FAIL],
        promise: (client) => client.get('ParametragePays/Pays')
    };
}


export function deletepays(id) {
    return {
        types: [DELETE_PAYS, DELETE_PAYS_SUCCESS, DELETE_PAYS_FAIL],
        promise: (client) => client.get('ParametragePays/delete/' + id)
    };
}

export function getInstance(id) {
    return {
        types: [GET_INSTANCE, GET_INSTANCE_SUCCESS, GET_INSTANCE_FAIL],
        promise: (client) => client.get('ParametragePays/instance/' + id),
    };
}


export function saveville(values) {


    const villesobject = values;
    villesobject.libelle = values.libelle;
    villesobject.idRegion = values.idRegion;


    return {
        types: [SAVE_VILLES, SAVE_VILLES_SUCCESS, SAVE_VILLES_FAIL],
        promise: (client) => client.post('ParametrageVilles/save', {data: objectToParams(villesobject)})
    };
}


export function updatevilles(values, id) {


    const villesobject = values;
    villesobject.id = id;
    villesobject.libelle = values.libelle;
    villesobject.idRegion = values.idRegion;
    return {
        types: [UPDATE_VILLES, UPDATE_VILLES_SUCCESS, UPDATE_VILLES_FAIL],
        promise: (client) => client.post('ParametrageVilles/updtae', {data: objectToParams(villesobject)})


    };
}

export function loadListVilles() {
    return {
        types: [LOAD_LIST_VILLES, LOAD_LIST_VILLES_SUCCESS, LOAD_LIST_VILLES_FAIL],
        promise: (client) => client.get('ParametrageVilles/Villes')
    };
}


export function deletevilles(id) {
    return {
        types: [DELETE_VILLES, DELETE_VILLES_SUCCESS, DELETE_VILLES_FAIL],
        promise: (client) => client.get('ParametrageVilles/delete/' + id)
    };
}

export function getInstancevilles(id) {
    return {
        types: [GET_INSTANCE_VILLES, GET_INSTANCE_VILLES_SUCCESS, GET_INSTANCE_VILLES_FAIL],
        promise: (client) => client.get('ParametrageVilles/instance/' + id),
    };
}


export function savecodepostal(values) {


    const villesobject = values;
    villesobject.libelle = values.libelle;
    villesobject.idvilles = values.libellevilles;

    return {
        types: [SAVE_CODE, SAVE_CODE_SUCCESS, SAVE_CODE_FAIL],
        promise: (client) => client.post('ParametrageCodesPostaux/save', {data: objectToParams(villesobject)})


    };
}


export function updateodepostal(values, idvilles, id) {


    const villesobject = values;
    villesobject.id = id;
    villesobject.idvilles = idvilles;
    villesobject.libelle = values.libelle;
    return {
        types: [UPDATE_CODE, UPDATE_CODE_SUCCESS, UPDATE_CODE_FAIL],
        promise: (client) => client.post('ParametrageCodesPostaux/updtae', {data: objectToParams(villesobject)})


    };
}


export function loadListcodepostal() {
    return {
        types: [LOAD_LIST_CODE, LOAD_LIST_CODE_SUCCESS, LOAD_LIST_CODE_FAIL],
        promise: (client) => client.get('ParametrageCodesPostaux/CodesPostaux')
    };
}


export function deleteodepostal(id) {
    return {
        types: [DELETE_CODE, DELETE_CODE_SUCCESS, DELETE_CODE_FAIL],
        promise: (client) => client.get('ParametrageCodesPostaux/delete/' + id)
    };
}

export function getInstanceodepostal(id) {
    return {
        types: [GET_INSTANCE_CODE, GET_INSTANCE_CODE_SUCCESS, GET_INSTANCE_CODE_FAIL],
        promise: (client) => client.get('ParametrageCodesPostaux/instance/' + id),
    };
}


export function loadListRegion() {
    return {
        types: [LOAD_LIST_REGION, LOAD_LIST_REGION_SUCCESS, LOAD_LIST_REGION_FAIL],
        promise: (client) => client.get('ParametrageRegions/Regions')
    };
}

export function deleteRegion(id) {
    return {
        types: [DELETE_REGION, DELETE_REGION_SUCCESS, DELETE_REGION_FAIL],
        promise: (client) => client.get('ParametrageRegions/delete/' + id)
    };
}

export function saveRegion(values) {


    const regionsobject = values;
    regionsobject.libelle = values.libelle;
    regionsobject.codeRegion = values.codeRegion;
    regionsobject.idcollaborateur = values.nomcollaborateur;
    regionsobject.idPays = values.pays;
    return {
        types: [SAVE_REGION, SAVE_REGION_SUCCESS, SAVE_REGION_FAIL],
        promise: (client) => client.post('ParametrageRegions/save', {data: objectToParams(regionsobject)})


    };
}

export function updateRegion(values, id) {


    const regionsobject = values;
    regionsobject.id = id;
    regionsobject.idcollaborateur = values.idcollaborateur;
    regionsobject.libelle = values.libelle;
    regionsobject.codeRegion = values.codeRegion;
    regionsobject.idPays = values.idPays;

    return {
        types: [UPDATE_REGION, UPDATE_REGION_SUCCESS, UPDATE_REGION_FAIL],
        promise: (client) => client.post('ParametrageRegions/updtae', {data: objectToParams(regionsobject)})


    };
}

export function getInstanregion(id) {
    return {
        types: [GET_INSTANCE_REGION, GET_INSTANCE_REGION_SUCCESS, GET_INSTANCE_REGION_FAIL],
        promise: (client) => client.get('ParametrageRegions/instance/' + id),
    };
}


export function loadListCollaborateur() {
    return {
        types: [LOAD_LIST_COLLABORATEUR, LOAD_LIST_COLLABORATEUR_SUCCESS, LOAD_LIST_COLLABORATEUR_FAIL],
        promise: (client) => client.get('Collaborateur/Collaborateurs')
    };
}

export function loadListchefRegionale() {
    return {
        types: [LOAD_LIST_CHEF_REGIONALE, LOAD_LIST_CHEF_REGIONALE_SUCCESS, LOAD_LIST_CHEF_REGIONALE_FAIL],
        promise: (client) => client.get('Collaborateur/chefRegionale')
    };
}

export function loadListChefAgence() {
    return {
        types: [LOAD_LIST_CHEF, LOAD_LIST_CHEF_SUCCESS, LOAD_LIST_CHEF_FAIL],
        promise: (client) => client.get('Collaborateur/chefagence')
    };
}

export function loadListAgence() {
    return {
        types: [LOAD_LIST_AGENCE, LOAD_LIST_AGENCE_SUCCESS, LOAD_LIST_AGENCE_FAIL],
        promise: (client) => client.get('ParametrageAgences/Agences')
    };
}

export function getInstanragence(id) {
    return {
        types: [GET_INSTANCE_AGENCE, GET_INSTANCE_AGENCE_SUCCESS, GET_INSTANCE_AGENCE_FAIL],
        promise: (client) => client.get('ParametrageAgences/instance/' + id),
    };
}


export function saveAgence(values) {


    const agencesobject = values;

    agencesobject.nomAgence = values.nomAgence;
    agencesobject.codeAgence = values.codeAgence;
    agencesobject.telAgence = values.telAgence;
    agencesobject.idVille = values.idVille;
    agencesobject.idcollaborateur = values.idcollaborateur;
    agencesobject.adresse = values.adresse;

    return {
        types: [SAVE_AGENCE, SAVE_AGENCE_SUCCESS, SAVE_AGENCE_FAIL],
        promise: (client) => client.post('ParametrageAgences/save', {data: objectToParams(agencesobject)})


    };
}

export function updateAgence(values, id) {


    const agencesobject = values;
    agencesobject.id = id;
    agencesobject.nomAgence = values.nomAgence;
    agencesobject.codeAgence = values.codeAgence;
    agencesobject.telAgence = values.telAgence;
    agencesobject.idVille = values.idVille;
    agencesobject.idcollaborateur = values.idcollaborateur;
    agencesobject.adresse = values.adresse;
    return {
        types: [UPDATE_AGENCE, UPDATE_AGENCE_SUCCESS, UPDATE_AGENCE_FAIL],
        promise: (client) => client.post('ParametrageAgences/updtae', {data: objectToParams(agencesobject)})


    };
}

export function deleteagences(id) {
    return {
        types: [DELETE_AGENCE, DELETE_AGENCE_SUCCESS, DELETE_AGENCE_FAIL],
        promise: (client) => client.get('ParametrageAgences/delete/' + id)
    };
}

export function loadListCaisse() {
    return {
        types: [LOAD_LIST_CAISSE, LOAD_LIST_CAISSE_SUCCESS, LOAD_LIST_CAISSE_FAIL],
        promise: (client) => client.get('ParametrageCaisse/Caisses')
    };
}


export function getInstancecaisse(id) {
    return {
        types: [GET_INSTANCE_CAISSE, GET_INSTANCE_CAISSE_SUCCESS, GET_INSTANCE_CAISSE_FAIL],
        promise: (client) => client.get('ParametrageCaisse/instance/' + id),
    };
}

export function deletecaisse(id) {
    return {
        types: [DELETE_CAISSE, DELETE_CAISSE_SUCCESS, DELETE_CAISSE_FAIL],
        promise: (client) => client.get('ParametrageCaisse/delete/' + id)
    };
}

export function saveCaisse(values) {


    const caisseobject = values;
    caisseobject.numeroCaisse = values.numeroCaisse;
    caisseobject.idagence = values.nomAgence;

    return {
        types: [SAVE_CAISSE, SAVE_CAISSE_SUCCESS, SAVE_CAISSE_FAIL],
        promise: (client) => client.post('ParametrageCaisse/save', {data: objectToParams(caisseobject)})


    };
}

export function updateCaisse(values, id, idagence) {
    const caisseobject = values;
    caisseobject.id = id;
    caisseobject.numeroCaisse = values.numeroCaisse;
    caisseobject.idagence = idagence;
    return {
        types: [UPDATE_CAISSE, UPDATE_CAISSE_SUCCESS, UPDATE_CAISSE_FAIL],
        promise: (client) => client.post('ParametrageCaisse/updtae', {data: objectToParams(caisseobject)})


    };
}

export function loadListShemaComptable() {
    return {
        types: [LOAD_LIST_SHEMA, LOAD_LIST_SHEMA_SUCCESS, LOAD_LIST_SHEMA_FAIL],
        promise: (client) => client.get('ParametrageShemaComptable/Shema')
    };
}

export function getInstanceShema(id) {
    return {
        types: [GET_INSTANCE_SHEMA, GET_INSTANCE_SHEMA_SUCCESS, GET_INSTANCE_SHEMA_FAIL],
        promise: (client) => client.get('ParametrageShemaComptable/instance/' + id),
    };
}

export function saveShema(values) {


    const shemasobject = values;

    shemasobject.libelle = values.libelle;
    shemasobject.empMontant = values.empMontant;
    shemasobject.empDate = values.empDate;
    shemasobject.empCompte = values.empCompte;
    shemasobject.sens = values.sens;
    shemasobject.idtypeOperation = values.typeOperation;

    return {
        types: [SAVE_SHEMA, SAVE_SHEMA_SUCCESS, SAVE_SHEMA_FAIL],
        promise: (client) => client.post('ParametrageShemaComptable/save', {data: objectToParams(shemasobject)})


    };
}

export function updateShema(values, id, idtypeOperation) {


    const shemaobject = values;
    shemaobject.id = id;
    shemaobject.libelle = values.libelle;
    shemaobject.empMontant = values.empMontant;
    shemaobject.empDate = values.empDate;
    shemaobject.empCompte = values.empCompte;
    shemaobject.idtypeOperation = values.typeOperation;
    return {
        types: [UPDATE_SHEMA, UPDATE_SHEMA_SUCCESS, UPDATE_SHEMA_FAIL],
        promise: (client) => client.post('ParametrageShemaComptable/updtae', {data: objectToParams(shemaobject)})


    };
}

export function loadListtypeoperation() {
    return {
        types: [LOAD_LIST_TYPE, LOAD_LIST_TYPE_SUCCESS, LOAD_LIST_TYPE_FAIL],
        promise: (client) => client.get('ParametrageShemaComptable/typeoperation')
    };
}

export function deleteshema(id) {
    return {
        types: [DELETE_SHEMA, DELETE_SHEMA_SUCCESS, DELETE_SHEMA_FAIL],
        promise: (client) => client.get('ParametrageShemaComptable/delete/' + id)
    };
}

export function loadListCommission() {
    return {
        types: [LOAD_LIST_COMMISSION, LOAD_LIST_COMMISSION_SUCCESS, LOAD_LIST_COMMISSION_FAIL],
        promise: (client) => client.get('ParametrageCommission/Commission')
    };
}

export function getInstanceCommission(id) {
    return {
        types: [GET_INSTANCE_COMMISSION, GET_INSTANCE_COMMISSION_SUCCESS, GET_INSTANCE_COMMISSION_FAIL],
        promise: (client) => client.get('ParametrageCommission/instance/' + id),
    };
}

export function loadListRoleUser() {
    return {
        types: [LOAD_LIST_ROLES, LOAD_LIST_ROLES_SUCCESS, LOAD_LIST_ROLES_FAIL],
        promise: (client) => client.get('ParametrageRoles/Roles')
    };
}

export function loadListPartenaireUser() {
    return {
        types: [LOAD_LIST_PARTENAIRES, LOAD_LIST_PARTENAIRES_SUCCESS, LOAD_LIST_PARTENAIRES_FAIL],
        promise: (client) => client.get('ParametragePartners/Partners')
    };
}
export function getInstanceRole(id) {
    return {
        types: [GET_INSTANCE_ROLE, GET_INSTANCE_ROLE_SUCCESS, GET_INSTANCE_ROLE_FAIL],
        promise: (client) => client.get('ParametrageRoles/instance/' + id),
    };
}

export function getInstancePartenaire(id) {
    return {
        types: [GET_INSTANCE_PARTENAIRE, GET_INSTANCE_PARTENAIRE_SUCCESS, GET_PARTENAIRE_FAIL],
        promise: (client) => client.get('ParametragePartners/instance/' + id),
    };
}

export function deleteuser(id) {
    return {
        types: [DELETE_USER, DELETE_USER_SUCCESS, DELETE_USER_FAIL],
        promise: (client) => client.get('ParametrageRoles/delete/' + id)
    };
}

export function deletePartenaire(id) {
    return {
        types: [DELETE_PARTENAIRE, DELETE_PARTENAIRE_SUCCESS, DELETE_PARTENAIRE_FAIL],
        promise: (client) => client.get('ParametragePartners/delete/' + id)
    };
}

export function deletcommission(id) {
    return {
        types: [DELETE_CO, DELETE_CO_SUCCESS, DELETE_CO_FAIL],
        promise: (client) => client.get('ParametrageCommission/delete/' + id)
    };
}


export function saveRoleUser(values) {


    const userroleobject = values;

    userroleobject.firstName = values.firstName;
    userroleobject.lastName = values.lastName;
    userroleobject.userName = values.userName;
    userroleobject.password = values.password;
    userroleobject.collaborateur = values.collaborateur;
    userroleobject.admin = values.admin;
    userroleobject.superAdmin = values.superAdmin;
    userroleobject.chefAgence = values.chefAgence;
    userroleobject.chefRegion = values.chefRegion;
    userroleobject.idAgence = values.idAgence;
    userroleobject.montantremisemax = values.montantremisemax;
    userroleobject.tauxremise = values.tauxremise;

    return {
        types: [SAVE_USER, SAVE_USER_SUCCESS, SAVE_USER_FAIL],
        promise: (client) => client.post('ParametrageRoles/save', {data: objectToParams(userroleobject)})


    };
}


export function savePartnaire(values,idAgence) {


    const userroleobject = values;

    userroleobject.firstName = values.firstName;
    userroleobject.lastName = values.lastName;
    userroleobject.userName = values.userName;
    userroleobject.password = values.password;
    userroleobject.partenaire = values.partenaire;
    userroleobject.montantremisemax = values.montantremisemax;
    userroleobject.tauxremise = values.tauxremise;
    userroleobject.email = values.email;
    userroleobject.idAgence = idAgence;
    userroleobject.telephone = values.telephone;
    userroleobject.numeroidentite=values.numeroidentite;

    return {
        types: [SAVE_PARTENAIRE, SAVE_PARTENAIRE_SUCCESS, SAVE_PARTENAIRE_FAIL],
        promise: (client) => client.post('ParametragePartners/save', {data: objectToParams(userroleobject)})


    };
}

export function updateRoleUser(values, id) {


    const userroleobject = values;
    userroleobject.id = id;
    userroleobject.firstName = values.firstName;
    userroleobject.lastName = values.lastName;
    userroleobject.userName = values.userName;
    userroleobject.password = values.password;
    userroleobject.collaborateur = values.collaborateur;
    userroleobject.admin = values.admin;
    userroleobject.superAdmin = values.superAdmin;
    userroleobject.chefAgence = values.chefAgence;
    userroleobject.chefRegion = values.chefRegion;
    userroleobject.idAgence = values.idAgence;
    userroleobject.montantremisemax = values.montantremisemax;
    userroleobject.tauxremise = values.tauxremise;

    return {
        types: [UPDATE_USER, UPDATE_USER_SUCCESS, UPDATE_USER_FAIL],
        promise: (client) => client.post('ParametrageRoles/update', {data: objectToParams(userroleobject)})


    };
}


export function updatePartenaire(values, id,idUser) {
    const userroleobject = values;
    userroleobject.idUser = idUser;
    userroleobject.id = id;
    userroleobject.firstName = values.firstName;
    userroleobject.lastName = values.lastName;
    userroleobject.userName = values.userName;
    userroleobject.password = values.password;
    userroleobject.partenaire = values.partenaire;
    userroleobject.montantremisemax = values.montantremisemax;
    userroleobject.tauxremise = values.tauxremise;
    userroleobject.email = values.email;
    userroleobject.telephone = values.telephone;
    userroleobject.numeroidentite=values.numeroidentite;

    return {
        types: [UPDATE_PARTENAIRE, UPDATE_PARTENAIRE_SUCCESS, UPDATE_PARTENAIRE_FAIL],
        promise: (client) => client.post('ParametragePartners/update', {data: objectToParams(userroleobject)})
  };
}


export function savefrais(values) {


    const fraisobject = values;

    fraisobject.montantMin = values.montantMin;
    fraisobject.montantMax = values.montantMax;
    fraisobject.dateMin = values.dateMin;
    fraisobject.dateMax = values.dateMax;
    fraisobject.typefrai = values.typefrai;
    fraisobject.valeurfrai = values.valeurfrai;

    return {
        types: [SAVE_FRAIS, SAVE_FRAIS_SUCCESS, SAVE_FRAIS_FAIL],
        promise: (client) => client.post('ParametrageCommission/save', {data: objectToParams(fraisobject)})
    };
}

export function updatefrais(values, id) {


    const fraisobject = values;
    fraisobject.id = id;
    fraisobject.montantMin = values.montantMin;
    fraisobject.montantMax = values.montantMax;
    fraisobject.dateMin = values.dateMin;
    fraisobject.dateMax = values.dateMax;
    fraisobject.typefrai = values.typefrai;
    fraisobject.valeurfrai = values.valeurfrai;
    return {
        types: [UPDATE_FRAIS, UPDATE_FRAIS_SUCCESS, UPDATE_FRAIS_FAIL],
        promise: (client) => client.post('ParametrageCommission/update', {data: objectToParams(fraisobject)})
    };
}


export function chevauchementMontant(montantMin, montantMax) {
    return {
        types: [CHE_MONTANT_FRAIS, CHE_MONTANT_SUCCESS, CHE_MONTANT_FAIL],
        promise: (client) => client.get('ParametrageCommission/chevauchementMontant/' + montantMin + '/' + montantMax)
    };
}

export function formatDatePicker(dateToFormat) {
    let day = dateToFormat.getDate();
    let month = dateToFormat.getMonth() + 1;
    const year = dateToFormat.getFullYear();
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }
    const DateDebut = day + '-' + month + '-' + year;
    return DateDebut;
}

export function chevauchementdate(dateMin, dateMax) {
    const DateDebutInitial = new Date(dateMin);
    let DateDebut = formatDatePicker(DateDebutInitial);
    const DateFinInitial = new Date(dateMax);
    let DateFin = formatDatePicker(DateFinInitial);

    return {
        types: [CHE_DATE, CHE_DATE_SUCCESS, CHE_DATE_FAIL],
        promise: (client) => client.get('ParametrageCommission/chevauchementTransfert/' + DateDebut + '/' + DateFin)


    };
}


export function intervaldateexceeded(dateMin, dateMax) {
    const DateDebutInitial = new Date(dateMin);
    let DateDebut = formatDatePicker(DateDebutInitial);
    const DateFinInitial = new Date(dateMax);
    let DateFin = formatDatePicker(DateFinInitial);

    return {
        types: [CHE_DATE_EXPE, CHE_DATE_EXPE_SUCCESS, CHE_DATE_EXPE_FAIL],
        promise: (client) => client.get('ParametrageCommission/chevauchementdate/' + DateDebut + '/' + DateFin)


    };
}

export function switchf() {
    return {type: SWITCHF};
}
export function dateminfrai(date) {
    return {type: DATEMIN,date}
}

export function resetAlert() {
    return {type: RESET};
}

