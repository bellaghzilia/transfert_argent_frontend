import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Label,
  Col,
  Row,
  Button,
  ButtonGroup,
  Alert,
  Modal,
} from "react-bootstrap";
import { connect } from "react-redux";
import { initializeWithKey } from "redux-form";
import Griddle from "griddle-react";
import OtherPager from "../Commons/DesignGrid";
import * as MoneyTransferAction from "./moneytransferreducer";

@connect((state) => ({}), { ...MoneyTransferAction, initializeWithKey })
class ActionComponent extends Component {
  static propTypes = {
    getInstance: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
  };
  render() {
    const { getInstance, showBouttonAnnuler } = this.props;
    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            onClick={() => {
              getInstance(this.props.rowData.id);
              showBouttonAnnuler("hide");
            }}
          >
            <i className="fa fa-eye" />
          </Button>
          {this.props.rowData.statut !== "Annulé" &&
            this.props.rowData.statut !== "Abandonné" && (
              <Button
                bsSize="small"
                bsStyle="danger"
                onClick={() => {
                  getInstance(this.props.rowData.id);
                  showBouttonAnnuler("show");
                }}
              >
                <i className="fa fa-times" />
              </Button>
            )}
        </ButtonGroup>
      </div>
    );
  }
}
class StatusComponent extends Component {
  render() {
    let libelleStatut = "";
    if (this.props.rowData.statut === "Annulé") {
      libelleStatut = <div style={{ color: "#999" }}>{this.props.data}</div>;
    } else if (this.props.rowData.statut === "Enregistré") {
      libelleStatut = <div style={{ color: "#FFC125" }}>{this.props.data}</div>;
    } else if (this.props.rowData.statut === "Abandonné") {
      libelleStatut = <div style={{ color: "red" }}>{this.props.data}</div>;
    } else {
      libelleStatut = <div style={{ color: "#859" }}>{this.props.data}</div>;
    }
    return <div>{libelleStatut}</div>;
  }
}
class compteComponent extends Component {
  render() {
    let libelle = "";
    libelle = <div style={{ color: "#10cfbd" }}>{this.props.data}</div>;
    return <div>{libelle}</div>;
  }
}

export default class ListMoneyTransferGAB extends Component {
  constructor() {
    super();
    this.state = {
      showModal: false,
    };
  }
  render() {
    const {
      listMoneyTransferGAB,
      getInstance,
      dataForDetail,
      view,
      setView,
      showAnnuler,
      annulerDemande,
      isAnnule,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const close = () => {
      this.setState({ showModal: false });
    };
    const gridMetaData = [
      {
        columnName: "identifiantDemande",
        displayName: "Ref",
      },
      {
        columnName: "beanDateExecution",
        displayName: "Date d'execution",
      },
      {
        columnName: "numCompte",
        displayName: "Compte à débiter",
      },
      {
        columnName: "numeroPieceIdentite",
        displayName: "Compte à créditer",
      },
      {
        columnName: "montant",
        displayName: "montant",
      },
      {
        columnName: "statut",
        displayName: "statut",
        customComponent: StatusComponent,
      },
      {
        columnName: "Action",
        displayName: "ACTION",
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];
    return (
      <div>
        {view === "grid" ? (
          <Griddle
            results={listMoneyTransferGAB}
            columnMetadata={gridMetaData}
            useGriddleStyles={false}
            useCustomPagerComponent="true"
            customPagerComponent={OtherPager}
            resultsPerPage={10}
            // onRowClick={onRowClick}
            nextText={<i className="fa fa-chevron-right" />}
            previousText={<i className="fa fa-chevron-left" />}
            tableClassName="table"
            columns={[
              "identifiantDemande",
              "beanDateExecution",
              "numeroPieceIdentite",
              "numeroCompte",
              "montant",
              "statut",
            ]}
          />
        ) : (
          dataForDetail && (
            <div>
              <fieldset>
                <legend>Infos compte</legend>
                <Col xs={12} md={6}>
                  <p>Numéro compte</p>
                  <p></p>
                </Col>
              </fieldset>
              <fieldset>
                <legend>info </legend>
                <Col xs={12} md={2}>
                  <p>Montant</p>
                  <p></p>
                </Col>
                <Col xs={12} md={3}>
                  <p>Motif</p>
                  <p></p>
                </Col>

                <Col xs={12} md={2}>
                  <p>Statut</p>
                  {!isAnnule ? <p></p> : <p>Annulé</p>}
                </Col>
              </fieldset>
              {showAnnuler && (
                <Alert bsStyle="info">
                  <p>
                    <i className="fa fa-exclamation-circle" /> &nbsp;&nbsp;
                    Merci de cliquer sur le bouton 'Annuler' pour annuler la
                    transaction
                  </p>
                </Alert>
              )}
              <Row className="pull-right">
                <Button
                  onClick={() => setView("grid")}
                  className={styles.ButtonStyle}
                >
                  <i className="fa fa-reply" />
                  &nbsp;&nbsp; Retour
                </Button>
                {showAnnuler && (
                  <Button
                    onClick={() => this.setState({ showModal: true })}
                    className={styles.ButtonStyle}
                  >
                    <i className="fa fa-times" />
                    &nbsp;&nbsp; Annuler
                  </Button>
                )}
                <Modal
                  show={this.state.showModal}
                  onHide={close}
                  container={this}
                  aria-labelledby="contained-modal-title"
                >
                  <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title">
                      Confirmation
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    Êtes-vous sûr de vouloir abandonner cette transaction?
                  </Modal.Body>
                  <Modal.Footer>
                    <ButtonGroup className="pull-right" bsSize="small">
                      <Button
                        className={styles.ButtonPasswordStyle}
                        onClick={() => close()}
                      >
                        Non
                      </Button>
                      <Button
                        className={styles.ButtonPasswordStyle}
                        onClick={() => {
                          close();
                          annulerDemande(dataForDetail.id);
                        }}
                      >
                        Oui
                      </Button>
                    </ButtonGroup>
                  </Modal.Footer>
                </Modal>
              </Row>
            </div>
          )
        )}
      </div>
    );
  }
}
