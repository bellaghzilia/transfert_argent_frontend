import React, { Component } from "react";
import PropTypes from "prop-types";

import { Alert, Col, ControlLabel, FormControl, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import moment from "moment";
import * as MoneyTransferAction from "./moneytransferreducer";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import * as UserActions from "../User/UserReducer";
import Button from "react-bootstrap-button-loader";
import { AuthorizedComponent } from "react-router-role-authorization";

var result = sessionStorage.getItem("myData");
@connect(
  (state) => ({
    loadingOpenCaisse: state.user.loadingOpenCaisse,
    userFrontDetails: state.user.userFrontDetails,
    etatCaisse: state.moneyTransfer.etatCaisse,
    listCaisse: state.moneyTransfer.listCaisse,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];
      if (result == null || result == "null") {
        promises.push(
          dispatch(
            MoneyTransferAction.getListCaisseByAgence(
              false,
              getState().user.userFrontDetails.agenceId
            )
          )
        );
      } else {
        promises.push(
          dispatch(MoneyTransferAction.getListCaisseByAgence(false, result))
        );
      }
      return Promise.all(promises);
    },
  },
])
@translate(["MoneyTransfer"], { wait: true })
export default class OuvertureCaisse extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      numCaisse: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(event) {
    this.setState({ numCaisse: event.target.value });
  }

  componentDidMount() {
    this.setState({ numCaisse: this.props.userFrontDetails.caisseNum });
  }

  async handleClick(e) {
    try {
      e.preventDefault();
      await this.props.openCaisse(this.state.numCaisse);
      if (this.props.successOpenCaisse) {
        sessionStorage.setItem("myData1", this.state.numCaisse);
        sessionStorage.setItem("myData2", this.state.numCaisse);
      }
      if (result == null || result == "null") {
        this.props.getListCaisseByAgence(
          false,
          this.props.userFrontDetails.agenceId
        );
      } else {
        this.props.getListCaisseByAgence(false, result);
      }

      this.props.loadUserFrontDetails();
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      userFrontDetails,
      openCaisse,
      successOpenCaisse,
      etatCaisse,
      getListCaisseByAgence,
      listCaisse,
      t,
      loadUserFrontDetails,
      loadingOpenCaisse,
    } = this.props;
    const styles = require("./moneytransfer.scss");

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.titleForm.ouverturecaisse")}</h2>
          </Col>
        </Row>
        <Row>
          {userFrontDetails.etatAgence === "CLOSE" && (
            <Col xs="12" md="12">
              <Alert bsStyle="warning">{t("msg.ouvertureagence")}</Alert>
            </Col>
          )}
          {userFrontDetails.collaborateurFlagSession !== "0" &&
            etatCaisse !== "OPEN" && (
              <Col xs="12" md="12">
                <Alert bsStyle="danger">{t("msg.ouverturecaisse")}</Alert>
              </Col>
            )}
          {successOpenCaisse && etatCaisse === "OPEN" && (
            <Col xs="12" md="12">
              <Alert bsStyle="success">{t("msg.sucessouverturecaisse")}</Alert>
            </Col>
          )}
          {!successOpenCaisse && etatCaisse === "null" && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{t("msg.failedouverturecaisse")}</Alert>
            </Col>
          )}
        </Row>
        <Row className="detailsBloc">
          <Col xs="12" md="3">
            <ControlLabel>{t("form.label.dateaujourdui")}:</ControlLabel>
            <p className="detail-value"> {moment().format("DD-MM-YYYY")}</p>
          </Col>
          <Col xs="12" md="3">
            <ControlLabel>{t("form.label.agence")} :</ControlLabel>
            <p className="detail-value">{userFrontDetails.agenceNom}</p>
          </Col>
          <Col xs="12" md="3">
            <ControlLabel>{t("form.label.agent")} :</ControlLabel>
            <p className="detail-value"> {userFrontDetails.collaborateurNom}</p>
          </Col>
        </Row>
        <Row style={{ padding: "0 10px" }}>
          <form className="formContainer" style={{ padding: "30px 20px 20px" }}>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="2" style={{ paddingTop: "6px" }}>
                <ControlLabel>{t("form.label.numeroCaisse")} : </ControlLabel>
              </Col>
              <Col xs="12" md="7">
                <FormControl
                  componentClass="select"
                  className={styles.datePickerFormControl}
                  placeholder="select"
                  onChange={this.handleChange}
                  disabled={userFrontDetails.collaborateurFlagSession !== "0"}
                >
                  <option value="" hidden>
                    {this.state.numCaisse !== ""
                      ? this.state.numCaisse
                      : t("form.label.numeroCaisse")}
                  </option>

                  {listCaisse &&
                    listCaisse.length !== 0 &&
                    listCaisse.map((caisseObjj) => (
                      <option value={caisseObjj}>{caisseObjj}</option>
                    ))}
                </FormControl>
              </Col>
              <Col xs="12" md="3">
                <Button
                  loading={loadingOpenCaisse}
                  onClick={this.handleClick}
                  disabled={
                    userFrontDetails.collaborateurFlagSession !== "0" ||
                    userFrontDetails.etatAgence === "CLOSE"
                  }
                  bsStyle="primary"
                  style={{ width: "46%", height: "35px" }}
                >
                  <i className="fa fa-folder-open-o" />{" "}
                  {t("form.buttons.ouvrir")}
                </Button>
                <Button
                  bsStyle="primary"
                  style={{ width: "46%", height: "35px" }}
                  onClick={() => {
                    browserHistory.push(baseUrl + "app/TDBAgentCaisse");
                  }}
                >
                  <i className="fa fa-times" /> {t("form.buttons.cancel")}
                </Button>
              </Col>
            </Row>
          </form>
        </Row>
      </div>
    );
  }
}
