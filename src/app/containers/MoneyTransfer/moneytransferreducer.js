const STEP_ONE = 'MoneyTransfer/STEP_ONE';
const STEP_TWO = 'MoneyTransfer/STEP_TWO';
const STEP_THREE = 'MoneyTransfer/STEP_THREE';

const STATUT_ABANDONNE = 'MoneyTransfer/STATUT_ABANDONNE';
const STATUT_INITIAL = 'MoneyTransfer/STATUT_INITIAL';

const LOAD_LIST_MONEYTRANSFER = 'MoneyTransfer/LOAD_LIST_MoneyTransfer';
const LOAD_LIST_MONEYTRANSFER_SUCCESS = 'MoneyTransfer/LOAD_LIST_MoneyTransfer_SUCCESS';
const LOAD_LIST_MONEYTRANSFER_FAIL = 'MoneyTransfer/LOAD_LIST_MoneyTransfer_FAIL';

const LOAD_LIST_MONEYTRANSFERGAB = 'MoneyTransfer/LOAD_LIST_MoneyTransferGAB';
const LOAD_LIST_MONEYTRANSFERGAB_SUCCESS = 'MoneyTransfer/LOAD_LIST_MoneyTransferGAB_SUCCESS';
const LOAD_LIST_MONEYTRANSFERGAB_FAIL = 'MoneyTransfer/LOAD_LIST_MoneyTransferGAB_FAIL';

const LOAD_LIST_BENEFICIARY = 'MoneyTransfer/LOAD_LIST_BENEFICIARY';
const LOAD_LIST_BENEFICIARY_SUCCESS = 'MoneyTransfer/LOAD_LIST_BENEFICIARY_SUCCESS';
const LOAD_LIST_BENEFICIARY_FAIL = 'MoneyTransfer/LOAD_LIST_BENEFICIARY_FAIL';

const LOAD_LIST_DEBITACCOUNT = 'MoneyTransfer/LOAD_LIST_DEBITACCOUNT';
const LOAD_LIST_DEBITACCOUNT_SUCCESS = 'MoneyTransfer/LOAD_LIST_DEBITACCOUNT_SUCCESS';
const LOAD_LIST_DEBITACCOUNT_FAIL = 'MoneyTransfer/LOAD_LIST_DEBITACCOUNT_FAIL';

const LOAD_LIST_BENEFICIARYGAB = 'MoneyTransfer/LOAD_LIST_BENEFICIARYGAB';
const LOAD_LIST_BENEFICIARYGAB_SUCCESS = 'MoneyTransfer/LOAD_LIST_BENEFICIARYGAB_SUCCESS';
const LOAD_LIST_BENEFICIARYGAB_FAIL = 'MoneyTransfer/LOAD_LIST_BENEFICIARYGAB_FAIL';

const LOAD_LIST_DEBITACCOUNTGAB = 'MoneyTransfer/LOAD_LIST_DEBITACCOUNTGAB';
const LOAD_LIST_DEBITACCOUNTGAB_SUCCESS = 'MoneyTransfer/LOAD_LIST_DEBITACCOUNTGAB_SUCCESS';
const LOAD_LIST_DEBITACCOUNTGAB_FAIL = 'MoneyTransfer/LOAD_LIST_DEBITACCOUNTGAB_FAIL';

const VALIDER_STEP = 'MoneyTransfer/VALIDER_STEP';
const VALIDER_STEP_SUCCESS = 'MoneyTransfer/VALIDER_STEP_SUCCESS';
const VALIDER_STEP_FAIL = 'MoneyTransfer/VALIDER_STEP_FAIL';

const SAVE_CLIENT = 'MoneyTransfer/SAVE_CLIENT';
const SAVE_CLIENT_SUCCESS = 'MoneyTransfer/SAVE_CLIENT_SUCCESS';
const SAVE_CLIENT_FAIL = 'MoneyTransfer/SAVE_CLIENT_FAIL';


  const SMS = 'MoneyTransfer/SMS';
  const SMS_SUCCESS = 'MoneyTransfer/SMS_SUCCESS';
  const SMS_FAIL = 'MoneyTransfer/SMS_FAIL';

const UPDATE_TRANSFERT = 'MoneyTransfer/UPDATE_TRANSFERT';
const UPDATE_TRANSFERT_SUCCESS = 'MoneyTransfer/UPDATE_TRANSFERT_SUCCESS';
const UPDATE_TRANSFERT_FAIL = 'MoneyTransfer/UPDATE_TRANSFERT_FAIL';

const PAYER_TRANSFERT = 'MoneyTransfer/PAYER_TRANSFERT';
const PAYER_TRANSFERT_SUCCESS = 'MoneyTransfer/PAYER_TRANSFERT_SUCCESS';
const PAYER_TRANSFERT_FAIL = 'MoneyTransfer/PAYER_TRANSFERT_FAIL';

const OPEN_CAISSE = 'MoneyTransfer/OPEN_CAISSE';
const OPEN_CAISSE_SUCCESS = 'MoneyTransfer/OPEN_CAISSE_SUCCESS';
const OPEN_CAISSE_FAIL = 'MoneyTransfer/OPEN_CAISSE_FAIL';

const SEND = 'MoneyTransfer/SEND';
const SEND_SUCCESS = 'MoneyTransfer/SEND_SUCCESS';
const SEND_FAIL = 'MoneyTransfer/SEND_FAIL';

const SIGNER_MONEYTRANSFER = 'MoneyTransferSIGNER_BENEFICIARY';
const SIGNER_MONEYTRANSFER_SUCCESS = 'MoneyTransfer/SIGNER_BENEFICIARY_SUCCESS';
const SIGNER_MONEYTRANSFER_FAIL = 'MoneyTransfer/SIGNER_BENEFICIARY_FAIL';

const CHANGE_TABS_ETAT = 'MoneyTransfer/CHANGE_TABS_ETAT';

const INITIAL_STATE = 'MoneyTransfer/INITIAL_STATE';

const TRANSFER_STATE = 'MoneyTransfer/TRANSFER_STATE';

const LOAD_INSTANCE_MONEYTRANSFER = 'MoneyTransfer/LOAD_INSTANCE__DEMANDE_LCN';
const LOAD_INSTANCE_MONEYTRANSFER_SUCCESS = 'MoneyTransfer/LOAD_INSTANCE__DEMANDE_LCN_SUCCESS';
const LOAD_INSTANCE_MONEYTRANSFER_FAIL = 'MoneyTransfer/LOAD_INSTANCE__DEMANDE_LCN_FAIL';

const ABANDONNER_MONEYTRANSFER = 'MoneyTransfer/ABANDONNER_DEMANDE_LCN';
const ABANDONNER_MONEYTRANSFER_SUCCESS = 'MoneyTransfer/ABANDONNER_DEMANDE_LCN_SUCCESS';
const ABANDONNER_MONEYTRANSFER_FAIL = 'MoneyTransfer/ABANDONNER_DEMANDE_LCN_FAIL';

const LOAD_PAGE_SHOW = 'MoneyTransfer/LOAD_PAGE_SHOW';
const LOAD_PAGE_SHOW_SUCCESS = 'MoneyTransfer/LOAD_PAGE_SHOW_SUCCESSFUL';
const LOAD_PAGE_SHOW_FAIL = 'MoneyTransfer/LOAD_PAGE_SHOW_FAILURE';

const GET_INSTANCE = 'MoneyTransfer/GET_INSTANCE';
const GET_INSTANCE_SUCCESS = 'MoneyTransfer/GET_INSTANCE_SUCCESS';
const GET_INSTANCE_FAIL = 'MoneyTransfer/GET_INSTANCE_FAIL';


const GET_PDF = 'MoneyTransfer/GET_PDF';
const GET_PDF_SUCCESS = 'MoneyTransfer/GET_PDF_SUCCESS';
const GET_PDF_FAIL = 'MoneyTransfer/GET_PDF_FAIL';


const GET_INSTANCE_COLLABORATEUR = 'MoneyTransfer/GET_INSTANCE_COLLABORATEUR';
const GET_INSTANCE_COLLABORATEUR_SUCCESS = 'MoneyTransfer/GET_INSTANCE_COLLABORATEUR_SUCCESS';
const GET_COLLABORATEUR_INSTANCE_FAIL = 'MoneyTransfer/GET_COLLABORATEUR_INSTANCE_FAIL';


const GET_FRAIS_ENVOIS = 'MoneyTransfer/GET_FRAIS_ENVOIS';
const GET_FRAIS_ENVOIS_SUCCESS = 'MoneyTransfer/GET_FRAIS_ENVOIS_SUCCESS';
const GET_FRAIS_ENVOIS_FAIL = 'MoneyTransfer/GET_FRAIS_ENVOIS_FAIL';

const FIND_TRANSFERT = 'MoneyTransfer/FIND_TRANSFERT';
const FIND_TRANSFERT_SUCCESS = 'MoneyTransfer/FIND_TRANSFERT_SUCCESS';
const FIND_TRANSFERT_FAIL = 'MoneyTransfer/FIND_TRANSFERT_FAIL';

const LOAD_LIST_REFERENCES = 'MoneyTransfer/LOAD_LIST_REFERENCES ';
const LOAD_LIST_REFERENCES_SUCCESS = 'MoneyTransfer/LOAD_LIST_REFERENCES_SUCCESS';
const LOAD_LIST_REFERENCES_FAIL = 'MoneyTransfer/LOAD_LIST_REFERENCES_FAIL';

const MONTANT_REMISE_ENVOIS = 'MoneyTransfer/MONTANT_REMISE_ENVOIS';
const MONTANT_REMISE_SUCCESS = 'MoneyTransfer/MONTANT_REMISE_SUCCESS';
const MONTANT_REMISE_FAIL = 'MoneyTransfer/MONTANT_REMISE_FAIL';


const ANNULER_DEMANDE = 'MoneyTransfer/ANNULER_DEMANDE';
const ANNULER_DEMANDE_SUCCESS = 'MoneyTransfer/ANNULER_DEMANDE_SUCCESS';
const ANNULER_DEMANDE_FAIL = 'MoneyTransfer/ANNULER_DEMANDE_FAIL';


const SUPPRIMER_DEMANDE = 'MoneyTransfer/SUPPRIMER_DEMANDE';
const SUPPRIMER_DEMANDE_SUCCESS = 'MoneyTransfer/SUPPRIMER_DEMANDE_SUCCESS';
const SUPPRIMER_DEMANDE_FAIL = 'MoneyTransfer/SUPPRIMER_DEMANDE_FAIL';


const VIEW_GRID = 'MoneyTransfer/VIEW_GRID';
const VIEW_DETAIL = 'MoneyTransfer/VIEW_DETAIL';

const SHOW_BOUTTON_ANNULER = 'MoneyTransfer/SHOW_BOUTTON_ANNULER';

const SHOW_BOUTTON_SUPPRIMER = 'MoneyTransfer/SHOW_BOUTTON_SUPPRIMER';

const INITPWD_VALID = 'MoneyTransfer/INITPWD_VALID';

const GET_JOURFIRIES = 'MoneyTransfer/GET_JOURFIRIES';
const GET_JOURFIRIES_SUCCESS = 'MoneyTransfer/GET_JOURFIRIES_SUCCESS';
const GET_JOURFIRIES_FAIL = 'MoneyTransfer/GET_JOURFIRIES_FAIL';

const GET_MAXDATE = 'MoneyTransfer/GET_MAXDATE';
const GET_MAXDATE_SUCCESS = 'MoneyTransfer/GET_MAXDATE_SUCCESS';
const GET_MAXDATE_FAIL = 'MoneyTransfer/GET_MAXDATE_FAIL';

const LOAD_STATUTS = 'MoneyTransfer/LOAD_STATUTS';
const LOAD_STATUTS_SUCCESS = 'MoneyTransfer/LOAD_STATUTS_SUCCESS';
const LOAD_STATUTS_FAIL = 'MoneyTransfer/LOAD_STATUTS_FAIL';

const TRANSFER_AGENCE = 'MoneyTransfer/TRANSFER_AGENCE';
const TRANSFER_GAB = 'MoneyTransfer/TRANSFER_GAB';
const TRANSFER_PARTENER = 'MoneyTransfer/TRANSFER_PARTENER';

const LOAD_SERVICE_TRANSFER = 'MoneyTransfer/LOAD_SERVICE_TRANSFER';
const LOAD_SERVICE_TRANSFER_SUCCESS = 'MoneyTransfer/LOAD_SERVICE_TRANSFER_SUCCESS';
const LOAD_SERVICE_TRANSFER_FAIL = 'MoneyTransfer/LOAD_SERVICE_TRANSFER_FAIL';


const OPEN_AGENCE_TRANSFER = 'MoneyTransfer/OPEN_AGENCE_TRANSFER';
const OPEN_AGENCE_TRANSFER_SUCCESS = 'MoneyTransfer/OPEN_AGENCE_TRANSFER_SUCCESS';
const OPEN_AGENCE_TRANSFER_FAIL = 'MoneyTransfer/OPEN_AGENCE_TRANSFER_FAIL';


const CLOSE_CAISSE_TRANSFER = 'MoneyTransfer/CLOSE_CAISSE_TRANSFER';
const CLOSE_CAISSE_TRANSFER_SUCCESS = 'MoneyTransfer/CLOSE_CAISSE_TRANSFER_SUCCESS';
const CLOSE_CAISSE_TRANSFER_FAIL = 'MoneyTransfer/CLOSE_CAISSE_TRANSFER_FAIL';


const ATTRIBUTION_LIST_TRANSFER = 'MoneyTransfer/ATTRIBUTION_LIST_TRANSFER';
const ATTRIBUTION_LIST_TRANSFER_SUCCESS = 'MoneyTransfer/ATTRIBUTION_LIST_TRANSFER_SUCCESS';
const ATTRIBUTION_LIST_TRANSFER_FAIL = 'MoneyTransfer/ATTRIBUTION_LIST_TRANSFER_FAIL';

const VALIDER_ATTRIBUTION_LIST_TRANSFER = 'MoneyTransfer/VALIDER_ATTRIBUTION_LIST_TRANSFER';
const VALIDER_ATTRIBUTION_LIST_TRANSFER_SUCCESS = 'MoneyTransfer/VALIDER_ATTRIBUTION_LIST_TRANSFER_SUCCESS';
const VALIDER_ATTRIBUTION_LIST_TRANSFER_FAIL = 'MoneyTransfer/VALIDER_ATTRIBUTION_LIST_TRANSFER_FAIL';


const REFUSION_ATTRIBUTION_LIST_TRANSFER = 'MoneyTransfer/REFUSION_ATTRIBUTION_LIST_TRANSFER';
const REFUSION_ATTRIBUTION_LIST_TRANSFER_SUCCESS = 'MoneyTransfer/REFUSION_ATTRIBUTION_LIST_TRANSFER_SUCCESS';
const REFUSION_ATTRIBUTION_LIST_TRANSFER_FAIL = 'MoneyTransfer/REFUSION_ATTRIBUTION_LIST_TRANSFER_FAIL';

const MISEADISPO_LIST_TRANSFER = 'MoneyTransfer/MISEADISPO_LIST_TRANSFER';
const MISEADISPO_LIST_TRANSFER_SUCCESS = 'MoneyTransfer/MISEADISPO_LIST_TRANSFER_SUCCESS';
const MISEADISPO_LIST_TRANSFER_FAIL = 'MoneyTransfer/MISEADISPO_LIST_TRANSFER_FAIL';


const ALIMENTATION = 'MoneyTransfer/ALIMENTATION';
const ALIMENTATION_SUCCESS = 'MoneyTransfer/ALIMENTATION_SUCCESS';
const ALIMENTATION_FAIL = 'MoneyTransfer/ALIMENTATION_FAIL';


const RETRAIT = 'MoneyTransfer/RETRAIT';
const RETRAIT_SUCCESS = 'MoneyTransfer/RETRAIT_SUCCESS';
const RETRAIT_FAIL = 'MoneyTransfer/RETRAIT_FAIL';

const VALIDER_MISEADISPO_LIST_TRANSFER = 'MoneyTransfer/VALIDER_MISEADISPO_LIST_TRANSFER';
const VALIDER_MISEADISPO_LIST_TRANSFER_SUCCESS = 'MoneyTransfer/VALIDER_MISEADISPO_LIST_TRANSFER_SUCCESS';
const VALIDER_MISEADISPO_LIST_TRANSFER_FAIL = 'MoneyTransfer/VALIDER_MISEADISPO_LIST_TRANSFER_FAIL';

const REFUSER_MISEADISPO_LIST_TRANSFER = 'MoneyTransfer/REFUSER_MISEADISPO_LIST_TRANSFER';
const REFUSER_MISEADISPO_LIST_TRANSFER_SUCCESS = 'MoneyTransfer/REFUSER_MISEADISPO_LIST_TRANSFER_SUCCESS';
const REFUSER_MISEADISPO_LIST_TRANSFER_FAIL = 'MoneyTransfer/REFUSER_MISEADISPO_LIST_TRANSFER_FAIL';

const MISEADISPO_AGENCE_TRANSFER = 'MoneyTransfer/MISEADISPO_AGENCE_TRANSFER';
const MISEADISPO_AGENCE_TRANSFER_SUCCESS = 'MoneyTransfer/MISEADISPO_AGENCE_TRANSFER_SUCCESS';
const MISEADISPO_AGENCE_TRANSFER_FAIL = 'MoneyTransfer/MISEADISPO_AGENCE_TRANSFER_FAIL';

const ENCAISSEMENT_AGENCE_TRANSFERT = 'MoneyTransfer/ENCAISSEMENT_AGENCE_TRANSFERT';
const ENCAISSEMENT_AGENCE_TRANSFERT_SUCCESS = 'MoneyTransfer/ENCAISSEMENT_AGENCE_TRANSFERT_SUCCESS';
const ENCAISSEMENT_AGENCE_TRANSFERT_FAIL = 'MoneyTransfer/ENCAISSEMENT_AGENCE_TRANSFERT_FAIL';


const LOAD_LIST_AGENCE = 'MoneyTransfer/LOAD_LIST_AGENCE';
const LOAD_LIST_AGENCE_SUCCESS = 'MoneyTransfer/LOAD_LIST_AGENCE_SUCCESS';
const LOAD_LIST_AGENCE_FAIL = 'MoneyTransfer/LOAD_LIST_AGENCE_FAIL';

const LOAD_LIST_MISE_A_DISPO_AGENCE = 'MoneyTransfer/LOAD_LIST_MISE_A_DISPO_AGENCE';
const LOAD_LIST_MISE_A_DISPO_AGENCE_SUCCESS = 'MoneyTransfer/LOAD_LIST_MISE_A_DISPO_AGENCE_SUCCESS';
const LOAD_LIST_MISE_A_DISPO_AGENCE_FAIL = 'MoneyTransfer/LOAD_LIST_MISE_A_DISPO_AGENCE_FAIL';

const LOAD_AGENCE_TRANSFER = 'MoneyTransfer/LOAD_AGENCE_TRANSFER';
const LOAD_AGENCE_TRANSFER_SUCCESS = 'MoneyTransfer/LOAD_AGENCE_TRANSFER_SUCCESS';
const LOAD_AGENCE_TRANSFER_FAIL = 'MoneyTransfer/LOAD_AGENCE_TRANSFER_FAIL';


const LOAD_SEUIL_SOLDE_AGENCE_TRANSFER = 'MoneyTransfer/LOAD_SEUIL_SOLDE_AGENCE_TRANSFER';
const LOAD_SEUIL_SOLDE_AGENCE_TRANSFER_SUCCESS = 'MoneyTransfer/LOAD_SEUIL_SOLDE_AGENCE_TRANSFER_SUCCESS';
const LOAD_SEUIL_SOLDE_AGENCE_TRANSFER_FAIL = 'MoneyTransfer/LOAD_SEUIL_SOLDE_AGENCE_TRANSFER_FAIL';

const LOAD_CAISSE_TRANSFER = 'MoneyTransfer/LOAD_CAISSE_TRANSFER';
const LOAD_CAISSE_TRANSFER_SUCCESS = 'MoneyTransfer/LOAD_CAISSE_TRANSFER_SUCCESS';
const LOAD_CAISSE_TRANSFER_FAIL = 'MoneyTransfer/LOAD_CAISSE_TRANSFER_FAIL';


const LOAD_CAISSE_TRANSFER_PARTENAIRE = 'MoneyTransfer/LOAD_CAISSE_TRANSFER_PARTENAIRE';
const LOAD_CAISSE_TRANSFER_PARTENAIRE_SUCCESS = 'MoneyTransfer/LOAD_CAISSE_TRANSFER_PARTENAIRE_SUCCESS';
const LOAD_CAISSE_TRANSFER_PARTENAIRE_FAIL = 'MoneyTransfer/LOAD_CAISSE_TRANSFER_PARTENAIRE_FAIL';

const LOAD_SEUIL_SOLDE_CAISSE_TRANSFER = 'MoneyTransfer/LOAD_SEUIL_SOLDE_CAISSE_TRANSFER';
const LOAD_SEUIL_SOLDE_CAISSE_TRANSFER_SUCCESS = 'MoneyTransfer/LOAD_SEUIL_SOLDE_CAISSE_TRANSFER_SUCCESS';
const LOAD_SEUIL_SOLDE_CAISSE_TRANSFER_FAIL = 'MoneyTransfer/LOAD_SEUIL_SOLDE_CAISSE_TRANSFER_FAIL';

const DELETE_TRANSFERT_CLT_CLT = 'MoneyTransfer/DELETE_TRANSFERT_CLT_CLT';
const DELETE_TRANSFERT_CLT_CLT_SUCCESS = 'MoneyTransfer/DELETE_TRANSFERT_CLT_CLT_SUCCESS';
const DELETE_TRANSFERT_CLT_CLT_FAIL = 'MoneyTransfer/DELETE_TRANSFERT_CLT_CLT_FAIL';

const CANCEL_TRANSFERT_CLT_CLT = 'MoneyTransfer/CANCEL_TRANSFERT_CLT_CLT';
const CANCEL_TRANSFERT_CLT_CLT_SUCCESS = 'MoneyTransfer/CANCEL_TRANSFERT_CLT_CLT_SUCCESS';
const CANCEL_TRANSFERT_CLT_CLT_FAIL = 'MoneyTransfer/CANCEL_TRANSFERT_CLT_CLT_FAIL';


const SIGNE_TRANSFERT_CLT_CLT = 'MoneyTransfer/SIGNE_TRANSFERT_CLT_CLT';
const SIGNE_TRANSFERT_CLT_CLT_SUCCESS = 'MoneyTransfer/SIGNE_TRANSFERT_CLT_CLT_SUCCESS';
const SIGNE_TRANSFERT_CLT_CLT_FAIL = 'MoneyTransfer/SIGNE_TRANSFERT_CLT_CLT_FAIL';


const SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT = 'MoneyTransfer/SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT';
const SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT_SUCCESS = 'MoneyTransfer/SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT_SUCCESS';
const SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT_FAIL = 'MoneyTransfer/SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT_FAIL';


const SIGNE_CHEFAGENCE_PAIEMENT_CLT_CLT = 'MoneyTransfer/SIGNE_CHEFAGENCE_PAIEMENT_CLT_CLT';
const SIGNE_CHEFAGENCE_PAIEMENT_CLT_CLT_SUCCESS = 'MoneyTransfer/SIGNE_CHEFAGENCE_PAIEMENT_CLT_CLT_SUCCESS';
const SIGNE_CHEFAGENCE_PAIEMNT_CLT_CLT_FAIL = 'MoneyTransfer/SIGNE_CHEFAGENCE_PAIEMNT_CLT_CLT_FAIL';

const LOAD_CAISSE_LIST_TRANSFER = 'MoneyTransfer/LOAD_CAISSE_LIST_TRANSFER';
const LOAD_CAISSE_LIST_TRANSFER_SUCCESS = 'MoneyTransfer/LOAD_CAISSE_LIST_TRANSFER_SUCCESS';
const LOAD_CAISSE_LIST_TRANSFER_FAIL = 'MoneyTransfer/LOAD_CAISSE_LIST_TRANSFER_FAIL';


const LOAD_LIST_CAISSE_TRANSFER = 'MoneyTransfer/LOAD_LIST_CAISSE_TRANSFER';
const LOAD_LIST_CAISSE_TRANSFER_SUCCESS = 'MoneyTransfer/LOAD_LIST_CAISSE_TRANSFER_SUCCESS';
const LOAD_LIST_CAISSE_TRANSFER_FAIL = 'MoneyTransfer/LOAD_LIST_CAISSE_TRANSFER_FAIL';


const LOAD_LIST_TRANSFER = 'MoneyTransfer/LOAD_LIST_TRANSFER';
const LOAD_LIST_TRANSFER_SUCCESS = 'MoneyTransfer/LOAD_LIST_TRANSFER_SUCCESS';
const LOAD_LIST_TRANSFER_FAIL = 'MoneyTransfer/LOAD_LIST_TRANSFER_FAIL';

const VERIFICATION_EXISTENCE_ATTRIBUTION = 'MoneyTransfer/VERIFICATION_EXISTENCE_ATTRIBUTION';
const VERIFICATION_EXISTENCE_ATTRIBUTION_SUCCESS = 'MoneyTransfer/VERIFICATION_EXISTENCE_ATTRIBUTION_SUCCESS';
const VERIFICATION_EXISTENCE_ATTRIBUTION_FAIL = 'MoneyTransfer/VERIFICATION_EXISTENCE_ATTRIBUTION_FAIL';


const LOAD_LIST_TRANSFER_AGNC_AGNC = 'MoneyTransfer/LOAD_LIST_TRANSFER_AGNC_AGNC';
const LOAD_LIST_TRANSFER_AGNC_AGNC_SUCCESS = 'MoneyTransfer/LOAD_LIST_TRANSFER_AGNC_AGNC_SUCCESS';
const LOAD_LIST_TRANSFER_AGNC_AGNC_FAIL = 'MoneyTransfer/LOAD_LIST_TRANSFER_AGNC_AGNC_FAIL';


const LOAD_LIST_COMPTE_COMPTA = 'MoneyTransfer/LOAD_LIST_COMPTE_COMPTA';
const LOAD_LIST_COMPTE_COMPTA_SUCCESS = 'MoneyTransfer/LOAD_LIST_COMPTE_COMPTA_SUCCESS';
const LOAD_LIST_COMPTE_COMPTA_FAIL = 'MoneyTransfer/LOAD_LIST_COMPTE_COMPTA_FAIL';


const LOAD_LIST_OUVERTURE_CLOTURE = 'MoneyTransfer/LOAD_LIST_OUVERTURE_CLOTURE';
const LOAD_LIST_OUVERTURE_CLOTURE_SUCCESS = 'MoneyTransfer/LOAD_LIST_OUVERTURE_CLOTURE_SUCCESS';
const LOAD_LIST_OUVERTURE_CLOTURE_FAIL = 'MoneyTransfer/LOAD_LIST_OUVERTURE_CLOTURE_FAIL';


const FIND_TEL = 'MoneyTransfer/FIND_TEL';
const FIND_TEL_SUCCESS = 'MoneyTransfer/FIND_TEL_SUCCESS';
const FIND_TEL_FAIL = 'MoneyTransfer/FIND_TEL_FAIL';

const FIND_IDENTITE = 'MoneyTransfer/FIND_IDENTITE';
const FIND_IDENTITE_SUCCESS = 'MoneyTransfer/FIND_IDENTITE_SUCCESS';
const FIND_IDENTITE_FAIL = 'MoneyTransfer/FIND_IDENTITE_FAIL';


const FIND_TEL_BEN = 'MoneyTransfer/FIND_TEL_BEN';
const FIND_TEL_BEN_SUCCESS = 'MoneyTransfer/FIND_TEL_BEN_SUCCESS';
const FIND_TEL_BEN_FAIL = 'MoneyTransfer/FIND_TEL_BEN_FAIL';


const FIND_CLIENT = 'MoneyTransfer/FIND_CLIENT';
const FIND_CLIENT_SUCCESS = 'MoneyTransfer/FIND_CLIENT_SUCCESS';
const FIND_CLIENT_FAIL = 'MoneyTransfer/FIND_CLIENT_FAIL';

const FIND_CLIENT_BEN = 'MoneyTransfer/FIND_CLIENT_BEN';
const FIND_CLIENT_BEN_SUCCESS = 'MoneyTransfer/FIND_CLIENT_BEN_SUCCESS';
const FIND_CLIENT_BEN_FAIL = 'MoneyTransfer/FIND_CLIENT_BEN_FAIL';

const LOAD_PAIEMENT_NON_PAYER = 'MoneyTransfer/LOAD_PAIEMENT_NON_PAYER';
const LOAD_PAIEMENT_NON_PAYER_SUCCESS = 'MoneyTransfer/LOAD_PAIEMENT_NON_PAYER_SUCCESS';
const LOAD_PAIEMENT_NON_PAYER_FAIL = 'MoneyTransfer/LOAD_PAIEMENT_NON_PAYER_FAIL';


const FIND_LOAD_REGION = 'MoneyTransfer/FIND_LOAD_REGION';
const FIND_LOAD_REGION_SUCCESS = 'MoneyTransfer/FIND_LOAD_REGION_SUCCESS';
const FIND_LOAD_REGION_FAIL = 'MoneyTransfer/FIND_LOAD_REGION_FAIL';


const FIND_VILLE_BY_REGION = 'MoneyTransfer/FIND_VILLE_BY_REGION';
const FIND_VILLE_BY_REGION_SUCCESS = 'MoneyTransfer/FIND_VILLE_BY_REGION_SUCCESS';
const FIND_VILLE_BY_REGION_FAIL = 'MoneyTransfer/FIND_VILLE_BY_REGION_FAIL';


const LOAD_LIST_TRANSFER_REGION = 'MoneyTransfer/LOAD_LIST_TRANSFER_REGION';
const LOAD_LIST_TRANSFER_REGION_SUCCESS = 'MoneyTransfer/LOAD_LIST_TRANSFER_REGION_SUCCESS';
const LOAD_LIST_TRANSFER_REGION_FAIL = 'MoneyTransfer/LOAD_LIST_TRANSFER_REGION_FAIL';


const LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION = 'MoneyTransfer/LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION';
const LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION_SUCCESS = 'MoneyTransfer/LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION_SUCCESS';
const LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION_FAIL = 'MoneyTransfer/LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION_FAIL';

const LOAD_LIST_OUVERTURE_CLOTURE_REGION = 'MoneyTransfer/LOAD_LIST_OUVERTURE_CLOTURE_REGION';
const LOAD_LIST_OUVERTURE_CLOTURE_REGION_SUCCESS = 'MoneyTransfer/LOAD_LIST_OUVERTURE_CLOTURE_REGION_SUCCESS';
const LOAD_LIST_OUVERTURE_CLOTURE_REGION_FAIL = 'MoneyTransfer/LOAD_LIST_OUVERTURE_CLOTURE_REGION_FAIL';


const LOAD_LIST_AGENCE_BY_REGION = 'MoneyTransfer/LOAD_LIST_AGENCE_BY_REGION';
const LOAD_LIST_AGENCE_BY_REGION_SUCCESS = 'MoneyTransfer/LOAD_LIST_AGENCE_BY_REGION_SUCCESS';
const LOAD_LIST_AGENCE_BY_REGION_FAIL = 'MoneyTransfer/LOAD_LIST_AGENCE_BY_REGION_FAIL';


const LOAD_LIST_PARTENAIRES = 'MoneyTransfer/LOAD_LIST_PARTENAIRES';
const LOAD_LIST_PARTENAIRES_SUCCESS = 'MoneyTransfer/LOAD_LIST_PARTENAIRES_SUCCESS';
const LOAD_LIST_PARTENAIRES_FAIL = 'MoneyTransfer/LOAD_LIST_PARTENAIRES_FAIL';

const LOAD_COMPTE_GARANTIE = 'MoneyTransfer/LOAD_COMPTE_GARANTIE';
const LOAD_COMPTE_GARANTIE_SUCCESS = 'MoneyTransfer/LOAD_COMPTE_GARANTIE_SUCCESS';
const LOAD_COMPTE_GARANTIE_FAIL = 'MoneyTransfer/LOAD_COMPTE_GARANTIE_FAIL';

const RESETREFERENCE = 'MoneyTransfer/RESETREFERENCE';


/**const LOAD_LIST_CAISSE_BY_AGENCE = 'MoneyTransfer/LOAD_LIST_CAISSE_BY_AGENCE';
 const LOAD_LIST_CAISSE_BY_AGENCE_SUCCESS = 'MoneyTransfer/LOAD_LIST_CAISSE_BY_AGENCE_SUCCESS';
 const LOAD_LIST_CAISSE_BY_AGENCE_FAIL = 'MoneyTransfer/LOAD_LIST_CAISSE_BY_AGENCE_FAIL';
 */
const RESET = 'MoneyTransfer/RESET';
const SWITCHF = 'Parametrage/SWITCHF';

const RESETALIMENTATION = 'MoneyTransfer/RESETALIMENTATION';
const RESETVALIDE ='MoneyTransfer/RESETVALIDE';
const RESETSIGNE ='MoneyTransfer/RESETSIGNE';
const TYPETB= 'MoneyTransfer/TYPETB';
const RESETSOLDE= 'MoneyTransfer/RESETSOLDE';
const LOAD_TRANSFER= 'MoneyTransfer/LOAD_TRANSFER';



const initialState = {
    loaded: false,
    step: 'stepOne',
    statut: '',
    activeAllTabs: true,
    codeErreur: '',
    success: true,
    listLcnRequest: true,
    Request: true,
    currentPage: 1,
    view: 'grid',
    isAnnule: false,
    isSupprimer: false,
    validatepwdError: false,
    isInstance: false,
    signRedirect: false,
    saveSuccessObject: {
        saveSuccess: false,
        id: '',
        taskId: '',
        urlred: '',
        version: '',
        CONTEXTPATH: '',
        referenceDemande: ''
    },
    saveUrl: 'demandeTransfertArgent/save',
    updateUrl: 'demandeTransfertArgent/update',
    signeUrl: 'demandeTransfertArgent/signer',
    cancelUrl: 'demandeTransfertArgent/abandonnerPhaseSignature',
    typeOfTransfer: 'agence',
    isSummary: false,
    instance: '',
    dataForDetailpaiement: {
        "id": "",
        "numeroPieceIdentiteemeteur": "",
        "numtelemetteur": "",
        "nomemetteur": "",
        "prenomemeteur": "",
        "emailemetteur": "",
        "villeemetteur": "",
        "regionemeteur": "",
        "paysindicatifemetteur": "",
        "paysemetteur": "",
        "typeTransfert": "",
        "nomEmetteur": "",
        "montantMin": "",
        "nomBenif": "",
        "montantMax":"",
        "numeroPieceIdentitebeneficiare": "",
        "nombeneficiare": "",
        "prenombeneficiare": "",
        "emailbeneficiare": "",
        "villebeneficiare": "",
        "regionbeneficiare": "",
        "paysindicatifbeneficiare": "",
        "paysbeneficiare": "",
        "statutOperation": "",
        "dateOperation": "",
        "montantOperation": "",
        "reference": "",
        "sensOperation": "",
        "statutPaiement": "",
        "motif": "",
        "fraiinclus": "",
        "fraisenvois": "",
        "montantremise": "",
        "montanttransfere": "",
        "motiftransfert": "",
        "agent": "",
        "agence": "",
        "nom": "",
        "prenom": "",
        "mail": "",
        "numeroidentite": "",
        "numerotelebeneficiare": "",
    },

    dataclient: {
        "nombeneficiare": "",
        "prenombeneficiare": "",
        "emailbeneficiare": "",
        "numeroPieceIdentitebeneficiare": "",
        "numtelbeneficiare": "",
        "codepostalbeneficiare": "",
        "adressebeneficiare": "",
        "paysbeneficiare": "",
        "villebeneficiare": ""
    },

    clientDetail: {},
    clientbenDetail: {},
    frais: ""


};
export default function reducer(state = initialState, action = {}) {
    switch (action.type) {


        case SWITCHF:
            return {
                ...state,

                successtransfert: false,
                successclient: false,
                successclientbene: false,
                transfertDetails: initialState.dataForDetailpaiement,
                clientDetails: initialState.clientDetail,
                clientbenDetails: initialState.clientbenDetail,
                fraienvois: initialState.frais,
                dataForDetail: initialState.dataForDetailpaiement,
                successfraisenvois: false,
                successtelben: false,
                successtel: false,

                error: null
            };


             case RESETREFERENCE:
            return {
                ...state,
                successreferencetransfert:null
            };

             case RESETALIMENTATION:
            return {
                ...state,
                successAlimentation:null
            };
            case RESETSIGNE:
            return {
                ...state,
                successSigneTransfert:null
            };

             case RESETSOLDE:
            return {
                ...state,
                soldegarantie:null
            };
             case TYPETB:
            return {
                ...state,
                TYPETB:action.tb
            };
            

            case RESETVALIDE:
            return {
                ...state,
                successAlimentation:null
            };

        // LOAD_JOURFIRIES
        case GET_JOURFIRIES:
            return {
                ...state,
                loading: true
            };
        case GET_JOURFIRIES_SUCCESS:
            const jourfiries = action.result.joursFeries;
            const weekend = action.result.joursWeek;
            return {
                ...state,
                loading: false,
                loaded: true,
                jourfiries,
                weekend,
                success: action.result.success,
                error: null
            };
        case GET_JOURFIRIES_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                jourfiries: null,
                weekend: null,
                error: action.error
            };

        // LOAD_JOURFIRIES
        case GET_MAXDATE:
            return {
                ...state,
                loading: true
            };
        case GET_MAXDATE_SUCCESS:
            let maxdate = action.result.MAX_DATE_EXEC_MODEL;
            const res = maxdate.split('-');
            maxdate = res[2] + '-' + res[1] + '-' + res[0];

            return {
                ...state,
                loading: false,
                loaded: true,
                signRedirect: false,
                maxdate,
                success: action.result.success,
                error: null
            };
        case GET_MAXDATE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                maxdate: null,
                error: action.error
            };

            case LOAD_LIST_PARTENAIRES:
                return {
                    ...state,
                    loading: true,
                    successMiseadispoAgence: null
                };
            case LOAD_LIST_PARTENAIRES_SUCCESS:
                const listPartenaire = action.result.list;
                listPartenaire.forEach(function (element) {
                    const demande = element;
                    demande.action = 'xxx';
                });

                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    success: action.result.success,
                    listPartenaire,
                    successtransfert: true,
                    chevauchementdatesuces: false,
                    chevauchementmontantsucces: false,
                    error: null,
                    successMiseadispoAgence: null

                };
            case LOAD_LIST_PARTENAIRES_FAIL:
                return {
                    ...state,
                    loading: false,
                    loaded: false,
                    success: false,
                    msg: action.result.msg,
                    error: action.error,
                    successMiseadispoAgence: null
                };


                //
                case LOAD_COMPTE_GARANTIE:
                    return {
                        ...state,
                        loading: true,
                        successMiseadispoAgence: null
                    };
                case LOAD_COMPTE_GARANTIE_SUCCESS:
                    const compteGarantie = action.result.data;
                    return {
                        ...state,
                        loading: false,
                        loaded: true,
                        success: action.result.success,
                        compteGarantie,
                        chevauchementdatesuces: false,
                        chevauchementmontantsucces: false,
                        error: null,
                        successMiseadispoAgence: null

                    };
              case LOAD_COMPTE_GARANTIE_FAIL:
                    return {
                        ...state,
                        loading: false,
                        loaded: false,
                        success: false,
                        msg: action.result.msg,
                        error: action.error,
                        successMiseadispoAgence: null
                    };

        // INITPWD_VALID
        case INITPWD_VALID:
            return {
                ...state,
                validatepwdError: false
            };

        // LOAD_LIST_MONEYTRANSFER
        case LOAD_LIST_MONEYTRANSFER:
            return {
                ...state,
                loading: true
            };
        case LOAD_LIST_MONEYTRANSFER_SUCCESS:
            const listMoneyTransfer = action.result.liste;
            const maxPages = action.result.total;
            listMoneyTransfer.forEach(function (element) {
                const demande = element;
                demande.Action = 'xxx';
            });
            return {
                ...state,
                loading: false,
                loaded: true,
                listMoneyTransfer,
                maxPages,
                currentPage: action.index,
                dateDebutRecherche: action.dateDebutRecherche,
                dateFinRecherche: action.dateFinRecherche,
                montantMin: action.montantMin,
                montantMax: action.montantMax,
               
                identifiantDemandeRecheche: action.identifiantDemandeRecheche,
                statutRecherche: action.statutRecherche,
                ncompteRecherche: action.ncompteRecherche,
                typeTransfert: action.typeTransfert,
                success: action.result.success,
                signRedirect: false,
                error: null
            };
        case LOAD_LIST_MONEYTRANSFER_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                listMoneyTransfer: null,
                nbpage: null,
                currentPage: action.index,
                dateDebutRecherche: action.dateDebutRecherche,
                dateFinRecherche: action.dateFinRecherche,
                monta: action.mmin,
                montantMax: action.montantMax,
                identifiantDemandeRecheche: action.identifiantDemandeRecheche,
                statutRecherche: action.statutRecherche,
                ncompteRecherche: action.ncompteRecherche,
                typeTransfert: action.typeTransfert,
                signRedirect: false,
                error: action.error
            };

        // LOAD_LIST_MONEYTRANSFERGAB
        case LOAD_LIST_MONEYTRANSFERGAB:
            return {
                ...state,
                loading: true
            };
        case LOAD_LIST_MONEYTRANSFERGAB_SUCCESS:
            const listMoneyTransferGAB = action.result.liste;
            return {
                ...state,
                loading: false,
                loaded: true,
                listMoneyTransferGAB,
                success: action.result.success,
                error: null
            };
        case LOAD_LIST_MONEYTRANSFERGAB_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                listMoneyTransferGAB: null,
                error: action.error
            };
        // LOAD_LIST_BENEFICIARY
        case LOAD_LIST_BENEFICIARY:
            return {
                ...state,
                loading: true
            };
        case LOAD_LIST_BENEFICIARY_SUCCESS:
            const listBeneficiary = action.result.beneficiaireList;
            return {
                ...state,
                loading: false,
                loaded: true,
                listBeneficiary,
                error: null
            };
        case LOAD_LIST_BENEFICIARY_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                listBeneficiary: null,
                error: action.error
            };

        // LOAD_LIST_DEBITACCOUNT
        case LOAD_LIST_DEBITACCOUNT:
            return {
                ...state,
                loading: true
            };
        case LOAD_LIST_DEBITACCOUNT_SUCCESS:
            const listDebitAccount = action.result.compteInstanceList;
            return {
                ...state,
                loading: false,
                loaded: true,
                listDebitAccount,
                error: null
            };
        case LOAD_LIST_DEBITACCOUNT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                listDebitAccount: null,
                error: action.error
            };

        // ////////

        // LOAD_LIST_BENEFICIARYGAB
        case LOAD_LIST_BENEFICIARYGAB:
            return {
                ...state,
                loading: true
            };
        case LOAD_LIST_BENEFICIARYGAB_SUCCESS:
            const listBeneficiaryGAB = action.result.beneficiaireList;
            return {
                ...state,
                loading: false,
                loaded: true,
                listBeneficiaryGAB,
                error: null
            };
        case LOAD_LIST_BENEFICIARYGAB_FAIL:
            console.log('GAB TRANSACTION FAIL');
            return {
                ...state,
                loading: false,
                loaded: false,
                listBeneficiaryGAB: null,
                error: action.error
            };

        // LOAD_LIST_DEBITACCOUNTGAB
        case LOAD_LIST_DEBITACCOUNTGAB:
            return {
                ...state,
                loading: true
            };
        case LOAD_LIST_DEBITACCOUNTGAB_SUCCESS:
            const listDebitAccountGAB = action.result.compteInstanceList;
            return {
                ...state,
                loading: false,
                loaded: true,
                listDebitAccountGAB,
                error: null
            };
        case LOAD_LIST_DEBITACCOUNTGAB_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                listDebitAccountGAB: null,
                error: action.error
            };
        // /////////

        // STEP
        case STEP_ONE:
            return {
                ...state,
                loading: false,
                loaded: true,
                step: 'stepOne',
                error: null
            };
        case STEP_TWO:
            return {
                ...state,
                loading: false,
                loaded: true,
                step: 'stepTwo',
                error: null
            };
        case STEP_THREE:
            return {
                ...state,
                loading: false,
                loaded: true,
                step: 'stepThree',
                error: null
            };
        case STATUT_ABANDONNE:
            return {
                ...state,
                loading: false,
                loaded: true,
                statut: 'Abandonné',
                error: null
            };
        case STATUT_INITIAL:
            return {
                ...state,
                loading: false,
                loaded: true,
                statut: '',
                error: null
            };



        //FIND_CLIENT


        case FIND_CLIENT:
            return {
                ...state,
                loading: true
            };
        case FIND_CLIENT_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                clientDetails: action.result.client,
                successclient: action.result.success,
                view: 'detail',
                error: null
            };
        case FIND_CLIENT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                clientDetails: null,
                error: action.error
            };

//FIND BENEFICIARE

        case FIND_CLIENT_BEN:
            return {
                ...state,
                loading: true
            };
        case FIND_CLIENT_BEN_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                clientbenDetails: action.result.Client,
                successclientbene: action.result.successbeneficiare,
                view: 'detail',
                error: null
            };
        case FIND_CLIENT_BEN_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                clientbenDetails: null,
                error: action.error
            };

        //FIND REFERENCE
        case FIND_TRANSFERT:
            return {
                ...state,
                loading: true
            };
        case FIND_TRANSFERT_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                transfertDetails: action.result.list,
                successpaiement:null,
                transfertsupprimer: false,
                transfertannuler:false,
                successreferencetransfert: action.result.success,
                view: 'detail',
                error: null
            };
        case FIND_TRANSFERT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                transfertDetails: null,
                error: action.error
            };
//FIND_IDENTI


        case FIND_IDENTITE:
            return {
                ...state,
                loading: true
            };
        case FIND_IDENTITE_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                identDetails: action.result.listtel,
                successidentite: action.result.success,
                view: 'detail',
                error: null
            };
        case FIND_IDENTITE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                identDetails: null,
                error: action.error
            };

        //FIND_TEL


        case FIND_TEL:
            return {
                ...state,
                loading: true
            };
        case FIND_TEL_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                telDetails: action.result.listtel,
                successtel: action.result.success,
                view: 'detail',
                error: null
            };
        case FIND_TEL_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                telDetails: null,
                error: action.error
            };

//find tel ben


        case FIND_TEL_BEN:
            return {
                ...state,
                loading: true
            };
        case FIND_TEL_BEN_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                telbenDetails: action.result.listtel,
                successtelben: action.result.success,
                view: 'detail',
                error: null
            };
        case FIND_TEL_BEN_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                telbenDetails: null,
                error: action.error
            };


        //PAIEMENT  TRANSFERT

        case PAYER_TRANSFERT:
            return {
                ...state,
                loadingpaiement: true,
            };
        case PAYER_TRANSFERT_SUCCESS:

            if (action.result.SUCCESS) {

                return {
                    ...state,
                    validatepwdError: false,
                    loadingpaiement: false,
                    loaded: true,
                    step: 'stepOne',
                    isInstance: true,
                    statut: 'Enregistre',
                    messagepaiement:action.result.MESSAGE,
                    successpaiement: action.result.SUCCESS,
                    transfertsupprimer:action.result.transfertsupprimer,
                    transfertannuler:action.result.transfertannuler,
                    transfertenregistrer:action.result.transfertenregistrer,
                    successCancelTransfert: false,

                    // activeAllTabs: false,

                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loadingpaiement: false,
                    loaded: true,
                    successpaiement: action.result.SUCCESS,
                    messagepaiement:action.result.MESSAGE,
                    transfertsupprimer:action.result.transfertsupprimer,
                    transfertannuler:action.result.transfertannuler,
                    transfertenregistrer:action.result.transfertenregistrer,
                    codeErreur: action.result.codeErreur,
                    successVirifierSolde:action.result.SUCCESS_CHECK_SOLDE,
                };
            }
        case PAYER_TRANSFERT_FAIL:
            return {
                ...state,
                loadingpaiement: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };


//UPADATE TRANSFERT
        case UPDATE_TRANSFERT:
            return {
                ...state,
                updateSuccess: false,
                loading: true
            };
        case UPDATE_TRANSFERT_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loading: false,
                    loaded: true,
                    step: 'stepOne',
                    isInstance: false,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    updateSuccess: action.result.success,
                    updateSuccessObject: {
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    updateSuccess: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                };
            }
        case UPDATE_TRANSFERT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };


//SAVE_CLIENTEXITE

        case SAVE_CLIENT:
            return {
                ...state,
                loadingsaveexiste: true
            };
        case SAVE_CLIENT_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    validatepwdError: false,
                    loadingsaveexiste: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: false,
                    statut: 'Enregistre',
                    successVirifierSolde:true,
                    moneyTransferObject: action.moneyTransferObject,

                    // activeAllTabs: false,
                    saveSuccessObject: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    successVirifierSolde:action.result.SUCCESS_CHECK_SOLDE,
                    codeErreur: action.result.codeErreur,
                };
            }
        case SAVE_CLIENT_FAIL:
            return {
                ...state,
                loadingsaveexiste: false,
                loaded: false,
                success: null,
                step: 'stepTwo',
                error: action.error
            };


        case SMS:
            return {
                ...state,
            };
        case SMS_SUCCESS:

            if (action.result.success) {


                return {
                    ...state,
                    saveSuccess: true,

                    error: null
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    saveSuccess: false,
                };
            }
        case SMS_FAIL:
            return {
                ...state,
                success: false,
                error: action.error
            };

        // VALIDER_STEP
        case VALIDER_STEP:
            return {
                ...state,
                loadingvalidestep: true,
            };
        case VALIDER_STEP_SUCCESS:

            if (action.result.success) {

                return {
                    ...state,
                    validatepwdError: false,
                    loadingvalidestep: false,
                    loaded: true,
                    step: 'stepTwo',
                    isInstance: true,
                    statut: 'Enregistre',
                    moneyTransferObject: action.moneyTransferObject,
                    saveSuccessValide: {
                        id: action.result.id,
                        urlred: action.result.urlred,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: true,
                        version: action.result.version,
                        CONTEXTPATH: action.result.CONTEXTPATH,
                    },
                    error: null
                };
            } else {
                return {
                    ...state,
                    validatepwdError: true,
                    loading: false,
                    loadingvalidestep: false,
                    codeErreur: action.result.codeErreur,
                    successVirifierSolde:action.result.SUCCESS_CHECK_SOLDE,
                };
            }
        case VALIDER_STEP_FAIL:
            return {
                ...state,
                loadingvalidestep: false,
                loaded: false,
                success: null,
                error: action.error
            };

        // SEND
        case SEND:
            return {
                ...state,
                loading: true
            };
        case SEND_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                dataSend: action.result,
                error: null
            };
        case SEND_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                data: null,
                error: action.error
            };

        // SIGNER_MONEYTRANSFER
        case SIGNER_MONEYTRANSFER:
            return {
                ...state,
                loading: true
            };
        case SIGNER_MONEYTRANSFER_SUCCESS:
            if (action.result.success) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    step: 'stepThree',
                    statut: 'Signé',
                    validatepwdError: false,
                    codeErreur: action.result.codeErreur,
                    signRedirect: true,
                    isInstance: false,
                    signUrlred: action.result.urlred,
                    error: null
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    showAlertError: true,
                    validatepwdError: true,
                    codeErreur: action.result.codeErreur,
                    signRedirect: false,
                    error: null
                };
            }
        case SIGNER_MONEYTRANSFER_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
// Change STATUT
        case STATUT_ABANDONNE:
            return {
                ...state,
                loading: false,
                loaded: true,
                statut: 'Abandonné',
                error: null
            };
        case STATUT_INITIAL:
            return {
                ...state,
                loading: false,
                loaded: true,
                statut: '',
                error: null
            };
// Change steps
        case STEP_ONE:
            return {
                ...state,
                loading: false,
                loaded: true,
                step: 'stepOne',
                success: false,
                show: false,
                error: null
            };
        case STEP_TWO:
            return {
                ...state,
                loading: false,
                loaded: true,
                step: 'stepTwo',
                error: null
            };
        case STEP_THREE:
            return {
                ...state,
                loading: false,
                loaded: true,
                step: 'stepThree',
                error: null
            };
// INITIAL_STATE
        case INITIAL_STATE:
            return {
                ...state,
                loading: false,
                loaded: true,
                saveSuccessObject: initialState.saveSuccessObject,
                listLcnRequest: action.list,
                error: null
            };

        // TRANSFER_STATE
        case TRANSFER_STATE:
            return {
                ...state,
                loading: false,
                loaded: true,
                saveSuccessObject: initialState.saveSuccessObject,
                Request: action.list,
                error: null
            };

// ABANDONNER_MONEYTRANSFER
        case ABANDONNER_MONEYTRANSFER:
            return {
                ...state,
                loading: true
            };
        case ABANDONNER_MONEYTRANSFER_SUCCESS:
            if (action.result.success) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    statut: 'Abandonné',
                    error: null
                };
            }
        case ABANDONNER_MONEYTRANSFER_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
// SEND_DEMANDE
        case SEND:
            return {
                ...state,
                loading: true
            };
        case SEND_SUCCESS:
            const resultSend = action.result.success;
            if (action.result.success) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    resultSend,
                    error: null
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    codeErreur: action.result.codeErreur,
                    success: false,
                    error: null
                };
            }

        case SEND_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                resultSend: null,
                error: action.error
            };
        // LOAD_PAGE_SHOW
        case LOAD_PAGE_SHOW:
            return {
                ...state,
                loading: true,
                isInstance: false
            };
        case LOAD_PAGE_SHOW_SUCCESS:
            const instance = action.result.transactionInstance;
            if (instance) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    instance,
                    otpSMS: action.result.otpSMS,
                    isInstance: false,
                    signRedirect: false,
                    saveSuccessObject: {
                        id: action.id,
                        taskId: action.taskId,
                        referenceDemande: action.result.referenceDemande,
                        saveSuccess: false,
                        version: instance.version,
                    },
                    step: 'stepTwo',
                    statut: 'Enregistre',
                    success: false,
                    listLcnRequest: false,
                    error: null
                };
            } else {
                return {
                    ...state,
                    step: 'stepOne',
                    statut: '',
                    codeErreur: '',
                    success: true,
                    signRedirect: false,
                    listLcnRequest: true,
                    isInstance: false,
                    saveSuccessObject: {
                        saveSuccess: false,
                        id: '',
                        taskId: '',
                        urlred: '',
                        version: '',
                        CONTEXTPATH: '',
                        referenceDemande: ''
                    },

                };
            }
        case LOAD_PAGE_SHOW_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                instance: null,
                otpSMS: null,
                signRedirect: false,
                isInstance: false,
                error: action.error
            };

        //GET_MONTANT_REMISE
        case MONTANT_REMISE_ENVOIS:
            return {
                ...state,
                loading: true
            };
        case MONTANT_REMISE_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                montantremise: action.result.detailmontantremise,
                successmontantremise: action.result.successmontantremise,
                view: 'detail',
                error: null
            };
        case MONTANT_REMISE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                montantremise: null,
                error: action.error
            };
//GET_FRAI_ENVOIS


        case GET_FRAIS_ENVOIS:
            return {
                ...state,
                loading: true
            };
        case GET_FRAIS_ENVOIS_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                fraienvois: action.result.fraisenvois,
                successfraisenvois: action.result.success,
                view: 'detail',
                error: null
            };
        case GET_FRAIS_ENVOIS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                fraienvois: null,
                error: action.error
            };
        // GET_INSTANCE_COLLABORATEUR
        case GET_INSTANCE_COLLABORATEUR:
            return {
                ...state,
                loading: true
            };
        case GET_INSTANCE_COLLABORATEUR_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                caisseDetails: action.result.data,
                success: action.result.success,
                etatCaisse: '',
                view: 'detail',
                error: null
            };
        case GET_COLLABORATEUR_INSTANCE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                caisseDetails: null,
                error: action.error
            };




        //GET_PDF

        case GET_PDF:
            return {
                ...state,
                loading: true
            };
        case GET_PDF_SUCCESS:

            return {
                ...state,
                loading: false,
                loaded: true,

                view: 'details',
                error: null
            };
        case GET_PDF_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,

                error: action.error
            };

        // GET_INSTANCE
        case GET_INSTANCE:
            return {
                ...state,
                updateSuccess:false,
                loading: true
            };
        case GET_INSTANCE_SUCCESS:
            const dataForDetail = action.result.DATA;
            return {
                ...state,
                loading: false,
                loaded: true,
                dataForDetail,
                loadingSigneTransfert: false,
                step: 'stepTwo',
                view: 'details',
                error: null
            };
        case GET_INSTANCE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                dataForDetail: null,
                error: action.error
            };
// VIEW_GRID
        case VIEW_GRID:
            return {
                ...state,
                loading: false,
                loaded: true,
                view: 'grid',
                isSummary: false,
                error: null
            };
        case VIEW_DETAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                view: 'detail',
                error: null
            };
// SHOW_BOUTTON_ANNULER
        case SHOW_BOUTTON_ANNULER:
            if (action.isShow === 'show') {
                return {
                    ...state,
                    showAnnuler: true,
                    error: null,
                    isSummary: true
                };
            } else {
                return {
                    ...state,
                    showAnnuler: false,
                    isSummary: true,
                    error: null
                };
            }

        // SHOW_BOUTTON_SUPPRIMER

        case SHOW_BOUTTON_SUPPRIMER:
            if (action.isShow === 'show') {
                return {
                    ...state,
                    showSupprimer: true,
                    error: null,
                    isSummary: true
                };
            } else {
                return {
                    ...state,
                    showSupprimer: true,
                    isSummary: true,
                    error: null
                };
            }
// ANNULER_DEMANDE
        case ANNULER_DEMANDE:
            return {
                ...state,
                loading: true
            };
        case ANNULER_DEMANDE_SUCCESS:
            const dataRetourAnnulation = action.result;
            return {
                ...state,
                loading: false,
                loaded: true,
                dataRetourAnnulation,
                showAnnuler: false,
                isAnnule: true,
                error: null
            };
        case ANNULER_DEMANDE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                dataRetourAnnulation: null,
                error: action.error
            };

// SUPPRIMER_DEMANDE
        case SUPPRIMER_DEMANDE:
            return {
                ...state,
                loading: true
            };
        case SUPPRIMER_DEMANDE_SUCCESS:
            const dataRetourSuppression = action.result;
            return {
                ...state,
                loading: false,
                loaded: true,
                dataRetourSuppression,
                showSupprimer: false,
                isSupprimer: true,
                error: null
            };
        case SUPPRIMER_DEMANDE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                dataRetourSuppression: null,
                error: action.error
            };


        // LOAD_STATUTS
        case LOAD_STATUTS:
            return {
                ...state,
                loading: true
            };
        case LOAD_STATUTS_SUCCESS:
            const listeStatuts = action.result.statutList;
            return {
                ...state,
                loading: false,
                loaded: true,
                listeStatuts,
                error: null
            };
        case LOAD_STATUTS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        // load transfer list
        case LOAD_SERVICE_TRANSFER:
            return {
                ...state,
                loading: true
            };
        case LOAD_SERVICE_TRANSFER_SUCCESS:
            const listeServiceTransfert = action.result.serviceTransfertList;
            return {
                ...state,
                loading: false,
                loaded: true,
                listeServiceTransfert,
                error: null
            };
        case LOAD_SERVICE_TRANSFER_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case TRANSFER_AGENCE:
            return {
                ...state,
                typeOfTransfer: 'agence',
                saveUrl: 'demandeTransfertArgent/save',
                updateUrl: 'demandeTransfertArgent/update',
                signeUrl: 'demandeTransfertArgent/signer',
                cancelUrl: 'demandeTransfertArgent/abandonnerPhaseSignature',
                error: null
            };
        case TRANSFER_GAB:
            return {
                ...state,
                typeOfTransfer: 'gab',
                saveUrl: 'demandeTransfertArgentGAB/save',
                updateUrl: 'demandeTransfertArgentGAB/update',
                signeUrl: 'demandeTransfertArgentGAB/signe',
                cancelUrl: 'demandeTransfertArgentGAB/abandonnerPhaseSignature',
                error: null
            };
        case TRANSFER_PARTENER:
            return {
                ...state,
                typeOfTransfer: 'partener',
                saveURl: '',
                updateUrl: '',
                signeUrl: '',
                cancelUrl: '',
                error: null
            };
        case RESET:
            return {
                ...state,
                successclient : null,
                step: 'stepOne',
                listLcnRequest: true,
                view: 'grid',
                instance: ''
            };

        //OPENCAISSE

        case OPEN_CAISSE:
            return {
                ...state,
                loadingOpenCaisse: true
            };
        case OPEN_CAISSE_SUCCESS:
            return {
                ...state,
                loadingOpenCaisse: false,
                loaded: true,
                successOpenCaisse: action.result.success,
                etatCaisse: action.result.etatCaisse,
                error: null
            }
        case OPEN_CAISSE_FAIL:
            return {
                ...state,
                loadingOpenCaisse: false,
                loaded: false,
                success: false,
                error: action.error,
                successOpenCaisse: false,
            };



        //Close Caisse

        case CLOSE_CAISSE_TRANSFER:
            return {
                ...state,
                loadingCloseCaisse: true
            };
        case CLOSE_CAISSE_TRANSFER_SUCCESS:
            return {
                ...state,
                loadingCloseCaisse: false,
                loaded: true,
                successCloseCaisse: action.result.success,
                unsuccessChangingEtat: action.result.unsuccess,
                messagevalidationattmise:action.result.message,
                etatCaisse: action.result.etatCaisse,
                error: null
            };
        case CLOSE_CAISSE_TRANSFER_FAIL:
            return {
                ...state,
                loadingCloseCaisse: false,
                loaded: false,
                success: false,
                error: action.error,
                successCloseCaisse: false,

            };

        //Open Agence

        case OPEN_AGENCE_TRANSFER:
            return {
                ...state,
                loading: true,
                loadingChangeEtatAgnece: true
            };
        case OPEN_AGENCE_TRANSFER_SUCCESS:
            return {
                ...state,
                loadingChangeEtatAgnece: false,
                loaded: true,
                loading: false,
                isSafeToClose: action.result.isSafeToClose,
                successOpenAgnc: action.result.etatAgence,
                unsuccessChangingEtat: action.result.unsuccess,
                successChangingEtat: action.result.success,
                statusChangingEtat: action.result.etatAgence,
                miseadispositionAgenceNv: action.result.miseadispositionAgenceNv,
                error: null
            };
        case OPEN_AGENCE_TRANSFER_FAIL:
            return {
                ...state,
                loadingChangeEtatAgnece: false,
                loaded: false,
                loading: false,
                success: false,
                error: action.error,
                statusChangingEtat: action.result.etatAgence,
                successOpenAgnc: false
            };

        //LOAD AGENCE
        case LOAD_AGENCE_TRANSFER:
            return {
                ...state,
                loading: true,
                loadingAgence: true,
                dataForDetail: null,
                successAttribution: null,
                successvalidationAttribution:null,
                successrefusionAttribution:null,
                successDeleteTransfert: null,
                successCancelTransfert: null,
                msg:null,
                message:null,
                loadingDeleteTransfert: null,
                loadingCancelTransfert: null,
                successMiseadispoAgence: null,
                unsuccessChangingEtat: null
            };
        case LOAD_AGENCE_TRANSFER_SUCCESS:

            return {
                ...state,
                loading: false,
                loaded: true,
                loadingAgence: false,
                success: action.result.success,
                agenceObj: action.result.data,
                caisseObj: false,
                dataForDetail: null,
                successOpenAgnc: action.result.data.etatAgence,
                statusChangingEtat: action.result.data.etatAgence,
                error: null
            };
        case LOAD_AGENCE_TRANSFER_FAIL:
            return {
                ...state,
                loading: false,
                loadingAgence: false,
                loaded: false,
                success: false,
                error: action.error
            };

            // LOAD_SEUIL_SOLDE_AGENCE_TRANSFER

            case LOAD_SEUIL_SOLDE_AGENCE_TRANSFER:
                return {
                    ...state,
                    loading: true,
                    loadingAgence: true,
                    dataForDetail: null,
                    successAttribution: null,
                    successvalidationAttribution:null,
                    successrefusionAttribution:null,
                    successDeleteTransfert: null,
                    successCancelTransfert: null,
                    msg:null,
                    message:null,
                    successVirifierSolde:true,
                    loadingDeleteTransfert: null,
                    loadingCancelTransfert: null,
                    successMiseadispoAgence: null,
                    unsuccessChangingEtat: null
                };
            case LOAD_SEUIL_SOLDE_AGENCE_TRANSFER_SUCCESS:

                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    loadingAgence: false,
                    successseuilagence: action.result.success,
                    messageSeuilAgence: action.result.messageSeuilAgence,
                    agenceObj: action.result.data,
                    caisseObj: false,
                    dataForDetail: null,
                    successOpenAgnc: action.result.data.etatAgence,
                    statusChangingEtat: action.result.data.etatAgence,
                    error: null
                };
            case LOAD_SEUIL_SOLDE_AGENCE_TRANSFER_FAIL:
                return {
                    ...state,
                    loading: false,
                    loadingAgence: false,
                    loaded: false,
                    successseuilagence: false,
                    error: action.error
                };

        //LOAD PAIMENT NON PAYER
        case LOAD_PAIEMENT_NON_PAYER:
            return {
                ...state,
                loading: true,
                loadingPaimentNonPayer: true,
            };
        case LOAD_PAIEMENT_NON_PAYER_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                loadingPaimentNonPayer: false,
                success: action.result.success,
                listPaimentNonPayer: action.result.list,
                error: null
            };
        case LOAD_PAIEMENT_NON_PAYER_FAIL:
            return {
                ...state,
                loading: false,
                loadingPaimentNonPayer: false,
                loaded: false,
                success: false,
                error: action.error
            };

        //LOAD Region
        case FIND_LOAD_REGION:
            return {
                ...state,
                loading: true,
                loadingRegion: true,
            };
        case FIND_LOAD_REGION_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                loadingRegion: false,
                success: action.result.success,
                regionObj: action.result.data,
                error: null
            };
        case FIND_LOAD_REGION_FAIL:
            return {
                ...state,
                loading: false,
                loadingRegion: false,
                loaded: false,
                success: false,
                error: action.error
            };

        // FIND_VILLE_BY_REGION
        case FIND_VILLE_BY_REGION:
            return {
                ...state,
                loading: true,
                loadingVille: true,
            };
        case FIND_VILLE_BY_REGION_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                loadingVille: false,
                success: action.result.success,
                listVille: action.result.list,
                error: null
            };
        case FIND_VILLE_BY_REGION_FAIL:
            return {
                ...state,
                loading: false,
                loadingVille: false,
                loaded: false,
                success: false,
                error: action.error
            };

        //LOAD Transfert region :
        case LOAD_LIST_TRANSFER_REGION:
            return {
                ...state,
                loadingListTransfertRegion: true,
                  updateSuccess:false,
            };
        case LOAD_LIST_TRANSFER_REGION_SUCCESS:
            const transfetListRegion = action.result.transfetListRegion;
            successSigneTransfert: false;
            transfetListRegion.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                loadingListTransfertRegion: false,
                loaded: true,
                updateSuccess:false,
                success: action.result.success,
                transfetListRegion,
                error: null
            };
        case LOAD_LIST_TRANSFER_REGION_FAIL:
            return {
                ...state,
                loaded: false,
                error: action.error,
                success: false,
                successpaiement: false,
                transfertsupprimer: false,
                transfertannuler:false,
                loadingListTransfertAgence: false
            };

        //LOAD Transfert Agence Agence region :
        case LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION:
            return {
                ...state,
                loadingListTransfertAgncAgncRegion: true
            };
        case LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION_SUCCESS:
            const transfetListAgncAgncRegion = action.result.list;

            transfetListAgncAgncRegion.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                loadingListTransfertAgncAgncRegion: false,
                loaded: true,
                successpaiement: null,
                transfertsupprimer: false,
                transfertannuler:false,
                success: action.result.success,
                transfetListAgncAgncRegion,
                totalmontantagenceregion: action.result.totalMontant,
                error: null
            };
        case LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION_FAIL:
            return {
                ...state,
                loaded: false,
                error: action.error,
                success: false,
                loadingListTransfertAgncAgncRegion: false
            };

        //LOAD LOAD_LIST_OUVERTURE_CLOTURE_REGION :
        case LOAD_LIST_OUVERTURE_CLOTURE_REGION:
            return {
                ...state,
                loadingListOuvertureClotureRegion: true
            };
        case LOAD_LIST_OUVERTURE_CLOTURE_REGION_SUCCESS:
            const listOuvertureClotureRegion = action.result.list;
            listOuvertureClotureRegion.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                loadingListOuvertureClotureRegion: false,
                loaded: true,
                success: action.result.success,
                listOuvertureClotureRegion,
                error: null
            };
        case LOAD_LIST_OUVERTURE_CLOTURE_REGION_FAIL:
            return {
                ...state,
                loaded: false,
                error: action.error,
                success: false,
                loadingListOuvertureClotureRegion: false
            };

        //LIST AGENCE
        case LOAD_LIST_AGENCE_BY_REGION:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_AGENCE_BY_REGION_SUCCESS:
            const listeAgenceByRegion = action.result.listeAgenceByRegion;
            listeAgenceByRegion.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                listeAgenceByRegion,
                error: null,
                successMiseadispoAgence: null

            };
        case LOAD_LIST_AGENCE_BY_REGION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: action.result.success,
                msg: action.result.msg,
                error: action.error,
                successMiseadispoAgence: null
            };
       /**
        *
         case LOAD_LIST_CAISSE_BY_AGENCE:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_CAISSE_BY_AGENCE_SUCCESS:
            const listeDesCaisses = action.result.listeDesCaisses;
            listeDesCaisses.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                listeDesCaisses,
                error: null,

            };
        case LOAD_LIST_CAISSE_BY_AGENCE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: action.result.success,
                msg: action.result.msg,
                error: action.error,
            };

*/
            /**
        case LOAD_LIST_USER:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null
            };
        case LOAD_LIST_USER_SUCCESS:
            const listeDesCaisses = action.result.listeDesCaisses;
            listeDesCaisses.forEach(function (element) {
                const demande = element;
                demande.action = 'xxx';
            });

            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                listeDesCaisses,
                error: null,

            };
        case LOAD_LIST_USER_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: action.result.success,
                msg: action.result.msg,
                error: action.error,
            };

            */
        //Attribution
        case ATTRIBUTION_LIST_TRANSFER:
            return {
                ...state,
                loadingAttribution: true
            };
        case ATTRIBUTION_LIST_TRANSFER_SUCCESS:
            return {
                ...state,
                loadingAttribution: false,
                loaded: true,
                successAttribution: action.result.success,
                error: null
            };
        case ATTRIBUTION_LIST_TRANSFER_FAIL:
            return {
                ...state,
                loadingAttribution: false,
                loaded: false,
                successAttribution: action.result.success,
                error: action.error
            };


            // validation Attribution
            case VALIDER_ATTRIBUTION_LIST_TRANSFER:
                return {
                    ...state,
                    loadingAttribution: true
                };
            case VALIDER_ATTRIBUTION_LIST_TRANSFER_SUCCESS:
                return {
                    ...state,
                    loadingvalidationAttribution: false,
                    loaded: true,
                    successvalidationAttribution: action.result.success,
                    error: null
                };
            case VALIDER_ATTRIBUTION_LIST_TRANSFER_FAIL:
                return {
                    ...state,
                    loadingvalidationAttribution: false,
                    loaded: false,
                    successvalidationAttribution: action.result.success,
                    error: action.error
                };
        //refusion Attribution
                case REFUSION_ATTRIBUTION_LIST_TRANSFER:
                    return {
                        ...state,
                        loadingAttribution: true
                    };
                case VALIDER_ATTRIBUTION_LIST_TRANSFER_SUCCESS:
                    return {
                        ...state,
                        loadingrefusionAttribution: false,
                        loaded: true,
                        successrefusionAttribution: action.result.success,
                        error: null
                    };
                case REFUSION_ATTRIBUTION_LIST_TRANSFER_FAIL:
                    return {
                        ...state,
                        loadingrefusionAttribution: false,
                        loaded: false,
                        successrefusionAttribution: action.result.success,
                        error: action.error
                    };
        // Mise a disposition
        case MISEADISPO_LIST_TRANSFER:
            return {
                ...state,
                loadingMiseadispo: true
            };
        case MISEADISPO_LIST_TRANSFER_SUCCESS:
            return {
                ...state,
                loadingMiseadispo: false,
                loaded: true,
                successMiseadispo: action.result.success,
                error: null
            };
        case MISEADISPO_LIST_TRANSFER_FAIL:
            return {
                ...state,
                loadingMiseadispo: false,
                loaded: false,
                successMiseadispo: action.result.success,
                error: action.error
            };
            //ALIMENTATION

            case ALIMENTATION:
                return {
                    ...state,
                    loadingAlimentation: true
                };
            case ALIMENTATION_SUCCESS:
                return {
                    ...state,
                    loadingAlimentation: false,
                    loaded: true,
                    successAlimentation: action.result.success,
                    error: null
                };
            case ALIMENTATION_FAIL:
                return {
                    ...state,
                    loadingAlimentation: false,
                    loaded: false,
                    successAlimentation: action.result.success,
                    error: action.error
                };
              //retrait
                case RETRAIT:
                  return {
                      ...state,
                      loadingRetrai: true
                  };
              case RETRAIT_SUCCESS:
                  return {
                      ...state,
                      loadingRetrai: false,
                      loaded: true,
                      successRetrait: action.result.success,
                      error: null
                  };
              case RETRAIT_FAIL:
                  return {
                      ...state,
                      loadingRetrai: false,
                      loaded: false,
                      successRetrait: action.result.success,
                      error: action.error
                  };

            // Validation Mise a disposition
            case VALIDER_MISEADISPO_LIST_TRANSFER:
                return {
                    ...state,
                    loadingvalideMiseadispo: true
                };
            case VALIDER_MISEADISPO_LIST_TRANSFER_SUCCESS:
                return {
                    ...state,
                    loadingvalideMiseadispo: false,
                    loaded: true,
                    successValideMiseadispo: action.result.success,
                    error: null
                };
            case VALIDER_MISEADISPO_LIST_TRANSFER_FAIL:
                return {
                    ...state,
                    loadingvalideMiseadispo: false,
                    loaded: false,
                    successValideMiseadispo: action.result.success,
                    error: action.error
                };

              // Refusion Mise a disposition
            case REFUSER_MISEADISPO_LIST_TRANSFER:
                return {
                    ...state,
                    loadingrefuseMiseadispo: true
                };
            case REFUSER_MISEADISPO_LIST_TRANSFER_SUCCESS:
                return {
                    ...state,
                    loadingrefuseMiseadispo: false,
                    loaded: true,
                    successRefuseMiseadispo: action.result.success,
                    error: null
                };
            case REFUSER_MISEADISPO_LIST_TRANSFER_FAIL:
                return {
                    ...state,
                    loadingrefuseMiseadispo: false,
                    loaded: false,
                    successRefuseMiseadispo: action.result.success,
                    error: action.error
                };


        // Mise a disposition Agence
        case MISEADISPO_AGENCE_TRANSFER:
            return {
                ...state,
                loadingMiseadispoAgence: true
            };
        case MISEADISPO_AGENCE_TRANSFER_SUCCESS:
            return {
                ...state,
                loadingMiseadispoAgence: false,
                loaded: true,
                successMiseadispoAgence: action.result.success,
                refMADA: action.result.ref,
                error: null
            };
        case MISEADISPO_AGENCE_TRANSFER_FAIL:
            return {
                ...state,
                loadingMiseadispoAgence: false,
                loaded: false,
                successMiseadispoAgence: action.result.success,
                error: action.error
            };

        // Encaissement Agence
        case ENCAISSEMENT_AGENCE_TRANSFERT:
            return {
                ...state,
                loadingEncaissemntAgence: true
            };
        case ENCAISSEMENT_AGENCE_TRANSFERT_SUCCESS:
            return {
                ...state,
                loadingEncaissemntAgence: false,
                loaded: true,
                successEncaissementAgence: action.result.success,
                refMADA: action.result.ref,
                error: null
            };
        case ENCAISSEMENT_AGENCE_TRANSFERT_FAIL:
            return {
                ...state,
                loadingEncaissemntAgence: false,
                loaded: false,
                successEncaissementAgence: false,
                error: action.error
            };

        // AGENT CAISSE LOAD CAISSE

        case LOAD_CAISSE_TRANSFER:
            return {
                ...state,
                loading: true,
                successMiseadispo: null,
                successValideMiseadispo: null,
                successRefuseMiseadispo: null,
                successCloseCaisse: null,
                successOpenCaisse: null,

            };
        case LOAD_CAISSE_TRANSFER_SUCCESS:
            return {
                ...state,
                error: null,
                loaded: true,
                loading: false,
                etatCaisse: null,
                successOpenCaisse: null,
                successCloseCaisse: null,
                caisseObj: action.result.data,
                success: action.result.success
            };
        case LOAD_CAISSE_TRANSFER_FAIL:
            return {
                ...state,
                loaded: false,
                loading: false,
                success: false,
                etatCaisse: null,
                error: action.error,
                successOpenCaisse: null
            };
            //types: [LOAD_CAISSE_TRANSFER_PARTENAIRE, LOAD_CAISSE_TRANSFER_PARTENAIRE_SUCCESS, LOAD_CAISSE_TRANSFER_PARTENAIRE_FAIL],
            case LOAD_CAISSE_TRANSFER_PARTENAIRE:
                return {
                    ...state,
                    loading: true,
                    successMiseadispo: null,
                    successValideMiseadispo: null,
                    successRefuseMiseadispo: null,
                    successCloseCaisse: null,
                    successOpenCaisse: null,

                };
            case LOAD_CAISSE_TRANSFER_PARTENAIRE_SUCCESS:
                return {
                    ...state,
                    error: null,
                    loaded: true,
                    loading: false,
                    etatCaisse: null,
                    successOpenCaisse: null,
                    successCloseCaisse: null,
                    caisseObjP: action.result.data,
                    success: action.result.success
                };
            case LOAD_CAISSE_TRANSFER_PARTENAIRE_FAIL:
                return {
                    ...state,
                    loaded: false,
                    loading: false,
                    success: false,
                    etatCaisse: null,
                    error: action.error,
                    successOpenCaisse: null
                };

      case LOAD_SEUIL_SOLDE_CAISSE_TRANSFER:
            return {
                ...state,
                loading: true,
                successMiseadispo: null,
                successValideMiseadispo: null,
                successRefuseMiseadispo: null,
                successCloseCaisse: null,
                successOpenCaisse: null,

            };
        case LOAD_SEUIL_SOLDE_CAISSE_TRANSFER_SUCCESS:
            return {
                ...state,
                error: null,
                loaded: true,
                loading: false,
                etatCaisse: null,
                successOpenCaisse: null,
                successCancelTransfert: null,
                successDeleteTransfert:null,
                successCloseCaisse: null,
                successpaiement: null,
                transfertsupprimer: false,
                transfertannuler:false,
                caisseObj: action.result.data,
                successeuil: action.result.success,
                msgSeuilSoldecaisee: action.result.msgSeuilSoldecaisee,
            };
        case LOAD_SEUIL_SOLDE_CAISSE_TRANSFER_FAIL:
            return {
                ...state,
                loaded: false,
                loading: false,
                success: false,
                etatCaisse: null,
                error: action.error,
                successOpenCaisse: null
            };

        //LOAD CAISSE LIST

        case LOAD_CAISSE_LIST_TRANSFER:
            return {
                ...state,
                loadingListTransfertCaisse: true
            };
        case LOAD_CAISSE_LIST_TRANSFER_SUCCESS:
            const transfetCaisseList = action.result.list;
            return {
                ...state,
                loadingListTransfertCaisse: false,
                loaded: true,
                success: action.result.success,
                transfetCaisseList,
                successpaiement: null,
                error: null
            };
        case LOAD_CAISSE_LIST_TRANSFER_FAIL:
            return {
                ...state,
                loadingListTransfertCaisse: false,
                loaded: false,
                success: false,
                error: action.error
            };

        //DELETE TRANSFERT CLT CLT

        case DELETE_TRANSFERT_CLT_CLT:
            return {
                ...state,
                loadingDeleteTransfert: true
            };
        case DELETE_TRANSFERT_CLT_CLT_SUCCESS:
            return {
                ...state,
                loadingDeleteTransfert: false,
                loaded: true,
                successDeleteTransfert: action.result.success,
                error: null
            };
        case DELETE_TRANSFERT_CLT_CLT_FAIL:
            return {
                ...state,
                loadingDeleteTransfert: false,
                loaded: false,
                successDeleteTransfert: null,
                error: action.error
            };

        //CANCEL TRANSFERT CLT CLT

        case CANCEL_TRANSFERT_CLT_CLT:
            return {
                ...state,
                loadingCancelTransfert: true
            };
        case CANCEL_TRANSFERT_CLT_CLT_SUCCESS:
            return {
                ...state,
                loadingCancelTransfert: false,
                loaded: true,
                successCancelTransfert: action.result.success,
                error: null
            };
        case CANCEL_TRANSFERT_CLT_CLT_FAIL:
            return {
                ...state,
                loadingCancelTransfert: false,
                loaded: false,
                successCancelTransfert: null,
                error: action.error
            };

        //SIGNE TRANSFERT CLT CLT

        case SIGNE_TRANSFERT_CLT_CLT:
            return {
                ...state,
                loadingSigneTransfert: true,
                updateSuccess:false,
            };
        case SIGNE_TRANSFERT_CLT_CLT_SUCCESS:

          if (action.result.success) {
            return {
                ...state,
                loadingSigneTransfert: false,
                loaded: true,
                step: 'stepThree',
                 updateSuccess:false,
                successSigneTransfert: action.result.success,
                soldegarantie: action.result.soldegarantie,
                error: null
            };}
         else {
              return {
                 ...state,
                 updateSuccess:false,
                 successSigneTransfert: action.result.success,
                 soldegarantie: action.result.soldegarantie,
                 message:action.result.msg,
                   };
               }
        case SIGNE_TRANSFERT_CLT_CLT_FAIL:
            return {
                ...state,
                loadingSigneTransfert: false,
                loaded: false,
                successSigneTransfert: "null",
                error: action.error
            };
          // SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT

          case SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT:
              return {
                  ...state,
                  loadingSignechefTransfert: true
              };
          case SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT_SUCCESS:
              return {
                  ...state,
                  loadingSignechefTransfert: false,
                  loaded: true,
                  step: 'stepThree',
                  updateSuccess:false,
                  successSignechefTransfert: action.result.success,
                  error: null
              };
          case SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT_FAIL:
              return {
                  ...state,
                  loadingSignechefTransfert: false,
                  loaded: false,
                  successSignechefTransfert: null,
                  error: action.error
              };

              //  SIGNE_CHEFAGENCE_PAIEMENT_CLT_CLT_

               case SIGNE_CHEFAGENCE_PAIEMENT_CLT_CLT:
                   return {
                       ...state,
                       loadingSignechefTransfert: true
                   };
               case SIGNE_CHEFAGENCE_PAIEMENT_CLT_CLT_SUCCESS:
                   return {
                       ...state,
                       loadingSignechefPaiement: false,
                       loaded: true,
                       step: 'stepThree',
                       successSignechefTransfert: null,
                       successSignechefPaiement: action.result.success,
                       error: null
                   };
               case SIGNE_CHEFAGENCE_PAIEMNT_CLT_CLT_FAIL:
                   return {
                       ...state,
                       loadingSignechefPaiement: false,
                       loaded: false,
                       successSignechefPaiement: null,
                       error: action.error
                   };
        //Liste des caisses by agence

        case LOAD_LIST_CAISSE_TRANSFER:
            return {
                ...state,
                loading: true
            };
        case LOAD_LIST_CAISSE_TRANSFER_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                successCancelTransfert: false,
                listCaisse: action.result.listNumCaisse,
                error: null
            };
        case LOAD_LIST_CAISSE_TRANSFER_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                error: action.error
            };


        //LOAD List Agence

        case LOAD_LIST_AGENCE:
            return {
                ...state,
                loading: true,
                successMiseadispoAgence: null,
                successEncaissementAgence: null
            };
        case LOAD_LIST_AGENCE_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                success: action.result.success,
                listAgence: action.result.list,
                error: null,
                successMiseadispoAgence: null,
                successEncaissementAgence: null

            };
        case LOAD_LIST_AGENCE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                success: false,
                error: action.error,
                successMiseadispoAgence: null,
                successEncaissementAgence: null

            };

        //LOAD List Mise a disposition Agence
        case LOAD_LIST_MISE_A_DISPO_AGENCE:
            return {
                ...state,
                loadingMiseadispoList: true
            };
        case LOAD_LIST_MISE_A_DISPO_AGENCE_SUCCESS:
            return {
                ...state,
                loadingMiseadispoList: false,
                loaded: true,
                success: action.result.success,
                listMiseadispositionAgence: action.result.list,
                error: null
            };
        case LOAD_LIST_MISE_A_DISPO_AGENCE_FAIL:
            return {
                ...state,
                loadingMiseadispoList: false,
                loaded: false,
                success: false,
                error: action.error
            };

        //LOAD List Compte Comptable

        case LOAD_LIST_COMPTE_COMPTA:
            return {
                ...state,
                loadingCompteComptaList: true
            };
        case LOAD_LIST_COMPTE_COMPTA_SUCCESS:
            return {
                ...state,
                loadingCompteComptaList: false,
                loaded: true,
                success: action.result.success,
                listCompteCompta: action.result.list,
                error: null
            };
        case LOAD_LIST_COMPTE_COMPTA_FAIL:
            return {
                ...state,
                loadingCompteComptaList: false,
                loaded: false,
                success: false,
                error: action.error
            };


        //LOAD List des ouvertures et les clotures des caisse

        case LOAD_LIST_OUVERTURE_CLOTURE:
            return {
                ...state,
                loadingActionList: true
            };
        case LOAD_LIST_OUVERTURE_CLOTURE_SUCCESS:
            return {
                ...state,
                loadingActionList: false,
                successSignechefTransfert:false,
                successSignechefPaiement:false,
                loaded: true,
                success: action.result.success,
                actionList: action.result.list,
                error: null
            };
        case LOAD_LIST_COMPTE_COMPTA_FAIL:
            return {
                ...state,
                loadingActionList: false,
                loaded: false,
                success: false,
                error: action.error
            };

        //LOAD8LIST REFERENCES
        case LOAD_LIST_REFERENCES:
            return {
                ...state,
                loading: true
            };
        case LOAD_LIST_REFERENCES_SUCCESS:
            const referenceList = action.result.list;

            return {
                ...state,
                loading: false,
                loaded: true,
                successreference: action.result.success,

                referenceList,
                error: null
            };
        case LOAD_LIST_REFERENCES_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                successreference: false,
                error: action.error
            };


case LOAD_TRANSFER:
            return {
                ...state
                
            };

        // LOAD TRANSFERT


        case LOAD_LIST_TRANSFER:
            return {
                ...state,
                loadingListTransfertAgence: true
            };
        case LOAD_LIST_TRANSFER_SUCCESS:
            const transfetList = action.result.list;
            transfetList.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                loadingListTransfertAgence: false,
                loaded: true,
                successVirifierSolde:true,
                success: action.result.success,
                totalMontant: action.result.totlaMontant,
                transfetList,
                error: null
            };
        case LOAD_LIST_TRANSFER_FAIL:
            return {
                ...state,
                loaded: false,
                error: action.error,
                success: false,
                loadingListTransfertAgence: false
            };
              // Load List transfert Agence Agence
            case VERIFICATION_EXISTENCE_ATTRIBUTION:
                return {
                    ...state,
                  loaded: false
                };
            case VERIFICATION_EXISTENCE_ATTRIBUTION_SUCCESS:

                return {
                    ...state,
                    loaded: true,
                    successverificationattribution: action.result.success,
                    transfetList,
                    error: null
                };
            case VERIFICATION_EXISTENCE_ATTRIBUTION_FAIL:
                return {
                    ...state,
                    loaded: false,
                    error: action.error,
                    successverificationattribution: false,
                };
        // Load List transfert Agence Agence
        case LOAD_LIST_TRANSFER_AGNC_AGNC:
            return {
                ...state,
                loadingListTransfertAgenceAgence: true
            };
        case LOAD_LIST_TRANSFER_AGNC_AGNC_SUCCESS:
            const transfetAgncAgncList = action.result.list;
            transfetAgncAgncList.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                loadingListTransfertAgenceAgence: false,
                loaded: true,
                paiementsucces: false,
                success: action.result.success,
                totalMontantTransfertAgence: action.result.totalMontant,
                transfetAgncAgncList,
                error: null
            };
        case LOAD_LIST_TRANSFER_AGNC_AGNC_FAIL:
            return {
                ...state,
                loadingListTransfertAgenceAgence: false,
                loaded: false,
                success: false,
                error: action.error
            };
        default:
            return state;
    }
}

function objectToParams(object) {
    let str = '';
    for (const key in object) {
        if (str !== '') {
            str += '&';
        }
        str += key + '=' + encodeURIComponent(object[key]);
    }
    return str;
}

export function loadRegionByIdRegion(idAgence) {
    return {
        types: [FIND_LOAD_REGION, FIND_LOAD_REGION_SUCCESS, FIND_LOAD_REGION_FAIL],
        promise: (client) => client.get('regionTransfertArgent/region/' + idAgence)
    };
}


export function loadVilleByIdRegion(idAgence) {
    return {
        types: [FIND_VILLE_BY_REGION, FIND_VILLE_BY_REGION_SUCCESS, FIND_VILLE_BY_REGION_FAIL],
        promise: (client) => client.get('regionTransfertArgent/villeByRegion/' + idAgence)
    };
}


export function loadPaiementNonPayer(idAgence, idVille, montanttotal) {

    if (idAgence == undefined) {
        idAgence = "";
    }

    if (idVille == undefined) {
        idVille = "";
    }

    if (montanttotal == undefined) {
        montanttotal = "";
    }


    const obj = {
        idAgence,
        idVille,
        montanttotal
    };

    return {
        types: [LOAD_PAIEMENT_NON_PAYER, LOAD_PAIEMENT_NON_PAYER_SUCCESS, LOAD_PAIEMENT_NON_PAYER_FAIL],
        promise: (client) => client.post('regionTransfertArgent/listEstimation', {data: objectToParams(obj)})
    };
}

export function loadListTransfertRegion(idAgence, idAgence1, codeCaisse, typeTransfert, dateTransfertDebut, dateTransfertFin, nomEmetteur, nomBenif, montantMin, montantMax, agent, statut) {
    return {
        types: [LOAD_LIST_TRANSFER_REGION, LOAD_LIST_TRANSFER_REGION_SUCCESS, LOAD_LIST_TRANSFER_REGION_FAIL],
        promise: (client) => client.get('regionTransfertArgent/region/' + idAgence + '/listtransfert?idAgence1=' + idAgence1 + '&codeCaisse=' + codeCaisse + '&typeTransfert=' + typeTransfert + '&dateTransfertDebut=' + dateTransfertDebut + '&dateTransfertFin=' + dateTransfertFin + '&nomEmetteur=' + nomEmetteur + '&nomBenif=' + nomBenif + '&montantMin=' + montantMin + '&montantMax=' + montantMax + '&agent=' + agent + '&statut=' + statut)
    };
}

export function loadListTransfertAgenceAgenceRegion(idAgence, typeTransfert, agenceEchange, dateReceptionDebut, dateReceptionFin, dateEmissionDebut, dateEmissionFin, montantOperationDebut, montantOperationFin) {
    return {
        types: [LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION, LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION_SUCCESS, LOAD_LIST_TRANSFER_AGENCE_AGENCE_REGION_FAIL],
        promise: (client) => client.get('regionTransfertArgent/region/' + idAgence + '/listtransfertagncagnc?typeTransfert=' + typeTransfert + '&agenceEchange=' + agenceEchange + '&dateReceptionDebut=' + dateReceptionDebut + '&dateReceptionFin=' + dateReceptionFin + '&dateEmissionDebut=' + dateEmissionDebut + '&dateEmissionFin=' + dateEmissionFin + '&montantOperationDebut=' + montantOperationDebut + '&montantOperationFin=' + montantOperationFin)
    };
}


export function loadListOuvertureClotureByRegion(idAgence, idAgence1, numcaisse, agentaction, montantinitial, montantfinal, dateActionDebut, dateActionFin, heuredebut, heurefin) {
    return {
        types: [LOAD_LIST_OUVERTURE_CLOTURE_REGION, LOAD_LIST_OUVERTURE_CLOTURE_REGION_SUCCESS, LOAD_LIST_OUVERTURE_CLOTURE_REGION_FAIL],
        promise: (client) => client.get('regionTransfertArgent/region/' + idAgence + '/listouvetturecloture?agenceAction='+idAgence1+'&numcaisse=' + numcaisse + '&agentaction=' + agentaction + '&montantinitial=' + montantinitial + '&montantfinal=' + montantfinal + '&dateActionDebut=' + dateActionDebut + '&dateActionFin=' + dateActionFin + '&heuredebut=' + heuredebut + '&heurefin=' + heurefin)
    };
}


export function loadListAgenceByRegion(idAgence) {
    return {
        types: [LOAD_LIST_AGENCE_BY_REGION, LOAD_LIST_AGENCE_BY_REGION_SUCCESS, LOAD_LIST_AGENCE_BY_REGION_FAIL],
        promise: (client) => client.get('regionTransfertArgent/region/' + idAgence + '/listAgences')
    };
}

export function loadCompteGarantie(idPartenaire) {
    return {
        types: [LOAD_COMPTE_GARANTIE, LOAD_COMPTE_GARANTIE_SUCCESS, LOAD_COMPTE_GARANTIE_FAIL],
        promise: (client) => client.get('compteGarantie/soldeCompteGarantie/' + idPartenaire)
    };
}

/**export function loadListCaisseByAgence(idAgence) {
    return {
        types: [LOAD_LIST_CAISSE_BY_AGENCE, LOAD_LIST_CAISSE_BY_AGENCE_SUCCESS, LOAD_LIST_CAISSE_BY_AGENCE_FAIL],
        promise: (client) => client.get('regionTransfertArgent/listeCaisses/' + idAgence)
    };
}*/
export function loadListMoneyTransfer(page, ncompteRecherche, dateDebutRecherche, dateFinRecherche, montantMin, montantMax, statutRecherche, identifiantDemandeRecheche, typeTransfert) {
    return {
        page,
        ncompteRecherche,
        dateDebutRecherche,
        dateFinRecherche,
        statutRecherche,
        identifiantDemandeRecheche,
        montantMin,
        montantMax,
        typeTransfert,
        types: [LOAD_LIST_MONEYTRANSFER, LOAD_LIST_MONEYTRANSFER_SUCCESS, LOAD_LIST_MONEYTRANSFER_FAIL],
        promise: (client) => client.get('demandeTransfertArgent/listObject?format=json&_dc=1476701343020&dateDebutRecherche=&dateFinRecherche=&montantMin=&montantMax=&identifiantDemandeRecheche=&page=' + page + '&offset=0&max=10')
    };
}

export function loadListMoneyTransferGAB(page) {
    return {
        types: [LOAD_LIST_MONEYTRANSFERGAB, LOAD_LIST_MONEYTRANSFERGAB_SUCCESS, LOAD_LIST_MONEYTRANSFERGAB_FAIL],
        promise: (client) => client.get('demandeTransfertArgent/listObject?format=json&_dc=1476701343020&dateDebutRecherche=&dateFinRecherche=&montantMin=&montantMax=&identifiantDemandeRecheche=&page=' + page + '&offset=0&max=10')
    };
}

export function loadListBeneficiary() {
    return {
        types: [LOAD_LIST_BENEFICIARY, LOAD_LIST_BENEFICIARY_SUCCESS, LOAD_LIST_BENEFICIARY_FAIL],
        promise: (client) => client.get('benificiaire/mesBeneficiairesListMAD')
    };
}


//

export function loadListBeneficiaryGAB() {
    return {
        types: [LOAD_LIST_BENEFICIARYGAB, LOAD_LIST_BENEFICIARYGAB_SUCCESS, LOAD_LIST_BENEFICIARYGAB_FAIL],
        promise: (client) => client.get('benificiaire/mesBeneficiairesList')
    };
}

export function loadListDebitAccountGAB() {
    return {
        types: [LOAD_LIST_DEBITACCOUNTGAB, LOAD_LIST_DEBITACCOUNTGAB_SUCCESS, LOAD_LIST_DEBITACCOUNTGAB_FAIL],
        promise: (client) => client.get('compte/comptesPourConsultation?typeOperation=DB')
    };
}

export function toStepX(step) {
    if (step === 'stepOne') {
        return {type: STEP_ONE};
    } else if (step === 'stepTwo') {
        return {type: STEP_TWO};
    }
    return {type: STEP_THREE};
}

export function setStatut(statut) {
    if (statut === 'Abandonné') {
        return {type: STATUT_ABANDONNE};
    }
    return {type: STATUT_INITIAL};
}

export function validerSTEP(moneyTransferFormObject, fraisenvois, villebeneficiare) {


    const moneyTransferMADFormObjectClone = moneyTransferFormObject;

    //info emeteur
    moneyTransferMADFormObjectClone.numeroPieceIdentiteemeteur = moneyTransferFormObject.numeroPieceIdentiteemeteur;
    moneyTransferMADFormObjectClone.numtelemetteur = moneyTransferFormObject.numtelemetteur;
    moneyTransferMADFormObjectClone.nomemetteur = moneyTransferFormObject.nomemetteur;
    moneyTransferMADFormObjectClone.prenomemeteur = moneyTransferFormObject.prenomemeteur;
    moneyTransferMADFormObjectClone.emailemeteur = moneyTransferFormObject.emailemetteur;
    moneyTransferMADFormObjectClone.adresseemeteur = moneyTransferFormObject.adresseemeteur;
    moneyTransferMADFormObjectClone.villeemeteur = moneyTransferFormObject.villeemeteur;
    moneyTransferMADFormObjectClone.codepostalemeteur = moneyTransferFormObject.codepostalemetteur;
    moneyTransferMADFormObjectClone.paysemetteur = moneyTransferFormObject.paysemetteur;

    //info beneficiare
    moneyTransferMADFormObjectClone.numeroPieceIdentitebeneficiare = moneyTransferFormObject.numeroPieceIdentitebeneficiare;
    moneyTransferMADFormObjectClone.numerotelebeneficiare = moneyTransferFormObject.numtelbeneficiare;
    moneyTransferMADFormObjectClone.nombeneficiare = moneyTransferFormObject.nombeneficiare;
    moneyTransferMADFormObjectClone.prenombeneficiare = moneyTransferFormObject.prenombeneficiare;
    moneyTransferMADFormObjectClone.emailbeneficiare = moneyTransferFormObject.emailbeneficiare;
    moneyTransferMADFormObjectClone.adressebeneficiare = moneyTransferFormObject.adressebeneficiare;
    moneyTransferMADFormObjectClone.villebeneficiare = villebeneficiare;
    moneyTransferMADFormObjectClone.codepostalbeneficiare = moneyTransferFormObject.codepostalbeneficiare;
    moneyTransferMADFormObjectClone.paysbeneficiare = moneyTransferFormObject.paysbeneficiare;
    //transfert client clinet
    moneyTransferMADFormObjectClone.fraiinclus = moneyTransferFormObject.fraiinclus;
    moneyTransferMADFormObjectClone.fraienvoi = moneyTransferFormObject.fraienvoi;
    if(moneyTransferFormObject.montantremise ===undefined){

      moneyTransferMADFormObjectClone.montantremise = 0;
    }else{
        moneyTransferMADFormObjectClone.montantremise = moneyTransferFormObject.montantremise;
    }

    moneyTransferMADFormObjectClone.montantOperation = moneyTransferFormObject.montanttransfere;
    moneyTransferMADFormObjectClone.motif = moneyTransferFormObject.motiftransfert;


    moneyTransferMADFormObjectClone.fraiinclus = moneyTransferFormObject.fraiinclus;
    moneyTransferMADFormObjectClone.fraisenvois = fraisenvois;
    moneyTransferMADFormObjectClone.motif = moneyTransferFormObject.motiftransfert;
    return {
        types: [VALIDER_STEP, VALIDER_STEP_SUCCESS, VALIDER_STEP_FAIL],
        promise: (client) => moneyTransferMADFormObjectClone.id === '' || typeof moneyTransferMADFormObjectClone.id == 'undefined' ?
            client.post('transfertargentBNIF/save', {data: objectToParams(moneyTransferMADFormObjectClone)})
            :
            client.post('demandeTransfertArgentOld/update', {data: objectToParams(moneyTransferMADFormObjectClone)})


    };
}


//UPDATE_TRANSFERT
export function UpdateTransfert(id, values,idVilleBen) {


    const moneyTransferMADFormObjectClone = values;

    //info emeteur
    moneyTransferMADFormObjectClone.numeroPieceIdentiteemeteur = values.numeroPieceIdentiteemeteur;
    moneyTransferMADFormObjectClone.numtelemetteur = values.numtelemetteur;
    moneyTransferMADFormObjectClone.nomemetteur = values.nomemetteur;
    moneyTransferMADFormObjectClone.prenomemeteur = values.prenomemeteur;
    moneyTransferMADFormObjectClone.emailemeteur = values.emailemetteur;
    moneyTransferMADFormObjectClone.villeemetteur = values.villeemetteur;
    moneyTransferMADFormObjectClone.paysemetteur = values.paysemetteur;

    //info beneficiare
    moneyTransferMADFormObjectClone.numeroPieceIdentitebeneficiare = values.numeroPieceIdentitebeneficiare;
    moneyTransferMADFormObjectClone.numerotelebeneficiare = values.numerotelebeneficiare;
    moneyTransferMADFormObjectClone.nombeneficiare = values.nombeneficiare;
    moneyTransferMADFormObjectClone.prenombeneficiare = values.prenombeneficiare;
    moneyTransferMADFormObjectClone.emailbeneficiare = values.emailbeneficiare;
    moneyTransferMADFormObjectClone.villebeneficiare = idVilleBen;
    moneyTransferMADFormObjectClone.paysbeneficiare = values.paysbeneficiare;
    //transfert client clinet
    moneyTransferMADFormObjectClone.id = id;
    moneyTransferMADFormObjectClone.fraiinclus = values.fraiinclus;
    moneyTransferMADFormObjectClone.montantremise = values.montantremise;
    moneyTransferMADFormObjectClone.montantOperation = values.montanttransfere;
    moneyTransferMADFormObjectClone.motif = values.motif;
    moneyTransferMADFormObjectClone.fraisenvois = values.fraisenvois;
    return {
        types: [UPDATE_TRANSFERT, UPDATE_TRANSFERT_SUCCESS, UPDATE_TRANSFERT_FAIL],
        promise: (client) => client.post('transfertargentBNIF/update', {data: objectToParams(moneyTransferMADFormObjectClone)})
    };
}


//PAYER_TRANSFERT

export function payerTransfert(transfertDetails, values) {

    const moneyTransferMADFormObjectClone = transfertDetails;
    moneyTransferMADFormObjectClone.numeroPieceIdentiteemeteur = transfertDetails.numeroPieceIdentiteemeteur;

    moneyTransferMADFormObjectClone.numeroPieceIdentitepercepteur = values.numeroPieceIdentitepercepteur;
    moneyTransferMADFormObjectClone.numtelpercepteur = values.numtelpercepteur;
    moneyTransferMADFormObjectClone.nompercepteur = values.nompercepteur;
    moneyTransferMADFormObjectClone.prenompercepteur = values.prenompercepteur;
    moneyTransferMADFormObjectClone.emailpercepteur = values.emailpercepteur;
    moneyTransferMADFormObjectClone.adressepercepteur = values.adressepercepteur;
    moneyTransferMADFormObjectClone.villepercepteur = values.villepercepteur;
    moneyTransferMADFormObjectClone.codepostalpercepteur = values.codepostalpercepteur;
    moneyTransferMADFormObjectClone.payspercepteur = values.payspercepteur;
    moneyTransferMADFormObjectClone.lientbeneficiaire = values.lientbeneficiaire;

    return {
        types: [PAYER_TRANSFERT, PAYER_TRANSFERT_SUCCESS, PAYER_TRANSFERT_FAIL],
        promise: (client) => client.post('transfertargentBNIF/PaiementTransfert', {data: objectToParams(moneyTransferMADFormObjectClone)})


    };
}

export function saveexisteclient(moneyTransferFormObject, clientbenDetails, values, fraisenvois) {


    const moneyTransferMADFormObjectClone = moneyTransferFormObject;
    //info emeteur
    moneyTransferMADFormObjectClone.numeroPieceIdentiteemeteur = moneyTransferFormObject.numeroPieceIdentiteemeteur;
    moneyTransferMADFormObjectClone.numtelemetteur = moneyTransferFormObject.numtelemetteur;
    moneyTransferMADFormObjectClone.nomemetteur = moneyTransferFormObject.nomemetteur;
    moneyTransferMADFormObjectClone.prenomemeteur = moneyTransferFormObject.prenomemeteur;
    moneyTransferMADFormObjectClone.emailemeteur = moneyTransferFormObject.emailemetteur;
    moneyTransferMADFormObjectClone.villeemeteur = moneyTransferFormObject.villeemetteur;
    moneyTransferMADFormObjectClone.paysemetteur = moneyTransferFormObject.paysemetteur;

    //info beneficiare
    moneyTransferMADFormObjectClone.numeroPieceIdentitebeneficiare = clientbenDetails.numeroPieceIdentitebeneficiare;
    moneyTransferMADFormObjectClone.numerotelebeneficiare = clientbenDetails.numerotelephone;
    moneyTransferMADFormObjectClone.nombeneficiare = clientbenDetails.nombeneficiare;
    moneyTransferMADFormObjectClone.prenombeneficiare = clientbenDetails.prenombeneficiare;
    moneyTransferMADFormObjectClone.emailbeneficiare = clientbenDetails.emailbeneficiare;
    moneyTransferMADFormObjectClone.villebeneficiare = clientbenDetails.villebeneficiare;
    moneyTransferMADFormObjectClone.paysbeneficiare = clientbenDetails.paysbeneficiare;


    moneyTransferMADFormObjectClone.fraiinclus = values.fraiinclus;
    moneyTransferMADFormObjectClone.fraisenvois = fraisenvois;
    moneyTransferMADFormObjectClone.montantremise = values.montantremise;
    moneyTransferMADFormObjectClone.montantOperation = values.montanttransfere;
    moneyTransferMADFormObjectClone.motif = values.motiftransfert;


    return {
        types: [SAVE_CLIENT, SAVE_CLIENT_SUCCESS, SAVE_CLIENT_FAIL],
        promise: (client) => client.post('transfertargentBNIF/save', {data: objectToParams(moneyTransferMADFormObjectClone)})


    };
}

export function sentMessage(telephone, sms) {



    const params = {
        telephone: telephone,
        sms: sms,
    };

    return {
        types: [SMS, SMS_SUCCESS, SMS_FAIL],
        promise: (client) => client.post('sms/send', {data: objectToParams(params)})


    };
}


export function loadReference(reference) {

    const params = {
        reference: reference,
    };
    return {
        types: [FIND_TRANSFERT, FIND_TRANSFERT_SUCCESS, FIND_TRANSFERT_FAIL],
        promise: (client) => client.get('transfertargentBNIF/Paiement/' + reference)

    };
}


export function loadListTelClient(numeroidentite) {

    const params = {
        numeroidentite: numeroidentite,
    };
    return {
        types: [FIND_TEL, FIND_TEL_SUCCESS, FIND_TEL_FAIL],
        promise: (client) => client.get('clienttransfert/Clientemeteur?numeroidentite=' + numeroidentite)

    };
}

export function loadListidentiteClient(numerotelephone) {

    const params = {
        numerotelephone: numerotelephone,
    };
    return {
        types: [FIND_IDENTITE, FIND_IDENTITE_SUCCESS, FIND_IDENTITE_FAIL],
        promise: (client) => client.get('clienttransfert/listidentiteClient?numerotelephone=' + numerotelephone)

    };
}

export function loadListTelClientben(numeroidentite) {

    const params = {
        numeroidentite: numeroidentite,
    };
    return {
        types: [FIND_TEL_BEN, FIND_TEL_BEN_SUCCESS, FIND_TEL_BEN_FAIL],
        promise: (client) => client.get('clienttransfert/Clientebeneficiare?numeroidentite=' + numeroidentite)

    };
}


export function loadClient(numerotelephone) {


    return {
        types: [FIND_CLIENT, FIND_CLIENT_SUCCESS, FIND_CLIENT_FAIL],
        promise: (client) => client.get('clienttransfert/Clientexiste?numeroEmetteurTelephone=' + numerotelephone)

    };
}
export function loadClients(numerotelephone,numeroBeneficiaireTelephone) {


    return {
        types: [FIND_CLIENT, FIND_CLIENT_SUCCESS, FIND_CLIENT_FAIL],
        promise: (client) => client.get('clienttransfert/Clientexiste?numeroEmetteurTelephone=' + numerotelephone+ '&numeroBeneficiaireTelephone='+numeroBeneficiaireTelephone)

    };
}




export function send(moneyTransferFormObject) {
    return {
        types: [SEND, SEND_SUCCESS, SEND_FAIL],
        promise: (client) => client.get('sendOtpSms/send?id=' + moneyTransferFormObject.id)
    };
}

export function changeTabsState(isActive) {
    return {type: CHANGE_TABS_ETAT, isActive};
}

export function signerDemande(moneyTransFormObject, taskId, password) {
    const params = {
        id: moneyTransFormObject.id,
        format: 'json',
        jpassword: password,
        taskId,
        version: moneyTransFormObject.version
    };
    return {
        types: [SIGNER_MONEYTRANSFER, SIGNER_MONEYTRANSFER_SUCCESS, SIGNER_MONEYTRANSFER_FAIL],
        promise: (client) => client.get('demandeTransfertArgent/signer?' + objectToParams(params))
    };
}

export function abandonnerDemande(objectForm) {
    const params = {id: objectForm.id, taskId: objectForm.taskId};
    return {
        types: [ABANDONNER_MONEYTRANSFER, ABANDONNER_MONEYTRANSFER_SUCCESS, ABANDONNER_MONEYTRANSFER_FAIL],
        promise: (client) => client.get('demandeTransfertArgent/abandonnerPhaseSignature?' + objectToParams(params))
    };
}

export function setToInitialState(listLcnRequest) {
    let list;
    if (listLcnRequest) {
        list = false;
    } else {
        list = true;
    }
    return {type: INITIAL_STATE, list};
}

export function setState(Request) {
    let list;
    if (Request) {
        list = true;
    }
    else {
        list = true;
    }
    return {type: TRANSFER_STATE, list};
}

export function setStateGAB(Request) {
    let list;
    if (Request) {
        list = false;
    }
    return {type: TRANSFER_STATE, list};
}

export function loadPageShow(id, location) {
    let taskId = '';
    if (location.search && location.search.split('=')) {
        taskId = location.search.split('=')[1].split('&')[0];
    }
    return {
        types: [LOAD_PAGE_SHOW, LOAD_PAGE_SHOW_SUCCESS, LOAD_PAGE_SHOW_FAIL],
        promise: (client) => client.get('demandeTransfertArgent/showParams/' + id + '?taskId=' + taskId),
        taskId,
        id,
    };
}

export function getInstance(id) {
    return {
        types: [GET_INSTANCE, GET_INSTANCE_SUCCESS, GET_INSTANCE_FAIL],
        promise: (client) => client.get('transfertargentBNIF/instance/' + id),
    };
}

export function getInstanceTransfert(id) {
    return {
        types: [GET_INSTANCE, GET_INSTANCE_SUCCESS, GET_INSTANCE_FAIL],
        promise: (client) => client.get('transfertargentBNIF/instance/' + id),
    };
}

export function createAvisTransfertPDF(id) {
    return {
        types: [GET_PDF, GET_PDF_SUCCESS, GET_PDF_FAIL],
        promise: (client) => client.get('transfertargentBNIF/createAvisTransfertPDF/' + id),
    };
}

export function loadCollaborateur(id) {
    return {
        types: [GET_INSTANCE_COLLABORATEUR, GET_INSTANCE_COLLABORATEUR_SUCCESS, GET_COLLABORATEUR_INSTANCE_FAIL],
        promise: (client) => client.get('caissetransfertargent/instance/' + id),

    };
}

export function loadfraisenvois(montanttransfere) {
    return {
        types: [GET_FRAIS_ENVOIS, GET_FRAIS_ENVOIS_SUCCESS, GET_FRAIS_ENVOIS_FAIL],
        promise: (client) => client.get('transfertargentBNIF/Fraienvois/' + montanttransfere),

    };
}

export function loadmontantremise(montanttransfere, montantremise) {

    return {
        types: [MONTANT_REMISE_ENVOIS, MONTANT_REMISE_SUCCESS, MONTANT_REMISE_FAIL],
        promise: (client) => client.get('transfertargentBNIF/Montantremise/' + montanttransfere + '/' + montantremise),

    };
}


export function setView(view) {
    if (view === 'grid') {
        return {type: VIEW_GRID};
    }
    return {type: VIEW_DETAIL};
}

export function showBouttonAnnuler(isShow) {
    return {type: SHOW_BOUTTON_ANNULER, isShow};
}

export function showBouttonSupprimer(isShow) {
    return {type: SHOW_BOUTTON_SUPPRIMER, isShow};
}

export function annulerDemande(id) {
    return {
        types: [ANNULER_DEMANDE, ANNULER_DEMANDE_SUCCESS, ANNULER_DEMANDE_FAIL],
        promise: (client) => client.get('demandeTransfertArgent/annulerDemande?id=' + id + '&format=json'),
    };
}

export function supprimerDemande(id) {
    return {
        types: [SUPPRIMER_DEMANDE, SUPPRIMER_DEMANDE_SUCCESS, SUPPRIMER_DEMANDE_FAIL],
        promise: (client) => client.get('demandeTransfertArgent/annulerDemande?id=' + id + '&format=json'),
    };
}



export function getjourferies() {
    return {
        types: [GET_JOURFIRIES, GET_JOURFIRIES_SUCCESS, GET_JOURFIRIES_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getJoursFeries?_dc=1477058292818'),
    };
}

export function getmaxdate() {
    return {
        types: [GET_MAXDATE, GET_MAXDATE_SUCCESS, GET_MAXDATE_FAIL],
        promise: (client) => client.get('demandeTransfertArgent/createParams'),
    };
}

export function loadStatuts() {
    return {
        types: [LOAD_STATUTS, LOAD_STATUTS_SUCCESS, LOAD_STATUTS_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getStatuts')
    };
}

export function loadListMoneyTransferAgence(ncompteRecherche, dateDebutRecherche, dateFinRecherche, statutRecherche, identifiantDemandeRecheche, montantMin, montantMax, typeTransfert) {
    const currentPage = initialState.currentPage;
    const DateDebutInitial = new Date(dateDebutRecherche);
    let DateDebut = formatDate(DateDebutInitial);
    let DateFin;
    if (dateFinRecherche !== null) {
        const DateFinInitial = new Date(dateFinRecherche);
        DateFin = formatDate(DateFinInitial);
    }

    if (DateDebut === 'NaN-NaN-NaN') {
        DateDebut = '';
    }
    if (DateFin === 'NaN-NaN-NaN' || dateFinRecherche === null) {
        DateFin = '';
    }
    if (isNaN(montantMax)) {
        montantMax = '';
    }
    if (isNaN(montantMin)) {
        montantMin = '';
    }
    return {
        types: [LOAD_LIST_MONEYTRANSFER, LOAD_LIST_MONEYTRANSFER_SUCCESS, LOAD_LIST_MONEYTRANSFER_FAIL],
        ncompteRecherche,
        dateDebutRecherche,
        dateFinRecherche,
        statutRecherche,
        identifiantDemandeRecheche,
        montantMin,
        montantMax,
        typeTransfert,
        promise: (client) => client.get('demandeTransfertArgent/listObject?_dc=1477498186097&ncompteRecherche=' +
            ncompteRecherche + '&dateDebutRecherche=' + DateDebut + '&dateFinRecherche=' + DateFin + '&montantMin=' +
            montantMin + '&montantMax=' + montantMax + '&typeTransfert=' + typeTransfert + '&statutRecherche=' + statutRecherche + '&identifiantDemandeRecheche=' +
            identifiantDemandeRecheche + '&page=' + currentPage + '&offset=0&max=10')
    };
}

export function changeIndexPage(index, ncompteRecherche, dateDebutRecherche, dateFinRecherche, montantMin, montantMax, statutRecherche, identifiantDemandeRecheche, typeTransfert) {
    const DateDebutInitial = new Date(dateDebutRecherche);
    let DateDebut = formatDate(DateDebutInitial);
    const DateFinInitial = new Date(dateFinRecherche);
    let DateFin = formatDate(DateFinInitial);
    if (DateDebut === 'NaN-NaN-NaN') {
        DateDebut = '';
    }
    if (DateFin === 'NaN-NaN-NaN') {
        DateFin = '';
    }
    return {
        types: [LOAD_LIST_MONEYTRANSFER, LOAD_LIST_MONEYTRANSFER_SUCCESS, LOAD_LIST_MONEYTRANSFER_FAIL],
        index,
        ncompteRecherche,
        dateDebutRecherche,
        dateFinRecherche,
        statutRecherche,
        identifiantDemandeRecheche,
        montantMin,
        montantMax,
        typeTransfert,
        promise: (client) => client.get('demandeTransfertArgent/listObject?_dc=1477498186097&ncompteRecherche=' +
            ncompteRecherche + '&dateDebutRecherche=' + DateDebut + '&dateFinRecherche=' + DateFin + '&montantMin=' +
            montantMin + '&montantMax=' + montantMax + '&typeTransfert=' + typeTransfert + '&statutRecherche=' + statutRecherche + '&identifiantDemandeRecheche=' +
            identifiantDemandeRecheche + '&page=' + index + '&offset=0&max=10')
    };
}


export function formatDate(dateToFormat) {
    let day = dateToFormat.getDate();
    let month = dateToFormat.getMonth() + 1;
    const year = dateToFormat.getFullYear();
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }
    const DateDebut = day + '-' + month + '-' + year;
    return DateDebut;
}



/* get list of transfer services for transfert to partener  */

export function loadServiceTransfert() {
    return {
        types: [LOAD_SERVICE_TRANSFER, LOAD_SERVICE_TRANSFER_SUCCESS, LOAD_SERVICE_TRANSFER_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getServiceTransfert')
    };
}

/* initilize the values of form  */

export function initializeForm() {
    return {
        type: RESET,
    };
}

// OPEN AGENCE
export function loadAgence(id) {
    return {
        types: [LOAD_AGENCE_TRANSFER, LOAD_AGENCE_TRANSFER_SUCCESS, LOAD_AGENCE_TRANSFER_FAIL],
        promise: (client) => client.get('agencetransfertargent/agence/' + id)
    };
}

export function loadSeuilSoldeAgence(id) {
    return {
        types: [LOAD_SEUIL_SOLDE_AGENCE_TRANSFER, LOAD_SEUIL_SOLDE_AGENCE_TRANSFER_SUCCESS, LOAD_SEUIL_SOLDE_AGENCE_TRANSFER_FAIL],
        promise: (client) => client.get('agencetransfertargent/seuilSoldeAgence/' + id)
    };
}


export function loadCaisse(idCaisse, isNumCaisse) {
    return {
        types: [LOAD_CAISSE_TRANSFER, LOAD_CAISSE_TRANSFER_SUCCESS, LOAD_CAISSE_TRANSFER_FAIL],
        promise: (client) => client.get('agentcaissetransfertargent/' + (isNumCaisse ? 'caisseByNum/' : 'caisse/') + idCaisse)
    };
}

export function loadCaisseClotureForce(idCaisse, isNumCaisse) {
    return {
        types: [LOAD_CAISSE_TRANSFER, LOAD_CAISSE_TRANSFER_SUCCESS, LOAD_CAISSE_TRANSFER_FAIL],
        promise: (client) => client.get('agentcaissetransfertargent/' + (isNumCaisse ? 'caisseByNumF/' : 'caisse/') + idCaisse)
    };
}
export function loadCaissePartenaire(idCaisse) {
    return {
        types: [LOAD_CAISSE_TRANSFER_PARTENAIRE, LOAD_CAISSE_TRANSFER_PARTENAIRE_SUCCESS, LOAD_CAISSE_TRANSFER_PARTENAIRE_FAIL],
        promise: (client) => client.get('compteGarantie/caisse/'+ idCaisse)
    };
}
export function VerificationSeuilSoldeCaisse(idCaisse) {
    return {
        types: [LOAD_SEUIL_SOLDE_CAISSE_TRANSFER, LOAD_SEUIL_SOLDE_CAISSE_TRANSFER_SUCCESS, LOAD_SEUIL_SOLDE_CAISSE_TRANSFER_FAIL],
        promise: (client) => client.get('agentcaissetransfertargent/seuilcaisse/' +  idCaisse)
    };
}


export function changeEtatAgence(agence) {

    agence.etatAgence = agence.etatAgence === "OPEN" ? "CLOSE" : "OPEN";

    return {
        types: [OPEN_AGENCE_TRANSFER, OPEN_AGENCE_TRANSFER_SUCCESS, OPEN_AGENCE_TRANSFER_FAIL],
        promise: (client) => client.post('agencetransfertargent/changeEtatAgence', {data: objectToParams(agence)})
    };
}

export function loadListReferences(numerotelephone) {
    if (numerotelephone == undefined) {
        return;
    }
    return {
        types: [LOAD_LIST_REFERENCES, LOAD_LIST_REFERENCES_SUCCESS, LOAD_LIST_REFERENCES_FAIL],
        promise: (client) => client.get('transfertargentBNIF/listReference?numerotelephone=' + numerotelephone)
    };
}

export function openCaisse(numerocaisse) {
    return {
        types: [OPEN_CAISSE, OPEN_CAISSE_SUCCESS, OPEN_CAISSE_FAIL],
        promise: (client) => client.get('caissetransfertargent/openCaisse/' + numerocaisse)
    };
}

export function closeCaisse(caisse,dateDebut,DateFin) {
    return {
        types: [CLOSE_CAISSE_TRANSFER, CLOSE_CAISSE_TRANSFER_SUCCESS, CLOSE_CAISSE_TRANSFER_FAIL],
        promise: (client) => client.get('caissetransfertargent/closeCaisse/' + caisse.id)
    };
}
export function closeForceCaisse(caisse,dateDebut,DateFin) {
    return {
        types: [CLOSE_CAISSE_TRANSFER, CLOSE_CAISSE_TRANSFER_SUCCESS, CLOSE_CAISSE_TRANSFER_FAIL],
        promise: (client) => client.get('caissetransfertargent/closeForceCaisse/' + caisse.id)
    };
}

export function getListCaisseByAgence(isOpen, idAgence) {
    return {
        types: [LOAD_LIST_CAISSE_TRANSFER, LOAD_LIST_CAISSE_TRANSFER_SUCCESS, LOAD_LIST_CAISSE_TRANSFER_FAIL],
        promise: (client) => client.get('caissetransfertargent/' + (isOpen ? 'getListOpenedCaisses' : 'getListCaisses') + '/' + idAgence)
    };
}


export function loadListTransfertAgence(codeagence, codeCaisse, typeTransfert, dateTransfertDebut, dateTransfertFin, nomEmetteur,numEmetteur, nomBenif, montantMin, montantMax, agent, statut) {
    return {
        types: [LOAD_LIST_TRANSFER, LOAD_LIST_TRANSFER_SUCCESS, LOAD_LIST_TRANSFER_FAIL],
        promise: (client) => client.get('agencetransfertargent/agence/' + codeagence + '/listtransfert?codeCaisse=' + codeCaisse + '&typeTransfert=' + typeTransfert + '&dateTransfertDebut=' + dateTransfertDebut + '&dateTransfertFin=' + dateTransfertFin + '&nomEmetteur=' + nomEmetteur + '&numerotelephone='+numEmetteur+'&nomBenif=' + nomBenif + '&montantMin=' + montantMin + '&montantMax=' + montantMax + '&agent=' + agent + '&statut=' + statut)
    };
}
export function loadListTransfertcomptegarantie(codeagence, codeCaisse, typeTransfert, dateTransfertDebut, dateTransfertFin, nomEmetteur, nomBenif, montantMin, montantMax, agent, statut) {
    return {
        types: [LOAD_LIST_TRANSFER, LOAD_LIST_TRANSFER_SUCCESS, LOAD_LIST_TRANSFER_FAIL],
        promise: (client) => client.get('compteGarantie/agence/' + codeagence + '/listtransfert?codeCaisse=' + codeCaisse + '&typeTransfert=' + typeTransfert + '&dateTransfertDebut=' + dateTransfertDebut + '&dateTransfertFin=' + dateTransfertFin + '&nomEmetteur=' + nomEmetteur + '&nomBenif=' + nomBenif + '&montantMin=' + montantMin + '&montantMax=' + montantMax + '&agent=' + agent + '&statut=' + statut)
    };
}
export function verificationvalidationattribution(codeagence, typeTransfert, dateTransfertDebut, dateTransfertFin) {
    return {
        types: [VERIFICATION_EXISTENCE_ATTRIBUTION, VERIFICATION_EXISTENCE_ATTRIBUTION_SUCCESS, VERIFICATION_EXISTENCE_ATTRIBUTION_FAIL],
        promise: (client) => client.get('agencetransfertargent/agence/' + codeagence + '/verification?typeTransfert=' + typeTransfert + '&dateTransfertDebut=' + dateTransfertDebut + '&dateTransfertFin=' + dateTransfertFin)
    };
}


export function loadListTransfertbyCaisse(codeagence, codeCaisse, typeTransfert, dateTransfertDebut, dateTransfertFin, montantMin, montantMax, statut) {
    return {
        types: [LOAD_CAISSE_LIST_TRANSFER, LOAD_CAISSE_LIST_TRANSFER_SUCCESS, LOAD_CAISSE_LIST_TRANSFER_FAIL],
        promise: (client) => client.get('agentcaissetransfertargent/caisse/' + codeagence + '/listtransfert?codeCaisse=' + codeCaisse + '&typeTransfert=' + typeTransfert + '&dateTransfertDebut=' + dateTransfertDebut + '&dateTransfertFin=' + dateTransfertFin + '&montantMin=' + montantMin + '&montantMax=' + montantMax + '&statut=' + statut)
    };
}

export function loadListAgncAgncTransfertAgence(idAgence, typeTransfert, agenceEchange, dateReceptionDebut, dateReceptionFin, dateEmissionDebut, dateEmissionFin, montantOperationDebut, montantOperationFin) {
    return {
        types: [LOAD_LIST_TRANSFER_AGNC_AGNC, LOAD_LIST_TRANSFER_AGNC_AGNC_SUCCESS, LOAD_LIST_TRANSFER_AGNC_AGNC_FAIL],
        promise: (client) => client.get('agencetransfertargent/agence/' + idAgence + '/listagenceagencetransfert?typeTransfert=' + typeTransfert + '&agenceEchange=' + agenceEchange + '&dateReceptionDebut=' + dateReceptionDebut + '&dateReceptionFin=' + dateReceptionFin + '&dateEmissionDebut=' + dateEmissionDebut + '&dateEmissionFin=' + dateEmissionFin + '&montantOperationDebut=' + montantOperationDebut + '&montantOperationFin=' + montantOperationFin)
    };
}


export function attributionAgenceCaisse(idAgence, numCaisse, montant,dateDebut,dateFin) {

    const obj = {
        idAgence,
        numCaisse,
        montant,
    }
    return {
        types: [ATTRIBUTION_LIST_TRANSFER, ATTRIBUTION_LIST_TRANSFER_SUCCESS, ATTRIBUTION_LIST_TRANSFER_FAIL],
        promise: (client) => client.post('agencetransfertargent/attribution?dateDebut=' + dateDebut + '&dateFin=' + dateFin , {data: objectToParams(obj)})
    };
}

export function valideattributionAgenceCaisse(idAgence, numCaisse, montant,idTransfert) {

    const obj = {
        idAgence,
        numCaisse,
        montant,
        idTransfert
    }
    return {


        types: [VALIDER_ATTRIBUTION_LIST_TRANSFER, VALIDER_ATTRIBUTION_LIST_TRANSFER_SUCCESS, VALIDER_ATTRIBUTION_LIST_TRANSFER_FAIL],
        promise: (client) => client.post('agencetransfertargent/valideAttribution/', {data: objectToParams(obj)})
    };
}


export function refusionattributionAgenceCaisse(idAgence, numCaisse, montant,idTransfert) {

    const obj = {
        idAgence,
        numCaisse,
        montant,
        idTransfert
    }
    return {


        types: [REFUSION_ATTRIBUTION_LIST_TRANSFER, REFUSION_ATTRIBUTION_LIST_TRANSFER_SUCCESS, REFUSION_ATTRIBUTION_LIST_TRANSFER_FAIL],
        promise: (client) => client.post('agencetransfertargent/refusionAttribution/', {data: objectToParams(obj)})
    };
}

export function miseadispoAgenceCaisse(idAgence, numCaisse, montant) {
    const obj = {
        idAgence,
        numCaisse,
        montant
    }
    return {
        types: [MISEADISPO_LIST_TRANSFER, MISEADISPO_LIST_TRANSFER_SUCCESS, MISEADISPO_LIST_TRANSFER_FAIL],
        promise: (client) => client.post('caissetransfertargent/miseadisposition/', {data: objectToParams(obj)})
    };
}

export function alimentationDepotGarantie(idAgence,idCaisse,numCaisse,idPartenaire,montant) {
    const obj = {
        idAgence,
        idCaisse,
        numCaisse,
        idPartenaire,
        montant
    }
    return {
        types: [ALIMENTATION, ALIMENTATION_SUCCESS, ALIMENTATION_FAIL],
        promise: (client) => client.post('compteGarantie/alimentation/', {data: objectToParams(obj)})
    };
}

export function retraitDepotGarantie(idAgence,idCaisse,numCaisse,idPartenaire,montant) {
    const obj = {
        idAgence,
        idCaisse,
        numCaisse,
        idPartenaire,
        montant
    }
    return {
        types: [RETRAIT, RETRAIT_SUCCESS, RETRAIT_FAIL],
        promise: (client) => client.post('compteGarantie/retrait/', {data: objectToParams(obj)})
    };
}


export function validationMiseadispositionAgence(idAgence, numCaisse, montant,idTransfert) {
    const obj = {
        idAgence,
        numCaisse,
        idTransfert,
        montant
    }
    return {
        types: [VALIDER_MISEADISPO_LIST_TRANSFER, VALIDER_MISEADISPO_LIST_TRANSFER_SUCCESS, VALIDER_MISEADISPO_LIST_TRANSFER_FAIL],
        promise: (client) => client.post('caissetransfertargent/validationMiseadisposition/', {data: objectToParams(obj)})
    };
}


export function refussionMiseadispositionAgence(idAgence, numCaisse, montant,idTransfert) {
    const obj = {
        idAgence,
        numCaisse,
        idTransfert,
        montant
    }
    return {
        types: [REFUSER_MISEADISPO_LIST_TRANSFER, REFUSER_MISEADISPO_LIST_TRANSFER_SUCCESS, REFUSER_MISEADISPO_LIST_TRANSFER_FAIL],
        promise: (client) => client.post('caissetransfertargent/refusionMiseadisposition/', {data: objectToParams(obj)})
    };
}
export function miseadispoAgenceAgence(idAgenceSrc, idAgenceDest, montant, motif) {
    const obj = {
        idAgenceSrc,
        idAgenceDest,
        montant,
        motif
    }
    return {
        types: [MISEADISPO_AGENCE_TRANSFER, MISEADISPO_AGENCE_TRANSFER_SUCCESS, MISEADISPO_AGENCE_TRANSFER_FAIL],
        promise: (client) => client.post('agencetransfertargent/miseadispositionagence/', {data: objectToParams(obj)})
    };
}


export function encaissementAgenceAgence(idAgenceSrc, idAgenceDest, reference, montant) {
    const obj = {
        idAgenceSrc,
        idAgenceDest,
        reference,
        montant
    }
    return {
        types: [ENCAISSEMENT_AGENCE_TRANSFERT, ENCAISSEMENT_AGENCE_TRANSFERT_SUCCESS, ENCAISSEMENT_AGENCE_TRANSFERT_FAIL],
        promise: (client) => client.post('agencetransfertargent/encaissement/', {data: objectToParams(obj)})
    };
}

export function loadListAgence() {
    return {
        types: [LOAD_LIST_AGENCE, LOAD_LIST_AGENCE_SUCCESS, LOAD_LIST_AGENCE_FAIL],
        promise: (client) => client.get('agencetransfertargent/agences')
    };
}

export function loadListMiseADipositionAgence(agenceReception, agenceEnvoi, dateOperation) {
    return {
        types: [LOAD_LIST_MISE_A_DISPO_AGENCE, LOAD_LIST_MISE_A_DISPO_AGENCE_SUCCESS, LOAD_LIST_MISE_A_DISPO_AGENCE_FAIL],
        promise: (client) => client.get('agencetransfertargent/agence/listiseadispositionagence?agenceReception=' + agenceReception + '&agenceEnvoi=' + agenceEnvoi + '&dateOperation=' + dateOperation)
    };
}


export function deleteTransfertCltCltByID(id) {
    return {
        types: [DELETE_TRANSFERT_CLT_CLT, DELETE_TRANSFERT_CLT_CLT_SUCCESS, DELETE_TRANSFERT_CLT_CLT_FAIL],
        promise: (client) => client.get('agentcaissetransfertargent/transfert/delete/' + id)
    };
}

export function annulerTransfertCltCltByID(numCaisse, idTransfert) {
    return {
        types: [CANCEL_TRANSFERT_CLT_CLT, CANCEL_TRANSFERT_CLT_CLT_SUCCESS, CANCEL_TRANSFERT_CLT_CLT_FAIL],
        promise: (client) => client.get('agentcaissetransfertargent/caisse/' + numCaisse + '/transfert/cancel/' + idTransfert)
    };
}

export function signeTransfertCltCltByID(id) {
    return {
        types: [SIGNE_TRANSFERT_CLT_CLT, SIGNE_TRANSFERT_CLT_CLT_SUCCESS, SIGNE_TRANSFERT_CLT_CLT_FAIL],
        promise: (client) => client.get('agentcaissetransfertargent/transfert/signe/' + id)
    };
}
export function signeTransfertChefAgenceByID(id) {
    return {
        types: [SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT, SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT_SUCCESS, SIGNE_CHEFAGENCE_TRANSFERT_CLT_CLT_FAIL],
        promise: (client) => client.get('agentcaissetransfertargent/transfert/signeChefAgence/' + id)
    };
}
export function signePaiementChefAgenceByID(id) {
    return {
        types: [SIGNE_CHEFAGENCE_PAIEMENT_CLT_CLT, SIGNE_CHEFAGENCE_PAIEMENT_CLT_CLT_SUCCESS, SIGNE_CHEFAGENCE_PAIEMNT_CLT_CLT_FAIL],
        promise: (client) => client.get('agentcaissetransfertargent/transfert/signaturepaiementChefAgence/' + id)
    };
}
export function loadListCompteComptable(numCompte, typeCompte, dateDebut, dateFin, soldeMin, soldeMax) {
    return {
        types: [LOAD_LIST_COMPTE_COMPTA, LOAD_LIST_COMPTE_COMPTA_SUCCESS, LOAD_LIST_COMPTE_COMPTA_FAIL],
        promise: (client) => client.get('comptabilite/listcompte?numCompte=' + numCompte + '&typeCompte=' + typeCompte + '&dateDebut=' + dateDebut + '&dateFin=' + dateFin + '&soldeMin=' + soldeMin + '&soldeMax=' + soldeMax)
    };
}

export function loadListOuvertureClotureCaisses(idAgence, numcaisse, agentaction, montantinitial, montantfinal, dateActionDebut, dateActionFin, heuredebut, heurefin) {
    return {
        types: [LOAD_LIST_OUVERTURE_CLOTURE, LOAD_LIST_OUVERTURE_CLOTURE_SUCCESS, LOAD_LIST_OUVERTURE_CLOTURE_FAIL],
        promise: (client) => client.get('agencetransfertargent/agence/' + idAgence + '/listouvetturecloture?numcaisse=' + numcaisse + '&agentaction=' + agentaction + '&montantinitial=' + montantinitial + '&montantfinal=' + montantfinal + '&dateActionDebut=' + dateActionDebut + '&dateActionFin=' + dateActionFin + '&heuredebut=' + heuredebut + '&heurefin=' + heurefin)
    };
}

export function loadListPartenaireUser() {
    return {
        types: [LOAD_LIST_PARTENAIRES, LOAD_LIST_PARTENAIRES_SUCCESS, LOAD_LIST_PARTENAIRES_FAIL],
        promise: (client) => client.get('ParametragePartners/Partners')
    };
}

export function switchf() {
    return {type: SWITCHF};
}
export function RestForm() {
	let  form="searchMoneyTransferAgence";
    return {type:"redux-form/RESET",form};
}

export function RestFormN() {
	let  form="NewMoneyTransfer";
    return {type: "redux-form/RESET",form};
}

export function RestFormChef() {
	let  form="searchMoneyTransferAgence";
    return {type:"redux-form/RESET",form};
}
export function resetAlertReference() {
    return {type: RESETREFERENCE};
}


export function resetAlertAlimentation() {
    return {type: RESETALIMENTATION};
}

export function resetAlertValidationChamps() {
    return {type: RESETVALIDE};
}
export function resetAlertSignature() {
    return {type: RESETSIGNE};
}


export function resetAlertSoldeInsufisante() {
    return {type: RESETSOLDE};
}

export function typeTB(tb) {
    return {type: TYPETB, tb};
}
export function generateXLS(codeagence, codeCaisse, typeTransfert, dateTransfertDebut, dateTransfertFin, nomEmetteur,numEmetteur, nomBenif, montantMin, montantMax, agent, statut) {
    return {
        types: [LOAD_TRANSFER],
        promise: (client) => client.post('agencetransfertargent/generateXLS/' + codeagence + '/listtransfert?codeCaisse=' + codeCaisse + '&typeTransfert=' + typeTransfert + '&dateTransfertDebut=' + dateTransfertDebut + '&dateTransfertFin=' + dateTransfertFin + '&nomEmetteur=' + nomEmetteur + '&numerotelephone='+numEmetteur+'&nomBenif=' + nomBenif + '&montantMin=' + montantMin + '&montantMax=' + montantMax + '&agent=' + agent + '&statut=' + statut)
    };
}
