import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Alert,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  annulerTransfertCltCltByID,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { asyncConnect } from "redux-connect";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./moneytransferreducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import Button from "react-bootstrap-button-loader";
import * as UserActions from "../User/UserReducer";
import { AuthorizedComponent } from "react-router-role-authorization";

var idCaisseDT = sessionStorage.getItem("myData1");
var result = sessionStorage.getItem("myData");

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      const promises = [];
      if (result == null) {
        promises.push(
          dispatch(
            MoneyTransferAction.verificationvalidationattribution(
              getState().user.userFrontDetails.agenceId,
              "CATOAG",
              moment(),
              moment()
            )
          )
        );
      } else {
        promises.push(
          dispatch(
            MoneyTransferAction.verificationvalidationattribution(
              result,
              "CATOAG",
              moment(),
              moment()
            )
          )
        );
      }
      if (idCaisseDT == null || idCaisseDT == "" || idCaisseDT == undefined) {
        if (getState().user.userFrontDetails.caisseId != "") {
          promises.push(
            dispatch(
              MoneyTransferAction.loadCaisse(
                getState().user.userFrontDetails.caisseId
              )
            )
          );
        }
      } else {
        promises.push(dispatch(MoneyTransferAction.loadCaisse(idCaisseDT)));
      }

      return Promise.all(promises);
    },
  },
])
@connect(
  (state) => ({
    userFrontDetails: state.user.userFrontDetails,
    caisseObj: state.moneyTransfer.caisseObj,
    successverificationattribution:
      state.moneyTransfer.successverificationattribution,
    successMiseadispo: state.moneyTransfer.successMiseadispo,
    loadingMiseadispo: state.moneyTransfer.loadingMiseadispo,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@translate(["MoneyTransfer"], { wait: true })
export default class Miseadisposition extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      montant: "",
      formValid: "",
    };

    this.setValidity = this.setValidity.bind(this);
    this.handleTotalClick = this.handleTotalClick.bind(this);
    this.handleMontantChange = this.handleMontantChange.bind(this);
    this.MiseadispoAgenceCaisseAsync = this.MiseadispoAgenceCaisseAsync.bind(
      this
    );
  }

  handleMontantChange(event) {
    this.setState({ montant: event.target.value });
    this.setValidity(event.target.value);
  }
  async MiseadispoAgenceCaisseAsync(agenceId, caisseNum, montant) {
    await this.props.miseadispoAgenceCaisse(
      agenceId,
      caisseNum,
      montant,
      moment(),
      moment()
    );
    this.setState({ montant: "0" });
  }
  setValidity(mnt) {
    if (mnt.length > 0) this.setState({ formValid: true });
    else this.setState({ formValid: false });
  }

  handleTotalClick(mnt) {
    this.setState({ montant: mnt });
    this.setValidity(mnt);
  }

  render() {
    const {
      t,
      loadCaisse,
      caisseObj,
      successMiseadispo,
      successverificationattribution,
      loadingMiseadispo,
      miseadispoAgenceCaisse,
      userFrontDetails,
    } = this.props;
    const styles = require("./moneytransfer.scss");

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.titleForm.miseadispositon")} </h2>
          </Col>
        </Row>

        <Row>
          {successverificationattribution === true && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">
                {t("msg.miseadispositionEnValidation")}
              </Alert>
            </Col>
          )}
          {userFrontDetails.caisseId === "" && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{t("msg.aucunecaisseouverte")}</Alert>
            </Col>
          )}
          {successMiseadispo === true && (
            <Col xs="12" md="12">
              <Alert bsStyle="success">
                {t("msg.sucessemiseadisposition")}
              </Alert>
            </Col>
          )}
          {successMiseadispo === false && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{t("msg.ereursolde")}</Alert>
            </Col>
          )}
          {successMiseadispo == "null" && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{t("msg.ereurattribition")}</Alert>
            </Col>
          )}
        </Row>

        <Row className="detailsBloc">
          <Row style={{ marginBottom: "15px" }}>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.dateaujourdui")}:</ControlLabel>
              <p className="detail-value">{moment().format("DD/MM/YYYY")}</p>
            </Col>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.agence")}: </ControlLabel>
              <p className="detail-value">
                {userFrontDetails.caisseId !== "" &&
                caisseObj != null &&
                caisseObj != "undefined"
                  ? caisseObj.nomAgence
                  : userFrontDetails.agenceNom}
              </p>
            </Col>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.agent")}:</ControlLabel>
              <p className="detail-value">
                {userFrontDetails.caisseId !== "" &&
                caisseObj != null &&
                caisseObj != "undefined"
                  ? caisseObj.nomAgent
                  : userFrontDetails.collaborateurNom}
              </p>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.numeroCaisse")}: </ControlLabel>
              <p className="detail-value">
                {userFrontDetails.caisseId !== "" &&
                  caisseObj != null &&
                  caisseObj != "undefined" &&
                  caisseObj.numCaisse}
              </p>
            </Col>
            <Col xs={12} md={4}>
              <ControlLabel> {t("form.label.soldeCaisse")}: </ControlLabel>
              <p className="detail-value">
                {userFrontDetails.caisseId !== "" &&
                  caisseObj != null &&
                  caisseObj != "undefined" &&
                  caisseObj.soldeActuel}{" "}
                XOF
              </p>
            </Col>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.heure")}: </ControlLabel>
              <p className="detail-value">
                {userFrontDetails.caisseId !== "" &&
                  caisseObj != null &&
                  caisseObj != "undefined" &&
                  caisseObj.heureOuvreture}
              </p>
            </Col>
          </Row>
        </Row>

        <Row style={{ padding: "0 10px" }}>
          <form className="formContainer" style={{ padding: "30px 20px 20px" }}>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="2" style={{ paddingTop: "6px" }}>
                <ControlLabel>
                  {t("form.label.montantaatribuer")} :
                </ControlLabel>
              </Col>
              <Col xs="12" md="4">
                <FormControl
                  type="text"
                  className={styles.datePickerFormControl}
                  onChange={this.handleMontantChange}
                  value={this.state.montant}
                />
              </Col>
              <Col xs="12" md="6">
                <Button
                  bsStyle="primary"
                  onClick={() => {
                    this.handleTotalClick(caisseObj.soldeActuel);
                  }}
                  style={{ width: "40%", height: "35px" }}
                >
                  <i className="fa fa-check" />{" "}
                  {t("form.buttons.miseadispositiontotal")}
                </Button>
                <Button
                  loading={loadingMiseadispo}
                  disabled={
                    !this.state.formValid || userFrontDetails.caisseId === ""
                  }
                  onClick={(e) => {
                    this.MiseadispoAgenceCaisseAsync(
                      caisseObj.idAgence,
                      caisseObj.numCaisse,
                      this.state.montant
                    );
                  }}
                  bsStyle="primary"
                  style={{ width: "25%", height: "35px" }}
                >
                  <i className="fa fa-check" /> {t("form.buttons.confirmer")}
                </Button>
                <Button
                  onClick={() => {
                    browserHistory.push(baseUrl + "app/TDBAgentCaisse");
                  }}
                  bsStyle="primary"
                  style={{ width: "25%", height: "35px" }}
                >
                  <i className="fa fa-times" /> {t("form.buttons.cancel")}
                </Button>
              </Col>
            </Row>
          </form>
        </Row>
      </div>
    );
  }
}
