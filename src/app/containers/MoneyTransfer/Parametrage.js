import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Button,
  Row,
  Col,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  PanelGroup,
  Alert,
  Panel,
  Modal,
  ControlLabel,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { browserHistory } from "react-router";
import Griddle from "griddle-react";
import moment from "moment";
import DatePicker from "react-datepicker";
import Wizard from "../Commons/Wizard";
import * as MoneyTransferAction from "./ParametrageReducer";
import VirtualKeyboard from "../../components/VirtualKeyboard/VirtualKeyboard";
import CustomDatePicke from "../../components/CustomDatePicke/CustomDatePicke";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import { reset } from "redux-form";
import { AuthorizedComponent } from "react-router-role-authorization";
import ParametragePaysValidator from "./Validateur/ParametragePaysValidator";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

@connect(
  (state) => ({
    dataForDetail: state.ParametrageReducer.dataForDetail,
    successinstance: state.ParametrageReducer.successinstance,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getInstance: PropTypes.func,
    getInstanceCollaborateur: PropTypes.func,
    showBouttonAnnuler: PropTypes.func,
    showBouttonSupprimer: PropTypes.func,
    deleteTransfertCltCltByID: PropTypes.func,
    loadListTransfertAgence: PropTypes.func,
    annulerTransfertCltCltByID: PropTypes.func,
  };

  constructor() {
    super();
    this.state = {
      updating: false,
      showModal: false,
      deleting: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick() {
    try {
      if (this.state.deleting) {
        await this.props.deletepays(this.props.rowData.id);
        this.props.loadListPays();
      }
      if (this.state.updating) {
        await this.props.getInstance(this.props.rowData.id);
        // this.props.loadListAdresse();
      }
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  render() {
    const {
      saveAdreese,
      loadListPays,
      updatepays,
      listAdresse,
      getInstance,
      deletepays,
      t,
    } = this.props;
    const styles = require("./moneytransfer.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
    };

    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="warning"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: true,
                showModal: true,
                deleting: false,
              })
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>
          <Button
            bsSize="small"
            bsStyle="danger"
            className={styles.actionButtonStyle}
            onClick={() =>
              this.setState({
                updating: false,
                showModal: true,
                deleting: true,
              })
            }
          >
            <i className="fa fa-trash-o fa-fw" />
          </Button>
        </ButtonGroup>

        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {this.state.deleting && <div>{t("popup.supression.title")}</div>}

              {this.state.updating && (
                <div>{t("popup.modification.title")}</div>
              )}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.deleting && <div>{t("popup.supression.msgpay")}</div>}
            {this.state.updating && <div>{t("popup.modification.msgpay")}</div>}
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.supression.noBtn")}
              </Button>
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={this.handleClick}
              >
                {t("popup.supression.yesBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

@asyncConnect([
  {
    deferred: false,
    promise: ({ store: { dispatch } }) => {
      return Promise.all([dispatch(MoneyTransferAction.loadListPays())]);
    },
  },
])
@reduxForm(
  {
    form: "NewMoneyTransfer",
    fields: ["libelle", "abreviation", "indicatif", "action"],
    validate: ParametragePaysValidator,
    destroyOnUnmount: true,
  },
  (state) =>
    state.ParametrageReducer.dataForDetail &&
    state.ParametrageReducer.dataForDetail !== null &&
    state.ParametrageReducer.dataForDetail !== undefined && {
      initialValues: state.ParametrageReducer.dataForDetail,
    }
)
@translate(["ParametrageMoneyTransfer"], { wait: true })
@connect(
  (state) => ({
    user: state.user,
    userFrontDetails: state.user.userFrontDetails,
    saveAdreese: state.ParametrageReducer.saveAdreese,
    updatepays: state.ParametrageReducer.updatepays,
    transfetList: state.moneyTransfer.transfetList,
    etatCaisse: state.moneyTransfer.etatCaisse,
    dataForDetail: state.ParametrageReducer.dataForDetail,
    successinstance: state.ParametrageReducer.successinstance,
    listCaisse: state.moneyTransfer.listCaisse,
    caisseDetails: state.moneyTransfer.caisseDetails,
    listAdresse: state.ParametrageReducer.listAdresse,
    successOpenCaisse: state.moneyTransfer.successOpenCaisse,
    changebutton: state.moneyTransfer.changebutton,
  }),
  { ...MoneyTransferAction, initializeWithKey }
)
export default class Parametrage extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.state = {
      updating: true,
      canceling: false,
      deleting: false,
      numCaisse: "",
      successinstance: false,
      data: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitTWO = this.onSubmitTWO.bind(this);
    this.clearInput = this.clearInput.bind(this);
  }

  clearInput(values) {
    //// this.setState({this.state.initialValues:{}}); /
    /// resetValue();
    values.libelle = "";
    values.abreviation = "";
    valuses.indicatif = "";
    ReactDOM.findDOMNode(this.refs.myInput).focus();
  }

  handleChange(event) {
    this.setState({ numCaisse: event.target.value });
  }

  async onSubmit(values) {
    try {
      await this.props.saveAdreese(values);
      this.props.loadListPays();
    } catch (error) {
      console.log(error.message);
    }
  }

  async onSubmitTWO(values, id) {
    try {
      await this.props.updatepays(values, id);
      this.props.loadListPays();
      this.props.switchf();
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    const {
      fields: { libelle, abreviation, indicatif, action },
      handleSubmit,
      dataForDetail,
      initialValues,
      initialize,
      userFrontDetails,
      successinstance,
      resetForm,
      updating,
      saveAdreese,
      changebutton,
      loadListPays,
      updatepays,
      switchf,
      caisseDetails,
      openCaisse,
      successOpenCaisse,
      listAdresse,
      etatCaisse,
      getListCaisseByAgence,
      values,
      listCaisse,
      t,
    } = this.props;

    const styles = require("./moneytransfer.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const gridMetaData = [
      {
        columnName: "libelle",
        displayName: t("list.cols.libelle"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "abreviation",
        displayName: t("list.cols.abreviation"),
        customHeaderComponent: HeaderComponent,
      },
      {
        columnName: "indicatif",
        displayName: t("list.cols.indicatif"),
        customHeaderComponent: HeaderComponent,
      },

      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        cssClassName: styles.LabelColumnDEvise,
      },
    ];

    console.log(updating);
    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.legend.parametragepay")}</h2>
          </Col>
        </Row>
        <Row className={styles.compteCard}>
          <form className="formContainer">
            <fieldset>
              {/*<legend>{t('form.legend.parametragepay')}</legend>*/}

              {successinstance === true ? (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="4">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.libelle")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libelle}
                      />
                      {libelle.error && libelle.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {libelle.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="4">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.abreviation")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...abreviation}
                      />
                      {abreviation.error && abreviation.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {abreviation.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="4">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.indicatif")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...indicatif}
                      />
                      {indicatif.error && indicatif.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {indicatif.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right">
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmitTWO(values, dataForDetail.id);
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.modifier")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          switchf();
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              ) : (
                <div>
                  <Row className={styles.paddingColumn}>
                    <Col xs="12" md="4">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.libelle")}{" "}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...libelle}
                      />
                      {libelle.error && libelle.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {libelle.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="4">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.abreviation")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...abreviation}
                      />
                      {abreviation.error && abreviation.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {abreviation.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="4">
                      <ControlLabel className={"requiredInput"}>
                        {t("form.label.indicatif")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        className={styles.datePickerFormControl}
                        {...indicatif}
                      />
                      {indicatif.error && indicatif.touched && (
                        <div className={styles.error}>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {indicatif.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right">
                      <Button
                        bsStyle="primary"
                        onClick={handleSubmit((e) => {
                          this.onSubmit(values);
                          resetForm();
                        })}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.ajouter")}
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          resetForm();
                          changebutton(successinstance);
                        }}
                      >
                        <i className="fa fa-times" />{" "}
                        {t("form.buttons.annuler")}
                      </Button>
                    </div>
                  </Row>
                </div>
              )}
            </fieldset>
            <fieldset>
              <legend>{t("form.legend.listepay")}</legend>
              <Row className="table-responsive">
                <Griddle
                  results={listAdresse}
                  columnMetadata={gridMetaData}
                  useGriddleStyles={false}
                  noDataMessage={t("list.search.msg.noResult")}
                  resultsPerPage={10}
                  nextText={<i className="fa fa-chevron-right" />}
                  previousText={<i className="fa fa-chevron-left" />}
                  tableClassName="table"
                  columns={["libelle", "abreviation", "indicatif", "action"]}
                />
              </Row>
            </fieldset>
          </form>
        </Row>
      </div>
    );
  }
}
