import React, { Component } from "react";
import { Col, ControlLabel, Alert, FormControl, Row } from "react-bootstrap";
import * as MoneyTransferAction from "./moneytransferreducer";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import { browserHistory } from "react-router";
import { initializeWithKey } from "redux-form";
import { connect } from "react-redux";
import moment from "moment";
import * as UserActions from "../User/UserReducer";
import Button from "react-bootstrap-button-loader";
import { AuthorizedComponent } from "react-router-role-authorization";

var idCaisseDT = sessionStorage.getItem("myData1");

@asyncConnect([
  {
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];
      if (
        getState().user.userFrontDetails != null &&
        getState().user.userFrontDetails.caisseId != ""
      ) {
        promises.push(
          dispatch(
            MoneyTransferAction.loadCaisse(
              getState().user.userFrontDetails.caisseId
            )
          )
        );
        promises.push(dispatch(UserActions.loadUserFrontDetails()));
      } else {
        promises.push(dispatch(MoneyTransferAction.loadCaisse(idCaisseDT)));
        promises.push(dispatch(UserActions.loadUserFrontDetails()));
      }

      return Promise.all(promises);
    },
  },
])
@connect(
  (state) => ({
    messagevalidationattmise: state.moneyTransfer.messagevalidationattmise,
    loadingCloseCaisse: state.user.loadingCloseCaisse,
    userFrontDetails: state.user.userFrontDetails,
    caisseObj: state.moneyTransfer.caisseObj,
    etatCaisse: state.moneyTransfer.etatCaisse,
    successCloseCaisse: state.moneyTransfer.successCloseCaisse,
  }),
  { ...MoneyTransferAction, ...UserActions, initializeWithKey }
)
@translate(["MoneyTransfer"], { wait: true })
export default class ClotureCaisse extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";

    this.state = {
      err: "",
    };
    this.changeEtatCaisses = this.changeEtatCaisses.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  changeEtatCaisses(caisse, etat) {
    caisse.etatCaisse = etat;
    return true;
  }

  async handleClick(e) {
    try {
      e.preventDefault();
      if (this.props.userFrontDetails.caisseId != "") {
        let callWS = true;
        if (this.props.caisseObj.etatCaisse === "CLOSE") {
          callWS = false;
          this.setState({ err: "La caisse est deja fermé" });
        }
        if (
          this.props.caisseObj.soldeActuel != "0.00" &&
          this.props.caisseObj.soldeActuel != "0"
        ) {
          callWS = false;
          this.setState({
            err:
              "Il faut faire une mise à disposition pour vider la caisse avant de la fermer !!",
          });
        }
        if (callWS) {
          await this.props.closeCaisse(
            this.props.caisseObj,
            moment(),
            moment()
          );
          if (this.props.successCloseCaisse) {
            // sessionStorage.removeItem("myData1",this.props.caisseObj.id);
          }
          //this.props.loadUserFrontDetails();
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  }

  componentWillMount() {
    this.setState({
      err: "",
    });
  }

  render() {
    const {
      t,
      caisseObj,
      messagevalidationattmise,
      closeCaisse,
      successCloseCaisse,
      etatCaisse,
      userFrontDetails,
      loadCaisse,
      loadingCloseCaisse,
      loadUserFrontDetails,
    } = this.props;
    const styles = require("./moneytransfer.scss");

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <h2>{t("form.titleForm.clotureCaisse")}</h2>
          </Col>
        </Row>

        <Row>
          {this.state.err !== "" && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{this.state.err}</Alert>
            </Col>
          )}
          {successCloseCaisse === false && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{messagevalidationattmise}</Alert>
            </Col>
          )}
          {successCloseCaisse &&
            etatCaisse === "CLOSE" &&
            caisseObj != undefined &&
            this.changeEtatCaisses(caisseObj, etatCaisse) && (
              <Col xs="12" md="12">
                <Alert bsStyle="info">{t("msg.sucesscloturecaisse")}</Alert>
              </Col>
            )}
          {!successCloseCaisse && etatCaisse === "null" && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{t("msg.failedcloturecaisse")}</Alert>
            </Col>
          )}
          {userFrontDetails.caisseId === "" && etatCaisse !== "CLOSE" && (
            <Col xs="12" md="12">
              <Alert bsStyle="danger">{t("msg.aucunecaisseouverte")}</Alert>
            </Col>
          )}
        </Row>
        <Row className="detailsBloc">
          <Row style={{ marginBottom: "15px" }}>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.dateaujourdui")}:</ControlLabel>
              <p className="detail-value">{moment().format("DD/MM/YYYY")}</p>
            </Col>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.agence")}: </ControlLabel>
              <p className="detail-value">
                {userFrontDetails.caisseId !== "" &&
                caisseObj != null &&
                caisseObj != "undefined"
                  ? caisseObj.nomAgence
                  : userFrontDetails.agenceNom}
              </p>
            </Col>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.agent")}: </ControlLabel>
              <p className="detail-value">
                {userFrontDetails.caisseId !== "" &&
                caisseObj != null &&
                caisseObj != "undefined"
                  ? caisseObj.nomAgent
                  : userFrontDetails.collaborateurNom}
              </p>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.numeroCaisse")}:</ControlLabel>
              <p className="detail-value">
                {userFrontDetails.caisseId !== "" &&
                  caisseObj != null &&
                  caisseObj != "undefined" &&
                  caisseObj.numCaisse}
              </p>
            </Col>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.soldeCaisse")}:</ControlLabel>
              <p className="detail-value">
                {userFrontDetails.caisseId !== "" &&
                  caisseObj != null &&
                  caisseObj != "undefined" &&
                  caisseObj.soldeActuel}{" "}
                XOF
              </p>
            </Col>
            <Col xs={12} md={4}>
              <ControlLabel>{t("form.label.heure")}:</ControlLabel>
              <p className="detail-value">
                {userFrontDetails.caisseId !== "" &&
                  caisseObj != null &&
                  caisseObj != "undefined" &&
                  caisseObj.heureOuvreture}
              </p>
            </Col>
          </Row>
        </Row>
        <Row style={{ padding: "0 10px" }}>
          <form className="formContainer" style={{ padding: "30px 20px 20px" }}>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="12" style={{ textAlign: "center" }}>
                <Button
                  loading={loadingCloseCaisse}
                  disabled={userFrontDetails.caisseId === ""}
                  onClick={this.handleClick}
                  bsStyle="primary"
                >
                  <i className="fa fa-expeditedssl" />{" "}
                  {t("form.buttons.coloturer")}
                </Button>
                <Button
                  onClick={() => {
                    browserHistory.push(baseUrl + "app/TDBAgentCaisse");
                  }}
                  bsStyle="primary"
                >
                  <i className="fa fa-times" /> {t("form.buttons.cancel")}
                </Button>
              </Col>
            </Row>
          </form>
        </Row>
      </div>
    );
  }
}
