import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Row,
  Col,
  FormControl,
  ButtonGroup,
  Modal,
  ControlLabel,
  PageHeader,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { translate } from "react-i18next";
import Button from "react-bootstrap-button-loader";
import * as CashInActions from "./CashingReducer";
import CashRecap from "./CashRecap";
import CashOutFormValidation from "./ValidatorCashOut";
import { isNullOrUndefined } from "util";
import { asyncConnect } from "redux-connect";
import { AuthorizedComponent } from "react-router-role-authorization";
import CashRecapViderCompte from "./CashRecapViderCompte";

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      dispatch(CashInActions.reload());
    },
  },
])
@translate(["cashincashout"], { wait: true })
@reduxForm(
  {
    form: "CashOut",
    fields: ["beneficiaire", "intitule", "numeroCompte"],
    // validate ,
  },
  (state) =>
    state.CashingReducer.dataClient &&
    state.CashingReducer.dataClient !== undefined
      ? {
          initialValues: {
            numeroCompte:
              state.CashingReducer.dataClient.accounts &&
              state.CashingReducer.dataClient.accounts.length === 1
                ? state.CashingReducer.dataClient.accounts[0].rib
                : "",
          },
        }
      : {
          initialValues: {
            intitule: "",
            beneficiaire: "",
            numeroCompte: "",
          },
        }
)
@connect(
  (state) => ({
    successR: state.CashingReducer.successR,
    messageRetour: state.CashingReducer.messageRetour,
    loadingUser: state.CashingReducer.loadingUser,
    isRecap: state.CashingReducer.isRecap,
    isUpdate: state.CashingReducer.isUpdate,
    data: state.CashingReducer.data,
    isSuccess: state.CashingReducer.isSuccess,
    CashMessageRetour: state.CashingReducer.CashMessageRetour,
    codeErreur: state.CashingReducer.codeErreur,
    disableCash: state.CashingReducer.disableCash,
    dataClient: state.CashingReducer.dataClient,
    accountId: state.CashingReducer.accountId,
    otpSuccess: state.CashingReducer.otpSuccess,
    typePack: state.CashingReducer.typePack,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...CashInActions, initializeWithKey }
)
export default class ViderCompte extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.getInitialState();
    this.handleChangeIntitule = this.handleChangeIntitule.bind(this);
    this.handleChangeBeneficiaire = this.handleChangeBeneficiaire.bind(this);
  }
  input(val) {
    console.log(val);
  }
  getInitialState() {
    this.state = {
      sens: "VC",
      account: "",
      showModalBenificiaire: false,
    };
  }
  componentWillMount() {
    this.props.reload();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      if (this.props.dataClient && nextProps.dataClient !== null) {
        this.setState({
          intitule:
            this.props.dataClient.user.firstName +
            " " +
            this.props.dataClient.user.lastName,
        });
      } else {
        this.setState({ intitule: "" });
        this.setState({ account: "" });
      }
    }
    if (
      nextProps.disableCash === true &&
      this.props.disableCash === false &&
      this.state.shouldReload === true
    ) {
      this.setState({ beneficiaire: "" });
      this.setState({ shouldReload: false });
    }
  }

  handleChangeIntitule(event) {
    this.setState({ intitule: event.target.value });
  }
  handleChangeBeneficiaire(event) {
    if (event.target.value === "" || /^[+|0-9]*$/i.test(event.target.value)) {
      this.setState({ beneficiaire: event.target.value });
    } else {
      this.setState({ beneficiaire: this.state.beneficiaire });
    }
    this.props.reload();
  }
  enterPressed(event) {
    var code = event.keyCode || event.which;
    if (code === 13) {
      this.Search();
    }
  }

  handleRetourButton(values, data) {
    this.props.values.beneficiaire = this.state.beneficiaire;
    values.intitule = data.intitule;
    values.numeroCompte = data.numeroCompte;
    this.props.updateCash(values, data.id, this.state.sens);
  }

  async Search() {
    this.props.values.beneficiaire = this.state.beneficiaire;
    if (
      !isNullOrUndefined(this.state.beneficiaire) &&
      /^((\+212|0)[0-9]{9})$/i.test(this.state.beneficiaire)
    ) {
      await this.props.getClientByPhoneNumber(this.state.beneficiaire);
      this.setState({ beneficiaire: this.state.beneficiaire });
      this.props.values.beneficiaire = this.state.beneficiaire;
      this.setState({ showModalBenificiaire: false });
      if (
        this.props.dataClient &&
        this.props.dataClient.accounts.length === 1
      ) {
        this.setState({ account: this.props.dataClient.accounts[0].rib });
        this.setState({
          solde: this.props.dataClient.accounts[0].balance.currentBalance
            .amount,
        });
      }
    } else {
      this.setState({ showModalBenificiaire: true });
    }
    this.setState({ shouldReload: true });
  }

  handleAccountChange(event) {
    this.setState({ account: event.target.value });
    this.props.dataClient.accounts.map((data) => {
      if (data.rib === event.target.value) {
        this.state.solde = data.balance.currentBalance.amount;
      }
    });
    this.props.values.numeroCompte = event.target.value;
  }

  handleSave(values) {
    this.props.values.beneficiaire = this.state.beneficiaire;
    values.intitule =
      this.props.dataClient.user.firstName +
      " " +
      this.props.dataClient.user.lastName;
    values.accountId = this.props.accountId;
    this.props.saveCash(values, this.state.sens);
  }

  handleUpdateValidation(values, id) {
    this.props.values.beneficiaire = this.state.beneficiaire;
    values.intitule =
      this.props.dataClient.user.firstName +
      " " +
      this.props.dataClient.user.lastName;
    values.accountId = this.props.accountId;

    this.props.updateCash(values, id, this.state.sens);
  }
  phone(value) {
    if (
      !isEmpty(value) &&
      !/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/i.test(value)
    ) {
      return false;
    } else {
      return true;
    }
  }

  render() {
    const {
      t,
      handleSubmit,
      loadingUser,
      isRecap,
      otpSuccess,
      isUpdate,
      messageRetour,
      successR,
      isSuccess,
      data,
      codeErreur,
      CashMessageRetour,
      disableCash,
      dataClient,
      values,
      fields: { beneficiaire, intitule, numeroCompte },
    } = this.props;
    const styles = require("./cashincashout.scss");
    const close = () => {
      this.setState({ showModalBenificiaire: false });
    };

    {
      if (this.props.values.beneficiaire === "") {
        this.props.values.beneficiaire = this.state.beneficiaire;
        console.log(this.props.values.beneficiaire);
      }
    }

    return (
      <div>
        <Modal
          show={loadingUser}
          className="loadingModal"
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>
            <Row>
              <Col xs={12} md={12}>
                <div className="spinner">
                  <span style={{ fontSize: "11px" }}>
                    {t("form.message.chargement")}
                  </span>
                </div>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>
        {isRecap !== true ? (
          <div>
            <PageHeader>
              <h3>Vider un compte</h3>
            </PageHeader>

            {successR === false && (
              <div className="alert alert-danger">{messageRetour}</div>
            )}
            {codeErreur && codeErreur !== "" && isSuccess === false && (
              <div className="alert alert-danger">{codeErreur}</div>
            )}

            <Row>
              <form className="formContainer">
                <fieldset>
                  <legend>{t("form.legend.infosCashIn")}</legend>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.benificiaire")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        {...beneficiaire}
                        onKeyPress={this.enterPressed.bind(this)}
                        onChange={(e) => {
                          this.handleChangeBeneficiaire(e);
                        }}
                        value={
                          this.state.beneficiaire ? this.state.beneficiaire : ""
                        }
                        className={styles.datePickerFormControl}
                      />
                      {beneficiaire.touched && beneficiaire.error && (
                        <div>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {beneficiaire.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <Button
                        onClick={(e) => {
                          this.Search(e);
                        }}
                        bsStyle="primary"
                        style={{ margin: "25px 0px" }}
                      >
                        <i className="fa fa-search " />
                      </Button>
                    </Col>
                  </Row>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.intitule")}</ControlLabel>
                      <FormControl
                        type="text"
                        {...intitule}
                        onChange={(e) => {
                          this.handleChangeIntitule(e);
                        }}
                        value={this.state.intitule ? this.state.intitule : ""}
                        disabled={true}
                        className={styles.datePickerFormControl}
                      />
                      {intitule.touched && intitule.error && (
                        <div>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {intitule.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.comptes")}</ControlLabel>
                      <FormControl
                        {...numeroCompte}
                        componentClass="select"
                        placeholder={t("form.label.account")}
                        className={styles.remDocInput}
                        value={this.state.account ? this.state.account : ""}
                        onChange={(e) => {
                          this.handleAccountChange(e);
                        }}
                        disabled={disableCash == true}
                      >
                        <option hidden>
                          {t("form.hidden.selectionnerCompte")}
                        </option>
                        {dataClient &&
                          dataClient.accounts.map((data) => (
                            <option key={data.rib} value={data.rib}>
                              {"RIB : " + data.rib}
                            </option>
                          ))}
                      </FormControl>
                      {numeroCompte.touched && numeroCompte.error && (
                        <div>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {numeroCompte.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                </fieldset>
                <Row className={styles.paddingColumn}>
                  <div className="pull-right" style={{ paddingTop: "10px" }}>
                    {isUpdate !== true ? (
                      <Button
                        disabled={disableCash == true}
                        onClick={handleSubmit(() => {
                          console.log("Les valeurs sont :" + values);
                          this.handleSave(values);
                        })}
                        bsStyle="primary"
                      >
                        <i className="fa fa-check " /> {t("form.buttons.save")}
                      </Button>
                    ) : (
                      <div
                        className="pull-right"
                        style={{ paddingTop: "10px" }}
                      >
                        <Button
                          disabled={disableCash == true}
                          onClick={handleSubmit(() => {
                            this.handleRetourButton(values, data);
                          })}
                          bsStyle="primary"
                        >
                          <i className="fa fa-check " />{" "}
                          {t("form.buttons.annulerModif")}
                        </Button>
                        <Button
                          disabled={disableCash == true}
                          onClick={handleSubmit(() => {
                            this.handleUpdateValidation(values, data.id);
                          })}
                          bsStyle="primary"
                        >
                          <i className="fa fa-check " />{" "}
                          {t("form.buttons.save")}
                        </Button>
                      </div>
                    )}
                  </div>
                </Row>
              </form>
            </Row>
            <Modal
              show={this.state.showModalBenificiaire}
              onHide={close}
              container={this}
              aria-labelledby="contained-modal-title"
            >
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title">
                  <div>{t("form.label.titreModal")}</div>
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div>{t("form.label.teleVide")}</div>
              </Modal.Body>
              <Modal.Footer>
                <ButtonGroup className="pull-right" bsSize="small">
                  <Button
                    className={styles.ButtonPasswordStyle}
                    onClick={() => close()}
                  >
                    {t("form.buttons.fermer")}
                  </Button>
                </ButtonGroup>
              </Modal.Footer>
            </Modal>
          </div>
        ) : (
          <CashRecapViderCompte
            data={data}
            isSuccess={isSuccess}
            numeroCompte={this.state.account}
            accountId={this.props.accountId}
            sens={this.state.sens}
            errorMsg={CashMessageRetour}
            otpSuccess={otpSuccess}
          />
        )}
      </div>
    );
  }
}
