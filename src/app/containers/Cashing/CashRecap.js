/**
 * Created by lenovo on 11/12/2018.
 */
import React, {Component, PropTypes} from 'react';
import {
    Row,
    Col,
    FormControl,
    Form,
    FormGroup,
    ButtonGroup,
    Alert,
    Panel,
    Modal,
    ControlLabel,
    Popover,
    OverlayTrigger
} from 'react-bootstrap';
import {connect} from 'react-redux';
import {reduxForm, initializeWithKey} from 'redux-form';
import {translate} from 'react-i18next';
import Button from "react-bootstrap-button-loader";
import * as CashInActions from "./CashingReducer";
import {browserHistory} from "react-router";


export const validate = values => {
    const errors = {};
    if (!values.sms || values.sms === '') {
        errors.sms = 'Attention ! Ce champ est obligatoire'
    }
    if (values.sms && values.sms.length !== 6) {
        errors.sms = 'Attention ! Pour remplir ce champ, il faut saisir 6 chiffres';
    }
    console.log(errors);
    return errors;
};

@reduxForm({
        form: 'CashRecap',
        fields: ['sms'],
        validate
    },
    state => (
        {
            initialValues: {
                sms: ""
            },
        }
    )
)

@connect(
    state => ({
        tentativesDepasse: state.CashingReducer.tentativesDepasse,
        otpSuccess : state.CashingReducer.otpSuccess,
        isRecap : state.CashingReducer.isRecap,
        statut : state.CashingReducer.statut
    }), {...CashInActions, initializeWithKey})
@translate(['cashincashout'], {wait: true})
export default class CashRecap extends Component {
    constructor() {
        super();
        this.getInitialState()
        this.handleChangeSms = this.handleChangeSms.bind(this);
        this.handleCashValidated = this.handleCashValidated.bind(this);
    }

    input(val) {
        console.log(val);
    }

    getInitialState() {
        this.state = {
            sens: "",
        };
    }

    handleChangeSms(event) {
        if(/^[0-9]*$/i.test(event.target.value) || event.target.value === '')
            this.setState({sms: event.target.value});
    }

    handleUpdate() {
        this.props.reset();
        console.log(this.props);
    }

    handleAnnuler() {
        this.props.reload();
    }

    async handleSign(values, data, accountId) {
        data.numeroCompte = this.props.numeroCompte;
        await this.props.signCash(values, data, this.props.sens, accountId);
        console.log("Signing.....");
        if(this.props.isSuccess === true)
            this.setState({isCashValidated: false});
    }

    handleCashVlidateReglement(){
        this.setState({showReglementModal: true});
    }

    async handleCashValidated(data) {
        this.setState({showReglementModal: false});
        await this.props.sendOtp(data.id, data.beneficiaire);
        if(this.props.otpSuccess === true)
            this.setState({isCashValidated: true});
    }
    
    handleResendOtp(data) {
        this.props.resendOtp(data.id, data.beneficiaire);
        this.setState({sms: ""});
    }

    render() {
        const {
            t, handleSubmit, data, accountId, isSuccess, sens, statut, errorMsg, values, fields: {sms}, tentativesDepasse
        } = this.props;
        const styles = require('./cashincashout.scss');
        const closeReglementModal = () => {
            this.setState({showReglementModal: false});
            window.location.replace(baseUrl + 'app/Historique');
        };

        return (
            <div>
                <Modal
                    show={this.state.showReglementModal}
                    onHide={closeReglementModal}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                            {sens && sens === "N1" &&
                            <div>{t('popup.confirmation.msgCashIN')}</div>
                            }
                            {sens && sens === "UL" &&
                            <div>{t('popup.confirmation.msgCashOUT')}</div>
                            }
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Footer>
                        <ButtonGroup
                            className="pull-right" bsSize="small"
                        >
                            <Button className={styles.ButtonPasswordStyle} style={{marginRight: '10px'}} onClick={(e) => {
                                this.handleCashValidated(data);
                            }}>{t('popup.confirmation.yesBtn')}</Button>
                            <Button className={styles.ButtonPasswordStyle}
                                    onClick={() => closeReglementModal()}>{t('popup.confirmation.noBtn')}</Button>
                        </ButtonGroup>
                    </Modal.Footer>
                </Modal>

                <div>
                    <Row>
                        <Col xs="12" md="12">
                            {sens && sens === "N1" &&
                            <h2>{t('form.titleForm.cashIn')}</h2>
                            }
                            {sens && sens === "UL" &&
                            <h2>{t('form.titleForm.cashOut')}</h2>
                            }
                        </Col>
                    </Row>
                    {isSuccess && isSuccess === true && sens === "UL" &&
                    <div className="alert alert-success">
                        {t('form.message.succesOut')}
                    </div>
                    }
                    {isSuccess && isSuccess === true && sens === "N1" &&
                    <div className="alert alert-success">
                        {t('form.message.succesIn')}
                    </div>
                    }
                    {errorMsg && errorMsg!==null && errorMsg !== "" && isSuccess === false &&
                    <div className="alert alert-danger">
                          {errorMsg}
                    </div>
                    }
                    <Row className={styles.compteCard}>
                        <form className="formContainer">
                            <fieldset>
                                <legend>{t('form.legend.infosRecapCashOut')}</legend>

                                <Row className={styles.fieldRow} style={{paddingTop: '20px'}}>
                                    <Col xs="12" md="2">
                                        <ControlLabel>{t('form.label.benificiaire')}</ControlLabel>
                                        <p className="detail-value">{data.beneficiaire}</p>
                                    </Col>
                                    <Col xs="12" md="2">
                                        <ControlLabel>{t('form.label.intitule')}</ControlLabel>
                                        <p className="detail-value">{data.intitule}</p>
                                    </Col>
                                    <Col xs="12" md="2">
                                        <ControlLabel>{t('form.label.motif')}</ControlLabel>
                                        <p className="detail-value">{data.motif}</p>
                                    </Col>
                                    <Col xs="12" md="2">
                                        <ControlLabel>{t('form.label.montant')}</ControlLabel>
                                        <p className="detail-value">{data.montant} MAD</p>
                                    </Col>
                                    <Col xs="12" md="2">
                                        <ControlLabel>{t('form.label.statut')}</ControlLabel>
                                        <p className="detail-value">{statut && statut !== null ? statut : data.statut}</p>
                                    </Col>
                                    <Col xs="12" md="2">
                                        <ControlLabel>{t('form.label.fees')}</ControlLabel>
                                        <p className="detail-value">{data.fees} MAD</p>
                                    </Col>
                                </Row>
                                {isSuccess !== true && tentativesDepasse !== true && this.state.isCashValidated && this.state.isCashValidated === true && data.statutCode !== "rejete" &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="4" md="4">
                                        <ControlLabel>{t('form.legend.saisirCode')}</ControlLabel>

                                        <FormControl type="text" {...sms} onChange={(e) => {
                                            this.handleChangeSms(e);
                                        }}
                                                     value={this.state.sms ? this.state.sms : ""}
                                                     className={styles.datePickerFormControl}/>
                                        {sms.touched && sms.error &&
                                        <div>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{sms.error}</i>
                                        </div>
                                        }
                                    </Col>
                                    <Col xs="2" md="2">
                                                <Button onClick={(e) => {
                                                    this.handleResendOtp(data);
                                                }} bsStyle="primary" style={{marginTop: '12%'}}> <i
                                                    className="fa fa-refresh"/> {t('form.buttons.renvoyerCode')} </Button>
                                    </Col>
                                </Row>
                                }
                                {isSuccess !== true && tentativesDepasse !== true && data.statutCode !== "rejete" && this.state.isCashValidated !== true &&
                                <fieldset style={{marginTop: '20px'}}>
                                    <form ref="form">

                                        <Row className={styles.paddingColumn}>
                                            <div className="pull-right" style={{paddingTop: '10px'}}>
                                                <Button  onClick={() => {
                                                    window.location.replace(baseUrl + 'app/Historique');
                                                }} bsStyle="primary"><i className="fa fa-check "/>
                                                    {t('form.buttons.cancel')}
                                                </Button>
                                            </div>
                                            <div className="pull-right" style={{paddingTop: '10px'}}>
                                                <Button onClick={(e) => {
                                                    this.handleUpdate();
                                                }} bsStyle="primary"><i className="fa fa-check "/>
                                                    {t('form.buttons.update')}
                                                </Button>
                                            </div>
                                            <div className="pull-right" style={{paddingTop: '10px'}}>
                                                <Button onClick={(e) => {
                                                    data.agentTypeTwo === true ? this.handleCashVlidateReglement() :
                                                        this.handleCashValidated(data);
                                                }} bsStyle="primary"><i className="fa fa-check "/>
                                                    {t('form.buttons.submit')}
                                                </Button>
                                            </div>


                                        </Row>

                                    </form>
                                </fieldset>
                                }
                                {this.state.isCashValidated && this.state.isCashValidated === true && statut !== 'Rejeté' && data.statut !== 'Rejeté' &&
                                <div className="pull-right" style={{paddingTop: '10px'}}>
                                    <Button onClick={handleSubmit(() => {
                                        this.handleSign(values, data, accountId);
                                    })} bsStyle="primary"><i className="fa fa-check "/>
                                        {t('form.buttons.submit')}
                                    </Button>
                                </div>
                                }
                                {isSuccess === true &&
                                <fieldset style={{marginTop: '20px'}}>
                                    <form ref="form">
                                        <Row className={styles.paddingColumn}>
                                            <div className="pull-right" style={{paddingTop: '10px'}}>
                                                <Button bsStyle="primary" style={{marginRight: '0'}}
                                                        onClick={() => window.open(baseUrl + 'cash/createCashPDF/' + data.id)}>
                                                    <i className="fa fa-file-pdf-o fa-4"/>{t('form.buttons.uploadpdf')}
                                                </Button>
                                            </div>
                                        </Row>
                                    </form>
                                </fieldset>
                                }

                            </fieldset>

                        </form>

                    </Row>

                </div>
            </div>
        );
    }
}
