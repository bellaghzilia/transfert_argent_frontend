import React, { Component } from "react";
import {
  Row,
  Col,
  FormControl,
  ButtonGroup,
  Modal,
  ControlLabel,
  PageHeader,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { translate } from "react-i18next";
import Button from "react-bootstrap-button-loader";
import * as CashInActions from "./CashingReducer";
import CashRecap from "./CashRecap";
import CashInFormValidation from "./ValidatorCashIn";
import { isNullOrUndefined } from "util";
import { asyncConnect } from "redux-connect";
import { AuthorizedComponent } from "react-router-role-authorization";

export const validate = (values, props) => {
  const errors = {};
  const { t } = props;
  if (!values.montant || !values.montant.trim()) {
    errors.montant = t("form.message.champsobligatoire");
  } else if (values.montant && values.montant === "0") {
    errors.montant = t("form.message.soldediffzéro");
  } else {
    errors.montant = null;
  }

  return errors;
};

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      return Promise.all([dispatch(CashInActions.reload())]);
    },
  },
])
@translate(["cashincashout"], { wait: true })
@reduxForm(
  {
    form: "CashIn",
    fields: ["beneficiaire", "intitule", "motif", "montant", "numeroCompte"],
    validate,
  },
  (state) =>
    state.CashingReducer.dataClient &&
    state.CashingReducer.dataClient !== undefined
      ? {
          initialValues: {
            numeroCompte:
              state.CashingReducer.dataClient.accounts &&
              state.CashingReducer.dataClient.accounts.length === 1
                ? state.CashingReducer.dataClient.accounts[0].rib
                : "",
          },
        }
      : {
          initialValues: {
            intitule: "",
            beneficiaire: "",
            motif: "",
            montant: "",
            numeroCompte: "",
          },
        }
)
@connect(
  (state) => ({
    successR: state.CashingReducer.successR,
    messageRetour: state.CashingReducer.messageRetour,
    loadingUser: state.CashingReducer.loadingUser,
    isRecap: state.CashingReducer.isRecap,
    isUpdate: state.CashingReducer.isUpdate,
    data: state.CashingReducer.data,
    isSuccess: state.CashingReducer.isSuccess,
    CashMessageRetour: state.CashingReducer.CashMessageRetour,
    codeErreur: state.CashingReducer.codeErreur,
    disableCash: state.CashingReducer.disableCash,
    dataClient: state.CashingReducer.dataClient,
    accountId: state.CashingReducer.accountId,
    typePack: state.CashingReducer.typePack,
    otpSuccess: state.CashingReducer.otpSuccess,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...CashInActions, initializeWithKey }
)
export default class CashIn extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.getInitialState();
    this.handleChangeIntitule = this.handleChangeIntitule.bind(this);
    this.handleChangeMotif = this.handleChangeMotif.bind(this);
    this.handleChangeBeneficiaire = this.handleChangeBeneficiaire.bind(this);
    this.handleChangeMontant = this.handleChangeMontant.bind(this);
  }

  getInitialState() {
    this.state = {
      sens: "N1",
      account: "",
      max: null,
      solde: "",
      showModalBenificiaire: false,
      MaxSolde: false,
      zeroMontant: false,
    };
  }
  componentWillMount() {
    this.props.reload();
    console.log(this.userRoles);
    console.log(this.notAuthorizedPath);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      if (this.props.dataClient && nextProps.dataClient !== null) {
        this.setState({
          intitule:
            this.props.dataClient.user.firstName +
            " " +
            this.props.dataClient.user.lastName,
        });
      } else {
        this.setState({ intitule: "" });
        this.setState({ motif: "" });
        this.setState({ montant: "" });
        this.setState({ account: "" });
        this.setState({ MaxSolde: false });
      }
    }
    if (
      nextProps.disableCash === true &&
      this.props.disableCash === false &&
      this.state.shouldReload === true
    ) {
      this.setState({ beneficiaire: "" });
      this.setState({ shouldReload: false });
    }
  }

  componentWillUnmount() {
    this.props.reload();
  }

  handleChangeIntitule(event) {
    this.setState({ intitule: event.target.value });
  }

  handleChangeMotif(event) {
    this.setState({ motif: event.target.value });
  }

  handleChangeBeneficiaire(event) {
    if (event.target.value === "" || /^[+|0-9]*$/i.test(event.target.value)) {
      this.setState({ beneficiaire: event.target.value });
    } else {
      this.setState({ beneficiaire: this.state.beneficiaire });
    }
    this.props.reload();
  }

  enterPressed(event) {
    var code = event.keyCode || event.which;
    if (code === 13) {
      this.Search();
    }
  }

  async Search() {
    this.props.values.beneficiaire = this.state.beneficiaire;
    if (
      !isNullOrUndefined(this.state.beneficiaire) &&
      /^((\+212|0)[0-9]{9})$/i.test(this.state.beneficiaire)
    ) {
      await this.props.getClientByPhoneNumber(this.state.beneficiaire);
      this.setState({ beneficiaire: this.state.beneficiaire });
      this.props.values.beneficiaire = this.state.beneficiaire;
      this.setState({ showModalBenificiaire: false });
      if (
        this.props.dataClient &&
        this.props.dataClient.accounts.length === 1
      ) {
        this.setState({ account: this.props.dataClient.accounts[0].rib });
        this.setState({
          solde: this.props.dataClient.accounts[0].balance.currentBalance
            .amount,
        });
      }
    } else {
      this.setState({ showModalBenificiaire: true });
    }
    this.setState({ shouldReload: true });
  }

  handleChangeMontant(event) {
    this.setState({ montant: event.target.value });
    this.setState({ zeroMontant: false });
    this.setState({ MaxSolde: false });
  }
  handleAccountChange(event) {
    this.setState({ account: event.target.value });
    this.props.dataClient.accounts.map((data) => {
      if (data.rib === event.target.value) {
        this.setState({ solde: data.balance.currentBalance.amount });
      }
    });
    this.props.values.numeroCompte = event.target.value;
  }

  handleRetourButton(values, data) {
    this.props.values.beneficiaire = this.state.beneficiaire;
    values.montant = data.montant;
    values.motif = data.motif;
    values.numeroCompte = data.numeroCompte;
    values.intitule = data.intitule;
    this.props.updateCash(values, data.id, this.state.sens);
  }

  handleSave(values) {
    let max = 0;
    this.props.values.beneficiaire = this.state.beneficiaire;
    if (parseFloat(this.state.montant) !== 0) {
      this.state.zeroMontant = false;
      if (this.props.typePack === "type_one") {
        max = 200;
      } else if (this.props.typePack === "type_two") {
        max = 5000;
      } else if (this.props.typePack === "type_three") {
        max = 20000;
      } else if (this.props.typePack === "type_pro") {
        max = 20000;
      }

      if (this.state.solde + parseFloat(this.state.montant) <= max) {
        this.state.MaxSolde = false;
        values.intitule =
          this.props.dataClient.user.firstName +
          " " +
          this.props.dataClient.user.lastName;
        if (values.motif === "") {
          values.motif = "Pas de motif";
          this.props.saveCash(values, this.state.sens);
        } else {
          this.props.saveCash(values, this.state.sens);
        }
        console.log("Saving.....");
      } else {
        this.state.MaxSolde = true;
        this.state.zeroMontant = false;
      }
    } else {
      this.state.zeroMontant = true;
      this.state.MaxSolde = false;
    }
  }
  handleUpdateValidation(values, id) {
    let max = 0;
    this.props.values.beneficiaire = this.state.beneficiaire;
    values.intitule =
      this.props.dataClient.user.firstName +
      " " +
      this.props.dataClient.user.lastName;
    values.accountId = this.props.accountId;
    if (parseFloat(this.state.montant) !== 0) {
      this.state.zeroMontant = false;
      if (this.props.typePack === "type_one") {
        max = 200;
      } else if (this.props.typePack === "type_two") {
        max = 5000;
      } else if (this.props.typePack === "type_three") {
        max = 20000;
      }

      if (this.state.solde + parseFloat(this.state.montant) <= max) {
        this.state.MaxSolde = false;
        if (values.motif === "") {
          values.motif = "Pas de motif";
          this.props.updateCash(values, id, this.state.sens);
        } else {
          this.props.updateCash(values, id, this.state.sens);
        }
      } else {
        this.state.MaxSolde = true;
        this.state.zeroMontant = false;
      }
    } else {
      this.state.zeroMontant = true;
      this.state.MaxSolde = false;
    }
  }

  render() {
    const {
      t,
      handleSubmit,
      loadingUser,
      isRecap,
      messageRetour,
      successR,
      isUpdate,
      data,
      isSuccess,
      otpSuccess,
      codeErreur,
      CashMessageRetour,
      disableCash,
      dataClient,
      values,
      fields: { beneficiaire, intitule, montant, motif, numeroCompte },
    } = this.props;
    const styles = require("./cashincashout.scss");
    const close = () => {
      this.setState({ showModalBenificiaire: false });
    };

    {
      if (this.props.values.beneficiaire === "") {
        this.props.values.beneficiaire = this.state.beneficiaire;
        console.log(this.props.values.beneficiaire);
      }
    }
    return (
      <div>
        <Modal
          show={loadingUser}
          className="loadingModal"
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>
            <Row>
              <Col xs={12} md={12}>
                <div className="spinner">
                  <span style={{ fontSize: "11px" }}>
                    {t("form.message.chargement")}
                  </span>
                </div>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>
        {isRecap !== true ? (
          <div>
            <PageHeader>
              <h3>{t("form.titleForm.cashIn")}</h3>
            </PageHeader>

            {successR === false && (
              <div className="alert alert-danger">{messageRetour}</div>
            )}

            {codeErreur && codeErreur !== "" && isSuccess === false && (
              <div className="alert alert-danger">{codeErreur}</div>
            )}

            <Row>
              <form className="formContainer">
                <fieldset>
                  <legend>{t("form.legend.infosCashIn")}</legend>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.benificiaire")}
                      </ControlLabel>
                      <FormControl
                        type="text"
                        {...beneficiaire}
                        onKeyPress={this.enterPressed.bind(this)}
                        onChange={(e) => {
                          this.handleChangeBeneficiaire(e);
                        }}
                        value={
                          this.state.beneficiaire ? this.state.beneficiaire : ""
                        }
                        className={styles.datePickerFormControl}
                      />
                      {beneficiaire.touched && beneficiaire.error && (
                        <div>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {beneficiaire.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <Button
                        onClick={(e) => {
                          this.Search(e);
                        }}
                        bsStyle="primary"
                        style={{ margin: "25px 0px" }}
                      >
                        <i className="fa fa-search " />
                      </Button>
                    </Col>
                  </Row>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.intitule")}</ControlLabel>
                      <FormControl
                        type="text"
                        {...intitule}
                        disabled={true}
                        onChange={(e) => {
                          this.handleChangeIntitule(e);
                        }}
                        value={this.state.intitule ? this.state.intitule : ""}
                        className={styles.datePickerFormControl}
                      />
                      {intitule.touched && intitule.error && (
                        <div>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {intitule.error}
                          </i>
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.comptes")}</ControlLabel>
                      <FormControl
                        {...numeroCompte}
                        componentClass="select"
                        placeholder={t("form.label.account")}
                        className={styles.remDocInput}
                        value={this.state.account ? this.state.account : ""}
                        onChange={(e) => {
                          this.handleAccountChange(e);
                        }}
                        disabled={disableCash == true}
                      >
                        <option hidden>
                          {t("form.hidden.selectionnerCompte")}
                        </option>
                        {dataClient &&
                          dataClient.accounts.map((data) => (
                            <option key={data.rib} value={data.rib}>
                              {"RIB : " + data.rib}
                            </option>
                          ))}
                      </FormControl>
                      {numeroCompte.touched && numeroCompte.error && (
                        <div>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {numeroCompte.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.montant")}</ControlLabel>
                      <FormControl
                        type="text"
                        {...montant}
                        disabled={disableCash == true}
                        onChange={(e) => {
                          this.handleChangeMontant(e);
                        }}
                        value={this.state.montant ? this.state.montant : ""}
                        className={styles.datePickerFormControl}
                      />
                      {montant.touched && montant.error && (
                        <div>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {montant.error}
                          </i>
                        </div>
                      )}
                      {this.state.zeroMontant === true && (
                        <div className="alert alert-danger">
                          {t("form.message.zeroSolde")}
                        </div>
                      )}
                      {this.state.MaxSolde === true && (
                        <div className="alert alert-danger">
                          {t("form.message.maxSolde")}
                        </div>
                      )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.motif")}</ControlLabel>
                      <FormControl
                        type="text"
                        {...motif}
                        disabled={disableCash == true}
                        onChange={(e) => {
                          this.handleChangeMotif(e);
                        }}
                        value={this.state.motif ? this.state.motif : ""}
                        className={styles.datePickerFormControl}
                      />
                      {motif.touched && motif.error && (
                        <div>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {motif.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                </fieldset>
                <Row className={styles.paddingColumn}>
                  <div className="pull-right" style={{ paddingTop: "10px" }}>
                    {isUpdate !== true ? (
                      <Button
                        disabled={disableCash == true}
                        onClick={handleSubmit(() => {
                          this.handleSave(values);
                        })}
                        bsStyle="primary"
                      >
                        <i className="fa fa-check " /> {t("form.buttons.save")}
                      </Button>
                    ) : (
                      <div
                        className="pull-right"
                        style={{ paddingTop: "10px" }}
                      >
                        <Button
                          disabled={disableCash == true}
                          onClick={handleSubmit(() => {
                            this.handleRetourButton(values, data);
                          })}
                          bsStyle="primary"
                        >
                          <i className="fa fa-check " />{" "}
                          {t("form.buttons.annulerModif")}
                        </Button>
                        <Button
                          disabled={disableCash == true}
                          onClick={handleSubmit(() => {
                            this.handleUpdateValidation(values, data.id);
                          })}
                          bsStyle="primary"
                        >
                          <i className="fa fa-check " />{" "}
                          {t("form.buttons.save")}
                        </Button>
                      </div>
                    )}
                  </div>
                </Row>
              </form>
            </Row>
            <Modal
              show={this.state.showModalBenificiaire}
              onHide={close}
              container={this}
              aria-labelledby="contained-modal-title"
            >
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title">
                  <div>{t("form.label.titreModal")}</div>
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div>{t("form.label.teleVide")}</div>
              </Modal.Body>
              <Modal.Footer>
                <ButtonGroup className="pull-right" bsSize="small">
                  <Button
                    className={styles.ButtonPasswordStyle}
                    onClick={() => close()}
                  >
                    {t("form.buttons.fermer")}
                  </Button>
                </ButtonGroup>
              </Modal.Footer>
            </Modal>
          </div>
        ) : (
          <CashRecap
            data={data}
            isSuccess={isSuccess}
            numeroCompte={this.state.account}
            accountId={this.props.accountId}
            sens={this.state.sens}
            errorMsg={CashMessageRetour}
            otpSuccess={otpSuccess}
          />
        )}
      </div>
    );
  }
}
