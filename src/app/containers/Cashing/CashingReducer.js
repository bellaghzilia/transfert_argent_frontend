
const LOAD_CASH = 'CASH/LOAD_CASH';
const LOAD_CASH_SUCCESS = 'CASH/LOAD_CASH_SUCCESS';
const LOAD_CASH_FAIL = 'CASH/LOAD_CASH_FAIL';

const RESET = 'CASH/RESET';
const RELOAD = 'CASH/RELOAD';

const SAVE_CASH = 'CASH/SAVE_CASH';
const SAVE_CASH_SUCCESS = 'CASH/SAVE_CASH_SUCCESS';
const SAVE_CASH_FAIL = 'CASH/SAVE_CASH_FAIL';

const SAVE_CASH_VIDER_COMPTE = 'CASH/SAVE_CASH_VIDER_COMPTE';
const SAVE_CASH_VIDER_COMPTE_SUCCESS = 'CASH/SAVE_CASH_VIDER_COMPTE_SUCCESS';
const SAVE_CASH_VIDER_COMPTE_FAIL = 'CASH/SAVE_CASH_VIDER_COMPTE_FAIL';

const UPDATE_CASH = 'CASH/UPDATE_CASH';
const UPDATE_CASH_SUCCESS = 'CASH/UPDATE_CASH_SUCCESS';
const UPDATE_CASH_FAIL = 'CASH/UPDATE_CASH_FAIL';

const SIGN_CASH = 'CASH/SIGN_CASH';
const SIGN_CASH_SUCCESS = 'CASH/SIGN_CASH_SUCCESS';
const SIGN_CASH_FAIL = 'CASH/SIGN_CASH_FAIL';

const SIGN_CASH_VIDER_COMPTE = 'CASH/SIGN_CASH_VIDER_COMPTE';
const SIGN_CASH_VIDER_COMPTE_SUCCESS = 'CASH/SIGN_CASH_VIDER_COMPTE_SUCCESS';
const SIGN_CASH_VIDER_COMPTE_FAIL = 'CASH/SIGN_CASH_VIDER_COMPTE_FAIL';

const LOAD_CASH_CLIENT = 'CASH/LOAD_CASH_CLIENT';
const LOAD_CASH_CLIENT_SUCCESS = 'CASH/LOAD_CASH_CLIENT_SUCCESS';
const LOAD_CASH_CLIENT_FAIL = 'CASH/LOAD_CASH_CLIENT_FAIL';

const SEND_OTP = 'CASH/SEND_OTP';
const SEND_OTP_SUCCESS = 'CASH/SEND_OTP_SUCCESS';
const SEND_OTP_FAIL = 'CASH/SEND_OTP_FAIL';

const RESEND_OTP = 'CASH/RESEND_OTP';
const RESEND_OTP_SUCCESS = 'CASH/RESEND_OTP_SUCCESS';
const RESEND_OTP_FAIL = 'CASH/RESEND_OTP_FAIL';

const LOAD_CASH_STATUT = 'CASH/LOAD_CASH_STATUT';
const LOAD_CASH_STATUT_SUCCESS = 'CASH/LOAD_CASH_STATUT_SUCCESS';
const LOAD_CASH_STATUT_FAIL = 'CASH/LOAD_CASH_STATUT_FAIL';


const initialState = {
    isUpdate:false,
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case LOAD_CASH_STATUT:
            return {
                ...state,
                loading: true,
                isRecap:false
            };
        case LOAD_CASH_STATUT_SUCCESS: {
            return {
                ...state,
                loading: false,
                loaded: true,
                statuts : action.result.cashStatutsList
            };
        }
        case LOAD_CASH_STATUT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                CASHList: null,
                error: action.error
            };
        case LOAD_CASH:
            return {
                ...state,
                loading: true,
                isRecap:false
            };
        case LOAD_CASH_SUCCESS: {

            return {
                ...state,
                loading: false,
                loaded: true,
            };
        }
        case LOAD_CASH_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                CASHList: null,
                error: action.error
            };
        case SAVE_CASH:
            return {
                ...state,
                loading: true,
                messageSaved: null,
                alertState: null,
                savingError: null,
                disableState: null,
                isRecap:false
            };
        case SAVE_CASH_SUCCESS:
            if(action.result.success) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    messageSaved: action.result.success,
                    alertState: action.action,
                    savingError: null,
                    isRecap: true,
                    disableState: true,
                    data: action.result.CashDTO,
                    numberAccount:action.numberAccount,
                    codeErreur : ""
                };
            }else{
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    alertState: action.action,
                    codeErreur: action.result.messageRetour,
                    isSuccess:action.result.success
                };
            }
        case SAVE_CASH_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                alertState: null,
                messageSaved: null,
                savingError: action.codeErreur,
                isRecap:false
            };

        case SAVE_CASH_VIDER_COMPTE:
            return {
                ...state,
                loading: true,
                messageSaved: null,
                alertState: null,
                savingError: null,
                disableState: null,
                isRecap:false
            };
        case SAVE_CASH_VIDER_COMPTE_SUCCESS:
            if(action.result.success) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    messageSaved: action.result.success,
                    alertState: action.action,
                    savingError: null,
                    isRecap: true,
                    disableState: true,
                    data: action.result.CashDTO,
                    numberAccount:action.numberAccount,
                    codeErreur : ""
                };
            }else{
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    alertState: action.action,
                    codeErreur: action.result.messageRetour,
                    isSuccess:action.result.success
                };
            }
        case SAVE_CASH_VIDER_COMPTE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                alertState: null,
                messageSaved: null,
                savingError: action.codeErreur,
                isRecap:false
            };

        case UPDATE_CASH:
            return {
                ...state,
                loading: true,
                messageSaved: null,
                alertState: null,
                savingError: null,
                disableState: null,
                isRecap:false,
                isSuccess: null,
            };
        case UPDATE_CASH_SUCCESS:
            if(action.result.success) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    messageSaved: action.result.success,
                    alertState: action.action,
                    errorMsg: action.result.codeErreur,
                    savingError: null,
                    isRecap: true,
                    disableState: true,
                    data: action.result.CashDTO
                };
            }else{
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    alertState: action.action,
                    codeErreur: action.result.messageRetour,
                    isSuccess:action.result.success
                };
            }
        case UPDATE_CASH_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                alertState: null,
                messageSaved: null,
                savingError: action.codeErreur,
                isRecap:false
            };
        case SIGN_CASH:
            return {
                ...state,
                loadingUser: true,
                messageSaved: null,
                alertState: null,
                savingError: null,
                disableState: null,
            };
        case SIGN_CASH_SUCCESS:
            if(action.result.success) {
                return {
                    ...state,
                    loadingUser: false,
                    loaded: true,
                    isSuccess: action.result.success,
                    CashMessageRetour: action.result.messageRetour,
                    data: action.result.CashDTO
                };
            }
            else{
                return {
                    ...state,
                    loadingUser: false,
                    loaded: true,
                    isSuccess: action.result.success,
                    CashMessageRetour: action.result.messageRetour,
                    tentativesDepasse : action.result.tentativesDepasse,
                    data: action.result.CashDTO
                };
            }
        case SIGN_CASH_FAIL:
            return {
                ...state,
                loadingUser: false,
                loaded: false,
                alertState: null,
                messageSaved: null,
                CashMessageRetour : action.result.messageRetour,
                isSuccess : action.result.success
            };

        case SIGN_CASH_VIDER_COMPTE:
            return {
                ...state,
                loadingUser: true,
                messageSaved: null,
                alertState: null,
                savingError: null,
                disableState: null,
            };
        case SIGN_CASH_VIDER_COMPTE_SUCCESS:
            if(action.result.success) {
                return {
                    ...state,
                    loadingUser: false,
                    loaded: true,
                    isSuccess: action.result.success,
                    CashMessageRetour: action.result.messageRetour,
                    data: action.result.CashDTO
                };
            }
            else{
                return {
                    ...state,
                    loadingUser: false,
                    loaded: true,
                    isSuccess: action.result.success,
                    CashMessageRetour: action.result.messageRetour,
                    tentativesDepasse : action.result.tentativesDepasse,
                    data: action.result.CashDTO
                };
            }
        case SIGN_CASH_VIDER_COMPTE_FAIL:
            return {
                ...state,
                loadingUser: false,
                loaded: false,
                alertState: null,
                messageSaved: null,
                CashMessageRetour : action.result.messageRetour,
                isSuccess : action.result.success
            };

        case SEND_OTP:
            return {
                ...state,
                loading: true
            };
        case SEND_OTP_SUCCESS: {
            if(action.result.success){
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    otpSuccess: action.result.success,
                    errorMsg: action.result.messageRetour
                };
            }else{
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    otpSuccess: action.result.success,
                    errorMsg: action.result.messageRetour
                };
            }
        }
        case SEND_OTP_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case RESEND_OTP:
            return {
                ...state,
                loading: true
            };
        case RESEND_OTP_SUCCESS: {
            if(action.result.success){
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    otpSuccess: action.result.success,
                    CashMessageRetour: action.result.messageRetour
                };
            }else{
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    otpSuccess: action.result.success,
                    errorMsg: action.result.messageRetour,
                    statut : 'Rejeté',
                    tentativesDepasse : action.result.tentativesDepasse
                };
            }
        }
        case RESEND_OTP_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case RESET:
            return {
                ...state,
                isRecap : false,
                isUpdate : true,
                statut : null
            };
        case RELOAD:
            return {
                ...state,
                isRecap : false,
                isUpdate : false,
                isSuccess : false,
                messageRetour : "",
                disableCash:true,
                tentativesDepasse : false,
                successR:null,
                codeErreur : "",
                intitule : "",
                dataClient : null,
                beneficiaire : "",
                statut : null,
                errorMsg:""
            };

        case LOAD_CASH_CLIENT:
            return {
                loadingUser: true,
                disableCash:null,
                messageRetour:null,
                successR:null,
            };

        case LOAD_CASH_CLIENT_SUCCESS:
            if(action.result.success) {
                return {
                    ...state,
                    loadingUser: false,
                    disableCash:false,
                    dataClient: action.result.dataClient,
                    accountId: action.result.accountId,
                    typePack:action.result.typePack,
                    successR:action.result.success,
                    errorMsg: action.result.messageRetour,
                };
            }else
             {
                return {
                    ...state,
                    loadingUser: false,
                    disableCash:true,
                    dataClient: null,
                    successR:action.result.success,
                    messageRetour:action.result.messageRetour,
                    errorMsg: action.result.messageRetour,
                };
            }
        case LOAD_CASH_CLIENT_FAIL:
            return {
                loadingUser: false,
                dataClient: null,
                disableCash:true,
                successR:action.result.success,
                messageRetour:action.result.messageRetour,
            }
        default:
            return state;

    }
}

function objectToParams(object) {
    let str = '';
    for (const key in object) {
        if (str !== '') {
            str += '&';
        }
        str += key + '=' + encodeURIComponent(object[key]);
    }
    return str;
}



export function loadCASH() {

    return {
        types: [LOAD_CASH, LOAD_CASH_SUCCESS, LOAD_CASH_FAIL],
        promise: (client) => client.get('cash/listObject')
    };
}

export function saveCash(values,sens) {
    values.sens = sens;
    return {
        types: [SAVE_CASH, SAVE_CASH_SUCCESS, SAVE_CASH_FAIL],
        promise: (client) => client.post('cash/save', {data: objectToParams(values)}),
        numberAccount:values.numeroCompte
    };
}

export function saveCashViderCompte(values,sens) {
    values.sens = sens;
    return {
        types: [SAVE_CASH, SAVE_CASH_SUCCESS, SAVE_CASH_FAIL],
        promise: (client) => client.post('cash/saveViderCompte', {data: objectToParams(values)}),
        numberAccount:values.numeroCompte
    };
}

export function updateCash(values,id,sens) {
    values.sens = sens;
    return {
        types: [UPDATE_CASH, UPDATE_CASH_SUCCESS, UPDATE_CASH_FAIL],
        promise: (client) => client.post('cash/update/'+id, {data: objectToParams(values)})
    };
}

export function signCash(values,data,sens, accountId) {
    data.sens = sens;
    data.codeOTP = values.sms;
    data.accountId = accountId;
    return {
        types: [SIGN_CASH, SIGN_CASH_SUCCESS, SIGN_CASH_FAIL],
        promise: (client) => client.post('cash/submit/', {data: objectToParams(data)})
    };
}

export function signCashViderCompte(values,data,sens, accountId) {
    data.sens = sens;
    data.codeOTP = values.sms;
    data.accountId = accountId;
    return {
        types: [SIGN_CASH_VIDER_COMPTE, SIGN_CASH_VIDER_COMPTE_SUCCESS, SIGN_CASH_VIDER_COMPTE_FAIL],
        promise: (client) => client.post('cash/submitViderCompte/', {data: objectToParams(data)})
    };
}

export function getClientByPhoneNumber(phone) {
    return {
        types: [ LOAD_CASH_CLIENT, LOAD_CASH_CLIENT_SUCCESS, LOAD_CASH_CLIENT_FAIL],
        promise: (client) => client.get('api/subscriber/getDetails/'+phone)
    };
}

export function resendOtp(id, gsm) {
    let obj = { id: id, telephone: gsm };
    return {
        types: [RESEND_OTP, RESEND_OTP_SUCCESS, RESEND_OTP_FAIL],
        promise: (client) => client.post('client/resendOtp?type=C', {data: objectToParams(obj)})
    };
}

export function sendOtp(id, gsm) {
    let obj = { id: id, telephone: gsm };
    return {
        types: [SEND_OTP, SEND_OTP_SUCCESS, SEND_OTP_FAIL],
        promise: (client) => client.post('client/sendOtp?type=C', {data: objectToParams(obj)})
    };
}

export function reset(){
    return{
        type: RESET,
    }
}

export function reload(){
    return{
        type: RELOAD,
    }
}

export function loadCashStatuts() {
    return {
        types: [LOAD_CASH_STATUT, LOAD_CASH_STATUT_SUCCESS, LOAD_CASH_STATUT_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getCashStatus')
    };
}