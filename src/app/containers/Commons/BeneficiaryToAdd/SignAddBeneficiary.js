import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Row,
  Col,
  Button,
  FormControl,
  FormGroup,
  Checkbox,
  Alert,
  ButtonToolbar,
  Form,
  OverlayTrigger,
  Popover,
} from "react-bootstrap";

export default class SignAddBeneficiary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pass: "",
      keyPadValues: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
    };
    this.generatekeyPadValues = this.generatekeyPadValues.bind(this);
    this.handelDelete = this.handelDelete.bind(this);
  }
  generatekeyPadValues() {
    const keyPadValues = this.state.keyPadValues;
    let j;
    let x;
    for (let i = keyPadValues.length; i; i--) {
      j = Math.floor(Math.random() * i);
      x = keyPadValues[i - 1];
      keyPadValues[i - 1] = keyPadValues[j];
      keyPadValues[j] = x;
    }
    this.setState({ keyPadValues });
  }
  handelClick(index) {
    this.setState({ pass: this.state.pass + "" + index });
    this.props.values.password = this.state.pass;
  }
  handelDelete() {
    if (this.state.pass.length) {
      this.setState({
        pass: this.state.pass.slice(0, this.state.pass.length - 1),
      });
    }
  }
  render() {
    const style = require("../styleKeyPad.scss");
    const { resultSave, signerBeneficiary, cancelSignature } = this.props;
    const popoverBottom = (
      <Popover id="popover-positioned-top" title="Code Validation">
        <div className={style.pageWrap}>
          <div className={style.keypad}>
            {this.state.keyPadValues.map((key) => (
              <span
                key={key}
                onClick={this.handelClick.bind(this, key)}
                className={key}
              >
                {key}
              </span>
            ))}
            <span
              onClick={this.handelDelete}
              className="glyphicon glyphicon-arrow-left"
            />
            <span
              onClick={() => this.setState({ pass: "" })}
              className="glyphicon glyphicon-refresh"
            />
            <br />
          </div>
        </div>
      </Popover>
    );
    return (
      <Row>
        <Row className="text-center">
          <Alert bsStyle="info">
            <p>
              Veuillez appuyer sur le bouton (Confirmer et signer) pour recevoir
              votre code provisoire qui valide électroniquement votre demande.
            </p>
          </Alert>
        </Row>
        <Row className="text-center">
          <Col md={2} xs={2}>
            <p> mot de passe</p>
          </Col>
          <Col md={6} xs={6}>
            <Form inline>
              <OverlayTrigger
                trigger="click"
                rootClose
                placement="bottom"
                overlay={popoverBottom}
              >
                <input
                  type="password"
                  value={this.state.pass}
                  onClick={this.generatekeyPadValues}
                  className="form-control"
                />
              </OverlayTrigger>
            </Form>
          </Col>
          <Col md={4} xs={4}>
            <ButtonToolbar className="pull-right" bsSize="small">
              <Button
                bsStyle="primary"
                onClick={() => {
                  signerBeneficiary(
                    resultSave.id,
                    resultSave.taskId,
                    this.state.pass
                  );
                }}
              >
                <i className="fa fa-check" /> Confirmer et signer
              </Button>
              <Button bsStyle="primary" onClick={() => cancelSignature()}>
                <i className="fa fa-times" />
                Annuler
              </Button>
            </ButtonToolbar>
          </Col>
        </Row>
      </Row>
    );
  }
}
