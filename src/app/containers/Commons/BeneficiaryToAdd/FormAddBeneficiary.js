import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Row,
  Col,
  Button,
  FormControl,
  FormGroup,
  Checkbox,
  HelpBlock,
  Alert,
  ButtonToolbar,
} from "react-bootstrap";
import { reduxForm } from "redux-form";

export const validate = (values) => {
  const errors = {};
  if (!values.intitule) {
    errors.intitule = "Ce champ est obligatoire";
  } else if (!/^[A-Z0-9]+$/.test(values.numeroCompte)) {
    errors.numeroCompte = "IBAN invalide => caractère miniscule";
  }
  if (!values.numeroCompte) {
    errors.numeroCompte = "Ce champ est obligatoire";
  } else if (values.maxLengthIban > 0) {
    if (values.numeroCompte.length !== values.maxLengthIban) {
      errors.numeroCompte =
        "Le nombre de caractère de ce champs est de " + values.maxLengthIban;
    }
  } else if (values.maxLengthIban === 0) {
    if (values.numeroCompte.length > 34) {
      errors.numeroCompte =
        "Le nombre de caractère maximum de ce champs est de 34";
    }
    if (values.numeroCompte.length < 11) {
      errors.numeroCompte =
        "Le nombre de caractère maximum de ce champs est de 11";
    }
  }
  return errors;
};
@reduxForm(
  {
    form: "AddBeneficaryToTransaction",
    fields: [
      "id",
      "taskId",
      "version",
      "type",
      "intitule",
      "deviseCompte",
      "numeroCompte",
      "canalWeb",
      "canalMobile",
      "genre",
      "nature",
      "maxLengthIban",
    ],
    validate,
  },
  (state) => ({
    initialValues: {
      id: "",
      taskId: "",
      version: "",
      type:
        state.nomEnClature &&
        state.nomEnClature.beneficiaryTypeList &&
        state.nomEnClature.beneficiaryTypeList[0] &&
        state.nomEnClature.beneficiaryTypeList[0].code,
      genre:
        state.nomEnClature &&
        state.nomEnClature.genreBeneficiaryList &&
        state.nomEnClature.genreBeneficiaryList[0] &&
        state.nomEnClature.genreBeneficiaryList[0].code,
      deviseCompte:
        state.params &&
        state.params.parametrage &&
        state.params.parametrage.deviseBanqueModel,
      canalMobile: true,
      canalWeb: true,
      nature: "DOMESTIQUE_NatureBeneficiaire",
      maxLengthIban:
        state.params &&
        state.params.parametrage &&
        state.params.parametrage.LENGTH_IBAN_RIB_BENEFCIARY,
      numeroCompte: "",
    },
  })
)
export default class FormAddBeneficiary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessageCheckBox: false,
    };
  }
  render() {
    const {
      fields: {
        id,
        taskId,
        version,
        type,
        intitule,
        deviseCompte,
        numeroCompte,
        canalWeb,
        canalMobile,
        genre,
        nature,
        maxLengthIban,
      },
      handleSubmit,
      values,
      submitting,
      user,
      resetForm,
      devisesList,
      beneficiaryTypeList,
      genreBeneficiaryList,
      saveBeneficiary,
      addBeneficiary,
      resultSave,
      parametrage,
    } = this.props;
    const verifyChekBoxWeb = (event) => {
      if (values.canalMobile === false && values.canalWeb === true) {
        event.target.checked = true;
        this.setState({ errorMessageCheckBox: true });
      } else {
        this.setState({ errorMessageCheckBox: false });
      }
    };
    const verifyChekBoxMobile = (event) => {
      if (values.canalWeb === false && values.canalMobile === true) {
        event.target.checked = true;
        this.setState({ errorMessageCheckBox: true });
      } else {
        this.setState({ errorMessageCheckBox: false });
      }
    };
    return (
      <form onSubmit={handleSubmit}>
        <fieldset>
          <legend>Béneficiaire</legend>
          <Row>
            {parametrage && parametrage.AFFICHAGE_BENIFICIAIRE_NATURE === "y" && (
              <span>
                <Col xs={2} md={2}>
                  <p> Nature </p>
                </Col>
                <Col xs={4} md={4}>
                  <FormControl
                    type="text"
                    className="form-control"
                    {...nature}
                    value="SEPA"
                    readOnly="true"
                  />
                </Col>
              </span>
            )}
            {parametrage && parametrage.AFFICHAGE_BENIFICIAIRE_GENRE === "y" && (
              <span>
                <Col xs={2} md={2}>
                  <p> Genre </p>
                </Col>
                <Col xs={4} md={4}>
                  <FormControl
                    {...genre}
                    componentClass="select"
                    placeholder="select"
                  >
                    <option value="" hidden>
                      Séléctionner genre bénéficiare
                    </option>
                    {genreBeneficiaryList &&
                      genreBeneficiaryList.length &&
                      genreBeneficiaryList.map((genreBeneficiary) => (
                        <option
                          key={genreBeneficiary.id}
                          value={genreBeneficiary.code}
                        >
                          {genreBeneficiary.libelle}
                        </option>
                      ))}
                  </FormControl>
                </Col>
              </span>
            )}
          </Row>
          <Row>
            {parametrage &&
              parametrage.AFFICHAGE_BENIFICIAIRE_COMBO_TYPE === "y" && (
                <span>
                  <Col xs={2} md={2}>
                    <p> Type </p>
                  </Col>
                  <Col xs={4} md={4}>
                    <FormControl
                      {...type}
                      componentClass="select"
                      placeholder="select"
                    >
                      <option value="" hidden>
                        Séléctionner type bénéficiare
                      </option>
                      {beneficiaryTypeList &&
                        beneficiaryTypeList.length &&
                        beneficiaryTypeList.map((typeBeneficiare) => (
                          <option
                            key={typeBeneficiare.id}
                            value={typeBeneficiare.code}
                          >
                            {typeBeneficiare.libelle}
                          </option>
                        ))}
                    </FormControl>
                  </Col>
                </span>
              )}
            <Col xs={2} md={2}>
              <p> Nom </p>
            </Col>
            <Col xs={4} md={4}>
              <FormControl type="text" className="form-control" {...intitule} />
              {intitule.touched && intitule.error && (
                <HelpBlock>
                  <i className="fa fa-exclamation-triangle" /> {intitule.error}{" "}
                </HelpBlock>
              )}
            </Col>
          </Row>
          {parametrage && parametrage.AFFICHAGE_BENIFICIAIRE_CANAL === "y" && (
            <Row>
              <Col xs={2} md={2}>
                <p> Canal </p>
              </Col>
              <Col xs={9} md={9}>
                <FormGroup>
                  <Checkbox inline {...canalWeb} onClick={verifyChekBoxWeb}>
                    Espace client web
                  </Checkbox>{" "}
                  <Checkbox
                    inline
                    {...canalMobile}
                    onClick={verifyChekBoxMobile}
                  >
                    Espace client mobile
                  </Checkbox>
                </FormGroup>
              </Col>
            </Row>
          )}
          {this.state.errorMessageCheckBox && (
            <Alert bsStyle="info">
              <p>Vous devez séléctionner au moin un canal</p>
            </Alert>
          )}
          <Row>
            <Col xs={2} md={2}>
              <p> Devise </p>
            </Col>
            <Col xs={4} md={4}>
              <FormControl
                {...deviseCompte}
                componentClass="select"
                disabled="true"
              >
                {devisesList &&
                  devisesList.length &&
                  devisesList.map((devise) => (
                    <option key={devise.id} value={devise.code}>
                      {devise.libelle}
                    </option>
                  ))}
              </FormControl>
            </Col>
            <Col xs={2} md={2}>
              <p> IBAN </p>
            </Col>
            <Col xs={4} md={4}>
              <FormControl
                type="text"
                className="form-control"
                {...numeroCompte}
              />
              {numeroCompte.touched && numeroCompte.error && (
                <HelpBlock>
                  <i className="fa fa-exclamation-triangle" />{" "}
                  {numeroCompte.error}{" "}
                </HelpBlock>
              )}
            </Col>
          </Row>
        </fieldset>
        <ButtonToolbar className="pull-right" bsSize="small">
          <Button
            bsStyle="primary"
            onClick={handleSubmit(() => {
              saveBeneficiary(values, user);
            })}
          >
            <i
              className={
                "fa " + (submitting ? "fa-cog fa-spin" : "fa fa-check")
              }
            />
            Valider
          </Button>
          <Button bsStyle="primary" onClick={() => resetForm()}>
            <i className="fa fa-times" />
            Vider
          </Button>
        </ButtonToolbar>
      </form>
    );
  }
}
