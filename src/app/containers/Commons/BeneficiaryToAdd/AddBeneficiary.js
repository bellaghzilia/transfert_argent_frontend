import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Row,
  Col,
  Button,
  FormControl,
  FormGroup,
  Checkbox,
  Alert,
  ButtonToolbar,
  Modal,
} from "react-bootstrap";
import { translate } from "react-i18next";
import { asyncConnect } from "redux-connect";
import { initializeWithKey, reduxForm } from "redux-form";
import { connect } from "react-redux";
import FormAddBeneficiary from "./FormAddBeneficiary";
import SignAddBeneficiary from "./SignAddBeneficiary";
import * as AddBeneficiaryAction from "./addBeneficiaryReducer";
import * as nomEnClatureActions from "../NomEnClatureReducer";
import * as ParamsAction from "../paramsReducer";

@connect(
  (state) => ({
    devisesList: state.nomEnClature.devisesList,
    beneficiaryTypeList: state.nomEnClature.beneficiaryTypeList,
    genreBeneficiaryList: state.nomEnClature.genreBeneficiaryList,
    resultSave: state.addBeneficiary.resultSave,
    resultSigner: state.addBeneficiary.resultSigner,
    codeErreur: state.addBeneficiary.codeErreur,
    showAlertError: state.addBeneficiary.showAlertError,
    showBlockSign: state.addBeneficiary.showBlockSign,
    titre: state.addBeneficiary.titre,
    parametrage: state.params.parametrage,
    user: state.user.userFrontDetails,
  }),
  {
    ...AddBeneficiaryAction,
    ...nomEnClatureActions,
    ...ParamsAction,
    initializeWithKey,
  }
)
@translate(["common"], { wait: true })
export default class AddBeneficiary extends Component {
  constructor(props) {
    super(props);
    const { loadNomEnClature, loadParamasBeneficiary } = this.props;
    loadParamasBeneficiary();
    loadNomEnClature("getDevises");
    loadNomEnClature("getTypesBeneficiaires");
    loadNomEnClature("getGenresBeneficiaires");
  }
  render() {
    const {
      user,
      devisesList,
      beneficiaryTypeList,
      genreBeneficiaryList,
      saveBeneficiary,
      resultSave,
      showBlockSign,
      resultSigner,
      signerBeneficiary,
      showAlertError,
      closePanelError,
      titre,
      codeErreur,
      parametrage,
      cancelSignature,
    } = this.props;
    return (
      <Row className="app-panel">
        <Modal
          show={showAlertError}
          onHide={closePanelError}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">{titre}</Modal.Title>
          </Modal.Header>
          <Modal.Body>{codeErreur}</Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={closePanelError}>
              OK
            </Button>
          </Modal.Footer>
        </Modal>
        {showBlockSign ? (
          <SignAddBeneficiary
            resultSave={resultSave}
            signerBeneficiary={signerBeneficiary}
            cancelSignature={cancelSignature}
          />
        ) : (
          <FormAddBeneficiary
            user={user}
            devisesList={devisesList}
            beneficiaryTypeList={beneficiaryTypeList}
            genreBeneficiaryList={genreBeneficiaryList}
            saveBeneficiary={saveBeneficiary}
            resultSave={resultSave}
            parametrage={parametrage}
          />
        )}
      </Row>
    );
  }
}
