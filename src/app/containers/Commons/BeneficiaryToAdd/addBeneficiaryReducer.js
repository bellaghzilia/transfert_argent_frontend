const SAVE_BENEFICIARY = 'addBeneficiary/SAVE_BENEFICIARY';
const SAVE_BENEFICIARY_SUCCESS = 'addBeneficiary/SAVE_BENEFICIARY_SUCCESS';
const SAVE_BENEFICIARY_FAIL = 'addBeneficiary/SAVE_BENEFICIARY_FAIL';

const SIGNER_BENEFICIARY = 'addBeneficiary/SIGNER_BENEFICIARY';
const SIGNER_BENEFICIARY_SUCCESS = 'addBeneficiary/SIGNER_BENEFICIARY_SUCCESS';
const SIGNER_BENEFICIARY_FAIL = 'addBeneficiary/SIGNER_BENEFICIARY_FAIL';

const CLOSE_PANEL_ERROR = 'addBeneficiary/CLOSE_PANEL_ERROR';

const CANCEL_SIGNATURE = 'addBeneficiary/CANCEL_SIGNATURE';

const InitialState = {
  loaded: false,
  showAlertError: false,
  showBlockSign: false
};
export default function reducer(state = InitialState, action = {}) {
  switch (action.type) {
    case SAVE_BENEFICIARY:
      return {
        ...state,
        loading: true
      };
    case SAVE_BENEFICIARY_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        resultSave: action.result,
        showBlockSign: action.result.success ? true : false,
        showAlertError: !action.result.success && true,
        titre: 'ERREUR',
        codeErreur: action.result.codeErreur,
        error: null
      };
    case SAVE_BENEFICIARY_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
// SIGNER_BENEFICIARY
    case SIGNER_BENEFICIARY:
      return {
        ...state,
        loading: true
      };
    case SIGNER_BENEFICIARY_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        resultSigner: action.result,
        showBlockSign: action.result.success ? false : true,
        showAlertError: true,
        codeErreur: action.result.success ? 'Votre demande a été signer avec succes' : action.result.codeErreur,
        titre: action.result.success ? 'Success' : 'ERREUR',
        error: null
      };
    case SIGNER_BENEFICIARY_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error,
      };
// CLOSE_PANEL_ERROR
    case CLOSE_PANEL_ERROR:
      return {
        ...state,
        showAlertError: false,
      };
// CANCEL_SIGNATURE
    case CANCEL_SIGNATURE:
      {
        return {
          ...state,
          showBlockSign: false,
        };
      }
    default:
      return state;
  }
}
function objectToParams(object) {
  let str = '';
  for (const key in object) {
    if (str !== '') {
      str += '&';
    }
    str += key + '=' + encodeURIComponent(object[key]);
  }
  return str;
}
export function saveBeneficiary(beneficiaryFormObject, user) {
  const beneficiaryFormObjectClone = beneficiaryFormObject;
  beneficiaryFormObjectClone.radical = user.identifiantRC;
  beneficiaryFormObjectClone.id = beneficiaryFormObject.id;
  return {
    types: [SAVE_BENEFICIARY, SAVE_BENEFICIARY_SUCCESS, SAVE_BENEFICIARY_FAIL],
    promise: (client) => client.post('ordreValidationBeneficiaire/save', { data: objectToParams(beneficiaryFormObjectClone) })
  };
}
export function signerBeneficiary(id, taskId, password) {
  const params = { id, format: 'json', jpassword: password, taskId, version: '' };
  return {
    types: [SIGNER_BENEFICIARY, SIGNER_BENEFICIARY_SUCCESS, SIGNER_BENEFICIARY_FAIL],
    promise: (client) => client.get('ordreValidationBeneficiaire/signer?' + objectToParams(params))
  };
}
export function closePanelError() {
  return { type: CLOSE_PANEL_ERROR };
}
export function cancelSignature() {
  return { type: CANCEL_SIGNATURE };
}
