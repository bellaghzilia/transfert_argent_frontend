import React, { Component } from 'react';

export default class StatusComponent extends Component {
  render() {
    return (
      <div>
        {
          this.props.rowData.credit ?
            <div style={{ color: '#ed6b75' }} >{this.props.data}</div> :
            <div style={{ color: '#10cfbd' }} >{this.props.data}</div>}
      </div>
      );
  }
}
