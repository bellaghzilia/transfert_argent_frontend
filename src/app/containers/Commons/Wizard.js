import React, { Component } from "react";
import PropTypes from "prop-types";

import { Row, Col } from "react-bootstrap";
import { translate } from "react-i18next";

@translate(["common"], { wait: true })
export default class Wizard extends Component {
  render() {
    const style = require("./WizardStyle.scss");
    const { t } = this.props;
    const step = this.props.children;
    return (
      <Row className={style.wizardBloc + " rowCont"}>
        <Col xs={12} md={12}>
          <nav>
            {step === "stepOne" && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.visited + " " + style.firstIcon}>
                  <a>
                    <span className={style.fontStyleCurrentLabel}>
                      <i className="fa fa-angle-left" aria-hidden="true" />{" "}
                      {t("wizard.step1")}
                    </span>
                  </a>
                </li>
                <li className={style.secendIcon}>
                  <a>
                    <span className={style.fontStyle}>
                      {t("wizard.step2")}{" "}
                      <i className="fa fa-angle-right" aria-hidden="true" />
                    </span>
                  </a>
                </li>
                <li className={style.thirdIcon}>
                  <a>
                    <span className={style.fontStyle}>
                      <i className="fa fa-angle-left" aria-hidden="true" />{" "}
                      {t("wizard.step3")}
                    </span>
                  </a>
                </li>
              </ol>
            )}
            {step === "stepTwo" && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.visited + " " + style.firstIcon}>
                  <a>
                    <span>
                      <i className="fa fa-angle-left" aria-hidden="true" />
                      {t("wizard.step1")}
                    </span>
                  </a>
                </li>
                <li className={style.visited + " " + style.secendIcon}>
                  <a>
                    <span>
                      {t("wizard.step2")}{" "}
                      <i className="fa fa-angle-right" aria-hidden="true" />
                    </span>
                  </a>
                </li>
                <li className={style.thirdIcon}>
                  <a>
                    <span className={style.fontStyle}>
                      <i className="fa fa-angle-left" aria-hidden="true" />{" "}
                      {t("wizard.step3")}
                    </span>
                  </a>
                </li>
              </ol>
            )}
            {step === "stepThree" && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.visited + " " + style.firstIcon}>
                  <a>
                    <span>
                      <i className="fa fa-angle-left" aria-hidden="true" />{" "}
                      {t("wizard.step1")}
                    </span>
                  </a>
                </li>
                <li className={style.visited + " " + style.secendIcon}>
                  <a>
                    <span>
                      {t("wizard.step2")}{" "}
                      <i className="fa fa-angle-right" aria-hidden="true" />
                    </span>
                  </a>
                </li>
                <li className={style.visited + " " + style.thirdIcon}>
                  <a>
                    <span>
                      <i className="fa fa-angle-left" aria-hidden="true" />
                      {t("wizard.step3")}
                    </span>
                  </a>
                </li>
              </ol>
            )}
          </nav>
        </Col>
      </Row>
    );
  }
}
