import React, { Component } from "react";
import PropTypes from "prop-types";
import { Row, Col } from "react-bootstrap";
export default class WizardRegister extends Component {
  render() {
    const style = require("./WizardStyle.scss");
    const step = this.props.children;
    return (
      <Row className={style.noPadding}>
        <Col xs={12} md={12}>
          <nav>
            {step == 1 && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.visited}>
                  <a>Infos générale</a>
                </li>
                <li>
                  <em>
                    <span className={style.fontStyle}>Adresse</span>
                  </em>
                </li>
                <li>
                  <em className={style.fontStyle}>Pièces d'identité</em>
                </li>
                <li>
                  <em className={style.fontStyle}>Terms</em>
                </li>
              </ol>
            )}
            {step == 2 && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.visited}>
                  <a>Infos générale</a>
                </li>
                <li className={style.visited}>
                  <a>Adresse</a>
                </li>
                <li>
                  <em className={style.fontStyle}>Pièces identité</em>
                </li>
                <li>
                  <em className={style.fontStyle}>Terms</em>
                </li>
              </ol>
            )}
            {step == 3 && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.visited}>
                  <a>Infos générale</a>
                </li>
                <li className={style.visited}>
                  <a>Adresse</a>
                </li>
                <li className={style.visited}>
                  <a>Pièces identité </a>
                </li>
                <li>
                  <em className={style.fontStyle}>Terms</em>
                </li>
              </ol>
            )}
            {step == 4 && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.visited}>
                  <a>Infos générale</a>
                </li>
                <li className={style.visited}>
                  <a>Adresse</a>
                </li>
                <li className={style.visited}>
                  <a>Pièces identité </a>
                </li>
                <li className={style.visited}>
                  <a>Terms </a>
                </li>
              </ol>
            )}
          </nav>
        </Col>
      </Row>
    );
  }
}
