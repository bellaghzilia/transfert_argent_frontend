const LOAD_RESULT = 'NomEnClatureReducer/LOAD_RESULT_REQUEST';
const LOAD_RESULT_SUCCESS = 'NomEnClatureReducer/LOAD_RESULT_SUCCESSFUL';
const LOAD_RESULT_FAIL = 'NomEnClatureReducer/LOAD_RESULT_FAILURE';

const LOAD_NATIONALITY_RESULT = 'NomEnClatureReducer/LOAD_NATIONALITY_RESULT_REQUEST';
const LOAD_NATIONALITY_SUCCESS = 'NomEnClatureReducer/LOAD_NATIONALITY_RESULT_SUCCESSFUL';
const LOAD_NATIONALITY_FAIL = 'NomEnClatureReducer/LOAD_NATIONALITY_RESULT_FAILURE';

const LOAD_FS_RESULT = 'NomEnClatureReducer/LOAD_FS_RESULT_REQUEST';
const LOAD_FS_SUCCESS = 'NomEnClatureReducer/LOAD_FS_RESULT_SUCCESSFUL';
const LOAD_FS_FAIL = 'NomEnClatureReducer/LOAD_FS_RESULT_FAILURE';

const LOAD_HS_RESULT = 'NomEnClatureReducer/LOAD_HS_RESULT_REQUEST';
const LOAD_HS_SUCCESS = 'NomEnClatureReducer/LOAD_HS_RESULT_SUCCESSFUL';
const LOAD_HS_FAIL = 'NomEnClatureReducer/LOAD_HS_RESULT_FAILURE';

const LOAD_PROFESSION_RESULT = 'NomEnClatureReducer/LOAD_PROFESSION_RESULT_REQUEST';
const LOAD_PROFESSION_SUCCESS = 'NomEnClatureReducer/LOAD_PROFESSION_RESULT_SUCCESSFUL';
const LOAD_PROFESSION_FAIL = 'NomEnClatureReducer/LOAD_PROFESSION_RESULT_FAILURE';

const LOAD_CT_RESULT = 'NomEnClatureReducer/LOAD_CT_RESULT_REQUEST';
const LOAD_CT_SUCCESS = 'NomEnClatureReducer/LOAD_CT_RESULT_SUCCESSFUL';
const LOAD_CT_FAIL = 'NomEnClatureReducer/LOAD_CT_RESULT_FAILURE';

const LOAD_PAYS_RESULT = 'NomEnClatureReducer/LOAD_PAYS_RESULT_REQUEST';
const LOAD_PAYS_SUCCESS = 'NomEnClatureReducer/LOAD_PAYS_RESULT_SUCCESSFUL';
const LOAD_PAYS_FAIL = 'NomEnClatureReducer/LOAD_PAYS_RESULT_FAILURE';

const LOAD_CITY_RESULT = 'NomEnClatureReducer/LOAD_CITY_RESULT_REQUEST';
const LOAD_CITY_SUCCESS = 'NomEnClatureReducer/LOAD_CITY_RESULT_SUCCESSFUL';
const LOAD_CITY_FAIL = 'NomEnClatureReducer/LOAD_CITY_RESULT_FAILURE';

const LOAD_STATUTS = 'NomEnClatureReducer/LOAD_STATUTS_REQUEST';
const LOAD_STATUTS_SUCCESS = 'NomEnClatureReducer/LOAD_STATUTS_SUCCESSFUL';
const LOAD_STATUTS_FAIL = 'NomEnClatureReducer/LOAD_STATUTS_FAILURE';

const LOAD_DEVISE_RESULT = 'NomEnClatureReducer/LOAD_DEVISE_RESULT_REQUEST';
const LOAD_DEVISE_RESULT_SUCCESS = 'NomEnClatureReducer/LOAD_DEVISE_RESULT_SUCCESSFUL';
const LOAD_DEVISE_RESULT_FAIL = 'NomEnClatureReducer/LOAD_DEVISE_RESULT_FAILURE';

const LOAD_TYPES_RECHARGES_RESULT = 'NomEnClatureReducer/LOAD_TYPES_RECHARGES_RESULT_REQUEST';
const LOAD_TYPES_RECHARGES_RESULT_SUCCESS = 'NomEnClatureReducer/LOAD_TYPES_RECHARGES_RESULT_SUCCESSFUL';
const LOAD_TYPES_RECHARGES_RESULT_FAIL = 'NomEnClatureReducer/LOAD_TYPES_RECHARGES_RESULT_FAILURE';

const LOAD_PERIODICITE = 'NomEnClatureReducer/LOAD_PERIODICITE_REQUEST';
const LOAD_PERIODICITE_SUCCESS = 'NomEnClatureReducer/LOAD_PERIODICITE_SUCCESSFUL';
const LOAD_PERIODICITE_FAIL = 'NomEnClatureReducer/LOAD_PERIODICITE_FAILURE';

const LOAD_TYPES_BENEFICIARY = 'NomEnClatureReducer/LOAD_TYPES_BENEFICIARY_REQUEST';
const LOAD_TYPES_BENEFICIARY_SUCCESS = 'NomEnClatureReducer/LOAD_TYPES_BENEFICIARY_SUCCESSFUL';
const LOAD_TYPES_BENEFICIARY_FAIL = 'NomEnClatureReducer/LOAD_TYPES_BENEFICIARY_FAILURE';

const LOAD_GENRE_BENEFICIARY = 'NomEnClatureReducer/LOAD_GENRE_BENEFICIARY_REQUEST';
const LOAD_GENRE_BENEFICIARY_SUCCESS = 'NomEnClatureReducer/LOAD_GENRE_BENEFICIARY_SUCCESSFUL';
const LOAD_GENRE_BENEFICIARY_FAIL = 'NomEnClatureReducer/LOAD_GENRE_BENEFICIARY_FAILURE';

const LOAD_STATUT_CHEQUE_PAYE = 'NomEnClatureReducer/LOAD_STATUT_CHEQUE_PAYE';
const LOAD_STATUT_CHEQUE_PAYE_SUCCESS = 'NomEnClatureReducer/LOAD_STATUT_CHEQUE_PAYE_SUCCESS';
const LOAD_STATUT_CHEQUE_PAYE_FAIL = 'NomEnClatureReducer/LOAD_STATUT_CHEQUE_PAYE_FAIL';

const LOAD_STATUT_CHEQUE_CASH = 'NomEnClatureReducer/LOAD_STATUT_CHEQUE_CASH';
const LOAD_STATUT_CHEQUE_CASH_SUCCESS = 'NomEnClatureReducer/LOAD_STATUT_CHEQUE_CASH_SUCCESS';
const LOAD_STATUT_CHEQUE_CASH_FAIL = 'NomEnClatureReducer/LOAD_STATUT_CHEQUE_CASH_FAIL';

const LOAD_NATURE_CHEQUE = 'NomEnClatureReducer/LOAD_NATURE_CHEQUE';
const LOAD_NATURE_CHEQUE_SUCCESS = 'NomEnClatureReducer/LOAD_NATURE_CHEQUE_SUCCESS';
const LOAD_NATURE_CHEQUE_FAIL = 'NomEnClatureReducer/LOAD_NATURE_CHEQUE_FAIL';

const LOAD_TYPES_PIECES_IDENTITES = 'NomEnClatureReducer/"LOAD_TYPES_PIECES_IDENTITES';
const LOAD_TYPES_PIECES_IDENTITES_SUCCESS = 'NomEnClatureReducer/LOAD_TYPES_PIECES_IDENTITES_SUCCESS';
const LOAD_TYPES_PIECES_IDENTITES_FAIL = 'NomEnClatureReducer/LOAD_TYPES_PIECES_IDENTITES_FAIL';

const LOAD_STATUT_LCN_ENCAISSER = 'NomEnClatureReducer/LOAD_STATUT_LCN_ENCAISSER';
const LOAD_STATUT_LCN_ENCAISSER_SUCCESS = 'NomEnClatureReducer/LOAD_STATUT_LCN_ENCAISSER_SUCCESS';
const LOAD_STATUT_LCN_ENCAISSER_FAIL = 'NomEnClatureReducer/LOAD_STATUT_LCN_ENCAISSER_FAIL';

const LOAD_NATURE_REMISE = 'NomEnClatureReducer/LOAD_NATURE_REMISE';
const LOAD_NATURE_REMISE_SUCCESS = 'NomEnClatureReducer/LOAD_NATURE_REMISE_SUCCESS';
const LOAD_NATURE_REMISE_FAIL = 'NomEnClatureReducer/LOAD_NATURE_REMISE_FAIL';


export default function reducer(state = {}, action = {}) {
    switch (action.type) {
        // LOAD_STATUTS
        case LOAD_STATUTS:
            return {
                ...state,
            };
        case LOAD_STATUTS_SUCCESS:
            return {
                ...state,
                listeStatuts: action.result.statutList,
                error: null
            };
        case LOAD_STATUTS_FAIL:
            return {
                ...state,
                error: action.error
            };
        case LOAD_RESULT:
            return {
                ...state,
            };
        case LOAD_RESULT_SUCCESS:
            return {
                ...state,
                civilityList: action.result.civilityList,
                error: null
            };
        case LOAD_RESULT_FAIL:
            return {
                ...state,
                civilityList: null,
                error: action.error
            };
        case LOAD_NATIONALITY_RESULT:
            return {
                ...state,
            };
        case LOAD_NATIONALITY_SUCCESS:
            return {
                ...state,
                nationalityList: action.result.nationalityList,
                error: null
            };
        case LOAD_NATIONALITY_FAIL:
            return {
                ...state,
                nationalityList: null,
                error: action.error
            };
        case LOAD_FS_RESULT:
            return {
                ...state,
                loading: true
            };
        case LOAD_FS_SUCCESS:
            return {
                ...state,
                familySituationList: action.result.familySituationList,
                error: null
            };
        case LOAD_FS_FAIL:
            return {
                ...state,
                familySituationList: null,
                error: action.error
            };
        case LOAD_HS_RESULT:
            return {
                ...state,
            };
        case LOAD_HS_SUCCESS:
            return {
                ...state,
                housingSituationList: action.result.housingSituationList,
                error: null
            };
        case LOAD_HS_FAIL:
            return {
                ...state,
                housingSituationList: null,
                error: action.error
            };
        case LOAD_PROFESSION_RESULT:
            return {
                ...state,
            };
        case LOAD_PROFESSION_SUCCESS:
            return {
                ...state,
                professionList: action.result.professionList,
                error: null
            };
        case LOAD_PROFESSION_FAIL:
            return {
                ...state,
                professionList: null,
                error: action.error
            };
        case LOAD_CT_RESULT:
            return {
                ...state,
            };
        case LOAD_CT_SUCCESS:
            return {
                ...state,
                contractTypeList: action.result.contractTypeList,
                error: null
            };
        case LOAD_CT_FAIL:
            return {
                ...state,
                contractTypeList: null,
                error: action.error
            };
        case LOAD_PAYS_RESULT:
            return {
                ...state,
            };
        case LOAD_PAYS_SUCCESS:
            return {
                ...state,
                countryList: action.result.paysList,
                error: null
            };
        case LOAD_PAYS_FAIL:
            return {
                ...state,
                countryList: null,
                error: action.error
            };
        case LOAD_CITY_RESULT:
            return {
                ...state,
            };
        case LOAD_CITY_SUCCESS:
            return {
                ...state,
                cityList: action.result.cityList,
                error: null
            };
        case LOAD_CITY_FAIL:
            return {
                ...state,
                cityList: null,
                error: action.error
            };
        // DEVISE_RESULT
        case LOAD_DEVISE_RESULT:
            return {
                ...state,
            };
        case LOAD_DEVISE_RESULT_SUCCESS:
            return {
                ...state,
                devisesList: action.result.devisesList,
                error: null
            };
        case LOAD_DEVISE_RESULT_FAIL:
            return {
                ...state,
                devisesList: null,
                error: action.error
            };
        // TYPES_RECHARGES_RESULT
        case LOAD_TYPES_RECHARGES_RESULT:
            return {
                ...state,
            };
        case LOAD_TYPES_RECHARGES_RESULT_SUCCESS:
            return {
                ...state,
                typesRechargesDemandeCarteList: action.result.typeRechargeList,
                error: null
            };
        case LOAD_TYPES_RECHARGES_RESULT_FAIL:
            return {
                ...state,
                typesRechargesDemandeCarteList: null,
                error: action.error
            };
        case LOAD_PERIODICITE:
            return {
                ...state,
            };
        case LOAD_PERIODICITE_SUCCESS:
            return {
                ...state,
                periodiciteData: action.result.periodiciteList,
                error: null
            };
        case LOAD_PERIODICITE_FAIL:
            return {
                ...state,
                periodiciteData: null,
                error: action.error
            };
        //   LOAD_TYPES_BENEFICIARY
        case LOAD_TYPES_BENEFICIARY:
            return {
                ...state,
            };
        case LOAD_TYPES_BENEFICIARY_SUCCESS:
            return {
                ...state,
                beneficiaryTypeList: action.result.typesBeneficiaires,
                error: null
            };
        case LOAD_TYPES_BENEFICIARY_FAIL:
            return {
                ...state,
                benefificiaryTypeList: null,
                error: action.error
            };
// LOAD_GENRE_BENEFICIARY
        case LOAD_GENRE_BENEFICIARY: {
            return {
                ...state,
            };
        }
        case LOAD_GENRE_BENEFICIARY_SUCCESS:
            return {
                ...state,
                genreBeneficiaryList: action.result.genresBeneficiaires,
                error: null
            };
        case LOAD_GENRE_BENEFICIARY_FAIL:
            return {
                ...state,
                genreBeneficiaryList: null,
                error: action.error
            };
// LOAD_STATUT_CHEQUE_PAYE
        case LOAD_STATUT_CHEQUE_PAYE: {
            return {
                ...state,
            };
        }
        case LOAD_STATUT_CHEQUE_PAYE_SUCCESS:
            return {
                ...state,
                statutChequePayeList: action.result.statutChequePayerList,
                error: null
            };
        case LOAD_STATUT_CHEQUE_PAYE_FAIL:
            return {
                ...state,
                statutChequePayeList: null,
                error: action.error
            };
// LOAD_STATUT_CHEQUE_CASH
        case LOAD_STATUT_CHEQUE_CASH: {
            return {
                ...state,
            };
        }
        case LOAD_STATUT_CHEQUE_CASH_SUCCESS:
            return {
                ...state,
                statutChequeCashList: action.result.statutChequeEncaissementList,
                error: null
            };
        case LOAD_STATUT_CHEQUE_CASH_FAIL:
            return {
                ...state,
                statutChequeCashList: null,
                error: action.error
            };
        // LOAD_NATURE_CHEQUE
        case LOAD_NATURE_CHEQUE: {
            return {
                ...state,
            };
        }
        case LOAD_NATURE_CHEQUE_SUCCESS:
            return {
                ...state,
                natureChequeCashList: action.result.natureChequeList,
                error: null
            };
        case LOAD_NATURE_CHEQUE_FAIL:
            return {
                ...state,
                natureChequeCashList: null,
                error: action.error
            };
        // LOAD_NATURE_CHEQUE
        case LOAD_TYPES_PIECES_IDENTITES: {
            return {
                ...state,
            };
        }
        case LOAD_TYPES_PIECES_IDENTITES_SUCCESS:
            return {
                ...state,
                typePieceIdentiteList: action.result.typePieceIdentiteList,
                error: null
            };
        case LOAD_TYPES_PIECES_IDENTITES_FAIL:
            return {
                ...state,
                typePieceIdentiteList: null,
                error: action.error
            };

//LOAD_STATUT_LCN_ENCAISSER
        case LOAD_STATUT_LCN_ENCAISSER: {
            return {
                ...state,
            };
        }
        case LOAD_STATUT_LCN_ENCAISSER_SUCCESS:
            return {
                ...state,
                statutLCNEncaissementList: action.result.statutLCNEncaissementList,
                error: null
            };
        case LOAD_STATUT_LCN_ENCAISSER_FAIL:
            return {
                ...state,
                statutLCNEncaissementList: null,
                error: action.error
            };
        // LOAD_NATURE_REMISE
        case LOAD_NATURE_REMISE: {
            return {
                ...state,
            };
        }
        case LOAD_NATURE_REMISE_SUCCESS:
            return {
                ...state,
                natureRemiseList: action.result.naturesRemiseTelechargementFichierList,
                error: null
            };
        case LOAD_NATURE_REMISE_FAIL:
            return {
                ...state,
                natureRemiseList: null,
                error: action.error
            };
        default:
            return state;
    }
}

export function loadNomEnClature(nomEnClature) {
    let types = [];
    if (nomEnClature === 'civility') {
        types = [LOAD_RESULT, LOAD_RESULT_SUCCESS, LOAD_RESULT_FAIL];
    } else if (nomEnClature === 'nationality') {
        types = [LOAD_NATIONALITY_RESULT, LOAD_NATIONALITY_SUCCESS, LOAD_NATIONALITY_FAIL];
    } else if (nomEnClature === 'familySituation') {
        types = [LOAD_FS_RESULT, LOAD_FS_SUCCESS, LOAD_FS_FAIL];
    } else if (nomEnClature === 'housingSituation') {
        types = [LOAD_HS_RESULT, LOAD_HS_SUCCESS, LOAD_HS_FAIL];
    } else if (nomEnClature === 'profession') {
        types = [LOAD_PROFESSION_RESULT, LOAD_PROFESSION_SUCCESS, LOAD_PROFESSION_FAIL];
    } else if (nomEnClature === 'contractType') {
        types = [LOAD_CT_RESULT, LOAD_CT_SUCCESS, LOAD_CT_FAIL];
    } else if (nomEnClature === 'getPays') {
        types = [LOAD_PAYS_RESULT, LOAD_PAYS_SUCCESS, LOAD_PAYS_FAIL];
    } else if (nomEnClature === 'city') {
        types = [LOAD_CITY_RESULT, LOAD_CITY_SUCCESS, LOAD_CITY_FAIL];
    } else if (nomEnClature === 'getDevises') {
        types = [LOAD_DEVISE_RESULT, LOAD_DEVISE_RESULT_SUCCESS, LOAD_DEVISE_RESULT_FAIL];
    } else if (nomEnClature === 'getTypesRechargesDemandeCarte') {
        types = [LOAD_TYPES_RECHARGES_RESULT, LOAD_TYPES_RECHARGES_RESULT_SUCCESS, LOAD_TYPES_RECHARGES_RESULT_FAIL];
    } else if (nomEnClature === 'getPeriodicites') {
        types = [LOAD_PERIODICITE, LOAD_PERIODICITE_SUCCESS, LOAD_PERIODICITE_FAIL];
    } else if (nomEnClature === 'getStatuts') {
        types = [LOAD_STATUTS, LOAD_STATUTS_SUCCESS, LOAD_STATUTS_FAIL];
    } else if (nomEnClature === 'getTypesBeneficiaires') {
        types = [LOAD_TYPES_BENEFICIARY, LOAD_TYPES_BENEFICIARY_SUCCESS, LOAD_TYPES_BENEFICIARY_FAIL];
    } else if (nomEnClature === 'getGenresBeneficiaires') {
        types = [LOAD_GENRE_BENEFICIARY, LOAD_GENRE_BENEFICIARY_SUCCESS, LOAD_GENRE_BENEFICIARY_FAIL];
    } else if (nomEnClature === 'getStatutChequePayer') {
        types = [LOAD_STATUT_CHEQUE_PAYE, LOAD_STATUT_CHEQUE_PAYE_SUCCESS, LOAD_STATUT_CHEQUE_PAYE_FAIL];
    } else if (nomEnClature === 'getStatutChequeEncaissement') {
        types = [LOAD_STATUT_CHEQUE_CASH, LOAD_STATUT_CHEQUE_CASH_SUCCESS, LOAD_STATUT_CHEQUE_CASH_FAIL];
    } else if (nomEnClature === 'getNatureCheque') {
        types = [LOAD_NATURE_CHEQUE, LOAD_NATURE_CHEQUE_SUCCESS, LOAD_NATURE_CHEQUE_FAIL];
    } else if (nomEnClature === 'getTypesPiecesIdentites') {
        types = [LOAD_TYPES_PIECES_IDENTITES, LOAD_TYPES_PIECES_IDENTITES_SUCCESS, LOAD_TYPES_PIECES_IDENTITES_FAIL];
    } else if(nomEnClature === 'getStatutLCNEncaissement' ){
        types = [LOAD_STATUT_LCN_ENCAISSER, LOAD_STATUT_LCN_ENCAISSER_SUCCESS, LOAD_STATUT_LCN_ENCAISSER_FAIL];
    } else if (nomEnClature === 'getNaturesRemiseTelechargementFichier'){
        types = [LOAD_NATURE_REMISE, LOAD_NATURE_REMISE_SUCCESS, LOAD_NATURE_REMISE_FAIL];
    }
    return {
        types,
        promise: (client) => client.get('nomenclatureCodification/' + nomEnClature)
    };
}

export function loadNomEnClatureLoaded(globalState, nomEnClature) {
    if (nomEnClature === 'getDevises') {
        return globalState.nomEnClature.devisesList && globalState.nomEnClature.loaded;
    } else if (nomEnClature === 'getStatuts') {
        return globalState.nomEnClature.listeStatuts && globalState.nomEnClature.loaded;
    }
}