const isEmpty = value => value === undefined || value === null || value === '' || !value.toString().length;
const join = (rules) => (value, data) => rules.map(rule => rule(value, data)).filter(error => !!error)[0];

export function email(value) {
    // Let's not start a debate on email regex. This is just for an example app!
    if (!isEmpty(value) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
        return 'Adresse email non valide';
    }
}

export function phone(value){
    if (!isEmpty(value) && !/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/i.test(value)) {
        return 'Téléphone non valide';
    }
}

export function isValidNumber(value) {
    if (!isEmpty(value) && !/^(0\.[0-9]+|[0-9]+(\.[0-9]+)?)$/i.test(value)) {
        return 'La valeur saisi n\'est pas valide';
    }
}

export function required(value) {
    if (isEmpty(value)) {
        return 'Ce champ est obligatoire';
    }
}

export function champOb (value) {
    if (value === "Choisir Compte :") {
        return 'Ce champ est obligatoire';
    }
}

export function notNumber(value) {
    if (isNaN(value)) {
        return 'entrer un nombre';
    }
}

export function minValue(min) {
    return value => {
        if (!isEmpty(value) && Number(value) < Number(min)) {
            return `il faut une valeur supérieur ou égal à ${min}`;
        }
    };
}

export function minLength(min) {
    return value => {
        if (!isEmpty(value) && value.length < min) {
            return `il faut au moin ${min} characters`;
        }
    };
}

export function maxLength(max) {
    return value => {
        if (!isEmpty(value) && value.length > max) {
            return `Oups ! Le motif saisi dépasse les ${max} caractères`;
        }
    };
}

export function integer(value) {
    if (!Number.isInteger(Number(value))) {
        return 'Nombre non valide';
    }
}

export function oneOf(enumeration) {
    return value => {
        if (!~enumeration.indexOf(value)) {
            return `Must be one of: ${enumeration.join(', ')}`;
        }
    };
}

export function match(field) {
    return (value, data) => {
        if (data) {
            if (value !== data[field]) {
                return 'Do not match';
            }
        }
    };
}

export function matchPassword(field) {
    return (value, data) => {
        if (data) {
            if (value !== data[field]) {
                return ' Nouveau mot de passe et confirmation de mot de passe ne sont pas identiques';
            }
        }
    };
}

export function moreThan(field) {
    return (value, data) => {
        if (data) {
            if (value < data[field] && data[field] !== '') {
                return 'La valeur minimum de ce champ doit être de ' + data[field];
            }
        }
    };
}

export function lessThan(field) {
    return (value, data) => {
        if (data) {
            if (value > data[field] && data[field] !== '') {
                return 'La valeur maximum de ce champ doit être inférieur à votre solde : ' + data[field];
            }
        }
    };
}

export function lessThanAmount(field) {
    return (value, data) => {
        if (data) {
            if (value > data[field] && data[field] !== '') {
                return 'Le montant saisie est supérieur à votre solde ( ' + data[field] +' )';
            }
        }
    };
}

export function moreThanForTextInput(field) {
    return (value, data) => {
        if (data) {
            if ((parseFloat(value) < parseFloat(data[field])) && data[field] !== '') {
                return 'La valeur minimum de ce champ doit être de ' + data[field];
            }
        }
    };
}

export function lessThanForTextInput(field) {
    return (value, data) => {
        if (data) {
            if (parseFloat(value) > parseFloat(data[field]) && data[field] !== '') {
                return 'La valeur maximum de ce champ doit être de ' + data[field];
            }
        }
    };
}

export function isValidAmount(value) {
  if (value && value !== '' && !/^\d+(\.\d{1,2})?$/.test(value)) {
    return 'Merci de saisir un montant composé uniquement de chiffres';
  }
}

export function isValidMontant(value) {
    if (value && value !== '' && !/(^\d+(\.\d{1,2})+$)|^[1-9]+/.test(value)) {
      return 'Saisissez une valeur différente de 0';
    }
  }

export function isValidMotif(value) {
    if (value && value !== '' && !/^[A-Za-z \u00C0-\u017F\.\,\-]+$/i.test(value.trim())) {
        return 'Saisie incorrecte';
    }
}

export function rib(value) {
    if (!isEmpty(value) && (!Number.isInteger(Number(value)) || value.trim().length !=  24)) {
        return 'RIB non valide';
    }
}

export function createValidator(rules) {
    return (data = {}) => {
        const errors = {};
        Object.keys(rules).forEach((key) => {
            const rule = join([].concat(rules[key])); // concat enables both functions and arrays of functions
            const error = rule(data[key], data);
            if (error) {
                errors[key] = error;
            }
        });
        return errors;
    };
}

export function customCreateValidator(mapValuesAndPropsToRules) {
	  return (values, props) => {
	    let rules;
	    if (typeof mapValuesAndPropsToRules === 'function') {
	      rules = mapValuesAndPropsToRules(values, props);
	    } else {
	      rules = mapValuesAndPropsToRules;
	    }
	    return validate(rules, values);
	  };
	}

function validate(rules, data) {
	  const errors = {};
	  Object.keys(rules).forEach((key) => {
	    const rule = join([].concat(rules[key]));
	    const error = rule(data[key], data);
	    if (error) {
	      errors[key] = error;
	    }
	  });
	  return errors;
	}