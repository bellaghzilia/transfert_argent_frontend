import React, {Component, PropTypes} from 'react';
import {Row, Col} from 'react-bootstrap';
import {translate} from 'react-i18next';

@translate(['common'], {wait: true})
export default class WizardTiersPPcomplete extends Component {

    render() {
        const style = require('./WizardStyle.scss');
        const {t} = this.props;
        const step = this.props.children;
        return (
            <Row className={style.noPadding + ' rowCont'}>
                <Col xs={12} md={12}>
                    <nav>
                        { step === 'stepOne' &&
                        <ol className={style.cdMultiSteps + ' ' + style.textBottom + ' ' + style.count}>
                            <li className={style.visited}>
                                <a >
                                    <span>
                                        <i className="fa fa-angle-left" aria-hidden="true"/>{t('wizard.step1')}
                                        </span>
                                </a>
                            </li>
                            <li>
                                <a className={style.fontStyle}>
                                    <span>
                                        <i className="fa fa-angle-left" aria-hidden="true"/>{t('wizard.step3')}
                                    </span>
                                </a>
                            </li>
                        </ol>
                        }
                        { step === 'stepTwo' &&
                        <ol className={style.cdMultiSteps + ' ' + style.textBottom + ' ' + style.count}>
                            <li className={style.visited}>
                                <a >
                                    <span>
                                        <i className="fa fa-angle-left" aria-hidden="true"/>{t('wizard.step1')}
                                    </span>
                                </a>
                            </li>
                            <li className={style.visited}>
                                <a >
                                    <span>
                                        <i className="fa fa-angle-left" aria-hidden="true"/>{t('wizard.step3')}
                                </span>
                                </a>
                            </li>
                        </ol>
                        }
                    </nav>
                </Col>
            </Row>
        );
    }
}
