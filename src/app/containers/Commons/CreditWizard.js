import React, { Component } from "react";
import PropTypes from "prop-types";
import { Row, Col } from "react-bootstrap";
export default class CreditWizard extends Component {
  constructor(props) {
    super(props);
    this.switchStep = this.switchStep.bind(this);
  }
  switchStep(step) {
    this.props.handleSwitchStep(step);
  }

  render() {
    const style = require("./WizardStyle.scss");
    const { step, user, handleSwitchStep, creditIdentifier } = this.props;
    const step5Label =
      typeof user !== "undefined" && user.typeProfile === "CL"
        ? "Réponse finale"
        : typeof user !== "undefined" && user.typeProfile === "BO"
        ? "Qualification et êtude"
        : "";
    return (
      <Row className={style.noPadding}>
        <Col xs={12} md={12}>
          <nav>
            {step === 1 && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.visited}>
                  <a>Simulation</a>
                </li>
                {creditIdentifier ? (
                  <li className={style.previousSteps}>
                    <a
                      onClick={(e) => {
                        e.preventDefault();
                        this.switchStep(2);
                      }}
                    >
                      Votre situation
                    </a>
                  </li>
                ) : (
                  <li>
                    <em>
                      <span className={style.fontStyle}>Votre situation</span>
                    </em>
                  </li>
                )}

                {creditIdentifier ? (
                  <li className={style.previousSteps}>
                    <a
                      onClick={(e) => {
                        e.preventDefault();
                        this.switchStep(3);
                      }}
                    >
                      Réponse préalable
                    </a>
                  </li>
                ) : (
                  <li>
                    <em className={style.fontStyle}>Réponse préalable</em>
                  </li>
                )}

                {typeof user !== "undefined" && creditIdentifier ? (
                  <li className={style.previousSteps}>
                    <a
                      onClick={(e) => {
                        e.preventDefault();
                        this.switchStep(4);
                      }}
                    >
                      Vos documents
                    </a>
                  </li>
                ) : (
                  <li>
                    <em className={style.fontStyle}>Vos documents</em>
                  </li>
                )}
                {typeof user !== "undefined" && (
                  <li>
                    <em className={style.fontStyle}>{step5Label}</em>
                  </li>
                )}
              </ol>
            )}
            {step === 2 && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.previousSteps}>
                  <a
                    onClick={(e) => {
                      e.preventDefault();
                      this.switchStep(1);
                    }}
                  >
                    Simulation
                  </a>
                </li>
                <li className={style.visited}>
                  <a>Votre situation</a>
                </li>
                {creditIdentifier ? (
                  <li className={style.previousSteps}>
                    <a
                      onClick={(e) => {
                        e.preventDefault();
                        this.switchStep(3);
                      }}
                    >
                      Réponse préalable
                    </a>
                  </li>
                ) : (
                  <li>
                    <em className={style.fontStyle}>Réponse préalable</em>
                  </li>
                )}
                {typeof user !== "undefined" && creditIdentifier ? (
                  <li className={style.previousSteps}>
                    <a
                      onClick={(e) => {
                        e.preventDefault();
                        this.switchStep(4);
                      }}
                    >
                      Vos documents
                    </a>
                  </li>
                ) : (
                  <li>
                    <em className={style.fontStyle}>Vos documents</em>
                  </li>
                )}
                {typeof user !== "undefined" && (
                  <li>
                    <em className={style.fontStyle}>{step5Label}</em>
                  </li>
                )}
              </ol>
            )}
            {step === 3 && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.previousSteps}>
                  <a
                    onClick={(e) => {
                      e.preventDefault();
                      this.switchStep(1);
                    }}
                  >
                    Simulation
                  </a>
                </li>
                <li className={style.previousSteps}>
                  <a
                    onClick={(e) => {
                      e.preventDefault();
                      this.switchStep(2);
                    }}
                  >
                    Votre situation
                  </a>
                </li>
                <li className={style.visited}>
                  <em className={style.fontStyle}>Réponse préalable</em>
                </li>
                {typeof user !== "undefined" && creditIdentifier ? (
                  <li className={style.previousSteps}>
                    <a
                      onClick={(e) => {
                        e.preventDefault();
                        this.switchStep(4);
                      }}
                    >
                      Vos documents
                    </a>
                  </li>
                ) : (
                  <li>
                    <em className={style.fontStyle}>Vos documents</em>
                  </li>
                )}
                {typeof user !== "undefined" && (
                  <li>
                    <em className={style.fontStyle}>{step5Label}</em>
                  </li>
                )}
              </ol>
            )}
            {step === 4 && typeof user !== "undefined" && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.previousSteps}>
                  <a
                    onClick={(e) => {
                      e.preventDefault();
                      this.switchStep(1);
                    }}
                  >
                    Simulation
                  </a>
                </li>
                <li className={style.previousSteps}>
                  <a
                    onClick={(e) => {
                      e.preventDefault();
                      this.switchStep(2);
                    }}
                  >
                    Votre situation
                  </a>
                </li>
                <li className={style.previousSteps}>
                  <a
                    onClick={(e) => {
                      e.preventDefault();
                      this.switchStep(3);
                    }}
                  >
                    Réponse préalable
                  </a>
                </li>
                <li className={style.visited}>
                  <em className={style.fontStyle}>Vos documents</em>
                </li>
                <li>
                  <em className={style.fontStyle}>{step5Label}</em>
                </li>
              </ol>
            )}
            {step === 5 && typeof user !== "undefined" && (
              <ol
                className={
                  style.cdMultiSteps +
                  " " +
                  style.textBottom +
                  " " +
                  style.count
                }
              >
                <li className={style.previousSteps}>
                  <a
                    onClick={(e) => {
                      e.preventDefault();
                      this.switchStep(1);
                    }}
                  >
                    Simulation
                  </a>
                </li>
                <li className={style.previousSteps}>
                  <a
                    onClick={(e) => {
                      e.preventDefault();
                      this.switchStep(2);
                    }}
                  >
                    Votre situation
                  </a>
                </li>
                <li className={style.previousSteps}>
                  <a
                    onClick={(e) => {
                      e.preventDefault();
                      this.switchStep(3);
                    }}
                  >
                    Réponse préalable
                  </a>
                </li>
                <li className={style.previousSteps}>
                  <a
                    onClick={(e) => {
                      e.preventDefault();
                      this.switchStep(4);
                    }}
                  >
                    Vos documents
                  </a>
                </li>
                <li className={style.visited}>
                  <em className={style.fontStyle}>{step5Label}</em>
                </li>
              </ol>
            )}
          </nav>
        </Col>
      </Row>
    );
  }
}
