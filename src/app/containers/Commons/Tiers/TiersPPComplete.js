/**
 * Created by makthink on 14/02/17.
 */
import React, { Component } from 'react';

export default class TiersPPComplete extends Component {

    render() {
        const {
            fields: {
                civilite, nom, prenom,nomJeuneFilleOuEpouse, typePieceIdentite, numeroPieceIdentite,codeGECli, matGECli,
                dateNaissanceString, villeNaissance, situationFamiliale, nombreEnfants, situationLogement,raisonSociale,
                numeroRegistreDeCommerce, codeTribunal, societeOuEE,adressePricipale, adresse2Pricipale,
                villePrincipale,codePostalAdressePrincipale, nationalite,telephoneFixe, telephoneProfessionnel, telephonegsm,
                fax, mailPrincipal, codeSecteurActivite, profession, ribCli, nomAgenceCli,
                nomConjoint, prenomConjoint, typePieceIdentConjoint, numPieceIdentConjoint, lieuNaissConjoint, nationaliteConjoint,
                professionConjoint, employeurConjoint, revenuConjointMensuelNet, telConjoint,
                nomEmployeur, typeContrat , telephonePrincipal, paysPrincipal },
            t, action, creditSituationConfig, creditSituationValues, creditFields,creditSimulatorValues, saveThirdParty ,values,
            saveSuccess, showSaveErrorModal, saveMessage, hideSaveErrorModal, eligibility, invalid
        } = this.props;
        const globalStyles = require('../styles.scss');
        const styles = require('./Credit.scss');
        /**DEMANDE**/
        let flagConjointSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'FLAG_CONJOINT');
        /**DEMANDE**/
//	let natCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'NAT_CLI');
        let qualiteCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'QUALITE_CLI');
        let nomCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'NOM_CLI');
        let prenomCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'PRENOM_CLI');
        let nomJfCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'NOM_JF_CLI');
        let typeIdentiteCli =  creditFields.find((creditField) =>  creditField.fieldCode == 'TYPE_PIECE_IDENTITE_CLI');
        let cinCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'CIN_CLI');
        let dateCinCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'DATE_CIN_CLI');
        let codeGECliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'CODE_GE_CLI');
        let matGECliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'MAT_GE_CLI');
        let dateNaisCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'DATE_NAIS_CLI');
        let lieuNaisCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'LIEU_NAIS_CLI');
        let etCivilCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'ET_CIVIL_CLI');
        let nbrEnfCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'NBR_ENF_CLI');
        let logCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'LOG_CLI');
        let raisSocCli = creditFields.find((creditField) =>  creditField.fieldCode == 'RAIS_SOC_CLI');
        let regCommCli = creditFields.find((creditField) =>  creditField.fieldCode == 'REG_COMM_CLI');
        let codeTribunalCli = creditFields.find((creditField) =>  creditField.fieldCode == 'CODE_TRIBUNAL_CLI');
        let typeEntrCli = creditFields.find((creditField) =>  creditField.fieldCode == 'TYPE_ENTR_CLI');
        let dateCreatEntr = creditFields.find((creditField) =>  creditField.fieldCode == 'DATE_CREAT_ENTR');
        let adr1L1CliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'ADR1_L1_CLI');
        let adr1L2CliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'ADR1_L2_CLI');
        let ville1CliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'VILLE1_CLI');
        let cp1CliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'CP1_CLI');
        let nationaliteCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'NATIONALITE_CLI');
        let telDomCli = creditFields.find((creditField) =>  creditField.fieldCode == 'TEL_DOM_CLI');
        let telBurCli = creditFields.find((creditField) =>  creditField.fieldCode == 'TEL_BUR_CLI');
        let telPortCli = creditFields.find((creditField) =>  creditField.fieldCode == 'TEL_PORT_CLI');
        let faxCli = creditFields.find((creditField) =>  creditField.fieldCode == 'FAX_CLI');
        let emailCli = creditFields.find((creditField) =>  creditField.fieldCode == 'EMAIL_CLI');
        let sectAct = creditFields.find((creditField) =>  creditField.fieldCode ==  'SECT_ACT_CLI');
        let professionCli = creditFields.find((creditField) =>  creditField.fieldCode == 'PROFESSION_CLI');
// let natEntrCli = creditFields.find((creditField) => creditField.fieldCode ==
// 'NAT_ENTR_CLI');
        let revNetCli = creditFields.find((creditField) =>  creditField.fieldCode == 'REV_NET_CLI');
        let autRevCli = creditFields.find((creditField) =>  creditField.fieldCode == 'AUT_REV_CLI');
        let fraisImmCli = creditFields.find((creditField) =>  creditField.fieldCode == 'FRAIS_IMM_CLI');
        let autresChargesMensu = creditFields.find((creditField) =>  creditField.fieldCode == 'AUTRES_CHARGES_MENSU');
        let ribCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'RIB_CLI');
        let nomAgenceCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'NOM_AGENCE_CLI');
        let dateCptCliSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'DATE_CPT_CLI');
        /** Conjoint* */
        let nomCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'NOM_CNJ');
        let prenomCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'PRENOM_CNJ');
        let typeIdentCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'TYPE_PIECE_IDENT_CNJ');
        let numIdentCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'CIN_CNJ');
        let dateIdentCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'DATE_CIN_CNJ');
        let dateNaisCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'DATE_NAIS_CNJ');
        let lieuNaisCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'LIEU_NAIS_CNJ');
        let nationaliteCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'NATIONALITE_CNJ');
        let professionCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'PROFESSION_CNJ');
        let employeurCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'EMPLOYEUR_CNJ');
        let dateEmbCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'DATE_EMB_CNJ');
        let revNetCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'REV_NET_CNJ');
        let telCnjSituation = creditFields.find((creditField) =>  creditField.fieldCode == 'TEL_CNJ');
        /** Conjoint* */

        const saveErrorModal = (
            <Modal show={showSaveErrorModal} onHide={this.closeSaveErrorModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Error</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>{saveMessage}</h4>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.closeSaveErrorModal}>OK</Button>
                </Modal.Footer>
            </Modal>
        );

        const eligibilityAlert = (
            <Alert bsStyle={eligibility ? "success" :  "danger"}>
                {eligibility ? t('credit.alert.eligibility.ok') : t('credit.alert.eligibility.ko') }
            </Alert>
        );

        console.log(invalid);

        return (
            <form onSubmit={this.onSubmit}>
                {saveErrorModal}
                {saveSuccess && eligibilityAlert}
                <Accordion className={styles.recapitulatifContainer}>
                    <Panel header="Demande" eventKey="1" className={styles.accordionPanel}>
                        {flagConjointSituation && flagConjointSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(flagConjointSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                <Checkbox checked={this.state.flagConjoint} onChange={this.handleChangeFlagConjoint} readOnly={!flagConjointSituation.editableBO} />
                            </Col>
                        </Row>}
                    </Panel>
                    <Panel header="Client" eventKey="2" className={styles.accordionPanel}>
                        {qualiteCliSituation && qualiteCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(qualiteCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...civilite} disabled={!qualiteCliSituation.editableBO}>
                                            <option value="">{t(String(qualiteCliSituation.labelCode))}</option>
                                            {
                                                creditSituationConfig.civilities && creditSituationConfig.civilities.length && creditSituationConfig.civilities.map((civility) =>
                                                    <option key={civility.id} value={civility.code}>{civility.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {civilite.error && civilite.touched && <div className="text-danger">{civilite.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.civilityFomatted} </span>
                                }
                            </Col>
                        </Row>
                        }

                        {nomCliSituation && nomCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(nomCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...nom} readOnly={!nomCliSituation.editableBO} />
                                        {nom.error && nom.touched && <div className="text-danger">{nom.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.nom}</span>
                                }
                            </Col>
                        </Row>}

                        {prenomCliSituation && prenomCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(prenomCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...prenom} readOnly={!prenomCliSituation.editableBO} />
                                        {prenom.error && prenom.touched && <div className="text-danger">{prenom.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow} > {creditSituationValues.prenom} </span>
                                }
                            </Col>
                        </Row>
                        }
                        {nomJfCliSituation && nomJfCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(nomJfCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...nomJeuneFilleOuEpouse} readOnly={!nomJfCliSituation.editableBO} />
                                        {nomJeuneFilleOuEpouse.error && nomJeuneFilleOuEpouse.touched &&
                                        <div className="text-danger">{nomJeuneFilleOuEpouse.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.nomJeuneFilleOuEpouse}</span>
                                }
                            </Col>
                        </Row>
                        }
                        {typeIdentiteCli && typeIdentiteCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(typeIdentiteCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...typePieceIdentite} disabled={!typeIdentiteCli.editableBO}>
                                            <option value="">{t(String(qualiteCliSituation.labelCode))}</option>
                                            {
                                                creditSituationConfig.identityTypes && creditSituationConfig.identityTypes.length
                                                && creditSituationConfig.identityTypes.map((identityType) =>
                                                    <option key={identityType.id} value={identityType.code}>{identityType.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {typePieceIdentite.error && typePieceIdentite.touched &&
                                        <div className="text-danger">{typePieceIdentite.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.typePieceIdentiteFormatted} </span>
                                }
                            </Col>
                        </Row>}

                        {cinCliSituation && cinCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(cinCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...numeroPieceIdentite} readOnly={!cinCliSituation.editableBO} />
                                        {numeroPieceIdentite.error && numeroPieceIdentite.touched &&
                                        <div className="text-danger">{numeroPieceIdentite.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.numeroPieceIdentite}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {dateCinCliSituation && dateCinCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t('credit.field.dateCinCli')}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <DatePicker
                                        selected={this.state.dateCinCli}
                                        className={globalStyles.datePickerFormControl}
                                        isClearable="true"
                                        disabled={!dateCinCliSituation.editableBO}
                                        onChange={this.handleChangeDateCinCli}
                                        dateFormat="DD/MM/YYYY"
                                    />
                                    : <span className={styles.pullLeftShow} > {creditSituationValues.dateCinCliString} </span>
                                }
                            </Col>
                        </Row>
                        }

                        {codeGECliSituation && codeGECliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(codeGECliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...codeGECli}>
                                            {
                                                creditSituationConfig.imputations && creditSituationConfig.imputations.length &&
                                                creditSituationConfig.imputations.map((imputation) =>
                                                    <option value={imputation.code}>{imputation.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {codeGECli.error && codeGECli.touched &&
                                        <div className="text-danger">{codeGECli.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.codeGECli}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {matGECliSituation && matGECliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(matGECliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...matGECli} readOnly={!matGECliSituation.editableBO} />
                                        {matGECli.error && matGECli.touched && <div className="text-danger">{matGECli.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.matGECli}</span>
                                }
                            </Col>
                        </Row>}

                        {dateNaisCliSituation && dateNaisCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(dateNaisCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <DatePicker
                                        selected={this.state.birthDay}
                                        className={globalStyles.datePickerFormControl}
                                        isClearable="true"
                                        disabled={!dateNaisCliSituation.editableBO}
                                        onChange={this.handleChangeBirthDay}
                                        dateFormat="DD-MM-YYYY"
                                    />
                                    : <span className={styles.pullLeftShow} > {creditSituationValues.dateNaissanceString} </span>
                                }
                            </Col>
                        </Row>
                        }

                        {lieuNaisCliSituation && lieuNaisCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(lieuNaisCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...villeNaissance} readOnly={!lieuNaisCliSituation.editableBO} />
                                        {villeNaissance.error && villeNaissance.touched && <div className="text-danger">{villeNaissance.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.villeNaissance}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {etCivilCliSituation && etCivilCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(etCivilCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...situationFamiliale}>
                                            <option value="">Veuillez choisir la nationalité</option>
                                            {
                                                creditSituationConfig.familySituations && creditSituationConfig.familySituations.length &&
                                                creditSituationConfig.familySituations.map((familySituation) =>
                                                    <option value={familySituation.code}>{familySituation.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {situationFamiliale.error && situationFamiliale.touched && <div className="text-danger">{situationFamiliale.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.familySituationFormatted}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {nbrEnfCliSituation && nbrEnfCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(nbrEnfCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...nombreEnfants} />
                                        {nombreEnfants.error && nombreEnfants.touched && <div className="text-danger">{nombreEnfants.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.nombreEnfants}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {logCliSituation && logCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(logCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...situationLogement}>
                                            <option value="">{t(String(logCliSituation.labelCode))}</option>
                                            {
                                                creditSituationConfig.housingSituations && creditSituationConfig.housingSituations.length &&
                                                creditSituationConfig.housingSituations.map((housingSituation) =>
                                                    <option value={housingSituation.code}>{housingSituation.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {situationLogement.error && situationLogement.touched && <div className="text-danger">{situationLogement.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.housingSituationFormatted}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {raisSocCli && raisSocCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(raisSocCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...raisonSociale} />
                                        {raisonSociale.error && raisonSociale.touched && <div className="text-danger">{raisonSociale.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.raisonSociale}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {regCommCli && regCommCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(regCommCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...numeroRegistreDeCommerce} />
                                        {numeroRegistreDeCommerce.error && numeroRegistreDeCommerce.touched &&
                                        <div className="text-danger">{numeroRegistreDeCommerce.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.numeroRegistreDeCommerce}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {codeTribunalCli && codeTribunalCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(codeTribunalCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...codeTribunal}>
                                            {creditSituationConfig.companyNatures && creditSituationConfig.companyNatures.length &&
                                            creditSituationConfig.companyNatures.map((companyNature) =>
                                                <option key={companyNature.id} value={companyNature.code}>{companyNature.libelle}</option>
                                            )
                                            }
                                        </FormControl>
                                        {codeTribunal.error && codeTribunal.touched && <div className="text-danger">{codeTribunal.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.codeTribunalFormatted}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {typeEntrCli && typeEntrCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(typeEntrCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...societeOuEE}>
                                            {creditSituationConfig.companyTypes && creditSituationConfig.companyTypes.length &&
                                            creditSituationConfig.companyTypes.map((companyType) =>
                                                <option key={companyType.id} value={companyType.code}>{companyType.libelle}</option>
                                            )
                                            }
                                        </FormControl>
                                        {societeOuEE.error && societeOuEE.touched && <div className="text-danger">{societeOuEE.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.societeOuEEFormatted}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {dateCreatEntr && dateCreatEntr.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(dateCreatEntr.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <DatePicker
                                        selected={this.state.companyCreationDate}
                                        className={globalStyles.datePickerFormControl}
                                        isClearable="true"
                                        onChange={this.handleChangeCompanyCreationDate}
                                        dateFormat="DD/MM/YYYY"
                                    />
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.dateCreationSocieteString}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {adr1L1CliSituation && adr1L1CliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(adr1L1CliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...adressePricipale} />
                                        {adressePricipale.error && adressePricipale.touched && <div className="text-danger">{adressePricipale.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.adressePricipale}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {adr1L2CliSituation && adr1L2CliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(adr1L2CliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...adresse2Pricipale} />
                                        {adresse2Pricipale.error && adresse2Pricipale.touched && <div className="text-danger">{adresse2Pricipale.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.adresse2Pricipale}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {ville1CliSituation && ville1CliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(ville1CliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...villePrincipale}>
                                            <option value="">Veuillez choisir votre ville</option>
                                            {creditSituationConfig.cities && creditSituationConfig.cities.length &&
                                            creditSituationConfig.cities.map((city) =>
                                                <option key={city.id} value={city.code}>{city.libelle}</option>
                                            )
                                            }
                                        </FormControl>
                                        {villePrincipale.error && villePrincipale.touched && <div className="text-danger">{villePrincipale.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.cityFormatted}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {cp1CliSituation && cp1CliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(cp1CliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...codePostalAdressePrincipale} />
                                        {codePostalAdressePrincipale.error && codePostalAdressePrincipale.touched &&
                                        <div className="text-danger">{codePostalAdressePrincipale.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.codePostalAdressePrincipale}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {nationaliteCliSituation && nationaliteCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(nationaliteCliSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...nationalite}>
                                            <option value="">Veuillez choisir la nationalité</option>
                                            {creditSituationConfig.nationalities && creditSituationConfig.nationalities.length &&
                                            creditSituationConfig.nationalities.map((nationality) =>
                                                <option key={nationality.id} value={nationality.code}>{nationality.libelle}</option>
                                            )
                                            }
                                        </FormControl>
                                        {nationalite.error && nationalite.touched && <div className="text-danger">{nationalite.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}> {creditSituationValues.nationalityFomatted} </span>
                                }
                            </Col>
                        </Row>}

                        {telDomCli && telDomCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(telDomCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...telephoneFixe} />
                                        {telephoneFixe.error && telephoneFixe.touched && <div className="text-danger">{telephoneFixe.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.telephoneFixe}</span>
                                }
                            </Col>
                        </Row>}

                        {telBurCli && telBurCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(telBurCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...telephoneProfessionnel} />
                                        {telephoneProfessionnel.error && telephoneProfessionnel.touched &&
                                        <div className="text-danger">{telephoneProfessionnel.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.telephoneProfessionnel}</span>
                                }
                            </Col>
                        </Row>}

                        {telPortCli && telPortCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(telPortCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...telephonegsm} />
                                        {telephonegsm.error && telephonegsm.touched && <div className="text-danger">{telephonegsm.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.telephonegsm}</span>
                                }
                            </Col>
                        </Row>}

                        {faxCli && faxCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(faxCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...fax} />
                                        {fax.error && fax.touched && <div className="text-danger">{fax.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.fax}</span>
                                }
                            </Col>
                        </Row>}

                        {emailCli && emailCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(emailCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...mailPrincipal} />
                                        {mailPrincipal.error && mailPrincipal.touched && <div className="text-danger">{mailPrincipal.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.mailPrincipal}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {sectAct && sectAct.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(sectAct.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...codeSecteurActivite}>
                                            {creditSituationConfig.activityAreas && creditSituationConfig.activityAreas.length &&
                                            creditSituationConfig.activityAreas.map((activityArea) =>
                                                <option key={activityArea.id} value={activityArea.code}>{activityArea.libelle}</option>
                                            )
                                            }
                                        </FormControl>
                                        {codeSecteurActivite.error && codeSecteurActivite.touched && <div className="text-danger">{codeSecteurActivite.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.secteurActiviteFormatted}</span>
                                }
                            </Col>
                        </Row>
                        }

                        {professionCli && professionCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(professionCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...profession}>
                                            <option value="">Veuillez choisir la proffesion</option>
                                            {
                                                creditSituationConfig.professions && creditSituationConfig.professions.length &&
                                                creditSituationConfig.professions.map((profession) =>
                                                    <option key={profession.id} value={profession.code}>{profession.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {profession.error && profession.touched && <div className="text-danger">{profession.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.professionFormatted}</span>
                                }
                            </Col>
                        </Row>}

                        {revNetCli && revNetCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(revNetCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl type="number" placeholder="" value={this.state.netMonthlyIncome} onChange={this.handleChangeNetMonthlyIncome} />
                                    : <span >{creditSituationValues.salaryFormatted}</span>
                                }
                            </Col>
                        </Row>}

                        {autRevCli && autRevCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(autRevCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl type="number" placeholder="" value={this.state.otherMonthlyIncome} onChange={this.handleChangeOtherMonthlyIncome} />
                                    : <span>{creditSituationValues.otherMonthlyIncomeFormatted}</span>
                                }
                            </Col>
                        </Row>}

                        {fraisImmCli && fraisImmCli.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(fraisImmCli.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl type="number" placeholder="" value={this.state.monthlyInstallments} onChange={this.handleChangeMonthlyInstallments} />
                                    : <span>{creditSituationValues.monthlyMaturitiesFormatted}</span>
                                }
                            </Col>
                        </Row>}

                        {autresChargesMensu && autresChargesMensu.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(autresChargesMensu.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl type="number" placeholder="" value={this.state.otherExpenses} onChange={this.handleChangeOtherExpenses} />
                                    : <span>{creditSituationValues.otherExpensesFormatted}</span>
                                }
                            </Col>
                        </Row>}

                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t('credit.field.totalEarnings')}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl type="number" placeholder="" value={this.state.totalEarnings} onChange={this.handleTotalEarnings} />
                                    : <span>{creditSituationValues.totalEarningsFormatted}</span>
                                }
                            </Col>
                        </Row>

                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t('credit.field.totalLoads')}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl type="number" placeholder="" value={this.state.totalLoads} onChange={this.handleTotalLoads} />
                                    : <span>{creditSituationValues.totalLoadsFormatted}</span>
                                }
                            </Col>
                        </Row>

                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t('credit.field.debtRatio')}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl type="number" placeholder="" value={this.state.debtRatio} />
                                    : <span>{creditSituationValues.debtRatioFormatted}</span>
                                }
                            </Col>
                        </Row>

                        {ribCliSituation && ribCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t('credit.field.ribCli')}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...ribCli} />
                                        {ribCli.error && ribCli.touched && <div className="text-danger">{ribCli.error}</div>}
                                    </Col>
                                    : <span>{creditSituationValues.ribCli}</span>
                                }
                            </Col>
                        </Row>}

                        {nomAgenceCliSituation && nomAgenceCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t('credit.field.nomAgenceCli')}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...nomAgenceCli} />
                                        {nomAgenceCli.error && nomAgenceCli.touched && <div className="text-danger">{nomAgenceCli.error}</div>}
                                    </Col>
                                    : <span>{creditSituationValues.nomAgenceCli}</span>
                                }
                            </Col>
                        </Row>}

                        {dateCptCliSituation && dateCptCliSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t('credit.field.dateCptCli')}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <DatePicker
                                        selected={this.state.cptDate}
                                        className={globalStyles.datePickerFormControl}
                                        isClearable="true"
                                        onChange={this.handleChangeCptDate}
                                        dateFormat="DD/MM/YYYY"
                                    />
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.dateCptCliString}</span>
                                }
                            </Col>
                        </Row>}
                    </Panel>
                    {this.state.flagConjoint &&
                    <Panel header="Conjoint" eventKey="3" className={styles.accordionPanel}>
                        {nomCnjSituation && nomCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(nomCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...nomConjoint} />
                                        {nomConjoint.error && nomConjoint.touched && <div className="text-danger">{nomConjoint.error}</div>}
                                    </Col>
                                    : <span>{creditSituationValues.nomConjoint}</span>
                                }
                            </Col>
                        </Row>}

                        {prenomCnjSituation && prenomCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(prenomCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...prenomConjoint} />
                                        {prenomConjoint.error && prenomConjoint.touched && <div className="text-danger">{prenomConjoint.error}</div>}
                                    </Col>
                                    : <span>{creditSituationValues.prenomConjoint}</span>
                                }
                            </Col>
                        </Row>}

                        {typeIdentCnjSituation && typeIdentCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(typeIdentCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                {action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...typePieceIdentConjoint}
                                                     disabled={!typeIdentCnjSituation.editableBO}>
                                            {
                                                creditSituationConfig.identityTypes && creditSituationConfig.identityTypes.length
                                                && creditSituationConfig.identityTypes.map((identityType) =>
                                                    <option key={identityType.id} value={identityType.code}>{identityType.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {typePieceIdentConjoint.error && typePieceIdentConjoint.touched &&
                                        <div className="text-danger">{typePieceIdentConjoint.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.typeIdenConjointString} </span>
                                }
                            </Col>
                        </Row>}

                        {numIdentCnjSituation && numIdentCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(numIdentCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...numPieceIdentConjoint} />
                                        {numPieceIdentConjoint.error && numPieceIdentConjoint.touched &&
                                        <div className="text-danger">{numPieceIdentConjoint.error}</div>}
                                    </Col>
                                    : <span>{creditSituationValues.numPieceIdentConjoint}</span>
                                }
                            </Col>
                        </Row>}

                        {dateIdentCnjSituation && dateIdentCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(dateIdentCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <DatePicker
                                        selected={this.state.dateCinCnj}
                                        className={globalStyles.datePickerFormControl}
                                        isClearable="true"
                                        disabled={!dateIdentCnjSituation.editableBO}
                                        onChange={this.handleChangeDateCinCnj}
                                        dateFormat="DD/MM/YYYY"
                                    />
                                    : <span className={styles.pullLeftShow} > {creditSituationValues.dateCinCliString} </span>
                                }
                            </Col>
                        </Row>}

                        {dateNaisCnjSituation && dateNaisCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4" ><ControlLabel>{t(String(dateNaisCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8" >
                                { action === 'create' ?
                                    <DatePicker
                                        selected={this.state.dateNaisCnj}
                                        className={globalStyles.datePickerFormControl}
                                        isClearable="true"
                                        disabled={!dateNaisCnjSituation.editableBO}
                                        onChange={this.handleChangeDateNaisCnj}
                                        dateFormat="DD/MM/YYYY"
                                    />
                                    : <span className={styles.pullLeftShow} > {creditSituationValues.dateNaissConjointString} </span>
                                }
                            </Col>
                        </Row>}

                        {lieuNaisCnjSituation && lieuNaisCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(lieuNaisCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl type="text" placeholder="" {...lieuNaissConjoint} />
                                    : <span>{creditSituationValues.lieuNaissConjoint}</span>
                                }
                            </Col>
                        </Row>}

                        {nationaliteCnjSituation && nationaliteCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(nationaliteCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...nationaliteConjoint}>
                                            <option value="">{t(String(nationaliteCnjSituation.labelCode))}</option>
                                            {creditSituationConfig.nationalities && creditSituationConfig.nationalities.length &&
                                            creditSituationConfig.nationalities.map((nationality) =>
                                                <option value={nationality.code}>{nationality.libelle}</option>
                                            )
                                            }
                                        </FormControl>
                                        {nationaliteConjoint.error && nationaliteConjoint.touched && <div className="text-danger">{nationaliteConjoint.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.nationaliteConjointString}</span>
                                }
                            </Col>
                        </Row>}

                        {professionCnjSituation && professionCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(professionCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <Col>
                                        <FormControl componentClass="select" placeholder="select" {...professionConjoint}>
                                            <option value="">{t(String(professionCnjSituation.labelCode))}</option>
                                            {
                                                creditSituationConfig.professions && creditSituationConfig.professions.length &&
                                                creditSituationConfig.professions.map((profession) =>
                                                    <option value={profession.code}>{profession.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {professionConjoint.error && professionConjoint.touched && <div className="text-danger">{professionConjoint.error}</div>}
                                    </Col>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.professionConjointString}</span>
                                }
                            </Col>
                        </Row>}

                        {employeurCnjSituation && employeurCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(employeurCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...employeurConjoint} />
                                        {employeurConjoint.error && employeurConjoint.touched && <div className="text-danger">{employeurConjoint.error}</div>}
                                    </Col>
                                    : <span>{creditSituationValues.employeurConjoint}</span>}
                            </Col>
                        </Row>}

                        {dateEmbCnjSituation && dateEmbCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(dateEmbCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <DatePicker
                                        selected={this.state.dateEmbCnj}
                                        className={globalStyles.datePickerFormControl}
                                        isClearable="true"
                                        disabled={!dateIdentCnjSituation.editableBO}
                                        onChange={this.handleChangeDateEmbCnj}
                                        dateFormat="DD/MM/YYYY"
                                    />
                                    : <span className={styles.pullLeftShow} > {creditSituationValues.dateEmbConjointString} </span>
                                }
                            </Col>
                        </Row>}

                        {revNetCnjSituation && revNetCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(revNetCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...revenuConjointMensuelNet} />
                                        {revenuConjointMensuelNet.error && revenuConjointMensuelNet.touched &&
                                        <div className="text-danger">{revenuConjointMensuelNet.error}</div>}
                                    </Col>
                                    : <span>{creditSituationValues.revenuConjointString}</span>}
                            </Col>
                        </Row>}

                        {telCnjSituation && telCnjSituation.visibleBO &&
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t(String(telCnjSituation.labelCode))}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                {action === 'create' ?
                                    <Col>
                                        <FormControl type="text" placeholder="" {...telConjoint} />
                                        {telConjoint.error && telConjoint.touched && <div className="text-danger">{telConjoint.error}</div>}
                                    </Col>
                                    : <span>{creditSituationValues.telConjoint}</span>}
                            </Col>
                        </Row>}
                    </Panel>
                    }

                    <Panel header="Emploi" eventKey="4" className={styles.accordionPanel}>
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t('credit.field.employer')}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl type="text" placeholder="" {...nomEmployeur} />
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.nomEmployeur}</span>
                                }
                            </Col>
                        </Row>

                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t('credit.field.hireDate')}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <DatePicker
                                        selected={this.state.hireDate}
                                        className={globalStyles.datePickerFormControl}
                                        isClearable="true"
                                        onChange={this.handleChangeHireDate}
                                        dateFormat="DD/MM/YYYY"
                                    />
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.dateEmbaucheString}</span>
                                }
                            </Col>
                        </Row>

                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t('credit.field.contractType')}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl componentClass="select" placeholder="select" {...typeContrat}>
                                        <option value="">Veuillez choisir le type du contrat</option>
                                        {creditSituationConfig.contractTypes && creditSituationConfig.contractTypes.length &&
                                        creditSituationConfig.contractTypes.map((contractType) =>
                                            <option key={contractType.id} value={contractType.code}>{contractType.libelle}</option>
                                        )
                                        }
                                    </FormControl>
                                    : <span className={styles.pullLeftShow}>{creditSituationValues.contractTypeFormatted}</span>
                                }
                            </Col>
                        </Row>
                    </Panel>
                    <Panel header="Coordonnées" eventKey="5" className={styles.accordionPanel}>
                        <Row className={styles.creditPadding}>
                            <Col xs="6" md="4"><ControlLabel>{t('credit.field.country')}</ControlLabel></Col>
                            <Col xs="6" md="8">
                                { action === 'create' ?
                                    <FormControl componentClass="select" placeholder="select" {...paysPrincipal}>
                                        <option value="">Veuillez choisir votre pays</option>
                                        {creditSituationConfig.countries && creditSituationConfig.countries.length &&
                                        creditSituationConfig.countries.map((country) =>
                                            <option key={country.id} value={country.code}>{country.libelle}</option>
                                        )
                                        }
                                    </FormControl>
                                    : <span className={styles.pullLeftShow} >{creditSituationValues.countryFormatted}</span>
                                }
                            </Col>
                        </Row>
                    </Panel>
                </Accordion>

                { action === 'create' &&
                <Panel className={styles.recapitulatifContainer}>
                    <Row>
                        <Col xs={12} md={12}>
                            <div className="pull-right">
                                <Button
                                    className={styles.creditButton}
                                    type="submit"
                                    disabled={invalid}
                                >
                                    <i className="fa fa-check "/> Soumettre ma demande
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </Panel>}

            </form>
        );
    }
}