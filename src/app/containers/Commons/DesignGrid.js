import React, { Component } from "react";
import PropTypes from "prop-types";

import { Button } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { initializeWithKey } from "redux-form";
import { asyncConnect } from "redux-connect";

@connect(
  (state) => ({
    accountId: state.mouvements.accountId,
    maxPages: state.mouvements.maxPages,
    pageCourant: state.mouvements.currentPage,
    currentTab: state.mouvements.currentTab,
  }),
  { initializeWithKey }
)
export default class OtherPager extends Component {
  static propTypes = {
    changeIndexPage: PropTypes.func,
    accountId: PropTypes.number,
    maxPages: PropTypes.number,
    pageCourant: PropTypes.number,
    currentTab: PropTypes.number,
  };
  getDefaultProps() {
    return {
      nextText: "",
      previousText: "",
    };
  }
  render() {
    const {
      changeIndexPage,
      accountId,
      maxPages,
      pageCourant,
      currentTab,
    } = this.props;
    const styles = require("./Mouvement.scss");
    let maxPage = 0;
    let previous = "";
    let next = "";
    const options = [];
    if (maxPages % 10 === 0) {
      maxPage = maxPages / 10;
    } else {
      maxPage = Math.ceil(maxPages / 10);
    }
    const pageChange = (event) => {
      const indexPage = parseInt(event.target.getAttribute("data-value"), 10);
      changeIndexPage(indexPage, accountId, currentTab);
    };
    const clickButtonNext = () => {
      changeIndexPage(pageCourant + 1, accountId, currentTab);
    };
    const clickButtonPrevious = () => {
      changeIndexPage(pageCourant - 1, accountId, currentTab);
    };
    for (let i = 1; i <= maxPage; i++) {
      const selected = pageCourant === i ? styles.currentPage : "";
      options.push(
        <Button className={selected} data-value={i} onClick={pageChange}>
          {i}
        </Button>
      );
    }
    if (pageCourant > 1 && pageCourant <= maxPage) {
      previous = (
        <Button
          onClick={() => clickButtonPrevious()}
          className={styles.previousNextButton}
        >
          {this.props.previousText}
        </Button>
      );
    }

    if (pageCourant !== maxPage && pageCourant < maxPage) {
      next = (
        <Button
          onClick={() => clickButtonNext()}
          className={styles.previousNextButton}
        >
          {this.props.nextText}
        </Button>
      );
    }
    return (
      <div className="row custom-pager">
        <div className="col-md-9 col-md-offset-3">
          {previous}
          {options}
          {next}
        </div>
      </div>
    );
  }
}
