const LOAD_PARAMS_BENEFICIARY = 'params/LOAD_PARAMS_BENEFICIARY';
const LOAD_PARAMS_BENEFICIARY_SUCCESS = 'params/LOAD_PARAMS_BENEFICIARY_SUCCESS';
const LOAD_PARAMS_BENEFICIARY_FAIL = 'params/LOAD_PARAMS_BENEFICIARY_FAIL';

const InitialState = {
  loaded: false,
  showAlertError: false
};
export default function reducer(state = InitialState, action = {}) {
  switch (action.type) {
    case LOAD_PARAMS_BENEFICIARY:
      return {
        ...state,
        loading: true
      };
    case LOAD_PARAMS_BENEFICIARY_SUCCESS:
      {
        const parametrage = action.result;
        return {
          ...state,
          loading: false,
          loaded: true,
          parametrage,
          error: null
        };
      }
    case LOAD_PARAMS_BENEFICIARY_FAIL:
      {
        return {
          ...state,
          loading: false,
          loaded: false,
          error: action.error
        };
      }
    default:
      return state;
  }
}
export function loadParamasBeneficiary() {
  return {
    types: [LOAD_PARAMS_BENEFICIARY, LOAD_PARAMS_BENEFICIARY_SUCCESS, LOAD_PARAMS_BENEFICIARY_FAIL],
    promise: (client) => client.get('api/config/beneficiairy'),
  };
}
