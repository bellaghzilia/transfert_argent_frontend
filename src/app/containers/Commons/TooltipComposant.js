import React, { Component } from 'react';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';

export default class TooltipComposant extends Component {
  render() {
    const placement = this.props.placement;
    const tooltip = <Tooltip id={this.props.id}>{this.props.tooltip}</Tooltip>;
    return (
         <OverlayTrigger
           overlay={tooltip} placement={placement}
           delayShow={50} delayHide={150}
         >
        {this.props.children}
      </OverlayTrigger>
      );
  }
}
