export function objectToParams(object) {
  let str = '';
  for (const key in object) {
    if (str !== '') {
      str += '&';
    }
    str += key + '=' + encodeURIComponent(object[key]);
  }
  return str;
}

export function serialize(obj, prefix) {
	  var str = [], p;
	  for(p in obj) {
	    if (obj.hasOwnProperty(p)) {
	      var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
	      str.push((v !== null && typeof v === "object") ?
	        serialize(v, k) :
	        encodeURIComponent(k) + "=" + encodeURIComponent(v));
	    }
	  }
	  return str.join("&");
	}

export function objectParametize(obj, delimeter, q) {
    var str = new Array();
    if (!delimeter) delimeter = '&';
    for (var key in obj) {
        switch (typeof obj[key]) {
            case 'string':
            case 'number':
                str[str.length] = key + '=' + obj[key];
            break;
            case 'object':
                str[str.length] = objectParametize(obj[key], delimeter);
        }
    }
    return (q === true ? '?' : '') + str.join(delimeter);
}

export function b64toBlob(b64Data, contentType, sliceSize) {
	  contentType = contentType || '';
	  sliceSize = sliceSize || 512;

	  var byteCharacters = atob(b64Data);
	  var byteArrays = [];

	  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
	    var slice = byteCharacters.slice(offset, offset + sliceSize);

	    var byteNumbers = new Array(slice.length);
	    for (var i = 0; i < slice.length; i++) {
	      byteNumbers[i] = slice.charCodeAt(i);
	    }

	    var byteArray = new Uint8Array(byteNumbers);

	    byteArrays.push(byteArray);
	  }

	  var blob = new Blob(byteArrays, {type: contentType});
	  return blob;
	}

export function formatDatePicker(dateToFormat) {
    let day = dateToFormat.getDate();
    let month = dateToFormat.getMonth() + 1;
    const year = dateToFormat.getFullYear();
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }
    const DateDebut = day + '-' + month + '-' + year;
    return DateDebut;
}


export function formatNumber(montant) {
    if (montant && montant !== '') {
      const  montantString = montant.toString();	
      const montantToNumber = Number(parseFloat(montantString.replace(/\s/g, '').replace(',', '.')).toFixed(3));
      const formattingNumber = montantToNumber.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ');
      return formattingNumber;
    }else{
    	return montant;
    }
  }