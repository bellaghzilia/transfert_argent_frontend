import React, { Component } from "react";
import { asyncConnect } from "redux-connect";
import { connect } from "react-redux";
import { push } from "react-router-redux";

import { Col, Modal, Row } from "react-bootstrap";
import { browserHistory } from "react-router";
import { withNamespaces } from "react-i18next";
import localStorage from "local-storage";
import jwt_decode from "jwt-decode";
import _ from "lodash";

// Loading
import { css } from "@emotion/core";
import { ScaleLoader } from "react-spinners";

import Cookies from "js-cookie";

// // UI
import LoginComponent from "./LoginComponent";
// import LoginComponentAWB from "./LoginComponentAWB";
// import LoginComponentAWBbis from "./LoginComponentAWBbis";
// import LoginComponentCAM from "./LoginComponentCAM";
// import LoginComponentCAMbis from "./LoginComponentCAMbis";
// import LoginComponentSG from "./LoginComponentSG";
// import LoginComponentUIB from "./LoginComponentUIB";
// import LoginComponentBNP from "./LoginComponentBNP";
// import LoginComponentBICEC from "./LoginComponentBICEC";

// Action
import * as loginActions from "../Login/loginReducer";
import * as UserActions from "../User/UserReducer";

@asyncConnect([
  {
    promise: ({ store: { dispatch } }) => {
      const promises = [];
      promises.push(dispatch(loginActions.getToken()));
      return Promise.all(promises);
    },
  },
])
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messageStatut: "",
      formValues: null,
    };
  }

  // Check if User already connected
  componentWillMount() {
    bankCode = this.props.codeBanque ? this.props.codeBanque : "00000";
    lang = this.props.codeLangue ? this.props.codeLangue : "fr";
    document.getElementById("favicon").href = "/favicon-" + bankCode + ".png";
  }

  // Check Status login - show errors
  // checkStatut = async () => {
  //   const { t, getStatusLogin } = this.props;
  //   try {
  //     await getStatusLogin(this.state.formValues);
  //     switch (this.props.statut) {
  //       case "1001":
  //         this.setState({ messageStatut: t("login.statut1001") });
  //         break;
  //       case "1002":
  //         this.setState({ messageStatut: t("login.statut1002") });
  //         break;
  //       case "1003":
  //         this.setState({ messageStatut: t("login.statut1003") });
  //         break;
  //       case "1004":
  //         this.setState({ messageStatut: t("login.statut1004") });
  //         break;
  //       case "1005":
  //         this.setState({ messageStatut: t("login.statut1005") });
  //         break;
  //       case "1006":
  //         this.setState({ messageStatut: t("login.statut1006") });
  //         break;
  //       case "1007":
  //         this.setState({ messageStatut: t("login.statut1007") });
  //         break;
  //       case "1008":
  //         this.setState({ messageStatut: t("login.statut1008") });
  //         break;
  //       case "1009":
  //         this.setState({ messageStatut: t("login.statut1009") });
  //         break;
  //       case "1010":
  //         this.setState({ messageStatut: t("login.statut1010") });
  //         break;
  //       case "1011":
  //         this.setState({ messageStatut: t("login.statut1011") });
  //         break;
  //       default:
  //         this.setState({ messageStatut: t("login.statut1001") });
  //         break;
  //     }
  //   } catch (error) {
  //     this.setState({ messageStatut: t("login.statut1001") });
  //   }
  // };

  // Decode Token & Save userDetails To Local Storage
  // saveToLocalStorage = () => {
  //   console.log(" this.props.token :: " + this.props.csrfToken);
  //   if (!_.isEmpty(this.props.csrfToken)) {
  //     localStorage.set("csrfToken", this.props.csrfToken);
  //     return true;
  //   } else {
  //     console.log(" ERROR: CAN'T save to local storage");
  //     return false;
  //   }
  // };

  // Authentification ADRIA
  auth = async (values) => {
    this.setState({ formValues: values });
    try {
      console.log(" this.props.token :: " + this.props.token);
      if (!_.isEmpty(this.props.token)) {
        console.log("token saved with success ");
        await this.props.login(values);
        console.log(
          "this.props.isAuthenticate :: " + this.props.isAuthenticate
        );
        if (this.props.isAuthenticate == true) {
          // if (true) {
          window.location.replace("/app");
        }
        console.log("Login Succedded ");
      } else console.log(" ERROR: can't call save to local storage funtion");
    } catch (error) {
      if (this.props.errorLogin) {
        console.log("loginError " + error);
      }
    }
  };

  // Change language
  handleLangChange = () => {
    // window.location.replace("/login/" + this.props.codeBanque + "/" + cl.value);
    window.location.replace("/login");
  };

  render() {
    const {
      t,
      codeBanque,
      codeLangue,
      loading,
      user,
      matrice,
      virtualKeyboardEnabled,
      alreadyConnected,
      token,
    } = this.props;
    const note = "";
    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
    `;

    return (
      <div>
        {codeBanque === "00013" ? (
          <LoginComponentBNP
            matrice={matrice}
            virtualKeyboardEnabled={virtualKeyboardEnabled}
            alreadyConnected={alreadyConnected}
            messageStatut={this.state.messageStatut}
            auth={this.auth}
          />
        ) : codeBanque === "00001" || codeBanque === "18319" ? (
          <LoginComponentSG
            matrice={matrice}
            virtualKeyboardEnabled={virtualKeyboardEnabled}
            alreadyConnected={alreadyConnected}
            messageStatut={this.state.messageStatut}
            handleLangChange={this.handleLangChange}
            codeLangue={codeLangue}
            auth={this.auth}
          />
        ) : codeBanque === "00012" ? (
          <LoginComponentUIB
            matrice={matrice}
            virtualKeyboardEnabled={virtualKeyboardEnabled}
            alreadyConnected={alreadyConnected}
            messageStatut={this.state.messageStatut}
            handleLangChange={this.handleLangChange}
            codeLangue={codeLangue}
            auth={this.auth}
          />
        ) : codeBanque === "00034" ? (
          <LoginComponentAWB
            alreadyConnected={alreadyConnected}
            messageStatut={this.state.messageStatut}
            auth={this.auth}
          />
        ) : codeBanque === "60010" ? (
          <LoginComponentAWBbis
            alreadyConnected={alreadyConnected}
            messageStatut={this.state.messageStatut}
            auth={this.auth}
          />
        ) : codeBanque === "00225" ? (
          <LoginComponentCAMbis
            matrice={matrice}
            virtualKeyboardEnabled={virtualKeyboardEnabled}
            alreadyConnected={alreadyConnected}
            messageStatut={this.state.messageStatut}
            auth={this.auth}
          />
        ) : codeBanque === "10001" ? (
          <LoginComponentBICEC
            matrice={matrice}
            virtualKeyboardEnabled={virtualKeyboardEnabled}
            alreadyConnected={alreadyConnected}
            messageStatut={this.state.messageStatut}
            handleLangChange={this.handleLangChange}
            codeLangue={codeLangue}
            auth={this.auth}
          />
        ) : (
          <LoginComponent
            matrice={matrice}
            virtualKeyboardEnabled={virtualKeyboardEnabled}
            alreadyConnected={alreadyConnected}
            messageStatut={this.state.messageStatut}
            handleLangChange={this.handleLangChange}
            codeLangue={codeLangue}
            auth={this.auth}
            alreadyConnected={alreadyConnected}
          />
        )}

        {note && (
          <section>
            <div className="container">
              <Row>
                <Col>
                  <div dangerouslySetInnerHTML={{ __html: note }}></div>
                </Col>
              </Row>
            </div>
          </section>
        )}
        {/** LOADING CONTROLER */}
        <Modal
          backdrop="static"
          className="loadingModal"
          show={loading ? true : false}
        >
          <Modal.Body>
            <Row>
              <Col md={12} xs={12}>
                <div>
                  <ScaleLoader
                    color={"#e7e7e7"}
                    css={override}
                    height={105}
                    heightUnit={"px"}
                    loading={loading !== 0 ? true : false}
                    margin={"8px"}
                    radius={10}
                    radiusUnit={"px"}
                    width={8}
                    widthUnit={"px"}
                  />
                  <br />
                  <span style={{ fontSize: "11px" }}>
                    {t("labels.loading")}
                  </span>
                </div>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
// State to props
const mapStateToProps = (state) => ({
  // userFrontDetails: state.user.userFrontDetails,
  // user: state.user,
  codeBanque: state.loginReducer.codeBanque,
  codeLangue: state.loginReducer.codeLangue,

  isAuthenticate: state.loginReducer.isAuthenticate,
  token: state.loginReducer.csrfToken,
  // refresh_token: state.loginReducer.refresh_token,
  loading: state.loginReducer.loading,
  statut: state.loginReducer.statut,
  errorLogin: state.loginReducer.errorLogin,
  matrice: state.loginReducer.matrice,
  reloadKeymap: state.loginReducer.reloadKeymap,
  idClavier: state.loginReducer.idClavier,
  virtualKeyboardEnabled: state.loginReducer.virtualKeyboardEnabled,
  alreadyConnected: state.loginReducer.alreadyConnected,
});

// Connect to the redux store
Login = connect(mapStateToProps, { ...loginActions, ...UserActions })(Login);
// Export the UI
export default withNamespaces(["messages"])(Login);
