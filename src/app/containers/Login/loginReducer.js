// Types
const LOAD_LOGIN = "user/LOAD_LOGIN_REQUEST";
const LOAD_LOGIN_SUCCESS = "user/LOAD_LOGIN_SUCCESS";
const LOAD_LOGIN_FAIL = "user/LOAD_LOGIN_FAIL";

const UPDATE_PASSWORD = "user/UPDATE_PASSWORD";
const UPDATE_PASSWORD_SUCCESS = "user/UPDATE_PASSWORD_SUCCESS";
const UPDATE_PASSWORD_FAIL = "user/UPDATE_PASSWORD_FAIL";

const UPDATE_LANGUE = "user/UPDATE_LANGUE";
const UPDATE_LANGUE_SUCCESS = "user/UPDATE_LANGUE_SUCCESS";
const UPDATE_LANGUE_FAIL = "user/UPDATE_LANGUE_FAIL";

const MINI_RELEVE = "user/MINI_RELEVE";
const MINI_RELEVE_SUCCESS = "user/MINI_RELEVE_SUCCESS";
const MINI_RELEVE_FAIL = "user/MINI_RELEVE_FAIL";

const RELOAD = "user/RELOAD";

const LOAD_CSRF_TOKEN = "user/LOAD_CSRF_TOKEN";
const LOAD_CSRF_TOKEN_SUCCESS = "user/LOAD_CSRF_TOKEN_SUCCESS";
const LOAD_CSRF_TOKEN_FAIL = "user/LOAD_CSRF_TOKEN_FAIL";

import _ from "lodash";
import localStorage from "local-storage";
import Cookies from "js-cookie";

const initialState = {
  loaded: false,
  loading: false,
  isAuthenticate: false,
  errorLogin: false,
  alreadyConnected: false,
};
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_LOGIN:
      console.log("LOAD_LOGIN " + action.result);
      return {
        ...state,
        loading: true,
      };
    case LOAD_LOGIN_SUCCESS:
      console.log("LOAD_LOGIN_SUCCESS " + action.result.success);
      let response = {};
      if (action.result.success) {
        response = {
          ...state,
          token: action.result.access_token,
          refresh_token: action.result.refresh_token,
          isAuthenticate: true,
          loading: false,
          reloadKeymap: false,
          errorLogin: false,
        };
      } else response = state;

      return response;
    case LOAD_LOGIN_FAIL:
      console.log("LOAD_LOGIN_FAIL " + action.result);
      return {
        ...state,
        token: "",
        refresh_token: "",
        loading: false,
        errorLogin: true,
        reloadKeymap: true,
      };
    case LOAD_CSRF_TOKEN:
      console.log("LOAD_CSRF_TOKEN " + action.result);
      return {
        ...state,
        loading: true,
      };
    case LOAD_CSRF_TOKEN_SUCCESS:
      console.log("LOAD_CSRF_TOKEN_SUCCESS" + action.result.success);
      if (action.result.success) {
        localStorage.set("csrfToken", action.result.csrfToken);
        console.log("csrfToken :: " + localStorage.get("csrfToken"));
      }
      return {
        ...state,
        loading: false,
        csrfToken: action.result.csrfToken,
      };
    case LOAD_CSRF_TOKEN_FAIL:
      console.log("LOAD_CSRF_TOKEN_FAIL" + action.result);
      return {
        ...state,
        loading: false,
        errorLoadingCsrfToken: true,
        csrfToken: null,
      };
    default:
      return state;
  }
}

function objectToParams(object) {
  let str = "";
  for (const key in object) {
    if (str !== "") {
      str += "&";
    }
    str += key + "=" + encodeURIComponent(object[key]);
  }
  return str;
}

export function loadUserFrontDetails() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get("api/user"),
  };
}

export function login() {
  console.log("Login call begin ");

  const userAuth = {
    j_username: "Agent",
    j_password: "123456",
    codeBanque: "00000",
    lang: "fr",
    codePays: "MA",
  };

  console.log("login reducer call : ");
  return {
    types: [LOAD_LOGIN, LOAD_LOGIN_SUCCESS, LOAD_LOGIN_FAIL],
    promise: (client) =>
      client.post("login/mobile", { data: objectToParams(userAuth) }),
  };
}

export function getToken() {
  const privateHash =
    "ac64a95e29010f2bb94230af205e4a09845836c86fb8fe4ce7d1bb094b6a7eb25cd941cd233213dab6a8219a402cf52bbe4542807823947d4ecd4b689644b86e";

  return {
    types: [LOAD_CSRF_TOKEN, LOAD_CSRF_TOKEN_SUCCESS, LOAD_CSRF_TOKEN_FAIL],
    promise: (client) =>
      client.get("client/entreEnRelationOffline?hash=" + privateHash),
  };
}

export function isUserFrontDetailsLoaded(globalState) {
  return (
    globalState.user.userLoadSuccess === true &&
    globalState.user.userFrontDetails
  );
}

export function saveChangePassword(dto) {
  return {
    types: [UPDATE_PASSWORD, UPDATE_PASSWORD_SUCCESS, UPDATE_PASSWORD_FAIL],
    promise: (client) =>
      client.post("api/user/change/password", { data: objectToParams(dto) }),
  };
}

export function updateLangue(langue) {
  let dto = {};
  dto.langue = langue;
  return {
    types: [UPDATE_LANGUE, UPDATE_LANGUE_SUCCESS, UPDATE_LANGUE_FAIL],
    promise: (client) =>
      client.get("api/user/updateLangue", { params: objectToParams(dto) }),
  };
}

export function getMiniReleve(id) {
  console.log("mini releve");
  return {
    types: [MINI_RELEVE, MINI_RELEVE_SUCCESS, MINI_RELEVE_FAIL],
    promise: (client) => client.get("agent/releveListAgent?id=" + id),
  };
}

export function reload() {
  return {
    type: RELOAD,
  };
}
