import React, { Component } from "react";
import { translate } from "react-i18next";
import { connect } from "react-redux";
import { initializeWithKey, reduxForm, reset } from "redux-form";
import {
  Row,
  Col,
  Modal,
  ControlLabel,
  PageHeader,
  Button,
  FormControl,
  ButtonGroup,
} from "react-bootstrap";
import { asyncConnect } from "redux-connect";
import Attachments from "../../components/Attachement/Attachments";
import * as AttachementActions from "../../components/Attachement/AttachementReducer";
import * as HistoriqueAgentsAction from "../HistoriqueAgent/HistoriqueAgentDetaillantReducer";
import DatePicker from "react-datepicker";
import moment from "moment";
import { browserHistory } from "react-router";
import * as UpdateActions from "../clients/UpdateClientReducer";
import * as UserActions from "./UserReducer";

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      return Promise.all([dispatch(UserActions.loadUserFrontDetails())]);
    },
  },
])
@translate(["CreateAgentType"], { wait: true })
@reduxForm(
  {
    form: "UpdateAgentForm",
    fields: [
      "adresse",
      "type",
      "nomSocial",
      "ice",
      "formeJuridique",
      "lastName",
      "firstName",
      "dateNaissance",
      "mail",
      "telephone",
      "numeroIdentite",
      "typePieceIdentite",
      "numPatente",
      "rc",
      "ribAgent",
    ],
  },
  (state) => ({
    initialValues: {
      type: "",
      lastName: "",
      firstName: "",
      mail: "",
      telephone: "",
      adresse: "",
      dateNaissance: "",
      numeroIdentite: "",
      typePieceIdentite: "",
      nomSocial: "",
      ice: "",
      numPatente: "",
      rc: "",
      formeJuridique: "",
      ribAgent: "",
    },
  })
)
@connect(
  (state) => ({
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...UserActions, initializeWithKey }
)
export default class MonCompte extends Component {
  constructor(props) {
    super(props);
    this.getInitialState();

    const _self = this;
    this.filesClickEvents = [_self.close, _self.deleteDoc];
    this.filesClickEvent = [];
  }

  componentWillMount() {
    // if(this.props.agent.statut === 'ENABLED' ){
    //     this.props.getAgentCurrency(this.props.agent.userName);
    //
    // }
  }

  getInitialState() {
    this.state = {
      fichiersjoints: null,
      isRecap: true,
      // type: this.props.agent.type,
      // isPhysique: this.props.agent.type === 'P_SegmentAgent'? true : false,
      message: null,
      // showSuspendModal: false
      // isOppSuccess: false
    };
  }

  render() {
    const {
      t,
      userFrontDetails,
      fields: { ribAgent },
    } = this.props;

    const styles = require("../HistoriqueAgent/HistoriqueAgent.scss");

    return (
      <div>
        <div>
          <PageHeader>
            <h3>
              <span>Mon Compte</span>
            </h3>
          </PageHeader>

          <form className="formContainer">
            <fieldset style={{ marginTop: "20px" }}>
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.identifiant")}</ControlLabel>
                  <p className="detail-value">{userFrontDetails.username}</p>
                </Col>
                {/*<Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.typeAgent')}</ControlLabel>
                                        <p className="detail-value">
                                            {(agent.type === 'P_SegmentAgent') &&  t('form.titleForm.persPhysique')}
                                            {(agent.type === 'M_SegmentAgent') && t('form.titleForm.persMorale')}
                                        </p>
                                    </Col>*/}
              </Row>
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.nom")} </ControlLabel>
                  <p className="detail-value">{userFrontDetails.lastName}</p>
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.prenom")} </ControlLabel>
                  <p className="detail-value">{userFrontDetails.firstName}</p>
                </Col>
              </Row>
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.numTel")}</ControlLabel>
                  <p className="detail-value">{userFrontDetails.gsm}</p>
                </Col>

                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.email")}</ControlLabel>
                  <p className="detail-value">{userFrontDetails.email}</p>
                </Col>
              </Row>
              <Row className={styles.fieldRow}>
                {/*<Col xs="12" md="6">*/}
                {/*    <ControlLabel>{t('form.label.statut')}</ControlLabel>*/}
                {/*    <p className="detail-value">*/}
                {/*        /!*{(agent.statut === 'ENABLED') && t('form.label.signe')}*!/*/}
                {/*        /!*{(agent.statut === 'ENCOURS_ACTIVATION') && t('form.label.enCours')}*!/*/}
                {/*        /!*{(agent.statut === 'DISABLED') && t('form.label.rejete')}*!/*/}
                {/*        /!*{(agent.statut === 'SUSPENDU') && t('form.label.suspendu')}*!/*/}
                {/*    </p>*/}
                {/*</Col>*/}
                {/*{ agent && agent.ribAgent && agent.ribAgent!=='' &&*/}
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>Numéro de la pièce d'identité</ControlLabel>
                    <p className="detail-value">
                      {userFrontDetails.numPieceIdentite}
                    </p>
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("form.label.rib")}</ControlLabel>
                    <p className="detail-value">
                      {userFrontDetails.identifiantRC}
                    </p>
                  </Col>
                </Row>
                {/*}*/}
              </Row>
            </fieldset>

            <Row className={styles.fieldRow}>
              <Col className="pull-right" style={{ paddingTop: "10px" }}>
                <Button
                  bsStyle="primary"
                  style={{ float: "right" }}
                  onClick={() =>
                    browserHistory.push({
                      pathname:
                        baseUrl + "app/ReleveAgent/" + userFrontDetails.idUser,
                    })
                  }
                >
                  <i className="fa fa-file-pdf-o fa-4" />
                  Activité
                </Button>
                <Button
                  bsStyle="primary"
                  style={{ float: "right" }}
                  onClick={() =>
                    window.open(
                      baseUrl + "agent/genererRIB/" + userFrontDetails.idUser
                    )
                  }
                >
                  <i className="fa fa-file-pdf-o fa-4" />{" "}
                  {t("form.buttons.genererRIB")}
                </Button>
              </Col>
            </Row>
          </form>
        </div>
      </div>
    );
  }
}
