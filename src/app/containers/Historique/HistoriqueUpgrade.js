import React, { Component } from "react";
import {
  Button,
  Col,
  FormControl,
  ControlLabel,
  Modal,
  Row,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";
import { initializeWithKey, reduxForm, change } from "redux-form";
import { browserHistory } from "react-router";
import { translate } from "react-i18next";
import Griddle from "griddle-react";
import * as HistoriqueAction from "./HistoriqueReducer";
import UpgradeHistoriquePagination from "./UpgradeHistoriquePagination";
import { connect } from "react-redux";
import { asyncConnect } from "redux-connect";
import { reset } from "redux-form";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

class ActionHeaderComponent extends Component {
  render() {
    return <div className="text-center">{this.props.displayName}</div>;
  }
}

class CenterComponent extends Component {
  render() {
    return <div>{this.props.data}</div>;
  }
}

@translate(["common"])
class NoDataComponent extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="noDataResult">
        <h4>{t("errors.noResult")}</h4>
      </div>
    );
  }
}

@translate(["upgrade"])
class ActionComponant extends Component {
  render() {
    const { t, rowData } = this.props;

    const ttipSelect = (
      <Tooltip id="tooltip" className="ttip">
        {t("form.label.visionnerHistorique")}
      </Tooltip>
    );

    return (
      <div style={{ textAlign: "center" }}>
        {
          <OverlayTrigger placement="top" overlay={ttipSelect}>
            {rowData.statutCode !== "Signe" ? (
              <Button
                className="iconbtn"
                onClick={() =>
                  browserHistory.push({
                    pathname: baseUrl + "app/upgradeInstance/" + rowData.id,
                  })
                }
              >
                <i className="f fa fa-eye" />
              </Button>
            ) : (
              <Button className="iconbtn" disabled={true}>
                <i className="fa fa-eye-slash" />
              </Button>
            )}
          </OverlayTrigger>
        }
      </div>
    );
  }
}

export const validate = (values, props) => {
  const errors = {};

  if (!/^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) && values.gsm !== "") {
    errors.gsm = props.t("gsm.msgErr");
  }

  return errors;
};

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      dispatch(reset("UpgradeSearchForm"));
    },
  },
])
@reduxForm({
  form: "UpgradeSearchForm",
  fields: ["gsm", "statut"],
  validate,
  initialValues: {
    reference: "",
    gsm: "",
    statut: "",
  },
  destroyOnUnmount: true,
})
@connect(
  (state) => ({
    statuts: state.HistoriqueReducer.souscriptionStatuts,
    searchLoading: state.HistoriqueReducer.searchLoading,
    search: state.HistoriqueReducer.search,
  }),
  { ...HistoriqueAction, initializeWithKey }
)
@translate(["historique"])
export default class HistoriqueUpgrade extends Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.search !== nextProps.search && nextProps.search === "") {
      this.props.dispatch(reset("UpgradeSearchForm"));
    }
  }

  handleReset = () => {
    this.props.loadUpgradeList(1, "");
    this.props.dispatch(reset("UpgradeSearchForm"));
  };

  handleChange = (event) => {
    this.props.dispatch(
      change("UpgradeSearchForm", "statut", event.target.value)
    );
  };

  handleSearch = (values) => {
    let search = "";

    if (
      /^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) &&
      values.gsm !== ""
    ) {
      search =
        search +
        "demandeSouscription-" +
        values.gsm.substring(values.gsm.length - 9, values.gsm.length);
    }

    if (values.statut !== "") {
      if (search !== "") search = search + ",";
      search = search + "statut:" + values.statut;
    }

    this.props.loadUpgradeList(1, search);
  };

  render() {
    const styles = require("./Historique.scss");
    const {
      t,
      listUpgrade,
      handleSubmit,
      searchLoading,
      values,
      statuts,
      fields: { gsm, statut },
    } = this.props;

    return (
      <div>
        <div>
          <Modal
            show={searchLoading}
            className="loadingModal"
            backdrop="static"
            keyboard={false}
          >
            <Modal.Body>
              <Row>
                <Col xs={12} md={12}>
                  <div className="spinner">
                    <span style={{ fontSize: "11px" }}>
                      {t("chargement.title")}
                    </span>
                  </div>
                </Col>
              </Row>
            </Modal.Body>
          </Modal>

          <Row>
            <form
              className="formContainer"
              style={{ marginTop: "20px", paddingBottom: "20px" }}
            >
              <Row className={styles.fieldRow}>
                <Col xs="12" md="4">
                  <ControlLabel>{t("form.label.tel")}</ControlLabel>
                  <FormControl
                    type="text"
                    {...gsm}
                    className={styles.datePickerFormControl}
                  />
                  {gsm.error && gsm.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {gsm.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="4">
                  <ControlLabel>{t("form.label.statut")}</ControlLabel>
                  <FormControl
                    componentClass="select"
                    {...statut}
                    ref="typeCodeList"
                    onChange={this.handleChange}
                    className={styles.datePickerFormControl}
                  >
                    <option hidden value="">
                      {t("form.label.choisirStatut")}
                    </option>
                    {statuts &&
                      statuts.length &&
                      statuts.map((typeP) => (
                        <option key={typeP.id} value={typeP.code}>
                          {typeP.libelle}
                        </option>
                      ))}
                  </FormControl>
                </Col>
                <Col xs="12" md="1"></Col>
                <Col xs="12" md="3">
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.handleSearch(values);
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-search " /> {t("form.buttons.search")}
                  </Button>
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.handleReset();
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-refresh " /> {t("form.buttons.reset")}
                  </Button>
                </Col>
              </Row>
            </form>
          </Row>

          <div className="table-responsive tablediv">
            <Griddle
              results={listUpgrade}
              tableClassName="table"
              customNoDataComponent={NoDataComponent}
              useGriddleStyles={false}
              useCustomPagerComponent="true"
              resultsPerPage={10}
              customPagerComponent={UpgradeHistoriquePagination}
              columns={[
                "dateCreation",
                "dateUpgrade",
                "agent",
                "gsm",
                "oldTypeLibelle",
                "typeLibelle",
                "statut",
                "action",
              ]}
              columnMetadata={[
                {
                  columnName: "dateCreation",
                  displayName: t("list.cols.dateCreation"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "dateUpgrade",
                  displayName: t("list.cols.dateUpgrade"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "agent",
                  displayName: t("list.cols.agent"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "gsm",
                  displayName: t("list.cols.tel"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "oldTypeLibelle",
                  displayName: t("list.cols.oldTypeLibelle"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "typeLibelle",
                  displayName: t("list.cols.typeProduit"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "statut",
                  displayName: t("list.cols.statut"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "action",
                  displayName: t("list.cols.actions"),
                  customHeaderComponent: ActionHeaderComponent,
                  customComponent: ActionComponant,
                  sortable: true,
                },
              ]}
            />
          </div>
        </div>
      </div>
    );
  }
}
