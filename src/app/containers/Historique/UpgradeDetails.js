import React, { Component } from "react";
import { translate } from "react-i18next";
import { connect } from "react-redux";
import { initializeWithKey } from "redux-form";
import { Row, Col, ControlLabel, PageHeader } from "react-bootstrap";
import Button from "react-bootstrap-button-loader";
import { browserHistory } from "react-router";
import { asyncConnect } from "redux-connect";
import * as HistoriqueAction from "./HistoriqueReducer";
import { AuthorizedComponent } from "react-router-role-authorization";

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      return Promise.all([
        dispatch(HistoriqueAction.upgradeInstance(params.id)),
      ]);
    },
  },
])
@connect(
  (state) => ({
    success: state.HistoriqueReducer.success,
    data: state.HistoriqueReducer.data,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { initializeWithKey }
)
@translate(["upgrade"], { wait: true })
export default class UpgradeDetails extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
  }

  render() {
    const styles = require("./Historique.scss");
    const { t, data } = this.props;

    return (
      <div>
        <PageHeader>
          <h3>{t("form.titleUpgrade")}</h3>
        </PageHeader>

        <form className="formContainer">
          <fieldset style={{ marginTop: "20px" }}>
            <legend>{t("form.legend.infoperso")}</legend>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.tel")}</ControlLabel>
                <p className="detail-value">{data.gsm}</p>
              </Col>
            </Row>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.oldPack")}</ControlLabel>
                <p className="detail-value">{data.oldTypeLibelle}</p>
              </Col>
              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.newPack")}</ControlLabel>
                <p className="detail-value">{data.typeLibelle}</p>
              </Col>
            </Row>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.nom")}</ControlLabel>
                <p className="detail-value">{data.nom}</p>
              </Col>
              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.prenom")}</ControlLabel>
                <p className="detail-value">{data.prenom}</p>
              </Col>
            </Row>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.typePieceIdenrite")}</ControlLabel>
                {data.typePieceIdentite === "MAR" && (
                  <p className="detail-value">{t("form.label.cin1")}</p>
                )}
                {data.typePieceIdentite === "CSJ" && (
                  <p className="detail-value">{t("form.label.carteSejour")}</p>
                )}
                {data.typePieceIdentite === "PSP" && (
                  <p className="detail-value">{t("form.label.passport")}</p>
                )}
              </Col>
              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.cin")}</ControlLabel>
                <p className="detail-value">{data.numeroIdentite}</p>
              </Col>
            </Row>
            <Row className={styles.fieldRow}>
              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.mail")}</ControlLabel>
                <p className="detail-value">{data.email}</p>
              </Col>
              <Col xs="12" md="6">
                <ControlLabel>{t("form.label.statut")}</ControlLabel>
                <p className="detail-value">{data.statut}</p>
              </Col>
            </Row>
            {data.typeCode === "type_three" && (
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>
                    {t("form.label.typeJustificationResidence")}
                  </ControlLabel>
                  <p className="detail-value">{data.typeJustifResidence}</p>
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.revenu")}</ControlLabel>
                  <p className="detail-value">{data.sourceRevenu}</p>
                </Col>
              </Row>
            )}
          </fieldset>

          <Row>
            <Col className="pull-right" style={{ paddingTop: "10px" }}>
              <Button
                bsStyle="primary"
                onClick={() => {
                  browserHistory.push(baseUrl + "app/Historique");
                }}
              >
                <i className="fa fa-check " />
                {t("form.buttons.cancel")}
              </Button>
            </Col>
          </Row>
        </form>
      </div>
    );
  }
}
