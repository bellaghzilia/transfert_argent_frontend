/**
 * Created by lenovo on 07/10/2019.
 */
import React, { Component } from "react";
import {
  Button,
  Col,
  FormControl,
  Link,
  ControlLabel,
  Modal,
  responsive,
  Row,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";
import { initializeWithKey, reduxForm, change } from "redux-form";
import { translate } from "react-i18next";
import Griddle from "griddle-react";
import * as HistoriqueActions from "./HistoriqueReducer";
import VirementHistoriquePagination from "./VirementHistoriquePagination";
import { connect } from "react-redux";
import { asyncConnect } from "redux-connect";
import { reset } from "redux-form";
import * as CashActions from "../Cashing/CashingReducer";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

class ActionHeaderComponent extends Component {
  render() {
    return <div className="text-center">{this.props.displayName}</div>;
  }
}

class CenterComponent extends Component {
  render() {
    const url = this.props.data;
    return <div>{this.props.data}</div>;
  }
}

@translate(["common"])
class NoDataComponent extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="noDataResult">
        <h4>{t("errors.noResult")}</h4>
      </div>
    );
  }
}

class ActionComponent extends Component {
  render() {
    const { rowData } = this.props;
    return (
      <div style={{ textAlign: "center" }}>
        <Button
          className="iconbtn"
          onClick={() =>
            window.open(
              baseUrl + "virementSIMT/createAvisVirementSIMTPDF/" + rowData.id
            )
          }
        >
          <i className="fa fa-file-pdf-o fa-4" />
        </Button>
      </div>
    );
  }
}

export const validate = (values) => {
  const errors = {};

  return errors;
};

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      dispatch(reset("VirementSearchForm"));
    },
  },
])
@reduxForm({
  form: "VirementSearchForm",
  fields: ["reference", "beneficiaire", "statut"],
  validate,
  initialValues: {
    reference: "",
    beneficiaire: "",
    statut: "",
  },
  destroyOnUnmount: true,
})
@connect(
  (state) => ({
    searchLoading: state.HistoriqueReducer.searchLoading,
    search: state.HistoriqueReducer.search,
    beneficiaires: state.HistoriqueReducer.beneficiaires,
    statuts: state.CashingReducer.statuts,
  }),
  { ...HistoriqueActions, ...CashActions, initializeWithKey }
)
@translate(["historique"])
export default class HistoriqueTransfert extends Component {
  constructor() {
    super();
    this.handleReset = this.handleReset.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      showPanelSearch: true,
      dateSaisie: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.search !== nextProps.search && nextProps.search === "") {
      this.props.dispatch(reset("VirementSearchForm"));
    }
  }

  handleReset() {
    this.props.loadVirementHistory(1, "");
    this.props.dispatch(reset("VirementSearchForm"));
  }

  handleChange(event) {
    this.props.dispatch(
      change("VirementSearchForm", "statut", event.target.value)
    );
  }

  handleSearch(values) {
    let search = "";

    if (
      values.beneficiaire !== "" &&
      values.beneficiaire !== "Sélectionner le bénéficiaire"
    ) {
      if (search !== "") search = search + ",";
      search = search + "benificiaire-" + values.beneficiaire;
    }

    if (values.statut !== "" && values.statut !== "Choisir un statut") {
      if (search !== "") search = search + ",";
      search = search + "statut:" + values.statut;
    }

    if (values.reference !== "") {
      if (search !== "") search = search + ",";
      search = search + "identifiantDemande:" + values.reference;
    }

    this.props.loadVirementHistory(1, search);
  }

  render() {
    const {
      t,
      beneficiaires,
      listVirement,
      handleSubmit,
      searchLoading,
      values,
      statuts,
      fields: { beneficiaire, reference, statut },
    } = this.props;
    const styles = require("../Souscription/Souscription.scss");
    return (
      <div>
        <div>
          <Modal
            show={searchLoading}
            className="loadingModal"
            backdrop="static"
            keyboard={false}
          >
            <Modal.Body>
              <Row>
                <Col xs={12} md={12}>
                  <div className="spinner">
                    <span style={{ fontSize: "11px" }}>
                      {t("chargement.title")}
                    </span>
                  </div>
                </Col>
              </Row>
            </Modal.Body>
          </Modal>

          <Row>
            <form
              className="formContainer"
              style={{ marginTop: "20px", paddingBottom: "20px" }}
            >
              <Row className={styles.fieldRow}>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.reference")}</ControlLabel>

                  <FormControl
                    type="text"
                    {...reference}
                    className={styles.datePickerFormControl}
                  />
                  {reference.error && reference.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {reference.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.beneficiaire")}</ControlLabel>

                  <FormControl
                    {...beneficiaire}
                    componentClass="select"
                    placeholder={t("form.label.account")}
                    className={styles.remDocInput}
                    onChange={beneficiaire.onChange}
                  >
                    <option hidden>Sélectionner le bénéficiaire</option>
                    {beneficiaires &&
                      beneficiaires.map((data) => (
                        <option key={data.identifiant} value={data.identifiant}>
                          {"RIB : " +
                            data.rib +
                            ", " +
                            data.nom +
                            " " +
                            data.prenom}
                        </option>
                      ))}
                  </FormControl>
                  {beneficiaire.error && beneficiaire.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {beneficiaire.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.statut")}</ControlLabel>
                  <FormControl
                    componentClass="select"
                    {...statut}
                    ref="typeCodeList"
                    onChange={this.handleChange}
                    className={styles.datePickerFormControl}
                  >
                    <option hidden>{t("form.label.choisirStatut")}</option>
                    {statuts &&
                      statuts.length &&
                      statuts.map((typeP) => (
                        <option key={typeP.id} value={typeP.code}>
                          {typeP.libelle}
                        </option>
                      ))}
                  </FormControl>
                </Col>
                {/*<Col xs="12" md="2">*/}
                {/*<ControlLabel>{t('form.label.reference')}</ControlLabel>*/}

                {/*<FormControl type="text" {...reference}*/}
                {/*className={styles.datePickerFormControl}/>*/}
                {/*</Col>*/}

                <Col xs="12" md="3">
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.handleSearch(values);
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-search " /> {t("form.buttons.search")}
                  </Button>
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.handleReset();
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-refresh " /> {t("form.buttons.reset")}
                  </Button>
                </Col>
              </Row>
            </form>
          </Row>

          <div className="table-responsive tablediv">
            <Griddle
              results={listVirement}
              tableClassName="table"
              customNoDataComponent={NoDataComponent}
              useGriddleStyles={false}
              useCustomPagerComponent="true"
              resultsPerPage={10}
              customPagerComponent={VirementHistoriquePagination}
              columns={[
                "dateCreation",
                "reference",
                "intituleCrediter",
                "motif",
                "montantS",
                "statut",
                "action",
              ]}
              columnMetadata={[
                {
                  columnName: "dateCreation",
                  displayName: t("list.cols.dateCreation"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "reference",
                  displayName: t("list.cols.reference"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "intituleCrediter",
                  displayName: t("list.cols.beneficiaire"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "motif",
                  displayName: t("list.cols.motif"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "montantS",
                  displayName: t("list.cols.montant"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "statut",
                  displayName: t("list.cols.statut"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "action",
                  displayName: t("list.cols.actions"),
                  customHeaderComponent: ActionHeaderComponent,
                  customComponent: ActionComponent,
                  sortable: false,
                },
              ]}
            />
          </div>
        </div>
      </div>
    );
  }
}
