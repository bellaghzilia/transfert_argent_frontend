/**
 * Created by lenovo on 07/10/2019.
 */
import React, { Component } from "react";
import {
  Button,
  Col,
  FormControl,
  Link,
  ControlLabel,
  Modal,
  responsive,
  Row,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";
import { initializeWithKey, reduxForm, change } from "redux-form";
import { browserHistory } from "react-router";
import { translate } from "react-i18next";
import Griddle from "griddle-react";
import * as HistoriqueActions from "./HistoriqueReducer";
import TransfertHistoriquePagination from "./TransfertHistoriquePagination";
import { connect } from "react-redux";
import { asyncConnect } from "redux-connect";
import { reset } from "redux-form";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

class ActionHeaderComponent extends Component {
  render() {
    return <div className="text-center">{this.props.displayName}</div>;
  }
}

class CenterComponent extends Component {
  render() {
    const url = this.props.data;
    return <div>{this.props.data}</div>;
  }
}

@translate(["common"])
class NoDataComponent extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="noDataResult">
        <h4>{t("errors.noResult")}</h4>
      </div>
    );
  }
}

@connect((state) => ({}), { ...HistoriqueActions, initializeWithKey })
@translate(["upgrade"])
class ActionComponant extends Component {
  render() {
    const { t, globalParams, rowData } = this.props;

    const ttipSelect = (
      <Tooltip id="tooltip" className="ttip">
        {t("form.label.visionner")}
      </Tooltip>
    );

    return (
      <div style={{ textAlign: "center" }}>
        {
          <OverlayTrigger placement="top" overlay={ttipSelect}>
            {rowData.statutCode === "Signe" ? (
              <Button
                className="iconbtn"
                onClick={() =>
                  browserHistory.push({
                    pathname: baseUrl + "app/listeUpgrade/" + rowData.id,
                  })
                }
              >
                <i className="f fa fa-eye" />
              </Button>
            ) : (
              <Button className="iconbtn" disabled={true}>
                <i className="fa fa-eye-slash" />
              </Button>
            )}
          </OverlayTrigger>
        }
      </div>
    );
  }
}

export const validate = (values, props) => {
  const errors = {};

  if (!/^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) && values.gsm !== "") {
    errors.gsm = props.t("gsm.msgErr");
  }
  if (
    !/^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.beneficiaire) &&
    values.beneficiaire !== ""
  ) {
    errors.beneficiaire = props.t("gsm.msgErr");
  }

  return errors;
};

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      dispatch(reset("Tp2pSearchForm"));
    },
  },
])
@reduxForm({
  form: "Tp2pSearchForm",
  fields: ["reference", "gsm", "beneficiaire", "statut"],
  validate,
  initialValues: {
    reference: "",
    gsm: "",
    beneficiaire: "",
    statut: "",
  },
  destroyOnUnmount: true,
})
@connect(
  (state) => ({
    statuts: state.HistoriqueReducer.tp2pStatuts,
    searchLoading: state.HistoriqueReducer.searchLoading,
    search: state.HistoriqueReducer.search,
    listTransfert: state.HistoriqueReducer.listTransfert,
  }),
  { ...HistoriqueActions, initializeWithKey }
)
@translate(["historique"])
export default class HistoriqueTransfert extends Component {
  constructor() {
    super();
    this.handleReset = this.handleReset.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      showPanelSearch: true,
      dateSaisie: "",
      typeOperation: bankCode === "00861" ? "TRANSFER" : "W",
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.search !== nextProps.search && nextProps.search === "") {
      this.props.dispatch(reset("Tp2pSearchForm"));
    }
  }

  handleReset() {
    this.props.loadTransfertHistory(1, "", this.state.typeOperation);
    this.props.dispatch(reset("Tp2pSearchForm"));
  }

  handleChange(event) {
    this.props.dispatch(change("Tp2pSearchForm", "statut", event.target.value));
  }

  handleSearch(values) {
    let search = "";

    if (
      /^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) &&
      values.gsm !== ""
    ) {
      search =
        search +
        "createdByUser:" +
        values.gsm.substring(values.gsm.length - 9, values.gsm.length);
    }

    if (
      /^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.beneficiaire) &&
      values.beneficiaire !== ""
    ) {
      if (search !== "") search = search + ",";
      search =
        search +
        "telephone:" +
        values.beneficiaire.substring(
          values.beneficiaire.length - 9,
          values.beneficiaire.length
        );
    }

    if (values.statut !== "" && values.statut !== "Choisir un statut") {
      if (search !== "") search = search + ",";
      search = search + "statut:" + values.statut;
    }

    this.props.loadTransfertHistory(1, search, this.state.typeOperation);
  }

  render() {
    const {
      t,
      listTransfert,
      handleSubmit,
      searchLoading,
      values,
      statuts,
      fields: { gsm, beneficiaire, reference, statut },
    } = this.props;
    const styles = require("../Souscription/Souscription.scss");
    return (
      <div>
        <div>
          <Modal
            show={searchLoading}
            className="loadingModal"
            backdrop="static"
            keyboard={false}
          >
            <Modal.Body>
              <Row>
                <Col xs={12} md={12}>
                  <div className="spinner">
                    <span style={{ fontSize: "11px" }}>
                      {t("chargement.title")}
                    </span>
                  </div>
                </Col>
              </Row>
            </Modal.Body>
          </Modal>

          <Row>
            <form
              className="formContainer"
              style={{ marginTop: "20px", paddingBottom: "20px" }}
            >
              <Row className={styles.fieldRow}>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.emeteur")}</ControlLabel>

                  <FormControl
                    type="text"
                    {...gsm}
                    className={styles.datePickerFormControl}
                  />
                  {gsm.error && gsm.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {gsm.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.beneficiaire")}</ControlLabel>

                  <FormControl
                    type="text"
                    {...beneficiaire}
                    className={styles.datePickerFormControl}
                  />
                  {beneficiaire.error && beneficiaire.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {beneficiaire.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.statut")}</ControlLabel>
                  <FormControl
                    componentClass="select"
                    {...statut}
                    ref="typeCodeList"
                    onChange={this.handleChange}
                    className={styles.datePickerFormControl}
                  >
                    <option hidden>{t("form.label.choisirStatut")}</option>
                    {statuts &&
                      statuts.length &&
                      statuts.map((typeP) => (
                        <option key={typeP.id} value={typeP.code}>
                          {typeP.libelle}
                        </option>
                      ))}
                  </FormControl>
                </Col>
                {/*<Col xs="12" md="2">*/}
                {/*<ControlLabel>{t('form.label.reference')}</ControlLabel>*/}

                {/*<FormControl type="text" {...reference}*/}
                {/*className={styles.datePickerFormControl}/>*/}
                {/*</Col>*/}

                <Col xs="12" md="3">
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.handleSearch(values);
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-search " /> {t("form.buttons.search")}
                  </Button>
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.handleReset();
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-refresh " /> {t("form.buttons.reset")}
                  </Button>
                </Col>
              </Row>
            </form>
          </Row>

          <div className="table-responsive tablediv">
            <Griddle
              results={listTransfert}
              tableClassName="table"
              customNoDataComponent={NoDataComponent}
              useGriddleStyles={false}
              useCustomPagerComponent="true"
              resultsPerPage={10}
              customPagerComponent={TransfertHistoriquePagination}
              columns={[
                "dateTransfert",
                "walletDebiter",
                "telephoneBeneficiare",
                "motif",
                "montant",
                "statut",
              ]}
              columnMetadata={[
                {
                  columnName: "dateTransfert",
                  displayName: t("list.cols.dateCreation"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "walletDebiter",
                  displayName: t("list.cols.emeteur"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "telephoneBeneficiare",
                  displayName: t("list.cols.beneficiaire"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "motif",
                  displayName: t("list.cols.motif"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "montant",
                  displayName: t("list.cols.montant"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "statut",
                  displayName: t("list.cols.statut"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
              ]}
            />
          </div>
        </div>
      </div>
    );
  }
}
