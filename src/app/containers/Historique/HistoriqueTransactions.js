import React, { Component } from "react";
import {
  Button,
  Col,
  FormControl,
  ControlLabel,
  Link,
  responsive,
  Modal,
  Row,
  Tooltip,
  OverlayTrigger,
  ButtonGroup,
} from "react-bootstrap";
import { initializeWithKey, reduxForm, change } from "redux-form";
import { browserHistory } from "react-router";
import { translate } from "react-i18next";
import Griddle from "griddle-react";
import * as HistoriqueAction from "./HistoriqueReducer";
import HistoriquePagination from "./TransactionHistoriquePagination";
import { connect } from "react-redux";
import { asyncConnect } from "redux-connect";
import { reset } from "redux-form";
import DatePicker from "react-datepicker";
import * as HistoriqueActions from "./HistoriqueReducer";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

class ActionHeaderComponent extends Component {
  render() {
    return <div className="text-center">{this.props.displayName}</div>;
  }
}

class CenterComponent extends Component {
  render() {
    const url = this.props.data;
    if (url === "Agent") {
      return <div>A distance</div>;
    } else if (url === "Payrolls") {
      return <div>Payroll</div>;
    } else return <div>{this.props.data}</div>;
  }
}

@translate(["common"])
class NoDataComponent extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="noDataResult">
        <h4>{t("errors.noResult")}</h4>
      </div>
    );
  }
}

export const validate = (values, props) => {
  const errors = {};

  return errors;
};

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      dispatch(reset("TransactionSearchForm"));
    },
  },
])
@reduxForm({
  form: "TransactionSearchForm",
  fields: ["operationType", "reference", "dateDebut", "dateFin", "typeReport"],
  validate,
  initialValues: {
    reference: "",
    operationType: "",
    dateDebut: "",
    dateFin: "",
    typeReport: "",
  },
  destroyOnUnmount: true,
})
@connect(
  (state) => ({
    userFrontDetails: state.user.userFrontDetails,
    searchLoading: state.HistoriqueReducer.transactionSearchLoading,
    typeOperationList: state.HistoriqueReducer.typeOperationList,
    search: state.HistoriqueReducer.search,
  }),
  { ...HistoriqueAction, initializeWithKey }
)
@translate(["historique"])
export default class HistoriqueTransactions extends Component {
  constructor() {
    super();
    this.handleReset = this.handleReset.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeTypeReport = this.handleChangeTypeReport.bind(this);
    this.handleChangeDateDebut = this.handleChangeDateDebut.bind(this);
    this.handleChangeDateFin = this.handleChangeDateFin.bind(this);

    this.state = {
      dateDebut: "",
      dateFin: "",
      selectTypeReport: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.search !== nextProps.search && nextProps.search === "") {
      this.props.dispatch(reset("TransactionSearchForm"));
    }
  }

  handleReset() {
    this.props.dispatch(reset("TransactionSearchForm"));
    this.props.loadTransactionsList(1, "");
    this.setState({
      dateDebut: "",
      dateFin: "",
    });
  }

  handleChangeDateDebut = (date) => {
    this.setState({
      dateDebut: date,
    });
  };

  handleChangeDateFin = (date) => {
    this.setState({
      dateFin: date,
    });
  };

  handleChange(event) {
    this.props.dispatch(
      change("TransactionSearchForm", "operationType", event.target.value)
    );
  }

  handleChangeTypeReport(event) {
    this.props.dispatch(
      change("TransactionSearchForm", "typeReport", event.target.value)
    );
  }

  handleSearch(values) {
    let search = "";
    if (values.reference !== "") {
      if (search !== "") search = search + ",";
      search = search + "refCommission:" + values.reference;
    }
    if (values.operationType !== "") {
      if (search !== "") search = search + ",";
      search = search + "operationType:" + values.operationType;
    }
    if (values.dateDebut !== "") {
      if (search !== "") {
        search += ",";
      }
      search =
        search + "dateOperation>" + values.dateDebut.replace(/\//gi, "_");
    }
    if (values.dateFin !== "") {
      if (search !== "") {
        search += ",";
      }
      search = search + "dateOperation<" + values.dateFin.replace(/\//gi, "_");
    }
    this.props.loadTransactionsList(1, search);
  }

  handleExport(values) {
    let search = "";
    let index = 1;

    if (values.reference !== "") {
      if (search !== "") search = search + ",";
      search = search + "refCommission:" + values.reference;
    }
    if (values.operationType !== "") {
      if (search !== "") search = search + ",";
      search = search + "operationType:" + values.operationType;
    }
    if (values.dateDebut !== "") {
      if (search !== "") {
        search += ",";
      }
      search =
        search + "dateOperation>" + values.dateDebut.replace(/\//gi, "_");
    }
    if (values.dateFin !== "") {
      if (search !== "") {
        search += ",";
      }
      search = search + "dateOperation<" + values.dateFin.replace(/\//gi, "_");
    }
    this.setState({ selectTypeReport: false });
    window.open(
      baseUrl +
        "client/exporterListe?search=" +
        search +
        "&dateDebut=" +
        values.dateDebut +
        "&dateFin=" +
        values.dateFin +
        "&typeReport=" +
        values.typeReport +
        "&page=" +
        index +
        "&offset=0&max=10"
    );

    console.log("exporting file");
  }

  showExportModal() {
    this.setState({ selectTypeReport: true });
  }

  render() {
    const {
      t,
      handleSubmit,
      searchLoading,
      userFrontDetails,
      listTransactions,
      typeOperationList,
      values,
      fields: { operationType, reference, dateDebut, dateFin, typeReport },
    } = this.props;
    const styles = require("../Souscription/Souscription.scss");
    const closetypeReportModal = () => {
      this.setState({
        selectTypeReport: false,
      });
    };

    return (
      <div>
        <div>
          <Modal
            show={searchLoading}
            className="loadingModal"
            backdrop="static"
            keyboard={false}
          >
            <Modal.Body>
              <Row>
                <Col xs={12} md={12}>
                  <div className="spinner">
                    <span style={{ fontSize: "11px" }}>
                      {t("chargement.title")}
                    </span>
                  </div>
                </Col>
              </Row>
            </Modal.Body>
          </Modal>

          <Modal
            show={this.state.selectTypeReport}
            onHide={closetypeReportModal}
            container={this}
            aria-labelledby="contained-modal-title"
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title">
                <div>{t("form.label.typeDoc")}</div>
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <Row className={styles.fieldRow}>
                  <Col xs="4" md="6">
                    <ControlLabel>
                      {t("form.message.selectTypeReport")}
                    </ControlLabel>

                    <FormControl
                      componentClass="select"
                      {...typeReport}
                      ref="typeReport"
                      onChange={this.handleChangeTypeReport}
                      className={styles.datePickerFormControl}
                    >
                      <option value="excel">Excel</option>
                      <option value="pdf">Pdf</option>
                    </FormControl>
                  </Col>
                </Row>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <ButtonGroup className="pull-right" bsSize="small">
                <Button
                  bsStyle="primary"
                  style={{ marginTop: "8%" }}
                  onClick={() => {
                    this.handleExport(values);
                  }}
                >
                  <i className="fa fa-refresh" /> {t("form.buttons.exporter")}{" "}
                </Button>
              </ButtonGroup>
            </Modal.Footer>
          </Modal>

          <Row>
            <form
              className="formContainer"
              style={{ marginTop: "20px", paddingBottom: "20px" }}
            >
              <Row className={styles.fieldRow}>
                <Col xs="12" md="2">
                  <ControlLabel>{t("form.label.reference")}</ControlLabel>

                  <FormControl
                    type="text"
                    {...reference}
                    className={styles.datePickerFormControl}
                  />
                  {reference.error && reference.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {reference.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="2">
                  <ControlLabel>{t("form.label.operationType")}</ControlLabel>
                  <FormControl
                    componentClass="select"
                    {...operationType}
                    ref="typeOperationList"
                    onChange={this.handleChange}
                    className={styles.datePickerFormControl}
                  >
                    <option hidden>
                      {t("form.label.choisirTypeOperation")}
                    </option>
                    {typeOperationList &&
                      typeOperationList.length &&
                      typeOperationList.map((typeP) => (
                        <option key={typeP.id} value={typeP.code}>
                          {typeP.libelle}
                        </option>
                      ))}
                  </FormControl>
                </Col>
                <Col xs="12" md="2">
                  <ControlLabel>{t("form.label.dateDebut")}</ControlLabel>
                  <span {...dateDebut}>
                    <DatePicker
                      placeholderText={t("form.label.dateDebutRecherche")}
                      onChange={this.handleChangeDateDebut}
                      selected={this.state.dateDebut}
                      dateDebut={this.state.dateDebut}
                      className={styles.datePickerFormControl}
                      isClearable="true"
                      locale="fr-FR"
                      dateFormat="DD/MM/YYYY"
                    />
                  </span>
                </Col>

                <Col xs="12" md="2">
                  <ControlLabel>{t("form.label.dateFin")}</ControlLabel>
                  <span {...dateFin}>
                    <DatePicker
                      placeholderText={t("form.label.dateFinRecherche")}
                      onChange={this.handleChangeDateFin}
                      selected={this.state.dateFin}
                      dateFin={this.state.dateFin}
                      className={styles.datePickerFormControl}
                      isClearable="true"
                      locale="fr-FR"
                      dateFormat="DD/MM/YYYY"
                    />
                  </span>
                </Col>
                <Col xs="12" md="4">
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.handleSearch(values);
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-search " /> {t("form.buttons.search")}
                  </Button>
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.handleReset();
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-refresh " /> {t("form.buttons.reset")}
                  </Button>
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.showExportModal();
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-file-pdf-o fa-4" />{" "}
                    {t("form.buttons.exporter")}
                  </Button>
                </Col>
              </Row>
            </form>
          </Row>

          <div className="table-responsive tablediv">
            <Griddle
              results={listTransactions}
              tableClassName="table"
              customNoDataComponent={NoDataComponent}
              useGriddleStyles={false}
              useCustomPagerComponent="true"
              resultsPerPage={10}
              customPagerComponent={HistoriquePagination}
              columns={[
                "dateOperation",
                "refCommission",
                "usernameAgent",
                "operationType",
                "montant",
                "commission",
              ]}
              columnMetadata={[
                {
                  columnName: "dateOperation",
                  displayName: t("list.cols.dateOperation"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "refCommission",
                  displayName: t("list.cols.reference"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "usernameAgent",
                  displayName: t("list.cols.agent"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "operationType",
                  displayName: t("list.cols.operationType"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "montant",
                  displayName: t("list.cols.montant"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "commission",
                  displayName: t("list.cols.commission"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
              ]}
            />
          </div>
        </div>
      </div>
    );
  }
}
