/**
 * Created by lenovo on 17/01/2019.
 */
import React, { Component } from "react";
import PropTypes from "prop-types";

import { Button, Pagination } from "react-bootstrap";
import { connect } from "react-redux";
import { initializeWithKey } from "redux-form";
import * as SouscriptionActions from "../SouscriptionRejete/SouscriptionRejeteListReducer";

@connect(
  (state) => ({
    maxPages: state.SouscriptionRejeteListReducer.souscriptionMaxPages,
    search: state.SouscriptionRejeteListReducer.search,
    index: state.SouscriptionRejeteListReducer.index,
  }),
  { ...SouscriptionActions, initializeWithKey }
)
export default class PaginationSouscriptionRejete extends Component {
  static propTypes = {
    loadRejectedSouscriptionList: PropTypes.func,
    maxPages: PropTypes.number,
  };
  constructor() {
    super();
    this.state = {
      activePage: 1,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.index !== nextProps.index &&
      this.props.index !== 1 &&
      nextProps.index === 1
    ) {
      this.setState({ activePage: nextProps.index });
    }
  }

  render() {
    const { loadRejectedSouscriptionList, maxPages, search } = this.props;
    let maxPage = 0;
    if (maxPages % 10 === 0) {
      maxPage = maxPages / 10;
    } else {
      maxPage = Math.ceil(maxPages / 10);
    }
    const changePage = (event) => {
      const changePage = (event) => {
        this.setState({
          activePage: event,
        });
        if (search && search !== "") {
          loadRejectedSouscriptionList(event, search);
        } else {
          loadRejectedSouscriptionList(event, "");
        }
      };
    };
    return (
      <div>
        {maxPages > 10 ? (
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={maxPage}
            maxButtons={5}
            bsSize="medium"
            activePage={this.state.activePage}
            onSelect={changePage}
          />
        ) : (
          <div />
        )}
      </div>
    );
  }
}
