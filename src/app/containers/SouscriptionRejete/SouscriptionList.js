import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Row,
  ButtonGroup,
  Modal,
  PageHeader,
  Col,
  FormControl,
  ControlLabel,
} from "react-bootstrap";
import { translate } from "react-i18next";
import { connect } from "react-redux";
import { initializeWithKey, reduxForm } from "redux-form";
import Griddle from "griddle-react";
import { browserHistory } from "react-router";
import moment from "moment";
import Button from "react-bootstrap-button-loader";
import * as UserActions from "../User/UserReducer";
import * as SouscriptionActions from "./SouscriptionRejeteListReducer";
import { AuthorizedComponent } from "react-router-role-authorization";
import { asyncConnect } from "redux-connect";
import * as AttachementActions from "../../components/Attachement/AttachementReducer";
import SouscriptionRejetePagination from "./SouscriptionRejetePagination";
import { reset } from "redux-form";

@connect(
  (state) => ({
    userFrontDetails: state.user.userFrontDetails,
    dataForDetail: state.moneyTransfer.dataForDetail,
    loadingSigneTransfert: state.moneyTransfer.loadingSigneTransfert,
    successSigneTransfert: state.moneyTransfer.successSigneTransfert,
    id: state.moneyTransfer.saveSuccessObject.id,
  }),
  {
    ...AttachementActions,
    ...SouscriptionActions,
    ...UserActions,
    initializeWithKey,
  }
)
@translate(["souscription"], { wait: true })
class ActionComponent extends Component {
  static propTypes = {
    getDemandeInstance: PropTypes.func,
    getDocuments: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      showModalSigne: false,
      showModal: false,
      canceling: false,
      deleting: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillMount() {
    this.props.reload();
  }

  async handleClick() {
    try {
    } catch (error) {
      console.log(error.message);
    }
    this.setState({ showModal: false });
  }

  async getSouscriptionInstance(id) {
    await this.props.getDemandeInstance(id);
    this.props.getDocuments(id);
  }

  render() {
    const { t, getSouscriptionInstance } = this.props;
    const styles = require("./souscriptionList.scss");
    const idtransfert = this.props.rowData.id;
    const close = () => {
      this.setState({ showModal: false });
      this.setState({ showModalSigne: false });
    };

    return (
      <div>
        <ButtonGroup>
          <Button
            bsSize="small"
            bsStyle="default"
            className={"pull-right  " + styles.actionButtonStyle}
            onClick={() =>
              browserHistory.push(
                baseUrl + "app/SouscriptionType/update/" + idtransfert
              )
            }
          >
            <i className="fa fa-pencil fa-fw" />
          </Button>

          <Modal
            show={this.state.showModal}
            onHide={close}
            container={this}
            aria-labelledby="contained-modal-title"
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title">
                {this.state.deleting && (
                  <div>{t("popup.supression.title")}</div>
                )}
                {this.state.canceling && (
                  <div>{t("popup.confirmation.title")}</div>
                )}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {this.state.canceling && <div>{t("popup.confirmation.msg")}</div>}
              {this.state.deleting && <div>{t("popup.supression.msg")}</div>}
            </Modal.Body>
            <Modal.Footer>
              <ButtonGroup className="pull-right" bsSize="small">
                <Button
                  className={styles.ButtonPasswordStyle}
                  onClick={() => close()}
                >
                  {t("popup.supression.noBtn")}
                </Button>
                <Button
                  className={styles.ButtonPasswordStyle}
                  onClick={this.handleClick}
                >
                  {t("popup.supression.yesBtn")}
                </Button>
              </ButtonGroup>
            </Modal.Footer>
          </Modal>
        </ButtonGroup>
      </div>
    );
  }
}

class StatusComponent extends Component {
  render() {
    let libelleStatut = "";
    if (this.props.rowData.statutCode === "annuler_client") {
      libelleStatut = <div style={{ color: "#999" }}>{this.props.data}</div>;
    } else if (this.props.rowData.statutCode === "Enregistre") {
      libelleStatut = <div style={{ color: "#FFC125" }}>{this.props.data}</div>;
    } else if (this.props.rowData.statutCode === "rejete") {
      libelleStatut = <div style={{ color: "red" }}>{this.props.data}</div>;
    } else {
      libelleStatut = <div style={{ color: "#859" }}>{this.props.data}</div>;
    }
    return <div>{libelleStatut}</div>;
  }
}

class CenterComponent extends Component {
  render() {
    const url = this.props.data;
    return <div>{this.props.data}</div>;
  }
}

class HeaderComponent extends Component {
  render() {
    return <div>{this.props.displayName}</div>;
  }
}

export const validate = (values, props) => {
  const errors = {};

  if (!/^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) && values.gsm !== "") {
    errors.gsm = props.t("gsm.msgErr");
  }

  return errors;
};

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      return Promise.all([
        dispatch(SouscriptionActions.loadRejectedSouscriptionList(1)),
      ]);
    },
  },
])
@translate(["souscription"], { wait: true })
@reduxForm({
  form: "SouscriptionSearchForm",
  fields: ["gsm"],
  validate,
  initialValues: {
    gsm: "",
  },
  destroyOnUnmount: true,
})
@connect(
  (state) => ({
    souscriptionList: state.SouscriptionRejeteListReducer.souscriptionList,
    dataForDetail: state.SouscriptionRejeteListReducer.dataForDetail,
    loadedDocuments: state.AttachementReducer.loadedDocuments,
    isSuccess: state.SouscriptionRejeteListReducer.isSuccess,
    errorMsg: state.SouscriptionRejeteListReducer.errorMsg,
    searchLoading:
      state.SouscriptionRejeteListReducer.souscriptionSearchLoading,
    search: state.SouscriptionRejeteListReducer.search,
    userFrontDetails: state.user.userFrontDetails,
  }),

  {
    ...AttachementActions,
    ...SouscriptionActions,
    ...UserActions,
    initializeWithKey,
  }
)
export default class TabSouscriptionList extends AuthorizedComponent {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.handleSearch = this.handleSearch.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.state = {
      showModal: false,
      startDate: moment(),
      endDate: moment(),
    };
  }

  async signeTransfert(id) {
    this.setState({ showModal: false });
    await this.props.signeTransfertCltCltByID(id);
    this.props.getInstanceTransfert(id);
  }

  componentDidMount() {
    console.log("componentDidMount");
    console.log("restprops");
  }

  componentWillMount() {
    this.props.reload();
  }

  handleReset() {
    this.props.loadRejectedSouscriptionList(1, "");
    this.props.dispatch(reset("SouscriptionSearchForm"));
  }

  handleSearch(values) {
    let search = "";

    if (
      /^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) &&
      values.gsm !== ""
    ) {
      search =
        search +
        "beneficiaire-" +
        values.gsm.substring(values.gsm.length - 9, values.gsm.length);
    }

    this.props.loadRejectedSouscriptionList(1, search);
  }

  render() {
    const {
      t,
      values,
      souscriptionList,
      dataForDetail,
      loadedDocuments,
      searchLoading,
      fields: { gsm },
      handleSubmit,
      isSuccess,
      errorMsg,
    } = this.props;

    const styles = require("./souscriptionList.scss");
    const panelStyles = { margin: "5px 10px 5px 10px" };
    const stylesDiv = {
      backgroundColor: "white",
      padding: "1%",
      paddingTop: "0%",
      marginTop: "3%",
    };
    const titleStyles = { paddingTop: "1.5%" };
    const close = () => {
      this.setState({ showModal: false });
      this.setState({ showModalSigne: false });
    };

    const gridMetaData = [
      {
        columnName: "dateCreation",
        displayName: t("list.cols.dateCreation"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
        sortable: false,
      },
      {
        columnName: "typeLibelle",
        displayName: t("list.cols.pack"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
        sortable: false,
      },
      {
        columnName: "nom",
        displayName: t("list.cols.nom"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
        sortable: false,
      },
      {
        columnName: "prenom",
        displayName: t("list.cols.prenom"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
        sortable: false,
      },
      {
        columnName: "gsm",
        displayName: t("list.cols.tel"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
        sortable: false,
      },
      {
        columnName: "email",
        displayName: t("list.cols.email"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
      },
      {
        columnName: "statut",
        displayName: t("list.cols.statut"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
        sortable: false,
      },
      {
        columnName: "motifValidation",
        displayName: t("list.cols.motif"),
        customHeaderComponent: HeaderComponent,
        customComponent: CenterComponent,
        sortable: false,
      },
      {
        columnName: "action",
        displayName: t("list.cols.actions"),
        customHeaderComponent: HeaderComponent,
        customComponent: ActionComponent,
        sortable: false,
      },
    ];
    return (
      <div>
        <PageHeader>
          <h3>{t("form.legend.listeOuvertures")} </h3>
        </PageHeader>

        <Modal
          show={searchLoading}
          className="loadingModal"
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>
            <Row>
              <Col xs={12} md={12}>
                <div className="spinner">
                  <span style={{ fontSize: "11px" }}>
                    Chargement en cours ...
                  </span>
                </div>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>
        <Row>
          <form
            className="formContainer"
            style={{ marginTop: "20px", paddingBottom: "20px" }}
          >
            <Row className={styles.fieldRow}>
              <Col xs="12" md="4">
                <ControlLabel>{t("form.label.tel")}</ControlLabel>

                <FormControl
                  type="text"
                  {...gsm}
                  className={styles.datePickerFormControl}
                />
                {gsm.error && gsm.touched && (
                  <div className={styles.error}>
                    <i
                      className="fa fa-exclamation-triangle"
                      aria-hidden="true"
                    >
                      {gsm.error}
                    </i>
                  </div>
                )}
              </Col>
              <Col xs="12" md="1"></Col>
              <Col xs="12" md="4">
                <Button
                  style={{ marginTop: "5%" }}
                  loading={searchLoading}
                  onClick={handleSubmit(() => {
                    this.handleSearch(values);
                  })}
                  bsStyle="primary"
                >
                  <i className="fa fa-search " /> {t("form.buttons.search")}
                </Button>
                <Button
                  style={{ marginTop: "5%" }}
                  loading={searchLoading}
                  onClick={handleSubmit(() => {
                    this.handleReset();
                  })}
                  bsStyle="primary"
                >
                  <i className="fa fa-refresh " /> {t("form.buttons.reset")}
                </Button>
              </Col>
            </Row>
          </form>
        </Row>

        <div className="app-panel">
          <Row className="table-responsive">
            <Griddle
              results={souscriptionList}
              columnMetadata={gridMetaData}
              useGriddleStyles={false}
              noDataMessage={t("list.search.msg.noResult")}
              resultsPerPage={10}
              useCustomPagerComponent="true"
              nextText={<i className="fa fa-chevron-right" />}
              previousText={<i className="fa fa-chevron-left" />}
              customPagerComponent={SouscriptionRejetePagination}
              tableClassName="table"
              columns={[
                "dateCreation",
                "typeLibelle",
                "nom",
                "prenom",
                "gsm",
                "email",
                "statut",
                "motifValidation",
                "action",
              ]}
            />
          </Row>
        </div>
      </div>
    );
  }
}
