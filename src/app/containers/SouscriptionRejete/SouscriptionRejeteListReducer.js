/**
 * Created by lenovo on 15/12/2018.
 */

const LOAD_SOUSCRIPTION = 'SOUSCRIPTION_REJETE/LOAD_SOUSCRIPTION';
const LOAD_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION_REJETE/LOAD_SOUSCRIPTION_SUCCESS';
const LOAD_SOUSCRIPTION_FAIL = 'SOUSCRIPTION_REJETE/LOAD_SOUSCRIPTION_FAIL';

const INSTANCE_SOUSCRIPTION = 'SOUSCRIPTION_REJETE/INSTANCE_SOUSCRIPTION';
const INSTANCE_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION_REJETE/INSTANCE_SOUSCRIPTION_SUCCESS';
const INSTANCE_SOUSCRIPTION_FAIL = 'SOUSCRIPTION_REJETE/INSTANCE_SOUSCRIPTION_FAIL';

const SIGN_SOUSCRIPTION = 'SOUSCRIPTION_REJETE/SIGN_SOUSCRIPTION';
const SIGN_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION_REJETE/SIGN_SOUSCRIPTION_SUCCESS';
const SIGN_SOUSCRIPTION_FAIL = 'SOUSCRIPTION_REJETE/SIGN_SOUSCRIPTION_FAIL';

const RELOAD = 'SOUSCRIPTION_REJETE/RELOAD';

export default function reducer(state = {}, action = {}) {
    switch (action.type) {
        case LOAD_SOUSCRIPTION:
            return {
                ...state,
                souscriptionSearchLoading: true,
                isRecap:false
            };
        case LOAD_SOUSCRIPTION_SUCCESS: {
            let souscriptionList = action.result.demandeList;
            souscriptionList.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                search : action.search,
                index : action.index,
                souscriptionList : souscriptionList,
                souscriptionMaxPages: action.result.total,
                souscriptionSearchLoading: false,
                loaded: true,
            };
        }
        case LOAD_SOUSCRIPTION_FAIL:
            return {
                ...state,
                souscriptionSearchLoading: false,
                loaded: false,
                souscriptionList: null,
                error: action.error
            };
        case INSTANCE_SOUSCRIPTION:
            return {
                ...state,
                loading: true,
                isRecap:false
            };
        case INSTANCE_SOUSCRIPTION_SUCCESS: {
            return {
                ...state,
                dataForDetail : action.result.demandeInstance,
                loading: false,
                loaded: true,
            };
        }
        case INSTANCE_SOUSCRIPTION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                dataForDetail: null,
                error: action.error
            };
        case SIGN_SOUSCRIPTION:
            return {
                ...state,
                loading: true,
                isRecap:false
            };
        case SIGN_SOUSCRIPTION_SUCCESS: {
            if (action.result.success) {
                return {
                    ...state,
                    dataForDetail: action.result.demandeInstance,
                    isSuccess : action.result.success,
                    loading: false,
                    loaded:true,
                    errorMsg: action.result.messageRetour
                };
            } else {
                return {
                    ...state,
                    isSuccess: action.result.success,
                    loading: false,
                    loaded: true,
                    errorMsg: action.result.messageRetour
                }
            }
        }
        case SIGN_SOUSCRIPTION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                dataForDetail: null,
                error: action.error
            };
        case RELOAD:
            return {
                ...state,
                loading: false,
                loaded: false,
                isRecap : false,
                isUpdate : false,
                isSuccess : false,
                dataForDetail : null,
                errorMsg : ""
            };
        default:
            return state;

    }
}

function objectToParams(object) {
    let str = '';
    for (const key in object) {
        if (str !== '') {
            str += '&';
        }
        str += key + '=' + encodeURIComponent(object[key]);
    }
    return str;
}



export function loadRejectedSouscriptionList(index, search) {

    return {
        search,
        index,
        types: [LOAD_SOUSCRIPTION, LOAD_SOUSCRIPTION_SUCCESS, LOAD_SOUSCRIPTION_FAIL],
        promise: (client) => client.get('demandeSouscription/rejectedList?search=' + search + '&page='+index+'&offset=0&max=10')
    };
}

export function getDemandeInstance(id) {
    return {
        types: [INSTANCE_SOUSCRIPTION, INSTANCE_SOUSCRIPTION_SUCCESS, INSTANCE_SOUSCRIPTION_FAIL],
        promise: (client) => client.get('demandeSouscription/instance/'+id)
    };
}

export function signSouscription(values, dto) {
    dto.motifValidation = values.motif;
    return {
        types: [SIGN_SOUSCRIPTION, SIGN_SOUSCRIPTION_SUCCESS, SIGN_SOUSCRIPTION_FAIL],
        promise: (client) => client.post('demandeSouscription/submit', {data: objectToParams(dto)})
    };
}

export function reload(){
    return{
        type: RELOAD,
    }
}




