const SAVE_SOUSCRIPTION = 'SOUSCRIPTION/SAVE_SOUSCRIPTION';
const SAVE_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION/SAVE_SOUSCRIPTION_SUCCESS';
const SAVE_SOUSCRIPTION_FAIL = 'SOUSCRIPTION/SAVE_SOUSCRIPTION_FAIL';

const LOAD_TYPE_PACK = 'SOUSCRIPTION/LOAD_TYPE_PACK';
const LOAD_TYPE_PACK_SUCCESS = 'SOUSCRIPTION/LOAD_TYPE_PACK_SUCCESS';
const LOAD_TYPE_PACK_FAIL = 'SOUSCRIPTION/LOAD_TYPE_PACK_FAIL';

const LOAD_IS_SIRON = 'SOUSCRIPTION/LOAD_IS_SIRON';
const LOAD_IS_SIRON_SUCCESS = 'SOUSCRIPTION/LOAD_IS_SIRON_SUCCESS';
const LOAD_IS_SIRON_FAIL = 'SOUSCRIPTION/LOAD_IS_SIRON_FAIL';

const LOAD_PROFESSION = 'SOUSCRIPTION/LOAD_PROFESSION';
const LOAD_PROFESSION_SUCCESS = 'SOUSCRIPTION/LOAD_PROFESSION_SUCCESS';
const LOAD_PROFESSION_FAIL = 'SOUSCRIPTION/LOAD_PROFESSION_FAIL';

const LOAD_TYPE_TRANSACTION = 'SOUSCRIPTION/LOAD_TYPE_TRANSACTION';
const LOAD_TYPE_TRANSACTION_SUCCESS = 'SOUSCRIPTION/LOAD_TYPE_TRANSACTION_SUCCESS';
const LOAD_TYPE_TRANSACTION_FAIL = 'SOUSCRIPTION/LOAD_TYPE_TRANSACTION_FAIL';

const LOAD_OBJET_RELATION = 'SOUSCRIPTION/LOAD_OBJET_RELATION';
const LOAD_OBJET_RELATION_SUCCESS = 'SOUSCRIPTION/LOAD_OBJET_RELATION_SUCCESS';
const LOAD_OBJET_RELATION_FAIL = 'SOUSCRIPTION/LOAD_OBJET_RELATION_FAIL';

const LOAD_FOND_ORIGIN = 'SOUSCRIPTION/LOAD_FOND_ORIGIN';
const LOAD_FOND_ORIGIN_SUCCESS = 'SOUSCRIPTION/LOAD_FOND_ORIGIN_SUCCESS';
const LOAD_FOND_ORIGIN_FAIL = 'SOUSCRIPTION/LOAD_FOND_ORIGIN_FAIL';


const LOAD_TRANCHES = 'SOUSCRIPTION/LOAD_TRANCHES';
const LOAD_TRANCHES_SUCCESS = 'SOUSCRIPTION/LOAD_TRANCHES_SUCCESS';
const LOAD_TRANCHES_FAIL = 'SOUSCRIPTION/LOAD_TRANCHES_FAIL';

const LOAD_TITLE = 'SOUSCRIPTION/LOAD_TITLE';
const LOAD_TITLE_SUCCESS = 'SOUSCRIPTION/LOAD_TITLE_SUCCESS';
const LOAD_TITLE_FAIL = 'SOUSCRIPTION/LOAD_TITLE_FAIL';

const LOAD_PAYS = 'SOUSCRIPTION/LOAD_PAYS';
const LOAD_PAYS_SUCCESS = 'SOUSCRIPTION/LOAD_PAYS_SUCCESS';
const LOAD_PAYS_FAIL = 'SOUSCRIPTION/LOAD_PAYS_FAIL';

const LOAD_TYPE_ACTIVITE = 'SOUSCRIPTION/LOAD_TYPE_ACTIVITE';
const LOAD_TYPE_ACTIVITE_SUCCESS = 'SOUSCRIPTION/LOAD_TYPE_ACTIVITE_SUCCESS';
const LOAD_TYPE_ACTIVITE_FAIL = 'SOUSCRIPTION/LOAD_TYPE_ACTIVITE_FAIL';

const LOAD_TYPE_IDENTITE = 'SOUSCRIPTION/LOAD_TYPE_IDENTITE';
const LOAD_TYPE_IDENTITE_SUCCESS = 'SOUSCRIPTION/LOAD_TYPE_IDENTITE_SUCCESS';
const LOAD_TYPE_IDENTITE_FAIL = 'SOUSCRIPTION/LOAD_TYPE_IDENTITE_FAIL';

const LOAD_TYPE_JUSTIF = 'SOUSCRIPTION/LOAD_TYPE_JUSTIF';
const LOAD_TYPE_JUSTIF_SUCCESS = 'SOUSCRIPTION/LOAD_TYPE_JUSTIF_SUCCESS';
const LOAD_TYPE_JUSTIF_FAIL = 'SOUSCRIPTION/LOAD_TYPE_JUSTIF_FAIL';

const LOAD_MCC = 'SOUSCRIPTION/LOAD_MCC';
const LOAD_MCC_SUCCESS = 'SOUSCRIPTION/LOAD_MCC_SUCCESS';
const LOAD_MCC_FAIL = 'SOUSCRIPTION/LOAD_MCC_FAIL';

const LOAD_PACK_ATTACHEMENTS = 'SOUSCRIPTION/LOAD_PACK_ATTACHEMENTS';
const LOAD_PACK_ATTACHEMENTS_SUCCESS = 'SOUSCRIPTION/LOAD_PACK_ATTACHEMENTS_SUCCESS';
const LOAD_PACK_ATTACHEMENTS_FAIL = 'SOUSCRIPTION/LOAD_PACK_ATTACHEMENTS_FAIL';

const SUBMIT_SOUSCRIPTION = 'SOUSCRIPTION/SUBMIT_SOUSCRIPTION';
const SUBMIT_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION/SUBMIT_SOUSCRIPTION_SUCCESS';
const SUBMIT_SOUSCRIPTION_FAIL = 'SOUSCRIPTION/SUBMIT_SOUSCRIPTION_FAIL';

const RESET = 'SOUSCRIPTION/RESET';
const RELOAD = 'SOUSCRIPTION/RELOAD';
const RELOADFORM = 'SOUSCRIPTION/RELOADFORM';

const UPDATE_SOUSCRIPTION = 'SOUSCRIPTION/UPDATE_SOUSCRIPTION';
const UPDATE_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION/UPDATE_SOUSCRIPTION_SUCCESS';
const UPDATE_SOUSCRIPTION_FAIL = 'SOUSCRIPTION/UPDATE_SOUSCRIPTION_FAIL';

const INSTANCE_SOUSCRIPTION = 'SOUSCRIPTION/INSTANCE_SOUSCRIPTION';
const INSTANCE_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION/INSTANCE_SOUSCRIPTION_SUCCESS';
const INSTANCE_SOUSCRIPTION_FAIL = 'SOUSCRIPTION/INSTANCE_SOUSCRIPTION_FAIL';

const SEND_OTP = 'SOUSCRIPTION/SEND_OTP';
const SEND_OTP_SUCCESS = 'SOUSCRIPTION/SEND_OTP_SUCCESS';
const SEND_OTP_FAIL = 'SOUSCRIPTION/SEND_OTP_FAIL';

const RESEND_OTP = 'SOUSCRIPTION/RESEND_OTP';
const RESEND_OTP_SUCCESS = 'SOUSCRIPTION/RESEND_OTP_SUCCESS';
const RESEND_OTP_FAIL = 'SOUSCRIPTION/RESEND_OTP_FAIL';

const GENERATE_REPORT = 'SOUSCRIPTION/GENERATE_REPORT';
const GENERATE_REPORT_SUCCESS = 'SOUSCRIPTION/GENERATE_REPORT_SUCCESS';
const GENERATE_REPORT_FAIL = 'SOUSCRIPTION/GENERATE_REPORT_FAIL';

const LOAD_TYPE_COMMERCANT = 'SOUSCRIPTION/LOAD_TYPE_COMMERCANT';
const LOAD_TYPE_COMMERCANT_SUCCESS = 'SOUSCRIPTION/LOAD_TYPE_COMMERCANT_SUCCESS';
const LOAD_TYPE_COMMERCANT_FAIL = 'SOUSCRIPTION/LOAD_TYPE_COMMERCANT_FAIL';

const LOAD_CHIFFRE_AFFAIRE = 'SOUSCRIPTION/LOAD_CHIFFRE_AFFAIRE';
const LOAD_CHIFFRE_AFFAIRE_SUCCESS = 'SOUSCRIPTION/LOAD_CHIFFRE_AFFAIRE_SUCCESS';
const LOAD_CHIFFRE_AFFAIRE_FAIL = 'SOUSCRIPTION/LOAD_CHIFFRE_AFFAIRE_FAIL';



const initialState = {
    SouscriptionSaved: null,
    SouscriptionUpdated: null,
    typeCode: "type_one",
    typeCommercant:"personne_physique"
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {

        /*------------------------------------------------------------------------------------------*/
        case LOAD_IS_SIRON:
            return {
                ...state,
                loading: true
            };
        case LOAD_IS_SIRON_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                showSironFields: action.result.activated,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_IS_SIRON_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_TITLE:
            return {
                ...state,
                loading: true
            };
        case LOAD_TITLE_SUCCESS:
            const titleList = action.result.titleList;
            return {
                ...state,
                loading: false,
                loaded: true,
                titleList,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_TITLE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_PROFESSION:
            return {
                ...state,
                loading: true
            };
        case LOAD_PROFESSION_SUCCESS:
            const professionList = action.result.professionList;
            return {
                ...state,
                loading: false,
                loaded: true,
                professionList,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_PROFESSION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_TYPE_TRANSACTION:
            return {
                ...state,
                loading: true
            };
        case LOAD_TYPE_TRANSACTION_SUCCESS:
            const transactionTypeList = action.result.transactionTypeList;
            return {
                ...state,
                loading: false,
                loaded: true,
                transactionTypeList,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_TYPE_TRANSACTION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_OBJET_RELATION:
            return {
                ...state,
                loading: true
            };
        case LOAD_OBJET_RELATION_SUCCESS:
            const relationList = action.result.objetRelationList;
            return {
                ...state,
                loading: false,
                loaded: true,
                relationList,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_OBJET_RELATION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_FOND_ORIGIN:
            return {
                ...state,
                loading: true
            };
        case LOAD_FOND_ORIGIN_SUCCESS:
            const sourceRevenuList = action.result.sourceRevenuList;
            return {
                ...state,
                loading: false,
                loaded: true,
                sourceRevenuList,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_FOND_ORIGIN_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_TRANCHES:
            return {
                ...state,
                loading: true
            };
        case LOAD_TRANCHES_SUCCESS:
            const tranchesRevenuList = action.result.tranchesRevenuList;
            return {
                ...state,
                loading: false,
                loaded: true,
                tranchesRevenuList,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_TRANCHES_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_PAYS:
            return {
                ...state,
                loading: true
            };
        case LOAD_PAYS_SUCCESS:
            const paysList = action.result.paysList;
            return {
                ...state,
                loading: false,
                loaded: true,
                paysList,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_PAYS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_TYPE_PACK:
            return {
                ...state,
                loading: true
            };
        case LOAD_TYPE_PACK_SUCCESS:
            const typesPack = action.result.typesPacksList;
            return {
                ...state,
                loading: false,
                loaded: true,
                typesPack,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_TYPE_PACK_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_PACK_ATTACHEMENTS:
            return {
                ...state,
                loading: true
            };
        case LOAD_PACK_ATTACHEMENTS_SUCCESS:
            const isTypeOneNeedAttachments = action.result.packOneAttachement;
            return {
                ...state,
                loading: false,
                loaded: true,
                isTypeOneNeedAttachments,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_PACK_ATTACHEMENTS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_TYPE_ACTIVITE:
            return {
                ...state,
                loading: true
            };
        case LOAD_TYPE_ACTIVITE_SUCCESS:
            const activiteList = action.result.activiteList;
            return {
                ...state,
                loading: false,
                loaded: true,
                activiteList,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_TYPE_ACTIVITE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_TYPE_IDENTITE:
            return {
                ...state,
                loading: true
            };
        case LOAD_TYPE_IDENTITE_SUCCESS:
            const typesIdentite = action.result.typesIdentiteList;
            return {
                ...state,
                loading: false,
                loaded: true,
                typesIdentite,
                error: null
            };
        case LOAD_TYPE_IDENTITE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOAD_TYPE_JUSTIF:
            return {
                ...state,
                loading: true
            };
        case LOAD_TYPE_JUSTIF_SUCCESS:
            const typesJustif = action.result.typeJustifList;
            return {
                ...state,
                loading: false,
                loaded: true,
                typesJustif,
                error: null
            };
        case LOAD_TYPE_JUSTIF_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };

        case LOAD_MCC:
            return {
                ...state,
                loading: true
            };
        case LOAD_MCC_SUCCESS:
            const merchantCategoryCode = action.result.mccList;
            return {
                ...state,
                loading: false,
                loaded: true,
                merchantCategoryCode,
                error: null
            };
        case LOAD_MCC_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case SAVE_SOUSCRIPTION:
            return {
                ...state,
                loading: true,
                SouscriptionSaved: null,
                data: null,
                alertState: null,
                savingError: null,
                disableState: null,

            };
        case SAVE_SOUSCRIPTION_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                SouscriptionSaved: action.result.success,
                data: action.result.demandeInstance,
                alertState: action.action,
                errorMsg: action.result.messageRetour,
                savingError: null,
                disableState: true,
            };
        case SAVE_SOUSCRIPTION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                alertState: null,
                SouscriptionSaved: null,
                data: null,
                savingError: action.codeErreur
            };
        case UPDATE_SOUSCRIPTION:
            return {
                ...state,
                loading: true,
                SouscriptionSaved: null,
                data: null,
                alertState: null,
                savingError: null,
                disableState: null,

            };
        case UPDATE_SOUSCRIPTION_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                SouscriptionSaved: action.result.success,
                data: action.result.demandeInstance,
                alertState: action.action,
                errorMsg: action.result.messageRetour,
                savingError: null,
                disableState: true,
            };
        case UPDATE_SOUSCRIPTION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                alertState: null,
                SouscriptionSaved: null,
                data: null,
                savingError: action.codeErreur
            };
        case SUBMIT_SOUSCRIPTION:
            return {
                ...state,
                loading: true,
            };
        case SUBMIT_SOUSCRIPTION_SUCCESS:
            if (action.result.success) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    isSuccess: action.result.success,
                    data: action.result.demandeInstance,
                    alertState: action.action,
                    errorMsg: action.result.messageRetour,
                    savingError: null
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    isSuccess: action.result.success,
                    errorMsg: action.result.messageRetour,
                    data: action.result.demandeInstance,
                    tentativesDepasse: action.result.tentativesDepasse
                };
            }
        case SUBMIT_SOUSCRIPTION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                errorMsg: action.result.messageRetour,
                savingError: action.codeErreur
            };
        case INSTANCE_SOUSCRIPTION:
            return {
                ...state,
                loading: true,
                isRecap: false
            };
        case INSTANCE_SOUSCRIPTION_SUCCESS: {
            if (action.result.success) {
                return {
                    ...state,
                    data: action.result.demandeInstance,
                    typePieceIdentite: action.result.demandeInstance.typePieceIdentite,
                    typeCode: action.result.demandeInstance.typeCode,
                    loading: false,
                    loaded: true,
                    isUpdate: true
                };
            } else {
                return {
                    instanceErrorMsg: action.result.messageRetour,
                    loading: false,
                    loaded: true,
                    data: null
                }
            }
        }
        case INSTANCE_SOUSCRIPTION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                dataForDetail: null,
                error: action.error
            };
        case SEND_OTP:
            return {
                ...state,
                loading: true
            };
        case SEND_OTP_SUCCESS: {
            if (action.result.success) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    otpSuccess: action.result.success,
                    errorMsg: action.result.messageRetour
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    otpSuccess: action.result.success,
                    errorMsg: action.result.messageRetour
                };
            }
        }
        case SEND_OTP_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case RESEND_OTP:
            return {
                ...state,
                loading: true
            };
        case RESEND_OTP_SUCCESS: {
            if (action.result.success) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    otpSuccess: action.result.success,
                    errorMsg: action.result.messageRetour
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    otpSuccess: action.result.success,
                    errorMsg: action.result.messageRetour,
                    isSuccess: action.result.success,
                    statut: 'Rejetée',
                    tentativesDepasse: action.result.tentativesDepasse
                };
            }
        }
        case RESEND_OTP_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };

        case LOAD_TYPE_COMMERCANT:
            return {
                ...state,
                loading: true
            };
        case LOAD_TYPE_COMMERCANT_SUCCESS:
            const typesCommercant = action.result.typesCommercant;
            return {
                ...state,
                loading: false,
                loaded: true,
                typesCommercant,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_TYPE_COMMERCANT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };

        case LOAD_CHIFFRE_AFFAIRE:
            return {
                ...state,
                loading: true
            };
        case LOAD_CHIFFRE_AFFAIRE_SUCCESS:
            const listeChiffreAffaire = action.result.listeChiffreAffaire;
            return {
                ...state,
                loading: false,
                loaded: true,
                listeChiffreAffaire,
                error: null,
                shouldReloadForm: false
            };
        case LOAD_TYPE_COMMERCANT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case RESET:
            return {
                ...state,
                SouscriptionSaved: null,
                isUpdate: true
            };
        case RELOAD:
            return {
                ...state,
                data: null,
                isRecap: false,
                isUpdate: false,
                isSuccess: null,
                errorMsg: "",
                SouscriptionSaved: null,
                typeCode: "type_one",
                typeCommercant:"personne_physique",
                isType3: false,
                isCIN: false,
                isPro: false,
                typePieceIdentite: null,
                tentativesDepasse: false,
                shouldReloadForm: true,
                statut: null
            };
        case RELOADFORM:
            return {
                ...state,
                shouldReloadForm: false,
                data: null,
                isRecap: false,
                isUpdate: false,
                isSuccess: null,
                errorMsg: "",
                SouscriptionSaved: null,
                typeCode: "type_one",
                typeCommercant:"personne_physique",
                isType3: false,
                isCIN: false,
                isPro: false,
                typePieceIdentite: null,
                tentativesDepasse: false,
                statut: null
            };
        case GENERATE_REPORT:
            return {
                ...state,
                loading: true
            };
        case GENERATE_REPORT_SUCCESS: {
            if (action.result.success) {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    errorMsg: action.result.messageRetour,
                    isSuccess: action.result.success
                };
            }
        }
        case GENERATE_REPORT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        default:
            return state;

    }
}

function objectToParams(object) {
    let str = '';
    for (const key in object) {
        if (str !== '') {
            str += '&';
        }
        str += key + '=' + encodeURIComponent(object[key]);
    }
    return str;
}

export function getSouscriptionInstance(id) {
    return {
        types: [INSTANCE_SOUSCRIPTION, INSTANCE_SOUSCRIPTION_SUCCESS, INSTANCE_SOUSCRIPTION_FAIL],
        promise: (client) => client.get('demandeSouscription/instance/' + id)
    };
}

export function saveSouscription(dto) {
    if (dto.transactionType.length > 0) {
        let list = [];
        for (let i = 0, l = dto.transactionType.length; i < l; i++) {
            list.push(dto.transactionType[i].value);
        }
        dto.transactionType = list;
    }
    return {
        types: [SAVE_SOUSCRIPTION, SAVE_SOUSCRIPTION_SUCCESS, SAVE_SOUSCRIPTION_FAIL],
        promise: (client) => client.post('demandeSouscription/save', { data: objectToParams(dto) })
    };
}

export function updateSouscription(dto, id) {
    if (dto.transactionType !== null) {
        if (dto.transactionType.length > 0) {
            let list = [];
            for (let i = 0, l = dto.transactionType.length; i < l; i++) {
                list.push(dto.transactionType[i].value);
            }
            dto.transactionType = list;
        }
    }
    return {
        types: [UPDATE_SOUSCRIPTION, UPDATE_SOUSCRIPTION_SUCCESS, UPDATE_SOUSCRIPTION_FAIL],
        promise: (client) => client.post('demandeSouscription/update/' + id, { data: objectToParams(dto) })
    };
}

export function submitSouscription(dto, values, id) {
    dto.id = id;
    dto.codeOTP = values.codeOTP;
    return {
        types: [SUBMIT_SOUSCRIPTION, SUBMIT_SOUSCRIPTION_SUCCESS, SUBMIT_SOUSCRIPTION_FAIL],
        promise: (client) => client.post('demandeSouscription/submit', { data: objectToParams(dto) })
    }

}

export function loadTypeTransaction() {
    return {
        types: [LOAD_TYPE_TRANSACTION, LOAD_TYPE_TRANSACTION_SUCCESS, LOAD_TYPE_TRANSACTION_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getTransactionTypeList')
    };
}

export function loadProfessions() {
    return {
        types: [LOAD_PROFESSION, LOAD_PROFESSION_SUCCESS, LOAD_PROFESSION_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getProfessionList')
    };
}

export function loadObjetRelation() {
    return {
        types: [LOAD_OBJET_RELATION, LOAD_OBJET_RELATION_SUCCESS, LOAD_OBJET_RELATION_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getObjetRelationList')
    };
}

export function loadFondOrigins() {
    return {
        types: [LOAD_FOND_ORIGIN, LOAD_FOND_ORIGIN_SUCCESS, LOAD_FOND_ORIGIN_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getFondsOrigineList')
    };
}

export function loadTranches () {
    return {
        types: [LOAD_TRANCHES, LOAD_TRANCHES_SUCCESS, LOAD_TRANCHES_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getTranchesRevenusList')
    };
}

export function loadTitles() {
    return {
        types: [LOAD_TITLE, LOAD_TITLE_SUCCESS, LOAD_TITLE_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getTitles')
    };
}

export function loadPays() {
    return {
        types: [LOAD_PAYS, LOAD_PAYS_SUCCESS, LOAD_PAYS_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getPays')
    };
}

export function loadTypePack() {
    return {
        types: [LOAD_TYPE_PACK, LOAD_TYPE_PACK_SUCCESS, LOAD_TYPE_PACK_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getPackTypes')
    };
}

export function loadTypeCommercant() {
    return {
        types: [LOAD_TYPE_COMMERCANT, LOAD_TYPE_COMMERCANT_SUCCESS, LOAD_TYPE_COMMERCANT_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getTypesCommercant')
    };
}

export function loadTypePackAttachments() {
    return {
        types: [LOAD_PACK_ATTACHEMENTS, LOAD_PACK_ATTACHEMENTS_SUCCESS, LOAD_PACK_ATTACHEMENTS_FAIL],
        promise: (client) => client.get('demandeSouscription/getAttachementByPackParametres')
    };
}

export function loadActivite() {
    return {
        types: [LOAD_TYPE_ACTIVITE, LOAD_TYPE_ACTIVITE_SUCCESS, LOAD_TYPE_ACTIVITE_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getActiviteClient')
    };
}

export function loadTypeIdentite() {
    return {
        types: [LOAD_TYPE_IDENTITE, LOAD_TYPE_IDENTITE_SUCCESS, LOAD_TYPE_IDENTITE_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getIdentityTypes')
    };
}

export function isSironActivated() {
    return {
        types: [LOAD_IS_SIRON, LOAD_IS_SIRON_SUCCESS, LOAD_IS_SIRON_FAIL],
        promise: (client) => client.get('demandeSouscription/isSironActivated')
    };
}

export function loadTypeResidence() {
    return {
        types: [LOAD_TYPE_JUSTIF, LOAD_TYPE_JUSTIF_SUCCESS, LOAD_TYPE_JUSTIF_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getTypeJustif')
    };
}

export function loadMcc() {
    return {
        types: [LOAD_MCC, LOAD_MCC_SUCCESS, LOAD_MCC_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getMcc')
    };
}

export function resendOtp(id, gsm) {
    let obj = { id: id, telephone: gsm };
    return {
        types: [RESEND_OTP, RESEND_OTP_SUCCESS, RESEND_OTP_FAIL],
        promise: (client) => client.post('client/resendOtp?type=S', { data: objectToParams(obj) })
    };
}

export function generateReport(data) {
    return {
        types: [GENERATE_REPORT, GENERATE_REPORT_SUCCESS, GENERATE_REPORT_FAIL],
        promise: (client) => client.get('demandeSouscription/createContratAbonnementPDF')
    }
}

export function sendOtp(id, gsm) {
    let obj = { id: id, telephone: gsm };
    return {
        types: [SEND_OTP, SEND_OTP_SUCCESS, SEND_OTP_FAIL],
        promise: (client) => client.post('client/sendOtp?type=S', { data: objectToParams(obj) })
    };
}

export function loadActiviteCommercant() {
    return {
        types: [LOAD_TYPE_ACTIVITE, LOAD_TYPE_ACTIVITE_SUCCESS, LOAD_TYPE_ACTIVITE_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getActiviteCommercant')
    };
}

export function loadObjetRelationCommercant() {
    return {
        types: [LOAD_OBJET_RELATION, LOAD_OBJET_RELATION_SUCCESS, LOAD_OBJET_RELATION_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getObjetRelationCommercant')
    };
}

export function getChiffreAffaireCommercant() {
    return {
        types: [LOAD_CHIFFRE_AFFAIRE, LOAD_CHIFFRE_AFFAIRE_SUCCESS, LOAD_CHIFFRE_AFFAIRE_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getChiffreAffaireCommercant')
    };
}

export function reset() {
    return {
        type: RESET,
    }

}

export function reload() {
    return {
        type: RELOAD,
    }
}

export function resetShouldReloadForm() {
    return {
        type: RELOADFORM,
    }
}
