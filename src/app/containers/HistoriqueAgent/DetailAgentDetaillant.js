import React, { Component } from "react";
import { translate } from "react-i18next";
import { connect } from "react-redux";
import { initializeWithKey, reduxForm, reset } from "redux-form";
import {
  Row,
  Col,
  Modal,
  ControlLabel,
  PageHeader,
  Button,
  FormControl,
} from "react-bootstrap";
import { asyncConnect } from "redux-connect";
import Attachments from "../../components/Attachement/Attachments";
import * as AttachementActions from "../../components/Attachement/AttachementReducer";
import * as HistoriqueAgentsAction from "./HistoriqueAgentDetaillantReducer";
import DatePicker from "react-datepicker";
import moment from "moment";
import { browserHistory } from "react-router";
import { AuthorizedComponent } from "react-router-role-authorization";

export const validate = (values, props) => {
  const errors = {};

  if (!values.lastName || !values.lastName.trim()) {
    errors.lastName = props.t("form.label.champObligatoire");
  } else if (values.lastName.length > 20) {
    errors.lastName = props.t("form.label.errNOM");
  } else if (!/^[a-zA-Z_][a-zA-Z_ ]*[a-zA-Z_]$/i.test(values.lastName.trim())) {
    errors.lastName = props.t("form.label.saisieIncorrect");
  }
  if (!values.firstName || !values.firstName.trim()) {
    errors.firstName = props.t("form.label.champObligatoire");
  } else if (values.firstName.length > 20) {
    errors.firstName = props.t("form.label.errPrenom");
  } else if (
    !/^[a-zA-Z_][a-zA-Z_ ]*[a-zA-Z_]$/i.test(values.firstName.trim())
  ) {
    errors.firstName = props.t("form.label.saisieIncorrect");
  }
  if (!values.telephone || !values.telephone.trim()) {
    errors.telephone = props.t("form.label.champObligatoire");
  } else if (
    !/^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.telephone) &&
    values.telephone !== ""
  ) {
    errors.telephone = props.t("form.label.errTel");
  }
  /*if (!values.mail || !(values.mail.trim())) {
        errors.mail = 'Ce champ est obligatoire';
    }
    if (values.mail && values.mail.trim()) {
        if (!/^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i.test(values.mail) && values.mail !== '') {
            errors.mail = 'Merci de saisir une adresse e-mail valide';
        }
    }*/
  if (values.mail && values.mail.length > 40) {
    errors.mail = props.t("form.label.emailLong");
  }
  if (!values.adresse || !values.adresse.trim()) {
    errors.adresse = props.t("form.label.champObligatoire");
  } else if (values.adresse.length > 80) {
    errors.adresse = props.t("form.label.adresseLong");
  }
  if (
    !values.typePieceIdentite ||
    !values.typePieceIdentite.trim() ||
    values.typePieceIdentite === "Choisir le type de votre pièce d'identité  "
  ) {
    errors.typePieceIdentite = props.t("form.label.champObligatoire");
  }
  if (!values.numeroIdentite || !values.numeroIdentite.trim()) {
    errors.numeroIdentite = props.t("form.label.champObligatoire");
  } else {
    if (
      !/^[A-Z]{1,2}[1-9]{1}[0-9]{4,7}$/i.test(values.numeroIdentite) &&
      values.numeroIdentite !== ""
    ) {
      errors.numeroIdentite = props.t("form.label.formatInvalid");
    }
    if (values.numeroIdentite && values.numeroIdentite.length > 9) {
      errors.numeroIdentite = props.t("form.label.champObligatoire");
    }
  }
  if (!values.dateNaissance || !values.dateNaissance.trim()) {
    errors.dateNaissance = props.t("form.label.champObligatoire");
  } else if (
    moment(values.dateNaissance).format("DD/MM/YYYY") >
    moment().subtract(16, "years")
  ) {
    errors.dateNaissance = props.t("form.label.dateInvalid");
  }
  if (values.type !== "P_SegmentAgent") {
    if (!values.nomSocial || !values.nomSocial.trim()) {
      errors.nomSocial = props.t("form.label.champObligatoire");
    }
    if (!values.formeJuridique || !values.formeJuridique.trim()) {
      errors.formeJuridique = props.t("form.label.champObligatoire");
    }
    if (!values.ice || !values.ice.trim()) {
      errors.ice = props.t("form.label.champObligatoire");
    } else if (!/^[0-9]*$/i.test(values.ice.trim())) {
      errors.ice = props.t("form.label.saisieIncorrect");
    }
  }

  if (!values.rc || !values.rc.trim()) {
    errors.rc = props.t("form.label.champObligatoire");
  } else if (!/^[0-9]*$/i.test(values.rc.trim())) {
    errors.rc = props.t("form.label.saisieIncorrect");
  }

  if (!values.numPatente || !values.numPatente.trim()) {
    errors.numPatente = props.t("form.label.champObligatoire");
  } else if (!/^[0-9]*$/i.test(values.numPatente.trim())) {
    errors.numPatente = props.t("form.label.saisieIncorrect");
  }

  if (
    values.ribAgent &&
    values.ribAgent.trim() &&
    !/^[0-9]*$/i.test(values.ribAgent.trim())
  ) {
    errors.ribAgent = props.t("form.label.saisieIncorrect");
  } else if (
    values.ribAgent &&
    values.ribAgent.trim() &&
    values.ribAgent.length !== 24
  ) {
    errors.ribAgent = props.t("form.label.saisieIncorrect");
  }

  return errors;
};

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      return Promise.all([
        dispatch(HistoriqueAgentsAction.loadAgent(params.id)),
        dispatch(AttachementActions.getDocumentsAgent(params.id)),
        dispatch(HistoriqueAgentsAction.getListMotif()),
      ]);
    },
  },
])
@translate(["historique"], { wait: true })
@reduxForm(
  {
    form: "UpdateAgentForm",
    fields: [
      "adresse",
      "type",
      "nomSocial",
      "ice",
      "formeJuridique",
      "lastName",
      "firstName",
      "dateNaissance",
      "mail",
      "telephone",
      "numeroIdentite",
      "typePieceIdentite",
      "numPatente",
      "rc",
      "ribAgent",
    ],
    validate,
  },
  (state) =>
    state.HistoriqueAgentDetaillantReducer.agent
      ? {
          initialValues: {
            type: state.HistoriqueAgentDetaillantReducer.agent.type,
            lastName: state.HistoriqueAgentDetaillantReducer.agent.lastName,
            firstName: state.HistoriqueAgentDetaillantReducer.agent.firstName,
            mail: state.HistoriqueAgentDetaillantReducer.agent.mail,
            telephone: state.HistoriqueAgentDetaillantReducer.agent.telephone,
            adresse: state.HistoriqueAgentDetaillantReducer.agent.adresse,
            dateNaissance:
              state.HistoriqueAgentDetaillantReducer.agent.dateNaissance,
            numeroIdentite:
              state.HistoriqueAgentDetaillantReducer.agent.numeroIdentite,
            typePieceIdentite:
              state.HistoriqueAgentDetaillantReducer.agent.typePieceIdentite,
            fichiersjoints: state.AttachementReducer.loadedDocuments,
            nomSocial: state.HistoriqueAgentDetaillantReducer.agent.nomSocial,
            ice: state.HistoriqueAgentDetaillantReducer.agent.ice,
            rc: state.HistoriqueAgentDetaillantReducer.agent.rc,
            numPatente: state.HistoriqueAgentDetaillantReducer.agent.numPatente,
            formeJuridique:
              state.HistoriqueAgentDetaillantReducer.agent.formeJuridique,
            ribAgent: state.HistoriqueAgentDetaillantReducer.agent.ribAgent,
          },
        }
      : {
          initialValues: {
            type: "",
            lastName: "",
            firstName: "",
            mail: "",
            telephone: "",
            adresse: "",
            dateNaissance: "",
            numeroIdentite: "",
            typePieceIdentite: "",
            nomSocial: "",
            ice: "",
            numPatente: "",
            rc: "",
            formeJuridique: "",
            rib: "",
          },
        }
)
@connect(
  (state) => ({
    agent: state.HistoriqueAgentDetaillantReducer.agent,
    isSuccess: state.HistoriqueAgentDetaillantReducer.isSuccess,
    loading: state.HistoriqueAgentDetaillantReducer.loading,
    loadedDocuments: state.AttachementReducer.loadedDocuments,
    messageRetour: state.HistoriqueAgentDetaillantReducer.messageRetour,
    userFrontDetails: state.user.userFrontDetails,
    listMotif: state.HistoriqueAgentDetaillantReducer.listMotif,
  }),
  { ...HistoriqueAgentsAction, ...AttachementActions, initializeWithKey }
)
export default class DetailAgent extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
    this.getInitialState();
    const _self = this;
    this.filesClickEvents = [_self.close, _self.deleteDoc];
    this.filesClickEvent = [];
  }

  getInitialState() {
    this.state = {
      fichiersjoints: null,
      isRecap: true,
      type: this.props.agent.type,
      isPhysique: this.props.agent.type === "P_SegmentAgent",
    };
  }

  handleFiles = (files) => {
    this.setState({
      fichiersjoints: files,
    });
  };

  handleValidate = async (dto) => {
    await this.props.valideCreation(dto);
    this.setState({ isSuccess: this.props.isSuccess });
  };

  handleRejeter = async (dto) => {
    if (!this.state.motif) {
      this.setState({ error: "Ce champ est obligatoire" });
    } else {
      this.setState({ error: "" });
      dto.motif = this.state.motif;
      await this.props.rejeterCreation(dto);
      this.setState({ isSuccess: this.props.isSuccess });
    }
  };

  sendSmsAndMail = async (dto) => {
    await this.props.sendSmsAndMail(dto);
    this.setState({ isSuccess: this.props.isSuccess });
  };

  handleChangeTypeAgent = (event) => {
    this.setState({
      type: event.target.value,
      isPhysique: !this.state.isPhysique,
    });
  };

  handleChangePiece = (event) => {
    this.setState({ typePieceIdentite: event.target.value });
  };

  handleChangeDateNaissance = (date) => {
    this.setState({ dateNaissance: date });
  };

  handleChangeMotif = (event) => {
    this.setState({
      motif: event.target.value,
      error: "",
    });
  };

  handleUpdate = () => {
    this.setState({
      isRecap: false,
      isSuccess: null,
    });
  };

  handleChangeRib = (event) => {
    this.setState({
      ribAgent: event.target.value,
    });
  };

  update = async (values) => {
    values.id = this.props.agent.id;
    if (values.type === "P_SegmentAgent") {
      values.nomSocial = null;
      values.ice = null;
      values.formeJuridique = null;
    }
    await this.props.update(values);
    if (this.props.isSuccess) {
      this.setState({
        isRecap: true,
        isSuccess: true,
      });
    }
  };

  handleAnnuler = () => {
    this.props.dispatch(reset("UpdateAgentForm"));
    this.setState({
      isRecap: true,
      isSuccess: null,
      type: this.props.agent.type,
      isPhysique: this.props.agent.type === "P_SegmentAgent",
    });
  };

  render() {
    const {
      t,
      loading,
      loadedDocuments,
      agent,
      messageRetour,
      userFrontDetails,
      values,
      handleSubmit,
      listMotif,
      fields: {
        nomSocial,
        ice,
        formeJuridique,
        type,
        rc,
        numPatente,
        dateNaissance,
        adresse,
        lastName,
        firstName,
        mail,
        telephone,
        numeroIdentite,
        typePieceIdentite,
        ribAgent,
      },
    } = this.props;

    const styles = require("./HistoriqueAgent.scss");

    const validationAgent =
      userFrontDetails.roles.indexOf("ROLE_EDP_VALIDATION_AGENT") !== -1;

    return (
      <div>
        <Modal
          show={loading}
          className="loadingModal"
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>
            <Row>
              <Col xs={12} md={12}>
                <div className="spinner">
                  <span style={{ fontSize: "11px" }}>
                    Chargement en cours ...
                  </span>
                </div>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>
        <div>
          <PageHeader>
            <h3>{t("agent.agentDetaillant")}</h3>
          </PageHeader>

          {this.state.isSuccess === true && (
            <div className="alert alert-success">{messageRetour}</div>
          )}

          {this.state.isSuccess === false && (
            <div className={"alert alert-danger"}>{messageRetour}</div>
          )}

          {this.state.isRecap === true ? (
            <form className="formContainer">
              <fieldset style={{ marginTop: "20px" }}>
                <legend>{t("agent.inforation")}</legend>
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("agent.identifiantAgent")}</ControlLabel>
                    <p className="detail-value">{agent.userName}</p>
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("agent.typeAgent")}</ControlLabel>
                    <p className="detail-value">
                      {agent.type === "P_SegmentAgent" &&
                        t("form.label.personnePhysique")}
                      {agent.type === "M_SegmentAgent" &&
                        t("form.label.personneMorale")}
                    </p>
                  </Col>
                </Row>
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("list.cols.nom")} </ControlLabel>
                    <p className="detail-value">{agent.lastName}</p>
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("list.cols.prenom")} </ControlLabel>
                    <p className="detail-value">{agent.firstName}</p>
                  </Col>
                </Row>

                {agent.type === "M_SegmentAgent" && (
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("list.cols.nomSociale")}</ControlLabel>
                      <p className="detail-value">{agent.nomSocial}</p>
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("list.cols.formJuridique")}
                      </ControlLabel>
                      <p className="detail-value">{agent.formeJuridique}</p>
                    </Col>
                  </Row>
                )}

                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("list.cols.numeroTelephone")}
                    </ControlLabel>
                    <p className="detail-value">{agent.telephone}</p>
                  </Col>

                  <Col xs="12" md="6">
                    <ControlLabel>{t("list.cols.email")}</ControlLabel>
                    <p className="detail-value">{agent.mail}</p>
                  </Col>
                </Row>

                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("form.label.typepieceIdentite")}
                    </ControlLabel>
                    {agent.typePieceIdentite === "MAR" && (
                      <p className="detail-value">{t("form.label.cin1")}</p>
                    )}
                    {agent.typePieceIdentite === "CSJ" && (
                      <p className="detail-value">
                        {t("form.label.carteSejour")}
                      </p>
                    )}
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("form.label.numeropieceIdentite")}
                    </ControlLabel>
                    <p className="detail-value">{agent.numeroIdentite}</p>
                  </Col>
                </Row>

                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("list.cols.dateNaissance")}</ControlLabel>
                    <p className="detail-value">{agent.dateNaissance}</p>
                  </Col>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("list.cols.adresse")}</ControlLabel>
                    <p className="detail-value">{agent.adresse}</p>
                  </Col>
                </Row>

                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("list.cols.numeroimmatriculation")}
                    </ControlLabel>
                    <p className="detail-value">{agent.rc}</p>
                  </Col>

                  <Col xs="12" md="6">
                    <ControlLabel>{t("list.cols.numeropatente")}</ControlLabel>
                    <p className="detail-value">{agent.numPatente}</p>
                  </Col>
                </Row>

                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>{t("list.cols.statut")}</ControlLabel>
                    <p className="detail-value">
                      {agent.statut === "ENABLED" && t("form.label.signe")}
                      {agent.statut === "ENCOURS_ACTIVATION" &&
                        t("form.label.enCours")}
                      {agent.statut === "DISABLED" && t("form.label.rejete")}
                    </p>
                  </Col>

                  {agent.type === "M_SegmentAgent" && (
                    <Col xs="12" md="6">
                      <ControlLabel>{t("list.cols.numeroICE")}</ControlLabel>
                      <p className="detail-value">{agent.ice}</p>
                    </Col>
                  )}
                </Row>

                <Row className={styles.fieldRow}>
                  {agent && agent.statut === "DISABLED" && (
                    <Col xs="12" md="6">
                      <ControlLabel>{t("list.cols.motifRejete")}</ControlLabel>
                      <p className="detail-value">{agent.motif}</p>
                    </Col>
                  )}

                  {agent && agent.ribAgent && agent.ribAgent !== "" && (
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.rib")}</ControlLabel>
                      <p className="detail-value">{agent.ribAgent}</p>
                    </Col>
                  )}
                </Row>
              </fieldset>

              {/*<fieldset style={{marginTop: '20px'}}>*/}
              {/*    <div>*/}
              {/*        <legend>Pièces jointes</legend>*/}
              {/*        <DownloadedFiles files={loadedDocuments}*/}
              {/*                         filesClickEvent={this.filesClickEvent}*/}
              {/*                         handleFiles={this.handleFiles}*/}
              {/*                         isConsultOnly={true}/>*/}
              {/*    </div>*/}
              {/*</fieldset>*/}

              {validationAgent &&
                agent &&
                agent.statut === "ENCOURS_ACTIVATION" && (
                  <fieldset style={{ marginTop: "30px" }}>
                    <Row>
                      <Col xs="12" md="6">
                        <legend>{t("form.label.motif1.title")}</legend>
                        <ControlLabel>
                          {t("form.label.motif1.selection")}
                        </ControlLabel>
                        <FormControl
                          componentClass="select"
                          onChange={this.handleChangeMotif}
                          value={this.state.motif ? this.state.motif : ""}
                          className={styles.datePickerFormControl}
                        >
                          <option hidden />
                          {listMotif &&
                            listMotif.length &&
                            listMotif.map((element) => (
                              <option key={element.id} value={element.code}>
                                {element.libelle}
                              </option>
                            ))}
                        </FormControl>
                        {this.state.error && this.state.error !== "" && (
                          <div>
                            <i
                              className="fa fa-exclamation-triangle"
                              aria-hidden="true"
                            >
                              {this.state.error}
                            </i>
                          </div>
                        )}
                      </Col>
                    </Row>
                  </fieldset>
                )}

              <Row className={styles.fieldRow}>
                <Col className="pull-right" style={{ paddingTop: "10px" }}>
                  <Button
                    bsStyle="primary"
                    onClick={() => {
                      browserHistory.push(baseUrl + "app/HistoriqueAgent");
                    }}
                  >
                    <i className="fa fa-check " />
                    {t("form.buttons.cancel")}
                  </Button>
                  {validationAgent &&
                    agent &&
                    agent.statut === "ENCOURS_ACTIVATION" && (
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          this.handleRejeter(agent);
                        }}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.reject")}
                      </Button>
                    )}
                  {validationAgent &&
                    agent &&
                    agent.statut === "ENCOURS_ACTIVATION" && (
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          this.handleValidate(agent);
                        }}
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.valider")}
                      </Button>
                    )}
                  {agent && agent.statut !== "DISABLED" && (
                    <Button
                      bsStyle="primary"
                      onClick={() => {
                        this.handleUpdate();
                      }}
                    >
                      <i className="fa fa-check " />
                      {t("form.buttons.update")}
                    </Button>
                  )}
                </Col>
              </Row>

              {userFrontDetails &&
                userFrontDetails.codeBanque === "00000" &&
                agent.statut === "ENABLED" && (
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        bsStyle="primary"
                        onClick={() => {
                          this.sendSmsAndMail(agent);
                        }}
                      >
                        <i className="fa fa-check " />
                        {t("form.label.envoyerSMSetMail")}
                      </Button>
                    </div>
                  </Row>
                )}
            </form>
          ) : (
            <div>
              <Row>
                <form className="formContainer" onSubmit={handleSubmit}>
                  <Row className={styles.fieldRow}>
                    <Col xs="12" md="12">
                      <ControlLabel>{t("agent.typeAgent")}</ControlLabel>
                      <FormControl
                        componentClass="select"
                        {...type}
                        className={styles.datePickerFormControl}
                        onChange={this.handleChangeTypeAgent}
                        value={
                          this.state.type ? this.state.type : "P_SegmentAgent"
                        }
                      >
                        <option key="P_SegmentAgent" value="P_SegmentAgent">
                          {t("form.label.personnePhysique")}
                        </option>
                        <option key="M_SegmentAgent" value="M_SegmentAgent">
                          {t("form.label.personneMorale")}
                        </option>
                      </FormControl>
                    </Col>
                  </Row>
                  <fieldset>
                    <legend>{t("form.label.informationsPersonnelles")}</legend>
                    <Row className={styles.fieldRow}>
                      {!this.state.isPhysique && (
                        <div>
                          <Col xs="12" md="6">
                            <ControlLabel>
                              {t("form.label.denominationSociale")}
                            </ControlLabel>
                            <FormControl
                              type="text"
                              {...nomSocial}
                              className={styles.datePickerFormControl}
                            />
                            {nomSocial.touched && nomSocial.error && (
                              <div className={styles.error}>
                                <i className="fa fa-exclamation-triangle" />{" "}
                                {nomSocial.error}
                              </div>
                            )}
                          </Col>
                          <Col xs="12" md="6">
                            <ControlLabel>
                              {t("list.cols.formJuridique")}
                            </ControlLabel>
                            <FormControl
                              type="text"
                              {...formeJuridique}
                              className={styles.datePickerFormControl}
                              placeholder="SA, SARL, Autre"
                            />
                            {formeJuridique.touched && formeJuridique.error && (
                              <div className={styles.error}>
                                <i className="fa fa-exclamation-triangle" />{" "}
                                {formeJuridique.error}
                              </div>
                            )}
                          </Col>
                        </div>
                      )}
                    </Row>
                    <Row className={styles.fieldRow}>
                      <Col xs="12" md="6">
                        <ControlLabel>{t("list.cols.nom1")}</ControlLabel>
                        <FormControl
                          type="text"
                          {...lastName}
                          className={styles.datePickerFormControl}
                          placeholder={
                            !this.state.isPhysique ? "dirigeant" : ""
                          }
                        />
                        {lastName.touched && lastName.error && (
                          <div className={styles.error}>
                            <i className="fa fa-exclamation-triangle" />{" "}
                            {lastName.error}
                          </div>
                        )}
                      </Col>
                      <Col xs="12" md="6">
                        <ControlLabel>{t("list.cols.prenom1")}</ControlLabel>
                        <FormControl
                          type="text"
                          {...firstName}
                          className={styles.datePickerFormControl}
                          placeholder={
                            !this.state.isPhysique ? "dirigeant" : ""
                          }
                        />
                        {firstName.touched && firstName.error && (
                          <div className={styles.error}>
                            <i className="fa fa-exclamation-triangle" />{" "}
                            {firstName.error}
                          </div>
                        )}
                      </Col>
                    </Row>
                    <Row className={styles.fieldRow}>
                      <Col xs="12" md="6">
                        <ControlLabel>
                          {t("form.label.pieceIdentite")}
                        </ControlLabel>
                        <FormControl
                          componentClass="select"
                          {...typePieceIdentite}
                          onChange={this.handleChangePiece}
                          value={
                            this.state.typePieceIdentite
                              ? this.state.typePieceIdentite
                              : "MAR"
                          }
                          placeholder={t("form.label.account")}
                          className={styles.remDonumeroIdentiteput}
                        >
                          <option key="MAR" value="MAR">
                            {t("form.label.cin1")}
                          </option>
                          <option key="CSJ" value="CSJ">
                            {t("form.label.carteSejour")}
                          </option>
                        </FormControl>
                        {typePieceIdentite.touched && typePieceIdentite.error && (
                          <div className={styles.error}>
                            <i className="fa fa-exclamation-triangle" />{" "}
                            {typePieceIdentite.error}
                          </div>
                        )}
                      </Col>
                      <Col xs="12" md="6">
                        <ControlLabel>
                          {t("form.label.npieceIdentite")}
                        </ControlLabel>
                        <FormControl
                          type="text"
                          {...numeroIdentite}
                          className={styles.datePickerFormControl}
                          placeholder={
                            !this.state.isPhysique ? "dirigeant" : ""
                          }
                        />
                        {numeroIdentite.touched && numeroIdentite.error && (
                          <div className={styles.error}>
                            <i className="fa fa-exclamation-triangle" />{" "}
                            {numeroIdentite.error}
                          </div>
                        )}
                      </Col>
                    </Row>
                    <Row className={styles.fieldRow}>
                      <Col xs="12" md="6">
                        <ControlLabel>
                          {t("list.cols.dateNaissance")}
                        </ControlLabel>
                        <span {...dateNaissance}>
                          <DatePicker
                            maxDate={moment().subtract(16, "years")}
                            selected={this.state.dateNaissance}
                            dateNaissance={this.state.dateNaissance}
                            placeholderText={dateNaissance.value}
                            onChange={this.handleChangeDateNaissance}
                            className={styles.datePickerFormControl}
                            isClearable="true"
                            locale="fr-FR"
                            dateFormat="DD/MM/YYYY"
                          />
                        </span>
                        {dateNaissance.error && dateNaissance.touched && (
                          <div className={styles.error}>
                            <i
                              className="fa fa-exclamation-triangle"
                              aria-hidden="true"
                            >
                              {dateNaissance.error}
                            </i>
                          </div>
                        )}
                      </Col>
                      <Col xs="12" md="6">
                        <ControlLabel>{t("list.cols.adresse")}</ControlLabel>
                        <FormControl
                          type="text"
                          {...adresse}
                          className={styles.datePickerFormControl}
                          placeholder={
                            !this.state.isPhysique ? "Point de vente" : ""
                          }
                        />
                        {adresse.touched && adresse.error && (
                          <div className={styles.error}>
                            <i className="fa fa-exclamation-triangle" />{" "}
                            {adresse.error}
                          </div>
                        )}
                      </Col>
                    </Row>
                    <Row className={styles.fieldRow}>
                      <Col xs="12" md="6">
                        <ControlLabel>{t("list.cols.email1")}</ControlLabel>
                        <FormControl
                          type="text"
                          {...mail}
                          className={styles.datePickerFormControl}
                        />
                        {mail.touched && mail.error && (
                          <div className={styles.error}>
                            <i className="fa fa-exclamation-triangle" />{" "}
                            {mail.error}
                          </div>
                        )}
                      </Col>
                      <Col xs="12" md="6">
                        <ControlLabel>
                          {t("list.cols.numeroTelephone")}
                        </ControlLabel>
                        <FormControl
                          type="text"
                          {...telephone}
                          className={styles.datePickerFormControl}
                          placeholder={
                            !this.state.isPhysique ? "du gérant" : ""
                          }
                          disabled={true}
                        />
                        {telephone.touched && telephone.error && (
                          <div className={styles.error}>
                            <i className="fa fa-exclamation-triangle" />{" "}
                            {telephone.error}
                          </div>
                        )}
                      </Col>
                    </Row>
                    <Row className={styles.fieldRow}>
                      <Col xs="12" md="6">
                        <ControlLabel>
                          {t("list.cols.numeroimmatriculation")}
                        </ControlLabel>
                        <FormControl
                          type="text"
                          {...rc}
                          className={styles.datePickerFormControl}
                        />
                        {rc.touched && rc.error && (
                          <div className={styles.error}>
                            <i className="fa fa-exclamation-triangle" />{" "}
                            {rc.error}
                          </div>
                        )}
                      </Col>
                      <Col xs="12" md="6">
                        <ControlLabel>
                          {t("list.cols.numeropatente")}
                        </ControlLabel>
                        <FormControl
                          type="text"
                          {...numPatente}
                          className={styles.datePickerFormControl}
                        />
                        {numPatente.touched && numPatente.error && (
                          <div className={styles.error}>
                            <i className="fa fa-exclamation-triangle" />{" "}
                            {numPatente.error}
                          </div>
                        )}
                      </Col>
                    </Row>

                    <Row className={styles.fieldRow}>
                      {!this.state.isPhysique && (
                        <div>
                          <Col xs="12" md="6">
                            <ControlLabel>
                              {t("list.cols.numeroICE")}
                            </ControlLabel>
                            <FormControl
                              type="text"
                              {...ice}
                              className={styles.datePickerFormControl}
                            />
                            {ice.touched && ice.error && (
                              <div className={styles.error}>
                                <i className="fa fa-exclamation-triangle" />{" "}
                                {ice.error}
                              </div>
                            )}
                          </Col>
                        </div>
                      )}
                      {bankCode === "00861" &&
                        agent &&
                        agent.ribAgent &&
                        agent.ribAgent !== "" && (
                          <div>
                            <Col xs="12" md="6">
                              <ControlLabel>
                                {t("form.label.rib")}{" "}
                              </ControlLabel>
                              <FormControl
                                type="text"
                                {...ribAgent}
                                className={styles.datePickerFormControl}
                                disabled={true}
                              />
                              {ribAgent.touched && ribAgent.error && (
                                <div className={styles.error}>
                                  <i className="fa fa-exclamation-triangle" />{" "}
                                  {ribAgent.error}
                                </div>
                              )}
                            </Col>
                          </div>
                        )}
                    </Row>
                    <Row className={styles.paddingColumn}>
                      <div
                        className="pull-right"
                        style={{ paddingTop: "10px" }}
                      >
                        <Button
                          bsStyle="primary"
                          onClick={() => {
                            this.handleAnnuler();
                          }}
                        >
                          <i className="fa fa-check " />
                          {t("form.buttons.cancel")}
                        </Button>
                        <Button
                          bsStyle="primary"
                          onClick={handleSubmit(() => {
                            this.update(values);
                          })}
                        >
                          <i className="fa fa-check " />
                          {t("form.buttons.record")}
                        </Button>
                      </div>
                    </Row>
                  </fieldset>
                </form>
              </Row>
            </div>
          )}
        </div>
      </div>
    );
  }
}

class DownloadedFiles extends Component {
  render() {
    const {
      t,
      handleSelect,
      open,
      files,
      isConsultOnly,
      formState,
      filesClickEvents,
      filesClickEvent,
      handleFiles,
      disableALL,
      styles,
      fichiersjoints,
      loadedDocuments,
      loadedContract,
    } = this.props;
    return (
      <fieldset>
        <Row>
          <Col xs={12} sm={12} md={6}>
            {!isConsultOnly /*File input that allows to list, add and delete files provided from the previous actions on this transaction*/ && (
              <Attachments
                title={"ajouter fichiers"}
                handleFiles={handleFiles}
                files={files}
                filesClickEvents={false}
                styles={styles}
              />
            )}
            {isConsultOnly /*File input that allows to list, add and delete files provided from the previous actions on this transaction*/ && (
              <Attachments
                title={"Télécharger les fichiers"}
                files={files}
                filesClickEvents={true}
              />
            )}
          </Col>
        </Row>
      </fieldset>
    );
  }
}
