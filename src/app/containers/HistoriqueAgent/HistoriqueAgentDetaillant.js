import React, { Component } from "react";
import {
  Button,
  Col,
  FormControl,
  ControlLabel,
  Modal,
  Row,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";
import { initializeWithKey, reduxForm } from "redux-form";
import { browserHistory } from "react-router";
import { translate } from "react-i18next";
import Griddle from "griddle-react";
import { connect } from "react-redux";
import { asyncConnect } from "redux-connect";
import { reset } from "redux-form";

import * as HistoriqueAgentAction from "./HistoriqueAgentDetaillantReducer";
import HistoriqueAgentPagination from "./HistoriqueAgentDetaillantPagination";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

class ActionHeaderComponent extends Component {
  render() {
    return <div className="text-center">{this.props.displayName}</div>;
  }
}

@translate(["upgrade"])
class CenterComponent extends Component {
  render() {
    const { t } = this.props;
    if (this.props.data === "ENABLED")
      return <div>{t("form.label.signe")}</div>;
    if (this.props.data === "ENCOURS_ACTIVATION")
      return <div>{t("form.label.enCours")}</div>;
    if (this.props.data === "DISABLED")
      return <div>{t("form.label.rejete")}</div>;
    if (this.props.data === "SUSPENDU")
      return <div>{t("form.label.suspendu")}</div>;
    else return <div>{this.props.data}</div>;
  }
}

@translate(["common"])
class NoDataComponent extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="noDataResult">
        <h4>{t("errors.noResult")}</h4>
      </div>
    );
  }
}

@connect((state) => ({}), { ...HistoriqueAgentAction, initializeWithKey })
@translate(["upgrade"])
class ActionComponant extends Component {
  render() {
    const { t, globalParams, rowData } = this.props;

    const ttipSelect = (
      <Tooltip id="tooltip" className="ttip">
        {t("form.label.visionner")}
      </Tooltip>
    );

    return (
      <div style={{ textAlign: "center" }}>
        {
          <OverlayTrigger placement="top" overlay={ttipSelect}>
            <Button
              className="iconbtn"
              onClick={() =>
                browserHistory.push({
                  pathname:
                    baseUrl +
                    "app/HistoriqueAgentDetailsDetaillant/" +
                    rowData.id,
                })
              }
            >
              <i className="f fa fa-eye" />
            </Button>
          </OverlayTrigger>
        }
      </div>
    );
  }
}

export const validate = (values, props) => {
  const errors = {};
  if (!/^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) && values.gsm !== "") {
    errors.gsm = props.t("gsm.msgErr");
  }
  return errors;
};

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      return Promise.all([dispatch(reset("AgentSearchForm"))]);
    },
  },
])
@reduxForm({
  form: "AgentSearchForm",
  fields: ["gsm", "username"],
  validate,
  initialValues: {
    gsm: "",
    username: "",
  },
  destroyOnUnmount: true,
})
@connect(
  (state) => ({
    agentList: state.HistoriqueAgentDetaillantReducer.agentList,
    loading: state.HistoriqueAgentDetaillantReducer.loading,
    search: state.HistoriqueAgentDetaillantReducer.search,
  }),
  { ...HistoriqueAgentAction, initializeWithKey }
)
@translate(["historique"])
export default class HistoriqueAgentDetaillant extends Component {
  constructor() {
    super();
    this.handleReset = this.handleReset.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.state = {
      typeAgent: "AD",
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.search !== nextProps.search && nextProps.search === "") {
      this.props.dispatch(reset("AgentSearchForm"));
    }
  }

  handleReset() {
    this.props.loadAgentsList(1, "", "", this.state.typeAgent);
    this.props.dispatch(reset("AgentSearchForm"));
  }

  handleSearch(values) {
    let search = "";
    if (values.gsm.trim() !== "") {
      search = values.gsm.substring(values.gsm.length - 9, values.gsm.length);
      this.props.loadAgentsList(1, "telephone", search, this.state.typeAgent);
    }
    if (values.username.trim() !== "") {
      search = values.username;
      this.props.loadAgentsList(1, "username", search, this.state.typeAgent);
    }
  }

  render() {
    const {
      t,
      agentList,
      handleSubmit,
      loading,
      values,
      fields: { gsm, username },
    } = this.props;

    const styles = require("./HistoriqueAgent.scss");
    return (
      <div>
        <Modal
          show={loading}
          className="loadingModal"
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>
            <Row>
              <Col xs={12} md={12}>
                <div className="spinner">
                  <span style={{ fontSize: "11px" }}>
                    {t("chargement.title")}
                  </span>
                </div>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>

        <div>
          <Row>
            <form
              className="formContainer"
              style={{ marginTop: "20px", paddingBottom: "20px" }}
            >
              <Row className={styles.fieldRow}>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.tel")}</ControlLabel>

                  <FormControl
                    type="text"
                    {...gsm}
                    className={styles.datePickerFormControl}
                  />
                  {gsm.error && gsm.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {gsm.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.identifiant")} </ControlLabel>

                  <FormControl
                    type="text"
                    {...username}
                    className={styles.datePickerFormControl}
                  />
                  {username.error && username.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {username.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <Button
                    style={{ marginTop: " 6%" }}
                    loading={loading}
                    onClick={handleSubmit(() => {
                      this.handleSearch(values);
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-search " /> {t("form.buttons.search")}
                  </Button>
                  <Button
                    style={{ marginTop: " 6%" }}
                    loading={loading}
                    onClick={handleSubmit(() => {
                      this.handleReset();
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-refresh " /> {t("form.buttons.reset")}
                  </Button>
                </Col>
              </Row>
            </form>
          </Row>

          <div className="table-responsive tablediv">
            <Griddle
              results={agentList}
              tableClassName="table"
              customNoDataComponent={NoDataComponent}
              useGriddleStyles={false}
              useCustomPagerComponent="true"
              resultsPerPage={10}
              customPagerComponent={HistoriqueAgentPagination}
              columns={[
                "dateCreation",
                "firstName",
                "lastName",
                "username",
                "gsm",
                "statut",
                "email",
                "action",
              ]}
              columnMetadata={[
                {
                  columnName: "dateCreation",
                  displayName: t("list.cols.dateCreation"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "firstName",
                  displayName: t("list.cols.nom"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "lastName",
                  displayName: t("list.cols.prenom"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "username",
                  displayName: t("form.label.identifiant"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "gsm",
                  displayName: t("list.cols.tel"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "statut",
                  displayName: t("list.cols.statut"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "email",
                  displayName: t("list.cols.email"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "action",
                  displayName: t("list.cols.actions"),
                  customHeaderComponent: ActionHeaderComponent,
                  customComponent: ActionComponant,
                  sortable: true,
                },
              ]}
            />
          </div>
        </div>
      </div>
    );
  }
}
