import React, { Component } from "react";
import {
  Button,
  Col,
  FormControl,
  Link,
  ControlLabel,
  Modal,
  responsive,
  Row,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";
import { initializeWithKey, reduxForm, change } from "redux-form";
import { browserHistory } from "react-router";
import { translate } from "react-i18next";
import Griddle from "griddle-react";
import * as CashActions from "../Cashing/CashingReducer";
import * as HistoriqueActions from "../Historique/HistoriqueReducer";
import { connect } from "react-redux";
import HistoriquePagination from "./CashOutAgentHistoriquePagination";
import { asyncConnect } from "redux-connect";
import { reset } from "redux-form";

class HeaderComponent extends Component {
  render() {
    return (
      <div style={{ fontSize: "12px", fontWeight: "bold" }}>
        {this.props.displayName}
      </div>
    );
  }
}

class ActionHeaderComponent extends Component {
  render() {
    return (
      <div
        className="text-center"
        style={{ fontSize: "12px", fontWeight: "bold", width: "110px" }}
      >
        {this.props.displayName}
      </div>
    );
  }
}

class CenterComponent extends Component {
  render() {
    const url = this.props.data;
    return <div>{this.props.data}</div>;
  }
}

@translate(["common"])
class NoDataComponent extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="noDataResult">
        <h4>{t("errors.noResult")}</h4>
      </div>
    );
  }
}

@connect((state) => ({}), { ...CashActions, initializeWithKey })
class ActionComponant extends Component {
  render() {
    const { globalParams, rowData } = this.props;

    const ttipSelect = (
      <Tooltip id="tooltip" className="ttip">
        consulter{" "}
      </Tooltip>
    );
    const ttipDelete = (
      <Tooltip id="tooltip" className="ttip">
        Supprimer
      </Tooltip>
    );
    /* const ttipSigner = (
            <Tooltip id="tooltip" className='ttip'>signer</Tooltip>
        );*/
    return (
      <div>
        <OverlayTrigger placement="top" overlay={ttipSelect}>
          <Button
            className="iconbtn"
            onClick={() =>
              browserHistory.push({
                pathname: baseUrl + "app/" + rowData.idForTnr,
              })
            }
          >
            <i className="fa fa-briefcase " />
          </Button>
        </OverlayTrigger>
        {/* <OverlayTrigger placement="top" overlay={ttipSigner}>
                    <Button  className= 'iconbtn'
                             onClick={() =>{this.props.AlertState("signer");this.props.signerMessage(rowData.id);this.props.loadCreDocImportToComplete('','','','','','','','','','');}}
                    >
                        <i className="fa fa-check "/>
                    </Button>
                </OverlayTrigger>*/}
        <OverlayTrigger placement="top" overlay={ttipDelete}>
          <Button
            className="iconbtn"
            onClick={(e) => {
              this.props.deleteTransaction(rowData.id);
              this.props.loadCreDocImportToComplete(
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                ""
              );
            }}
          >
            <i className="fa fa-trash " />
          </Button>
        </OverlayTrigger>
      </div>
    );
  }
}

export const validate = (values) => {
  const errors = {};

  return errors;
};

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      dispatch(reset("CashOutSearchForm"));
    },
  },
])
@reduxForm({
  form: "CashOutSearchForm",
  fields: ["reference", "gsm", "statut"],
  validate,
  initialValues: {
    reference: "",
    gsm: "",
    statut: "",
  },
  destroyOnUnmount: true,
})
@connect(
  (state) => ({
    statuts: state.CashingReducer.statuts,
    searchLoading: state.HistoriqueReducer.searchLoading,
    search: state.HistoriqueReducer.search,
  }),
  { ...CashActions, ...HistoriqueActions, initializeWithKey }
)
export default class HistoriqueCashOutAgent extends Component {
  constructor() {
    super();
    this.handleSearch = this.handleSearch.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      showPanelSearch: true,
      dateSaisie: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.search !== nextProps.search && nextProps.search === "") {
      this.props.dispatch(reset("CashOutSearchForm"));
    }
  }

  handleChange(event) {
    this.props.dispatch(
      change("CashOutSearchForm", "statut", event.target.value)
    );
  }

  handleReset() {
    this.props.loadCashOutAgent(1, "");
    this.props.dispatch(reset("CashOutSearchForm"));
  }

  handleSearch(values) {
    let search = "";

    if (values.gsm !== "") {
      search = search + "beneficiaire:" + values.gsm;
    }

    if (values.statut !== "" && values.statut !== "Choisir un statut") {
      if (search !== "") search = search + ",";
      search = search + "statut:" + values.statut;
    }

    if (values.reference !== "") {
      if (search !== "") search = search + ",";
      search = search + "identifiantDemande:" + values.reference;
    }

    this.props.loadCashOutAgent(1, search);
  }

  render() {
    const {
      t,
      listCashOutAgent,
      handleSubmit,
      searchLoading,
      values,
      statuts,
      fields: { gsm, reference, statut },
    } = this.props;
    values.dateDebut = this.state.startDate;
    values.dateFin = this.state.endDate;
    const styles = require("../Souscription/Souscription.scss");
    return (
      <div>
        <Modal
          show={searchLoading}
          className="loadingModal"
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>
            <Row>
              <Col xs={12} md={12}>
                <div className="spinner">
                  <span style={{ fontSize: "11px" }}>
                    {t("chargement.title")}
                  </span>
                </div>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>
        <div>
          <Row>
            <form
              className="formContainer"
              style={{ marginTop: "20px", paddingBottom: "20px" }}
            >
              <Row className={styles.fieldRow}>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.phone")}</ControlLabel>

                  <FormControl
                    type="text"
                    {...gsm}
                    className={styles.datePickerFormControl}
                  />
                  {gsm.error && gsm.touched && (
                    <div className={styles.error}>
                      <i
                        className="fa fa-exclamation-triangle"
                        aria-hidden="true"
                      >
                        {gsm.error}
                      </i>
                    </div>
                  )}
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.statut")}</ControlLabel>
                  <FormControl
                    componentClass="select"
                    {...statut}
                    ref="typeCodeList"
                    onChange={this.handleChange}
                    className={styles.datePickerFormControl}
                  >
                    <option hidden>{t("form.label.choisirStatut")}</option>
                    {statuts &&
                      statuts.length &&
                      statuts.map((typeP) => (
                        <option key={typeP.id} value={typeP.code}>
                          {typeP.libelle}
                        </option>
                      ))}
                  </FormControl>
                </Col>
                <Col xs="12" md="3">
                  <ControlLabel>{t("form.label.reference")}</ControlLabel>

                  <FormControl
                    type="text"
                    {...reference}
                    className={styles.datePickerFormControl}
                  />
                </Col>

                <Col xs="12" md="3">
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.handleSearch(values);
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-search " /> {t("form.buttons.search")}
                  </Button>
                  <Button
                    style={{ marginTop: " 8%" }}
                    loading={searchLoading}
                    onClick={handleSubmit(() => {
                      this.handleReset();
                    })}
                    bsStyle="primary"
                  >
                    <i className="fa fa-refresh " /> {t("form.buttons.reset")}
                  </Button>
                </Col>
              </Row>
            </form>
          </Row>
          <div className="table-responsive tablediv">
            <Griddle
              results={listCashOutAgent}
              tableClassName="table"
              customNoDataComponent={NoDataComponent}
              useGriddleStyles={false}
              useCustomPagerComponent="true"
              resultsPerPage={10}
              customPagerComponent={HistoriquePagination}
              columns={[
                "dateCreation",
                "identifiantDemande",
                "agent",
                "beneficiaire",
                "motif",
                "montantFormatted",
                "statut",
              ]}
              columnMetadata={[
                {
                  columnName: "dateCreation",
                  displayName: t("list.cols.dateCreation"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "identifiantDemande",
                  displayName: t("list.cols.reference"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "agent",
                  displayName: t("list.cols.agent"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: false,
                },
                {
                  columnName: "beneficiaire",
                  displayName: t("list.cols.beneficiaire"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "motif",
                  displayName: t("list.cols.motif"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "montantFormatted",
                  displayName: t("list.cols.montant"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
                {
                  columnName: "statut",
                  displayName: t("list.cols.statut"),
                  customHeaderComponent: HeaderComponent,
                  customComponent: CenterComponent,
                  sortable: true,
                },
              ]}
            />
          </div>
        </div>
      </div>
    );
  }
}
