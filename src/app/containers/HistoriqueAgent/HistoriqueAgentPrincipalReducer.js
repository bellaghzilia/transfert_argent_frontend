const LOAD_HISTORIQUE_AGENTS = 'HistoriqueAgentsPrincipal/LOAD_HISTORIQUE_AGENTS';
const LOAD_HISTORIQUE_AGENTS_SUCCESS = 'HistoriqueAgentsPrincipal/LOAD_HISTORIQUE_AGENTS_SUCCESS';
const LOAD_HISTORIQUE_AGENTS_FAIL = 'HistoriqueAgentsPrincipal/LOAD_HISTORIQUE_AGENTS_FAIL';

const LOAD_AGENT = 'HistoriqueAgentsPrincipal/LOAD_AGENT';
const LOAD_AGENT_SUCCESS = 'HistoriqueAgentsPrincipal/LOAD_AGENT_SUCCESS';
const LOAD_AGENT_FAIL = 'HistoriqueAgentsPrincipal/LOAD_AGENT_FAIL';

const SET_PAGE_TITLE = 'HistoriqueAgentsPrincipal/SET_PAGE_TITLE';
const SET_TAB_INDEX = 'HistoriqueAgentsPrincipal/SET_TAB_INDEX';

const UPDATE_AGENT = 'HistoriqueAgentsPrincipal/UPDATE_AGENT';
const UPDATE_AGENT_SUCCESS = 'HistoriqueAgentsPrincipal/UPDATE_AGENT_SUCCESS';
const UPDATE_AGENT_FAIL = 'HistoriqueAgentsPrincipal/UPDATE_AGENT_FAIL';

const initialState = {};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case LOAD_HISTORIQUE_AGENTS:
            return {
                ...state,
                loading: true,
                loaded: false
            };
        case LOAD_HISTORIQUE_AGENTS_SUCCESS:
            let list = action.result.agentList;
            list.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                typeSearch : action.typeSearch,
                search : action.search,
                index : action.index,
                profileMetier: action.profileMetier,
                loading: false,
                loaded: true,
                agentList: list,
                success: action.result.success,
                agentsListMaxPages: action.result.total
            };
        case LOAD_HISTORIQUE_AGENTS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                success: false
            };
        case LOAD_AGENT:
            return {
                ...state,
                loading: true,
                loaded: false
            };
        case LOAD_AGENT_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                agent: action.result.agent,
                success: action.result.success
            };
        case LOAD_AGENT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                success: false
            };

        case SET_TAB_INDEX:
            let _tabIndex = action.index ? action.index : 1;
            return {
                ...state,
                tabIndex: _tabIndex
            };
        case SET_PAGE_TITLE:
            return {
                ...state,
                title: action.title
            };
        case UPDATE_AGENT:
            return {
                ...state,
                loading: true
            };
        case UPDATE_AGENT_SUCCESS:
            if (action.result.success)
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    agent : action.result.agent,
                    messageRetour: action.result.messageRetour,
                    isSuccess: action.result.success
                };
            else
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    messageRetour: action.result.messageRetour,
                    isSuccess: action.result.success
                };
        case UPDATE_AGENT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                isSuccess: false
            };
        default:
            return state;
    }
}

function objectToParams(object) {
    let str = '';
    for (const key in object) {
        if (str !== '') {
            str += '&';
        }
        str += key + '=' + encodeURIComponent(object[key]);
    }
    return str;
}

export function setTitle(title) {
    return {
        type: SET_PAGE_TITLE,
        title
    }
}

export function setTabIndex(index) {
    return {
        type: SET_TAB_INDEX,
        index
    }
}

export function loadAgentsList(index, typeSearch,search, profileMetier) {
    return {
        typeSearch,
        search,
        index,
        profileMetier,
        types: [LOAD_HISTORIQUE_AGENTS, LOAD_HISTORIQUE_AGENTS_SUCCESS, LOAD_HISTORIQUE_AGENTS_FAIL],
        promise: (client) => client.get('agent/listAgents?typeSearch=' + typeSearch + '&search=' + search + '&pm=' + profileMetier + '&page=' + index + '&offset=0&max=10')
    };
}

export function loadAgent(id) {
    return {
        types: [LOAD_AGENT, LOAD_AGENT_SUCCESS, LOAD_AGENT_FAIL],
        promise: (client) => client.get('agent/getAgent/' + id)
    };
}

export function update(dto) {
    return {
        types: [UPDATE_AGENT, UPDATE_AGENT_SUCCESS, UPDATE_AGENT_FAIL],
        promise: (client) => client.post('/agent/update',{params: {type:'json'}, data: dto})
    };
}
