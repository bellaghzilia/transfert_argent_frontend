const LOAD_HISTORIQUE_AGENTS = 'HistoriqueAgents/LOAD_HISTORIQUE_AGENTS';
const LOAD_HISTORIQUE_AGENTS_SUCCESS = 'HistoriqueAgents/LOAD_HISTORIQUE_AGENTS_SUCCESS';
const LOAD_HISTORIQUE_AGENTS_FAIL = 'HistoriqueAgents/LOAD_HISTORIQUE_AGENTS_FAIL';

const LOAD_AGENT = 'HistoriqueAgents/LOAD_AGENT';
const LOAD_AGENT_SUCCESS = 'HistoriqueAgents/LOAD_AGENT_SUCCESS';
const LOAD_AGENT_FAIL = 'HistoriqueAgents/LOAD_AGENT_FAIL';

const VALIDATE_AGENT = 'AGENT/VALIDATE_AGENT';
const VALIDATE_AGENT_SUCCESS = 'AGENT/VALIDATE_AGENT_SUCCESS';
const VALIDATE_AGENT_FAIL = 'AGENT/VALIDATE_AGENT_FAIL';

const REJETER_AGENT = 'AGENT/REJETER_AGENT';
const REJETER_AGENT_SUCCESS = 'AGENT/REJETER_AGENT_SUCCESS';
const REJETER_AGENT_FAIL = 'AGENT/REJETER_AGENT_FAIL';

const SEND_MAIL = 'AGENT/SEND_MAIL';
const SEND_MAIL_SUCCESS = 'AGENT/SEND_MAIL_SUCCESS';
const SEND_MAIL_FAIL = 'AGENT/SEND_MAIL_FAIL';

const SET_PAGE_TITLE = 'HistoriqueAgents/SET_PAGE_TITLE';
const SET_TAB_INDEX = 'HistoriqueAgents/SET_TAB_INDEX';

const UPDATE_AGENT = 'AGENT/UPDATE_AGENT';
const UPDATE_AGENT_SUCCESS = 'AGENT/UPDATE_AGENT_SUCCESS';
const UPDATE_AGENT_FAIL = 'AGENT/UPDATE_AGENT_FAIL';

const LOAD_MOTIF = 'AGENT/INSTANCE_MOTIF';
const LOAD_MOTIF_SUCCESS = 'AGENT/LOAD_MOTIF_SUCCESS';
const LOAD_MOTIF_FAIL = 'AGENT/LOAD_MOTIF_FAIL';

const LOAD_AGENT_CURRENCY = 'AGENT/LOAD_AGENT_CURRENCY';
const LOAD_AGENT_CURRENCY_SUCCESS = 'AGENT/LOAD_AGENT_CURRENCY_SUCCESS';
const LOAD_AGENT_CURRENCY_FAIL = 'AGENT/LOAD_AGENT_CURRENCY_FAIL';

const UPDATE_OPPOSE = 'AGENT/UPDATE_OPPOSE';
const UPDATE_OPPOSE_SUCCESS = 'AGENT/UPDATE_OPPOSE_SUCCESS';
const UPDATE_OPPOSE_FAIL = 'AGENT/UPDATE_OPPOSE_FAIL';

const UPDATE_UNSUSPEND = 'AGENT/UPDATE_UNSUSPEND';
const UPDATE_UNSUSPEND_SUCCESS = 'AGENT/UPDATE_UNSUSPEND_SUCCESS';
const UPDATE_UNSUSPEND_FAIL = 'AGENT/UPDATE_UNSUSPEND_FAIL';

const BLOCK_AGENT = 'AGENT/BLOCK_AGENT';
const BLOCK_AGENT_SUCCESS = 'AGENT/BLOCK_AGENT_SUCCESS';
const BLOCK_AGENT_FAIL = 'AGENT/BLOCK_AGENT_FAIL';


const RELOAD = 'AGENT/RELOAD';

const initialState = {};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case LOAD_HISTORIQUE_AGENTS:
            return {
                ...state,
                loading: true,
                loaded: false
            };
        case LOAD_HISTORIQUE_AGENTS_SUCCESS:
            let list = action.result.agentList;
            list.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                typeSearch : action.typeSearch,
                search : action.search,
                index : action.index,
                profileMetier: action.profileMetier,
                loading: false,
                loaded: true,
                agentList: list,
                success: action.result.success,
                agentsListMaxPages: action.result.total
            };
        case LOAD_HISTORIQUE_AGENTS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                success: false
            };
        case LOAD_AGENT:
            return {
                ...state,
                loading: true,
                loaded: false
            };
        case LOAD_AGENT_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                agent: action.result.agent,
                success: action.result.success
            };
        case LOAD_AGENT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                success: false
            };
        case VALIDATE_AGENT:
            return {
                ...state,
                loading: true
            };
        case VALIDATE_AGENT_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                agent : action.result.success ? action.result.agent : state.agent,
                messageRetour: action.result.messageRetour,
                isSuccess: action.result.success
            };
        case VALIDATE_AGENT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                isSuccess: false
            };
        case REJETER_AGENT:
            return {
                ...state,
                loading: true
            };
        case REJETER_AGENT_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                agent : action.result.success ? action.result.agent : state.agent,
                messageRetour: action.result.messageRetour,
                isSuccess: action.result.success
            };
        case REJETER_AGENT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                isSuccess: false
            };
        case SEND_MAIL:
            return {
                ...state,
                loading: true
            };
        case SEND_MAIL_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                messageRetour: action.result.messageRetour,
                isSuccess: action.result.success
            };
        case SEND_MAIL_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                isSuccess: false
            };
        case SET_TAB_INDEX:
            let _tabIndex = action.index ? action.index : 1;
            return {
                ...state,
                tabIndex: _tabIndex
            };
        case SET_PAGE_TITLE:
            return {
                ...state,
                title: action.title
            };
        case UPDATE_AGENT:
            return {
                ...state,
                loading: true
            };
        case UPDATE_AGENT_SUCCESS:
            if (action.result.success)
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    agent : action.result.agent,
                    messageRetour: action.result.messageRetour,
                    isSuccess: action.result.success
                };
            else
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    messageRetour: action.result.messageRetour,
                    isSuccess: action.result.success
                };
        case UPDATE_AGENT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                isSuccess: false
            };
        case LOAD_MOTIF:
            return {
                ...state,
                loading: true
            };
        case LOAD_MOTIF_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                listMotif: action.result.ListMotifsRejet,
                error: null,
                shouldReloadForm : false
            };
        case LOAD_MOTIF_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
            case LOAD_AGENT_CURRENCY:
            return {
                ...state,
                loading: true
            };
        case LOAD_AGENT_CURRENCY_SUCCESS:
            if (action.result.success)
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    soldeAgent : action.result.solde,
                    success: action.result.success
                };
            else
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    success: action.result.success
                };
        case LOAD_AGENT_CURRENCY_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                success: false,
                message:action.result.message

            };


        case UPDATE_OPPOSE:
            return {
                ...state,
                SouscriptionSaved: null,
                loading: true,
                loadedDocuments: null,
                isRecap: false,
            };
        case UPDATE_OPPOSE_SUCCESS:
            if (action.result.success) {
                return {
                    ...state,
                    loading: false,
                    isOppSuccess: action.result.success,
                    messageRetour: action.result.messageRetour,
                    agent: action.result.agent
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    isOppSuccess: false,
                    messageRetour: action.result.messageRetour
                };
            }
        case UPDATE_OPPOSE_FAIL:
            return {
                ...state,
                loading: false,
                SouscriptionSaved: null
            };
        case UPDATE_UNSUSPEND:
            return {
                ...state,
                SouscriptionSaved: null,
                loadedDocuments: null,
                loading: true,
                isRecap: false,
            };
        case UPDATE_UNSUSPEND_SUCCESS:
            if (action.result.success) {
                return {
                    ...state,
                    loading: false,
                    isOppSuccess: action.result.success,
                    messageRetour: action.result.messageRetour,
                    agent: action.result.agent
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    isOppSuccess: action.result.success,
                    opposedWallet: true,
                    messageRetour: action.result.messageRetour
                };
            }
        case UPDATE_UNSUSPEND_FAIL:
            return {
                ...state,
                loading: false,
                SouscriptionSaved: null
            };

        case BLOCK_AGENT:
            return {
                ...state,
                SouscriptionSaved: null,
                loadedDocuments: null,
                loading: true,
                isRecap: false,
            };
        case BLOCK_AGENT_SUCCESS:
            if (action.result.success) {
                return {
                    ...state,
                    loading: false,
                    isBlockSuccess: action.result.success,
                    messageRetour: action.result.messageRetour,
                    agent: action.result.agent
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    isBlockSuccess: action.result.success,
                    messageRetour: action.result.messageRetour
                };
            }
        case BLOCK_AGENT_FAIL:
            return {
                ...state,
                loading: false,
                SouscriptionSaved: null
            }
        case RELOAD:
            return {
                ...state,
                isOppSuccess: null,
                messageRetourOpp: null,
                isSuccess : null
            };
        default:
            return state;
    }
}

export function reloadUpdateForm() {
    return {
        type: RELOAD,
    }
}

function objectToParams(object) {
    let str = '';
    for (const key in object) {
        if (str !== '') {
            str += '&';
        }
        str += key + '=' + encodeURIComponent(object[key]);
    }
    return str;
}

export function setTitle(title) {
    return {
        type: SET_PAGE_TITLE,
        title
    }
}

export function setTabIndex(index) {
    return {
        type: SET_TAB_INDEX,
        index
    }
}

export function loadAgentsList(index,typeSearch, search, profileMetier) {
    return {
        typeSearch,
        search,
        index,
        profileMetier,
        types: [LOAD_HISTORIQUE_AGENTS, LOAD_HISTORIQUE_AGENTS_SUCCESS, LOAD_HISTORIQUE_AGENTS_FAIL],
        promise: (client) => client.get('agent/listAgents?typeSearch=' + typeSearch + '&search=' + search + '&pm=' + profileMetier + '&page=' + index + '&offset=0&max=10')
    };
}

export function loadAgent(id) {
    return {
        types: [LOAD_AGENT, LOAD_AGENT_SUCCESS, LOAD_AGENT_FAIL],
        promise: (client) => client.get('agent/getAgent/' + id)
    };
}

export function valideCreation(dto) {
    return {
        types: [VALIDATE_AGENT, VALIDATE_AGENT_SUCCESS, VALIDATE_AGENT_FAIL],
            promise: (client) => client.get('agent/validate?idUser=' + dto.id)
    };
}

export function rejeterCreation(dto) {
    return {
        types: [REJETER_AGENT, REJETER_AGENT_SUCCESS, REJETER_AGENT_FAIL],
        promise: (client) => client.post('agent/rejeter', {params: {type:'json'}, data: dto})
    };
}

export function sendSmsAndMail(dto) {
    return {
        types: [SEND_MAIL, SEND_MAIL_SUCCESS, SEND_MAIL_FAIL],
        promise: (client) => client.get('agent/sendSmsAndMail?id=' + dto.id)
    };
}

export function update(dto) {
    return {
        types: [UPDATE_AGENT, UPDATE_AGENT_SUCCESS, UPDATE_AGENT_FAIL],
        promise: (client) => client.post('/agent/update',{params: {type:'json'}, data: dto})
    };
}

export function getListMotif() {
    return {
        types: [LOAD_MOTIF, LOAD_MOTIF_SUCCESS, LOAD_MOTIF_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getListMotifs')
    };
}

export function getAgentCurrency(username) {
    return {
        types: [LOAD_AGENT_CURRENCY, LOAD_AGENT_CURRENCY_SUCCESS, LOAD_AGENT_CURRENCY_FAIL],
        promise: (client) => client.get('/agent/getAgentCurrency?username='+username)
    };
}

export function suspendAgent(username,id) {
    let obj = [];
    obj.username = username;
    obj.idagent = id;
    return {
        types: [UPDATE_OPPOSE, UPDATE_OPPOSE_SUCCESS, UPDATE_OPPOSE_FAIL],
        promise: (client) => client.post('/agent/oppose', { data: objectToParams(obj) })
    };
}

export function unsuspendAgent(username,id) {
    let obj = [];
    obj.username = username;
    obj.idagent = id;
    return {
        types: [UPDATE_UNSUSPEND, UPDATE_UNSUSPEND_SUCCESS, UPDATE_UNSUSPEND_FAIL],
        promise: (client) => client.post('/agent/leverOppose', { data: objectToParams(obj) })
    };
}

export function blockAgent(username,id) {
    let obj = [];
    obj.username = username;
    obj.idagent = id;
    return {
        types: [BLOCK_AGENT, BLOCK_AGENT_SUCCESS, BLOCK_AGENT_FAIL],
        promise: (client) => client.post('agent/cloture', { data: objectToParams(obj) })
    };
}