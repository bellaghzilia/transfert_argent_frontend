import React, { Component } from "react";
import PropTypes from "prop-types";
import { Row, Tab, Tabs, Col, Modal } from "react-bootstrap/lib";
import { translate } from "react-i18next";
import { connect } from "react-redux";
import { initializeWithKey, reset } from "redux-form";
import { asyncConnect } from "redux-connect";
import PageHeader from "react-bootstrap/lib/PageHeader";

import * as HistoriqueADAction from "./HistoriqueAgentDetaillantReducer";
import * as HistoriqueAPAction from "./HistoriqueAgentPrincipalReducer";
import HistoriqueAgentDetaillant from "./HistoriqueAgentDetaillant";
import HistoriqueAgentPrincipal from "./HistoriqueAgentPrincipal";
import * as UserActions from "../User/UserReducer";
import HistoriqueCashInAgent from "./HistoriqueCashInAgent";
import HistoriqueCashOutAgent from "./HistoriqueCashOutAgent";
import * as HistoriqueActions from "../Historique/HistoriqueReducer";
import { Button } from "react-bootstrap";
import { AuthorizedComponent } from "react-router-role-authorization";

@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      return Promise.all([
        dispatch(reset("AgentSearchForm")),
        dispatch(HistoriqueADAction.loadAgentsList(1, "", "", "AD")),
        dispatch(HistoriqueAPAction.loadAgentsList(1, "", "", "AP")),
        dispatch(HistoriqueActions.loadCashInAgent(1, "")),
        dispatch(HistoriqueActions.loadCashOutAgent(1, "")),
      ]);
    },
  },
])
@connect(
  (state) => ({
    loading: state.HistoriqueAgentDetaillantReducer.loading,
    listCashInAgent: state.HistoriqueReducer.listCashInAgent,
    listCashOutAgent: state.HistoriqueReducer.listCashOutAgent,
    userFrontDetails: state.user.userFrontDetails,
  }),
  { ...HistoriqueADAction, ...UserActions, initializeWithKey }
)
@translate(["historique"], { wait: true })
export default class HistoriqueAgent extends AuthorizedComponent {
  constructor(props) {
    super(props);
    this.userRoles = this.props.userFrontDetails.roles;
    this.notAuthorizedPath = baseUrl + "app/AccessDenied";
  }

  render() {
    const {
      t,
      tabIndex,
      userFrontDetails,
      setTabIndex,
      setTitle,
      loading,
      listCashInAgent,
      listCashOutAgent,
    } = this.props;
    const showMenuHistoriqueEDPAgentDetaillant =
      userFrontDetails.roles.indexOf("ROLE_EDP_HISTORIQUE_DETAILLANT") !== -1;
    const showMenuHistoriqueEDPAgentPrincipal =
      userFrontDetails.roles.indexOf("ROLE_EDP_HISTORIQUE_PRINCIPAL") !== -1;
    const showMenuCashHistorique =
      userFrontDetails.roles.indexOf("ROLE_EDP_CASH_HISTORIQUE") !== -1;

    const changeIndexTab = (index) => {
      switch (index) {
        case 1:
          setTitle("HistoriqueDetaillant");
          setTabIndex(1);
          break;
        case 2:
          setTitle("HistoriquePrincipal");
          setTabIndex(2);
          break;
        case 3:
          setTitle("cash In Agent");
          setTabIndex(4);
          break;
        case 4:
          setTitle("cash Out Agent");
          setTabIndex(5);
      }
    };

    return (
      <div>
        <Modal
          show={loading}
          className="loadingModal"
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>
            <Row>
              <Col xs={12} md={12}>
                <div className="spinner">
                  <span style={{ fontSize: "11px" }}>
                    {t("chargement.title")}
                  </span>
                </div>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>

        <PageHeader>
          <h3> {t("agent.title")}</h3>
        </PageHeader>

        <div className="formContainer" style={{ padding: "12px" }}>
          <Tabs
            defaultActiveKey={tabIndex}
            onSelect={changeIndexTab}
            className="full-width-tabs"
          >
            {showMenuHistoriqueEDPAgentPrincipal && (
              <Tab
                eventKey={2}
                onSelect={changeIndexTab}
                title={t("header.tabTitles.agentPrincipal")}
              >
                <Row>
                  <HistoriqueAgentPrincipal t={t} />
                </Row>
              </Tab>
            )}
            {showMenuHistoriqueEDPAgentDetaillant && (
              <Tab eventKey={1} title={t("header.tabTitles.agentDetaillant")}>
                <Row>
                  <HistoriqueAgentDetaillant t={t} />
                </Row>
              </Tab>
            )}
            {showMenuCashHistorique && (
              <Tab
                eventKey={4}
                onSelect={changeIndexTab}
                title={t("header.tabTitles.cashInAgent")}
              >
                <Row>
                  <HistoriqueCashInAgent
                    listCashInAgent={listCashInAgent}
                    t={t}
                  />
                </Row>
              </Tab>
            )}
            {showMenuCashHistorique && (
              <Tab
                eventKey={5}
                onSelect={changeIndexTab}
                title={t("header.tabTitles.cashOutAgent")}
              >
                <Row>
                  <HistoriqueCashOutAgent
                    listCashOutAgent={listCashOutAgent}
                    t={t}
                  />
                </Row>
              </Tab>
            )}
          </Tabs>
        </div>
      </div>
    );
  }
}
