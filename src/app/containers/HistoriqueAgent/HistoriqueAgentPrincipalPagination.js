import React, { Component } from "react";
import PropTypes from "prop-types";

import { Pagination } from "react-bootstrap";
import { connect } from "react-redux";
import { initializeWithKey } from "redux-form";

import * as HistoriqueAgentActions from "./HistoriqueAgentPrincipalReducer";

@connect(
  (state) => ({
    maxPages: state.HistoriqueAgentPrincipalReducer.agentsListMaxPages,
    search: state.HistoriqueAgentPrincipalReducer.search,
    index: state.HistoriqueAgentPrincipalReducer.index,
    profileMetier: state.HistoriqueAgentPrincipalReducer.profileMetier,
    typeSearch: state.HistoriqueAgentPrincipalReducer.typeSearch,
  }),
  { ...HistoriqueAgentActions, initializeWithKey }
)
export default class PaginationHistorique extends Component {
  static propTypes = {
    loadAgentsList: PropTypes.func,
    maxPages: PropTypes.number,
  };
  constructor() {
    super();
    this.state = {
      activePage: 1,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.index !== nextProps.index &&
      this.props.index !== 1 &&
      nextProps.index === 1
    ) {
      this.setState({ activePage: nextProps.index });
    }
  }

  render() {
    const {
      loadAgentsList,
      maxPages,
      search,
      profileMetier,
      typeSearch,
    } = this.props;
    let maxPage = 0;
    if (maxPages % 10 === 0) {
      maxPage = maxPages / 10;
    } else {
      maxPage = Math.ceil(maxPages / 10);
    }
    const changePage = (event) => {
      this.setState({
        activePage: event,
      });
      loadAgentsList(event, typeSearch, search, profileMetier);
    };
    return (
      <div>
        {maxPages > 10 ? (
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={maxPage}
            maxButtons={5}
            bsSize="medium"
            activePage={this.state.activePage}
            onSelect={changePage}
          />
        ) : (
          <div />
        )}
      </div>
    );
  }
}
