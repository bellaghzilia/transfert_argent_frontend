/**
 * Created by lenovo on 26/12/2018.
 */

const LOAD_SOUSCRIPTION = 'SOUSCRIPTION/LOAD_SOUSCRIPTION';
const LOAD_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION/LOAD_SOUSCRIPTION_SUCCESS';
const LOAD_SOUSCRIPTION_FAIL = 'SOUSCRIPTION/LOAD_SOUSCRIPTION_FAIL';

const INSTANCE_SOUSCRIPTION = 'SOUSCRIPTION/INSTANCE_SOUSCRIPTION';
const INSTANCE_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION/INSTANCE_SOUSCRIPTION_SUCCESS';
const INSTANCE_SOUSCRIPTION_FAIL = 'SOUSCRIPTION/INSTANCE_SOUSCRIPTION_FAIL';

const UNLOCK_SOUSCRIPTION = 'SOUSCRIPTION/UNLOCK_SOUSCRIPTION';
const UNLOCK_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION/UNLOCK_SOUSCRIPTION_SUCCESS';
const UNLOCK_SOUSCRIPTION_FAIL = 'SOUSCRIPTION/UNLOCK_SOUSCRIPTION_FAIL';

const SIGN_SOUSCRIPTION = 'SOUSCRIPTION/SIGN_SOUSCRIPTION';
const SIGN_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION/SIGN_SOUSCRIPTION_SUCCESS';
const SIGN_SOUSCRIPTION_FAIL = 'SOUSCRIPTION/SIGN_SOUSCRIPTION_FAIL';

const REJECT_SOUSCRIPTION = 'SOUSCRIPTION/REJECT_SOUSCRIPTION';
const REJECT_SOUSCRIPTION_SUCCESS = 'SOUSCRIPTION/REJECT_SOUSCRIPTION_SUCCESS';
const REJECT_SOUSCRIPTION_FAIL = 'SOUSCRIPTION/REJECT_SOUSCRIPTION_FAIL';

const LOAD_MOTIF = 'SOUSCRIPTION/INSTANCE_MOTIF';
const LOAD_MOTIF_SUCCESS = 'SOUSCRIPTION/LOAD_MOTIF_SUCCESS';
const LOAD_MOTIF_FAIL = 'SOUSCRIPTION/LOAD_MOTIF_FAIL';

const LOAD_IS_SIRON = 'SOUSCRIPTION/LOAD_IS_SIRON';
const LOAD_IS_SIRON_SUCCESS = 'SOUSCRIPTION/LOAD_IS_SIRON_SUCCESS';
const LOAD_IS_SIRON_FAIL = 'SOUSCRIPTION/LOAD_IS_SIRON_FAIL';

const RELOAD = 'SOUSCRIPTION/RELOAD';

const initialState = {
    dataForDetail:null
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case LOAD_SOUSCRIPTION:
            return {
                ...state,
                souscriptionSearchLoading: true,
                isRecap:false
            };
        case LOAD_SOUSCRIPTION_SUCCESS: {
            let souscriptionList = action.result.demandeList;
            souscriptionList.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                search : action.search,
                index : action.index,
                souscriptionList : souscriptionList,
                souscriptionMaxPages: action.result.total,
                souscriptionSearchLoading: false,
                loaded: true,
            };
        }
        case LOAD_SOUSCRIPTION_FAIL:
            return {
                ...state,
                souscriptionSearchLoading: false,
                loaded: false,
                souscriptionList: null,
                error: action.error
            };
        case LOAD_IS_SIRON:
            return {
                ...state,
                loading: true
            };
        case LOAD_IS_SIRON_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                showSironFields : action.result.activated,
                error: null,
                shouldReloadForm : false
            };
        case LOAD_IS_SIRON_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case INSTANCE_SOUSCRIPTION:
            return {
                ...state,
                loading: true,
                isRecap:false,
                dataForDetail:null
            };
        case INSTANCE_SOUSCRIPTION_SUCCESS: {
            if (action.result.success) {
                return {
                    ...state,
                    dataForDetail: action.result.demandeInstance,
                    loading: false,
                    loaded: true,
                };
            }else{
                return {
                    ...state,
                    dataForDetail : null,
                    instanceErrorMsg: action.result.messageRetour,
                    loading: false,
                    loaded: true,
                };
            }
        }
        case INSTANCE_SOUSCRIPTION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                dataForDetail: null,
                error: action.error
            };
        case UNLOCK_SOUSCRIPTION:
            return {
                ...state,
                loading: true,
            };
        case UNLOCK_SOUSCRIPTION_SUCCESS: {
            if (action.result.success) {
                return {
                    ...state,
                    dataForDetail : null,
                    loading: false,
                    loaded: true,
                };
            }else{
                return {
                    ...state,
                    dataForDetail : null,
                    instanceErrorMsg: action.result.messageRetour,
                    loading: false,
                    loaded: true,
                };
            }
        }
        case UNLOCK_SOUSCRIPTION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                dataForDetail: null,
                error: action.error
            };
        case SIGN_SOUSCRIPTION:
            return {
                ...state,
                loadingV: true,
            };
        case SIGN_SOUSCRIPTION_SUCCESS: {
            if (action.result.success) {
                return {
                    ...state,
                    dataForDetail: action.result.demandeInstance,
                    isSuccess : action.result.success,
                    loadingV: false,
                    loaded:true,
                    errorMsg: action.result.messageRetour
                };
            } else {
                return {
                    ...state,
                    isSuccess: action.result.success,
                    loadingV: false,
                    loaded: true,
                    dataForDetail: action.result.demandeInstance,
                    errorMsg: action.result.messageRetour
                }
            }
        }
        case SIGN_SOUSCRIPTION_FAIL:
            return {
                ...state,
                loadingV: false,
                loaded: false,
                dataForDetail: null,
                error: action.error
            };
        case REJECT_SOUSCRIPTION:
            return {
                ...state,
                loading: true,
                isRecap:false
            };
        case REJECT_SOUSCRIPTION_SUCCESS: {
            if (action.result.success) {
                return {
                    ...state,
                    dataForDetail: action.result.demandeInstance,
                    isSuccess : action.result.success,
                    loading: false,
                    loaded:true,
                    errorMsg: action.result.messageRetour
                };
            } else {
                return {
                    ...state,
                    isSuccess: action.result.success,
                    loading: false,
                    loaded: true,
                    errorMsg: action.result.messageRetour
                }
            }
        }
        case REJECT_SOUSCRIPTION_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                dataForDetail: null,
                error: action.error
            };
        case LOAD_MOTIF:
            return {
                ...state,
                loading: true
            };
        case LOAD_MOTIF_SUCCESS:
            const listMotif = action.result.ListMotifsRejet;
            return {
                ...state,
                loading: false,
                loaded: true,
                listMotif,
                error: null,
                shouldReloadForm : false
            };
        case LOAD_MOTIF_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case RELOAD:
            return {
                ...state,
                loading: false,
                loaded: false,
                isRecap : false,
                isUpdate : false,
                isSuccess : null,
                dataForDetail : null,
                instanceErrorMsg : "",
                errorMsg : ""
            };
        default:
            return state;

    }
}

function objectToParams(object) {
    let str = '';
    for (const key in object) {
        if (str !== '') {
            str += '&';
        }
        str += key + '=' + encodeURIComponent(object[key]);
    }
    return str;
}



export function loadSouscriptionList(index, search) {

    return {
        search,
        index,
        types: [LOAD_SOUSCRIPTION, LOAD_SOUSCRIPTION_SUCCESS, LOAD_SOUSCRIPTION_FAIL],
        promise: (client) => client.get('demandeSouscription/toValidateList?search=' + search + '&page='+index+'&offset=0&max=10')
    };
}

export function getDemandeInstance(id) {
    return {
        types: [INSTANCE_SOUSCRIPTION, INSTANCE_SOUSCRIPTION_SUCCESS, INSTANCE_SOUSCRIPTION_FAIL],
        promise: (client) => client.get('demandeSouscription/instance/'+id)
    };
}

export function unlockDemande(id) {
    return {
        types: [UNLOCK_SOUSCRIPTION, UNLOCK_SOUSCRIPTION_SUCCESS, UNLOCK_SOUSCRIPTION_FAIL],
        promise: (client) => client.get('demandeSouscription/unlock/' + id)
    };

}

export function signSouscription(values, dto) {
    dto.motifValidation = values.motif;
    dto.dateValidite = values.dateValidite;
    dto.dateNaissance = values.dateNaissance;
    dto.lieuNaissance = values.lieuNaissance;
    dto.adresse = values.adresse;
    dto.numeroIdentite = values.numeroIdentite;
    return {
        types: [SIGN_SOUSCRIPTION, SIGN_SOUSCRIPTION_SUCCESS, SIGN_SOUSCRIPTION_FAIL],
        promise: (client) => client.post('demandeSouscription/submit', {data: objectToParams(dto)})
    };
}

export function rejectSouscription(values, dto) {
    dto.motifValidation = values.motif;
    return {
        types: [REJECT_SOUSCRIPTION, REJECT_SOUSCRIPTION_SUCCESS, REJECT_SOUSCRIPTION_FAIL],
        promise: (client) => client.post('demandeSouscription/reject', {data: objectToParams(dto)})
    };
}

export function getListMotif() {
    return {
        types: [LOAD_MOTIF, LOAD_MOTIF_SUCCESS, LOAD_MOTIF_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getListMotifs')
    };
}

export function reload(){
    return{
        type: RELOAD,
    }
}

export function isSironActivated() {
    return {
        types: [LOAD_IS_SIRON, LOAD_IS_SIRON_SUCCESS, LOAD_IS_SIRON_FAIL],
        promise: (client) => client.get('demandeSouscription/isSironActivated')
    };
}




