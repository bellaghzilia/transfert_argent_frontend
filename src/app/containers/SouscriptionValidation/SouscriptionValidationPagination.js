/**
 * Created by lenovo on 17/01/2019.
 */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, Pagination } from "react-bootstrap";
import { connect } from "react-redux";
import { initializeWithKey } from "redux-form";
import * as SouscriptionActions from "../SouscriptionValidation/SouscriptionListReducer";

@connect(
  (state) => ({
    maxPages: state.SouscriptionListReducer.souscriptionMaxPages,
    search: state.SouscriptionListReducer.search,
    index: state.SouscriptionListReducer.index,
  }),
  { ...SouscriptionActions, initializeWithKey }
)
export default class PaginationSouscriptionValidation extends Component {
  static propTypes = {
    loadSouscriptionList: PropTypes.func,
    maxPages: PropTypes.number,
  };
  constructor() {
    super();
    this.state = {
      activePage: 1,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.index !== nextProps.index &&
      this.props.index !== 1 &&
      nextProps.index === 1
    ) {
      this.setState({ activePage: nextProps.index });
    }
  }

  render() {
    const { loadSouscriptionList, maxPages, search } = this.props;
    let maxPage = 0;
    if (maxPages % 10 === 0) {
      maxPage = maxPages / 10;
    } else {
      maxPage = Math.ceil(maxPages / 10);
    }
    const changePage = (event) => {
      this.setState({
        activePage: event,
      });
      if (search && search !== "") {
        loadSouscriptionList(event, search);
      } else {
        loadSouscriptionList(event, "");
      }
    };
    return (
      <div>
        {maxPages > 10 ? (
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={maxPage}
            maxButtons={5}
            bsSize="medium"
            activePage={this.state.activePage}
            onSelect={changePage}
          />
        ) : (
          <div />
        )}
      </div>
    );
  }
}
