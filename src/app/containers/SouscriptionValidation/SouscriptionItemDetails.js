/**
 * Created by lenovo on 27/12/2018.
 */

import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Row,
  Col,
  FormControl,
  ButtonGroup,
  Modal,
  ControlLabel,
  PageHeader,
} from "react-bootstrap";
import { connect } from "react-redux";
import { reduxForm, initializeWithKey } from "redux-form";
import { translate } from "react-i18next";
import DatePicker from "react-datepicker";
import moment from "moment";
import Button from "react-bootstrap-button-loader";
import * as SouscriptionActions from "./SouscriptionListReducer";
import Attachments from "../../components/Attachement/Attachments";
import * as AttachementActions from "../../components/Attachement/AttachementReducer";
import { asyncConnect } from "redux-connect";

@reduxForm(
  {
    form: "SouscriptionValidation",
    fields: [
      "adresse",
      "dateValidite",
      "numeroIdentite",
      "dateNaissance",
      "lieuNaissance",
      "motif",
    ],
  },
  (state) => ({
    initialValues: {
      adresse: "",
      dateValidite: "",
      dateNaissance: "",
      lieuNaissance: "",
      numeroIdentite: "",
      motif: "",
    },
  })
)
@asyncConnect([
  {
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
      dispatch(SouscriptionActions.isSironActivated());
    },
  },
])
@connect(
  (state) => ({
    listMotif: state.SouscriptionListReducer.listMotif,
    loading: state.SouscriptionListReducer.loadingV,
    DocumentsLoadSuccess: state.AttachementReducer.cashDocumentsLoadSuccess,
    sironActived: state.SouscriptionReducer.showSironFields,
    isSuccess: state.SouscriptionListReducer.isSuccess,
  }),
  { ...SouscriptionActions, ...AttachementActions, initializeWithKey }
)
@translate(["souscription"], { wait: true })
export default class SouscriptionRecap extends Component {
  constructor() {
    super();
    this.getInitialState();
    this.handleChangeMotif = this.handleChangeMotif.bind(this);
    this.handleFiles = this.handleFiles.bind(this);
    this.handleChangeDateNaissance = this.handleChangeDateNaissance.bind(this);
    this.handleChangeDateValidite = this.handleChangeDateValidite.bind(this);
    this.handleChangeAdresse = this.handleChangeAdresse.bind(this);
    this.handleChangeCin = this.handleChangeCin.bind(this);
    this.handleChangeLieuNaissance = this.handleChangeLieuNaissance.bind(this);
    const _self = this;
    this.filesClickEvents = [_self.close, _self.deleteDoc];
    this.filesClickEvent = [];

    setTimeout(() => {
      this.props.unlockDemande(this.props.data.id);
    }, 420000);
  }

  input(val) {
    console.log(val);
  }

  componentWillUnmount() {
    console.log("Start unlocking task");
    this.props.unlockDemande(this.props.data.id);
    this.props.resetDocuments();
    console.log("task unlocked");
  }

  getInitialState() {
    this.state = {
      fichierjointes: "",
      error: "",
      errors: {
        dateValidite: "",
        dateNaissance: "",
        lieuNaissance: "",
        adresse: "",
      },
    };
  }

  handleFiles(files) {
    this.setState({
      fichierjointes: files,
    });
  }

  handleChangeDateNaissance(date) {
    this.setState({ dateNaissance: date });
    this.state.errors.dateNaissance = "";
    if (moment(date, "DD/MM/YYYY") > moment().subtract(18, "years")) {
      this.state.errors.dateNaissance = this.props.t(
        "form.message.dateInvalid"
      );
    }
  }

  handleChangeDateValidite(date) {
    this.setState({ dateValidite: date });
    this.state.errors.dateValidite = "";
    if (
      moment(date, "DD/MM/YYYY") < moment() ||
      moment(date, "DD/MM/YYYY") > moment().add(10, "years")
    ) {
      this.state.errors.dateValidite = this.props.t("form.message.dateInvalid");
    }
  }

  handleChangeMotif(event) {
    this.setState({ motif: event.target.value });
    this.setState({ error: "" });
  }

  handleUpdate() {
    // this.props.reset();
    // console.log(this.props);
    this.getInitialState();
  }

  handleChangeAdresse(event) {
    if (event.target.value !== "") {
      this.state.errors.adresse = "";
    }
    this.setState({ adresse: event.target.value });
  }

  handleChangeCin(event) {
    if (event.target.value !== "" && event.target.value.length < 10) {
      this.state.errors.numeroIdentite = "";
    } else if (event.target.value.length > 10) {
      this.state.errors.numeroIdentite = this.props.t("form.message.numLong");
    }
    this.setState({ numeroIdentite: event.target.value });
  }

  handleChangeLieuNaissance(event) {
    if (event.target.value !== "") {
      this.state.errors.lieuNaissance = "";
    }
    this.setState({ lieuNaissance: event.target.value });
  }

  handleReject(values, data) {
    if (
      !values.motif ||
      values.motif === "" ||
      values.motif === "Choisir Motif :"
    ) {
      this.state.error = this.props.t("form.message.champObligatoire");
    } else {
      this.state.error = "";
      this.props.rejectSouscription(values, data);
    }
  }

  async handleSign(values, data) {
    let sign = true;

    if (data && data.typeCode === "type_one") {
      if (!values.numeroIdentite || values.numeroIdentite === "") {
        this.state.errors.numeroIdentite = this.props.t(
          "form.message.champObligatoire"
        );
        sign = false;
      }

      if (values.numeroIdentite && values.numeroIdentite.length > 10) {
        this.state.errors.numeroIdentite = this.props.t("form.message.numLong");
        sign = false;
      }
    }

    if (!this.props.sironActived || this.props.sironActived === false) {
      if (!values.dateValidite || values.dateValidite === "") {
        this.state.errors.dateValidite = this.props.t(
          "form.message.champObligatoire"
        );
        sign = false;
      } else if (moment(values.dateValidite, "DD/MM/YYYY") < moment()) {
        this.state.errors.dateValidite = this.props.t(
          "form.message.dateInvalid"
        );
        sign = false;
      }

      if (!values.dateNaissance || values.dateNaissance === "") {
        this.state.errors.dateNaissance = this.props.t(
          "form.message.champObligatoire"
        );
        sign = false;
      } else if (
        moment(values.dateNaissance, "DD/MM/YYYY") >
        moment().subtract(18, "years")
      ) {
        this.state.errors.dateNaissance = this.props.t(
          "form.message.dateInvalid"
        );
        sign = false;
      }

      if (!values.lieuNaissance || values.lieuNaissance === "") {
        this.state.errors.lieuNaissance = this.props.t(
          "form.message.champObligatoire"
        );
        sign = false;
      }

      if (!values.adresse || values.adresse === "") {
        this.state.errors.adresse = this.props.t(
          "form.message.champObligatoire"
        );
        sign = false;
      }
    }

    if (
      values.motif !== null &&
      values.motif !== "" &&
      values.motif !== "Choisir Motif :"
    ) {
      this.setState({ showModal: true });
      values.motif = "Choisir Motif :";
      this.setState({ motif: "Choisir Motif :" });
    } else {
      if (sign) {
        await this.props.signSouscription(values, data);
        window.scrollTo(0, -500);
      }
    }
  }

  render() {
    const close = () => {
      this.setState({ showModal: false });
    };

    const {
      t,
      handleSubmit,
      data,
      isSuccess,
      errorMsg,
      showSironFields,
      DocumentsLoadSuccess,
      listMotif,
      loading,
      loadedDocuments,
      values,
      fields: {
        adresse,
        numeroIdentite,
        dateValidite,
        dateNaissance,
        lieuNaissance,
        motif,
      },
    } = this.props;

    const styles = require("./souscriptionList.scss");

    return (
      <div>
        <PageHeader>
          <h3>{t("form.titleForm.souscriptionDetails")}</h3>
        </PageHeader>

        {isSuccess && isSuccess === true && (
          <div className="alert alert-success">{errorMsg}</div>
        )}
        {errorMsg && errorMsg !== "" && isSuccess === false && (
          <div className="alert alert-danger">{errorMsg}</div>
        )}

        <Row>
          <form className="formContainer">
            <Row className={styles.fieldRow}>
              <Col xs="12" md="12">
                <ControlLabel>{t("form.label.packChoisi")}</ControlLabel>
                <p className="detail-value">{data.typeLibelle}</p>
              </Col>
            </Row>

            <fieldset style={{ marginTop: "20px" }}>
              <legend>{t("form.legend.infoperso")}</legend>
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.dateCreation")}</ControlLabel>
                  <p className="detail-value">{data.dateCreation}</p>
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.statut")}</ControlLabel>
                  <p className="detail-value">{data.statut}</p>
                </Col>
              </Row>

              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.nom")}</ControlLabel>
                  <p className="detail-value">{data.nom}</p>
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.prenom")} </ControlLabel>
                  <p className="detail-value">{data.prenom}</p>
                </Col>
              </Row>

              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.phone")} </ControlLabel>
                  <p className="detail-value">{data.gsm}</p>
                </Col>
                {data.email && data.email !== null && data.email !== "" && (
                  <Col xs="12" md="6">
                    <ControlLabel>{t("form.label.mail")} </ControlLabel>
                    <p className="detail-value">{data.email}</p>
                  </Col>
                )}
              </Row>

              <Row className={styles.fieldRow}>
                {data && data.typePieceIdentite === "MAR" && (
                  <Col xs="12" md="6">
                    <ControlLabel>{t("form.label.pieceIdentite")}</ControlLabel>
                    <p className="detail-value">{t("form.label.cin1")}</p>
                  </Col>
                )}

                {data && data.typePieceIdentite === "PSP" && (
                  <Col xs="12" md="6">
                    <ControlLabel>{t("form.label.pieceIdentite")}</ControlLabel>
                    <p className="detail-value"> {t("form.label.passeport")}</p>
                  </Col>
                )}

                {data && data.typePieceIdentite === "CSJ" && (
                  <Col xs="12" md="6">
                    <ControlLabel>{t("form.label.pieceIdentite")}</ControlLabel>
                    <p className="detail-value">
                      {t("form.label.carteSejour1")}
                    </p>
                  </Col>
                )}

                {data.numeroIdentite &&
                  data.numeroIdentite !== null &&
                  data.typePieceIdentite &&
                  data.typePieceIdentite === "MAR" && (
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.cin")} </ControlLabel>
                      <p className="detail-value">{data.numeroIdentite}</p>
                    </Col>
                  )}
                {data.numeroIdentite &&
                  data.numeroIdentite !== null &&
                  data.typePieceIdentite &&
                  data.typePieceIdentite === "PSP" && (
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.numpasseport")}{" "}
                      </ControlLabel>
                      <p className="detail-value">{data.numeroIdentite}</p>
                    </Col>
                  )}
                {data.numeroIdentite &&
                  data.numeroIdentite !== null &&
                  data.typePieceIdentite &&
                  data.typePieceIdentite === "CSJ" && (
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.carteSejour")}{" "}
                      </ControlLabel>
                      <p className="detail-value">{data.numeroIdentite}</p>
                    </Col>
                  )}
              </Row>
              {data.typeCode !== "type_one" && data.typeCode !== "type_two" && (
                <Row className={styles.fieldRow}>
                  <Col xs="12" md="6">
                    <ControlLabel>
                      {t("form.label.typeJustification")}{" "}
                    </ControlLabel>
                    <p className="detail-value">{data.typeJustifResidence}</p>
                  </Col>
                  {showSironFields && (
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {" "}
                        {t("form.label.activite")} activite
                      </ControlLabel>
                      <p className="detail-value">{data.activite}</p>
                    </Col>
                  )}
                  <Col xs="12" md="6">
                    <ControlLabel> {t("form.label.revenu")} </ControlLabel>
                    <p className="detail-value">{data.sourceRevenu}</p>
                  </Col>
                </Row>
              )}
            </fieldset>
            {data.typeCode === "type_pro" && (
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel> {t("form.label.RaisonSocial")} </ControlLabel>
                  <p className="detail-value">{data.nomRaisonSociale}</p>
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel>{t("form.label.CodeSocial")}</ControlLabel>
                  <p className="detail-value">{data.codeRaisonSociale}</p>
                </Col>
              </Row>
            )}
            {data.typeCode === "type_pro" && (
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel> {t("form.label.NumPatente")}</ControlLabel>
                  <p className="detail-value">{data.numeroPatente}</p>
                </Col>
                <Col xs="12" md="6">
                  <ControlLabel> {t("form.label.mcc")} </ControlLabel>
                  <p className="detail-value">{data.mcc}</p>
                </Col>
              </Row>
            )}
            {data.typeCode === "type_pro" && (
              <Row className={styles.fieldRow}>
                <Col xs="12" md="6">
                  <ControlLabel> {t("form.label.CodeTribunal")} </ControlLabel>
                  <p className="detail-value">{data.codeTribunal}</p>
                </Col>
              </Row>
            )}

            {loadedDocuments &&
              loadedDocuments !== null &&
              data.attachementRequired &&
              data.attachementRequired === true &&
              loadedDocuments.length !== 0 && (
                <fieldset style={{ marginTop: "20px" }}>
                  <legend>{t("form.legend.piecejoints")}</legend>
                  <form ref="form">
                    <DownloadedFiles
                      files={loadedDocuments}
                      filesClickEvent={this.filesClickEvent}
                      isConsultOnly={true}
                    />
                  </form>
                </fieldset>
              )}

            {isSuccess !== true && !showSironFields && (
              <Row className={styles.fieldRow}>
                <fieldset style={{ marginTop: "20px" }}>
                  <Row>
                    <Col xs="12" md="6"></Col>
                  </Row>
                  <legend>{t("form.legend.infosComp")}</legend>
                  <Row>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.cin")}</ControlLabel>
                      <FormControl
                        type="text"
                        {...numeroIdentite}
                        value={this.state.numeroIdentite}
                        onChange={this.handleChangeCin}
                        className={styles.datePickerFormControl}
                      />
                      {this.state.errors.numeroIdentite &&
                        this.state.errors.numeroIdentite !== "" && (
                          <div className={styles.error}>
                            <i
                              className="fa fa-exclamation-triangle"
                              aria-hidden="true"
                            >
                              {this.state.errors.numeroIdentite}
                            </i>
                          </div>
                        )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.validite")}</ControlLabel>
                      <span {...dateValidite}>
                        <DatePicker
                          minDate={moment()}
                          maxDate={moment().add(10, "years")}
                          selected={this.state.dateValidite}
                          dateValidite={this.state.dateValidite}
                          placeholderText={t("form.label.validite")}
                          onChange={this.handleChangeDateValidite}
                          className={styles.datePickerFormControl}
                          isClearable="true"
                          locale="fr-FR"
                          dateFormat="DD/MM/YYYY"
                        />
                      </span>
                      {this.state.errors.dateValidite &&
                        this.state.errors.dateValidite !== "" && (
                          <div className={styles.error}>
                            <i
                              className="fa fa-exclamation-triangle"
                              aria-hidden="true"
                            >
                              {this.state.errors.dateValidite}
                            </i>
                          </div>
                        )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>
                        {t("form.label.dateNaissance")}
                      </ControlLabel>
                      <span {...dateNaissance}>
                        <DatePicker
                          maxDate={moment().subtract(18, "years")}
                          selected={this.state.dateNaissance}
                          dateNaissance={this.state.dateNaissance}
                          placeholderText={t("form.label.dateNaissance")}
                          onChange={this.handleChangeDateNaissance}
                          className={styles.datePickerFormControl}
                          isClearable="true"
                          locale="fr-FR"
                          dateFormat="DD/MM/YYYY"
                        />
                      </span>
                      {this.state.errors.dateNaissance &&
                        this.state.errors.dateNaissance !== "" && (
                          <div className={styles.error}>
                            <i
                              className="fa fa-exclamation-triangle"
                              aria-hidden="true"
                            >
                              {this.state.errors.dateNaissance}
                            </i>
                          </div>
                        )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.lieu")}</ControlLabel>
                      <FormControl
                        type="text"
                        {...lieuNaissance}
                        value={this.state.lieuNaissance}
                        onChange={this.handleChangeLieuNaissance}
                        className={styles.datePickerFormControl}
                      />
                      {this.state.errors.lieuNaissance &&
                        this.state.errors.lieuNaissance !== "" && (
                          <div className={styles.error}>
                            <i
                              className="fa fa-exclamation-triangle"
                              aria-hidden="true"
                            >
                              {this.state.errors.lieuNaissance}
                            </i>
                          </div>
                        )}
                    </Col>
                    <Col xs="12" md="6">
                      <ControlLabel>{t("form.label.adresse")}</ControlLabel>
                      <FormControl
                        type="text"
                        {...adresse}
                        value={this.state.adresse}
                        onChange={this.handleChangeAdresse}
                        className={styles.datePickerFormControl}
                      />
                      {this.state.errors.adresse &&
                        this.state.errors.adresse !== "" && (
                          <div className={styles.error}>
                            <i
                              className="fa fa-exclamation-triangle"
                              aria-hidden="true"
                            >
                              {this.state.errors.adresse}
                            </i>
                          </div>
                        )}
                    </Col>
                  </Row>
                </fieldset>
              </Row>
            )}
            {isSuccess !== true && (
              <fieldset style={{ marginTop: "20px" }}>
                <Row className={styles.fieldRow}>
                  <Row>
                    <Col xs="12" md="6">
                      <legend>{t("form.legend.motif")}</legend>
                      <ControlLabel>
                        {t("form.legend.motifreject")}
                      </ControlLabel>
                      <FormControl
                        componentClass="select"
                        {...motif}
                        onChange={this.handleChangeMotif}
                        value={this.state.motif ? this.state.motif : ""}
                        className={styles.datePickerFormControl}
                      >
                        <option hidden>{t("form.message.choisirMotif")}</option>
                        {listMotif &&
                          listMotif.length &&
                          listMotif.map((liste) => (
                            <option key={liste.id} value={liste.code}>
                              {liste.libelle}
                            </option>
                          ))}
                      </FormControl>
                      {this.state.error && this.state.error !== "" && (
                        <div>
                          <i
                            className="fa fa-exclamation-triangle"
                            aria-hidden="true"
                          >
                            {this.state.error}
                          </i>
                        </div>
                      )}
                    </Col>
                  </Row>
                </Row>
              </fieldset>
            )}
            {isSuccess !== true && (
              <fieldset style={{ marginTop: "20px" }}>
                <form ref="form">
                  <Row className={styles.paddingColumn}>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        onClick={handleSubmit(() => {
                          this.handleSign(values, data);
                        })}
                        bsStyle="primary"
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.submit")}
                      </Button>
                    </div>
                    <div className="pull-right" style={{ paddingTop: "10px" }}>
                      <Button
                        onClick={handleSubmit(() => {
                          this.handleReject(values, data);
                        })}
                        bsStyle="primary"
                      >
                        <i className="fa fa-check " />
                        {t("form.buttons.reject")}
                      </Button>
                    </div>
                  </Row>
                </form>
              </fieldset>
            )}
          </form>
        </Row>

        <Modal
          show={loading || !DocumentsLoadSuccess}
          className="loadingModal"
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>
            <Row>
              <Col xs={12} md={12}>
                <div className="spinner">
                  <span style={{ fontSize: "11px" }}>
                    {t("chargement.title")}
                  </span>
                </div>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>
        <Modal
          show={this.state.showModal}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              <div>{t("popup.motif.title")}</div>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>{t("popup.motif.msg")}</div>
          </Modal.Body>
          <Modal.Footer>
            <ButtonGroup className="pull-right" bsSize="small">
              <Button
                className={styles.ButtonPasswordStyle}
                onClick={() => close()}
              >
                {t("popup.motif.noBtn")}
              </Button>
            </ButtonGroup>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

class DownloadedFiles extends Component {
  render() {
    const { files, isConsultOnly, handleFiles, styles } = this.props;
    return (
      <fieldset>
        <Row>
          <Col xs={12} sm={12} md={6}>
            {!isConsultOnly /*File input that allows to list, add and delete files provided from the previous actions on this transaction*/ && (
              <Attachments
                title={"ajouter fichiers"}
                handleFiles={handleFiles}
                files={files}
                filesClickEvents={false}
                styles={styles}
              />
            )}
            {isConsultOnly /*File input that allows to list, add and delete files provided from the previous actions on this transaction*/ && (
              <Attachments
                title={"Télécharger les fichiers"}
                files={files}
                filesClickEvents={true}
              />
            )}
          </Col>
        </Row>
      </fieldset>
    );
  }
}
