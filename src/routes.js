import React from "react";
import { IndexRoute, Route, IndexRedirect, Redirect } from "react-router";
import App from "./app/containers/App/App";
import Login from "./app/containers/Login/Login";
import Historique from "./app/containers/Historique/Historique";
import MoneyTransfer from "./app/containers/MoneyTransfer/MoneyTransfer";

const routes = (store) => {
  return (
    <Route>
      <Route path={baseUrl + "app"} component={App}>
        {/* Home (main) route */}
        <IndexRoute component={Historique} />
        {/* MoneyTransfer */}
        <Route path="request/MoneyTransfer" component={MoneyTransfer} />
      </Route>
      <Route path={baseUrl + "login"} component={Login} />
      {/* <Redirect from="*" to="/app" /> */}
    </Route>
  );
};

export default routes;
