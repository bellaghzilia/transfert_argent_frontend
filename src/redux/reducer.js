import { combineReducers } from "redux";
import app from "../app/containers/App/appReducer";
import { routerReducer } from "react-router-redux";
import { reducer as reduxAsyncConnect } from "redux-connect";
import { reducer as form } from "redux-form";
import { loadingBarReducer } from "react-redux-loading-bar";
import user from "../app/containers/User/UserReducer";
import loginReducer from "../app/containers/Login/loginReducer";

import HistoriqueReducer from "../app/containers/Historique/HistoriqueReducer";
import HistoriqueAgentDetaillantReducer from "../app/containers/HistoriqueAgent/HistoriqueAgentDetaillantReducer";
import HistoriqueAgentPrincipalReducer from "../app/containers/HistoriqueAgent/HistoriqueAgentPrincipalReducer";

import CashingReducer from "../app/containers/Cashing/CashingReducer";
// import CashingAgentReducer from '../containers/CashingAgent/CashingAgentReducer';

import SouscriptionReducer from "../app/containers/Souscription/SouscriptionReducer";
import SouscriptionListReducer from "../app/containers/SouscriptionValidation/SouscriptionListReducer";
import SouscriptionRejeteListReducer from "../app/containers/SouscriptionRejete/SouscriptionRejeteListReducer";

// import AttachementReducer from "../components/Attachement/AttachementReducer";
// import UpgradeReducer from '../containers/upgrade/UpgradeReducer';
// import UpdateClientReducer from '../containers/clients/UpdateClientReducer';
// import UpgradeListReducer from '../containers/UpgradeValidation/UpgradeListReducer';
// import UpgradeRejeteListReducer from '../containers/UpgradeRejete/UpgradeRejeteListReducer';

import moneyTransfer from "../app/containers/MoneyTransfer/moneytransferreducer";
import ParametrageReducer from "../app/containers/MoneyTransfer/ParametrageReducer";

// import VirementReducer from '../containers/Virement/VirementReducer';
// import paiemntBills from '../containers/PaiementFacture/paiemntBillsReducer';
// import CreateAgentReducer from '../containers/Agent/CreateAgentReducer';
// import CreateAgentPrincipalReducer from '../containers/AgentPrincipal/CreateAgentPrincipalReducer';
// import EnrolementDistanceReducer from '../containers/EnrolementDistance/EnrolementDistanceReducer';
// import FiltrageCreanciersReducer from '../containers/FiltrageCreanciers/FiltrageCreanciersReducer';
// import TraitementMasseReducer from '../containers/TraitementMasse/TraitementMasseReducer';
// import CartesReducer from '../containers/Cartes/CartesReducer';
// import UpdateAgentReducer from '../containers/Agent/UpdateAgentReducer';
// import ReclamationReducer from "../containers/Reclamation/ReclamationReducer";
// import NotificationPushMasseReducer from "../containers/NotificationPushMasse/NotificationPushMasseReducer";

export default combineReducers({
  app,
  routing: routerReducer,
  reduxAsyncConnect,
  loadingBar: loadingBarReducer,
  form,
  user,
  loginReducer,
  HistoriqueReducer,
  CashingReducer,
  SouscriptionReducer,
  moneyTransfer,
  ParametrageReducer,
  HistoriqueAgentDetaillantReducer,
  HistoriqueAgentPrincipalReducer,
  SouscriptionListReducer,
  SouscriptionRejeteListReducer,
});
