import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { applyRouterMiddleware, browserHistory, Router } from "react-router";
import { syncHistoryWithStore } from "react-router-redux";
import { ReduxAsyncConnect } from "redux-connect";
// import { ReduxAsyncConnect } from "redux-connect";
// import useScroll from "react-router-scroll";

import ApiClient from "./app/helpers/ApiClient";
import createStore from "./redux/storeCreator";
import routes from "./routes";

import { CookiesProvider } from "react-cookie";

// I18n
import "./i18n";

const client = new ApiClient();
const dest = document.getElementById("root");
const store = createStore(client);
const history = syncHistoryWithStore(browserHistory, store);

const component = (
  <Router
    history={history}
    render={(props) => (
      <ReduxAsyncConnect
        {...props}
        filter={(item) => !item.deferred}
        helpers={{ client }}
        render={applyRouterMiddleware()}
      />
    )}
  >
    {routes(store)}
  </Router>
);

ReactDOM.render(
  <Provider key="provider" store={store}>
    {component}
  </Provider>,
  dest
);

if (process.env.NODE_ENV !== "production") {
  window.React = React; // Enable debugger
}

// const client = new ApiClient();
// const dest = document.getElementById("root");
// /* jslint nomen: true*/
// const store = createStore(browserHistory, client, window.__data);
// /* jslint nomen: false*/
// const history = syncHistoryWithStore(browserHistory, store);

// const component = (
//   <Router
//     render={(props) => (
//       <ReduxAsyncConnect
//         {...props}
//         helpers={{ client }}
//         filter={(item) => !item.deferred}
//         render={applyRouterMiddleware(useScroll())}
//       />
//     )}
//     history={history}
//     routes={getRoutes(store)}
//   />
// );

// ReactDOM.render(
//   <I18nextProvider i18n={i18n}>
//     <Provider store={store} key="provider">
//       {component}
//     </Provider>
//   </I18nextProvider>,
//   dest
// );

// if (process.env.NODE_ENV !== "production") {
//   window.React = React; // enable debugger

//   if (
//     !dest ||
//     !dest.firstChild ||
//     !dest.firstChild.attributes ||
//     !dest.firstChild.attributes["data-react-checksum"]
//   ) {
//     console.info(
//       "Server-side React render was discarded. Make sure that your initial render does not contain any client-side code."
//     );
//   }
// }
